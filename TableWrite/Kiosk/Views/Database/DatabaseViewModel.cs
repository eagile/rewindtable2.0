﻿using EA.Helpers.WPF;
using GalaSoft.MvvmLight.Messaging;
using Kiosk.Messages;
using Kiosk.Views.BaseModels;
using KioskManagerLib;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Kiosk.Views
{
    public class DatabaseViewModel : ViewModel
    {
        public string MaxdbSizeDisplay => string.Format(Properties.Strings.Database_Max_Size, Controller.CurrentSettings?.Kiosk?.DatabaseMaxSizeGB ?? 0);

        public string CurrentdbSizeDisplay => string.Format(Properties.Strings.Database_Current_Size, Controller.CurrentDatabaseSizeGB);

        public TableController Controller { get; }
        
        public RelayCommand BackUpDatabaseCommand => _backUpDatabaseCommand ?? (_backUpDatabaseCommand = new RelayCommand(ExecuteBackUpDatabaseCommand, CanBackUpDatabase));
        private RelayCommand _backUpDatabaseCommand;

        public RelayCommand ClearLogsCommand => _clearLogsCommand ?? (_clearLogsCommand = new RelayCommand(ExecuteClearLogsCommand, CanClearLogs));
        private RelayCommand _clearLogsCommand;

        public bool IsBusy => IsBackingUp || IsClearing;

        public bool IsBackingUp { get => _isBackingUp; set => SetProperty(ref _isBackingUp, value, nameof(IsBackingUp), nameof(IsBusy)); }
        private bool _isBackingUp;

        public bool IsClearing { get => _isClearing; set => SetProperty(ref _isClearing, value, nameof(IsClearing), nameof(IsBusy)); }
        private bool _isClearing;

        public DatabaseViewModel()
        {
            Controller = TableController.Instance;
            Controller.DatabaseSizeUpdated += Controller_DatabaseSizeUpdated;
            Controller.LanguageChanged += Controller_LanguageChanged;

            RegisterMessages();
        }

        private bool CanBackUpDatabase()
        {
            return !IsBusy;
        }

        private async Task ExecuteBackUpDatabaseCommand()
        {
            try
            {
                IsBackingUp = true;
                await Controller.BackUpDatabase().ConfigureAwait(true); ;
            }
            catch { }
            finally
            {
                IsBackingUp = false;
            }
        }

        private bool CanClearLogs()
        {
            return !IsBusy;
        }

        private async Task ExecuteClearLogsCommand()
        {
            try
            {
                IsClearing = true;

                var Countmessage = new MessageBoxesView($"{ Properties.Strings.Database_Truncate_warning}{System.Environment.NewLine}{Properties.Strings.Database_Truncate_warning2}"
                        , MessageBoxButton.YesNo, MessageBoxImage.Information);

                bool? dialogResult = Countmessage.ShowDialog();

                switch (Countmessage.Result)
                {
                    case MessageBoxResult.Yes:
                        await Controller.TruncateDatabase().ConfigureAwait(true);
                        break;
                    default:
                        break;
                }
            }
            catch { }
            finally
            {
                IsClearing = false;
            }
        }

        private void Controller_DatabaseSizeUpdated(object sender, EventArgs e)
        {
            OnPropertyChanged(nameof(MaxdbSizeDisplay));
            OnPropertyChanged(nameof(CurrentdbSizeDisplay));
        }

        private void Controller_LanguageChanged(object sender, System.Globalization.CultureInfo e)
        {
            RefreshProperties();
        }

        public override void RegisterMessages()
        {
            Messenger.Default.Register<DatabaseFocusMessage>(this, ExecuteDatabaseFocusMessage);
        }

        public override void UnRegisterMessages()
        {
            Messenger.Default.Unregister<DatabaseFocusMessage>(this, ExecuteDatabaseFocusMessage);
        }

        private void ExecuteDatabaseFocusMessage(DatabaseFocusMessage msg)
        {
        }

    }
}
