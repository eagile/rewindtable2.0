﻿using EA.Helpers.WPF;
using EA.Impinj;
using Kiosk.Views.BaseModels;
using Kiosk.Views.Configuration;
using KioskManagerLib;
using KioskManagerLib.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using static EAReaderSDK.SupportClasses.DeviceTypes;

namespace Kiosk.Views
{
    public class RFIDManagementViewModel : ViewModel
    {
        public TableController Controller { get; }
        public EAReaderSDK.Settings.RfidReaderSettings RfidReader { get; set; }
        public EAReaderSDK.Settings.RfidValidatorSettings RfidValidator { get; set; }
        public EAReaderSDK.Settings.RfidWriterSettings RfidWriter { get; set; }

        private DisplaySettings _Selected_Settings;
        public DisplaySettings Selected_Settings
        {
            get => _Selected_Settings;
            set
            {
                SetProperty(ref _Selected_Settings, value);
           
            }
        }



        private string _selectedReader;
        public string SelectedReader
        {
            get => _selectedReader;
            set
            {
                SetProperty(ref _selectedReader, value);
                SetDisplay(_selectedReader);
            }
        }

        private string[] _readersList = new string[] { "Indexer", "Writer", "Validator" };
        public string[] ReadersList
        {
            get => _readersList;
            set
            {
                SetProperty(ref _readersList, value);
            }
        }


        private DeviceType[] _deviceTypeList;
        public DeviceType[] DeviceTypeList
        {
            get => _deviceTypeList;
            set
            {
                SetProperty(ref _deviceTypeList, value);
            }
        }

        private DeviceType _selectedDeviceType;
        public DeviceType SelectedDeviceType
        {
            get => _selectedDeviceType;
            set
            {
                SetProperty(ref _selectedDeviceType, value);
            }
        }


        private ReaderMode[] _readerModeList;
        public ReaderMode[] ReaderModeList
        {
            get => _readerModeList;
            set
            {
                SetProperty(ref _readerModeList, value);
            }
        }

        private ReaderMode _selectedReaderMode;
        public ReaderMode SelectedReaderMode
        {
            get => _selectedReaderMode;
            set
            {
                SetProperty(ref _selectedReaderMode, value);
            }
        }


        private Session[] _sessionList;
        public Session[] SessionList
        {
            get => _sessionList;
            set
            {
                SetProperty(ref _sessionList, value);
            }
        }

        private Session _selectedsession;
        public Session Selectedsession
        {
            get => _selectedsession;
            set
            {
                SetProperty(ref _selectedsession, value);
            }
        }

        private SearchMode[] _searchModeList;
        public SearchMode[] SearchModeList
        {
            get => _searchModeList;
            set
            {
                SetProperty(ref _searchModeList, value);
            }
        }

        private SearchMode _selectedsearchMode;
        public SearchMode SelectedsearchMode
        {
            get => _selectedsearchMode;
            set
            {
                SetProperty(ref _selectedsearchMode, value);
            }
        }

        //public RelayCommand SaveCommand => _startCommand ?? (_startCommand = new RelayCommand(ExecuteSaveCommand, CanSave));
        //private RelayCommand _startCommand;
        //public RelayCommand EnterKeyCommand => _enterKeyCommand ?? (_enterKeyCommand = new RelayCommand(ExecuteEnterKeyCommand, CanSave));
        //private RelayCommand _enterKeyCommand;
        


        //EditTimeSheetRowCommand = new RelayCommand<RouteListModel>((row) => ExecuteEditTimeSheetRowCommand(row));

        public RFIDManagementViewModel()
        {
            Controller = TableController.Instance;
            Controller.SettingsLoaded += Controller_SettingsLoaded; 
            DeviceTypeList = (DeviceType[])Enum.GetValues(typeof(DeviceType));
            ReaderModeList = (ReaderMode[])Enum.GetValues(typeof(ReaderMode));
            SessionList = (Session[])Enum.GetValues(typeof(Session));
            SearchModeList = (SearchMode[])Enum.GetValues(typeof(SearchMode));
            
            Selected_Settings = new DisplaySettings();

        }

        private void Controller_SettingsLoaded(object sender, EventArgs e)
        {
            //var aa = Controller.ParsedSettings.ParsedSettings;
            RfidReader = Controller.ParsedSettings.ParsedSettings[0].RfidReader;
            RfidWriter = Controller.ParsedSettings.ParsedSettings[0].RfidWriter;
            RfidValidator = Controller.ParsedSettings.ParsedSettings[0].RfidValidator;

            SelectedReader = ReadersList[0];

        }

        private bool CanSave()
        {

            return true;
        }
        private string CanSave2()
        {


            return "";
        }

        public SettingViewModel CurrentSetting { get => _currentSetting; set => SetProperty(ref _currentSetting, value); }
        public SettingViewModel _currentSetting;
        private string GetType(string name)
        {
            switch (name)
            {

                case "IsEnabled": case "UseETSI":case "LockCheck":case "UserCheck":

                    return "bit";

                case "Port": case "HeartbeatInterval":case "ConnectTimeout":case "CommandTimeout":case "SearchMode":
                    return "Int";

                case "ScanDuration": case "TagPopulationEstimate":case "RxSensitivityInDbm":case "ReaderMode": case "Session":
                    return "Int";

                case "TxPowerInDbm":
                    return "Float";

                case "Antennas": case "EtsiTxFrequenciesInMhz": case "Address":
                    return "String";
                
                
            }

            return "";
        }

        public async Task ChangeSettingAsync(string name, string newvalue)
        {

            CurrentSetting = new SettingViewModel();
            CurrentSetting.DefaultValue = Selected_Settings.GetType().GetProperty(name).GetValue(Selected_Settings, null).ToString();
            CurrentSetting.DeviceName = Selected_Settings.DeviceName;
            CurrentSetting.DeviceType = Selected_Settings.DeviceType.ToString();
            CurrentSetting.IsReasonForChangeRequired = false;
            CurrentSetting.IsValidationRequired = true;
            CurrentSetting.KioskName = "Kiosk";
            CurrentSetting.Level = KioskManagerLib.Configuration.SettingLevel.Kiosk;
            CurrentSetting.Name = name;

            CurrentSetting.Type = GetType(name);
            CurrentSetting.ProductName = "Default";
            CurrentSetting.ReasonForChange = "";
            CurrentSetting.Value = Selected_Settings.GetType().GetProperty(name).GetValue(Selected_Settings, null).ToString();
            if (CurrentSetting != null)
            {
                var Countmessage = new ConfigurationView(CurrentSetting);

                Countmessage.Top = 10;
                Countmessage.Left = 300;
                Countmessage.Topmost = true;

                bool? dialogResult = Countmessage.ShowDialog();

                switch (Countmessage.Result)
                {
                    case MessageBoxResult.Yes:
                        List<ChangedDbSetting> changedDbSettings = new List<ChangedDbSetting>();
                        List<DbSetting> allDbSettings = new List<DbSetting>();
                        List<string> productNames = new List<string>();
                        var ctx = (ConfigurationViewModel)Countmessage.DataContext;
                        var ctxsettings = (SettingsViewModel)Countmessage.DataContext;
                        foreach (DeviceSettingsViewModel device in ctxsettings.DeviceSettingsData)
                        {
                            if (device.Name == ctx.CurrentModel.DeviceName)
                            {
                                device.SettingsConfig.FirstOrDefault(
                                    sc => sc.Name == ctx.CurrentModel.Name).Value = ctx.CurrentModel.Value;
                                device.SettingsConfig.FirstOrDefault(sc => sc.Name == ctx.CurrentModel.Name).ReasonForChange = ctx.ReasonForChange;
                                await ctxsettings.SaveSettingsChanges().ConfigureAwait(true);
                            }
                        }
                        break;
                    case MessageBoxResult.Cancel:

            //            ExecuteSettingsDiscardCommand();
                        break;
                };
            }

        }

        private void ExecuteEnterKeyCommand()
        {







            //};

            if (CurrentSetting != null)
            {

                var Countmessage = new ConfigurationView(CurrentSetting);

                Countmessage.Top = 10;
                Countmessage.Left = 300;
                Countmessage.Topmost = true;

                bool? dialogResult = Countmessage.ShowDialog();

                switch (Countmessage.Result)
                {
                    case MessageBoxResult.Yes:
                        //List<ChangedDbSetting> changedDbSettings = new List<ChangedDbSetting>();
                        //List<DbSetting> allDbSettings = new List<DbSetting>();
                        //List<string> productNames = new List<string>();
                        //var ctx = (ConfigurationViewModel)Countmessage.DataContext;
                        //foreach (DeviceSettingsViewModel device in DeviceSettingsData)
                        //{
                        //    if (device.Name == ctx.CurrentModel.DeviceName)
                        //    {
                        //        device.SettingsConfig.FirstOrDefault(
                        //            sc => sc.Name == ctx.CurrentModel.Name).Value = ctx.CurrentModel.Value;
                        //        device.SettingsConfig.FirstOrDefault(sc => sc.Name == ctx.CurrentModel.Name).ReasonForChange = ctx.ReasonForChange;
                        //        await SaveSettingsChanges().ConfigureAwait(true);
                        //    }
                        //}
                        break;
                    case MessageBoxResult.Cancel:

                       // ExecuteSettingsDiscardCommand();
                        break;
                };
            }




        }



        private void ExecuteSaveCommand()
        {

         //   _ = Controller.SaveReader();

        }


        private void SetDisplay(string readername)

        {
            if (RfidReader == null) return;

            object buildobject = null;

            if (readername == "Indexer")
            {
                buildobject = RfidReader;
            }
            if (readername == "Writer")
            {
                buildobject = RfidWriter;
            }
            if (readername == "Validator")
            {
                buildobject = RfidValidator;
            }


            Selected_Settings.DeviceName = buildobject.GetType().GetProperty("DeviceName").GetValue(buildobject, null).ToString();

            Selected_Settings.DeviceType = RfidReader.DeviceType;
            SelectedDeviceType = RfidReader.DeviceType;
            SelectedReaderMode = RfidReader.ReaderMode;
            Selectedsession = RfidReader.Session;
            SelectedsearchMode = RfidReader.SearchMode;
            Selected_Settings.IsEnabled = (bool)buildobject.GetType().GetProperty("IsEnabled").GetValue(buildobject, null);
            Selected_Settings.Address = buildobject.GetType().GetProperty("Address").GetValue(buildobject, null).ToString();
            Selected_Settings.Port = (int)buildobject.GetType().GetProperty("Port").GetValue(buildobject, null);
            Selected_Settings.HeartbeatInterval = (int)buildobject.GetType().GetProperty("HeartbeatInterval").GetValue(buildobject, null);
            Selected_Settings.ConnectTimeout = (int)buildobject.GetType().GetProperty("ConnectTimeout").GetValue(buildobject, null);
            Selected_Settings.CommandTimeout = (int)buildobject.GetType().GetProperty("CommandTimeout").GetValue(buildobject, null);
            Selected_Settings.UseETSI = (bool)buildobject.GetType().GetProperty("UseETSI").GetValue(buildobject, null);
            Selected_Settings.EtsiTxFrequenciesInMhz = string.Join(",", (List<double>)buildobject.GetType().GetProperty("EtsiTxFrequenciesInMhz").GetValue(buildobject, null));
            Selected_Settings.TagPopulationEstimate = (ushort)buildobject.GetType().GetProperty("TagPopulationEstimate").GetValue(buildobject, null);
            Selected_Settings.TxPowerInDbm = (double)buildobject.GetType().GetProperty("TxPowerInDbm").GetValue(buildobject, null);
            Selected_Settings.RxSensitivityInDbm = (double)buildobject.GetType().GetProperty("RxSensitivityInDbm").GetValue(buildobject, null);
            Selected_Settings.Antennas = string.Join(",", (List<int>)buildobject.GetType().GetProperty("Antennas").GetValue(buildobject, null));
            Selected_Settings.ScanDuration = (int)buildobject.GetType().GetProperty("ScanDuration").GetValue(buildobject, null); 
            Selected_Settings.LockCheck = (bool)buildobject.GetType().GetProperty("LockCheck").GetValue(buildobject, null); 
            Selected_Settings.UserCheck = (bool)buildobject.GetType().GetProperty("UserCheck").GetValue(buildobject, null); 
            OnPropertyChanged(nameof(Selected_Settings));
        }

        public override void RegisterMessages()
        {
           
        }

        public override void UnRegisterMessages()
        {
           
        }
    }

    public class DisplaySettings 
    {
        public string DeviceName { get; set; }
        public DeviceType DeviceType { get; set; }
        public bool IsEnabled { get; set; } 
        public string Address { get; set; } 
        public int Port { get; set; } 
        public int HeartbeatInterval { get; set; } 
        public int ConnectTimeout { get; set; } 
        public int CommandTimeout { get; set; } 
        public bool UseETSI { get; set; } 
        public string EtsiTxFrequenciesInMhz { get; set; } 
        public ReaderMode ReaderMode { get; set; } 
        public SearchMode SearchMode { get; set; } 
        public Session Session { get; set; } 
        public ushort TagPopulationEstimate { get; set; } 
        public double TxPowerInDbm { get; set; } 
        public double RxSensitivityInDbm { get; set; } 
        public string Antennas { get; set; } 
        public int ScanDuration { get; set; } 
        public bool LockCheck { get; set; }
        public bool UserCheck { get; set; }

        
    }
}
