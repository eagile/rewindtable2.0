﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

 namespace Kiosk.Views
{
    /// <summary>
    /// Interaction logic for RFIDManagement.xaml
    /// </summary>
    public partial class RFIDManagement : UserControl
    {
        public RFIDManagement()
        {
            InitializeComponent();
            DeviceName.GotFocus += DeviceName_GotFocus;
        }

        private async void DeviceName_GotFocus(object sender, RoutedEventArgs e)
        {
            if (sender is TextBox tb)
            {
                var ctx = (RFIDManagementViewModel)this.DataContext;
                await ctx.ChangeSettingAsync(tb.Name, tb.Name);
            }
        }

        
    }
}
