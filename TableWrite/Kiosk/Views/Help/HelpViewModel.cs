﻿namespace WriteRewind.Views.Help
{
    using EA.Helpers.WPF;
    using Kiosk.Views.BaseModels;
    using KioskManagerLib;
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Windows.Input;

    using System.IO;

    public class HelpViewModel : ViewModel
    {

        public string VersionNumber => System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();

        public string PLCVersion { get => _plcVersion; set => SetProperty(ref _plcVersion, value); }
        private string _plcVersion;

        public string VSVersion { get => _vsVersion; set => SetProperty(ref _vsVersion, value); }
        private string _vsVersion;

        public string WSVersion { get => _wsVersion; set => SetProperty(ref _wsVersion, value); }
        private string _wsVersion;

        public bool IsAvailable { get => _isAvailable; private set => SetProperty(ref _isAvailable, value); }
        private bool _isAvailable;

        public ICommand BackUpSystemCommand => _backUpSystemCommand ?? (_backUpSystemCommand = new RelayCommand(BackUpSystem, CanBackUp));
        private ICommand _backUpSystemCommand;
        public ICommand BackUpLogsCommand => _backUpLogsCommand ?? (_backUpLogsCommand = new RelayCommand(BackUpLogFiles, CanBackUp));
        private ICommand _backUpLogsCommand;

        public TableController Controller { get; }

        public HelpViewModel()
        {
            Controller = TableController.Instance;
            Controller.LoginChanged += Controller_LoginChanged;


        }

        private void Controller_LoginChanged(object sender, KioskManager.Events.KioskManagerException e)
        {
            IsAvailable = Controller?.CurrentUser?.HasRight(KioskRight.mayManageDatabase) ?? false;
        }


        private bool CanBackUp()
        {
            return IsAvailable;
        }
        private void BackUpLogFiles()
        {
            Controller.BackUpLogFiles(Kiosk.Properties.Strings.BackUp_LogFiles).ConfigureAwait(false);
        }

        private void BackUpSystem()
        {
            Controller.BackUpSystem(Kiosk.Properties.Strings.BackUp_System).ConfigureAwait(false);
        }

        public override void RegisterMessages()
        {

        }

        public override void UnRegisterMessages()
        {

        }
    }
}
