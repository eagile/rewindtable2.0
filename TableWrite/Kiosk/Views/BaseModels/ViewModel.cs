﻿using EA.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Kiosk.Views.BaseModels
{
    public abstract class ViewModel : ObservableObject
    {
        public abstract void RegisterMessages();
        public abstract void UnRegisterMessages();
    }
}
