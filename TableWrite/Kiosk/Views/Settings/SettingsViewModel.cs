﻿namespace Kiosk.Views
{
    using ceTe.DynamicPDF;
    using ceTe.DynamicPDF.LayoutEngine;
    using ceTe.DynamicPDF.Merger;
    using ceTe.DynamicPDF.PageElements;
    using ceTe.DynamicPDF.Printing;
    using EA.Helpers;
    using EA.Helpers.WPF;
    using EAManagerLib.Configuration.Kiosk;
    using GalaSoft.MvvmLight.Messaging;
    using Kiosk.Messages;
    using Kiosk.Views.BaseModels;
    using Kiosk.Views.Configuration;
    using KioskManager.Events;
    using KioskManagerLib;
    using KioskManagerLib.Configuration;
    using NLog.Fluent;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Configuration;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Net.Http.Headers;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Data;

    public class SettingsViewModel : ViewModel
    {
        public TableController Controller { get; }

        public RelayCommand SettingsSaveCommand { get; }

        public RelayCommand SettingsDiscardCommand { get; }

        public RelayCommand SettingsPrintCommand { get; }

        public RelayCommand SettingsExportCommand { get; }

        public ObservableCollection<SkuViewModel> SkuData { get; } = new ObservableCollection<SkuViewModel>();

        public object SkuDataSyncRoot { get; } = new object();

        public SkuViewModel SkuSelected
        {
            get => _skuSelected;
            set
            {
                if (SetProperty(ref _skuSelected, value, nameof(SkuSelected), nameof(IsLineSettingsSelected), nameof(IsSkuSettingsSelected)))
                    RefreshDisplayedData();
            }
        }
        private SkuViewModel _skuSelected;

        public Dictionary<string, Dictionary<string, List<SettingViewModel>>> AllProductsDeviceSettingsData { get; } = new Dictionary<string, Dictionary<string, List<SettingViewModel>>>(StringComparer.OrdinalIgnoreCase);

        public ObservableCollection<DeviceSettingsViewModel> DeviceSettingsData { get; } = new ObservableCollection<DeviceSettingsViewModel>();

        public object DeviceSettingsDataSyncRoot { get; } = new object();

        public DeviceSettingsViewModel DeviceSelected { get => _deviceSelected; set => SetProperty(ref _deviceSelected, value); }
        private DeviceSettingsViewModel _deviceSelected;

        public DeviceSettingsViewModel KioskSettings { get => _kioskSettings; set => SetProperty(ref _kioskSettings, value); }
        private DeviceSettingsViewModel _kioskSettings;

        public DeviceSettingsViewModel RfidReaderSettings { get => _rfidReaderSettings; set => SetProperty(ref _rfidReaderSettings, value); }
        private DeviceSettingsViewModel _rfidReaderSettings;

        public DeviceSettingsViewModel RfidWriterSettings { get => _rfidWriterSettings; set => SetProperty(ref _rfidWriterSettings, value); }
        private DeviceSettingsViewModel _rfidWriterSettings;
        public DeviceSettingsViewModel RfidValidatorSettings { get => _rfidValidatorSettings; set => SetProperty(ref _rfidValidatorSettings, value); }
        private DeviceSettingsViewModel _rfidValidatorSettings;

        


        public bool RequireChangeReason { get => _requireChangeReason; set => SetProperty(ref _requireChangeReason, value); }
        private bool _requireChangeReason;

        public bool IsReadOnly { get => _readOnly; set => SetProperty(ref _readOnly, value, nameof(IsReadOnly), nameof(IsNotReadOnly)); }

        public bool IsNotReadOnly => !IsReadOnly;
        private bool _readOnly;

        public SettingViewModel CurrentSetting { get => _currentSetting; set => SetProperty(ref _currentSetting, value); }
        public SettingViewModel _currentSetting;

        public bool IsExport { get; set; }
        
        public bool IsLineSettingsSelected => SkuSelected == null;
        public bool IsSkuSettingsSelected => SkuSelected != null;

        public bool IsBusy => IsLoadingSettings || IsSavingSettings;

        public bool IsLoadingSettings { get => _isLoadingSettings; set => SetProperty(ref _isLoadingSettings, value, nameof(IsLoadingSettings), nameof(IsBusy)); }
        private bool _isLoadingSettings;


        public bool IsSavingSettings { get => _isSavingSettings; set => SetProperty(ref _isSavingSettings, value, nameof(IsSavingSettings), nameof(IsBusy)); }
        private bool _isSavingSettings;

        public bool IsSettingsAvailable { get => _isSettingsAvailable; private set => SetProperty(ref _isSettingsAvailable, value); }
        private bool _isSettingsAvailable;

        public bool IsUserManagementAvailable { get => _isUserManagementAvailable; private set => SetProperty(ref _isUserManagementAvailable, value); }
        private bool _isUserManagementAvailable;

        public bool IsDatabaseAvailable { get => _isDatabaseAvailable; private set => SetProperty(ref _isDatabaseAvailable, value); }
        private bool _isDatabaseAvailable;

        public SettingsViewModel()
        {
            Controller = TableController.Instance;
            Controller.SettingsLoaded += Controller_SettingsLoaded;
            Controller.LoginChanged += Controller_LoginChanged;

            BindingOperations.EnableCollectionSynchronization(SkuData, SkuDataSyncRoot);
            BindingOperations.EnableCollectionSynchronization(DeviceSettingsData, DeviceSettingsDataSyncRoot);

            SettingsSaveCommand = new RelayCommand(ExecuteSettingsSaveCommand, CanSettingsSave);
            SettingsDiscardCommand = new RelayCommand(ExecuteSettingsDiscardCommand, CanSettingsDiscard);
            SettingsPrintCommand = new RelayCommand(ExecuteSettingsPrintCommand, CanSettingsPrint);
            SettingsExportCommand = new RelayCommand(ExecuteSettingsExportCommand, CanSettingsPrint);
        }

        private void Controller_LoginChanged(object sender, KioskManager.Events.KioskManagerException e)
        {
            IsReadOnly = !(Controller?.CurrentUser?.HasRight(KioskRight.mayEditSettings) ?? false);
            IsSettingsAvailable = Controller?.CurrentUser?.HasRight(KioskRight.mayViewSettings) ?? false;
            IsUserManagementAvailable = Controller?.CurrentUser?.HasRight(KioskRight.mayManageDatabase) ?? false;
            IsDatabaseAvailable = Controller?.CurrentUser?.HasRight(KioskRight.mayManageDatabase) ?? false;
            ReloadSettingsData();
        }

        private void Controller_SettingsLoaded(object sender, EventArgs e)
        {
            ReloadSettingsData();
        }

        private void ReloadSettingsData()
        {
            try
            {
                IsLoadingSettings = true;
                RelayCommand.RefreshCanExecute();

                string currentProductName = Controller?.CurrentSettings?.ProductName;

                if (string.IsNullOrEmpty(currentProductName))
                {
                    return;
                }

                ParseSettingsResult currentSettingsResult = new ParseSettingsResult(Controller.ParsedSettings);
                bool requireChangeReason = currentSettingsResult.GetDefaultSettings().Kiosk.TrackUpdateReason;

                // Update display properties
                lock (SkuDataSyncRoot)
                {
                    string selectedSkuName = SkuSelected?.Name;
                    SkuSelected = null;

                    SkuData.Clear();

                    foreach (string productName in currentSettingsResult.ProductNames.OrderBy(p => p))
                    {
                        if (!string.Equals(productName, SettingsParser.DefaultProductName, StringComparison.OrdinalIgnoreCase))
                        {
                            SkuData.Add(new SkuViewModel(productName));
                        }
                    }
                    SkuViewModel skuSelected = null;
                    if (!string.IsNullOrWhiteSpace(selectedSkuName))
                    {
                        skuSelected = SkuData.FirstOrDefault(p => string.Equals(p.Name, selectedSkuName, StringComparison.OrdinalIgnoreCase));
                    }
                    SkuSelected = skuSelected;
                }

                lock (DeviceSettingsDataSyncRoot)
                {
                    RequireChangeReason = requireChangeReason;

                    AllProductsDeviceSettingsData.Clear();
                    Dictionary<string, DeviceSettingsViewModel> emptyDeviceList = new Dictionary<string, DeviceSettingsViewModel>(StringComparer.OrdinalIgnoreCase);
                    foreach (string productName in currentSettingsResult.ProductNames)
                    {
                        bool isLineSettings = string.Equals(productName, SettingsParser.DefaultProductName, StringComparison.OrdinalIgnoreCase);

                        Dictionary<string, List<SettingViewModel>> devicesByName = new Dictionary<string, List<SettingViewModel>>(StringComparer.OrdinalIgnoreCase);
                        foreach (ParsedDbSetting dbSetting in currentSettingsResult.ParsedDbSettings)
                        {
                            List<SettingViewModel> deviceSettings = null;
                            if (devicesByName.ContainsKey(dbSetting.DeviceName))
                            {
                                deviceSettings = devicesByName[dbSetting.DeviceName];
                            }
                            else if (Enum.TryParse(dbSetting.DeviceType, true, out DeviceType deviceType))
                            {
                                deviceSettings = new List<SettingViewModel>();
                                devicesByName.Add(dbSetting.DeviceName, deviceSettings);

                                if (!emptyDeviceList.ContainsKey(dbSetting.DeviceName))
                                {
                                    DeviceSettingsViewModel deviceViewModel = new DeviceSettingsViewModel
                                    {
                                        KioskName = dbSetting.KioskName,
                                        Name = dbSetting.DeviceName,
                                        Type = deviceType,
                                        ReadOnlyDevice = IsReadOnly,
                                    };
                                    emptyDeviceList.Add(dbSetting.DeviceName, deviceViewModel);
                                }
                            }
                            else
                            {
                                // Unknown device type
                                continue;
                            }

                            if (string.Equals(productName, dbSetting.ProductName, StringComparison.OrdinalIgnoreCase))
                            {
                                ////Show everything until sku specific settings are needed
                                if ((isLineSettings && (dbSetting.Level == SettingLevel.Kiosk || SettingsParser.IncludeDefaultSkuSettings))
                                    || (!isLineSettings && dbSetting.Level == SettingLevel.Sku))
                                {
                                    SettingViewModel setting = new SettingViewModel()
                                    {
                                        Validate = dbSetting.Validate,
                                        KioskName = dbSetting.KioskName,
                                        DeviceName = dbSetting.DeviceName,
                                        ProductName = productName,
                                        DeviceType = dbSetting.DeviceType,
                                        SortBy = dbSetting.SortBy,
                                        Name = dbSetting.Name,
                                        Type = dbSetting.Type,
                                        DefaultValue = dbSetting.Value,
                                        Value = dbSetting.Value,
                                        SavedOldValue = dbSetting.Value,
                                        //ValueOverridden = false,
                                        ReasonForChange = "",
                                        IsReasonForChangeRequired = requireChangeReason,
                                        //IsDefault = true,
                                        IsValidationRequired = dbSetting.IsValidationRequired,
                                        Level = dbSetting.Level,
                                    };
                                    deviceSettings.Add(setting);
                                }
                            }
                        }

                        AllProductsDeviceSettingsData.Add(productName, devicesByName);
                    }

                    // Sort device list
                    List<DeviceSettingsViewModel> sortedDeviceList = emptyDeviceList.Values.OrderBy(p => p.SortBy).ToList();

                    // Remove devices that no longer exist
                    foreach (DeviceSettingsViewModel deviceSettingsViewModel in DeviceSettingsData.ToList())
                    {
                        if (!sortedDeviceList.Any(p => string.Equals(p.KioskName, deviceSettingsViewModel.KioskName, StringComparison.OrdinalIgnoreCase)
                            && string.Equals(p.Name, deviceSettingsViewModel.Name, StringComparison.OrdinalIgnoreCase)))
                        {
                            DeviceSettingsData.Remove(deviceSettingsViewModel);
                        }
                    }

                    // Add missing devices and sort 
                    for (int i = 0; i < sortedDeviceList.Count; i++)
                    {
                        DeviceSettingsViewModel d = DeviceSettingsData.FirstOrDefault(p => string.Equals(p.KioskName, sortedDeviceList[i].KioskName, StringComparison.OrdinalIgnoreCase)
                                                                                           && string.Equals(p.Name, sortedDeviceList[i].Name, StringComparison.OrdinalIgnoreCase));
                        if (d == null)
                        {
                            d = sortedDeviceList[i];
                            DeviceSettingsData.Add(sortedDeviceList[i]);
                        }
                        else
                        {
                            d.KioskName = sortedDeviceList[i].KioskName;
                            d.Name = sortedDeviceList[i].Name;
                            d.Type = sortedDeviceList[i].Type;
                            d.ReadOnlyDevice = sortedDeviceList[i].ReadOnlyDevice;
                        }

                        int idx = DeviceSettingsData.IndexOf(d);
                        if (idx != i)
                        {
                            DeviceSettingsData.Move(idx, i);
                        }
                    }

                    if (DeviceSelected == null || !DeviceSettingsData.Contains(DeviceSelected))
                    {
                        DeviceSelected = DeviceSettingsData.FirstOrDefault();
                    }

                    DeviceSettingsViewModel kioskSettings = DeviceSettingsData.FirstOrDefault(p => p.Type == DeviceType.kiosk);
                    DeviceSettingsViewModel rfidSettings = DeviceSettingsData.FirstOrDefault(p => p.Type == DeviceType.rfid );
                    DeviceSettingsViewModel rfid_W_Settings = DeviceSettingsData.FirstOrDefault(p => p.Type == DeviceType.writer);
                    DeviceSettingsViewModel  rfid_V_Settings = DeviceSettingsData.FirstOrDefault(p => p.Type == DeviceType.validator);
                    if (kioskSettings != KioskSettings)
                    {
                        KioskSettings = kioskSettings;
                    }
                    if (rfidSettings != RfidReaderSettings)
                    {
                        RfidReaderSettings = rfidSettings;
                    }
                    if (rfid_W_Settings != RfidWriterSettings)
                    {
                        RfidWriterSettings = rfid_W_Settings;
                    }
                    if (rfid_V_Settings != RfidWriterSettings)
                    {
                        RfidValidatorSettings = rfid_V_Settings;
                    }
                    RefreshDisplayedData();
                }

            }
            finally
            {
                IsLoadingSettings = false;
                RelayCommand.RefreshCanExecute();
            }
        }

        public void RefreshDisplayedData()
        {
            lock (DeviceSettingsDataSyncRoot)
            {
                // Get settings for the selected product name
                string selected = SkuSelected?.Name ?? SettingsParser.DefaultProductName;

                Dictionary<string, List<SettingViewModel>> productDeviceSettings = null;
                if (selected != null && AllProductsDeviceSettingsData.ContainsKey(selected))
                {
                    productDeviceSettings = AllProductsDeviceSettingsData[selected];
                }

                // Clear the displayed device settings data and repopulate with settings for the selected product name
                foreach (DeviceSettingsViewModel deviceSettings in DeviceSettingsData)
                {
                    deviceSettings.ReadOnlyDevice = IsReadOnly; // TODO: Globalize?
                    lock (deviceSettings.SettingsConfigSyncRoot)
                    {
                        deviceSettings.SettingsConfig.Clear();
                        if (productDeviceSettings != null && productDeviceSettings.ContainsKey(deviceSettings.Name))
                        {
                            List<SettingViewModel> settings = productDeviceSettings[deviceSettings.Name];
                            foreach (SettingViewModel setting in settings.OrderBy(p => p.SortBy).ToList())
                            {
                                //if (CanDisplaySetting(deviceSettings, setting, setting.ProductName == "DEFAULT"))
                                deviceSettings.SettingsConfig.Add(setting);
                            }
                        }
                    }
                }

                if (DeviceSelected == null || !DeviceSettingsData.Contains(DeviceSelected))
                {
                    DeviceSelected = DeviceSettingsData.FirstOrDefault();
                }
            }
        }

        private bool CanSettingsSave()
        {
            return !IsReadOnly && !IsBusy;
        }

        private async Task ExecuteSettingsSaveCommand()
        {
            try
            {
                IsSavingSettings = true;
                await SaveSettingsChanges().ConfigureAwait(true);
            }
            catch { }
            finally
            {
                IsSavingSettings = false;
            }
        }

        private bool CanSettingsDiscard()
        {
            return !IsBusy;
        }

        private void ExecuteSettingsDiscardCommand()
        {
            ReloadSettingsData();
        }

        private bool CanSettingsPrint()
        {
            return !IsBusy;
        }

        private void ExecuteSettingsPrintCommand()
        {
            IsExport = false;
            PrintSettingsReportAsync();
        }

        private void ExecuteSettingsExportCommand()
        {
            IsExport = true;
            PrintSettingsReportAsync();
        }

        private void PrintSettingsReportAsync()
        {
            try
            {
                //IsBusy = true;

                List<DeviceSettingsViewModel> deviceSettings;
                lock (DeviceSettingsDataSyncRoot)
                {
                    deviceSettings = DeviceSettingsData.ToList();
                }
                var skuName = (SkuSelected?.Name ?? "DEFAULT") == "DEFAULT" ? Controller.ParsedSettings.KioskName : SkuSelected?.Name;

                MergeDocument combinedPDF = new MergeDocument();
                Template docTemplate = new Template();
                PageNumberingLabel pageNumberingLabel = new PageNumberingLabel("%%CP%% of %%TP%%", 380, 660, 100, 12, Font.Helvetica, 12, ceTe.DynamicPDF.TextAlign.Right, RgbColor.Black);
                docTemplate.Elements.Add(pageNumberingLabel);
                combinedPDF.Template = docTemplate;

                foreach (DeviceSettingsViewModel device in deviceSettings)
                {
                    if (device.SettingsConfig.Count > 0)
                    {
                        DocumentLayout layoutReport = new DocumentLayout(System.IO.Path.Combine(Environment.CurrentDirectory, "ReportTemplates", Controller.CurrentSettings.Kiosk.SettingsRepTemplate));

                        NameValueLayoutData layoutData = new NameValueLayoutData();
                        layoutData.Add("LineName", Controller.ParsedSettings.KioskName );
                        layoutData.Add("Username", Controller.CurrentUser.Username);
                        layoutData.Add("BatchNumber", skuName);
                        layoutData.Add("DeviceName", device.Name);
                        var formatCurrentdate = String.Format("{0:yyyy-MM-dd HH:mm:ss}", DateTime.Now);
                        layoutData.Add("DatePrinted", formatCurrentdate);
                        lock (device.SettingsConfigSyncRoot)
                        {
                            layoutData.Add("Settings", device.SettingsConfig.ToList());
                        }
                        Document document = layoutReport.Layout(layoutData);

                        combinedPDF.Append(new PdfDocument(document.Draw()));
                    }
                }

                byte[] combinedpdfBytes = combinedPDF.Draw();

                var filename = "Settings_" + skuName + "_" + String.Format("{0:yyyyMMdd_HHmm}", DateTime.Now) + ".pdf";
                var SavePDFfilename = string.Empty;
                if (IsExport)
                {
                    string drive = System.IO.Path.GetPathRoot(Controller.CurrentSettings.Kiosk.ReportSaveLocation);
                    
                    if (!Directory.Exists(drive))
                    {
                        // no external drive
                        if (!Directory.Exists(Controller.CurrentSettings.Kiosk.DefaultReportPath))
                        {
                            Directory.CreateDirectory(Controller.CurrentSettings.Kiosk.DefaultReportPath);
                        }
                        SavePDFfilename = System.IO.Path.Combine(Controller.CurrentSettings.Kiosk.DefaultReportPath, filename);

                    }
                    else
                    {
                        if (!Directory.Exists(Controller.CurrentSettings.Kiosk.ReportSaveLocation))
                        {
                            Directory.CreateDirectory(Controller.CurrentSettings.Kiosk.ReportSaveLocation);
                        }

                        SavePDFfilename = System.IO.Path.Combine(Controller.CurrentSettings.Kiosk.ReportSaveLocation, filename);
                    }

                    combinedPDF.Draw(SavePDFfilename);
                   
                }
                else
                {
                    // sends to default printer, no dialog box
                    InputPdf pdf = new InputPdf(combinedpdfBytes);
                    PrintJob printJob = new PrintJob(Printer.Default, pdf);
                    try
                    {
                        printJob.Print();
                    }
                    catch (Exception)
                    { }
                    finally
                    {
                        // cleanup objects
                        printJob.Dispose();
                        pdf.Dispose();
                    }

                }

                Notification printnote = new Notification()
                {
                    Message = $"Saved to: {SavePDFfilename}"
                };
                Controller.CurrentMessage = printnote;

                Messenger.Default.Send(new PrintSettingsMessage() { Printed = true, PrintedPath = printnote});
                OnSettingsReportPrintedOccurred();




            }
            catch (Exception ex)
            {
             //   Controller.Log.Error(ex, FkEventCode.Error_printing_settings);
            }
            finally
            {
                //IsBusy = false;
            }
          
        }

        public event EventHandler SettingsPrintedEvent;

        protected virtual void OnSettingsReportPrintedOccurred()
        {
            try { SettingsPrintedEvent?.Invoke(this, EventArgs.Empty); } catch { }
        }

        public async Task SaveSettingsChanges()
        {
            try
            {
                string currentSelectedSku = SkuSelected?.Name ?? SettingsParser.DefaultProductName;

                List<ChangedDbSetting> changedDbSettings = new List<ChangedDbSetting>();
                List<DbSetting> allDbSettings = new List<DbSetting>();

                List<string> productNames = new List<string>();

                lock (DeviceSettingsDataSyncRoot)
                {
                    foreach (DeviceSettingsViewModel device in DeviceSettingsData)
                    {
                        lock (device.SettingsConfigSyncRoot)
                        {
                            foreach (SettingViewModel setting in device.SettingsConfig)
                            {
                                if (!string.Equals(setting.SavedOldValue, setting.Value, StringComparison.Ordinal))
                                {
                                    ChangedDbSetting changedDbSetting = new ChangedDbSetting(setting.Validate)
                                    {
                                        KioskName = setting.KioskName,
                                        DeviceName = setting.DeviceName,
                                        DeviceType = setting.DeviceType,
                                        ProductName = setting.ProductName,
                                        Name = setting.Name,
                                        Type = setting.Type,
                                        Value = setting.Value,
                                        OldValue = setting.SavedOldValue,
                                        ReasonForChange = RequireChangeReason ? setting.ReasonForChange : null,
                                        DefaultValue = setting.DefaultValue,
                                        SortBy = setting.SortBy,
                                        IsReasonForChangeRequired = RequireChangeReason,
                                        IsValidationRequired = setting.IsValidationRequired,
                                        Level = setting.Level,
                                    };
                                    allDbSettings.Add(changedDbSetting);
                                    changedDbSettings.Add(changedDbSetting);
                                }
                                else
                                {
                                    DbSetting dbSetting = new DbSetting()
                                    {
                                        KioskName = setting.KioskName,
                                        DeviceName = setting.DeviceName,
                                        DeviceType = setting.DeviceType,
                                        ProductName = setting.ProductName,
                                        Name = setting.Name,
                                        Type = setting.Type,
                                        Value = setting.Value,
                                        SortBy = setting.SortBy,
                                    };

                                    if (!productNames.Contains(dbSetting.ProductName))
                                        productNames.Add(dbSetting.ProductName);

                                    allDbSettings.Add(dbSetting);
                                }
                            }
                        }
                    }
                }

                ParseSettingsResult parseSettingsResult = SettingsParser.ParseDbSettings(allDbSettings.First().KioskName, productNames, allDbSettings);
                if (parseSettingsResult.ParseSettingsExceptions != null && parseSettingsResult.ParseSettingsExceptions.Count > 0)
                {
                    Exception ex = parseSettingsResult.ParseSettingsExceptions.First();
                    Controller.Log.Error(ex, FkEventCode.Unable_to_save_settings, ex);
                }
                else
                {
                    bool missingReason = false;
                    if (RequireChangeReason)
                    {
                        foreach (ChangedDbSetting changedDbSetting in changedDbSettings)
                        {
                            if (string.IsNullOrWhiteSpace(changedDbSetting.ReasonForChange))
                            {
                                missingReason = true;
                                Controller.Log.Error(FkEventCode.Unable_to_save_settings_missingReason, changedDbSetting.Name);
                                break;
                            }
                        }
                    }
                    if (!missingReason)
                    {
                        await Controller.ChangeSettings(changedDbSettings).ConfigureAwait(true);
                    }
                }
            }
            catch (Exception ex)
            {
                Controller.Log.Error(ex, FkEventCode.Unable_to_save_settings, ex.Message);
            }
        }

        public async Task ChangeSettingAsync()
        {
            if (CurrentSetting != null)
            {
                var Countmessage = new ConfigurationView(CurrentSetting);

                Countmessage.Top = 10;
                Countmessage.Left = 300;
                Countmessage.Topmost =true;

                bool? dialogResult = Countmessage.ShowDialog();

                switch (Countmessage.Result)
                {
                    case MessageBoxResult.Yes:
                        List<ChangedDbSetting> changedDbSettings = new List<ChangedDbSetting>();
                        List<DbSetting> allDbSettings = new List<DbSetting>();
                        List<string> productNames = new List<string>();
                        var ctx = (ConfigurationViewModel)Countmessage.DataContext;
                        foreach (DeviceSettingsViewModel device in DeviceSettingsData)
                        {
                            if( device.Name == ctx.CurrentModel.DeviceName)
                            {
                             device.SettingsConfig.FirstOrDefault(
                                 sc => sc.Name == ctx.CurrentModel.Name).Value = ctx.CurrentModel.Value;
                             device.SettingsConfig.FirstOrDefault(sc => sc.Name == ctx.CurrentModel.Name).ReasonForChange = ctx.ReasonForChange;
                                await SaveSettingsChanges().ConfigureAwait(true);
                            }
                        }
                        break;
                    case MessageBoxResult.Cancel:

                        ExecuteSettingsDiscardCommand();
                        break;
                };
            }

        }

        public override void RegisterMessages()
        {

        }

        public override void UnRegisterMessages()
        {

        }

    }


    public class DeviceSettingsViewModel : ObservableObject
    {
        public DeviceSettingsViewModel()
        {
            Application.Current?.Dispatcher?.BeginInvoke(new System.Action(() =>
            {
                BindingOperations.EnableCollectionSynchronization(SettingsConfig, SettingsConfigSyncRoot);
            }));
        }

        public string KioskName { get => _lineName; set => SetProperty(ref _lineName, value); }
        private string _lineName;

        public string Name { get => _name; set => SetProperty(ref _name, value); }
        private string _name;

        public DeviceType Type { get => _type; set => SetProperty(ref _type, value, nameof(Type), nameof(SortBy)); }
        private DeviceType _type;

        public bool ReadOnlyDevice { get => _readOnlyDevice; set => SetProperty(ref _readOnlyDevice, value); }
        private bool _readOnlyDevice;

        public int SortBy { get => (int)Type; }

        public ObservableCollection<SettingViewModel> SettingsConfig { get;} = new ObservableCollection<SettingViewModel>();
        public object SettingsConfigSyncRoot { get; } = new object();
    }

    public class SettingViewModel : ObservableObject, IDataErrorInfo
    {
        public string KioskName { get => _lineName; set => SetProperty(ref _lineName, value); }
        private string _lineName;

        public string DeviceName { get => _deviceName; set => SetProperty(ref _deviceName, value); }
        private string _deviceName;

        public string DeviceType { get => _deviceType; set => SetProperty(ref _deviceType, value); }
        private string _deviceType;

        public string ProductName { get => _productName; set => SetProperty(ref _productName, value); }
        private string _productName;

        public string Name { get => _name; set => SetProperty(ref _name, value); }
        private string _name;

        public string Type { get => _type; set => SetProperty(ref _type, value); }
        private string _type;

        public string Value
        {
            get => _value;
            set => SetProperty(ref _value, ValidateInternal(value), nameof(Value), nameof(IsValueOverridden), nameof(ErrorMessage), nameof(IsValid), nameof(IsNotValid));
        }
        private string _value;

        public SettingLevel Level { get => _level; set => SetProperty(ref _level, value); }
        private SettingLevel _level;

        public bool IsValidationRequired
        {
            get => _isValidationRequired;
            set
            {
                if (SetProperty(ref _isValidationRequired, value))
                    Value = ValidateInternal(Value);
            }
        }
        private bool _isValidationRequired;

        public Func<string, string> Validate
        {
            get => _validate;
            set
            {
                if (SetProperty(ref _validate, value))
                    Value = ValidateInternal(Value);
            }
        }
        private Func<string, string> _validate;

        public bool IsValid => ErrorMessage == null;
        public bool IsNotValid => !IsValid;

        public ParseSettingException ValidationError { get => _validationError; private set => SetProperty(ref _validationError, value, nameof(ValidationError), nameof(Error), nameof(ErrorMessage), nameof(IsValid), nameof(IsNotValid)); }
        private ParseSettingException _validationError;

        public int SortBy { get => _sortBy; set => SetProperty(ref _sortBy, value); }
        private int _sortBy;

        public string DefaultValue { get => _defaultValue; set => SetProperty(ref _defaultValue, value); }
        private string _defaultValue;

        public string SavedOldValue { get => _savedOldValue; set => SetProperty(ref _savedOldValue, value, nameof(SavedOldValue), nameof(IsValueOverridden), nameof(ErrorMessage), nameof(IsValid), nameof(IsNotValid)); }
        private string _savedOldValue;

        public bool IsValueOverridden => !string.Equals(SavedOldValue, Value, StringComparison.Ordinal);

        public string ReasonForChange { get => _reasonForChange; set => SetProperty(ref _reasonForChange, value, nameof(ReasonForChange), nameof(ErrorMessage), nameof(IsValid), nameof(IsNotValid)); }
        private string _reasonForChange;

        public bool IsReasonForChangeRequired { get => _isReasonForChangeRequired; set => SetProperty(ref _isReasonForChangeRequired, value, nameof(IsReasonForChangeRequired), nameof(ErrorMessage), nameof(IsValid), nameof(IsNotValid)); }
        private bool _isReasonForChangeRequired;

        public string ErrorMessage
        {
            get
            {
                string errorMessage = ValidationError?.ErrorMessage;
                if (string.IsNullOrWhiteSpace(errorMessage))
                {
                    errorMessage = null;
                    if (IsReasonForChangeRequired
                        && string.IsNullOrWhiteSpace(ReasonForChange)
                        && !string.Equals(Value, SavedOldValue, StringComparison.Ordinal))
                    {
                        errorMessage = "Reason for change is required"; // TODO: localize string
                    }
                }
                return errorMessage;
            }
        }

        public string Error => ValidationError?.ErrorMessage;

        public string this[string columnName]
        {
            get
            {
                if (columnName == "Value")
                {
                    return ValidationError?.ErrorMessage;
                }
                else if (columnName == "ChangeReason")
                {
                    if (IsReasonForChangeRequired
                        && string.IsNullOrWhiteSpace(ReasonForChange)
                        && !string.Equals(Value, SavedOldValue, StringComparison.Ordinal))
                    {
                        return "Reason for change is required"; // TODO: localize string
                    }
                }
                return null;
            }
        }

        private string ValidateInternal(string value)
        {
            string validatedValue = value;
            Func<string, string> validateFunc = Validate;
            if (IsValidationRequired && validateFunc != null)
            {
                try
                {
                    validatedValue = validateFunc(value);
                    ValidationError = null;
                }
                catch (ParseSettingException pse)
                {
                    ValidationError = pse;
                }
                catch (Exception ex)
                {
                    // Should never happen since Validate should only ever throw ParseSettingException
                    ValidationError = new ParseSettingException("Error parsing setting", ex);
                }
            }
            else
            {
                ValidationError = null;

            }
            return validatedValue;
        }
    }

    public class SkuViewModel : ObservableObject
    {
        public string Name { get => _name; set => SetProperty(ref _name, value); }
        private string _name;

        public SkuViewModel(string name)
        {
            Name = name;
        }
    }
}
