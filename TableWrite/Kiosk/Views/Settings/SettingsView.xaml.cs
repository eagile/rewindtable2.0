﻿using GalaSoft.MvvmLight.Messaging;
using Kiosk.Messages;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using Telerik.Windows.Data;


namespace Kiosk.Views
{
    /// <summary>
    /// Interaction logic for SettingsView.xaml
    /// </summary>
    public partial class SettingsView : UserControl 
    {
        public SettingsView()
        {
            InitializeComponent();
            Messenger.Default.Register<SettingTabFocusMessage>(this, ExecuteSettingTabFocusMessage);
        }

        private void Tabcontrol_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var tc = sender as TabControl; //The sender is a type of TabControl...

            if (tc != null)
            {
                var tab = (TabItem)tc.SelectedItem;

                if( tab.Header.ToString() == "User Management")
                {
                    Messenger.Default.Send(new UserMgtFocusMessage() { UserMgtHasfocus = true });
                }
                if (tab.Header.ToString() == "Database")
                {
                    Messenger.Default.Send(new DatabaseFocusMessage() { DatabaseHasfocus= true });
                }

            }
        }

        private void ExecuteSettingTabFocusMessage(SettingTabFocusMessage msg)
        {
            if (msg.Kioskhasfocus)
                this.Dispatcher.Invoke(() =>
                {
                    tabcontrol.SelectedIndex = 0;
                });
        }

        private void DataGridCell_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (sender is DataGridCell cell && !cell.IsEditing && !cell.IsReadOnly)
            {

                DataGrid dataGrid = FindVisualParent<DataGrid>(cell);

              

                if (!cell.IsFocused)
                {
                    cell.Focus();
                }
                dataGrid.BeginEdit(e);
                cell.Dispatcher.Invoke(DispatcherPriority.Background, new Action(delegate { }));
            }
        }

        private void DataGridCell_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (sender is DataGridCell cell && !cell.IsEditing && !cell.IsReadOnly)
            {
                TextBox textBox = FindVisualChild<TextBox>(cell);
                if (textBox != null && !textBox.IsFocused)
                {
                    textBox.Focus();
                }
            }
        }

        static T FindVisualParent<T>(DependencyObject element) where T : DependencyObject
        {
            DependencyObject parent = element;
            do
            {
                parent = VisualTreeHelper.GetParent(parent);
                if (parent != null && parent is T correctlyTyped)
                    return correctlyTyped;
            }
            while (parent != null);
            return null;
        }

        private T FindVisualChild<T>(DependencyObject element) where T : DependencyObject
        {
            int childCount = VisualTreeHelper.GetChildrenCount(element);
            for (int i = 0; i < childCount; i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(element, i);
                if (child != null && child is T correctlyTyped)
                    return correctlyTyped;
                T childOfChild = FindVisualChild<T>(child);
                if (childOfChild != null)
                    return childOfChild;
            }
            return null;
        }

        private void Scrollup_Click(object sender, RoutedEventArgs e)
        {
            DataGrid found = FindChild<DataGrid>(Application.Current.MainWindow, "");
            if (found == null) return;
            //found.Items.MoveCurrentToLast();
            IQueryable test = found.Items.SourceCollection.AsQueryable();
            var tt = test.First();
            found.UpdateLayout();
            found.ScrollIntoView(tt);

        }
        private void ScrollDown_Click(object sender, RoutedEventArgs e)
        {
            DataGrid found = FindChild<DataGrid>(Application.Current.MainWindow, "");
            if (found == null) return;
            //found.Items.MoveCurrentToLast();

            IQueryable test = found.Items.SourceCollection.AsQueryable();
            var tt = test.ElementAt(test.Count() - 1);
            found.UpdateLayout();

            found.ScrollIntoView(tt);


        }

        // <summary>
        /// Finds a Child of a given item in the visual tree. 
        /// </summary>
        /// <param name="parent">A direct parent of the queried item.</param>
        /// <typeparam name="T">The type of the queried item.</typeparam>
        /// <param name="childName">x:Name or Name of child. </param>
        /// <returns>The first parent item that matches the submitted type parameter. 
        /// If not matching item can be found, 
        /// a null parent is being returned.</returns>
        public static T FindChild<T>(DependencyObject parent, string childName)
           where T : DependencyObject
        {
            // Confirm parent and childName are valid. 
            if (parent == null) return null;

            T foundChild = null;

            int childrenCount = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < childrenCount; i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);
                // If the child is not of the request child type child
                T childType = child as T;
                if (childType == null)
                {
                    // recursively drill down the tree
                    foundChild = FindChild<T>(child, childName);

                    // If the child is found, break so we do not overwrite the found child. 
                    if (foundChild != null) break;
                }
                else if (!string.IsNullOrEmpty(childName))
                {
                    var frameworkElement = child as FrameworkElement;
                    // If the child's name is set for search
                    if (frameworkElement != null && frameworkElement.Name == childName)
                    {
                        // if the child's name is of the request name
                        foundChild = (T)child;
                        break;
                    }
                }
                else
                {
                    // child element found.
                    foundChild = (T)child;
                    break;
                }
            }

            return foundChild;
        }


        private void DataGrid_BeginningEdit(object sender, DataGridBeginningEditEventArgs e)
        {
            DataGrid grid = (DataGrid)sender;

            var ctx = (SettingsViewModel)this.DataContext;

            ctx.CurrentSetting = (SettingViewModel)grid.CurrentItem;

            _ = ctx.ChangeSettingAsync();

        }
    }


    /// <summary>
    /// Class BitToBoolConverter.
    /// Implements the <see cref="System.Windows.Data.IValueConverter" />
    /// </summary>
    /// <seealso cref="System.Windows.Data.IValueConverter" />
    public class BitToBoolConverter : IValueConverter
    {
        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>A converted value. If the method returns <see langword="null" />, the valid null value is used.</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value == null ? false : value.ToString() == "1" ? true : false;
        }

        /// <summary>
        /// Converts the back.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="parameter">The parameter.</param>
        /// <param name="culture">The culture.</param>
        /// <returns>System.Object.</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            object temp = value is bool ? (bool)value ? "1" : "0" : "0";
            return temp;
        }
    }
}
