﻿using EA.Helpers.WPF;
using GalaSoft.MvvmLight.Messaging;
using Kiosk.Messages;
using Kiosk.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Telerik.Windows.Controls;

namespace Kiosk
{
    /// <summary>
    /// Interaction logic for MainKiosk.xaml
    /// </summary>
    public partial class MainKiosk : Window
    {

        public MainKiosk()
        {
            Loaded += MainKiosk_Loaded;
            Closing += MainKiosk_Closing;
            Closed += MainKiosk_Closed;
            InitializeComponent();
            radDocking.ActivePaneChanged += RadDocking_ActivePaneChanged;
          
            Application.Current.MainWindow = this;
        }

        private async void MainKiosk_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DataContext is IWindowModelAsync windowModel)
                {
                    await windowModel.OnLoadedAsync().ConfigureAwait(false);
                }
            }
            catch { }
            
        }

        private async void MainKiosk_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                if (DataContext is IWindowModelAsync windowModel)
                {
                    await windowModel.OnClosingAsync().ConfigureAwait(false);
                }
            }
            catch { }
        }

        private async void MainKiosk_Closed(object sender, EventArgs e)
        {
            try
            {
                if (DataContext is IWindowModelAsync windowModel)
                {
                    await windowModel.OnClosedAsync().ConfigureAwait(false);
                }
            }
            catch { }
        }

        private void RadDocking_ActivePaneChanged(object sender, Telerik.Windows.Controls.Docking.ActivePangeChangedEventArgs e)
        {
            RadPane olpane = e.NewPane;

            if (olpane.Name == "RadPaneLogging")
            {
                Messenger.Default.Send(new LogGotFocusMessage() { Loghasfocus = true });
            }

        }

    }
}
