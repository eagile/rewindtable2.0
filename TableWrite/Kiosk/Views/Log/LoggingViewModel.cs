﻿using Kiosk.Messages;
using Kiosk.Views.BaseModels;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using KioskManagerLib;
using EA.Helpers.WPF;
using KioskManagerLib.Database.Models;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Controls;
using Telerik.Windows.Data;
using System.Linq;
using ceTe.DynamicPDF;
using ceTe.DynamicPDF.Merger;
using ceTe.DynamicPDF.PageElements;
using ceTe.DynamicPDF.LayoutEngine;
using System.IO;
using Spire.Pdf;
using System.Windows.Xps.Packaging;
using System.Windows.Documents;
using ceTe.DynamicPDF.Printing;
using Telerik.Windows.Controls;
using Spire.Pdf.Exporting.XPS.Schema.Mc;
using KioskManagerLib.Models;

namespace Kiosk.Views
{
    public class LoggingViewModel : ViewModel
    {
        private string _pdfFilename;
        public string PDFfilename
        {
            get { return _pdfFilename; }
            set { SetProperty(ref _pdfFilename, value); }
        }
      //  public ObservableCollection<MasterModel> CurrentLogList { get; set; } = new ObservableCollection<MasterModel>();

       
        private FixedDocumentSequence _flowDoc2;
        public FixedDocumentSequence FlowDoc2
        {
            get { return _flowDoc2; }
            set { SetProperty(ref _flowDoc2, value); }
        }

        private string _xpsFilename;
        public string XPSfilename
        {
            get { return _xpsFilename; }
            set { SetProperty(ref _xpsFilename, value); }
        }
        private byte[] combinedpdfBytes { get; set; }
        public TableController Controller { get; }

        private RelayCommand _loggingFilterLogCommand;
        public RelayCommand LoggingFilterLogCommand => _loggingFilterLogCommand ?? (_loggingFilterLogCommand = new RelayCommand(ExecuteLoggingFilterLogCommand, CanLoggingFilter));

        private RelayCommand _loggingPrintLogCommand;
        public RelayCommand LoggingPrintLogCommand => _loggingPrintLogCommand ?? (_loggingPrintLogCommand = new RelayCommand(ExecuteLoggingPrintLogCommand, CanLoggingPrint));

      
        public object LogsSyncRoot { get; } = new object();

        public CompositeFilterDescriptorCollection Filters { get; set; } = new CompositeFilterDescriptorCollection();

     
        public bool IsLogExportAvailable { get => _isLogExportAvailable; private set => SetProperty(ref _isLogExportAvailable, value); }
        private bool _isLogExportAvailable;

        public bool IsPrintingLogs { get => _isPrintingLogs; private set => SetProperty(ref _isPrintingLogs, value); }
        private bool _isPrintingLogs;

        public bool IsBusy => IsPrintingLogs;

        public string Currentindex { get => _currentindex; set => SetProperty(ref _currentindex, value); }
        private string _currentindex ="0";
        public string TotalIndex { get => _totalIndex; set => SetProperty(ref _totalIndex, value); }
        private string _totalIndex = "0";
        public bool IsScrollFirst { get => _isScrollFirst;  set => SetProperty(ref _isScrollFirst, value); }
        private bool _isScrollFirst = false;
        public bool IsScrollLast { get => _isScrollLast; set => SetProperty(ref _isScrollLast, value); }
        private bool _isScrollLast = true;

        public bool IsScrollNext { get => _isScrollNext; set => SetProperty(ref _isScrollNext, value); }
        private bool _isScrollNext = true;
        public bool IsScrollPrevious { get => _isScrollPrevious; set => SetProperty(ref _isScrollPrevious, value); }
        private bool _isScrollPrevious = false;




        public LoggingViewModel()
        {
            Controller = TableController.Instance;
            Controller.LoginChanged += Controller_LoginChanged;
            BindingOperations.EnableCollectionSynchronization(Controller.CurrentLogList, Controller.CurrentLogListSyncRoot);

            RegisterMessages();
            
            

        }

        
        public async Task ExecuteRefreshCommand()
        {
            await ExecuteGetLog().ConfigureAwait(false);
        }

        private void Controller_LoginChanged(object sender, KioskManager.Events.KioskManagerException e)
        {
            IsLogExportAvailable = Controller?.CurrentUser?.HasRight(KioskRight.mayExportLogs) ?? false;
            RelayCommand.RefreshCanExecute();
        }

        private bool CanLoggingFilter()
        {
            return true;
        }
        private void ExecuteLoggingFilterLogCommand()
        {

        }

        private bool CanLoggingPrint()
        {
            return IsLogExportAvailable && !IsBusy;
        }
        private void ExecuteLoggingSaveToFileCommand()
        {
            //try
            //{
            //    IsPrintingLogs = true;

            //    var OrderFilter = string.Empty;

            //    Dimensions newdoc = new Dimensions(PageSize.Letter, ceTe.DynamicPDF.PageOrientation.Landscape);
            //    MergeDocument combinedPDF = new MergeDocument();
            //    Template docTemplate = new Template();

            //    PageNumberingLabel pageNumberingLabel = new ceTe.DynamicPDF.PageElements.PageNumberingLabel("%%CP%% of %%TP%%", 380, 660, 100, 12, Font.Helvetica, 12, ceTe.DynamicPDF.TextAlign.Right, RgbColor.Black);
            //    docTemplate.Elements.Add(pageNumberingLabel);
            //    combinedPDF.Template = docTemplate;

            //    DocumentLayout layoutReport = new DocumentLayout(System.IO.Path.Combine(Environment.CurrentDirectory, "ReportTemplates", Controller.CurrentSettings.Kiosk.LogsRepTemplate));

            //    NameValueLayoutData layoutData = new NameValueLayoutData();
            //    layoutData.Add("Username", Controller.CurrentUser.Username);
            //    layoutData.Add("OrderFilter", OrderFilter);
            //    var formatCurrentdate = String.Format("{0:yyyy-MM-dd HH:mm:ss}", DateTime.Now);
            //    layoutData.Add("DatePrinted", formatCurrentdate);
            //    layoutData.Add("Logs", CurrentLogList.ToList());
            //    Document document = layoutReport.Layout(layoutData);
            //    combinedPDF.Append(new ceTe.DynamicPDF.Merger.PdfDocument(document.Draw()));

            //    byte[] combinedpdfBytes = combinedPDF.Draw();

               
            //    var filename = "Logs_" + OrderFilter + "_" + String.Format("{0:yyyyMMdd_HHmmss}", DateTime.Now) + ".pdf";

            //    string drive = System.IO.Path.GetPathRoot(Controller.CurrentSettings.Kiosk.ReportSaveLocation);
            //    var SavePDFfilename = string.Empty;
            //    if (!Directory.Exists(drive))
            //    {
            //        // no external drive
            //        // file is already in the InternalFileSaveLocation

            //        if (!Directory.Exists(Controller.CurrentSettings.Kiosk.DefaultReportPath))
            //        {
            //            Directory.CreateDirectory(Controller.CurrentSettings.Kiosk.DefaultReportPath);
            //        }
            //        SavePDFfilename = System.IO.Path.Combine(Controller.CurrentSettings.Kiosk.DefaultReportPath, filename);
                    
            //    }
            //    else
            //    {
            //        if (!Directory.Exists(Controller.CurrentSettings.Kiosk.ReportSaveLocation))
            //        {
            //            Directory.CreateDirectory(Controller.CurrentSettings.Kiosk.ReportSaveLocation);
            //        }

            //        SavePDFfilename = System.IO.Path.Combine(Controller.CurrentSettings.Kiosk.ReportSaveLocation, filename);
            //    }
            //    combinedPDF.Draw(SavePDFfilename);


            //}
            //catch (Exception ex)
            //{
            //    // TODO: Error handling
            //    string msg = ex.Message;
            //}

            //finally
            //{
            //    IsPrintingLogs = false;
            // //   OnLogReportPrintedOccurred(CurrentLogList.ToList());

            //}
        }
        private void ExecuteLoggingPrintLogCommand()
        {
            //try
            //{
            //    IsPrintingLogs = true;

            //    var OrderFilter = string.Empty;

            //    Dimensions newdoc = new Dimensions(PageSize.Letter, ceTe.DynamicPDF.PageOrientation.Landscape);
            //    MergeDocument combinedPDF = new MergeDocument();
            //    Template docTemplate = new Template();

            //    PageNumberingLabel pageNumberingLabel = new ceTe.DynamicPDF.PageElements.PageNumberingLabel("%%CP%% of %%TP%%", 380, 660, 100, 12, Font.Helvetica, 12, ceTe.DynamicPDF.TextAlign.Right, RgbColor.Black);
            //    docTemplate.Elements.Add(pageNumberingLabel);
            //    combinedPDF.Template = docTemplate;

            //    DocumentLayout layoutReport = new DocumentLayout(System.IO.Path.Combine(Environment.CurrentDirectory, "ReportTemplates", Controller.CurrentSettings.Kiosk.LogsRepTemplate));

            //    NameValueLayoutData layoutData = new NameValueLayoutData();
            //    layoutData.Add("Username", Controller.CurrentUser.Username);
            //    layoutData.Add("OrderFilter", OrderFilter);
            //    var formatCurrentdate = String.Format("{0:yyyy-MM-dd HH:mm:ss}", DateTime.Now);
            //    layoutData.Add("DatePrinted", formatCurrentdate);
            //    layoutData.Add("Logs", CurrentLogList.ToList());
            //    Document document = layoutReport.Layout(layoutData);
            //    combinedPDF.Append(new ceTe.DynamicPDF.Merger.PdfDocument(document.Draw()));

            //    byte[] combinedpdfBytes = combinedPDF.Draw();

            //    if (!Directory.Exists(Controller.CurrentSettings.Kiosk.DefaultReportPath))
            //    {
            //        Directory.CreateDirectory(Controller.CurrentSettings.Kiosk.DefaultReportPath);
            //    }

            //    var filename = "Logs_" + OrderFilter + "_" + String.Format("{0:yyyyMMdd_HHmmss}", DateTime.Now) + ".pdf";

            //    combinedPDF.Draw(System.IO.Path.Combine(Controller.CurrentSettings.Kiosk.DefaultReportPath, filename));

            //    // sends to default printer, no dialog box
            //    InputPdf pdf = new InputPdf(combinedpdfBytes);
            //    PrintJob printJob = new PrintJob(Printer.Default, pdf);
            //    try
            //    {
            //        if (!printJob.Printer.DriverName.Contains("Microsoft Print To PDF"))
            //        {
            //            printJob.Print();
            //        }

            //        File.Delete(System.IO.Path.Combine(Controller.CurrentSettings.Kiosk.DefaultReportPath, filename));
             
            //    }
            //    catch (Exception)
            //    { }
            //    finally
            //    {
            //        // cleanup objects
            //        printJob.Dispose();
            //        pdf.Dispose();
            //    }


            //}
            //catch (Exception ex)
            //{
            //    // TODO: Error handling
            //    string msg = ex.Message;
            //}

            //finally
            //{
            //    IsPrintingLogs = false;
            ////    OnLogReportPrintedOccurred(CurrentLogList.ToList());
                
            //}
        }
        public event EventHandler<List<Logs>> LogPrintedEvent;
        protected virtual void OnLogReportPrintedOccurred(List<Logs> log)
        {
            try { LogPrintedEvent?.Invoke(this, log); } catch { }
        }

        public override void RegisterMessages()
        {
            Messenger.Default.Register<LogGotFocusMessage>(this, ExecuteLogGotFocusMessage);
        }

        public override void UnRegisterMessages()
        {
            
            Messenger.Default.Unregister<LogGotFocusMessage>(this, ExecuteLogGotFocusMessage);
        }

        /// <summary>
        /// Print log list or save to file. Call from code behind.
        /// </summary>
        /// <param name="msgprint"></param>
        public void ExecutePrintMessage(bool msgprint)
        {
            if(msgprint)
            {
                ExecuteLoggingPrintLogCommand();
            }
            else
            {
                ExecuteLoggingSaveToFileCommand();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="msg"></param>
        private void ExecuteLogGotFocusMessage(LogGotFocusMessage msg)
        {
            if (Controller != null)
            {
                _ = ExecuteGetLog();
            }

        }
        private async Task ExecuteGetLog()
        {
            //try
            //{
            //   // var results = await Controller.GetLogsAsync().ConfigureAwait(true);
            //    lock (CurrentLogListSyncRoot)
            //    {
            // //       CurrentLogList =new ObservableCollection<MasterModel>( Controller.MasterFileList);
            //        //if(results != null)
            //        //{
            //        //CurrentLogList.Clear();
            //        //foreach (var item in CurrentLogList)
            //        //{
            //        //    CurrentLogList.Add(item);
            //        //}

            //        if (CurrentLogList.Count() > 0)
            //        {
            //            var pagecount = (CurrentLogList.Count() / 10);

            //            TotalIndex = pagecount.ToString();

            //            Currentindex = "1";
            //        }
            //        //}
            //    }


            //}
            //catch { }
        }



    }
}
