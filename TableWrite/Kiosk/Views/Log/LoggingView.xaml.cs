﻿namespace Kiosk.Views
{
    using GalaSoft.MvvmLight.Messaging;
    using Kiosk.Messages;
    using System.Windows;
    using System.Windows.Controls;
    using Telerik.Windows.Controls;
    using Telerik.Windows.Controls.Filtering.Editors;
    /// <summary>
    /// Interaction logic for LoggingView.xaml
    /// </summary>
    public partial class LoggingView : UserControl
    {
        private LoggingViewModel ctx;
        public LoggingView()
        {

            InitializeComponent();

          //  this.radGridView.Columns["LogTime"].FilteringControl = new Helpers.FilteringControlTelerik(this.radGridView.Columns["LogTime"]);

           // this.radGridView.Columns["User"].FilteringControl = new Helpers.FilteringControlTelerik(this.radGridView.Columns["User"]);

         //   this.radGridView.Columns["Message"].FilteringControl = new Helpers.FilteringControlTelerik(this.radGridView.Columns["Message"]);
            rdpLogs.PageIndexChanged += RdpLogs_PageIndexChanged;
            ctx = (LoggingViewModel)this.DataContext;
         
        }



        /// <summary>
        /// Used to set the page index and total pages. 
        /// enable and disable button for next, previous, first ,last.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RdpLogs_PageIndexChanged(object sender, PageIndexChangedEventArgs e)
        {
            RadDataPager pager = (RadDataPager)sender;
            
            var index = e.NewPageIndex + 1;

            ctx.Currentindex = index.ToString();

            ctx.TotalIndex = pager.PageCount.ToString();

            if (pager.PageIndex == 0)
            {
                ctx.IsScrollFirst = false;
                ctx.IsScrollNext = true;
                ctx.IsScrollPrevious = false;
                ctx.IsScrollLast = true;
            }
            else if (pager.PageIndex + 1 == pager.PageCount)
            {
                ctx.IsScrollFirst = true;
                ctx.IsScrollNext = false;
                ctx.IsScrollPrevious = true;
                ctx.IsScrollLast = false;
            }
            else
            {
                ctx.IsScrollFirst = true;
                ctx.IsScrollNext = true;
                ctx.IsScrollPrevious = true;
                ctx.IsScrollLast = true;
            }
        }

        private void GridView_FieldFilterEditorCreated(object sender, Telerik.Windows.Controls.GridView.EditorCreatedEventArgs e)
        {
            // change from the DatePicker to the DateTimePicker for LogTime
            if (e.Column.UniqueName == "LogTime")
            {
                RadDateTimePicker picker = e.Editor as RadDateTimePicker;
                if (picker != null)
                {
                    picker.InputMode = Telerik.Windows.Controls.InputMode.DateTimePicker;
                }
            }

            //Hide the case button in filters, look for a  StringFilterEditor in RadGridView 
            var stringFilterEditor = e.Editor as StringFilterEditor;
            if (stringFilterEditor != null)
            {
                stringFilterEditor.MatchCaseVisibility = Visibility.Hidden;
            }
        }

        private void GridView_FilterOperatorsLoading(object sender, Telerik.Windows.Controls.GridView.FilterOperatorsLoadingEventArgs e)
        {
            if (e.Column.UniqueName == "User" || e.Column.UniqueName == "Order" || e.Column.UniqueName == "Message")
            {
                e.DefaultOperator1 = Telerik.Windows.Data.FilterOperator.Contains;
                e.DefaultOperator2 = Telerik.Windows.Data.FilterOperator.Contains;
            }
        }

        private void GridView_DistinctValuesLoading(object sender, Telerik.Windows.Controls.GridView.GridViewDistinctValuesLoadingEventArgs e)
        {
            // this line gives you a set number of distinct values in the filter, 15 in this case
            //e.ItemsSource = ( (Telerik.Windows.Controls.RadGridView) sender ).GetDistinctValues( e.Column, true, 15 );

            // this line would give you distinct values all in lower case
            //e.ItemsSource = ( (Telerik.Windows.Controls.RadGridView) sender ).GetDistinctValues( e.Column, false ).OfType<string>().Select( x => x.ToLower() ).Distinct();
        }

        private void GridView_Filtered(object sender, Telerik.Windows.Controls.GridView.GridViewFilteredEventArgs e)
        {
            // Set the filters for Case insensitve
            foreach (var item in e.ColumnFilterDescriptor.DistinctFilter.FilterDescriptors)
            {
                item.IsCaseSensitive = false;
            }
        }
        private void Refresh_Click(object sender, RoutedEventArgs e)
        {
            _= ctx.ExecuteRefreshCommand();
        }
        private void PrintLogs_Click(object sender, RoutedEventArgs e)
        {
            ctx.ExecutePrintMessage(true);
        }
        private void SaveLogs_Click(object sender, RoutedEventArgs e)
        {
            ctx.ExecutePrintMessage(false);
        }

        private void ClearFilters_Click(object sender, RoutedEventArgs e)
        {
            this.radGridView.FilterDescriptors.SuspendNotifications();
            foreach (Telerik.Windows.Controls.GridViewColumn column in this.radGridView.Columns)
            {
                column.ClearFilters();
            }
            this.radGridView.FilterDescriptors.ResumeNotifications();
        }

        /// <summary>
        ///  Move to previous set of "10" records in log list.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ScrollPrevious_Click(object sender, RoutedEventArgs e)
        {
            rdpLogs.MoveToPreviousPage();
        }

        /// <summary>
        ///  ove to first item in the the log list.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ScrollFirst_Click(object sender, RoutedEventArgs e)
        {
            rdpLogs.MoveToFirstPage();
        }

        /// <summary>
        /// Move to next iten in log list.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ScrollNext_Click(object sender, RoutedEventArgs e)
        {
            rdpLogs.MoveToNextPage();
        }

        /// <summary>
        /// Move to last item in log list.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ScrollLast_Click(object sender, RoutedEventArgs e)
        {
            rdpLogs.MoveToLastPage();
        }
    }
}
