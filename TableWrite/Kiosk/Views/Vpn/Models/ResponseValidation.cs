﻿namespace eAgileKiosk.Views.Vpn.Models
{
    public class ResponseValidation
    {
        public bool IsValid { get; set; }
        public string Message = Messages.Success;
    }
}
