﻿using Kiosk.Messages;
using Kiosk.Models;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Telerik.Windows.Controls;
using System.Collections.ObjectModel;
using Telerik.Windows.Controls.Docking;
using Kiosk.Views.BaseModels;
using KioskManagerLib;
using System.Linq;
using EA.Helpers.WPF;
using System.Windows.Controls;
using KioskManager.Events;
using Kiosk.Cultures;
using System.Globalization;
using System.IO;
using System.Reflection;

namespace Kiosk.Views
{
    public class MainWindowViewModel : ViewModel, IWindowModelAsync
    {
        public TableController Controller { get; }

        public string ClockTime => _clockDateTime.ToString(Properties.Strings.Clock_Time_Format, Thread.CurrentThread.CurrentCulture.DateTimeFormat);
        public string ClockDate => _clockDateTime.ToString(Properties.Strings.Clock_Date_Format, Thread.CurrentThread.CurrentCulture.DateTimeFormat);
        private DateTime _clockDateTime = DateTime.Now;
        private System.Timers.Timer _clockTimer = new System.Timers.Timer();

        public string MessageBarMessage { get => _messageBarMessage; set => SetProperty(ref _messageBarMessage, value); }
        private string _messageBarMessage;
        private System.Timers.Timer _messageTimer = new System.Timers.Timer();

        public System.Windows.Media.Brush MessageBarBackground { get => _messageBarBackground; private set => SetProperty(ref _messageBarBackground, value); }
        private System.Windows.Media.Brush _messageBarBackground = InfoBrush;

        public static System.Windows.Media.Brush GreyBrush { get; } = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.LightGray);
        public static System.Windows.Media.Brush InfoBrush { get; } = (System.Windows.Media.SolidColorBrush)new System.Windows.Media.BrushConverter().ConvertFromString("#374F8D");
        public static System.Windows.Media.Brush SuccessBrush { get; } = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Green);
        public static System.Windows.Media.Brush WarnBrush { get; } = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.DarkGoldenrod);
        public static System.Windows.Media.Brush ErrorBrush { get; } = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Red);

        private string _kioskId = $"KioskId:";
        public string KioskId { get => _kioskId; set => SetProperty(ref _kioskId, value); }

        public string LoginName { get => _loginName ?? "Log In"; set => SetProperty(ref _loginName, value); }
        private string _loginName = null;

        public bool IsLoginViewActive { get => _isLoginViewActive; set => SetProperty(ref _isLoginViewActive, value); }
        public bool _isLoginViewActive = false;

        public bool IsHomeViewActive { get => _isHomeViewActive; set => SetProperty(ref _isHomeViewActive, value); }
        public bool _isHomeViewActive = false;

        public bool IsSettingsViewActive { get => _isSettingsViewActive; set => SetProperty(ref _isSettingsViewActive, value); }
        public bool _isSettingsViewActive = false;

        public bool IsLogsViewActive { get => _isLogsViewActive; set => SetProperty(ref _isLogsViewActive, value); }
        public bool _isLogsViewActive = false;

        public bool IsHelpViewActive { get => _isHelpViewActive; set => SetProperty(ref _isHelpViewActive, value); }
        public bool _isHelpViewActive = false;

        public string SourceUri
        {
            get
            {
                return Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Resources/eagile_logo_120.png");
            }
        }

        public bool IsHomeAvailable { get => _isHomeAvailable; private set => SetProperty(ref _isHomeAvailable, value); }
        private bool _isHomeAvailable;

        public bool IsSettingsAvailable { get => _isSettingsAvailable; private set => SetProperty(ref _isSettingsAvailable, value); }
        private bool _isSettingsAvailable;

        public bool IsLogsAvailable { get => _isLogsAvailable; private set => SetProperty(ref _isLogsAvailable, value); }
        private bool _isLogsAvailable;

        public bool IsUserManagementAvailable { get => _isUserManagementAvailable; private set => SetProperty(ref _isUserManagementAvailable, value); }
        private bool _isUserManagementAvailable;

        public bool IsDatabaseAvailable { get => _isDatabaseAvailable; private set => SetProperty(ref _isDatabaseAvailable, value); }
        private bool _isDatabaseAvailable;

        public bool IsHelpAvailable { get => _isHelpAvailable; private set => SetProperty(ref _isHelpAvailable, value); }
        private bool _isHelpAvailable;

        public bool IsTopMost { get => _isTopMost; set => SetProperty(ref _isTopMost, value); }
        /// <summary>
        /// The is form available
        /// </summary>
        private bool _isTopMost = true;

        public MainWindowViewModel()
        {

            Controller = TableController.Instance;
            Controller.LoginChanged += Controller_LoginChanged;
            Controller.LanguageChanged += Controller_LanguageChanged;
            Controller.NotificationOccurred += Controller_NotificationOccurred;
            IsTopMost = Controller.TopMost;
            _clockTimer.Interval = TimeSpan.FromSeconds(1).TotalMilliseconds;
            _clockTimer.AutoReset = true;
            _clockTimer.Elapsed += ClockTimer_Elapsed;
            _clockTimer.Start();

            _messageTimer.Interval = TimeSpan.FromSeconds(5).TotalMilliseconds;
            _messageTimer.AutoReset = false;
            _messageTimer.Elapsed += MessageTimer_Elapsed;

            RegisterMessages();
        }

        private void Controller_NotificationOccurred(object sender, EventArgs e)
        {
            _messageTimer.Stop();
            RefreshNotificationBar(false);
            _messageTimer.Start();
        }

        private void MessageTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            RefreshNotificationBar(true);
        }

        private void RefreshNotificationBar(bool reset)
        {
            Notification display = null;
            lock (Controller.AlarmsSyncRoot)
            {
                if (Controller.IsLoaded)
                {
                    // Don't display alarms until controller is loaded
                    display = Controller.Alarms.OrderBy(p => (int)p.Level).ThenBy(p => p.Timestamp).FirstOrDefault();
                }
                Notification current = Controller.CurrentMessage;
                if (reset && display != null)
                {
                    Controller.CurrentMessage = null;
                }
                else if (current != null)
                {
                    display = current;
                }
                if (display != null)
                {
                    MessageBarMessage = $"[{display.Timestamp:yyyy-MM-dd HH:mm:ss}]    ({display.EventCodeNumber})    {display.Message}";
                    switch (display.Level)
                    {
                        case FkErrorLevel.Error:
                            MessageBarBackground = ErrorBrush;
                            break;
                        case FkErrorLevel.Warn:
                            MessageBarBackground = WarnBrush;
                            break;
                        default:
                            MessageBarBackground = InfoBrush;
                            break;
                    }
                }
                else
                {
                    MessageBarMessage = string.Empty;
                    MessageBarBackground = GreyBrush;
                }
            }
        }

        private void Controller_LanguageChanged(object sender, CultureInfo e)
        {
            CultureResources.ChangeCulture(e);
            RefreshProperties();
        }

        private void ClockTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                _clockDateTime = DateTime.Now;
                OnPropertyChanged(nameof(ClockTime), nameof(ClockDate));
            }
            catch { }
        }


        private void ExecutePrintMessage(PrintSettingsMessage msg)
        {
            MessageBarMessage = msg.PrintedPath.Message.ToString();
            MessageBarBackground = InfoBrush;
        }

        public override void RegisterMessages()
        {
            Messenger.Default.Register<PrintSettingsMessage>(this, ExecutePrintMessage);
            
        }

        public override void UnRegisterMessages()
        {
            Messenger.Default.Register<PrintSettingsMessage>(this, ExecutePrintMessage);
        }

        private void Controller_LoginChanged(object sender, KioskManagerException error)
        {
            LoginName = Controller?.CurrentUser?.Username;
            IsHomeAvailable = Controller?.CurrentUser?.HasRight(KioskRight.mayReadRFID) ?? false;
            IsUserManagementAvailable = Controller?.CurrentUser?.HasRight(KioskRight.mayManageDatabase) ?? false;
            IsDatabaseAvailable = Controller?.CurrentUser?.HasRight(KioskRight.mayManageDatabase) ?? false;
            IsSettingsAvailable = IsUserManagementAvailable || IsDatabaseAvailable || (Controller?.CurrentUser?.HasRight(KioskRight.mayViewSettings) ?? false);
            IsLogsAvailable = Controller?.CurrentUser?.HasRight(KioskRight.mayViewLogs) ?? false;
            IsHelpAvailable = Controller?.CurrentUser?.HasRight(KioskRight.mayReadRFID) ?? false;
            if (Controller.CurrentUser.IsLoggedIn)
            {
                if (IsHomeAvailable)
                    IsHomeViewActive = true;
                else if (IsLogsAvailable)
                    IsLogsViewActive = true;
                else if (IsSettingsAvailable)
                    IsSettingsViewActive = true;
                else if (IsHelpAvailable)
                    IsHelpViewActive = true;
            }
            else
            {
                IsLoginViewActive = true; 
            }
            
            Messenger.Default.Send(new SettingTabFocusMessage() { Kioskhasfocus = true });
        }

        public async Task OnLoadedAsync()
        {
            await Controller.LoadControllerAsync().ConfigureAwait(true);

            KioskId = $"KioskId: {Controller.DAL.KioskName}";
            Messenger.Default.Send(new LoginGotFocusMessage() { Loginhasfocus = true });
        }

        public async Task OnClosingAsync()
        {
            await Controller.UnloadControllerAsync().ConfigureAwait(true);
        }

        public Task OnClosedAsync()
        {
            Controller.Dispose();
            return Task.CompletedTask;
        }

    }

}
