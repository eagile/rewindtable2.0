﻿using Kiosk.Messages;
using Kiosk.Views.BaseModels;
using GalaSoft.MvvmLight.Messaging;
using System;
using KioskManagerLib;
using EA.Helpers.WPF;
using KioskManagerLib.Login;
using System.Threading.Tasks;
using System.Security;
using System.Windows.Input;
using System.Diagnostics;
using KioskManager.Events;
using System.Windows;

namespace Kiosk.Views
{
    public class LoginViewModel : ViewModel
    {
        public TableController Controller { get; }

        public ICommand LoginCommand => _loginCommand ?? (_loginCommand = new RelayCommand(LoginAsync, CanLogin));
        private ICommand _loginCommand;

        public ICommand CancelCommand => _cancelCommand ?? (_cancelCommand = new RelayCommand(Cancel, CanCancel));
        private ICommand _cancelCommand;

        public ICommand LogoutCommand => _logoutCommand ?? (_logoutCommand = new RelayCommand(LogoutAsync, CanLogout));
        private ICommand _logoutCommand;

        public ICommand CloseCommand => _closeCommand ?? (_closeCommand = new RelayCommand(Close, CanClose));
        private ICommand _closeCommand;

        public ICommand ShutdownCommand => _shutdownCommand ?? (_shutdownCommand = new RelayCommand(Shutdown, CanShutdown));
        private ICommand _shutdownCommand;

        public SecureString SecurePassword { private get; set; } = new SecureString();

        public string VersionNumber => string.Format(Properties.Strings.Login_Version, System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString());

        public string UserName { get => _userName; set => SetProperty(ref _userName, value); }
        private string _userName = string.Empty;

        public bool HasFocus { get => _hasFocus; set => SetProperty(ref _hasFocus, value); }
        public bool _hasFocus = true;

        public bool IsLoginAvailable { get => _isLoginAvailable; private set => SetProperty(ref _isLoginAvailable, value); }
        private bool _isLoginAvailable;

        public bool IsSettingsAvailable { get => _isSettingsAvailable; private set => SetProperty(ref _isSettingsAvailable, value); }
        private bool _isSettingsAvailable;

        public bool IsLoggingIn { get => _isLoggingIn; private set => SetProperty(ref _isLoggingIn, value, nameof(IsLoggingIn), nameof(IsBusy)); }
        private bool _isLoggingIn;

        public bool IsCancelling { get => _isCancelling; private set => SetProperty(ref _isCancelling, value, nameof(IsCancelling), nameof(IsBusy)); }
        private bool _isCancelling;

        public bool IsLoggingOut { get => _isLoggingOut; private set => SetProperty(ref _isLoggingOut, value, nameof(IsLoggingOut), nameof(IsBusy)); }
        private bool _isLoggingOut;

        public bool IsBusy => IsLoggingIn || IsCancelling || IsLoggingOut;

        public LoginViewModel()
        {
            Controller = TableController.Instance;
            Controller.ControllerStatusChanged += Controller_ControllerStatusChanged;
            Controller.LoginChanged += Controller_LoginChanged;

            RegisterMessages();
        }

        private void Controller_LoginChanged(object sender, KioskManager.Events.KioskManagerException e)
        {
            IsSettingsAvailable = Controller?.CurrentUser?.HasRight(KioskRight.mayEditSettings) ?? false;
            RelayCommand.RefreshCanExecute();
        }

        private void Controller_ControllerStatusChanged(object sender, EventArgs e)
        {
            IsLoginAvailable = Controller.IsLoaded;
            RelayCommand.RefreshCanExecute();
        }

        private bool CanLogin()
        {
            return IsLoginAvailable && !IsBusy && Controller.CurrentUser.IsNotLoggedIn;
        }

        private async Task LoginAsync()
        {
            string username = string.Empty;
            string passwordHash = string.Empty;
            try
            {
                IsLoggingIn = true;

                username = UserName;
                UserName = string.Empty;
                try
                {
                    if (SecurePassword != null && SecurePassword.Length > 0)
                    {
                        passwordHash = PasswordHash.GetPasswordHash(SecurePassword);
                    }
                }
                finally
                {
                    SecurePassword?.Clear();
                }

                await Controller.LoginAsync(username, passwordHash).ConfigureAwait(true);
            }
            catch (Exception ex)
            {
                // TODO: Error handling
                string msg = ex.Message;
            }
            finally
            {
                IsLoggingIn = false;
            }
        }

        private bool CanCancel()
        {
            return IsLoginAvailable && !IsBusy && Controller.CurrentUser.IsNotLoggedIn;
        }

        private void Cancel()
        {
            try
            {
                IsCancelling = true;

                UserName = string.Empty;
                SecurePassword?.Clear();
            }
            catch (Exception ex)
            {
                // TODO: Error handling
                string msg = ex.Message;
            }
            finally
            {
                IsCancelling = false;
            }
        }

        private bool CanClose()
        {
            return true;
        }

        private void Close()
        {
            Controller.Log.Info(FkEventCode.Close_Application);
            try
            {
                Application.Current.MainWindow.Close();
            }
            catch { }
        }

        private bool CanShutdown()
        {
            return true;
        }

        private void Shutdown()
        {
            Controller.Log.Info(FkEventCode.Shutdown_Computer);
            try
            {
                Process.Start(new ProcessStartInfo("shutdown", "/s /t 0")
                {
                    CreateNoWindow = true,
                    UseShellExecute = false
                });
            }
            catch { }
        }

        private bool CanLogout()
        {
            return IsLoginAvailable && !IsBusy && Controller.CurrentUser.IsLoggedIn;
        }

        private async Task LogoutAsync()
        {
            try
            {
                IsLoggingOut = true;

                await Controller.LogoutAsync().ConfigureAwait(true);
            }
            catch (Exception ex)
            {
                // TODO: Error handling
                string msg = ex.Message;
            }
            finally
            {
                IsLoggingOut = false;
            }
        }

        public override void RegisterMessages()
        {
           // Messenger.Default.Register<LoginGotFocusMessage>(this, ExecuteLoginGotFocusMessage);
        }

        public override void UnRegisterMessages()
        {
           // Messenger.Default.Register<LoginGotFocusMessage>(this, ExecuteLoginGotFocusMessage);
        }

        //private void ExecuteLoginGotFocusMessage(LoginGotFocusMessage msg)
        //{
        //    if (msg.Loginhasfocus)
        //    {

        //        HasFocus = true;
        //    }
        //    else
        //    {
        //        HasFocus = false;
        //    }

        //}

    }
}
