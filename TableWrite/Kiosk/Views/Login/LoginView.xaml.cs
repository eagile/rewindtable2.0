﻿using Kiosk.Messages;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Kiosk.Views
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class LoginView : UserControl
    {
        public LoginView()
        {
            InitializeComponent();
            Messenger.Default.Register<LoginGotFocusMessage>(this, ExecuteLoginGotFocusMessage);
           
        }

        private void ExecuteLoginGotFocusMessage(LoginGotFocusMessage msg)
        {
            if (msg.Loginhasfocus)

                UsernameBox.Focus();

        }


        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            if (DataContext is LoginViewModel loginViewModel)
            {
                loginViewModel.SecurePassword = LoginPasswordBox.SecurePassword;
                LoginPasswordBox.Clear();
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            if (DataContext is LoginViewModel loginViewModel)
            {
                LoginPasswordBox.Clear();
            }
        }

        
    }
}
