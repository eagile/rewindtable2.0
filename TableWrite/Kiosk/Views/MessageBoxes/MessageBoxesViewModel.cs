﻿    // ***********************************************************************
// Assembly         : LineManager
// Author           : eAgile
// Created          : 04-30-2020
//
// Last Modified By : eAgile
// Last Modified On : 05-13-2020
// ***********************************************************************
// <copyright file="MessageBoxesViewModel.cs" company="eAgile Inc">
//     eAgile Inc.
// </copyright>
// <summary></summary>
// ***********************************************************************
using EA.Helpers.WPF;
using Kiosk.Views.BaseModels;
using NLog;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace Kiosk.Views
{
    /// <summary>
    /// Class MessageBoxesViewModel.
    /// Implements the <see cref="Kiosk.Views.ViewModel" />
    /// Implements the <see cref="System.IDisposable" />
    /// </summary>
    /// <seealso cref="Kiosk.Views.ViewModel" />
    /// <seealso cref="System.IDisposable" />
    public class MessageBoxesViewModel : ViewModel, IDisposable
    {
        /// <summary>
        /// The shut down timer
        /// </summary>
        private System.Timers.Timer shutDownTimer = new System.Timers.Timer();

        /// <summary>
        /// The database logger
        /// </summary>
        private static Logger dbLogger = LogManager.GetLogger( "db" );

        /// <summary>
        /// Gets or sets the window title.
        /// </summary>
        /// <value>The window title.</value>
        public string WindowTitle { get => windowTitle; set => SetProperty( ref windowTitle, value ); }
        /// <summary>
        /// The window title
        /// </summary>
        private string windowTitle;

        /// <summary>
        /// Gets or sets a value indicating whether [yes no].
        /// </summary>
        /// <value><c>true</c> if [yes no]; otherwise, <c>false</c>.</value>
        public bool YesNo { get => yesNo; set => SetProperty( ref yesNo, value ); }
        /// <summary>
        /// The yes no
        /// </summary>
        private bool yesNo;

        /// <summary>
        /// Gets or sets a value indicating whether [yes no cancel].
        /// </summary>
        /// <value><c>true</c> if [yes no cancel]; otherwise, <c>false</c>.</value>
        public bool YesNoCancel { get => yesNoCancel; set => SetProperty( ref yesNoCancel, value ); }
        /// <summary>
        /// The yes no cancel
        /// </summary>
        private bool yesNoCancel;

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="MessageBoxesViewModel"/> is ok.
        /// </summary>
        /// <value><c>true</c> if ok; otherwise, <c>false</c>.</value>
        public bool OK { get => ok; set => SetProperty( ref ok, value ); }
        /// <summary>
        /// The ok
        /// </summary>
        private bool ok;

        /// <summary>
        /// Gets or sets a value indicating whether [ok cancel].
        /// </summary>
        /// <value><c>true</c> if [ok cancel]; otherwise, <c>false</c>.</value>
        public bool OKCancel { get => okCancel; set => SetProperty( ref okCancel, value ); }
        /// <summary>
        /// The ok cancel
        /// </summary>
        private bool okCancel;

        /// <summary>
        /// Gets or sets the message text.
        /// </summary>
        /// <value>The message text.</value>
        public string MessageText { get => messageText; set => SetProperty( ref messageText, value ); }
        /// <summary>
        /// The message text
        /// </summary>
        private string messageText;

        /// <summary>
        /// Gets or sets the box image.
        /// </summary>
        /// <value>The box image.</value>
        public BitmapImage BoxImage { get => boxImage; set => SetProperty( ref boxImage, value ); }
        /// <summary>
        /// The box image
        /// </summary>
        private BitmapImage boxImage;

        /// <summary>
        /// Gets or sets the result.
        /// </summary>
        /// <value>The result.</value>
        public MessageBoxResult Result { get => result; set => SetProperty( ref result, value ); }
        /// <summary>
        /// The result
        /// </summary>
        private MessageBoxResult result;

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageBoxesViewModel"/> class.
        /// </summary>
        public MessageBoxesViewModel()
        {
         
            //shutDownTimer.Interval = 25000;
            //shutDownTimer.Elapsed += ShutdownTimer_Elapsed;
            //shutDownTimer.Enabled = true;
        }


        /// <summary>
        /// Handles the Elapsed event of the ShutdownTimer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Timers.ElapsedEventArgs"/> instance containing the event data.</param>
        private void ShutdownTimer_Elapsed( object sender, System.Timers.ElapsedEventArgs e )
        {

        }
        public override void RegisterMessages()
        {
         
        }

        public override void UnRegisterMessages()
        {
         
        }
        /// <summary>
        /// Sets the message box button.
        /// </summary>
        /// <param name="messageBoxButton">The message box button.</param>
        public void SetMessageBoxButton ( MessageBoxButton messageBoxButton )
        {
            switch ( messageBoxButton )
            {
                case MessageBoxButton.OK:
                    OK = true;
                    OKCancel = YesNoCancel = YesNo = false;
                    break;
                case MessageBoxButton.OKCancel:
                    OKCancel = true;
                    OK = YesNoCancel = YesNo = false;
                    break;
                case MessageBoxButton.YesNoCancel:
                    YesNoCancel = true;
                    OK = OKCancel = YesNo = false;
                    break;
                case MessageBoxButton.YesNo:
                    YesNo = true;
                    break;
                default:
                    OKCancel = true;
                    OK = YesNoCancel = YesNo = false;
                    break;
            }
        }

        /// <summary>
        /// Sets the message box icon.
        /// </summary>
        /// <param name="messageBoxImage">The message box image.</param>
        public void SetMessageBoxIcon ( MessageBoxImage messageBoxImage )
        {
            switch ( messageBoxImage )
            {
                case MessageBoxImage.Information:
                    BoxImage = new BitmapImage(new Uri(@"\Resources\information.png", UriKind.RelativeOrAbsolute));
                    break;
                case MessageBoxImage.Error:
                    BoxImage = new BitmapImage( new Uri( @"\Resources\information.png", UriKind.RelativeOrAbsolute ) );
                    break;
                case MessageBoxImage.Exclamation:
                    BoxImage = new BitmapImage( new Uri( @"\Resources\information.png", UriKind.RelativeOrAbsolute ) );
                    break;
                case MessageBoxImage.None:
                    BoxImage = new BitmapImage( new Uri( @"\Resources\information.png", UriKind.RelativeOrAbsolute ) );
                    break;
                case MessageBoxImage.Question:
                    BoxImage = new BitmapImage( new Uri( @"\Resources\information.png", UriKind.RelativeOrAbsolute ) );
                    break;
            }
        }

        /// <summary>
        /// Shows the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <exception cref="ArgumentNullException">message</exception>
        public void Show( string message )
        {
            if ( string.IsNullOrEmpty( message ) )
                throw new ArgumentNullException( nameof( message ) );

            MessageText = message;

            SetMessageBoxButton( MessageBoxButton.OK );

            SetMessageBoxIcon(MessageBoxImage.Information);
        }

        /// <summary>
        /// Shows the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="messageBoxButton">The message box button.</param>
        /// <exception cref="ArgumentNullException">message</exception>
        public void Show( string message, MessageBoxButton messageBoxButton )
        {
            if ( string.IsNullOrEmpty( message ) )
                throw new ArgumentNullException( nameof( message ) );

            MessageText = message;

            SetMessageBoxButton( messageBoxButton );

            SetMessageBoxIcon( MessageBoxImage.Information );
        }

        /// <summary>
        /// Shows the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="title">The title.</param>
        /// <param name="messageBoxButton">The message box button.</param>
        /// <exception cref="ArgumentNullException">message</exception>
        /// <exception cref="ArgumentNullException">title</exception>
        public void Show( string message, string title, MessageBoxButton messageBoxButton )
        {
            if ( string.IsNullOrEmpty( message ) )
                throw new ArgumentNullException( nameof( message ) );

            if ( string.IsNullOrEmpty( message ) )
                throw new ArgumentNullException( nameof( title ) );

            MessageText = message;

            WindowTitle = title;

            SetMessageBoxButton( messageBoxButton );

            SetMessageBoxIcon( MessageBoxImage.Information );
        }

        /// <summary>
        /// Shows the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="title">The title.</param>
        /// <param name="messageBoxButton">The message box button.</param>
        /// <param name="messageBoxImage">The message box image.</param>
        /// <exception cref="ArgumentNullException">message</exception>
        /// <exception cref="ArgumentNullException">title</exception>
        public void Show( string message, string title, MessageBoxButton messageBoxButton, MessageBoxImage messageBoxImage )
        {
            if ( string.IsNullOrEmpty( message ) )
                throw new ArgumentNullException( nameof( message ) );

            if ( string.IsNullOrEmpty( message ) )
                throw new ArgumentNullException( nameof( title ) );

            MessageText = message;

            WindowTitle = title;

            SetMessageBoxButton( messageBoxButton );

            SetMessageBoxIcon( messageBoxImage );
        }

        /// <summary>
        /// Shows the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="messageBoxButton">The message box button.</param>
        /// <param name="messageBoxImage">The message box image.</param>
        /// <exception cref="ArgumentNullException">message</exception>
        public void Show( string message, MessageBoxButton messageBoxButton, MessageBoxImage messageBoxImage )
        {
            if ( string.IsNullOrEmpty( message ) )
                throw new ArgumentNullException( nameof( message ) );

            MessageText = message;

            SetMessageBoxButton( messageBoxButton );

            SetMessageBoxIcon( messageBoxImage );
        }

        #region Dispose

        /// <summary>
        /// The disposed
        /// </summary>
        private bool disposed = false;
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose( true );
        }
        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose( bool disposing )
        {
            if ( disposed )
                return;

            if ( disposing )
            {
                // Free any managed objects here.
                shutDownTimer.Dispose();
            }

            // Free any unmanaged objects here
            disposed = true;
        }
        /// <summary>
        /// Finalizes an instance of the <see cref="MessageBoxesViewModel"/> class.
        /// </summary>
        ~MessageBoxesViewModel()
        {
            Dispose( false );
        }

        #endregion Dispose

    }
}
