﻿// ***********************************************************************
// Assembly         : LineManager
// Author           : eAgile
// Created          : 04-30-2020
//
// Last Modified By : eAgile
// Last Modified On : 05-13-2020
// ***********************************************************************
// <copyright file="MessageBoxesView.xaml.cs" company="eAgile Inc">
//     eAgile Inc.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Kiosk.Views
{
    /// <summary>
    /// Interaction logic for MessageBoxesView.xaml
    /// </summary>
    public partial class MessageBoxesView : Window
    {
        /// <summary>
        /// The vm
        /// </summary>
        private MessageBoxesViewModel vm;

        /// <summary>
        /// The response received
        /// </summary>
        private bool responseReceived = false;

        /// <summary>
        /// The response
        /// </summary>
        private bool response = false;

        /// <summary>
        /// Gets or sets the result.
        /// </summary>
        /// <value>The result.</value>
        public MessageBoxResult Result { get; set; }
        /// <summary>
        /// The result
        /// </summary>
        private MessageBoxResult result;


        /// <summary>
        /// Initializes a new instance of the <see cref="MessageBoxesView"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="messageBoxButton">The message box button.</param>
        /// <param name="messageBoxImage">The message box image.</param>
        public MessageBoxesView(string message, MessageBoxButton messageBoxButton, MessageBoxImage messageBoxImage)
        {
            InitializeComponent();
            
            this.DataContext = new MessageBoxesViewModel();
            vm = (MessageBoxesViewModel) this.DataContext;
            Message = message;
            vm.SetMessageBoxButton(messageBoxButton);
            vm.SetMessageBoxIcon(messageBoxImage);
            Loaded += MessageBoxesView_Loaded;
            Yesbutton.Click += Yesbutton_Click;
            Nobutton.Click += Nobutton_Click;
            Cancelbutton.Click += Cancelbutton_Click;
            OKbutton.Click += OKbutton_Click;
        }

        /// <summary>
        /// Handles the Click event of the OKbutton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void OKbutton_Click(object sender, RoutedEventArgs e)
        {
            Result = MessageBoxResult.OK;
        }

        /// <summary>
        /// Handles the Click event of the Cancelbutton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Cancelbutton_Click(object sender, RoutedEventArgs e)
        {
            Result = MessageBoxResult.Cancel;
        }

        /// <summary>
        /// Handles the Click event of the Nobutton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Nobutton_Click(object sender, RoutedEventArgs e)
        {
            Result = MessageBoxResult.No;
        }

        /// <summary>
        /// Handles the Click event of the Yesbutton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Yesbutton_Click(object sender, RoutedEventArgs e)
        {
            Result = MessageBoxResult.Yes;
        }


        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>The message.</value>
        public string Message
        {
            get { return lblMessage.Text; }
            set
            {
                lblMessage.Text = value;
            }
        }

        /// <summary>
        /// Handles the Loaded event of the MessageBoxesView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void MessageBoxesView_Loaded(object sender, RoutedEventArgs e)
        {

            
            
        }

        /// <summary>
        /// Closes the window.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void CloseWindow( object sender, RoutedEventArgs e )
        {
            DialogResult =true;
            this.Close();
        }

        /// <summary>
        /// Shows the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void Show( string message )
        {
            vm.Show( message );
        }

        /// <summary>
        /// Shows the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="messageBoxButton">The message box button.</param>
        public void Show( string message, MessageBoxButton messageBoxButton )
        {
            vm.Show( message, messageBoxButton );
        }

        /// <summary>
        /// Shows the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="title">The title.</param>
        /// <param name="messageBoxButton">The message box button.</param>
        public void Show( string message, string title, MessageBoxButton messageBoxButton )
        {
            vm.Show( message, title, messageBoxButton );
        }

        /// <summary>
        /// Shows the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="title">The title.</param>
        /// <param name="messageBoxButton">The message box button.</param>
        /// <param name="messageBoxImage">The message box image.</param>
        public void Show( string message, string title, MessageBoxButton messageBoxButton, MessageBoxImage messageBoxImage )
        {
            vm.Show( message, title, messageBoxButton, messageBoxImage );
        }

        /// <summary>
        /// Shows the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="messageBoxButton">The message box button.</param>
        /// <param name="messageBoxImage">The message box image.</param>
        public void Show( string message, MessageBoxButton messageBoxButton, MessageBoxImage messageBoxImage )
        {
            vm.Show( message, messageBoxButton, messageBoxImage );
        }

        /// <summary>
        /// Shows the dialog.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public bool ShowDialog( string message )
        {
            vm.Show( message );
            return WaitForResponse();
        }

        /// <summary>
        /// Shows the dialog.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="messageBoxButton">The message box button.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public bool ShowDialog( string message, MessageBoxButton messageBoxButton )
        {
            vm.Show( message, messageBoxButton );
            return WaitForResponse();
        }

        /// <summary>
        /// Shows the dialog.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="title">The title.</param>
        /// <param name="messageBoxButton">The message box button.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public bool ShowDialog( string message, string title, MessageBoxButton messageBoxButton )
        {
            vm.Show( message, title, messageBoxButton );
            return WaitForResponse();
        }

        /// <summary>
        /// Shows the dialog.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="title">The title.</param>
        /// <param name="messageBoxButton">The message box button.</param>
        /// <param name="messageBoxImage">The message box image.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public bool ShowDialog( string message, string title, MessageBoxButton messageBoxButton, MessageBoxImage messageBoxImage )
        {
            vm.Show( message, title, messageBoxButton, messageBoxImage );
            return WaitForResponse();
        }

        /// <summary>
        /// Shows the dialog.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="messageBoxButton">The message box button.</param>
        /// <param name="messageBoxImage">The message box image.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public bool ShowDialog( string message, MessageBoxButton messageBoxButton, MessageBoxImage messageBoxImage )
        {
            vm.Show( message, messageBoxButton, messageBoxImage );
            return WaitForResponse();
        }

        /// <summary>
        /// Waits for response.
        /// </summary>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public bool WaitForResponse()
        {
            while ( !responseReceived )
            {
                // HACK: Stop the thread if the application is about to close
                if ( this.Dispatcher.HasShutdownStarted ||
                    this.Dispatcher.HasShutdownFinished )
                {
                    break;
                }

                // HACK: Simulate "DoEvents"
                this.Dispatcher.Invoke(DispatcherPriority.Background, new ThreadStart( delegate { } ) );
                Thread.Sleep( 20 );
            }

            return response;
        }
    }
}
