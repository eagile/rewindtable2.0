﻿using Kiosk.Views.BaseModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Kiosk.Views.Configuration
{
    public class ConfigurationViewModel:ViewModel
    {
        
        public SettingViewModel CurrentModel { get => _currentModel; set => SetProperty(ref _currentModel, value); }
        private SettingViewModel _currentModel;
        public SettingViewModel OrigModel { get; }
        
        public string ReasonForChange { get => _reasonForChange; set => SetProperty(ref _reasonForChange, value); }
        private string _reasonForChange;
        public bool CanSave { get => _canSave; set => SetProperty(ref _canSave, value); }
        private bool _canSave =false;



        public ConfigurationViewModel(SettingViewModel currentModel)
        {
            CurrentModel = currentModel;
            OrigModel = currentModel;
        }

       

        public override void RegisterMessages()
        {
          
        }

        public override void UnRegisterMessages()
        {
          
        }
    }
}
