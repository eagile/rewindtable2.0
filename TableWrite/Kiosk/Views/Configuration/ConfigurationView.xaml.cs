﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Kiosk.Views.Configuration
{
    /// <summary>
    /// Interaction logic for ConfigurationView.xaml
    /// </summary>
    public partial class ConfigurationView : Window
    {
        public MessageBoxResult Result { get; set; }
        public bool AllowSave { get; set; }
        public ConfigurationView(SettingViewModel currentModel)
        {
            InitializeComponent();
            DataContext = new ConfigurationViewModel(currentModel);
            Savebutton.Click += Savebutton_Click;
            Cancelbutton.Click += Cancelbutton_Click;
        }

        private void Cancelbutton_Click(object sender, RoutedEventArgs e)
        {
            Result = MessageBoxResult.Cancel;
        }

        private void Savebutton_Click(object sender, RoutedEventArgs e)
        {
            Result = MessageBoxResult.Yes;
        }
        private void CloseWindow(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            this.Close();

        }


        private void DefaultValuebox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (DataContext != null)
            {
                var ctx = (ConfigurationViewModel)this.DataContext;
               // if (!string.IsNullOrWhiteSpace(DefaultValuebox.Text))
               // {
                    if (ctx.OrigModel.DefaultValue != DefaultValuebox.Text)
                    {
                        AllowSave = true;
                        ctx.CanSave = true;
                    }
                    else
                    {
                        AllowSave = false;
                        ctx.CanSave = false;
                    ChangeReasonBox.Text = string.Empty;
                    }
               // }
            }


        }

        private void ChangeReasonBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (DataContext != null)
            {
                var ctx = (ConfigurationViewModel)this.DataContext;
                if (AllowSave)
                {
                    ctx.CanSave = true;
                }
                else
                {
                    ctx.CanSave = false;
                }
            }
        }
    }
}
