﻿using EA.Helpers.WPF;
using GalaSoft.MvvmLight.Messaging;
using Kiosk.Messages;
using Kiosk.Views.BaseModels;
using KioskManager.Events;
using KioskManagerLib;
using KioskManagerLib.Configuration;
using KioskManagerLib.Database.Models;
using KioskManagerLib.Login;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Kiosk.Views
{
    public class UserManagementViewModel : ViewModel
    {
        public RelayCommand SaveUserCommand { get; }
        public RelayCommand AddUserCommand { get; }
        public ObservableCollection<DisplayUser> UserLoginList { get; } = new ObservableCollection<DisplayUser>();
        public object UserLoginListSyncRoot { get; } = new object();
        public DisplayUser SelectedUser 
        { get => _selectedUser;
            set
            {
                SetProperty(ref _selectedUser, value);
                if(SelectedUser !=null)

                    Addrights();
            }
            
        }
        private DisplayUser _selectedUser;
        public ObservableCollection<GridRights> _allRightsList =new ObservableCollection<GridRights>(); 
        public ObservableCollection<GridRights> AllRightsList 
        {
            get => _allRightsList;
            set
            {
                SetProperty(ref _allRightsList, value);
            }
        }

        //public GridRights _allRightsCurrentRight;
        //public GridRights AllRightsCurrentRight 
        //{
        //    get => _allRightsCurrentRight;
        //    set
        //    {

        //        if(value !=null)
        //        {

        //            if(value.IsSelected == true)
        //            {

        //                UsersRights newright = new UsersRights();

        //                var addedRights = new Rights();
        //                addedRights.Id = value.Id;
        //                addedRights.BitValid = value.BitValid;
        //                addedRights.StrName = value.StrName;
        //                addedRights.StrDescription = value.StrDescription;
        //                newright.IdRightNavigation = addedRights;
        //                newright.IdRight = value.Id;
        //                newright.IdUser = SelectedUser.Id;

        //                SelectedUser.UsersRights.Clear();
                        
                      
        //            }
        //            else
        //            {
        //                UsersRights newright = new UsersRights();
        //                newright.IdRight = value.Id;
        //                newright.IdUser = SelectedUser.Id;
        //                SelectedUser.UsersRights.Add(newright);
        //            }

        //        }
        //        SetProperty(ref _allRightsCurrentRight, value);
               
        //    }
            

        //}

        public SecureString _newpassword;
        public SecureString NewPassword { get => _newpassword; set => SetProperty(ref _newpassword, value); }

        public SecureString _confirmPassword;
        public SecureString ConfirmPassword { get => _confirmPassword; set => SetProperty(ref _confirmPassword, value); }

        public object AllRightsListSyncRoot { get; } = new object();

        public bool _addUser = false;
        public bool AddUser{ get => _addUser; set => SetProperty(ref _addUser, value); }

        public bool _isSaveable = true;
        public bool IsSaveable { get => _isSaveable; set => SetProperty(ref _isSaveable, value); }
        
        public List<Users>CurrentAllList{ get; set; } = new List<Users>();

    public TableController Controller { get; }
        public UserManagementViewModel()
        {
            Controller = TableController.Instance;
            Controller.ControllerStatusChanged += Controller_ControllerStatusChanged;
            SaveUserCommand = new RelayCommand(ExecuteSaveUserCommand, CanSaveUser);
            
            AddUserCommand = new RelayCommand(ExecuteAddUserCommand, CanAddUser);

            BindingOperations.EnableCollectionSynchronization(UserLoginList, UserLoginListSyncRoot);
            RegisterMessages();
        }

        private void Controller_ControllerStatusChanged(object sender, EventArgs e)
        {
            _ = GetAllRights();
            _ = GetUsers();
        }

        private void Addrights()
        {
            if (AllRightsList != null)
            {
             
                    var temprights = AllRightsList;

                    foreach (var foundallrights in temprights)
                    {
                        foundallrights.IsSelected = false;
                    }
                    foreach (var foundrights in SelectedUser.UsersRights)
                    {
                        foreach (var foundallrights in temprights)
                        {
                            if (foundrights.IdRightNavigation.StrName == foundallrights.StrName)
                            {
                                foundallrights.IsSelected = true;
                            }
                        }
                    }
                    lock (AllRightsListSyncRoot)
                    {
                        AllRightsList = temprights;
                      
                    }

                AddUser = false;
               // IsSaveable = false;
            }
        }

        private async Task GetAllRights()
        {
            var results = await Controller.GetAllRightsAsync().ConfigureAwait(true);

            lock (AllRightsListSyncRoot)
            {

                AllRightsList.Clear();
                foreach (var item in results)
                {
                    GridRights newitem = new GridRights();
                    newitem.IsSelected = false;
                    newitem.StrName = item.StrName;
                    newitem.Id = item.Id;
                    
                    newitem.StrDescription = item.StrDescription;
                    AllRightsList.Add(newitem);
                }
                
            }
        }

        private async Task GetUsers()
        {
            var results = await Controller.GetAllUsersAsync().ConfigureAwait(true);
            CurrentAllList = results;
            lock (UserLoginListSyncRoot)
            {
                string selectedSkuName = SelectedUser?.StrName;
                UserLoginList.Clear();
                foreach (var item in results)
                {
                    DisplayUser newuser = new DisplayUser();
                    newuser.Id = item.Id;
                    newuser.StrName = item.StrName;
                    newuser.StrDescription = item.StrDescription;
                    newuser.StrPassword = item.StrPassword;
                    newuser.DteExpires = item.DteExpires;
                    newuser.DteLastpwdchange = item.DteLastpwdchange;
                    newuser.DteLastfailedlogin = item.DteLastfailedlogin;
                    newuser.IntFailedlogincount = item.IntFailedlogincount;
                    newuser.BitLocked = item.BitLocked;
                    newuser.BitValid = item.BitValid;
                    newuser.UsersGroups = item.UsersGroups;
                    newuser.UsersRights = item.UsersRights;
                    newuser.AllRights = AllRightsList.ToList();
                    UserLoginList.Add(newuser);
                    DisplayUser userSelected = null;
                    if (!string.IsNullOrWhiteSpace(selectedSkuName))
                    {
                        userSelected = UserLoginList.FirstOrDefault(p => string.Equals(p.StrName, selectedSkuName, StringComparison.OrdinalIgnoreCase));
                        SelectedUser = userSelected;
                    }
                    else
                    {
                        SelectedUser = UserLoginList.FirstOrDefault();
                    }
                    

                }
            }
            

        }

        private void ExecuteAddUserCommand()
        {

            var newuser =  new DisplayUser();
            newuser.StrName = "";
            newuser.StrDescription ="Add Desc";
            newuser.StrPassword = "";
            newuser.BitLocked = false;
            newuser.BitValid = true;
            newuser.IntFailedlogincount = 0;
            newuser.DteLastpwdchange = DateTime.Now;
            newuser.AllRights = AllRightsList.ToList();
            SelectedUser = newuser;
            AddUser = true;
            IsSaveable = true;

        }
        private bool CanAddUser()
        {
            return true;
        }

        private bool CanSaveUser()
        {
            return true;
        }

        private bool SecureStringEqual(SecureString secureString1, SecureString secureString2)
        {
            if (secureString1 == null)
            {
                throw new ArgumentNullException("secureString1");
            }
            if (secureString2 == null)
            {
                throw new ArgumentNullException("secureString2");
            }

            if (secureString1.Length != secureString2.Length)
            {
                return false;
            }

            IntPtr ss_bstr1_ptr = IntPtr.Zero;
            IntPtr ss_bstr2_ptr = IntPtr.Zero;

            try
            {
                ss_bstr1_ptr = Marshal.SecureStringToBSTR(secureString1);
                ss_bstr2_ptr = Marshal.SecureStringToBSTR(secureString2);

                String str1 = Marshal.PtrToStringBSTR(ss_bstr1_ptr);
                String str2 = Marshal.PtrToStringBSTR(ss_bstr2_ptr);

                return str1.Equals(str2);
            }
            finally
            {
                if (ss_bstr1_ptr != IntPtr.Zero)
                {
                    Marshal.ZeroFreeBSTR(ss_bstr1_ptr);
                }

                if (ss_bstr2_ptr != IntPtr.Zero)
                {
                    Marshal.ZeroFreeBSTR(ss_bstr2_ptr);
                }
            }

        }

        private bool validateUser()
        {
          
            if (string.IsNullOrWhiteSpace(SelectedUser.StrName))
            {
                Controller.Log.Info(FkEventCode.User_Updated_Name_Failed, SelectedUser.StrName);
                return false;
            }
            if (string.IsNullOrWhiteSpace(SelectedUser.StrDescription))
            {
                Controller.Log.Info(FkEventCode.User_Updated_Desc_Failed, SelectedUser.StrName);
                return false;
            }

            if (AddUser)
            {
                if (NewPassword.Length <=0)
                {
                    var Countmessage = new MessageBoxesView($"{ Properties.Strings.UserSettings_Password_Required}{System.Environment.NewLine}"
                                       , MessageBoxButton.OK, MessageBoxImage.Information);

                    bool? dialogResult = Countmessage.ShowDialog();
                    Controller.Log.Info(FkEventCode.User_Updated_Password_Failed, SelectedUser.StrName);
                    return false;
                }

                if (NewPassword.Length < 4)
                {
                    var Countmessage = new MessageBoxesView($"{ Properties.Strings.UserSettings_Password_Length}{System.Environment.NewLine}"
                                        , MessageBoxButton.OK, MessageBoxImage.Information);

                    bool? dialogResult = Countmessage.ShowDialog();

                    Controller.Log.Info(FkEventCode.User_Updated_PasswordLength_Failed, SelectedUser.StrName);
                    return false;
   
                }

                if (ConfirmPassword.Length<=0)
                {
                    var Countmessage = new MessageBoxesView($"{ Properties.Strings.UserSettings_Password_Required}{System.Environment.NewLine}"
                   , MessageBoxButton.OK, MessageBoxImage.Information);

                    bool? dialogResult = Countmessage.ShowDialog();

                    Controller.Log.Info(FkEventCode.User_Updated_Password_Failed, SelectedUser.StrName);
                    return false;
                    
                }
                if (ConfirmPassword.Length < 4)
                {
                    var Countmessage = new MessageBoxesView($"{ Properties.Strings.UserSettings_Password_Length}{System.Environment.NewLine}"
                    , MessageBoxButton.OK, MessageBoxImage.Information);

                    bool? dialogResult = Countmessage.ShowDialog();

                    Controller.Log.Info(FkEventCode.User_Updated_PasswordLength_Failed, SelectedUser.StrName);
                    return false;
                }
            }
            else
            {
                if (NewPassword.Length >0)
                {
                    if (NewPassword.Length < 4)
                    {
                        var Countmessage = new MessageBoxesView($"{ Properties.Strings.UserSettings_Password_Length}{System.Environment.NewLine}"
                    , MessageBoxButton.OK, MessageBoxImage.Information);

                        bool? dialogResult = Countmessage.ShowDialog();

                        Controller.Log.Info(FkEventCode.User_Updated_PasswordLength_Failed, SelectedUser.StrName);
                        return false;
                    }
                }
                if (ConfirmPassword.Length>0)
                {
                    if (ConfirmPassword.Length < 4)
                    {
                        var Countmessage = new MessageBoxesView($"{ Properties.Strings.UserSettings_Password_Length}{System.Environment.NewLine}"
                    , MessageBoxButton.OK, MessageBoxImage.Information);

                        bool? dialogResult = Countmessage.ShowDialog();

                        Controller.Log.Info(FkEventCode.User_Updated_PasswordLength_Failed, SelectedUser.StrName);
                        return false;
                    }
                }
            }
            if (NewPassword.Length > 0)
            {
                if (SecureStringEqual(NewPassword, ConfirmPassword))
                {
                    SelectedUser.StrPassword = PasswordHash.GetPasswordHash(NewPassword);
                }
                else
                {
                    Controller.Log.Info(FkEventCode.User_Updated_Password_Mismatch, SelectedUser.StrName);
                    return false;
                }
            }

            var found = SelectedUser.AllRights.FirstOrDefault(f => f.IsSelected == true);
            if (found == null)
            {
                var Countmessage = new MessageBoxesView($"{ Properties.Strings.UserSettings_Right_Error}{System.Environment.NewLine}"
                , MessageBoxButton.OK, MessageBoxImage.Information);

                bool? dialogResult = Countmessage.ShowDialog();

                Controller.Log.Info(FkEventCode.User_No_Rights_Selected, SelectedUser.StrName);
                return false;
            }

            return true;
        }

        private async Task ExecuteSaveUserCommand()
        {
            try
            {
                if (validateUser())
                {
                    List<UsersRights> usernewrights = new List<UsersRights>();
                    foreach (var Newrights in SelectedUser.AllRights)
                    {
                        if (Newrights.IsSelected == true)
                        {
                            var myUserRights = new UsersRights();
                            myUserRights.IdUser = SelectedUser.Id;
                            myUserRights.IdRight = Newrights.Id;

                            var addedRights = new Rights();
                            addedRights.Id = Newrights.Id;
                            addedRights.BitValid = addedRights.BitValid;
                            addedRights.StrName = Newrights.StrName;
                            addedRights.StrDescription = Newrights.StrDescription;
                            myUserRights.IdRightNavigation = addedRights;
                            usernewrights.Add(myUserRights);
                        }

                    }
                    SaveState savestate;
                    if (AddUser)
                    {
                        savestate = SaveState.Add;
                    }
                    else
                    {
                        savestate = SaveState.Update;
                    }

                    var founduser = CurrentAllList.FirstOrDefault(u => u.StrName == SelectedUser.StrName);
                    await Controller.SaveUserAsync(SelectedUser, usernewrights, savestate, founduser).ConfigureAwait(true);
                }
            }
            catch(Exception ex)
            {

            }
            finally
            {
                AddUser = false;
              //  ConfirmPassword =null;
               // NewPassword =null;
                Messenger.Default.Send(new UserMgtFocusMessage() { UserMgtHasfocus = true });
                //RefreshCanExecute();
            }
        }


        public void RefreshCanExecute()
        {
            _ = GetAllRights();
            _ = GetUsers();
        }


        public override void RegisterMessages()
        {
            Messenger.Default.Register<UserMgtFocusMessage>(this, ExecuteUserMgtFocusMessage);
        }

        public override void UnRegisterMessages()
        {
            Messenger.Default.Unregister<UserMgtFocusMessage>(this, ExecuteUserMgtFocusMessage);
        }

        private void ExecuteUserMgtFocusMessage(UserMgtFocusMessage msg)
        {

            AddUser = false;
            RefreshCanExecute();
        }

    }
    public class GridRights :Rights
    {
        public bool? IsSelected { get; set; } 

    }

    

    public class DisplayUser : Users
    {
        public List<GridRights> AllRights { get; set; }

    }


    

}
