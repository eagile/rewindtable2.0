﻿using GalaSoft.MvvmLight.Messaging;
using Kiosk.Messages;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Kiosk.Views
{
    /// <summary>
    /// Interaction logic for UserManagementView.xaml
    /// </summary>
    public partial class UserManagementView : UserControl
    {
        public UserManagementView()
        {
            InitializeComponent();
            NewPasswordBox.LostFocus += NewPasswordBox_LostFocus;
            ConfirmPasswordBox.LostFocus += ConfirmPasswordBox_LostFocus;
            Messenger.Default.Register<UserMgtFocusMessage>(this, ExecuteUserMgtFocusMessage);
            radgridview.SelectedCellsChanged += Radgridview_SelectedCellsChanged;
        }

        private void Radgridview_SelectedCellsChanged(object sender, Telerik.Windows.Controls.GridView.GridViewSelectedCellsChangedEventArgs e)
        {
            if (DataContext is UserManagementViewModel UserManagementView)
            {

              
            }
        }

        private void ExecuteUserMgtFocusMessage(UserMgtFocusMessage msg)
        {
            ConfirmPasswordBox.Clear();
            NewPasswordBox.Clear();
        }

        private void ConfirmPasswordBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (DataContext is UserManagementViewModel UserManagementView)
            {
                UserManagementView.ConfirmPassword = ConfirmPasswordBox.SecurePassword;
            }
        }

        private void NewPasswordBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (DataContext is UserManagementViewModel UserManagementView)
            {
                UserManagementView.NewPassword= NewPasswordBox.SecurePassword;
                UserManagementView.ConfirmPassword = ConfirmPasswordBox.SecurePassword;
            }
        }

        private void RadButton_Click(object sender, RoutedEventArgs e)
        {
            UserName.SelectAll();
            UserName.Focus();
        }
    }
}
