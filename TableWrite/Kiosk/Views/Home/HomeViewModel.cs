﻿using ceTe.DynamicPDF;
using ceTe.DynamicPDF.LayoutEngine;
using ceTe.DynamicPDF.Merger;
using ceTe.DynamicPDF.PageElements;
using ceTe.DynamicPDF.Printing;
using EA.Helpers.WPF;
using GalaSoft.MvvmLight.Messaging;
using Kiosk.Cultures;
using Kiosk.Messages;
using Kiosk.Views.BaseModels;
using KioskManager.Events;
using KioskManagerLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;


namespace Kiosk.Views
{
    public class HomeViewModel : ViewModel
    {
        public TableController Controller { get; }

        //public RelayCommand ReadRFIDCommand => _readRFIDCommand ?? (_readRFIDCommand = new RelayCommand(ExecuteReadRFIDCommand, CanRead));
        //private RelayCommand _readRFIDCommand;

        public RelayCommand StartCommand => _startCommand ?? (_startCommand = new RelayCommand(ExecuteStartCommand, CanStart));
        private RelayCommand _startCommand;
        public RelayCommand stopmotorCommand => _stopmotorCommand ?? (_stopmotorCommand = new RelayCommand(ExecutestopmotorCommand, CanStart));
        private RelayCommand _stopmotorCommand;

        
        public bool IsReading { get => _isReading; private set => SetProperty(ref _isReading, value, nameof(IsReading), nameof(IsBusy)); }
        private bool _isReading;

        public Visibility _isLookupVisible;
        public Visibility IsLookupVisible { get => _isLookupVisible; private set => SetProperty(ref _isLookupVisible, value, nameof(IsLookupVisible)); }



        public bool IsPrinting { get => _isPrinting; private set => SetProperty(ref _isPrinting, value, nameof(IsPrinting), nameof(IsBusy)); }
        private bool _isPrinting;

        public bool IsBusy => IsReading || IsPrinting;

        public HomeViewModel()
        {
            Controller = TableController.Instance;
            Controller.LanguageChanged += Controller_LanguageChanged;
            Controller.SettingsLoaded += Controller_SettingsLoaded;

            
        }

        private void Controller_SettingsLoaded(object sender, EventArgs e)
        {
            if(Controller.CurrentSettings.Kiosk.AllowLookUp)
            {
                IsLookupVisible = Visibility.Visible;
            }
            else
            {
                IsLookupVisible = Visibility.Hidden;
            }


        }

        private void Controller_LanguageChanged(object sender, CultureInfo e)
        {
            RefreshProperties();
            

        }

        //private bool CanRead()
        //{

        //    return !IsBusy && Controller.IsLoaded && Controller.RfidReader.IsConnected && !Controller.RfidReader.AntennaDisconnected;
        //}
        ///// <summary>
        ///// 
        ///// </summary>
        ///// <returns></returns>
        //private async Task ExecuteReadRFIDCommand()
        //{
        //    try
        //    {
        //        IsReading = true;
        //        var message = string.Empty;
        //     //   await Controller.ReadTags().ConfigureAwait(false);

        //    }
        //    catch (Exception ex)
        //    {
        //        Controller.Log.Error(ex, FkEventCode.Rfid_Read_Error, ex.Message);
        //    }
        //    finally
        //    {
        //        IsReading = false;
        //    }
        //}

        private bool CanStart()
        {
            return !IsBusy;
        }


        public event EventHandler BatchOrderReportPrinted;
        protected virtual void OnBatchOrderReportPrinted()
        {
            try { BatchOrderReportPrinted?.Invoke(this, EventArgs.Empty); } catch { }
        }

        private bool CanLookUp()
        {
            return !IsBusy;
        }

        private async Task ExecuteStartCommand()
        {
            await Controller.BarCodeReader.CloseBarcodeReader();

//            await Controller.LoadRecoveryData();
            await Controller.GetProductionData();
        }

        private void ExecutestopmotorCommand()
        {
            
             Controller.StopMotor();
        }


        public override void RegisterMessages()
        {
            Messenger.Default.Register<ControllerMessage>(this, ExecuteAssignController);
        }

        public override void UnRegisterMessages()
        {
            Messenger.Default.Unregister<ControllerMessage>(this, ExecuteAssignController);
        }

        private void ExecuteAssignController(ControllerMessage msg)
        {

            //if ((msg.controller != null))
            //{
            //    Controller = msg.controller;
            //}

        }


    }
}
