﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kiosk.Models
{

    public class TotalSkuAssociationModel
    {
        public string SKU { get; set; }
        public List<ScannedItem> ScannedItems { get; set; }
    }
    public class ScannedItem
    {
        public string ThingID { get; set; }

    }
}
