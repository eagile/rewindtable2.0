﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kiosk.Models
{
    public class KioskLogModel
    {

        public string EPC { get; set; }
        public string RawEPC { get; set; }
        public string TimeStamp { get; set; }
        public string TID { get; set; }
        public string Location { get; set; }



    }
}
