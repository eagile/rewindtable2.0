USE [QC_Kiosk]
GO
/****** Object:  StoredProcedure [dbo].[QC_Kiosk_DatabaseBackup_FULL]    Script Date: 7/22/2020 3:17:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		eAgile inc.
-- Create date: 20200715
-- Description:	full backup of QC_Kiosk database
-- =============================================

ALTER PROCEDURE [dbo].[QC_Kiosk_DatabaseBackup_FULL]
@backupDirectory VARCHAR(500)

AS
BEGIN
	SET NOCOUNT ON;

		--set backup location
		DECLARE @backupFolder VARCHAR(1000) = @backupDirectory;

		DECLARE @user TABLE 
				(	rowNbr INT IDENTITY(1,1) PRIMARY KEY,
					name varchar(100)
				);

		INSERT INTO @user 
				(name)

		-- user databases
		SELECT	name
		FROM	master.sys.databases
		WHERE	name in ('QC_Kiosk');

		-- get count of databaseses to loop through
		DECLARE @loopCount INT;
		SET		@loopCount =	(	SELECT	COUNT(*) 
									FROM	@user
								);

		-- loop through each databse and create backup
		WHILE (@loopCount > 0)
			BEGIN
		
					--Date logic for backup name suffix
					DECLARE @curDate DATETIME = GETDATE();
					DECLARE @formatDate VARCHAR(8) = CONVERT(VARCHAR(25), @curDate, 112);
					DECLARE @hour VARCHAR(2) = RIGHT('00' + CONVERT(VARCHAR(2), DATEPART(hour, @curDate)),2);
					DECLARE @min VARCHAR(2) = RIGHT('00' + CONVERT(VARCHAR(2), DATEPART(minute, @curDate)),2);
					DECLARE @sec VARCHAR(2) = RIGHT('00' + CONVERT(VARCHAR(2), DATEPART(second, @curDate)), 2);
					DECLARE @backupDate VARCHAR(20) = @formatDate + '_' + @hour + @min + @sec;

					DECLARE @tableName VARCHAR(100);
					SET		@tableName =	(	SELECT	name 
												FROM	@user 
												WHERE	rowNbr = @loopCount
											);
					DECLARE @backupLocation VARCHAR(1000) =	(	@backupFolder + @tableName + '_' + @backupDate + '_FULL.bak'	);

					BACKUP DATABASE @tableName TO  DISK = @backupLocation WITH NOFORMAT, 
						NOINIT,  
						NAME = N'Full Database Backup', 
						SKIP, 
						NOREWIND, 
						NOUNLOAD, 
						COMPRESSION,  
						STATS = 10;

					SET @loopCount = @loopCount - 1;

END
END