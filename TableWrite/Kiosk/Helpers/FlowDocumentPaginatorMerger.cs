﻿// ***********************************************************************
// Assembly         : LineManager
// Author           : eAgile
// Created          : 01-28-2020
//
// Last Modified By : eAgile
// Last Modified On : 01-28-2020
// ***********************************************************************
// <copyright file="FlowDocumentPaginatorMerger.cs" company="eAgile Inc">
//     eAgile Inc.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;

namespace Kiosk.Helpers
{
    /// <summary>
    /// Class FlowDocumentPaginatorMerger.
    /// Implements the <see cref="DocumentPaginator" />
    /// </summary>
    /// <seealso cref="DocumentPaginator" />
    class FlowDocumentPaginatorMerger : DocumentPaginator
    {
        /// <summary>
        /// The document paginators
        /// </summary>
        private List<DocumentPaginator> documentPaginators = new List<DocumentPaginator>();
        /// <summary>
        /// The page count
        /// </summary>
        private int pageCount;
        /// <summary>
        /// The page size
        /// </summary>
        private Size pageSize;

        /// <summary>
        /// Initializes a new instance of the <see cref="FlowDocumentPaginatorMerger"/> class.
        /// </summary>
        /// <param name="flowDocumentsToMerge">The flow documents to merge.</param>
        /// <param name="PageSize">Size of the page.</param>
        public FlowDocumentPaginatorMerger(List<FlowDocument> flowDocumentsToMerge, Size PageSize)
        {
            pageCount = 0;

            foreach (FlowDocument flowDocument in flowDocumentsToMerge)
            {
                var documentPaginator = ((IDocumentPaginatorSource)flowDocument).DocumentPaginator;

                documentPaginator.PageSize = PageSize;
                documentPaginator.ComputePageCount();
                pageCount += documentPaginator.PageCount;

                documentPaginators.Add(documentPaginator);
            }
            pageSize = PageSize;
        }

        /// <summary>
        /// When overridden in a derived class, gets a value indicating whether <see cref="P:System.Windows.Documents.DocumentPaginator.PageCount" /> is the total number of pages.
        /// </summary>
        /// <value><c>true</c> if this instance is page count valid; otherwise, <c>false</c>.</value>
        public override bool IsPageCountValid
        {
            get
            {
                foreach (DocumentPaginator documentPaginator in documentPaginators)
                {
                    if (!documentPaginator.IsPageCountValid)
                        return false;
                }
                return true;
            }
        }

        /// <summary>
        /// When overridden in a derived class, gets a count of the number of pages currently formatted
        /// </summary>
        /// <value>The page count.</value>
        public override int PageCount
        {
            get { return pageCount; }
        }

        /// <summary>
        /// When overridden in a derived class, gets or sets the suggested width and height of each page.
        /// </summary>
        /// <value>The size of the page.</value>
        public override Size PageSize
        {
            get { return pageSize; }
            set { pageSize = value; }
        }

        /// <summary>
        /// When overridden in a derived class, returns the element being paginated.
        /// </summary>
        /// <value>The source.</value>
        public override IDocumentPaginatorSource Source
        {
            get { return documentPaginators[0].Source; }
        }

        /// <summary>
        /// When overridden in a derived class, gets the <see cref="T:System.Windows.Documents.DocumentPage" /> for the specified page number.
        /// </summary>
        /// <param name="pageNumber">The zero-based page number of the document page that is needed.</param>
        /// <returns>The <see cref="T:System.Windows.Documents.DocumentPage" /> for the specified <paramref name="pageNumber" />, or <see cref="F:System.Windows.Documents.DocumentPage.Missing" /> if the page does not exist.</returns>
        public override DocumentPage GetPage(int pageNumber)
        {
            int remainingPageNumber = pageNumber;
            DocumentPage documentPage = null;

            foreach (DocumentPaginator documentPaginator in documentPaginators)
            {
                DocumentPage documentPageTemp = null;

                if (remainingPageNumber < documentPaginator.PageCount)
                {
                    documentPageTemp = documentPaginator.GetPage(remainingPageNumber);
                    documentPage = AddHeaderFooter(documentPageTemp, documentPaginators.IndexOf(documentPaginator));
                    break;
                }
                else
                {
                    remainingPageNumber -= documentPaginator.PageCount;
                }
            }
            return documentPage;
        }

        /// <summary>
        /// Adds the header footer.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="PageNumber">The page number.</param>
        /// <returns>DocumentPage.</returns>
        private DocumentPage AddHeaderFooter(DocumentPage page, int PageNumber)
        {
            ContainerVisual newVisual = new ContainerVisual();
            newVisual.Children.Add(page.Visual);

            //DrawingVisual header = new DrawingVisual();
            //using ( DrawingContext dc = header.RenderOpen() )
            //{
            //    //Header data
            //}
            //newVisual.Children.Add( header );

            DrawingVisual footer = new DrawingVisual();
            using (DrawingContext dc = footer.RenderOpen())
            {
                Typeface typeface = new Typeface(new FontFamily("Calibri")
                                                , FontStyles.Normal
                                                , FontWeights.Bold
                                                , FontStretches.Normal);

                string date = DateTime.Now.ToString(Thread.CurrentThread.CurrentCulture.DateTimeFormat);

                FormattedText printedDate = new FormattedText(date
                                            , CultureInfo.CurrentCulture
                                            , FlowDirection.LeftToRight
                                            , typeface
                                            , 14
                                            , Brushes.Black);

                FormattedText printedPageNumber = new FormattedText("Page " + (PageNumber + 1).ToString() + " of " + PageCount.ToString()
                                            , CultureInfo.CurrentCulture
                                            , FlowDirection.LeftToRight
                                            , typeface
                                            , 14
                                            , Brushes.Black);

                dc.DrawText(printedDate, new Point(80, page.Size.Height - 50));
                dc.DrawText(printedPageNumber, new Point(page.Size.Width - 150, page.Size.Height - 50));
            }

            newVisual.Children.Add(footer);

            DocumentPage newPage = new DocumentPage(newVisual);
            return newPage;
        }

    }
}
