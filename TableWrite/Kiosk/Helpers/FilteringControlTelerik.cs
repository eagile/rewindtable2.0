﻿using System.Windows.Controls.Primitives;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Kiosk.Helpers
{
    public class FilteringControlTelerik : FilteringControl
    {
        public FilteringControlTelerik(GridViewColumn column) : base(column)
        {

        }

        public override void Prepare(GridViewColumn columnToPrepare)
        {

            base.Prepare(columnToPrepare);
        }

        protected override void OnApplyFilter()
        {
            base.OnApplyFilter();

            var popup = this.ParentOfType<Popup>();
            if (popup != null)
            {
                popup.IsOpen = false;
            }
        }

        protected override void OnClearFilter()
        {
            base.OnClearFilter();

            var popup = this.ParentOfType<Popup>();
            if (popup != null)
            {
                popup.IsOpen = false;
            }
        }
    }
}
