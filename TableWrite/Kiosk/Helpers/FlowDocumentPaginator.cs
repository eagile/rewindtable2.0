﻿// ***********************************************************************
// Assembly         : LineManager
// Author           : eAgile
// Created          : 01-28-2020
//
// Last Modified By : eAgile
// Last Modified On : 01-28-2020
// ***********************************************************************
// <copyright file="FlowDocumentPaginator.cs" company="eAgile Inc">
//     eAgile Inc.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;

namespace Kiosk.Helpers
{
    /// <summary>
    /// Class FlowDocumentPaginator.
    /// Implements the <see cref="DocumentPaginator" />
    /// </summary>
    /// <seealso cref="DocumentPaginator" />
    class FlowDocumentPaginator : DocumentPaginator
    {
        /// <summary>
        /// The flow documentpaginator
        /// </summary>
        private DocumentPaginator flowDocumentpaginator;

        /// <summary>
        /// Initializes a new instance of the <see cref="FlowDocumentPaginator"/> class.
        /// </summary>
        /// <param name="document">The document.</param>
        public FlowDocumentPaginator(FlowDocument document)
        {
            flowDocumentpaginator = ((IDocumentPaginatorSource)document).DocumentPaginator;
        }

        /// <summary>
        /// When overridden in a derived class, gets a value indicating whether <see cref="P:System.Windows.Documents.DocumentPaginator.PageCount" /> is the total number of pages.
        /// </summary>
        /// <value><c>true</c> if this instance is page count valid; otherwise, <c>false</c>.</value>
        public override bool IsPageCountValid
        {
            get { return flowDocumentpaginator.IsPageCountValid; }
        }

        /// <summary>
        /// When overridden in a derived class, gets a count of the number of pages currently formatted
        /// </summary>
        /// <value>The page count.</value>
        public override int PageCount
        {
            get { return flowDocumentpaginator.PageCount; }
        }

        /// <summary>
        /// When overridden in a derived class, gets or sets the suggested width and height of each page.
        /// </summary>
        /// <value>The size of the page.</value>
        public override Size PageSize
        {
            get { return flowDocumentpaginator.PageSize; }
            set { flowDocumentpaginator.PageSize = value; }
        }

        /// <summary>
        /// When overridden in a derived class, returns the element being paginated.
        /// </summary>
        /// <value>The source.</value>
        public override IDocumentPaginatorSource Source
        {
            get { return flowDocumentpaginator.Source; }
        }

        /// <summary>
        /// Gets the page.
        /// </summary>
        /// <param name="pageNumber">The page number.</param>
        /// <returns>DocumentPage.</returns>
        public override DocumentPage GetPage(int pageNumber)
        {
            DocumentPage page = flowDocumentpaginator.GetPage(pageNumber);
            ContainerVisual newVisual = new ContainerVisual();
            newVisual.Children.Add(page.Visual);

            //DrawingVisual header = new DrawingVisual();
            //using ( DrawingContext dc = header.RenderOpen() )
            //{
            //    //Header data
            //}
            //newVisual.Children.Add( header );

            DrawingVisual footer = new DrawingVisual();
            using (DrawingContext dc = footer.RenderOpen())
            {
                Typeface typeface = new Typeface(new FontFamily("Calibri")
                                                , FontStyles.Normal
                                                , FontWeights.Bold
                                                , FontStretches.Normal);

                FormattedText printedDate = new FormattedText(DateTime.Now.ToString("yyyy-MM-dd")
                                            , CultureInfo.CurrentCulture
                                            , FlowDirection.LeftToRight
                                            , typeface
                                            , 14
                                            , Brushes.Black);

                FormattedText printedPageNumber = new FormattedText("Page " + (pageNumber + 1).ToString() + " of " + PageCount.ToString()
                                            , CultureInfo.CurrentCulture
                                            , FlowDirection.LeftToRight
                                            , typeface
                                            , 14
                                            , Brushes.Black);

                dc.DrawText(printedDate, new Point(80, page.Size.Height - 50));
                dc.DrawText(printedPageNumber, new Point(page.Size.Width - 150, page.Size.Height - 50));
            }

            newVisual.Children.Add(footer);

            DocumentPage newPage = new DocumentPage(newVisual);
            return newPage;
        }
    }
}
