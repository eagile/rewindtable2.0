﻿// ***********************************************************************
// Assembly         : LineManager
// Author           : eAgile
// Created          : 01-28-2020
//
// Last Modified By : eAgile
// Last Modified On : 04-30-2020
// ***********************************************************************
// <copyright file="XamlTemplatePrinter.cs" company="eAgile Inc">
//     eAgile Inc.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.IO;
using System.Printing;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Windows.Xps;
using System.Xml;

namespace Kiosk.Helpers
{
    /// <summary>
    /// Class XamlTemplatePrinter.
    /// </summary>
    public class XamlTemplatePrinter
    {
        /// <summary>
        /// render flow document template as an asynchronous operation.
        /// </summary>
        /// <param name="templatePath">The template path.</param>
        /// <returns>IDocumentPaginatorSource.</returns>
        public static async Task<IDocumentPaginatorSource> RenderFlowDocumentTemplateAsync(string templatePath)
        {
            string rawXamlText = "";

            //Create a StreamReader that will read from the document template.
            using (StreamReader streamReader = File.OpenText(templatePath))
            {
                rawXamlText = await streamReader.ReadToEndAsync();
            }
            //Use the XAML reader to create a FlowDocument from the XAML string.
            FlowDocument document = XamlReader.Load(new XmlTextReader(new StringReader(rawXamlText))) as FlowDocument;
            return document;
        }

        /// <summary>
        /// Renders the flow document template.
        /// </summary>
        /// <param name="templatePath">The template path.</param>
        /// <param name="dataContextObject">The data context object.</param>
        /// <returns>IDocumentPaginatorSource.</returns>
        public static async Task<IDocumentPaginatorSource> RenderFlowDocumentTemplate(string templatePath, object dataContextObject)
        {
            string rawXamlText = "";
            using (StreamReader streamReader = File.OpenText(templatePath))
            {
                rawXamlText = await streamReader.ReadToEndAsync();
            }

            FlowDocument document = XamlReader.Load(new XmlTextReader(new StringReader(rawXamlText))) as FlowDocument;
            if (dataContextObject != null)
            {
                document.DataContext = dataContextObject;
            }
            return document;
        }

        /// <summary>
        /// Renders the flow document string.
        /// </summary>
        /// <param name="rawXamlString">The raw xaml string.</param>
        /// <param name="dataContextObject">The data context object.</param>
        /// <returns>IDocumentPaginatorSource.</returns>
        public static IDocumentPaginatorSource RenderFlowDocumentString(string rawXamlString, object dataContextObject)
        {
            FlowDocument document = XamlReader.Load(new XmlTextReader(new StringReader(rawXamlString))) as FlowDocument;
            if (dataContextObject != null)
            {
                document.DataContext = dataContextObject;
            }
            return document;
        }


        /// <summary>
        /// render flow document template as an asynchronous operation.
        /// </summary>
        /// <param name="templatePath">The template path.</param>
        /// <param name="dataContextObject">The data context object.</param>
        /// <param name="toBeReplacedString">To be replaced string.</param>
        /// <param name="replacementString">The replacement string.</param>
        /// <returns>IDocumentPaginatorSource.</returns>
        public static async Task<IDocumentPaginatorSource> RenderFlowDocumentTemplateAsync(string templatePath, object dataContextObject, string toBeReplacedString, string replacementString)
        {
            string rawXamlText = "";
            string rawXamlTextWithReplaces = "";
            FlowDocument document = new FlowDocument();
            using (StreamReader streamReader = File.OpenText(templatePath))
            {
                rawXamlText = await streamReader.ReadToEndAsync();
            }
            if (!string.IsNullOrWhiteSpace(toBeReplacedString) && !string.IsNullOrWhiteSpace(replacementString))
            {
                rawXamlTextWithReplaces = rawXamlText.Replace(toBeReplacedString, replacementString);

            }
            else
            {
                rawXamlTextWithReplaces = rawXamlText;
            }

            try
            {
                document = XamlReader.Load(new XmlTextReader(new StringReader(rawXamlTextWithReplaces))) as FlowDocument;

                if (dataContextObject != null)
                {
                    document.DataContext = dataContextObject;
                }

            }
            catch (Exception ex)
            {


            }
            return document;
        }


        /// <summary>
        /// Gets the print dialog.
        /// </summary>
        /// <param name="ShowPrintDialog">if set to <c>true</c> [show print dialog].</param>
        /// <returns>PrintDialog.</returns>
        public static PrintDialog GetPrintDialog(bool ShowPrintDialog)
        {
            PrintDialog printDialog = null;

            // Create a Print dialog.
            PrintDialog dlg = new PrintDialog();

            if (ShowPrintDialog)
            {
                // Show the printer dialog.  If the return is "true",
                // the user made a valid selection and clicked "Ok".
                if (dlg.ShowDialog() == true)
                    printDialog = dlg;  // return the dialog the user selections.
            }
            else
                printDialog = dlg;

            return printDialog;
        }


        /// <summary>
        /// print flow document as an asynchronous operation.
        /// </summary>
        /// <param name="printQueue">The print queue.</param>
        /// <param name="document">The document.</param>
        public static async Task PrintFlowDocumentAsync(PrintQueue printQueue, DocumentPaginator document)
        {
            await PrintDocumentPaginatorAsync(PrintQueue.CreateXpsDocumentWriter(printQueue), document);
        }

        /// <summary>
        /// print document paginator as an asynchronous operation.
        /// </summary>
        /// <param name="xpsDocumentWriter">The XPS document writer.</param>
        /// <param name="document">The document.</param>
        private static async Task PrintDocumentPaginatorAsync(XpsDocumentWriter xpsDocumentWriter, DocumentPaginator document)
        {
            TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool>();

            xpsDocumentWriter.WritingCompleted += (sender, args) =>
            {
                if (args.Error != null)
                    tcs.TrySetException(args.Error);
                else
                    tcs.TrySetResult(true);
            };

            xpsDocumentWriter.WritingCancelled += (sender, args) =>
            {
                tcs.TrySetResult(true);
            };

            xpsDocumentWriter.WriteAsync(document);
            await tcs.Task;
        }

    }


}
