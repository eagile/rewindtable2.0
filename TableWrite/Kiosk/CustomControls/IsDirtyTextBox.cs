﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
namespace Kiosk.CustomControls
{
    public class IsDirtyTextBox : TextBox, IWarning
    {
        private bool _isSet = false;

        public IsDirtyTextBox()
        {
            TextChanged += IsDirtyButton_TextChanged;
            GotFocus += IsDirtyTextBox_GotFocus;
            PreviewMouseDown += IsDirtyTextBox_PreviewMouseDown;
        }

        private void IsDirtyTextBox_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            // If its a triple click, select all text for the user.
            if (e.ClickCount == 3)
            {
                TextBox_SelectAllText(sender, new RoutedEventArgs());
                return;
            }

            // Find the TextBox
            DependencyObject parent = e.OriginalSource as UIElement;
            while (parent != null && !(parent is TextBox))
            {
                parent = VisualTreeHelper.GetParent(parent);
            }

            if (parent != null)
            {
                var textBox = (TextBox)parent;
                if (!textBox.IsKeyboardFocusWithin)
                {
                    // If the text box is not yet focussed, give it the focus and
                    // stop further processing of this click event.
                    textBox.Focus();
                    e.Handled = true;
                }
            }
        }

        private void TextBox_SelectAllText(object sender, RoutedEventArgs e)
        {
            ((TextBox)sender).SelectAll();
        }

        void IsDirtyTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (!_isSet)
            {
                SetValue(DefaultTextProperty, ((TextBox)sender).Text);
                _isSet = true;
            }
        }

        void IsDirtyButton_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!_isSet)
            {
                SetValue(DefaultTextProperty, ((TextBox)sender).Text);
                _isSet = true;
            }
            IsDirty = (string)GetValue(DefaultTextProperty) != ((TextBox)sender).Text;
        }

        public bool IsDirty
        {
            get { return (bool)GetValue(IsDirtyProperty); }
            set { SetValue(IsDirtyProperty, value); }
        }

        public bool OverrideTextChangedColor
        {
            get { return (bool)GetValue(OverrideTextChangedColorProperty); }
            set { SetValue(OverrideTextChangedColorProperty, value); }
        }

        public SolidColorBrush OverrideBackGroundColor
        {
            get { return (SolidColorBrush)GetValue(OverrideBackGroundColorProperty); }
            set { SetValue(OverrideBackGroundColorProperty, value); }
        }

        public string DefaultText
        {
            get { return (string)GetValue(DefaultTextProperty); }
            set { SetValue(DefaultTextProperty, value); }
        }

        public static readonly DependencyProperty OverrideBackGroundColorProperty = DependencyProperty.Register("OverrideBackGroundColor", typeof(SolidColorBrush), typeof(IsDirtyTextBox), new FrameworkPropertyMetadata(SystemColors.InactiveBorderBrush, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        public static readonly DependencyProperty OverrideTextChangedColorProperty = DependencyProperty.Register("OverrideTextChangedColor", typeof(bool), typeof(IsDirtyTextBox), new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        public static readonly DependencyProperty IsDirtyProperty = DependencyProperty.Register("IsDirty", typeof(bool), typeof(IsDirtyTextBox), new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        public static readonly DependencyProperty DefaultTextProperty = DependencyProperty.Register("DefaultText", typeof(string), typeof(IsDirtyTextBox), new FrameworkPropertyMetadata("", FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public bool HasWarning { get; set; }
        public Color WarningColor { get; set; }
        public string Message { get; set; }
    }

    public interface IWarning
    {
        bool HasWarning { get; set; }
        Color WarningColor { get; set; }
        string Message { get; set; }
    }
}
