﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WriteRewind.CustomControls
{
    /// <summary>
    /// Interaction logic for BaseAnalysis.xaml
    /// </summary>
    public partial class BaseAnalysis : UserControl,INotifyPropertyChanged
    {
        public BaseAnalysis()
        {
            InitializeComponent();
        }
       // public string Title { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        //public static DependencyProperty DefaultTextProperty =
        //DependencyProperty.Register("DefaultText", typeof(string), typeof(BaseAnalysis), new FrameworkPropertyMetadata("", FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, PropertyChangedCallback));
        //private static void PropertyChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        //{
        //    ((BaseAnalysis)dependencyObject).DefaultText = (string)dependencyPropertyChangedEventArgs.NewValue.ToString();
        //}
        //private const string HelloMessage = "Hello World";

        //public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string info)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(info));
            }
        }

        
        private string _title;

        public string Title
        {
            get { return _title; }
            set
            {
                _title = value;
                OnPropertyChanged(Title);
            }
        }


    }

}

