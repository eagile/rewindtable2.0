﻿using GalaSoft.MvvmLight.Messaging;
using Kiosk.Messages;
using Kiosk.Views;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace Kiosk
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private CultureInfo cultureOverride = System.Globalization.CultureInfo.GetCultureInfo("en-US");
        private Splash _splashScreen;

        
        public App()
        {
            cultureOverride = CultureInfo.CurrentCulture;

            if (Debugger.IsAttached == true && cultureOverride != null)
            {
                Thread.CurrentThread.CurrentUICulture = cultureOverride;
                Thread.CurrentThread.CurrentCulture = cultureOverride;
            }
            _splashScreen = new Splash();
            _splashScreen.Show();


        }

        private void RegisterMessages()
        {
            
        }

        private void UnRegisterMessages()
        {
            
        }
        private void ExecuteLogGotFocusMessage(SettingTabFocusMessage msg)
        {

            
            
        }

    }
}
