﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Kiosk.Cultures
{
    /// <summary>
    /// Wraps up XAML access to instance of Kiosk.Properties.Strings
    /// </summary>
    public class CultureResources
    {
        // https://www.codeproject.com/Articles/22967/WPF-Runtime-Localization
        // https://stackoverflow.com/questions/373388/best-way-to-implement-multi-language-globalization-in-large-net-project

        /// <summary>
        /// The Resources ObjectDataProvider uses this method to get an instance of the WPFLocalize.Properties.Resources class
        /// </summary>
        /// <returns></returns>
        public Properties.Strings GetStringsInstance()
        {
            return new Properties.Strings();
        }

        private static ObjectDataProvider m_provider;
        public static ObjectDataProvider ResourceProvider
        {
            get
            {
                if (m_provider == null)
                    m_provider = (ObjectDataProvider)App.Current.FindResource("Strings");
                return m_provider;
            }
        }

        /// <summary>
        /// Change the current culture used in the application.
        /// If the desired culture is available all localized elements are updated.
        /// </summary>
        /// <param name="culture">Culture to change to</param>
        public static void ChangeCulture(CultureInfo culture)
        {
            CultureInfo.DefaultThreadCurrentUICulture = culture;
            System.Threading.Thread.CurrentThread.CurrentUICulture = culture;
            Properties.Strings.Culture = culture;
            ResourceProvider.Refresh();
        }
    }
}
