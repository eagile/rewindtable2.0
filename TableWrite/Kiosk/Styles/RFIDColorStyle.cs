﻿namespace Kiosk.Styles
{

    using KioskManagerLib.Models;
    using System;
    using System.Windows;
    using System.Windows.Controls;
    /// <summary>
    ///  Sets background of gridrow based on TID result
    /// </summary>
    public class RFIDColorStyle : StyleSelector
    {
        public Style DefaultColor { get; set; }
        public Style PassedColor { get; set; }
        public Style RejectedColor { get; set; }
        public Style NoResultColor { get; set; }
        public override Style SelectStyle(object item, DependencyObject container)
        {
            if (item.GetType() == typeof(KioskScannedItem))
            {
                var thing = ((KioskScannedItem)item).RlmStatus;
                if (thing != null && thing.ToUpper() == "PASSED")
                {
                    return PassedColor;
                }
                if (thing != null && thing.ToUpper() == "REJECTED")
                {
                    return RejectedColor;
                }
                if (thing != null && thing.ToUpper() == "NO RESULT")
                {
                    return NoResultColor;
                }
            }
            return DefaultColor;
        }
    }
}
