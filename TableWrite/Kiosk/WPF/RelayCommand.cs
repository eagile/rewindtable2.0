﻿// ***********************************************************************
// Assembly         : LineManager
// Author           : eAgile
// Created          : 01-28-2020
//
// Last Modified By : eAgile
// Last Modified On : 05-13-2020
// ***********************************************************************
// <copyright file="RelayCommand.cs" company="eAgile Inc">
//     eAgile Inc.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace EA.Helpers.WPF
{
    /// <summary>
    /// A command whose sole purpose is to relay its functionality to other
    /// objects by invoking delegates. The default return value for the CanExecute
    /// method is 'true'.
    /// </summary>
    public class RelayCommand : ICommand
    {
        /// <summary>
        /// The execute
        /// </summary>
        private readonly Action _execute;
        /// <summary>
        /// The execute asynchronous
        /// </summary>
        private readonly Func<Task> _executeAsync;
        /// <summary>
        /// The can execute
        /// </summary>
        private readonly Func<bool> _canExecute;
        /// <summary>
        /// The task
        /// </summary>
        private Task _task = Task.CompletedTask;
        private object v;
        private Func<bool> canSave;
        private readonly object _taskLock = new object();

       

        /// <summary>
        /// Creates a new command that can always execute.
        /// </summary>
        /// <param name="execute">The execution logic.</param>
        public RelayCommand(Action execute, object canSave) : this(execute, null) { }


        /// <summary>
        /// Creates a new command.
        /// </summary>
        /// <param name="execute">The execution logic.</param>
        /// <param name="canExecute">The execution status logic.</param>
        public RelayCommand(Action execute, Func<bool> canExecute = null)
        {
            _execute = execute;
            _executeAsync = null;
            _canExecute = canExecute;
        }

        /// <summary>
        /// Creates a new async command that can always execute.
        /// </summary>
        /// <param name="execute">The execution logic.</param>
        public RelayCommand(Func<Task> execute, object canSave) : this(execute, null) { }

        /// <summary>
        /// Creates a new async command.
        /// </summary>
        /// <param name="execute">The execution logic.</param>
        /// <param name="canExecute">The execution status logic.</param>
        public RelayCommand(Func<Task> execute, Func<bool> canExecute = null)
        {
            _execute = null;
            _executeAsync = execute;
            _canExecute = canExecute;
        }

        public RelayCommand(object v, Func<bool> canSave)
        {
            this.v = v;
            this.canSave = canSave;
        }



        /// <summary>
        /// Defines the method that determines whether the command can execute in its current state.
        /// </summary>
        /// <param name="parameter">Data used by the command.  If the command does not require data to be passed, this object can be set to <see langword="null" />.</param>
        /// <returns><see langword="true" /> if this command can be executed; otherwise, <see langword="false" />.</returns>
        public bool CanExecute(object parameter)
        {
            return CanExecute();
        }

        /// <summary>
        /// Determines whether this instance can execute.
        /// </summary>
        /// <returns><c>true</c> if this instance can execute; otherwise, <c>false</c>.</returns>
        private bool CanExecute()
        {
            return _task.IsCompleted && (_canExecute == null ? true : _canExecute());
        }

        /// <summary>
        /// Defines the method to be called when the command is invoked.
        /// </summary>
        /// <param name="parameter">Data used by the command.  If the command does not require data to be passed, this object can be set to <see langword="null" />.</param>
        public void Execute(object parameter)
        {
            if (_executeAsync != null)
            {
                lock (_taskLock)
                {
                    if (CanExecute())
                    {
                        _task = _executeAsync().ContinueWith(c => { var ignored = c.Exception; }, CancellationToken.None, TaskContinuationOptions.OnlyOnFaulted | TaskContinuationOptions.ExecuteSynchronously, TaskScheduler.Current);
                        Task refresh = _task.ContinueWith(antecedent => RefreshCanExecute(), CancellationToken.None, TaskContinuationOptions.ExecuteSynchronously, TaskScheduler.Current);
                    }
                }
            }
            else if (_execute != null)
            {
                if (CanExecute())
                {
                    _execute();
                }
            }
        }

        ///// <summary>
        ///// execute as an asynchronous operation.
        ///// </summary>
        //private async Task ExecuteAsync()
        //{
        //    try { await _executeAsync().ConfigureAwait(true); } catch { }
        //    //RefreshCanExecuteChanged();
        //}

        ///// <summary>
        ///// Raises the can execute changed.
        ///// </summary>
        //public static void RefreshCanExecuteChanged()
        //{
        //    try { CommandManager.InvalidateRequerySuggested(); } catch { }
        //    //RefreshCanExecute();
        //}

        /// <summary>
        /// Triggers CanExecute to be refreshed
        /// </summary>
        public static void RefreshCanExecute()
        {
            //CommandManager.InvalidateRequerySuggested();
            try
            {
                Action action = CommandManager.InvalidateRequerySuggested;
                Dispatcher dispatcher = Application.Current?.Dispatcher;
                if (dispatcher == null || dispatcher.CheckAccess())
                    action();
                else
                    dispatcher.BeginInvoke(action);
            }
            catch { }
        }

        /// <summary>
        /// Tied to CommandManager.RequerySuggested
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add
            {
                if (_canExecute != null)
                {
                    CommandManager.RequerySuggested += value;
                }
            }

            remove
            {
                if (_canExecute != null)
                {
                    CommandManager.RequerySuggested -= value;
                }
            }
        }
    }
}
