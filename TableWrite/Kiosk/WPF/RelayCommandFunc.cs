﻿// ***********************************************************************
// Assembly         : LineManager
// Author           : eAgile
// Created          : 01-28-2020
//
// Last Modified By : eAgile
// Last Modified On : 05-13-2020
// ***********************************************************************
// <copyright file="RelayCommandFunc.cs" company="eAgile Inc">
//     eAgile Inc.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace EA.Helpers.WPF
{
    /// <summary>
    /// A command whose sole purpose is to relay its functionality to other
    /// objects by invoking delegates. The default return value for the CanExecute
    /// method is 'true'.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class RelayCommand<T> : ICommand
    {
        /// <summary>
        /// The execute
        /// </summary>
        private readonly Action<T> _execute;
        /// <summary>
        /// The execute asynchronous
        /// </summary>
        private readonly Func<T, Task> _executeAsync;
        /// <summary>
        /// The can execute
        /// </summary>
        private readonly Func<T, bool> _canExecute;
        /// <summary>
        /// The task
        /// </summary>
        private Task _task = Task.CompletedTask;
        private readonly object _taskLock = new object();

        /// <summary>
        /// Creates a new command that can always execute.
        /// </summary>
        /// <param name="execute">The execution logic.</param>
        /// <param name="canPage">The can page.</param>
        public RelayCommand(Action<T> execute) : this(execute, null) { }

        /// <summary>
        /// Creates a new command.
        /// </summary>
        /// <param name="execute">The execution logic.</param>
        /// <param name="canExecute">The execution status logic.</param>
        public RelayCommand(Action<T> execute, Func<T, bool> canExecute = null)
        {
            _execute = execute;
            _executeAsync = null;
            _canExecute = canExecute;
        }

        /// <summary>
        /// Creates a new async command that can always execute.
        /// </summary>
        /// <param name="execute">The execution logic.</param>
        public RelayCommand(Func<T, Task> execute) : this(execute, null) { }

        /// <summary>
        /// Creates a new async command.
        /// </summary>
        /// <param name="execute">The execution logic.</param>
        /// <param name="canExecute">The execution status logic.</param>
        public RelayCommand(Func<T, Task> execute, Func<T, bool> canExecute = null)
        {
            _execute = null;
            _executeAsync = execute;
            _canExecute = canExecute;
        }

        /// <summary>
        /// Defines the method that determines whether the command can execute in its current state.
        /// </summary>
        /// <param name="parameter">Data used by the command.  If the command does not require data to be passed, this object can be set to <see langword="null" />.</param>
        /// <returns><see langword="true" /> if this command can be executed; otherwise, <see langword="false" />.</returns>
        public bool CanExecute(object parameter)
        {
            return CanExecute((T)parameter);
        }

        /// <summary>
        /// Determines whether this instance can execute the specified parameter.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns><c>true</c> if this instance can execute the specified parameter; otherwise, <c>false</c>.</returns>
        private bool CanExecute(T parameter)
        {
            return _task.IsCompleted && (_canExecute == null ? true : _canExecute(parameter));
        }

        /// <summary>
        /// Defines the method to be called when the command is invoked.
        /// </summary>
        /// <param name="parameter">Data used by the command.  If the command does not require data to be passed, this object can be set to <see langword="null" />.</param>
        public void Execute(object parameter)
        {
            if (_executeAsync != null)
            {
                lock (_taskLock)
                {
                    if (CanExecute(parameter))
                    {
                        _task = _executeAsync((T)parameter).ContinueWith(c => { var ignored = c.Exception; }, CancellationToken.None, TaskContinuationOptions.OnlyOnFaulted | TaskContinuationOptions.ExecuteSynchronously, TaskScheduler.Current);
                        Task refresh = _task.ContinueWith(antecedent => RefreshCanExecute(), CancellationToken.None, TaskContinuationOptions.ExecuteSynchronously, TaskScheduler.Current);
                    
                    }
                }
            }
            else if (_execute != null)
            {
                if (CanExecute(parameter))
                {
                    _execute((T)parameter);
                }
            }

        }

        /// <summary>
        /// execute as an asynchronous operation.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        //private async Task ExecuteAsync(T parameter)
        //{
        //    try { await _executeAsync(parameter).ConfigureAwait(true); } catch { }
        //    //RefreshCanExecuteChanged();
        //}

        ///// <summary>
        ///// Raises the can execute changed.
        ///// </summary>
        //public void RefreshCanExecuteChanged()
        //{
        //    //try { CommandManager.InvalidateRequerySuggested(); } catch { }
        //    RefreshCanExecute();
        //}

        /// <summary>
        /// Triggers CanExecute to be refreshed
        /// </summary>
        public static void RefreshCanExecute()
        {
            //CommandManager.InvalidateRequerySuggested();
            try
            {
                Action action = CommandManager.InvalidateRequerySuggested;
                Dispatcher dispatcher = Application.Current?.Dispatcher;
                if (dispatcher == null || dispatcher.CheckAccess())
                    action();
                else
                    dispatcher.BeginInvoke(action);
            }
            catch { }
        }

        /// <summary>
        /// Tied to CommandManager.RequerySuggested
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add
            {
                if (_canExecute != null)
                {
                    CommandManager.RequerySuggested += value;
                }
            }

            remove
            {
                if (_canExecute != null)
                {
                    CommandManager.RequerySuggested -= value;
                }
            }
        }
    }
}
