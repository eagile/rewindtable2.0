﻿using System.Threading.Tasks;

namespace EA.Helpers.WPF
{
    public interface IWindowModel
    {
        void OnLoaded();
        void OnClosing();
        void OnClosed();
    }

    public interface IWindowModelAsync
    {
        Task OnLoadedAsync();
        Task OnClosingAsync();
        Task OnClosedAsync();
    }
}
