﻿namespace RewindDriver
{
    using NLog;
    using System;
    using System.ComponentModel;
    using System.IO.Ports;

    public delegate void MotorCallBackFunction(UInt32 Attn);
    public enum Direction
    {
        OUT, // Enable the output motor only to pull material through production
        IN, // Enable the input motor only to reverse tag direction
        FEED // Feed using the IN motor to drive out for initial setup
    }
    public class MotorDriver : INotifyPropertyChanged
    {
        Logger Logger = LogManager.GetCurrentClassLogger();
        public bool Initialized { get; set; } = false;

        MotorCallBackFunction alertcallback;
        
        private uint move1;

        private uint move2;

        private SerialPort port;

        public bool Open(SerialPort serialport)
        {
            try
            {
                port = new SerialPort();
                port = serialport;
                // Setup ClearPath motors 

                port.DataReceived += Port_DataReceived;
                port.Open();
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return false;
            }

            port.RtsEnable = false; // Reset complete
            return true;
        }

        public bool Close()
        {
            try
            {
                port.DataReceived -= Port_DataReceived;
                port.Close();
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return false;
            }

            return true;
        }
        private void Port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            bool serial = false;
            bool motor = false;
            if (!Initialized)
            {
                while (port.BytesToRead > 0)
                {
                    string line = port.ReadLine();
                    Logger.Info(line);
                    if (line.StartsWith("Serial")) serial = true;
                    if (line.StartsWith("Driver")) motor = true;
                }
                if (serial && motor)
                {
                    Initialized = true;
                    NotifyPropertyChanged(nameof(Initialized));
                    Logger.Info("Initialized");
                }
            }
            else
            {
                while (port.BytesToRead > 0)
                {
                    string line = port.ReadLine();
                    Logger.Info(line);
                    if (line.Length == 1 || line[1] == ' ')
                    {
                        switch (line[0])
                        {
                            case 'A':
                                alertcallback('A');
                                break;
                            case 'B':
                                alertcallback('B');
                                break;
                            case 'C':
                                alertcallback('C');
                                break;
                            case 'S':
                                alertcallback('S');
                                break;
                        }
                    }
                }
            }
        }

        public void SetDIrection(Direction dir)
        {
            string data = string.Empty;
            if (dir == Direction.OUT)
            {
                data = "EN1 1";
                write(data);
                data = "EN2 0";
                write(data);
            }
            else if (dir == Direction.IN)
            {
                data = "EN1 0";
                write(data);
                data = "EN2 1";
                write(data);
                data = "D CC";
                write(data);
            }
            else if (dir == Direction.FEED)
            {
                data = "EN1 0";
                write(data);
                data = "EN2 1";
                write(data);
                data = "D CW";
                write(data);
            }
        }
        public void SetMove(uint distance, uint distance2)
        {
            move1 = distance;
         //   move2 = distance2;
        }

        /* <summary> Sets the distance for alerts as percentage of move
         * adistance and bdistance sets alert 1 and 2.  90=90% or move*.9
         */
        public void SetDistance(uint adistance, uint bdistance)
        {
            string data = $"A {adistance}";
            write(data);
            data = $"B {bdistance}";
            write(data);
        }

        public void Move()
        {
            string data = $"M {move1} M {5000}";
            write(data);
        }

        /* <summary> set internal delegate to call when data updates are received from the motor driver
         * 
         * */
        public void setCallback(MotorCallBackFunction cb)
        {
            alertcallback = cb;
        }

        private void write(string data)
        {
            try
            {
                port.WriteLine(data);
                Logger.Info(data);

            }
            catch { };

        }
        public void Stop()
        {
            string data = $"S";
            write(data);
        }
        /* <summary> sets the maximum calculated RPM the motor will achieve during a non timed move
         * This update will only take effect if the motor is stopped.  If the motor is moving it will take effect after stopping and starting a new move chain
         * This can be overridden with a SetTime call.
         * */
        public void SetMaxRPM(long rpm)
        {
            string data = $"R {rpm}";
            write(data);
        }

        /* <summary> sets the time the move should complete within, this overrides any previously set RPM setting.
         * This update will only take effect if the motor is stopped.  If the motor is moving it will take effect after stopping and starting a new move chain
         * Time defaults to 0 and is consrained by default RPM
         * */
        public void SetTime(long time = 0)
        {
            string data = $"T {time}";
            write(data);
        }

        private void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
