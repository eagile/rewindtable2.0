﻿namespace CompanyStandards.BaseModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    /// <summary>
    /// Class ObservableObject.
    /// Implements the <see cref="INotifyPropertyChanged" />
    /// </summary>
    /// <seealso cref="INotifyPropertyChanged" />
    public abstract class ObservableObject : INotifyPropertyChanged
    {
        #region SetProperty
        /// <summary>
        /// Sets the property.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="backingField">The backing field.</param>
        /// <param name="newValue">The new value.</param>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="dependentProperties">The dependent properties.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        protected virtual bool SetProperty<T>(ref T backingField, T newValue, [CallerMemberName] string propertyName = null, params string[] dependentProperties)
        {
            if (EqualityComparer<T>.Default.Equals(backingField, newValue))
            {
                return false;
            }

            backingField = newValue;

            OnPropertyChanged(propertyName, dependentProperties);

            return true;
        }
        #endregion SetProperty

        #region INotifyPropertyChanged
        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Called when [property changed].
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="propertyNames">The property names.</param>
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null, params string[] propertyNames)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            foreach (string p in propertyNames)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(p));
            }
        }
        /// <summary>
        /// Refreshes the properties.
        /// </summary>
        public virtual void RefreshProperties()
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(string.Empty));
        }
        #endregion INotifyPropertyChanged
    }

    /// <summary>
    /// Class ObservableEventArgs.
    /// Implements the <see cref="EventArgs" />
    /// Implements the <see cref="INotifyPropertyChanged" />
    /// </summary>
    /// <seealso cref="EventArgs" />
    /// <seealso cref="INotifyPropertyChanged" />
    public abstract class ObservableEventArgs : EventArgs, INotifyPropertyChanged
    {
        #region SetProperty
        /// <summary>
        /// Sets the property.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="backingField">The backing field.</param>
        /// <param name="newValue">The new value.</param>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="dependentProperties">The dependent properties.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        protected virtual bool SetProperty<T>(ref T backingField, T newValue, [CallerMemberName] string propertyName = null, params string[] dependentProperties)
        {
            if (EqualityComparer<T>.Default.Equals(backingField, newValue))
            {
                return false;
            }

            backingField = newValue;

            OnPropertyChanged(propertyName, dependentProperties);

            return true;
        }
        #endregion SetProperty

        #region INotifyPropertyChanged
        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Called when [property changed].
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="propertyNames">The property names.</param>
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null, params string[] propertyNames)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            foreach (string p in propertyNames)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(p));
            }
        }
        /// <summary>
        /// Refreshes the properties.
        /// </summary>
        public virtual void RefreshProperties()
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(string.Empty));
        }
        #endregion INotifyPropertyChanged
    }
}
