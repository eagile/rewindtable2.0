﻿using System;

namespace EADatabaseSDK.SupportClasses.Login
{
    public enum Right
    {
        mayReadRFID,
        mayViewSettings,
        mayEditSettings,
        mayEditUser,
        mayManageDatabase,
        mayViewLogs,
        mayExportLogs,
    }
}
