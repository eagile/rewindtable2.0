﻿using System;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Cryptography;
using System.Text;

namespace EADatabaseSDK.SupportClasses.Login
{
    public static class PasswordHash
    {
        public static string GetPasswordHash(string s)
        {
            using (MD5 hs = MD5.Create())
            {
                byte[] db = hs.ComputeHash(Encoding.UTF8.GetBytes(s));

                string result = Convert.ToBase64String(db);

                return result;
            }
        }

        public static string GetPasswordHash(SecureString ss)
        {
            // https://stackoverflow.com/questions/14293344/hashing-a-securestring-in-net
#pragma warning disable CA2000 // Dispose objects before losing scope
            using (MD5 hs = MD5.Create())
#pragma warning restore CA2000 // Dispose objects before losing scope
            {
                IntPtr bstr = Marshal.SecureStringToBSTR(ss);
                int length = Marshal.ReadInt32(bstr, -4);
                byte[] bytesUtf16 = new byte[length];
                var bytesUtf16Pin = GCHandle.Alloc(bytesUtf16, GCHandleType.Pinned);
                try
                {
                    Marshal.Copy(bstr, bytesUtf16, 0, length);
                    Marshal.ZeroFreeBSTR(bstr);
                    byte[] db = hs.ComputeHash(Encoding.UTF8.GetBytes(Encoding.Unicode.GetString(bytesUtf16)));
                    string result = Convert.ToBase64String(db);
                    return result;
                }
                finally
                {
                    Array.Clear(bytesUtf16, 0, bytesUtf16.Length);
                    bytesUtf16Pin.Free();
                }
            }
        }

    }
}
