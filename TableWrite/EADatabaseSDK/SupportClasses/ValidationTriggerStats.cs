﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace EADatabaseSDK.SupportClasses
{
    public enum ValidationResult
    {
        /// <summary>
        /// No Result (Orange) = 0 no results, empty query
        /// </summary>
        NoRead = 0,
        /// <summary>
        /// Passed (Green) = 1 found
        /// </summary>
        Success = 1,
        /// <summary>
        ///  Rejected (Red) =2  results but no 1
        /// </summary>
        WrongData = 2,
    }

    /// <summary>
    /// Bitwise validation error flags
    /// </summary>
    public enum ValidationFlags
    {
        /// <summary>
        /// Successful validation.
        /// </summary>
        NoError = 0,
        /// <summary>
        /// No tags were read within the trigger window.
        /// </summary>
        NoRead = 0x1,
        /// <summary>
        /// Multiple TIDs were read within the trigger window. Multiple tags on vial, improperly spaced vials, or bit flipping.
        /// </summary>
        MultipleTIDs = 0x2,
        /// <summary>
        /// TID on the tag is not 96 bits.
        /// </summary>
        InvalidTID = 0x4,
        /// <summary>
        /// EPC on the tag is not a valid SGTIN.
        /// </summary>
        InvalidEPC = 0x8,
        /// <summary>
        /// Depreciated: The EPC does not start the header specified in the current product settings.
        /// </summary>
        //WrongEPCHeader = 0x10,
        /// <summary>
        /// Serial number on the tag is not within valid range specified in the current product settings.
        /// </summary>
        InvalidSerial = 0x20,
        /// <summary>
        /// Multiple reads with the same TID have different EPCs. Bit flipping or duplicate TID.
        /// </summary>
        EPCMismatch = 0x40,
        /// <summary>
        /// Multiple reads with the same TID have different USER data. Bit flipping or duplicate TID.
        /// </summary>
        UserMismatch = 0x80,
        /// <summary>
        /// Multiple reads with the same TID have different RESERVED data. Bit flipping or duplicate TID.
        /// </summary>
        ReservedMismatch = 0x100,
        /// <summary>
        /// USER data could not be read at all.
        /// </summary>
        NoUser = 0x200,
        /// <summary>
        /// RESERVED data could not be read at all, RESERVED is needed to validate the USER lock state.
        /// </summary>
        NoReserved = 0x400,
        /// <summary>
        /// A GTIN could not be decoded from the tag data. EPC not a valid SGTIN or EPC could not be read.
        /// </summary>
        NoGTIN = 0x800,
        /// <summary>
        /// A batch number could not be decoded from the tag data. USER not written or only partially written, not a valid packed object, or USER could not be read.
        /// </summary>
        NoBatch = 0x1000,
        /// <summary>
        /// An expiry date could not be decoded from the tag data. USER not written or only partially written, not a valid packed object, or USER could not be read.
        /// </summary>
        NoExpiry = 0x2000,
        /// <summary>
        /// A serial number could not be decoded from the tag data. EPC not a valid SGTIN or EPC could not be read.
        /// </summary>
        NoSerial = 0x4000,
        /// <summary>
        /// The GTIN on the tag does not match the current product settings.
        /// </summary>
        WrongGTIN = 0x8000,
        /// <summary>
        /// The batch on the tag does not match the current order from PCE.
        /// </summary>
        WrongBatch = 0x10000,
        /// <summary>
        /// The expiry on the tag does not match the current order from PCE.
        /// </summary>
        WrongExpiry = 0x20000,
        /// <summary>
        /// Depreciated: The EPC serial does not match USER serial.
        /// </summary>
        //WrongSerial = 0x40000,
        /// <summary>
        /// The USER lock state in RESERVED did not match the product settings or RESERVED could not be read.
        /// </summary>
        WrongLockState = 0x80000,
        /// <summary>
        /// The TID on the tag was already seen during this batch
        /// </summary>
        DuplicateTID = 0x100000,
        /// <summary>
        /// The serial number on the tag was already seen during this batch
        /// </summary>
        DuplicateSerial = 0x200000,
        /// <summary>
        /// The PLC already rejected the tag before we could provide a result. The service was late providing a result.
        /// </summary>
        PlcRejectedResult = 0x400000,
        /// <summary>
        /// There was an error sending the result to the PLC. PLC offline or configured incorrectly.
        /// </summary>
        PlcError = 0x800000,
    }

}
