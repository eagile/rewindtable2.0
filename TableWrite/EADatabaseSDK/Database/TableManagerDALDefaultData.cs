﻿using EADatabaseSDK.SupportClasses.Login;
using KioskManagerLib.Database.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KioskManagerLib.Database
{
    public partial class TableManagerDAL
    {
        public async Task CreateDefaultData(CancellationToken ct = default)
        {
            string kadmin = "kadmin";
            string kuser = "kuser";
            string kmaint = "kmaint";

            List<Right> kadminRights = new List<Right>()
            {
                Right.mayReadRFID,
                Right.mayViewSettings,
                Right.mayEditSettings,
                Right.mayEditUser,
                Right.mayManageDatabase,
                Right.mayViewLogs,
                Right.mayExportLogs
            };
            List<Right> kuserRights = new List<Right>()
            {
                Right.mayReadRFID,
                Right.mayViewSettings,
                Right.mayViewLogs
            };
            List<Right> kmaintRights = new List<Right>()
            {
                Right.mayViewSettings,
                Right.mayEditSettings,
                Right.mayManageDatabase,
                Right.mayViewLogs,
                Right.mayExportLogs
            };


            List<Users> defaultUsers = new List<Users>()
            {
                new Users()
                {
                    StrName = kadmin,
                    StrDescription = "Kiosk admin",
                    DteLastpwdchange = DateTime.Now,
                },
                new Users()
                {
                    StrName = kuser,
                    StrDescription = "Kiosk user",
                    DteLastpwdchange = DateTime.Now,
                },
                new Users()
                {
                    StrName = kmaint,
                    StrDescription = "Kiosk maintenance",
                    DteLastpwdchange = DateTime.Now,
                },
            };

            List<Rights> defaultRights = new List<Rights>()
            {
                new Rights()
                {
                    StrName = Right.mayReadRFID.ToString(),
                    StrDescription = "User can perform RFID read, print RFID report",
                    BitValid = true,
                },
                new Rights()
                {
                    StrName = Right.mayViewSettings.ToString(),
                    StrDescription = "User can view settings but not change or print",
                    BitValid = true,
                },
                new Rights()
                {
                    StrName = Right.mayEditSettings.ToString(),
                    StrDescription = "User can change and print settings",
                    BitValid = true,
                },
                new Rights()
                {
                    StrName = Right.mayEditUser.ToString(),
                    StrDescription = "User can access User Management",
                    BitValid = true,
                },
                new Rights()
                {
                    StrName = Right.mayManageDatabase.ToString(),
                    StrDescription = "User can manage Kiosk database",
                    BitValid = true,
                },
                new Rights()
                {
                    StrName = Right.mayViewLogs.ToString(),
                    StrDescription = "User can view and filter logs/audit trail",
                    BitValid = true,
                },
                new Rights()
                {
                    StrName = Right.mayExportLogs.ToString(),
                    StrDescription = "User can print and save PDF of logs/audit trail",
                    BitValid = true,
                },
            };

            using (var db = GetKioskContext())
            {
                List<Users> existingUsers = await db.Users.AsNoTracking().Where(p => defaultUsers.Select(du => du.StrName).Contains(p.StrName)).ToListAsync(ct).ConfigureAwait(false);
                foreach (Users defaultUser in defaultUsers)
                {
                    if (!existingUsers.Any(p => p.StrName == defaultUser.StrName))
                    {
                        db.Users.Add(defaultUser);
                    }
                }
                List<Rights> existingRights = await db.Rights.AsNoTracking().Where(p => defaultRights.Select(dr => dr.StrName).Contains(p.StrName)).ToListAsync(ct).ConfigureAwait(false);
                foreach (Rights defaultRight in defaultRights)
                {
                    if (!existingUsers.Any(p => p.StrName == defaultRight.StrName))
                    {
                        db.Rights.Add(defaultRight);
                    }
                }
                await db.SaveChangesAsync(ct).ConfigureAwait(false);

                // reload users and rights to get the populated IDs
                existingUsers = await db.Users.AsNoTracking().Where(p => defaultUsers.Select(du => du.StrName).Contains(p.StrName)).ToListAsync(ct).ConfigureAwait(false);
                existingRights = await db.Rights.AsNoTracking().Where(p => defaultRights.Select(dr => dr.StrName).Contains(p.StrName)).ToListAsync(ct).ConfigureAwait(false);

                List<UsersRights> defaultUserRights = new List<UsersRights>();

                Users kadminUser = existingUsers.FirstOrDefault(p => p.StrName == kadmin);
                Users kuserUser = existingUsers.FirstOrDefault(p => p.StrName == kuser);
                Users kmaintUser = existingUsers.FirstOrDefault(p => p.StrName == kmaint);

                if (kadminUser != null)
                    defaultUserRights.AddRange(existingRights.Where(p => kadminRights.Select(p => p.ToString()).Contains(p.StrName)).Select(p => new UsersRights() { IdUser = kadminUser.Id, IdRight = p.Id }));

                if (kuserUser != null)
                    defaultUserRights.AddRange(existingRights.Where(p => kuserRights.Select(p => p.ToString()).Contains(p.StrName)).Select(p => new UsersRights() { IdUser = kuserUser.Id, IdRight = p.Id }));

                if (kmaintUser != null)
                    defaultUserRights.AddRange(existingRights.Where(p => kmaintRights.Select(p => p.ToString()).Contains(p.StrName)).Select(p => new UsersRights() { IdUser = kmaintUser.Id, IdRight = p.Id }));

                List<UsersRights> existingUserRights = await db.UsersRights.AsNoTracking().Where(p => existingUsers.Select(dr => dr.Id).Contains(p.IdUser)).ToListAsync(ct).ConfigureAwait(false);
                foreach (UsersRights defaultUserRight in defaultUserRights)
                {
                    if (!existingUserRights.Any(p => p.IdUser == defaultUserRight.IdUser && p.IdRight == defaultUserRight.IdRight))
                    {
                        db.UsersRights.Add(defaultUserRight);
                    }
                }
                await db.SaveChangesAsync(ct).ConfigureAwait(false);
            }
        }
    }
}
