﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KioskManagerLib.Database.Models
{
    public class PartAttributes
    {
        public long Id { get; set; }
        public string PartId { get; set; }
        public string PartHeader { get; set; }
    }
}
