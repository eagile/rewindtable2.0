﻿using System;
using System.Collections.Generic;

namespace KioskManagerLib.Database.Models
{
    public partial class UsersGroups
    {
        public long IdUser { get; set; }
        public long IdGroup { get; set; }

        public virtual Groups IdGroupNavigation { get; set; }
        public virtual Users IdUserNavigation { get; set; }
    }
}
