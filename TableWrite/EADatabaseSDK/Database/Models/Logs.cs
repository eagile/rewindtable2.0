﻿using System;
using System.Collections.Generic;

namespace KioskManagerLib.Database.Models
{
    public partial class Logs
    {
        public Guid Id { get; set; }
        public Guid? OrderId { get; set; }
        public DateTime LogTime { get; set; }
        public string User { get; set; }
        public string Line { get; set; }
        public string OrderName { get; set; }
        public string Message { get; set; }
        public int Code { get; set; }
        public string LogLevel { get; set; }
        public int SyncStatus { get; set; }

        public string FormattedLogTime
        {
            get
            {
                return String.Format("{0:yyyy-MM-dd HH:mm:ss}", LogTime);
            }
        }

    }
}
