﻿using System;
using System.Collections.Generic;

namespace KioskManagerLib.Database.Models
{
    public partial class Users
    {
        public Users()
        {
            UsersGroups = new HashSet<UsersGroups>();
            UsersRights = new HashSet<UsersRights>();
        }

        public long Id { get; set; }
        public string StrName { get; set; }
        public string StrDescription { get; set; }
        public string StrPassword { get; set; }
        public DateTime? DteExpires { get; set; }
        public DateTime DteLastpwdchange { get; set; }
        public DateTime? DteLastfailedlogin { get; set; }
        public int IntFailedlogincount { get; set; }
        public bool BitLocked { get; set; }
        public bool? BitValid { get; set; }

        public virtual ICollection<UsersGroups> UsersGroups { get; set; }
        public virtual ICollection<UsersRights> UsersRights { get; set; }
    }
}
