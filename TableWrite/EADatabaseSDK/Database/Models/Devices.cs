﻿using System;
using System.Collections.Generic;

namespace KioskManagerLib.Database.Models
{
    public partial class Devices
    {
        public Guid Id { get; set; }
        public Guid LineId { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public bool Active { get; set; }
        public int SyncStatus { get; set; }
        public int Sortby { get; set; }
    }
}
