﻿using System;
using System.Collections.Generic;

namespace KioskManagerLib.Database.Models
{
    public partial class GroupsRights
    {
        public long IdGroup { get; set; }
        public long IdRight { get; set; }

        public virtual Groups IdGroupNavigation { get; set; }
        public virtual Rights IdRightNavigation { get; set; }
    }
}
