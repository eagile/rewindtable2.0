﻿using System;
using System.Collections.Generic;

namespace KioskManagerLib.Database.Models
{
    public partial class UsersRights
    {
        public long IdUser { get; set; }
        public long IdRight { get; set; }

        public virtual Rights IdRightNavigation { get; set; }
        public virtual Users IdUserNavigation { get; set; }
    }
}
