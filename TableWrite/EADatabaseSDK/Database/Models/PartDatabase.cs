﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KioskManagerLib.Database.Models
{
    public class PartDatabase
    {
        public long Id { get; set; }
        public string PartId { get; set; }
        public string DatabaseFile { get; set; }
    }
}
