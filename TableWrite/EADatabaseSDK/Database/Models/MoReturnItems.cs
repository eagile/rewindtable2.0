﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KioskManagerLib.Database.Models
{
    public class MoReturnItems
    {
        public string Items_ID { get; set; }
        public string Item_Part { get; set; }
        public string Item_Job { get; set; }
        public string Parts_DBb1 { get; set; }
        public int Parts_ItemSize { get; set; }
        public string Parts_Class { get; set; }
    }
}
