﻿namespace KioskManagerLib.Database
{
    
    using KioskManagerLib.Database.DataContexts;
    using KioskManagerLib.Database.Models;
   
    using Microsoft.Data.SqlClient;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Query.SqlExpressions;
    using Microsoft.EntityFrameworkCore.Storage;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    /// <summary>
    /// RemoteKioskManagerDAL is used to connect to remote RLM line and view last result of a TID.
    /// </summary>
    public partial class ProductionDAL
    {
        /// <summary>
        ///  Remote DB connection string
        /// </summary>
        public bool ProductionConnectionStringExists => ProductionContext.ConnectionStringExists;

        /// <summary>
        /// Remote DB Context . contains dbset of lines related to TID. 
        /// </summary>
        /// <returns></returns>
        private ProductionContext GetProductionContext()
        {
            return new ProductionContext();
        }

        /// <summary>
        /// Constuctor may need to build out.
        /// </summary>
        public ProductionDAL()
        {
            
        }

        /// <summary>
        ///  Get all lines. not sure if will be used.
        /// </summary>
        /// <param name="ct"></param>
        /// <returns></returns>
        public async Task<ProductionPartWrapper> GetPartAttribute(string partnumber,CancellationToken ct)
        {
            using var db = GetProductionContext();
            ProductionPartWrapper ProductionInfo = new ProductionPartWrapper()
            {
                PartProductionAttrib = new PartAttributes(),
                PartProductionDB = new PartDatabase(),
            }; 
            ProductionInfo.PartProductionAttrib = db.PartAttributes.FirstOrDefault(p => p.PartId == partnumber);
            ProductionInfo.PartProductionDB =db.PartDatabase.FirstOrDefault(p => p.PartId == partnumber);

            return ProductionInfo;
        }

    }

}
