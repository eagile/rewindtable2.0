﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using KioskManagerLib.Database.Models;

namespace KioskManagerLib.Database.DataContexts
{
    public partial class RemoteTableContext : DbContext
    {
        public static string ConnectionString { private get; set; }
        public static bool ConnectionStringExists => !string.IsNullOrEmpty(ConnectionString);

        public RemoteTableContext()
        {
        }

        public RemoteTableContext(DbContextOptions<RemoteTableContext> options)
            : base(options)
        {

        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured && ConnectionStringExists)
            {
                optionsBuilder.UseMySQL(ConnectionString);
            }
        }

        public virtual DbSet<ValidationTriggerData> ValidationTriggerData{ get; set; }

        
    
    }
}
