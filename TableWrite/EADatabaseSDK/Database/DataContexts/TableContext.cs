﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using KioskManagerLib.Database.Models;

namespace KioskManagerLib.Database.DataContexts
{
    public partial class TableContext : DbContext
    {
        public static string ConnectionString { private get; set; }
        public static bool ConnectionStringExists => !string.IsNullOrEmpty(ConnectionString);

        public TableContext()
        {
        }

        public TableContext(DbContextOptions<TableContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Devices> Devices { get; set; }
        public virtual DbSet<Groups> Groups { get; set; }
        public virtual DbSet<GroupsRights> GroupsRights { get; set; }
        public virtual DbSet<Lines> Lines { get; set; }
        public virtual DbSet<Logs> Logs { get; set; }
        public virtual DbSet<Rights> Rights { get; set; }
        public virtual DbSet<Settings> Settings { get; set; }
        public virtual DbSet<Skus> Skus { get; set; }
        public virtual DbSet<Users> Users { get; set; }
        public virtual DbSet<UsersGroups> UsersGroups { get; set; }
        public virtual DbSet<UsersRights> UsersRights { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured && ConnectionStringExists)
            {
                optionsBuilder.UseSqlServer(ConnectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Devices>(entity =>
            {
                entity.ToTable("devices");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.Active).HasColumnName("active");

                entity.Property(e => e.LineId).HasColumnName("line_id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(64);

                entity.Property(e => e.Sortby).HasColumnName("sortby");

                entity.Property(e => e.SyncStatus).HasColumnName("syncStatus");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnName("type")
                    .HasMaxLength(64);
            });

            modelBuilder.Entity<Groups>(entity =>
            {
                entity.ToTable("groups");

                entity.HasIndex(e => e.StrName)
                    .HasName("groups_uq")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.BitValid)
                    .IsRequired()
                    .HasColumnName("bit_valid")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.StrDescription)
                    .HasColumnName("str_description")
                    .HasMaxLength(255);

                entity.Property(e => e.StrName)
                    .IsRequired()
                    .HasColumnName("str_name")
                    .HasMaxLength(64);
            });

            modelBuilder.Entity<GroupsRights>(entity =>
            {
                entity.HasKey(e => new { e.IdGroup, e.IdRight })
                    .HasName("groups_rights_pk");

                entity.ToTable("groups_rights");

                entity.Property(e => e.IdGroup).HasColumnName("id_group");

                entity.Property(e => e.IdRight).HasColumnName("id_right");

                entity.HasOne(d => d.IdGroupNavigation)
                    .WithMany(p => p.GroupsRights)
                    .HasForeignKey(d => d.IdGroup)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("groups_rights_fk_id_group");

                entity.HasOne(d => d.IdRightNavigation)
                    .WithMany(p => p.GroupsRights)
                    .HasForeignKey(d => d.IdRight)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("groups_rights_fk_id_right");
            });

            modelBuilder.Entity<Lines>(entity =>
            {
                entity.ToTable("lines");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.Active).HasColumnName("active");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(64);

                entity.Property(e => e.SyncStatus).HasColumnName("syncStatus");
            });

            modelBuilder.Entity<Logs>(entity =>
            {
                entity.ToTable("logs");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.Code).HasColumnName("code");

                entity.Property(e => e.Line)
                    .IsRequired()
                    .HasColumnName("line")
                    .HasMaxLength(64);

                entity.Property(e => e.LogLevel)
                    .IsRequired()
                    .HasColumnName("logLevel")
                    .HasMaxLength(10);

                entity.Property(e => e.LogTime)
                    .HasColumnName("logTime")
                    .HasColumnType("datetime");

                entity.Property(e => e.Message)
                    .HasColumnName("message")
                    .HasMaxLength(2000);

                entity.Property(e => e.OrderId).HasColumnName("orderId");

                entity.Property(e => e.OrderName)
                    .HasColumnName("orderName")
                    .HasMaxLength(64);

                entity.Property(e => e.SyncStatus).HasColumnName("syncStatus");

                entity.Property(e => e.User)
                    .HasColumnName("user")
                    .HasMaxLength(64);
            });

            modelBuilder.Entity<Rights>(entity =>
            {
                entity.ToTable("rights");

                entity.HasIndex(e => e.StrName)
                    .HasName("tbl_rights_uq")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.BitValid)
                    .IsRequired()
                    .HasColumnName("bit_valid")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.StrDescription)
                    .HasColumnName("str_description")
                    .HasMaxLength(255);

                entity.Property(e => e.StrName)
                    .IsRequired()
                    .HasColumnName("str_name")
                    .HasMaxLength(64);
            });

            modelBuilder.Entity<Settings>(entity =>
            {
                entity.ToTable("settings");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.Active).HasColumnName("active");

                entity.Property(e => e.DeviceId).HasColumnName("device_id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(64);

                entity.Property(e => e.SkuId).HasColumnName("sku_id");

                entity.Property(e => e.Sortby).HasColumnName("sortby");

                entity.Property(e => e.SyncStatus).HasColumnName("syncStatus");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnName("type")
                    .HasMaxLength(64);

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnName("value")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<Skus>(entity =>
            {
                entity.ToTable("skus");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.Active).HasColumnName("active");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(64);

                entity.Property(e => e.SyncStatus).HasColumnName("syncStatus");
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.ToTable("users");

                entity.HasIndex(e => e.StrName)
                    .HasName("tbl_users_uq")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.BitLocked).HasColumnName("bit_locked");

                entity.Property(e => e.BitValid)
                    .IsRequired()
                    .HasColumnName("bit_valid")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.DteExpires)
                    .HasColumnName("dte_expires")
                    .HasColumnType("datetime");

                entity.Property(e => e.DteLastfailedlogin)
                    .HasColumnName("dte_lastfailedlogin")
                    .HasColumnType("datetime");

                entity.Property(e => e.DteLastpwdchange)
                    .HasColumnName("dte_lastpwdchange")
                    .HasColumnType("datetime");

                entity.Property(e => e.IntFailedlogincount).HasColumnName("int_failedlogincount");

                entity.Property(e => e.StrDescription)
                    .HasColumnName("str_description")
                    .HasMaxLength(255);

                entity.Property(e => e.StrName)
                    .IsRequired()
                    .HasColumnName("str_name")
                    .HasMaxLength(64);

                entity.Property(e => e.StrPassword)
                    .HasColumnName("str_password")
                    .HasMaxLength(128);
            });

            modelBuilder.Entity<UsersGroups>(entity =>
            {
                entity.HasKey(e => new { e.IdUser, e.IdGroup })
                    .HasName("users_groups_pk");

                entity.ToTable("users_groups");

                entity.Property(e => e.IdUser).HasColumnName("id_user");

                entity.Property(e => e.IdGroup).HasColumnName("id_group");

                entity.HasOne(d => d.IdGroupNavigation)
                    .WithMany(p => p.UsersGroups)
                    .HasForeignKey(d => d.IdGroup)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("users_groups_fk_id_group");

                entity.HasOne(d => d.IdUserNavigation)
                    .WithMany(p => p.UsersGroups)
                    .HasForeignKey(d => d.IdUser)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("users_groups_fk_id_user");
            });

            modelBuilder.Entity<UsersRights>(entity =>
            {
                entity.HasKey(e => new { e.IdUser, e.IdRight })
                    .HasName("rel_users_rights_pk");

                entity.ToTable("users_rights");

                entity.Property(e => e.IdUser).HasColumnName("id_user");

                entity.Property(e => e.IdRight).HasColumnName("id_right");

                entity.HasOne(d => d.IdRightNavigation)
                    .WithMany(p => p.UsersRights)
                    .HasForeignKey(d => d.IdRight)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("users_rights_fk_id_right");

                entity.HasOne(d => d.IdUserNavigation)
                    .WithMany(p => p.UsersRights)
                    .HasForeignKey(d => d.IdUser)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("users_rights_fk_id_user");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
