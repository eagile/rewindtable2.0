﻿using EA.Impinj;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace EATags
{
    
  [Serializable()]
    public class FK4425Model: EntityModel
    {
        public override string IDNumber { get; set; }

        [XmlElement]
        public string EPC { get; set; }
        [XmlElement]
        public TagLockState EPC_LOCK { get; set; }
        [XmlElement]
        public string USER { get; set; }
        [XmlElement]
        public TagLockState USER_LOCK { get; set; }
        [XmlElement]
        public string ACCESS { get; set; }
        [XmlElement]
        public TagLockState ACCESS_LOCK { get; set; }
        [XmlElement]
        public string KILL { get; set; }
        [XmlElement]
        public TagLockState KILL_LOCK { get; set; }
        [XmlElement]
        public string PRINT1 { get; set; }
        [XmlElement]
        public string PRINT2 { get; set; }
        [XmlElement]
        public string PRINT3 { get; set; }
        [XmlElement]
        public string PRINT4 { get; set; }
        [XmlElement]
        public string PRINT5 { get; set; }

        public override string TagName
        {
            get { return "4425"; }
        }

        public override IModel GetNewModel()
        {
            return new FK4425Model();
        }

        //public override TagOpSequence BuildReadOps(string tid, DataBaseEntry dataentry)
        //{
        //    TagOpSequence newop = new TagOpSequence();

        //    return newop;
        //}

        //public override TagOpSequence BuildWriteOps(TagInfo tag, string tid, DataBaseEntry dataentry)
        //{
        //}   

        //public override TagInfo TagOpProcess(Tag tag, TagInfo ti, DataBaseEntry resultsdata, DataBaseEntry dataentry)
        //{

        //}
    }
}
