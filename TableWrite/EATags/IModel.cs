﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EATags
{
    public interface IModel
    {
      
        IModel GetNewModel();
      
        void UpdateModelData(IList<string> valueList);
      
        string IDNumber { get; set; }
        string TagName { get; }
      
      
    }
}
