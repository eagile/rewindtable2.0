﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EATags
{
    [Serializable]
    public abstract class EntityModel : IModel
    {
       
        public abstract string IDNumber { get; set; }

        public abstract string TagName { get;}

        public abstract IModel GetNewModel();
        public void UpdateModelData(IList<string> valueList)
        {
            throw new NotImplementedException();
        }
        //public abstract TagOpSequence BuildReadOps(string tid, DataBaseEntry dataentry);
        //public abstract TagOpSequence BuildWriteOps(TagInfo tag, string tid, DataBaseEntry dataentry);
        //public abstract TagInfo TagOpProcess(Tag tag, TagInfo ti, DataBaseEntry resultsdata, DataBaseEntry dataentry);

    }
}
