﻿using EA.Impinj;
using System;
using System.Collections.Generic;
using System.Text;

namespace EATags
{
    public class TagFactory
    {
        internal static Dictionary<string, Type> TagData = new Dictionary<string, Type>
        {
            { "4425", typeof(FK4425Model) },
            { "4423", typeof(FK4425Model) },
        };
        public static Type GetType(string e)
        {
            return TagData[e];

        }
        private static TagLockState lockstate(string lockstring)
        {
            if (lockstring.ToUpper().Contains("PERMALOCK"))
            {
                //temp
                return TagLockState.Lock;
            }
            else if (lockstring.ToUpper().Contains("?") || string.IsNullOrEmpty(lockstring))
            {
                return TagLockState.None;
            }
            else if (lockstring.ToUpper().Contains("PERMAUNLOCK"))
            {
                return TagLockState.Permaunlock;
            }
            else if (lockstring.ToUpper().Contains("UNLOCK"))
            {
                return TagLockState.Unlock;
            }
            else if (lockstring.ToUpper().StartsWith("LOCK"))
            {
                return TagLockState.Lock;
            }
            return TagLockState.None;
        }


        public static object SetValue(string e, string strLine)
        {
            switch (e)
            {
                case "4425":
                    var strValues = strLine.Split('|');
                    var testc = new FK4425Model
                    {
                        EPC = strValues[0],
                        EPC_LOCK = lockstate(strValues[1].ToString()),
                        USER = strValues[2],
                        USER_LOCK = lockstate(strValues[3].ToString()),
                        ACCESS = strValues[4],
                        ACCESS_LOCK = lockstate(strValues[5].ToString()),
                        KILL = strValues[6],
                        KILL_LOCK = lockstate(strValues[7].ToString()),
                        PRINT1 = strValues[8],
                        PRINT2 = strValues[9],
                        PRINT3 = strValues[10],
                        PRINT4 = strValues[11],
                        PRINT5 = strValues[12]
                    };
                    return testc;
                case "4423":
                    return new object();
            };

            return new object();
        }


    }
}
