namespace EA.Impinj
{
    /// <summary>
    /// Enum defining the possible Impinj reader autostop modes.
    /// </summary>
    public enum AutoStopMode
    {
        /// <summary>No autostop mode specified.</summary>
        None,
        /// <summary>Autostop defined by a timeout.</summary>
        Duration,
        /// <summary>Autostop defined by a GPI trigger value.</summary>
        GpiTrigger,
    }
}
