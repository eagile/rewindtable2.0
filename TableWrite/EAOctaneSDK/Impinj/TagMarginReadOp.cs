using System;

namespace EA.Impinj
{
    /// <summary>Used to perform a margin read operation</summary>
    public class TagMarginReadOp : TagOp
    {
        /// <summary>
        /// Tag access password. Required only if an access
        /// password has been set for the target memory bank.
        /// </summary>
        public TagData AccessPassword
        {
            get => _accessPassword;
            set => _accessPassword = value ?? new TagData();
        }
        private TagData _accessPassword = new TagData();

        /// <summary>Tag memory bank to perform margin read.</summary>
        public MemoryBank MemoryBank { get; set; }

        /// <summary>Numeric pointer to first bit to read.</summary>
        public ushort BitPointer { get; set; }

        /// <summary>A mask, which specifies the mask to look for.</summary>
        public MarginReadMask MarginMask
        {
            get => _marginMask;
            set => _marginMask = value ?? new MarginReadMask();
        }
        private MarginReadMask _marginMask = new MarginReadMask();



        public TagMarginReadOp() : this(GetNextTagOpID()) { }
        public TagMarginReadOp(UInt16 tagOpId) : base(tagOpId) { }
    }
}
