namespace EA.Impinj
{
    /// <summary>
    /// Specialization of <see cref="T:Impinj.OctaneSdk.TagOpResult" /> class that contains the
    /// results of an individual tag write operation, as reported in the
    /// <see cref="T:Impinj.OctaneSdk.TagOpReport" /> parameter of a
    /// <see cref="E:Impinj.OctaneSdk.ImpinjReader.TagOpComplete" /> event.
    /// </summary>
    public class TagWriteOpResult : TagOpResult
    {
        /// <summary>
        /// The number of 16-bit words written by the tag write operation.
        /// </summary>
        public ushort NumWordsWritten { get; set; }

        /// <summary>The result of the tag write operation.</summary>
        public WriteResultStatus Result { get; set; }

        /// <summary>
        /// Indicates whether the tag write was a standard or BlockWrite
        /// operation.
        /// </summary>
        public bool IsBlockWrite { get; set; }
    }
}
