namespace EA.Impinj
{
    /// <summary>
    /// Class used to define the configuration for a reader with direction or location role capabilities.
    /// </summary>
    public class SpatialConfig
    {
        /// <summary>
        /// Tracks the mode (Inventory, Location, or Direction) the reader with direction or location role capabilities is currently in.
        /// </summary>
        public SpatialMode Mode { get; set; }

        /// <summary>Contains the reader's placement configuration.</summary>
        public PlacementConfig Placement { get; set; }

        /// <summary>
        /// Contains the reader's location configuration. This should only be used
        /// </summary>
        public LocationConfig Location { get; set; }

        /// <summary>Contains the reader's direction configuration.</summary>
        public DirectionConfig Direction { get; set; }

        public SpatialConfig()
        {
            Placement = new PlacementConfig();
            Location = new LocationConfig();
            Direction = new DirectionConfig();
        }
    }
}
