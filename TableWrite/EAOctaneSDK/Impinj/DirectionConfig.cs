using System.Collections.Generic;

namespace EA.Impinj
{
    /// <summary>
    /// Class used to define the direction configuration for a reader with direction or location role capabilities.
    /// </summary>
    public class DirectionConfig
    {
        private static readonly ushort DEFAULT_TAG_AGE_INTERVAL_SECONDS = 2;
        private static readonly ushort DEFAULT_UPDATE_INTERVAL_SECONDS = 2;
        private static readonly DirectionFieldOfView DEFAULT_FIELD_OF_VIEW = DirectionFieldOfView.ReaderSelected;
        private static readonly double DEFAULT_TX_POWER_IN_DBM = 30.0;

        /// <summary>
        /// Adds the list of sectors that should be monitored for tag entrances, exits, and updates.
        /// </summary>
        public List<ushort> EnabledSectorIDs { get; set; }

        /// <summary>
        /// The "age interval" specifies how long a tag needs to go unseen before
        /// it's removed from the population of tags tracked for direction and an
        /// exit report is sent.
        /// </summary>
        public ushort TagAgeIntervalSeconds { get; set; }

        /// <summary>
        /// Specifies how often update reports are sent when updates are enabled.
        /// This method sets that interval to the desired value in seconds.
        /// </summary>
        public ushort UpdateIntervalSeconds { get; set; }

        /// <summary>
        /// When update reports are enabled, direction reports are sent by the reader
        /// periodically for all tags in the direction database
        /// </summary>
        public bool UpdateReportEnabled { get; set; }

        /// <summary>
        /// When enabled, an entry report is sent immediately upon the tag
        /// entering the field of view of the reader.
        /// </summary>
        public bool EntryReportEnabled { get; set; }

        /// <summary>
        /// When enabled, an exit report is generated when the tag has not been
        /// seen by the reader in the tag age Interval.
        /// </summary>
        public bool ExitReportEnabled { get; set; }

        /// <summary>Sets the field of view for tag direction tracking.</summary>
        public DirectionFieldOfView FieldOfView { get; set; }

        /// <summary>
        /// The performance/sensitivity mode enabled for this direction reporting session.
        /// </summary>
        public DirectionMode Mode { get; set; }

        /// <summary>Enables or disables the diagnostic report.</summary>
        public ushort TagPopulationLimit { get; set; }

        /// <summary>Enables or disables the diagnostic report.</summary>
        public bool DiagnosticReportEnabled { get; set; }

        /// <summary>
        /// If true, the reader will make antennas use their maximum transmit power.
        /// If false, the TxPowerInDbm value will be used as the antenna transmit power.
        /// </summary>
        public bool MaxTxPower { get; set; }

        /// <summary>Sets the antenna transmit power in dbm</summary>
        public double TxPowerInDbm { get; set; }

        public DirectionConfig()
        {
            EnabledSectorIDs = new List<ushort>();
            TagAgeIntervalSeconds = DEFAULT_TAG_AGE_INTERVAL_SECONDS;
            UpdateIntervalSeconds = DEFAULT_UPDATE_INTERVAL_SECONDS;
            UpdateReportEnabled = true;
            EntryReportEnabled = true;
            ExitReportEnabled = true;
            FieldOfView = DEFAULT_FIELD_OF_VIEW;
            Mode = DirectionMode.HighPerformance;
            DiagnosticReportEnabled = false;
            MaxTxPower = true;
            TxPowerInDbm = DEFAULT_TX_POWER_IN_DBM;
        }
    }
}
