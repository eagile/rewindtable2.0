namespace EA.Impinj
{
    /// <summary>
    /// Enumeration used to determine whether to configure the tag in reduced sensitivity
    /// (i.e. Short Range) mode, or not.
    /// </summary>
    public enum QtAccessRange
    {
        /// <summary>Leave tag access range as-is.</summary>
        Unknown,
        /// <summary>Normal-Range tells tag to use normal sensitivity.</summary>
        NormalRange,
        /// <summary>
        /// Short-Range tells tag to reduce sensitivity, adding a layer of
        /// physical security by preventing readers farther than roughly one
        /// meter from the tag from switching the tag from Public to Private
        /// (or vice versa).
        /// </summary>
        ShortRange,
    }
}
