namespace EA.Impinj
{
    /// <summary>
    /// Specialization of <see cref="T:Impinj.OctaneSdk.TagOpResult" /> class that contains the
    /// results of an individual tag kill operation, as reported in the
    /// <see cref="T:Impinj.OctaneSdk.TagOpReport" /> parameter of a
    /// <see cref="E:Impinj.OctaneSdk.ImpinjReader.TagOpComplete" /> event.
    /// </summary>
    public class TagKillOpResult : TagOpResult
    {
        /// <summary>The result of the tag kill operation.</summary>
        public KillResultStatus Result { get; set; }
    }
}
