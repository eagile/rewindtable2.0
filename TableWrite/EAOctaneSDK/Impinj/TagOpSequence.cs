using System;
using System.Collections.Generic;
using System.Threading;

namespace EA.Impinj
{
    /// <summary>
    /// Class for containing and configuring a sequence of tag operations
    /// that are submitted to the reader using the
    /// <see cref="M:Impinj.OctaneSdk.ImpinjReader.AddOpSequence(Impinj.OctaneSdk.TagOpSequence)" /> method.
    /// </summary>
    public class TagOpSequence
    {
        /// <summary>
        /// Identifies the target tag(s) on which to perform this tag operation
        /// sequence.
        /// </summary>
        public TargetTag TargetTag { get; set; }

        /// <summary>The list of tag operations to perform.</summary>
        public List<TagOp> Ops { get; set; } = new List<TagOp>();

        /// <summary>
        /// Definition of trigger that will cause this operating sequence to stop.
        /// </summary>
        public SequenceTriggerType SequenceStopTrigger { get; set; } = SequenceTriggerType.ExecutionCount;

        /// <summary>
        /// The unique ID for this sequence. A unique ID is automatically assigned
        /// by the class constructor.
        /// </summary>
        public uint Id { get; set; }

        /// <summary>
        /// The Antenna ID to which this sequence will be applied.
        /// Antennas start at antenna 1, and for all antennas use 0.
        /// </summary>
        public ushort AntennaId { get; set; }

        /// <summary>
        /// Identifies whether the sequence is disabled or active.
        /// </summary>
        public SequenceState State { get; set; } = SequenceState.Active;

        /// <summary>
        /// When the SequenceStopTrigger member variable is set to ExecutionCount,
        /// this parameter identifies the number of times this sequence of
        /// operations will execute before it gets deleted.
        /// A value of zero (0) means that the sequence never gets deleted.
        /// </summary>
        public ushort ExecutionCount { get; set; } = 1;

        /// <summary>
        /// Specify the number of times a tag operation will be automatically retried by the Reader before failure is declared.
        /// </summary>
        public ushort RetryCount { get; set; }

        /// <summary>Flag whether BlockWrites should be used, or not.</summary>
        public bool BlockWriteEnabled { get; set; }

        /// <summary>
        /// Specify whether 16 or 32-bit BlockWrites should be used.
        /// </summary>
        public ushort BlockWriteWordCount { get; set; }

        /// <summary>
        /// Constructor; automatically assigns a unique ID number.
        /// </summary>
        public TagOpSequence() : this(GetNextTagOpSequenceID()) { }
        public TagOpSequence(UInt32 tagOpId)
        {
            Id = tagOpId;
        }

        private static int _tagOpSequenceIdSequence = 0;
        public static UInt32 GetNextTagOpSequenceID()
        {
            UInt32 nextId;
            do
            {
                nextId = unchecked((UInt32)Interlocked.Increment(ref _tagOpSequenceIdSequence));
            } while (nextId == 0);
            return nextId;
        }
    }
}
