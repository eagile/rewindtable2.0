namespace EA.Impinj
{
    /// <summary>
    /// Class for defining the trigger conditions that will prompt an Impinj
    /// reader to automatically stop its current operation. Even when an
    /// auto-stop trigger is set, the Reader may be explicitly stopped using
    /// the <see cref="M:Impinj.OctaneSdk.ImpinjReader.Stop" /> method.
    /// </summary>
    public class AutoStopConfig
    {
        /// <summary>
        /// Indicates whether and how the Reader should automatically stop;
        /// either None, Duration or GpiTrigger.
        /// </summary>
        public AutoStopMode Mode { get; set; }

        /// <summary>
        /// When using Duration mode, this is period for which the Reader
        /// singulates.
        /// </summary>
        public uint DurationInMs { get; set; }

        /// <summary>GPI port to monitor for a stop event.</summary>
        public ushort GpiPortNumber { get; set; }

        /// <summary>
        /// When using GpiTrigger mode, and when the GPI Port changes to this
        /// level, then the reader stops. True for high, false for low.
        /// </summary>
        public bool GpiLevel { get; set; }

        /// <summary>
        /// This is the longest the Reader will singulate even if the GPI does
        /// not change. A value of 0 means there is no time limit.
        /// </summary>
        public uint Timeout { get; set; }
    }
}
