namespace EA.Impinj
{
    /// <summary>
    /// Class for configuring the Impinj Reader Low Duty Cycle reader capability
    /// </summary>
    public class LowDutyCycleSettings
    {
        /// <summary>
        /// Flag indicating whether the reader Low Duty Cycle feature is
        /// enabled.
        /// </summary>
        public bool IsEnabled { get; set; }

        /// <summary>
        /// Specifies in milliseconds the time the Reader will wait before entering
        /// low duty cycle mode.
        /// </summary>
        public ushort EmptyFieldTimeoutInMs { get; set; }

        /// <summary>
        /// Specifies in milliseconds how frequently the Reader will rescan the
        /// field of view for tags.
        /// </summary>
        public ushort FieldPingIntervalInMs { get; set; }
    }
}
