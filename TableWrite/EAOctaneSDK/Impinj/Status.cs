namespace EA.Impinj
{
    /// <summary>
    /// Class encapsulating the reader status information, as returned by
    /// <see cref="M:Impinj.OctaneSdk.ImpinjReader.QueryStatus" />.
    /// </summary>
    public class Status
    {
        /// <summary>Collection of antenna status information.</summary>
        public AntennaStatusGroup Antennas { get; set; }

        /// <summary>Collection of GPI port status information.</summary>
        public GpiStatusGroup Gpis { get; set; }

        /// <summary>Collection of antenna hub status information.</summary>
        public AntennaHubStatusGroup AntennaHubs { get; set; }

        /// <summary>The reader connection status.</summary>
        public bool IsConnected { get; set; }

        /// <summary>
        /// Temperature reading from on-board reader temperature sensor, in Celsius.
        /// </summary>
        public short TemperatureInCelsius { get; set; }

        /// <summary>
        /// Holds the X, Y, and Z values returned by the tilt sensor.
        /// </summary>
        public TiltSensorValue TiltSensor { get; set; }

        /// <summary>The reader inventory status.</summary>
        public bool IsSingulating { get; set; }

        public Status()
        {
            Antennas = new AntennaStatusGroup();
            Gpis = new GpiStatusGroup();
            AntennaHubs = new AntennaHubStatusGroup();
        }
    }
}
