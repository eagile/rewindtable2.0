namespace EA.Impinj
{
    /// <summary>
    /// Session
    /// </summary>
    public enum Session
    {
        /// <summary>
        /// Session 0
        /// </summary>
        S0 = 0,
        /// <summary>
        /// Session 1
        /// </summary>
        S1 = 1,
        /// <summary>
        /// Session 2
        /// </summary>
        S2 = 2,
        /// <summary>
        /// Session 3
        /// </summary>
        S3 = 3,
    }
}
