using System;

namespace EA.Impinj
{
    /// <summary>
    /// Class used to encapsulate the details of a
    /// <see cref="E:Impinj.OctaneSdk.ImpinjReader.AntennaChanged" /> reader event.
    /// </summary>
    public class AntennaEvent : EventArgs
    {
        /// <summary>Timestamp of when the host received the event.</summary>
        public DateTime HostReceivedTime { get; set; }

        /// <summary>Timestamp of the event.</summary>
        public DateTime ReaderTimestamp { get; set; }

        /// <summary>Port number of the affected antenna.</summary>
        public ushort PortNumber { get; set; }

        /// <summary>State to which antenna changed, prompting the event.</summary>
        public AntennaEventType State { get; set; }
    }
}
