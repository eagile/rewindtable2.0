using EA.Impinj.LLRP;
using System;
using System.Collections.Generic;

namespace EA.Impinj
{
    /// <summary>Uses to specify which blocks to permalock.</summary>
    public class BlockPermalockMask
    {
        /// <summary>Gets the block permalock mask.</summary>
        public UInt16List Mask { get; }

        public BlockPermalockMask()
        {
            Mask = new UInt16List();
        }

        private BlockPermalockMask(IList<ushort> blocks)
        {
            Mask = new UInt16List(blocks);
        }

        /// <summary>
        /// Creates a block permalock mask from the specified block.
        /// </summary>
        /// <param name="block"></param>
        /// <returns>A block permalock mask</returns>
        public static BlockPermalockMask FromBlockNumber(ushort block)
        {
            return FromBlockNumberArray(new ushort[1] { block });
        }

        /// <summary>
        /// Creates a block permalock mask from an array of block numbers.
        /// </summary>
        /// <param name="blocks"></param>
        /// <returns>A block permalock mask</returns>
        public static BlockPermalockMask FromBlockNumberArray(ushort[] blocks)
        {
            ushort val2 = 0;

            foreach (ushort block in blocks)
                val2 = Math.Max(block, val2);

            ushort[] numArray = new ushort[val2 / 16 + 1];

            foreach (ushort block in blocks)
            {
                ushort num = (ushort)(block / 16U);
                numArray[num] |= (ushort)(32768 >> block - num * 16);
            }
            return new BlockPermalockMask(numArray);
        }

        /// <summary>
        /// Converts the block permalock mask into a hexadecimal string.
        /// </summary>
        /// <returns></returns>
        public string ToHexString() => LLRPUtil.UInt16ListToHexString(Mask);
    }
}
