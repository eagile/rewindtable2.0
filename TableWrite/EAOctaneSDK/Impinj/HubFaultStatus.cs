namespace EA.Impinj
{
    /// <summary>Enum defining the antenna hub fault options.</summary>
    public enum HubFaultStatus
    {
        /// <summary>No known antenna hub fault exists.</summary>
        No_Fault,
        /// <summary>
        /// A non-specific RF power distribution error has occurred.
        /// </summary>
        RF_Power,
        /// <summary>
        /// An RF power distribution error has occurred; RF power is present
        /// on Hub 1.
        /// </summary>
        RF_Power_On_Hub_1,
        /// <summary>
        /// An RF power distribution error has occurred; RF power is present
        /// on Hub 2.
        /// </summary>
        RF_Power_On_Hub_2,
        /// <summary>
        /// An RF power distribution error has occurred; RF power is present
        /// on Hub 3.
        /// </summary>
        RF_Power_On_Hub_3,
        /// <summary>
        /// An RF power distribution error has occurred; RF power is present
        /// on Hub 4.
        /// </summary>
        RF_Power_On_Hub_4,
        /// <summary>The hub has not yet been initialized.</summary>
        No_Init,
        /// <summary>
        /// A serial communications error has occurred between the reader and
        /// the hub controller.
        /// </summary>
        Serial_Overflow,
        /// <summary>The antenna hub is disconnected.</summary>
        Disconnected,
    }
}
