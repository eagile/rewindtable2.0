using System;

namespace EA.Impinj
{
    /// <summary>xArray location report</summary>
    public class LocationReport : EventArgs
    {
        /// <summary>Timestamp of when the host received the event.</summary>
        public DateTime HostReceivedTime { get; set; }

        /// <summary>Contents of the tag EPC memory bank.</summary>
        public TagData Epc { get; }

        /// <summary>The time stamp of the location event</summary>
        public DateTime Timestamp { get; set; }

        /// <summary>The X location in centimeters</summary>
        public int LocationXCm { get; set; }

        /// The Y location in centimeters
        public int LocationYCm { get; set; }

        /// <summary>
        /// The Confidence field contains confidence that the event occurred in the form of a percentage.
        /// 0 is not confident. 100 is perfectly confident.
        /// </summary>
        public LocationConfidenceFactors ConfidenceFactors { get; set; }

        /// <summary>The location report type.</summary>
        public LocationReportType ReportType { get; set; }

        public LocationReport(TagData epc)
        {
            Epc = epc ?? throw new ArgumentNullException(nameof(epc));
        }
    }
}
