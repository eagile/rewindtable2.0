namespace EA.Impinj
{
    /// <summary>
    /// Helper class used to manage GPS coordinates within a tag object.
    /// </summary>
    public class GpsCoordinates
    {
        /// <summary>Parameter for storing Latitude data.</summary>
        public double Latitude { get; set; }

        /// <summary>Parameter for storing Longitude data.</summary>
        public double Longitude { get; set; }

        /// <summary>
        /// Override of the ToString operator that concatenates the
        /// Latitude and Longitude parameters into a single string.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return $"{Latitude},{Longitude}";
        }
    }
}
