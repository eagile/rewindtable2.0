﻿namespace EA.Impinj
{
    /// <summary>
    /// Class for containing a tag select for a single inventory filter
    /// </summary>
    public class TagSelectFilter
    {
        /// <summary>
        /// The tag mask defines the bit pattern that the filter must match on.
        /// The mask should be expressed as a hex string.
        /// </summary>
        public string TagMask { get; set; }

        /// <summary>
        /// The bit offset in the specified memory bank at which the tag mask
        /// begins. It is important to note that this is a bit offset and need
        /// not be word or even byte-aligned.
        /// </summary>
        public ushort BitPointer { get; set; }

        /// <summary>
        /// The length of the mask in bits.
        /// If no length is specified, the entire mask is used.
        /// </summary>
        public int BitCount { get; set; }
        /// <summary>
        /// The memory bank on which the filter is applied. Filters may be
        /// configured to search for content in the Epc, Tid, and User memory
        /// banks. Filters may not match against the Reserved memory bank.
        /// </summary>
        public MemoryBank MemoryBank { get; set; }

        /// <summary>The action to perform when the tag filter matches.</summary>
        public StateUnawareAction MatchAction { get; set; }

        /// <summary>
        /// The action to perform when the tag filter does not match.
        /// </summary>
        public StateUnawareAction NonMatchAction { get; set; }
    }
}
