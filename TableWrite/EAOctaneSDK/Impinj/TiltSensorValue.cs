namespace EA.Impinj
{
    /// <summary>
    /// Holds the X, Y, and Z values returned by the tilt sensor.
    /// </summary>
    public class TiltSensorValue
    {
        /// <summary>X axis value in degrees returned by the tilt sensor</summary>
        public int XAxis { get; set; }

        /// <summary>Y axis value in degrees returned by the tilt sensor</summary>
        public int YAxis { get; set; }
    }
}
