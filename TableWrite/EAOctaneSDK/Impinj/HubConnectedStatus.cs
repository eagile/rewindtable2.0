namespace EA.Impinj
{
    /// <summary>
    /// Enum defining the antenna hub connection status options.
    /// </summary>
    public enum HubConnectedStatus
    {
        /// <summary>The antenna hub is in an unknown state.</summary>
        Unknown,
        /// <summary>The antenna hub is disconnected.</summary>
        Disconnected,
        /// <summary>The antenna hub is connected.</summary>
        Connected,
    }
}
