using System;
using System.Threading;

namespace EA.Impinj
{
    /// <summary>Parent class for tag operation definitions</summary>
    public abstract class TagOp
    {
        /// <summary>The tag operation unique identifier.</summary>
        public ushort Id { get; set; }

        /// <summary>
        /// Constructor; automatically assigns a unique ID number.
        /// </summary>
        protected TagOp() : this(GetNextTagOpID()) { }
        protected TagOp(UInt16 tagOpId)
        {
            Id = tagOpId;
        }

        private static int _tagOpIdSequence = 0;
        public static UInt16 GetNextTagOpID()
        {
            UInt16 nextId;
            do
            {
                nextId = unchecked((UInt16)Interlocked.Increment(ref _tagOpIdSequence));
            } while (nextId == 0);
            return nextId;
        }
    }
}
