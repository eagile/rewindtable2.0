using System.Collections.Generic;

namespace EA.Impinj
{
    /// <summary>
    /// Contains the confidence factors for a location report.
    /// </summary>
    public class LocationConfidenceFactors
    {
        /// <summary>The number of reads used to calculate the location.</summary>
        public ushort ReadCount { get; set; }

        /// <summary>Additional confidence factors. Internal use only.</summary>
        public List<uint> Data { get; set; }
    }
}
