using System.Collections.Generic;

namespace EA.Impinj
{
    /// <summary>
    /// Container class encapsulating all of the GPI configuration objects.
    /// This is for internal use only, and does not appear in the generated
    /// documentation.
    /// </summary>
    public class GpiConfigGroup : List<GpiConfig>
    {
        /// <summary>
        /// The GPIO Port number scheme is not 0 indexed. The first port is "1", which this value represents.
        /// </summary>
        public static int GPI_PORT_START_INDEX { get; } = 1;

        private readonly bool _createdBySerializer;

        public GpiConfigGroup()
        {
            _createdBySerializer = true;
        }

        /// <summary>
        /// Creates a new instance of the GpiConfigGroup class that is
        /// initialized with the specified number of GPI objects.
        /// </summary>
        /// <param name="numGpis">The number of GPI configuration objects to create.</param>
        public GpiConfigGroup(ushort numGpis) : base(numGpis)
        {
            _createdBySerializer = false;
            for (int i = GPI_PORT_START_INDEX; i < GPI_PORT_START_INDEX + numGpis; i++)
            {
                base.Add(new GpiConfig((ushort)i));
            }
        }

        /// <summary>
        /// Adds the provided GPI configuration object to the collection.
        /// For internal library use only.
        /// </summary>
        /// <param name="config">The GpiConfig object to add.</param>
        public new void Add(GpiConfig config)
        {
            if (!_createdBySerializer)
                throw new OctaneSdkException("Illegal operation - cannot Add to GPIConfigGroup object!");
            base.Add(config);
        }

        /// <summary>Enable all GPIs in the collection.</summary>
        public void EnableAll()
        {
            foreach (GpiConfig gpiConfig in this)
                gpiConfig.IsEnabled = true;
        }

        /// <summary>Disable all GPIs in the collection.</summary>
        public void DisableAll()
        {
            foreach (GpiConfig gpiConfig in this)
                gpiConfig.IsEnabled = false;
        }

        /// <summary>The number of GPIs in the collection.</summary>
        public int Length => Count;

        /// <summary>Returns the settings for the specified GPI port.</summary>
        /// <returns>The GPI settings or throws an exception if the port does not exist.</returns>
        public GpiConfig GetGpi(ushort portNumber)
        {
            foreach (GpiConfig gpiConfig in this)
            {
                if (gpiConfig.PortNumber == portNumber)
                    return gpiConfig;
            }
            throw new OctaneSdkException("Invalid GPI port number specified.");
        }
    }
}
