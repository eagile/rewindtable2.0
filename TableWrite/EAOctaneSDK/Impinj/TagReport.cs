using System;
using System.Collections.Generic;

namespace EA.Impinj
{
    /// <summary>
    /// Container class used to encapsulate individual tag details returned
    /// from the reader.
    /// </summary>
    public class TagReport : EventArgs
    {
        /// <summary>Timestamp of when the host received the event.</summary>
        public DateTime HostReceivedTime { get; set; }

        /// <summary>A list of tag details.</summary>
        public List<Tag> Tags { get; } = new List<Tag>();

    }
}
