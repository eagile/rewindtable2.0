using System;

namespace EA.Impinj
{
    /// <summary>Used to perform a block permalock operation.</summary>
    public class TagBlockPermalockOp : TagOp
    {
        /// <summary>
        /// Tag access password.  Required only if an access
        /// password has been set for the target memory bank.
        /// </summary>
        public TagData AccessPassword
        {
            get => _accessPassword;
            set => _accessPassword = value ?? new TagData();
        }
        private TagData _accessPassword = new TagData();

        /// <summary>
        /// Parameter defining Memory Bank to access for operation.
        /// </summary>
        public MemoryBank MemoryBank { get; set; }

        /// <summary>
        /// Specifies the starting address for BlockMask in units of 16 blocks.
        /// </summary>
        public ushort BlockPointer { get; set; }

        /// <summary>A mask, which specifies the blocks to permalock.</summary>
        public BlockPermalockMask BlockMask
        {
            get => _blockMask;
            set => _blockMask = value ?? new BlockPermalockMask();
        }
        private BlockPermalockMask _blockMask = new BlockPermalockMask();



        public TagBlockPermalockOp() : this(GetNextTagOpID()) { }
        public TagBlockPermalockOp(UInt16 tagOpId) : base(tagOpId) { }
    }
}
