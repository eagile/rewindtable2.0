using System.Collections.Generic;

namespace EA.Impinj
{
    /// <summary>
    /// Class for containing all the settings necessary for a reader to begin
    /// singulating. It is a composite class consisting of other composite
    /// classes containing individual settings, and is consumed by the
    /// following methods:
    /// <see cref="M:Impinj.OctaneSdk.ImpinjReader.ApplySettings(Impinj.OctaneSdk.Settings)" />,
    /// and produced by the following methods:
    /// <see cref="M:Impinj.OctaneSdk.ImpinjReader.QuerySettings" />,
    /// <see cref="M:Impinj.OctaneSdk.ImpinjReader.QueryDefaultSettings" />,
    /// </summary>
    public class ImpinjSettings
    {
        /// <summary>
        /// The conditions in which an Impinj reader will automatically start
        /// operation.
        /// </summary>
        public AutoStartConfig AutoStart { get; set; }

        /// <summary>
        /// The conditions in which an Impinj reader will automatically stop
        /// operation.
        /// </summary>
        public AutoStopConfig AutoStop { get; set; }

        /// <summary>The selected reader mode for this configuration.</summary>
        public ReaderMode ReaderMode { get; set; }

        /// <summary>The selected tag search mode for this configuration.</summary>
        public SearchMode SearchMode { get; set; }

        /// <summary>
        /// Session number (0 - 3) to use for the inventory operation for this
        /// configuration.
        /// </summary>
        public Session Session { get; set; }

        /// <summary>
        /// An estimate of the tag population in view of the RF field of the antenna.
        /// </summary>
        public ushort TagPopulationEstimate { get; set; }

        /// <summary>
        /// The settings for the reader Low Duty Cycle feature, which optionally
        /// reduces RF activity.
        /// </summary>
        public LowDutyCycleSettings LowDutyCycle { get; set; }

        /// <summary>
        /// The settings for defining any tag filters that the reader must use to
        /// select a portion of the tag population to participate in singulation.
        /// </summary>
        public FilterSettings Filters { get; set; }

        public TruncatedReplySettings TruncatedReply { get; set; }

        /// <summary>
        /// Set how tags are reported and select optional report fields.
        /// </summary>
        public ReportConfig Report { get; set; }

        /// <summary>
        /// Per-antenna settings: power, sensitivity, etc. may be iterated with
        /// foreach or subscripted by antenna port number.
        /// </summary>
        public AntennaConfigGroup Antennas { get; set; }

        /// <summary>
        /// Enable general purpose input (GPI) events on specific GPI ports.
        /// May be iterated with foreach or subscripted by GPI port number.
        /// </summary>
        public GpiConfigGroup Gpis { get; set; }

        /// <summary>
        /// Enable general purpose output (GPO) events on specific GPO ports.
        /// May be iterated with foreach or subscripted by GPO port number.
        /// </summary>
        public GpoConfigGroup Gpos { get; set; }

        /// <summary>
        /// Optionally cause the reader to send a keep-alive message periodically.
        /// </summary>
        public KeepaliveConfig Keepalives { get; set; }

        /// <summary>
        /// Indicates whether the reader has been configured to hold reports
        /// and events; i.e. the reader events and reports parameter
        /// HoldEventsAndReportsUponReconnect is set to true.
        /// </summary>
        public bool HoldReportsOnDisconnect { get; set; }

        /// <summary>
        /// Transmit frequencies to use in regions that allow it.
        /// An empty list means the reader chooses.
        /// </summary>
        public List<double> TxFrequenciesInMhz { get; set; }

        /// <summary>Reduced power frequencies</summary>
        public List<double> ReducedPowerFrequenciesInMhz { get; set; }

        /// <summary>
        /// Configuration object for a reader with direction or location role capabilities
        /// </summary>
        public SpatialConfig SpatialConfig { get; set; }

        /// <summary>Default Constructor</summary>
        public ImpinjSettings()
        {
            AutoStart = new AutoStartConfig();
            AutoStop = new AutoStopConfig();
            Keepalives = new KeepaliveConfig();
            Report = new ReportConfig();
            Filters = new FilterSettings();
            LowDutyCycle = new LowDutyCycleSettings();
            TruncatedReply = new TruncatedReplySettings();
            TxFrequenciesInMhz = new List<double>();
            ReducedPowerFrequenciesInMhz = new List<double>();
            SpatialConfig = new SpatialConfig();
        }

        public ImpinjSettings(ushort numberOfAntennas, ushort numberOfGpis, ushort numberOfGpos) : this()
        {
            Antennas = new AntennaConfigGroup(numberOfAntennas)
            {
                EnableIntelligentAntennaManagement = false
            };
            Gpis = new GpiConfigGroup(numberOfGpis);
            Gpos = new GpoConfigGroup(numberOfGpos);
            AutoStart.Mode = AutoStartMode.None;
            AutoStop.Mode = AutoStopMode.None;
            ReaderMode = ReaderMode.DenseReaderM4;
            SearchMode = SearchMode.DualTarget;
            Session = Session.S2;
            TagPopulationEstimate = 32;
            Report.IncludeAntennaPortNumber = false;
            Report.IncludeChannel = false;
            Report.IncludeFirstSeenTime = false;
            Report.IncludeLastSeenTime = false;
            Report.IncludePeakRssi = false;
            Report.IncludeSeenCount = false;
            Report.IncludeFastId = false;
            Report.IncludePhaseAngle = false;
            Report.IncludeDopplerFrequency = false;
            Report.IncludeGpsCoordinates = false;
            Report.IncludePcBits = false;
            Report.IncludeCrc = false;
            Report.Mode = ReportMode.Individual;
            Report.BufferMode = ReportBufferMode.Normal;
            Keepalives.Enabled = false;
            Keepalives.EnableLinkMonitorMode = false;
            LowDutyCycle.IsEnabled = false;
            LowDutyCycle.EmptyFieldTimeoutInMs = 500;
            LowDutyCycle.FieldPingIntervalInMs = 200;
            AutoStart.Mode = AutoStartMode.None;
            Filters.Mode = TagFilterMode.None;
            Filters.TagFilter1.BitCount = 0;
            Filters.TagFilter2.BitCount = 0;
            TruncatedReply.IsEnabled = false;
            TxFrequenciesInMhz.Clear();
            ReducedPowerFrequenciesInMhz.Clear();
            HoldReportsOnDisconnect = false;
            SpatialConfig.Mode = SpatialMode.Inventory;
            SpatialConfig.Placement.HeightCm = 400;
            SpatialConfig.Placement.FacilityXLocationCm = 0;
            SpatialConfig.Placement.FacilityYLocationCm = 0;
            SpatialConfig.Placement.OrientationDegrees = 0;
            SpatialConfig.Location.EntryReportEnabled = true;
            SpatialConfig.Location.ExitReportEnabled = true;
            SpatialConfig.Location.UpdateReportEnabled = true;
            SpatialConfig.Location.DiagnosticReportEnabled = false;
            SpatialConfig.Location.ComputeWindowSeconds = 10;
            SpatialConfig.Location.UpdateIntervalSeconds = 5;
            SpatialConfig.Location.TagAgeIntervalSeconds = 20;
            SpatialConfig.Direction.EntryReportEnabled = true;
            SpatialConfig.Direction.ExitReportEnabled = true;
            SpatialConfig.Direction.UpdateReportEnabled = true;
            SpatialConfig.Direction.UpdateIntervalSeconds = 5;
            SpatialConfig.Direction.TagAgeIntervalSeconds = 20;
            SpatialConfig.Direction.FieldOfView = DirectionFieldOfView.Wide;
            SpatialConfig.Direction.MaxTxPower = true;
        }
    }
}
