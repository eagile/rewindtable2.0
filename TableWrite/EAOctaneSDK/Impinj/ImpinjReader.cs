using EA.Impinj.LLRP;
using EA.Impinj.LLRP.Impinj;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EA.Impinj
{
    /// <summary>
    /// The main class for controlling and configuring an Impinj RFID reader.
    /// </summary>
    public class ImpinjReader : IDisposable
    {
        protected readonly ImpinjLLRPClientOctane Client;

        /// <summary>
        /// Indicates whether or not a connection to the reader exists.
        /// </summary>
        public bool IsConnected => Client.IsConnected;

        /// <summary>Contains the IP address or hostname of the reader.</summary>
        /// <remarks>Read only.</remarks>
        public string Address { get => Client.Address; set => Client.Address = value; }

        public int Port { get => Client.Port; set => Client.Port = value; }

        public bool UseTLS { get => Client.UseTLS; set => Client.UseTLS = value; }

        /// <summary>
        /// Stream read buffer size
        /// </summary>
        public int BufferSize { get => Client.BufferSize; set => Client.BufferSize = value; }

        /// <summary>
        /// Controls how many active commands can be set to the reader at a time
        /// </summary>
        public int PendingRequestLimit { get => Client.PendingRequestLimit; set => Client.PendingRequestLimit = value; }

        /// <summary>
        /// The connection timeout in milliseconds.
        /// If a connection to the reader cannot be established in this time, an exception is thrown.
        /// </summary>
        public int ConnectTimeout { get => Client.ConnectTimeout; set => Client.ConnectTimeout = value; }

        /// <summary>The message reply timeout.</summary>
        public int ResponseTimeout { get => Client.ResponseTimeout; set => Client.ResponseTimeout = value; }

        /// <summary>The close connection timeout.</summary>
        public int CloseTimeout { get => Client.CloseTimeout; set => Client.CloseTimeout = value; }

        /// <summary>
        /// Assigns a name to this reader. For example, "Dock Door #1 Reader".
        /// This name can be used to identify a particular reader in an application
        /// that controls multiple readers.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Returns whether the connected reader is an xArray or not.
        /// </summary>
        public bool IsXArray => Client.IsXArray;

        /// <summary>
        /// Returns whether the connected reader is an xSpan or not.
        /// </summary>
        public bool IsXSpan => Client.IsXSpan;

        /// <summary>
        /// Returns whether or not the connected reader is a reader with direction or location role capabilities.
        /// </summary>
        public bool IsSpatialReader => Client.IsSpatialReader;

        public FeatureSet ReaderCapabilities => Client.ReaderCapabilities;



        public ImpinjReader() : this(null, LLRPConstants.LLRP_PORT, false) { }
        public ImpinjReader(string address) : this(address, LLRPConstants.LLRP_PORT, false) { }
        public ImpinjReader(string address, int port) : this(address, port, false) { }
        public ImpinjReader(string address, bool useTls) : this(address, useTls ? LLRPConstants.LLRP_PORT_ENCRYPTED : LLRPConstants.LLRP_PORT, useTls) { }
        public ImpinjReader(string address, int port, bool useTls) : base()
        {
            Client = new ImpinjLLRPClientOctane();
            Client.ConnectionChanged += Client_ConnectionChanged;
            Client.MessageReceived += Client_MessageReceived;

            Address = address;
            Port = port;
            UseTLS = useTls;
        }







        /// <summary>
        /// Connect to an Impinj RFID reader.
        /// </summary>
        public Task ConnectAsync() => ConnectAsync(Address, Port, UseTLS);

        /// <summary>
        /// Connect to an Impinj RFID reader.
        /// </summary>
        /// <param name="address">IP address or hostname of the target reader.</param>
        public Task ConnectAsync(string address) => ConnectAsync(address, Port, UseTLS);

        /// <summary>Connect to an Impinj RFID reader.</summary>
        /// <param name="address">IP address or hostname of the target reader.</param>
        /// <param name="port">TCP/IP port number used by the target reader.</param>
        /// <param name="useTLS">Enable TLS encryption if "true".</param>
        public async Task ConnectAsync(string address, int port, bool useTls)
        {
            await Client.ConnectAsync(address, port, useTls, ConnectTimeout).ConfigureAwait(false);
        }

        public Task<bool> DisconnectAsync() => DisconnectAsync(null);

        /// <summary>Closes the connection to the reader.</summary>
        public async Task<bool> DisconnectAsync(Exception error)
        {
            bool result = false;
            try { result = await Client.DisconnectAsync(error).ConfigureAwait(false); } catch { }
            return result;
        }




        /// <summary>
        /// Starts the reader. Tag reports will be received asynchronously via an event.
        /// </summary>
        /// <exception cref="T:Impinj.OctaneSdk.OctaneSdkException">
        /// Thrown if the Start method is called before connecting to a reader.
        /// </exception>
        public Task StartAsync()
        {
            if (!IsConnected)

                throw new InvalidOperationException("Reader disconnected");

            return Client.StartAsync();
        }

        /// <summary>Stops the reader. Tags will no longer be read.</summary>
        public Task StopAsync()
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            return Client.StopAsync();
        }

        /// <summary>
        /// Tells the reader to send all the tag reports it has buffered.
        /// The ReportMode should be set so that reader accumulates tag reads
        /// (WaitForQuery or BatchAfterStop).
        /// </summary>
        /// <exception cref="T:Impinj.OctaneSdk.OctaneSdkException">
        /// Thrown when this method is called prior to establishing a connection
        /// with a reader.</exception>
        public Task QueryTagsAsync()
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            return Client.GetTagReportsAsync();
        }

        /// <summary>
        /// This function tells the reader to resume sending reports and events.
        /// Messages in the queue on the reader may then be sent.
        /// </summary>
        /// <exception cref="T:Impinj.OctaneSdk.OctaneSdkException">Thrown if a communication error
        /// occurs while talking with the reader.</exception>
        public Task ResumeEventsAndReportsAsync()
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            return Client.ResumeEventsAndReportsAsync();
        }

        /// <summary>Turns the xArray beacon light on.</summary>
        /// <param name="duration">
        /// The period in milliseconds to blink the beacon light.
        /// </param>
        public Task TurnBeaconOnAsync(ulong duration)
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            return Client.EnableBeaconAsync(duration);
        }

        /// <summary>Turns the xArray beacon light off.</summary>
        public Task TurnBeaconOffAsync()
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            return Client.DisableBeaconAsync();
        }

        /// <summary>
        /// This function is used to set the value of a GPO signal to the
        /// specified value. The output is set to high when state is true,
        /// and low when state is false.
        /// </summary>
        /// <param name="port">GPO port to set</param>
        /// <param name="state">Value to set GPO to</param>
        /// <exception cref="T:Impinj.OctaneSdk.OctaneSdkException">Thrown if a communication error
        /// occurs while talking with the reader.</exception>
        public Task SetGpoAsync(ushort port, bool state)
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            return Client.SetGpoAsync(port, state);
        }

        /// <summary>Returns the reader default settings.</summary>
        /// <returns>The default settings of the reader.</returns>
        /// <exception cref="T:Impinj.OctaneSdk.OctaneSdkException">
        /// Thrown when this method is called prior to establishing a connection with a reader.
        /// </exception>
        public ImpinjSettings QueryDefaultSettings()
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            FeatureSet readerCapabilities = Client.ReaderCapabilities ?? throw new InvalidOperationException("Reader disconnected");
            ImpinjSettings settings = new ImpinjSettings(readerCapabilities.AntennaCount, readerCapabilities.GpiCount, readerCapabilities.GpoCount);
            return settings;
        }

        /// <summary>Applies the default settings to the reader.</summary>
        /// <exception cref="T:Impinj.OctaneSdk.OctaneSdkException">
        /// Thrown if this method is called
        /// prior to establishing a connection with a reader.
        /// </exception>
        public Task ApplyDefaultSettingsAsync()
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            return ApplySettingsAsync(QueryDefaultSettings());
        }

        /// <summary>
        /// This function queries the reader for its current settings.
        /// </summary>
        /// <returns>An object containing the current reader settings</returns>
        /// <exception cref="T:Impinj.OctaneSdk.OctaneSdkException">Thrown if a communication error
        /// occurs while talking with the reader, the reader has not
        /// been configured, or if the configuration was not applied
        /// by the SDK.</exception>
        /// <remarks>
        /// When querying a SpatialReader that has most recently been used
        /// in either Direction or Location mode, not all configuration
        /// data may be available. In their place, certain defaults may be
        /// present. One example is RfMode and Session when the reader has
        /// run most recently in Direction mode.
        /// </remarks>
        public Task<ImpinjSettings> QuerySettingsAsync()
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            return Client.QuerySettingsAsync();
        }

        /// <summary>Applies the provided settings to the reader.</summary>
        /// <param name="settings">Settings to apply to the reader</param>
        /// <exception cref="T:Impinj.OctaneSdk.OctaneSdkException">
        /// Thrown when this method is called prior to establishing a
        /// connection with a reader, or if the settings are invalid.
        /// </exception>
        public Task ApplySettingsAsync(ImpinjSettings settings)
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            return Client.ApplySettingsAsync(settings);
        }

        /// <summary>
        /// Instructs the Reader to save the current configuration to persistent
        /// storage. The saved parameters then become the Reader's power-on and
        /// reset settings.
        /// </summary>
        public Task SaveSettingsAsync()
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            return Client.SaveSettingsAsync();
        }

        /// <summary>
        /// This function queries the reader for a summary of its current status.
        /// </summary>
        /// <returns>An object containing the current reader status.</returns>
        /// <exception cref="T:Impinj.OctaneSdk.OctaneSdkException">Thrown if a communication error
        /// occurs while talking with the reader.</exception>
        public Task<Status> QueryStatusAsync()
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            return Client.QueryStatusAsync();
        }

        public Task<bool> QueryIsSingulatingAsync()
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            return Client.QueryIsSingulatingAsync();
        }

        public Task<short> QueryTemperatureAsync()
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            return Client.QueryTemperatureAsync();
        }

        public Task<GpiStatusGroup> QueryGpisAsync()
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            return Client.QueryGpisAsync();
        }

        public Task<AntennaStatusGroup> QueryAntennasAsync()
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            return Client.QueryAntennasAsync();
        }

        public Task<List<uint>> GetOpSequenceIDsAsync()
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            return Client.GetOpSequenceIDsAsync();
        }

        /// <summary>
        /// Adds a sequence of tag operations (read, write, lock, kill) to the
        /// reader.
        /// </summary>
        /// <param name="sequence">
        /// The sequence of operations to add to the reader.
        /// </param>
        /// <exception cref="T:Impinj.OctaneSdk.OctaneSdkException">
        /// Thrown if a zero length operations sequence is provided.</exception>
        public Task AddOpSequenceAsync(TagOpSequence sequence)
        {
            if (!IsConnected)
            {
                throw new InvalidOperationException("Reader disconnected");
            }
                

            return Client.AddOpSequenceAsync(sequence);
        }

        /// <summary>Deletes a tag operation sequence from the reader.</summary>
        /// <param name="sequenceId">
        /// The sequence ID of the operation sequence to delete.
        /// </param>
        /// <exception cref="T:Impinj.OctaneSdk.OctaneSdkException">
        /// Thrown if the referenced sequence does not exist on the reader.
        /// </exception>
        public Task DeleteOpSequenceAsync(uint sequenceId)
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            return Client.DeleteAccessSpecAsync(sequenceId);
        }

        /// <summary>Deletes all tag operation sequences from the reader.</summary>
        public Task DeleteAllOpSequencesAsync()
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            return Client.DeleteAllAccessSpecsAsync();
        }

        /// <summary>Enables a tag operation sequence on the reader.</summary>
        /// <param name="sequenceId">
        /// The sequence ID of the operation sequence to enable.
        /// </param>
        /// <exception cref="T:Impinj.OctaneSdk.OctaneSdkException">
        /// Thrown if the referenced sequence does not exist on the reader.
        /// </exception>
        public Task EnableOpSequenceAsync(uint sequenceId)
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            return Client.EnableAccessSpecAsync(sequenceId);
        }

        public Task DisableOpSequenceAsync(uint sequenceId)
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            return Client.DisableAccessSpecAsync(sequenceId);
        }





        private void Client_ConnectionChanged(object sender, ConnectionChangedEventArgs e)
        {
            OnConnectionChanged(new ConnectionEvent(e.Timestamp, e.IsConnected, e.Error));
        }

        private void Client_MessageReceived(object sender, MessageReceivedEventArgs e)
        {
            switch ((ENUM_LLRP_MSG_TYPE)e.Message.MsgTypeID)
            {
                case ENUM_LLRP_MSG_TYPE.KEEPALIVE:
                    ProcessKeepAlive((MSG_KEEPALIVE)e.Message);
                    break;
                case ENUM_LLRP_MSG_TYPE.RO_ACCESS_REPORT:
                    ProcessTagReport((MSG_RO_ACCESS_REPORT)e.Message);
                    break;
                case ENUM_LLRP_MSG_TYPE.READER_EVENT_NOTIFICATION:
                    ProcessReaderEvent((MSG_READER_EVENT_NOTIFICATION)e.Message);
                    break;
            }
        }

        private void ProcessKeepAlive(MSG_KEEPALIVE msg)
        {
            if (msg == null)
                throw new ArgumentNullException(nameof(msg));

            OnKeepaliveReceived();
        }

        private void ProcessTagReport(MSG_RO_ACCESS_REPORT msg)
        {
            if (msg == null)
                throw new ArgumentNullException(nameof(msg));

            if (msg.TagReportData != null && msg.TagReportData.Length != 0)
            {
                TagReport report = new TagReport
                {
                    HostReceivedTime = msg.MsgReceivedTimestamp
                };

                foreach (PARAM_TagReportData tagReportData in msg.TagReportData)
                {
                    if (tagReportData.EPCParameter.Count == 0)
                    {
                        continue;
                    }

                    TagData epc = null;

                    if (tagReportData.EPCParameter[0] is PARAM_EPC_96 epc96)
                    {
                        epc = TagData.FromBitList(epc96.EPC);
                    }
                    else if (tagReportData.EPCParameter[0] is PARAM_EPCData epcData)
                    {
                        epc = TagData.FromBitList(epcData.EPC);
                    }

                    Tag tag = new Tag(epc)
                    {
                        HostReceivedTime = msg.MsgReceivedTimestamp
                    };

                    if (tagReportData.AntennaID != null)
                    {
                        tag.AntennaPortNumber = tagReportData.AntennaID.AntennaID;
                        tag.IsAntennaPortNumberPresent = true;
                    }
                    if (tagReportData.ChannelIndex != null)
                    {
                        tag.ChannelInMhz = ReaderCapabilities.TxFrequencies[tagReportData.ChannelIndex.ChannelIndex - 1];
                        tag.IsChannelInMhzPresent = true;
                    }
                    if (tagReportData.FirstSeenTimestampUTC != null)
                    {
                        tag.FirstSeenTime = LLRPUtil.ConvertMicrosecondsToDateTime(tagReportData.FirstSeenTimestampUTC.Microseconds);
                        tag.IsFirstSeenTimePresent = true;
                    }
                    if (tagReportData.LastSeenTimestampUTC != null)
                    {
                        tag.LastSeenTime = LLRPUtil.ConvertMicrosecondsToDateTime(tagReportData.LastSeenTimestampUTC.Microseconds);
                        tag.IsLastSeenTimePresent = true;
                    }
                    if (tagReportData.TagSeenCount != null)
                    {
                        tag.TagSeenCount = tagReportData.TagSeenCount.TagCount;
                        tag.IsSeenCountPresent = true;
                    }

                    foreach (ILLRPParameter airProtocolTagData in tagReportData.AirProtocolTagData)
                    {
                        if (airProtocolTagData is PARAM_C1G2_CRC paramC1G2Crc)
                        {
                            tag.Crc = paramC1G2Crc.CRC;
                            tag.IsCrcPresent = true;
                        }
                        else if (airProtocolTagData is PARAM_C1G2_PC paramC1G2Pc)
                        {
                            tag.PcBits = paramC1G2Pc.PC_Bits;
                            tag.IsPcBitsPresent = true;
                        }
                    }

                    foreach (ILLRPCustomParameter custom in tagReportData.Custom)
                    {
                        if (custom is PARAM_ImpinjSerializedTID impinjSerializedTid)
                        {
                            tag.Tid = TagData.FromWordList(impinjSerializedTid.TID);
                            tag.IsFastIdPresent = true;
                        }
                        else if (custom is PARAM_ImpinjRFDopplerFrequency dopplerFrequency)
                        {
                            tag.RfDopplerFrequency = dopplerFrequency.DopplerFrequency / 16.0;
                            tag.IsRfDopplerFrequencyPresent = true;
                        }
                        else if (custom is PARAM_ImpinjRFPhaseAngle impinjRfPhaseAngle)
                        {
                            tag.PhaseAngleInRadians = impinjRfPhaseAngle.PhaseAngle * 0.00153398078788564;
                            tag.IsRfPhaseAnglePresent = true;
                        }
                        else if (custom is PARAM_ImpinjPeakRSSI paramImpinjPeakRssi)
                        {
                            tag.PeakRssiInDbm = paramImpinjPeakRssi.RSSI / 100.0;
                            tag.IsPeakRssiInDbmPresent = true;
                        }
                        else if (custom is PARAM_ImpinjGPSCoordinates impinjGpsCoordinates)
                        {
                            tag.GpsCoodinates = new GpsCoordinates()
                            {
                                Latitude = impinjGpsCoordinates.Latitude / 1000000.0,
                                Longitude = impinjGpsCoordinates.Longitude / 1000000.0,
                            };
                            tag.IsGpsCoordinatesPresent = true;
                        }
                    }

                    uint accessSpecId = tagReportData.AccessSpecID.AccessSpecID;

                    foreach (ILLRPParameter accessCommandOpSpecResult in tagReportData.AccessCommandOpSpecResult)
                    {
                        if (accessCommandOpSpecResult is PARAM_C1G2ReadOpSpecResult readOpSpecResult)
                        {
                            tag.TagOpResults.Add(new TagReadOpResult()
                            {
                                SequenceId = accessSpecId,
                                OpId = readOpSpecResult.OpSpecID,
                                Result = (ReadResultStatus)readOpSpecResult.Result,
                                Data = readOpSpecResult.Result == ENUM_C1G2ReadResultType.Success ? TagData.FromWordList(readOpSpecResult.ReadData) : new TagData(),
                                Tag = tag,
                            });
                        }
                        else if (accessCommandOpSpecResult is PARAM_C1G2WriteOpSpecResult writeOpSpecResult)
                        {
                            tag.TagOpResults.Add(new TagWriteOpResult()
                            {
                                SequenceId = accessSpecId,
                                OpId = writeOpSpecResult.OpSpecID,
                                Result = (WriteResultStatus)writeOpSpecResult.Result,
                                NumWordsWritten = writeOpSpecResult.NumWordsWritten,
                                IsBlockWrite = false,
                                Tag = tag,
                            });
                        }
                        else if (accessCommandOpSpecResult is PARAM_C1G2BlockWriteOpSpecResult blockWriteOpSpecResult)
                        {
                            tag.TagOpResults.Add(new TagWriteOpResult()
                            {
                                SequenceId = accessSpecId,
                                OpId = blockWriteOpSpecResult.OpSpecID,
                                Result = (WriteResultStatus)blockWriteOpSpecResult.Result,
                                NumWordsWritten = blockWriteOpSpecResult.NumWordsWritten,
                                IsBlockWrite = true,
                                Tag = tag,
                            });
                        }
                        else if (accessCommandOpSpecResult is PARAM_C1G2LockOpSpecResult lockOpSpecResult)
                        {
                            tag.TagOpResults.Add(new TagLockOpResult()
                            {
                                SequenceId = accessSpecId,
                                OpId = lockOpSpecResult.OpSpecID,
                                Result = (LockResultStatus)lockOpSpecResult.Result,
                                Tag = tag,
                            });
                        }
                        else if (accessCommandOpSpecResult is PARAM_C1G2KillOpSpecResult killOpSpecResult)
                        {
                            tag.TagOpResults.Add(new TagKillOpResult()
                            {
                                SequenceId = accessSpecId,
                                OpId = killOpSpecResult.OpSpecID,
                                Result = (KillResultStatus)killOpSpecResult.Result,
                                Tag = tag,
                            });
                        }
                        else if (accessCommandOpSpecResult is PARAM_ImpinjSetQTConfigOpSpecResult setQTConfigOpSpecResult)
                        {
                            tag.TagOpResults.Add(new TagQtSetOpResult()
                            {
                                SequenceId = accessSpecId,
                                OpId = setQTConfigOpSpecResult.OpSpecID,
                                Result = (QtSetConfigResultStatus)setQTConfigOpSpecResult.Result,
                                Tag = tag,
                            });
                        }
                        else if (accessCommandOpSpecResult is PARAM_ImpinjGetQTConfigOpSpecResult getQTConfigOpSpecResult)
                        {
                            tag.TagOpResults.Add(new TagQtGetOpResult()
                            {
                                SequenceId = accessSpecId,
                                OpId = getQTConfigOpSpecResult.OpSpecID,
                                Result = (QtGetConfigResultStatus)getQTConfigOpSpecResult.Result,
                                AccessRange = (QtAccessRange)getQTConfigOpSpecResult.AccessRange,
                                DataProfile = (QtDataProfile)getQTConfigOpSpecResult.DataProfile,
                                Tag = tag,
                            });
                        }
                        else if (accessCommandOpSpecResult is PARAM_ImpinjBlockPermalockOpSpecResult blockPermalockOpSpecResult)
                        {
                            tag.TagOpResults.Add(new TagBlockPermalockOpResult()
                            {
                                SequenceId = accessSpecId,
                                OpId = blockPermalockOpSpecResult.OpSpecID,
                                Result = (BlockPermalockResult)blockPermalockOpSpecResult.Result,
                                Tag = tag,
                            });
                        }
                        else if (accessCommandOpSpecResult is PARAM_ImpinjMarginReadOpSpecResult marginReadOpSpecResult)
                        {
                            tag.TagOpResults.Add(new TagMarginReadOpResult()
                            {
                                SequenceId = accessSpecId,
                                OpId = marginReadOpSpecResult.OpSpecID,
                                Result = (MarginReadResult)marginReadOpSpecResult.Result,
                                Tag = tag,
                            });
                        }
                    }

                    report.Tags.Add(tag);
                }

                if (report.Tags.Count > 0)
                {
                    OnTagsReported(report);
                }
            }

            foreach (ILLRPCustomParameter custom in msg.Custom)
            {
                if (custom is PARAM_ImpinjExtendedTagInformation extendedTagInformation)
                {
                    TagData epc = TagData.FromBitList(extendedTagInformation.EPCData[0].EPC);

                    if (extendedTagInformation.ImpinjLocationReportData != null)
                    {
                        LocationReport report = new LocationReport(epc)
                        {
                            HostReceivedTime = msg.MsgReceivedTimestamp,
                            LocationXCm = extendedTagInformation.ImpinjLocationReportData.LocXCentimeters,
                            LocationYCm = extendedTagInformation.ImpinjLocationReportData.LocYCentimeters,
                            ReportType = (LocationReportType)extendedTagInformation.ImpinjLocationReportData.Type,
                            Timestamp = LLRPUtil.ConvertMicrosecondsToDateTime(extendedTagInformation.ImpinjLocationReportData.LastSeenTimestampUTC),
                            ConfidenceFactors = new LocationConfidenceFactors()
                            {
                                ReadCount = extendedTagInformation.ImpinjLocationReportData.ImpinjLocationConfidence.ReadCount,
                                Data = extendedTagInformation.ImpinjLocationReportData.ImpinjLocationConfidence.ConfidenceData,
                            },
                        };
                        OnLocationReported(report);
                    }
                    else if (extendedTagInformation.ImpinjDirectionReportData != null)
                    {
                        DirectionReport report = new DirectionReport(epc)
                        {
                            HostReceivedTime = msg.MsgReceivedTimestamp,
                            FirstSeenSector = extendedTagInformation.ImpinjDirectionReportData.FirstSeenSectorID,
                            LastReadSector = extendedTagInformation.ImpinjDirectionReportData.LastSeenSectorID,
                            FirstSeenTimestamp = LLRPUtil.ConvertMicrosecondsToDateTime(extendedTagInformation.ImpinjDirectionReportData.FirstSeenTimestampUTC),
                            LastReadTimestamp = LLRPUtil.ConvertMicrosecondsToDateTime(extendedTagInformation.ImpinjDirectionReportData.LastSeenTimestampUTC),
                            DirectionDiagnosticDataMetric = extendedTagInformation.ImpinjDirectionReportData.ImpinjDirectionDiagnosticData?.Metric,
                            TagPopulationStatus = (DirectionTagPopulationStatus)extendedTagInformation.ImpinjDirectionReportData.TagPopulationStatus,
                            ReportType = (DirectionReportType)extendedTagInformation.ImpinjDirectionReportData.Type,
                        };
                        OnDirectionReported(report);
                    }
                }
                else if (custom is PARAM_ImpinjDiagnosticReport diagnosticReport)
                {
                    DiagnosticReport report = new DiagnosticReport
                    {
                        HostReceivedTime = msg.MsgReceivedTimestamp
                    };
                    report.Metrics.AddRange(diagnosticReport.Metric);
                    OnDiagnosticsReported(report);
                }
            }
        }

        private void ProcessReaderEvent(MSG_READER_EVENT_NOTIFICATION msg)
        {
            if (msg == null)
                throw new ArgumentNullException(nameof(msg));

            if (msg.ReaderEventNotificationData.GPIEvent != null)
            {
                GpiEvent e = new GpiEvent
                {
                    HostReceivedTime = msg.MsgReceivedTimestamp
                };
                if (msg.ReaderEventNotificationData.Timestamp[0] is PARAM_UTCTimestamp utcTimestamp)
                {
                    e.ReaderTimestamp = LLRPUtil.ConvertMicrosecondsToDateTime(utcTimestamp.Microseconds);
                }
                e.PortNumber = msg.ReaderEventNotificationData.GPIEvent.GPIPortNumber;
                e.State = msg.ReaderEventNotificationData.GPIEvent.GPIEvent;
                OnGpiChanged(e);
            }
            if (msg.ReaderEventNotificationData.AntennaEvent != null)
            {
                AntennaEvent e = new AntennaEvent
                {
                    HostReceivedTime = msg.MsgReceivedTimestamp
                };
                if (msg.ReaderEventNotificationData.Timestamp[0] is PARAM_UTCTimestamp utcTimestamp)
                {
                    e.ReaderTimestamp = LLRPUtil.ConvertMicrosecondsToDateTime(utcTimestamp.Microseconds);
                }
                e.PortNumber = msg.ReaderEventNotificationData.AntennaEvent.AntennaID;
                e.State = (AntennaEventType)msg.ReaderEventNotificationData.AntennaEvent.EventType;
                OnAntennaChanged(e);
            }
            if (msg.ReaderEventNotificationData.ReportBufferLevelWarningEvent != null)
            {
                ReportBufferWarningEvent e = new ReportBufferWarningEvent
                {
                    HostReceivedTime = msg.MsgReceivedTimestamp
                };
                if (msg.ReaderEventNotificationData.Timestamp[0] is PARAM_UTCTimestamp utcTimestamp)
                {
                    e.ReaderTimestamp = LLRPUtil.ConvertMicrosecondsToDateTime(utcTimestamp.Microseconds);
                }
                e.PercentFull = msg.ReaderEventNotificationData.ReportBufferLevelWarningEvent.ReportBufferPercentageFull;
                OnReportBufferWarning(e);
            }
            if (msg.ReaderEventNotificationData.ReportBufferOverflowErrorEvent != null)
            {
                ReportBufferOverflowEvent e = new ReportBufferOverflowEvent
                {
                    HostReceivedTime = msg.MsgReceivedTimestamp
                };
                if (msg.ReaderEventNotificationData.Timestamp[0] is PARAM_UTCTimestamp utcTimestamp)
                {
                    e.ReaderTimestamp = LLRPUtil.ConvertMicrosecondsToDateTime(utcTimestamp.Microseconds);
                }
                OnReportBufferOverflow(e);
            }
            if (msg.ReaderEventNotificationData.ROSpecEvent != null)
            {
                RoSpecEventType roSpecEventType = (RoSpecEventType)msg.ReaderEventNotificationData.ROSpecEvent.EventType;
                if (roSpecEventType == RoSpecEventType.StartOfROSpec)
                {
                    ReadingEvent e = new ReadingEvent
                    {
                        HostReceivedTime = msg.MsgReceivedTimestamp
                    };
                    if (msg.ReaderEventNotificationData.Timestamp[0] is PARAM_UTCTimestamp utcTimestamp)
                    {
                        e.ReaderTimestamp = LLRPUtil.ConvertMicrosecondsToDateTime(utcTimestamp.Microseconds);
                    }
                    e.IsReading = true;
                    OnReadingChanged(e);
                }
                else if (roSpecEventType == RoSpecEventType.EndOfROSpec)
                {
                    ReadingEvent e = new ReadingEvent
                    {
                        HostReceivedTime = msg.MsgReceivedTimestamp
                    };
                    if (msg.ReaderEventNotificationData.Timestamp[0] is PARAM_UTCTimestamp utcTimestamp)
                    {
                        e.ReaderTimestamp = LLRPUtil.ConvertMicrosecondsToDateTime(utcTimestamp.Microseconds);
                    }
                    e.IsReading = false;
                    OnReadingChanged(e);
                }
            }
        }






        /// <summary>
        /// Event to provide notification when a tag report is available.
        /// </summary>
        public event EventHandler<TagReport> TagsReported;
        protected virtual void OnTagsReported(TagReport tagReport)
        {
            try { TagsReported?.Invoke(this, tagReport); } catch { }
        }

        /// <summary>
        /// Event to provide notification when a GPI port status changes.
        /// </summary>
        public event EventHandler<GpiEvent> GpiChanged;
        protected virtual void OnGpiChanged(GpiEvent gpiEvent)
        {
            try { GpiChanged?.Invoke(this, gpiEvent); } catch { }
        }

        /// <summary>
        /// Event to provide notification when an AntennaChanged event
        /// occurs.
        /// </summary>
        public event EventHandler<AntennaEvent> AntennaChanged;
        protected virtual void OnAntennaChanged(AntennaEvent antennaEvent)
        {
            try { AntennaChanged?.Invoke(this, antennaEvent); } catch { }
        }

        /// <summary>
        /// Event to provide notification when the report buffer on the reader is nearly full.
        /// </summary>
        public event EventHandler<ReportBufferWarningEvent> ReportBufferWarning;
        protected virtual void OnReportBufferWarning(ReportBufferWarningEvent reportBufferWarningEvent)
        {
            try { ReportBufferWarning?.Invoke(this, reportBufferWarningEvent); } catch { }
        }

        /// <summary>
        /// Event to provide notification when the report buffer on the reader
        /// could not hold more tag reports.
        /// </summary>
        public event EventHandler<ReportBufferOverflowEvent> ReportBufferOverflow;
        protected virtual void OnReportBufferOverflow(ReportBufferOverflowEvent reportBufferOverflowEvent)
        {
            try { ReportBufferOverflow?.Invoke(this, reportBufferOverflowEvent); } catch { }
        }

        /// <summary>
        /// Event to provide notification when the reader has started or stopped.
        /// </summary>
        public event EventHandler<ReadingEvent> ReadingChanged;
        protected virtual void OnReadingChanged(ReadingEvent readingEvent)
        {
            try { ReadingChanged?.Invoke(this, readingEvent); } catch { }
        }

        /// <summary>
        /// Event to provide notification that a keep alive TCP/IP packet
        /// was received from the reader.
        /// </summary>
        public event EventHandler KeepaliveReceived;
        protected virtual void OnKeepaliveReceived()
        {
            try { KeepaliveReceived?.Invoke(this, EventArgs.Empty); } catch { }
        }

        /// <summary>
        /// Event to provide notification that the TCP/IP connection to the
        /// Impinj Reader has been lost.
        /// </summary>
        public event EventHandler<ConnectionEvent> ConnectionChanged;
        protected virtual void OnConnectionChanged(ConnectionEvent connectionEvent)
        {
            try { ConnectionChanged?.Invoke(this, connectionEvent); } catch { }
        }

        /// <summary>
        /// Event to provide notification of a location report. Only available on the xArray.
        /// </summary>
        public event EventHandler<LocationReport> LocationReported;
        protected virtual void OnLocationReported(LocationReport locationReport)
        {
            try { LocationReported?.Invoke(this, locationReport); } catch { }
        }

        /// <summary>
        /// Event to provide notification of a direction report. Only available on the xArray.
        /// </summary>
        public event EventHandler<DirectionReport> DirectionReported;
        protected virtual void OnDirectionReported(DirectionReport directionReport)
        {
            try { DirectionReported?.Invoke(this, directionReport); } catch { }
        }

        /// <summary>
        /// Event to provide notification of a diagnostic report. Only available on the xArray.
        /// </summary>
        public event EventHandler<DiagnosticReport> DiagnosticsReported;
        protected virtual void OnDiagnosticsReported(DiagnosticReport diagnosticReport)
        {
            try { DiagnosticsReported?.Invoke(this, diagnosticReport); } catch { }
        }


        #region Dispose

        private bool disposed = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                // Free any managed objects here.
                try { Client.ConnectionChanged -= Client_ConnectionChanged; } catch { }
                try { Client.MessageReceived -= Client_MessageReceived; } catch { }
                try { Client.Dispose(); } catch { }
            }

            // Free any unmanaged objects here

            disposed = true;
        }
        ~ImpinjReader()
        {
            Dispose(false);
        }

        #endregion Dispose
    }
}
