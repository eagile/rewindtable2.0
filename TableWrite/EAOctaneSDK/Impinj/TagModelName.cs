namespace EA.Impinj
{
    /// <summary>An enumeration used to indicate a tag's model name.</summary>
    public enum TagModelName
    {
        /// <summary>%Impinj Monza 4QT</summary>
        Monza4QT,
        /// <summary>%Impinj Monza 4U</summary>
        Monza4U,
        /// <summary>%Impinj Monza 4E</summary>
        Monza4E,
        /// <summary>%Impinj Monza 4D</summary>
        Monza4D,
        /// <summary>%Impinj Monza 5</summary>
        Monza5,
        /// <summary>%Impinj Monza 5U</summary>
        Monza5U,
        /// <summary>%Impinj Monza X-2K Dura</summary>
        MonzaX_2K_Dura,
        /// <summary>%Impinj Monza X-8K Dura</summary>
        MonzaX_8K_Dura,
        /// <summary>%Impinj Monza R6</summary>
        MonzaR6,
        /// <summary>%Impinj Monza R6-P</summary>
        MonzaR6_P,
        /// <summary>%Impinj Monza S6-C</summary>
        MonzaS6_C,
        /// <summary>An unknown tag</summary>
        Other,
        /// <summary>%Impinj Monza R6-A</summary>
        MonzaR6_A,
    }
}
