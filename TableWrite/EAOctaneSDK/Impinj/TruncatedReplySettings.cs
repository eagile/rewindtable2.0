﻿namespace EA.Impinj
{
    /// <summary>
    /// Class containing the settings for using Truncated Reply during Inventory.
    /// </summary>
    public class TruncatedReplySettings
    {
        /// <summary>
        /// Flag indicating whether the Truncated Reply feature is enabled.
        /// </summary>
        public bool IsEnabled { get; set; }

        /// <summary>
        /// If only Gen2v2 tags are in the field of view then setting the
        /// Gen2v2TagsOnly flag will perform an additional CRC check
        /// to further validate the integrity of the tag response.
        /// If this flag is enabled non-Gen2v2 tags will be ignored.
        /// </summary>
        public bool Gen2v2TagsOnly { get; set; } = true;

        /// <summary>
        /// The EPC length, in 16-bit words, of all tags in the field of view.
        /// If Gen2V2TagsOnly is true, only tags with EPCs of this length can reply
        /// and tags with EPCs that are not this length will be ignored.
        /// </summary>
        public byte EpcLengthInWords { get; set; }

        /// <summary>
        /// Select a starting bit location in the EPC memory bank that will be
        /// used for the truncating select command.
        /// </summary>
        public ushort BitPointer { get; set; }

        /// <summary>
        /// Specifies the mask that must match the EPC memory bank starting at
        /// Pointer.  The end of the mask defines the start location of EPC
        /// reported by the tag.  If the RO Spec contains C1G2Filters then the
        /// length of TagMask must be 0. Filter validation is disabled when
        /// truncated reply is used.
        /// </summary>
        public string TagMask { get; set; }
    }
}
