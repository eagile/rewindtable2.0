using System.Collections.Generic;

namespace EA.Impinj
{
    /// <summary>
    /// Container class encapsulating all of the GPO configuration objects.
    /// This is for internal use only, and does not appear in the generated
    /// documentation.
    /// </summary>
    public class GpoConfigGroup : List<GpoConfig>
    {
        /// <summary>
        /// The GPIO Port number scheme is not 0 indexed. The first port is "1", which this value represents.
        /// </summary>
        public static int GPO_PORT_START_INDEX { get; } = 1;

        private readonly bool _createdBySerializer;

        public GpoConfigGroup()
        {
            _createdBySerializer = true;
        }

        public GpoConfigGroup(ushort numGpos) : base(numGpos)
        {
            _createdBySerializer = false;
            for (int i = GPO_PORT_START_INDEX; i < GPO_PORT_START_INDEX + numGpos; i++)
            {
                base.Add(new GpoConfig((ushort)i));
            }
        }

        /// <summary>
        /// Adds the provided GPO configuration object to the collection.
        /// For internal library use only.
        /// </summary>
        /// <param name="config">The GpOConfig object to add.</param>
        public new void Add(GpoConfig config)
        {
            if (!_createdBySerializer)
                throw new OctaneSdkException("Illegal operation - cannot Add to GPOConfigGroup object!");
            base.Add(config);
        }

        /// <summary>The number of GPOs in the collection.</summary>
        public int Length => Count;

        /// <summary>Returns the settings for the specified GPO port.</summary>
        /// <returns>The GPO settings or throws an exception if the port does not exist.</returns>
        public GpoConfig GetGpo(ushort portNumber)
        {
            foreach (GpoConfig gpoConfig in this)
            {
                if (gpoConfig.PortNumber == portNumber)
                    return gpoConfig;
            }
            throw new OctaneSdkException("Invalid GPO port number specified.");
        }
    }
}
