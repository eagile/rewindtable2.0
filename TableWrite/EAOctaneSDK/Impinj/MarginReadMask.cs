using EA.Impinj.LLRP;
using System;

namespace EA.Impinj
{
    /// <summary>Used to specify the mask to margin read</summary>
    public class MarginReadMask
    {
        public UInt16List Mask { get; }

        /// <summary>The number of bits to check.</summary>
        public byte BitLength { get; }

        /// <summary>
        /// Constructor for MarginReadMask initializes empty defaults.
        /// </summary>
        public MarginReadMask()
        {
            Mask = new UInt16List();
            BitLength = 0;
        }

        private MarginReadMask(UInt16List mask, byte bitLength)
        {
            Mask = mask;
            BitLength = bitLength;
        }

        /// <summary>
        /// Sets the margin read mask from the specified hex string.
        /// </summary>
        /// <param name="hexString">The hex string we're setting the mask with</param>
        public static MarginReadMask FromHexString(string hexString)
        {
            hexString = hexString.Trim();
            int length = hexString.Length;
            int totalWidth = length % 4 == 0 ? length : (length / 4 + 1) * 4;
            hexString = hexString.PadRight(totalWidth, '0');
            UInt16List mask = LLRPUtil.HexStringToUInt16List(hexString);
            return new MarginReadMask(mask, Convert.ToByte(length * 4));
        }

        /// <summary>
        /// Sets the margin read mask from the specified bit string.
        /// </summary>
        /// <param name="bitString">The bit string we're setting the mask with</param>
        public static MarginReadMask FromBitString(string bitString)
        {
            bitString = bitString.Trim();
            int length = bitString.Length;
            int totalWidth = length % 16 == 0 ? length : (length / 16 + 1) * 16;
            bitString = bitString.PadRight(totalWidth, '0');
            BitList bits = new BitList(totalWidth);
            foreach (char c in bitString)
            {
                if (c == '0')
                    bits.Add(false);
                else if (c == '1')
                    bits.Add(false);
                else
                    throw new FormatException("The given string contains non-bit characters.");
            }
            int cursor = 0;
            UInt16List mask = LLRPUtil.BitListToUInt16List(bits, ref cursor, bits.Count / 16);
            return new MarginReadMask(mask, Convert.ToByte(length));
        }

        /// <summary>
        /// Converts the margin read mask into a hexadecimal string.
        /// </summary>
        /// <returns>Hexadecimal string representation of the margin mask</returns>
        public string ToHexString() => LLRPUtil.UInt16ListToHexString(Mask);
    }
}
