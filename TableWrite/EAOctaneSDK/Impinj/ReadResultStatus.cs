namespace EA.Impinj
{
    /// <summary>
    /// Enum defining the possible status outcomes for the tag read operation.
    /// </summary>
    public enum ReadResultStatus
    {
        /// <summary>The tag read operation was successful.</summary>
        Success,
        /// <summary>An unidentified tag error occurred.</summary>
        NonspecificTagError,
        /// <summary>
        /// No ACK response was received from the tag in response to the operation.
        /// </summary>
        NoResponseFromTag,
        /// <summary>An unidentified reader error occurred.</summary>
        NonspecificReaderError,
    }
}
