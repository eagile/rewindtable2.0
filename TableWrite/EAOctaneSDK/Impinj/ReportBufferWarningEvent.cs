using System;

namespace EA.Impinj
{
    /// <summary>
    /// Class used to encapsulate the details of a
    /// <see cref="E:Impinj.OctaneSdk.ImpinjReader.ReportBufferWarning" />
    /// reader event.
    /// </summary>
    public class ReportBufferWarningEvent : EventArgs
    {
        /// <summary>Timestamp of when the host received the event.</summary>
        public DateTime HostReceivedTime { get; set; }

        /// <summary>Timestamp of the event.</summary>
        public DateTime ReaderTimestamp { get; set; }

        /// <summary>
        /// Parameter defining how full the reader report buffer is in percent.
        /// </summary>
        public byte PercentFull { get; set; }
    }
}
