namespace EA.Impinj
{
    /// <summary>
    /// Enum defining the possible status outcomes for the tag QT operation.
    /// </summary>
    public enum QtSetConfigResultStatus
    {
        /// <summary>The tag QT operation was successful.</summary>
        Success,
        /// <summary>The tag did not meet the required RSSI threshold.</summary>
        InsufficientPower,
        /// <summary>An unidentified tag error occurred.</summary>
        NonspecificTagError,
        /// <summary>
        /// No ACK response was received from the tag in response to the operation.
        /// </summary>
        NoResponseFromTag,
        /// <summary>An unidentified reader error occurred.</summary>
        NonspecificReaderError,
        /// <summary>An incorrect QT password was used.</summary>
        IncorrectPasswordError,
    }
}
