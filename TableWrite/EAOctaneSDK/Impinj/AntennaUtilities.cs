using System.Collections.Generic;

namespace EA.Impinj
{
    /// <summary>
    /// Various utility functions for getting antenna patterns
    /// </summary>
    public static class AntennaUtilities
    {
        /// <summary>
        /// Maps a sector to its associated beam list for an xArray
        /// </summary>
        private static readonly Dictionary<ushort, IList<ushort>> XArraySectorToBeamMap = new Dictionary<ushort, IList<ushort>>()
        {
            {
                 1,
                 new List<ushort>()
                {
                     1,
                     2,
                     3,
                     4
                }
            },
            {
                 2,
                 new List<ushort>()
                {
                     5,
                     13,
                     21,
                     29,
                     37,
                     45
                }
            },
            {
                 3,
                 new List<ushort>()
                {
                     6,
                     14,
                     22,
                     30,
                     38,
                     46
                }
            },
            {
                 4,
                 new List<ushort>()
                {
                     7,
                     15,
                     23,
                     31,
                     39,
                     47
                }
            },
            {
                 5,
                 new List<ushort>()
                {
                     8,
                     16,
                     24,
                     32,
                     40,
                     48
                }
            },
            {
                 6,
                 new List<ushort>()
                {
                     9,
                     17,
                     25,
                     33,
                     41,
                     49
                }
            },
            {
                 7,
                 new List<ushort>()
                {
                     10,
                     18,
                     26,
                     34,
                     42,
                     50
                }
            },
            {
                 8,
                 new List<ushort>()
                {
                     11,
                     19,
                     27,
                     35,
                     43,
                     51
                }
            },
            {
                 9,
                 new List<ushort>()
                {
                     12,
                     20,
                     28,
                     36,
                     44,
                     52
                }
            }
        };
        /// <summary>
        /// Maps a sector to its associated beam list for an xSpan
        /// </summary>
        private static readonly Dictionary<ushort, IList<ushort>> XSpanSectorToBeamMap = new Dictionary<ushort, IList<ushort>>()
        {
            {
                 1,
                 new List<ushort>()
                {
                     1
                }
            },
            {
                 2,
                 new List<ushort>()
                {
                     2,
                     4,
                     6,
                     8,
                     10,
                     12
                }
            },
            {
                 3,
                 new List<ushort>()
                {
                     3,
                     5,
                     7,
                     9,
                     11,
                     13
                }
            }
        };
        /// <summary>Maps a ring to its associated beam list for an xArray</summary>
        private static readonly Dictionary<ushort, IList<ushort>> XArrayRingToBeamMap = new Dictionary<ushort, IList<ushort>>()
        {
            {
                 1,
                 new List<ushort>()
                {
                     1,
                     2,
                     3,
                     4
                }
            },
            {
                 2,
                 new List<ushort>()
                {
                     5,
                     6,
                     7,
                     8,
                     9,
                     10,
                     11,
                     12
                }
            },
            {
                 3,
                 new List<ushort>()
                {
                     13,
                     14,
                     15,
                     16,
                     17,
                     18,
                     19,
                     20
                }
            },
            {
                 4,
                 new List<ushort>()
                {
                     21,
                     22,
                     23,
                     24,
                     25,
                     26,
                     27,
                     28
                }
            },
            {
                 5,
                 new List<ushort>()
                {
                     29,
                     30,
                     31,
                     32,
                     33,
                     34,
                     35,
                     36
                }
            },
            {
                 6,
                 new List<ushort>()
                {
                     37,
                     38,
                     39,
                     40,
                     41,
                     42,
                     43,
                     44
                }
            },
            {
                 7,
                 new List<ushort>()
                {
                     45,
                     46,
                     47,
                     48,
                     49,
                     50,
                     51,
                     52
                }
            }
        };
        /// <summary>The optimized antenna list for an xArray</summary>
        private static readonly List<ushort> OptimizedXArrayAntennaList = new List<ushort>()
        {
             45,
             16,
             35,
             6,
             41,
             28,
             47,
             18,
             29,
             8,
             43,
             22,
             4,
             49,
             20,
             31,
             10,
             37,
             24,
             51,
             14,
             33,
             12,
             39,
             26,
             2,
             13,
             32,
             11,
             38,
             25,
             52,
             15,
             34,
             5,
             40,
             27,
             46,
             1,
             17,
             36,
             7,
             42,
             21,
             48,
             19,
             30,
             9,
             44,
             23,
             50,
             3
        };
        /// <summary>The optimized antenna list for an xSpan</summary>
        private static readonly List<ushort> OptimizedXSpanAntennaList = new List<ushort>()
        {
             1,
             2,
             3,
             4,
             5,
             6,
             7,
             8,
             9,
             10,
             11,
             12,
             13
        };

        /// <summary>Maps a given reader model number to its sector map</summary>
        private static readonly Dictionary<ReaderModel, IDictionary<ushort, IList<ushort>>> ReaderModelToBeamList = new Dictionary<ReaderModel, IDictionary<ushort, IList<ushort>>>()
        {
            { ReaderModel.XArray, XArraySectorToBeamMap },
            { ReaderModel.XArrayEAP, XArraySectorToBeamMap },
            { ReaderModel.XArrayWM, XArraySectorToBeamMap },
            { ReaderModel.XSpan, XSpanSectorToBeamMap }
        };

        /// <summary>
        /// Maps a given reader model number to its optimized antenna list
        /// </summary>
        private static readonly Dictionary<ReaderModel, IList<ushort>> ReaderModelToOptimizedBeamList = new Dictionary<ReaderModel, IList<ushort>>()
        {
            { ReaderModel.XArray, OptimizedXArrayAntennaList },
            { ReaderModel.XArrayEAP, OptimizedXArrayAntennaList },
            { ReaderModel.XArrayWM, OptimizedXArrayAntennaList },
            { ReaderModel.XSpan, OptimizedXSpanAntennaList }
        };

        /// <summary>
        /// Gets the list of available sectors for the reader model
        /// </summary>
        /// <param name="model">The model of the reader</param>
        /// <returns>A ReadOnlyCollection containing all of the available sectors for the reader model</returns>
        public static List<ushort> GetAvailableSectors(ReaderModel model)
        {
            List<ushort> ushortList = new List<ushort>();
            foreach (ushort key in ReaderModelToBeamList[model].Keys)
            {
                ushortList.Add(key);
            }
            return ushortList;
        }

        /// <summary>
        /// Get optimized antenna list for the reader model passed in
        /// </summary>
        /// <param name="model">The reader model we want the optimized antenna list for</param>
        /// <returns>The optimized antenna list</returns>
        public static List<ushort> GetOptimizedAntennaList(
            ReaderModel model)
        {
            IList<ushort> list = new List<ushort>();
            if (ReaderModelToOptimizedBeamList.ContainsKey(model))
            {
                list = ReaderModelToOptimizedBeamList[model];
            }
            return new List<ushort>(list);
        }

        /// <summary>
        /// Accepts a sector number and a reader model and returns the list of antenna numbers in the selected sector
        /// </summary>
        /// <param name="sector">The sector number associated with the antennas</param>
        /// <param name="model">The model of the reader to determine the definition of a sector</param>
        /// <returns>List of antenna numbers in the selected sectors</returns>
        public static List<ushort> GetAntennaIdsBySector(ushort sector, ReaderModel model)
        {
            List<ushort> ushortList = new List<ushort>();
            if (ReaderModelToBeamList.ContainsKey(model))
            {
                ushortList.AddRange(ReaderModelToBeamList[model][sector]);
            }
            return ushortList;
        }

        /// <summary>
        /// Accepts an array of sector numbers and a reader model and returns the list of antenna numbers in the selected sectors
        /// </summary>
        /// <param name="sectors">The sector numbers to get the antenna numbers for</param>
        /// <param name="model">The model of the reader to determine the definition of a sector</param>
        /// <returns>List of antenna numbers in the selected sectors</returns>
        public static List<ushort> GetAntennaIdsBySector(IEnumerable<ushort> sectors, ReaderModel model)
        {
            List<ushort> ushortList = new List<ushort>();
            if (ReaderModelToBeamList.ContainsKey(model))
            {
                foreach (ushort sector in sectors)
                {
                    ushortList.AddRange(ReaderModelToBeamList[model][sector]);
                }
            }
            return ushortList;
        }

        /// <summary>
        /// Accepts an array of ring numbers and a reader model and returns the list of antenna numbers in the selected rings
        /// </summary>
        /// <param name="rings">The ring numbers to get the antenna numbers for</param>
        /// <param name="model">The model of the reader to determine the definition of a ring</param>
        /// <returns>List of antenna numbers in the selected rings</returns>
        public static List<ushort> GetAntennaIdsByRing(IEnumerable<ushort> rings, ReaderModel model)
        {
            List<ushort> ushortList = new List<ushort>();
            if (model == ReaderModel.XArray || model == ReaderModel.XArrayEAP || model == ReaderModel.XArrayWM)
            {
                foreach (ushort ring in rings)
                {
                    ushortList.AddRange(XArrayRingToBeamMap[ring]);
                }
            }
            return ushortList;
        }
    }
}
