namespace EA.Impinj
{
    /// <summary>
    /// Class used to define the configuration settings for an
    /// individual antenna port.
    /// </summary>
    public class AntennaConfig
    {
        /// <summary>Default Constructor</summary>
        public AntennaConfig()
        {
        }

        /// <summary>Constructor for a new antenna configuration object.</summary>
        /// <param name="NewPortNumber">Antenna port to configure. A value of 0 means
        /// configuration is applied to all antennas.</param>
        public AntennaConfig(ushort NewPortNumber)
        {
            PortNumber = NewPortNumber;
            PortName = string.Format("Antenna Port {0}", PortNumber);
            IsEnabled = true;
            MaxRxSensitivity = true;
            MaxTxPower = true;
        }

        /// <summary>If set to true, the reader will try to inventory tags on that port</summary>
        public bool IsEnabled { get; set; }

        /// <summary>
        /// Specifies the antenna port number (starting at port 1) to configure.
        /// </summary>
        public ushort PortNumber { get; set; }

        /// <summary>A string that the user can name the antenna port. Example: Door Antenna</summary>
        public string PortName { get; set; }

        /// <summary>
        /// Defines the antenna transmit power in dBm (e.g. 24.0 dBm).
        /// </summary>
        public double TxPowerInDbm
        {
            get => _txPowerinDbm;
            set
            {
                _txPowerinDbm = value;
                MaxTxPower = false;
            }
        }
        private double _txPowerinDbm;

        /// <summary>
        /// Defines the antenna receive sensitivity (AKA receive sensitivity filter)
        /// in dBm (e.g. -55 dBm).
        /// </summary>
        public double RxSensitivityInDbm
        {
            get => _rxSensitivityinDbm;
            set
            {
                _rxSensitivityinDbm = value;
                MaxRxSensitivity = false;
            }
        }
        private double _rxSensitivityinDbm;

        /// <summary>
        /// Specifies that the maximum antenna transmit power setting should
        /// be used.
        /// </summary>
        public bool MaxTxPower { get; set; }

        /// <summary>
        /// Specifies that the maximum antenna receive sensitivity setting
        /// should be used.
        /// </summary>
        public bool MaxRxSensitivity { get; set; }
    }
}
