namespace EA.Impinj
{
    /// <summary>
    /// Contains model-specific details about the tag.
    /// You must enable FastID in the TagReport to get this information.
    /// FastID is only available on %Impinj Monza 4 and later tags.
    /// </summary>
    public class TagModelDetails
    {
        public TagModelDetails()
        {
            ModelName = TagModelName.Other;
            EpcSizeBits = 0;
            UserMemorySizeBits = 0;
            SupportsQt = false;
        }

        public TagModelDetails(string tid) : this()
        {
            string cls = tid.Substring(0, 2).ToUpper();
            string vendor = tid.Substring(2, 3).ToUpper();
            string model = tid.Substring(5, 3).ToUpper();
            if (cls == "E2" && vendor == "801")
            {
                switch (model)
                {
                    case "105":
                        ModelName = TagModelName.Monza4QT;
                        EpcSizeBits = 128;
                        UserMemorySizeBits = 512;
                        SupportsQt = true;
                        break;
                    case "104":
                        ModelName = TagModelName.Monza4U;
                        EpcSizeBits = 128;
                        UserMemorySizeBits = 512;
                        SupportsQt = false;
                        break;
                    case "10C":
                        ModelName = TagModelName.Monza4E;
                        EpcSizeBits = 496;
                        UserMemorySizeBits = 128;
                        SupportsQt = false;
                        break;
                    case "100":
                        ModelName = TagModelName.Monza4D;
                        EpcSizeBits = 128;
                        UserMemorySizeBits = 32;
                        SupportsQt = false;
                        break;
                    case "130":
                        ModelName = TagModelName.Monza5;
                        EpcSizeBits = 128;
                        UserMemorySizeBits = 0;
                        SupportsQt = false;
                        break;
                    case "132":
                        ModelName = TagModelName.Monza5U;
                        EpcSizeBits = 128;
                        UserMemorySizeBits = 32;
                        SupportsQt = false;
                        break;
                    case "140":
                        ModelName = TagModelName.MonzaX_2K_Dura;
                        EpcSizeBits = 128;
                        UserMemorySizeBits = 2176;
                        SupportsQt = true;
                        break;
                    case "150":
                        ModelName = TagModelName.MonzaX_8K_Dura;
                        EpcSizeBits = 128;
                        UserMemorySizeBits = 8192;
                        SupportsQt = true;
                        break;
                    case "160":
                        ModelName = TagModelName.MonzaR6;
                        EpcSizeBits = 96;
                        UserMemorySizeBits = 0;
                        SupportsQt = false;
                        break;
                    case "170":
                        ModelName = TagModelName.MonzaR6_P;
                        EpcSizeBits = 0;
                        UserMemorySizeBits = 0;
                        SupportsQt = false;
                        break;
                    case "171":
                        ModelName = TagModelName.MonzaR6_A;
                        EpcSizeBits = 96;
                        UserMemorySizeBits = 0;
                        SupportsQt = false;
                        break;
                    case "173":
                        ModelName = TagModelName.MonzaS6_C;
                        EpcSizeBits = 96;
                        UserMemorySizeBits = 32;
                        SupportsQt = false;
                        break;
                }
            }
        }

        /// <summary>The model name of the tag.</summary>
        public TagModelName ModelName { get; private set; }

        /// <summary>The size of the User memory bank (in bits).</summary>
        public ushort UserMemorySizeBits { get; private set; }

        /// <summary>
        /// The maximum supported EPC size (in bits).
        /// This size does not include the PC bits or CRC.
        /// </summary>
        public ushort EpcSizeBits { get; private set; }

        /// <summary>
        /// Indicates whether or not the tag supports the
        /// %Impinj QT features.
        /// </summary>
        public bool SupportsQt { get; private set; }
    }
}
