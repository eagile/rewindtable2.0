using System.Collections.Generic;

namespace EA.Impinj
{
    /// <summary>
    /// Container class encapsulating all of the antenna status objects.
    /// This is for internal use only, and does not appear in the generated
    /// documentation.
    /// </summary>
    public class AntennaStatusGroup : List<AntennaStatus>
    {
        private readonly bool _createdBySerializer;

        public AntennaStatusGroup()
        {
            _createdBySerializer = true;
        }

        /// <summary>Returns the status for the specified antenna port.</summary>
        /// <returns>
        /// The antenna status or throws an exception if the port does not exist.
        /// </returns>
        public AntennaStatus GetAntenna(ushort antennaNumber)
        {
            foreach (AntennaStatus antennaStatuse in this)
            {
                if (antennaStatuse.PortNumber == antennaNumber)
                    return antennaStatuse;
            }
            throw new OctaneSdkException("Invalid antenna number specified : " + antennaNumber);
        }

        /// <summary>
        /// Adds the provided antenna status object to the collection.
        /// For internal library use only.
        /// </summary>
        /// <param name="status">The AntennaStatus object to add.</param>
        public void Add(object status)
        {
            if (!_createdBySerializer)
                throw new OctaneSdkException("Invalid operation - cannot Add to AntennaStatusGroup object!");
            base.Add((AntennaStatus)status);
        }
    }
}
