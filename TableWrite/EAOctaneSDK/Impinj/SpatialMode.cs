namespace EA.Impinj
{
    /// <summary>
    /// Enum for defining a reader with direction or location role capabilities' operational mode.
    /// </summary>
    public enum SpatialMode
    {
        /// <summary>
        /// Inventory mode. Reader will perform a tag inventory on all antennas.
        /// </summary>
        Inventory,
        /// <summary>Location mode. Reader will report tag location.</summary>
        Location,
        /// <summary>Direction mode. Reader will report tag direction.</summary>
        Direction,
    }
}
