namespace EA.Impinj
{
    /// <summary>
    /// Enum for determining which lock operation to perform on the tag memory bank;
    /// this determines what state to leave the tag memory bank in.
    /// </summary>
    public enum TagLockState
    {
        /// <summary>
        /// Leave the tag memory bank in the 'Lock' state;
        /// the tag memory bank can subsequently be unlocked.
        /// </summary>
        Lock = 0,
        /// <summary>
        /// Leave the tag memory bank in the 'Permalock' state;
        /// the tag memory bank can never subsequently be unlocked.
        /// </summary>
        Permalock = 1,
        /// <summary>
        /// Leave the tag memory bank in the 'Permaunlock' state;
        /// the tag memory bank can never subsequently be locked.
        /// </summary>
        Permaunlock = 2,
        /// <summary>
        /// Leave the tag memory bank in the 'Unlock' state;
        /// the tag memory bank can subsequently be locked.
        /// </summary>
        Unlock = 3,
        /// <summary>Do not change the lock state of the tag memory bank.</summary>
        None = 4,
    }
}
