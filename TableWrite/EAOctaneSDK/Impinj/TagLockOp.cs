using System;

namespace EA.Impinj
{
    /// <summary>Class used to specify a tag lock operation.</summary>
    public class TagLockOp : TagOp
    {
        /// <summary>
        /// Tag access password.  Required only if an access password has been set
        /// for the target memory bank.
        /// </summary>
        public TagData AccessPassword
        {
            get => _accessPassword;
            set => _accessPassword = value ?? new TagData();
        }
        private TagData _accessPassword = new TagData();

        /// <summary>Lock operation to perform on the Kill Password.</summary>
        public TagLockState KillPasswordLockType { get; set; } = TagLockState.None;

        /// <summary>Lock operation to perform on the Access Password.</summary>
        public TagLockState AccessPasswordLockType { get; set; } = TagLockState.None;

        /// <summary>Lock operation to perform on the EPC memory bank.</summary>
        public TagLockState EpcLockType { get; set; } = TagLockState.None;

        /// <summary>Lock operation to perform on the TID memory bank.</summary>
        public TagLockState TidLockType { get; set; } = TagLockState.None;

        /// <summary>Lock operation to perform on the User memory bank.</summary>
        public TagLockState UserLockType { get; set; } = TagLockState.None;

        /// <summary>The number of locks specified in this operation.</summary>
        public ushort LockCount
        {
            get
            {
                ushort num = 0;
                if (KillPasswordLockType != TagLockState.None)
                    ++num;
                if (AccessPasswordLockType != TagLockState.None)
                    ++num;
                if (EpcLockType != TagLockState.None)
                    ++num;
                if (TidLockType != TagLockState.None)
                    ++num;
                if (UserLockType != TagLockState.None)
                    ++num;
                return num;
            }
        }



        public TagLockOp() : this(GetNextTagOpID()) { }
        public TagLockOp(UInt16 tagOpId) : base(tagOpId) { }
    }
}
