namespace EA.Impinj
{
    /// <summary>
    /// Enumeration used to manage whether switching to the private Monza QT Data Profile
    /// should be persistent, or whether the tag should revert to the private state
    /// once the tag loses power.
    /// </summary>
    public enum QtPersistence
    {
        /// <summary>Leave the tag persistence configuration as-is.</summary>
        Unknown,
        /// <summary>
        /// Stay in the private data profile a until the tag loses power, when it will
        /// automatically revert to the Public profile.
        /// </summary>
        Temporary,
        /// <summary>Stay in the private data profile.</summary>
        Permanent,
    }
}
