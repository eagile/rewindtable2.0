namespace EA.Impinj
{
    /// <summary>
    /// Specified if a narrow or wide ring should be used when reading and tracking tag direction.
    /// It wraps the equivalent LTK type ENUM_ImpinjDirectionFieldOfView.
    /// </summary>
    public enum DirectionFieldOfView
    {
        /// <summary>Field of view is determined by the reader.</summary>
        ReaderSelected,
        /// <summary>
        /// Specifies a wider field for monitoring tag direction, corresponding to rings 2 and 4.
        /// </summary>
        Wide,
        /// <summary>
        /// Specifies a narrow field for monitoring tag direction, corresponding to ring 2.
        /// </summary>
        Narrow,
    }
}
