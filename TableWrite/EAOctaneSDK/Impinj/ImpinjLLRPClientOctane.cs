using EA.Impinj.LLRP;
using EA.Impinj.LLRP.Impinj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EA.Impinj
{
    /// <summary>
    /// Impinj LLRP Client with Octane command wrappers
    /// </summary>
    public class ImpinjLLRPClientOctane : ImpinjLLRPClient
    {
        private const UInt32 RO_SPEC_ID = 14150;

        public FeatureSet ReaderCapabilities { get; private set; }

        public bool IsXArray => ReaderCapabilities?.ReaderModel == ReaderModel.XArray || ReaderCapabilities?.ReaderModel == ReaderModel.XArrayEAP || ReaderCapabilities?.ReaderModel == ReaderModel.XArrayWM;

        public bool IsXSpan => ReaderCapabilities?.ReaderModel == ReaderModel.XSpan || ReaderCapabilities?.ReaderModel == ReaderModel.XSpanR703 || ReaderCapabilities?.ReaderModel == ReaderModel.XSpanR705;

        public bool IsSpatialReader => IsXArray || IsXSpan;



        protected override async Task InitializeStreamAsync()
        {
            await EnableImpinjExtensionsAsync().ConfigureAwait(false);
            await GetReaderCapabilitiesAsync().ConfigureAwait(false);
            if (ReaderCapabilities.ModelNumber == 1000000)
            {
                throw new NotImplementedException("The Octane SDK does not support the Speedway R1000 reader.");
            }
        }

        protected override Task FinalizeStreamAsync()
        {
            ReaderCapabilities = null;
            return Task.CompletedTask;
        }





        protected async Task EnableImpinjExtensionsAsync()
        {
            if (!IsStreamConnected)
                throw new InvalidOperationException("Reader disconnected");

            MSG_IMPINJ_ENABLE_EXTENSIONS msg = new MSG_IMPINJ_ENABLE_EXTENSIONS(GetNextMessageID());
            await IMPINJ_ENABLE_EXTENSIONS(msg).ConfigureAwait(false);
        }

        protected async Task GetReaderCapabilitiesAsync()
        {
            if (!IsStreamConnected)
                throw new InvalidOperationException("Reader disconnected");

            MSG_GET_READER_CAPABILITIES msg = new MSG_GET_READER_CAPABILITIES(GetNextMessageID());
            MSG_GET_READER_CAPABILITIES_RESPONSE response = await GET_READER_CAPABILITIES(msg).ConfigureAwait(false);
            ReaderCapabilities = new FeatureSet(response);
        }




        public async Task StartAsync()
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            await EnableRoSpecAsync(RO_SPEC_ID).ConfigureAwait(false);
            await StartRoSpecAsync(RO_SPEC_ID).ConfigureAwait(false);
        }

        public async Task StopAsync()
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            await DisableRoSpecAsync(RO_SPEC_ID).ConfigureAwait(false);
        }



        public async Task GetTagReportsAsync()
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            MSG_GET_REPORT msg = new MSG_GET_REPORT(GetNextMessageID());
            await GET_REPORT(msg).ConfigureAwait(false);
        }

        public async Task ResumeEventsAndReportsAsync()
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            MSG_ENABLE_EVENTS_AND_REPORTS msg = new MSG_ENABLE_EVENTS_AND_REPORTS(GetNextMessageID());
            await ENABLE_EVENTS_AND_REPORTS(msg).ConfigureAwait(false);
        }




        public async Task EnableBeaconAsync(ulong duration)
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            MSG_SET_READER_CONFIG msg = new MSG_SET_READER_CONFIG(GetNextMessageID());
            msg.TryAddCustomParameter(new PARAM_ImpinjBeaconConfiguration()
            {
                BeaconDurationSeconds = duration,
                BeaconState = true
            });
            await SET_READER_CONFIG(msg).ConfigureAwait(false);
        }

        public async Task DisableBeaconAsync()
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            MSG_SET_READER_CONFIG msg = new MSG_SET_READER_CONFIG(GetNextMessageID());
            msg.TryAddCustomParameter(new PARAM_ImpinjBeaconConfiguration()
            {
                BeaconState = false
            });
            await SET_READER_CONFIG(msg).ConfigureAwait(false);
        }



        public async Task SetGpoAsync(int port, bool state)
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            MSG_SET_READER_CONFIG msg = new MSG_SET_READER_CONFIG(GetNextMessageID())
            {
                GPOWriteData = new PARAM_GPOWriteData[]
                {
                    new PARAM_GPOWriteData()
                    {
                         GPOPortNumber = (ushort)port,
                         GPOData = state
                    }
                }
            };
            await SET_READER_CONFIG(msg).ConfigureAwait(false);
        }

        public async Task SetGposAsync(Dictionary<int, bool> outputStates)
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            MSG_SET_READER_CONFIG msg = new MSG_SET_READER_CONFIG(GetNextMessageID())
            {
                GPOWriteData = outputStates.Select(outputState => new PARAM_GPOWriteData
                {
                    GPOPortNumber = (ushort)outputState.Key,
                    GPOData = outputState.Value
                }).ToArray()
            };
            MSG_SET_READER_CONFIG_RESPONSE response = await SET_READER_CONFIG(msg).ConfigureAwait(false);
        }

        public async Task ApplySettingsAsync(ImpinjSettings settings)
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            MSG_SET_READER_CONFIG setReaderConfigMessage = BuildSetReaderConfigMessage(settings);
            MSG_ADD_ROSPEC addRoSpecMessage = BuildAddROSpecMessage(settings);

            await ResetToFactoryDefaultsAsync().ConfigureAwait(false);
            await DeleteAllRoSpecsAsync().ConfigureAwait(false);
            await DeleteAllAccessSpecsAsync().ConfigureAwait(false);
            await EnableImpinjExtensionsAsync().ConfigureAwait(false);
            await SetReaderConfigAsync(setReaderConfigMessage).ConfigureAwait(false);
            await AddRoSpecAsync(addRoSpecMessage).ConfigureAwait(false);
            await EnableRoSpecAsync(addRoSpecMessage.ROSpec.ROSpecID).ConfigureAwait(false);

            if (settings.Keepalives.Enabled)
            {
                EnableKeepAlives((int)(settings.Keepalives.PeriodInMs * 1.5));
            }
        }

        public async Task<ImpinjSettings> QuerySettingsAsync()
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            MSG_GET_READER_CONFIG_RESPONSE readerConfig = await GetAllReaderConfigAsync().ConfigureAwait(false);
            MSG_GET_ROSPECS_RESPONSE roSpecs = await GetRoSpecsAsync().ConfigureAwait(false);
            PARAM_ROSpec[] roSpec = roSpecs.ROSpec;
            if (roSpec == null || roSpec.Length != 1 || roSpec[0] == null || readerConfig == null)
                throw new OctaneSdkException("The reader has not been configured.");
            if (roSpec[0].ROSpecID != RO_SPEC_ID)
                throw new OctaneSdkException("The reader configuration is invalid.");
            return ParseRoSpecAndConfig(roSpec[0], readerConfig);
        }

        private async Task SetReaderConfigAsync(MSG_SET_READER_CONFIG msg)
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            await SET_READER_CONFIG(msg).ConfigureAwait(false);
        }

        public async Task ResetToFactoryDefaultsAsync()
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            MSG_SET_READER_CONFIG msg = new MSG_SET_READER_CONFIG(GetNextMessageID())
            {
                ResetToFactoryDefault = true
            };
            await SET_READER_CONFIG(msg).ConfigureAwait(false);
        }

        public async Task SaveSettingsAsync()
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            MSG_IMPINJ_SAVE_SETTINGS msg = new MSG_IMPINJ_SAVE_SETTINGS(GetNextMessageID())
            {
                SaveConfiguration = true
            };
            await IMPINJ_SAVE_SETTINGS(msg).ConfigureAwait(false);
        }

        public async Task<Status> QueryStatusAsync()
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            MSG_GET_ROSPECS_RESPONSE roSpecs = await GetRoSpecsAsync().ConfigureAwait(false);
            MSG_GET_READER_CONFIG_RESPONSE readerConfig = await GetAllReaderConfigAsync().ConfigureAwait(false);

            Status status = new Status
            {
                IsConnected = IsConnected
            };

            PARAM_ROSpec[] roSpec = roSpecs.ROSpec;
            status.IsSingulating = roSpec != null && roSpec[0].CurrentState == ENUM_ROSpecState.Active;

            if (readerConfig.AntennaProperties != null)
            {
                foreach (PARAM_AntennaProperties antennaProperty in readerConfig.AntennaProperties)
                {
                    status.Antennas.Add(new AntennaStatus()
                    {
                        PortNumber = antennaProperty.AntennaID,
                        IsConnected = antennaProperty.AntennaConnected
                    });
                }
            }
            if (readerConfig.GPIPortCurrentState != null)
            {
                foreach (PARAM_GPIPortCurrentState portCurrentState in readerConfig.GPIPortCurrentState)
                {
                    status.Gpis.Add(new GpiStatus()
                    {
                        PortNumber = portCurrentState.GPIPortNum,
                        State = portCurrentState.State == ENUM_GPIPortState.High
                    });
                }
            }
            status.AntennaHubs = new AntennaHubStatusGroup();
            if (readerConfig.Custom != null)
            {
                foreach (ILLRPCustomParameter custParam in readerConfig.Custom)
                {
                    if (custParam is PARAM_ImpinjReaderTemperature readerTemperature)
                    {
                        status.TemperatureInCelsius = readerTemperature.Temperature;
                    }
                    else if (custParam is PARAM_ImpinjHubConfiguration hubConfiguration)
                    {
                        status.AntennaHubs.Add(new AntennaHubStatus()
                        {
                            HubId = hubConfiguration.HubID,
                            Connected = (HubConnectedStatus)hubConfiguration.Connected,
                            Fault = (HubFaultStatus)hubConfiguration.Fault
                        });
                    }
                    else if (custParam is PARAM_ImpinjTiltConfiguration tiltConfiguration)
                    {
                        status.TiltSensor = new TiltSensorValue()
                        {
                            XAxis = tiltConfiguration.XAxis,
                            YAxis = tiltConfiguration.YAxis
                        };
                    }
                }
            }
            return status;
        }

        public async Task<bool> QueryIsSingulatingAsync()
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            MSG_GET_ROSPECS_RESPONSE roSpecs = await GetRoSpecsAsync().ConfigureAwait(false);
            PARAM_ROSpec[] roSpec = roSpecs.ROSpec;
            bool isSingulating = roSpec != null && roSpec.Length == 1 && roSpec[0].CurrentState == ENUM_ROSpecState.Active;
            return isSingulating;
        }

        public async Task<short> QueryTemperatureAsync()
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            short temperatureInCelsius = 0;
            MSG_GET_READER_CONFIG_RESPONSE readerConfig = await GetReaderConfigAsync(ENUM_GetReaderConfigRequestedData.Identification, ENUM_ImpinjRequestedDataType.Impinj_Reader_Temperature).ConfigureAwait(false);
            if (readerConfig.Custom != null)
            {
                foreach (ILLRPCustomParameter custParam in readerConfig.Custom)
                {
                    if (custParam is PARAM_ImpinjReaderTemperature readerTemperature)
                    {
                        temperatureInCelsius = readerTemperature.Temperature;
                    }
                }
            }
            return temperatureInCelsius;
        }

        public async Task<GpiStatusGroup> QueryGpisAsync()
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            GpiStatusGroup gpis = new GpiStatusGroup();
            MSG_GET_READER_CONFIG_RESPONSE readerConfig = await GetReaderConfigAsync(ENUM_GetReaderConfigRequestedData.GPIPortCurrentState, null).ConfigureAwait(false);
            if (readerConfig.GPIPortCurrentState != null)
            {
                foreach (PARAM_GPIPortCurrentState portCurrentState in readerConfig.GPIPortCurrentState)
                {
                    gpis.Add(new GpiStatus()
                    {
                        PortNumber = portCurrentState.GPIPortNum,
                        State = portCurrentState.State == ENUM_GPIPortState.High
                    });
                }
            }
            return gpis;
        }

        public async Task<AntennaStatusGroup> QueryAntennasAsync()
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            AntennaStatusGroup antennas = new AntennaStatusGroup();
            MSG_GET_READER_CONFIG_RESPONSE readerConfig = await GetReaderConfigAsync(ENUM_GetReaderConfigRequestedData.AntennaProperties, null).ConfigureAwait(false);
            if (readerConfig.AntennaProperties != null)
            {
                foreach (PARAM_AntennaProperties antennaProperty in readerConfig.AntennaProperties)
                {
                    antennas.Add(new AntennaStatus()
                    {
                        PortNumber = antennaProperty.AntennaID,
                        IsConnected = antennaProperty.AntennaConnected
                    });
                }
            }
            return antennas;
        }

        private Task<MSG_GET_READER_CONFIG_RESPONSE> GetAllReaderConfigAsync()
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            return GetReaderConfigAsync(ENUM_GetReaderConfigRequestedData.All, ENUM_ImpinjRequestedDataType.All_Configuration);
        }

        private async Task<MSG_GET_READER_CONFIG_RESPONSE> GetReaderConfigAsync(ENUM_GetReaderConfigRequestedData requestedData, ENUM_ImpinjRequestedDataType? impinjRequestedData)
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            MSG_GET_READER_CONFIG msg = new MSG_GET_READER_CONFIG(GetNextMessageID())
            {
                RequestedData = requestedData
            };
            if (impinjRequestedData != null)
            {
                msg.Custom.Add(new PARAM_ImpinjRequestedData()
                {
                    RequestedData = (ENUM_ImpinjRequestedDataType)impinjRequestedData
                });
            }
            MSG_GET_READER_CONFIG_RESPONSE response = await GET_READER_CONFIG(msg).ConfigureAwait(false);
            return response;
        }








        public async Task<MSG_GET_ROSPECS_RESPONSE> GetRoSpecsAsync()
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            MSG_GET_ROSPECS msg = new MSG_GET_ROSPECS(GetNextMessageID());
            MSG_GET_ROSPECS_RESPONSE response = await GET_ROSPECS(msg).ConfigureAwait(false);
            return response;
        }

        public async Task AddRoSpecAsync(MSG_ADD_ROSPEC msg)
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            await ADD_ROSPEC(msg).ConfigureAwait(false);
        }

        public async Task DeleteRoSpecAsync(uint roSpecId)
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            MSG_DELETE_ROSPEC msg = new MSG_DELETE_ROSPEC(GetNextMessageID())
            {
                ROSpecID = roSpecId
            };
            await DELETE_ROSPEC(msg).ConfigureAwait(false);
        }

        public async Task EnableRoSpecAsync(uint roSpecId)
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            MSG_ENABLE_ROSPEC msg = new MSG_ENABLE_ROSPEC(GetNextMessageID())
            {
                ROSpecID = roSpecId
            };
            await ENABLE_ROSPEC(msg).ConfigureAwait(false);
        }

        public async Task DisableRoSpecAsync(uint roSpecId)
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            MSG_DISABLE_ROSPEC msg = new MSG_DISABLE_ROSPEC(GetNextMessageID())
            {
                ROSpecID = roSpecId
            };
            await DISABLE_ROSPEC(msg).ConfigureAwait(false);
        }

        public async Task StartRoSpecAsync(uint roSpecId)
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            MSG_START_ROSPEC msg = new MSG_START_ROSPEC(GetNextMessageID())
            {
                ROSpecID = roSpecId
            };
            await START_ROSPEC(msg).ConfigureAwait(false);
        }

        public async Task StopRoSpecAsync(uint roSpecId)
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            MSG_STOP_ROSPEC msg = new MSG_STOP_ROSPEC(GetNextMessageID())
            {
                ROSpecID = roSpecId
            };
            await STOP_ROSPEC(msg).ConfigureAwait(false);
        }

        public Task EnableAllRoSpecsAsync()
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            return EnableRoSpecAsync(0);
        }

        public Task DisableAllRoSpecsAsync()
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            return DisableRoSpecAsync(0);
        }

        public Task DeleteAllRoSpecsAsync()
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            return DeleteRoSpecAsync(0);
        }


        public async Task<List<uint>> GetOpSequenceIDsAsync()
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            List<uint> ids = new List<uint>();
            MSG_GET_ACCESSSPECS_RESPONSE msg = await GetAccessSpecsAsync().ConfigureAwait(false);
            if (msg.AccessSpec != null)
            {
                foreach (PARAM_AccessSpec accessSpec in msg.AccessSpec)
                {
                    ids.Add(accessSpec.AccessSpecID);
                }
            }
            return ids;
        }

        public async Task AddOpSequenceAsync(TagOpSequence sequence)
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            await AddAccessSpecAsync(sequence).ConfigureAwait(false);
            if (sequence.State == SequenceState.Active)
            {
                await EnableAccessSpecAsync(sequence.Id).ConfigureAwait(false);
            }
        }

        public async Task<MSG_GET_ACCESSSPECS_RESPONSE> GetAccessSpecsAsync()
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            MSG_GET_ACCESSSPECS msg = new MSG_GET_ACCESSSPECS(GetNextMessageID());
            MSG_GET_ACCESSSPECS_RESPONSE accessSpecs = await GET_ACCESSSPECS(msg).ConfigureAwait(false);
            return accessSpecs;
        }

        private async Task AddAccessSpecAsync(TagOpSequence sequence)
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            if (sequence == null || sequence.Ops.Count == 0)
                throw new OctaneSdkException("Cannot add an empty operation sequence");

            MSG_ADD_ACCESSSPEC msg = new MSG_ADD_ACCESSSPEC(GetNextMessageID())
            {
                AccessSpec = new PARAM_AccessSpec()
            };
            msg.AccessSpec.AntennaID = sequence.AntennaId;
            msg.AccessSpec.ROSpecID = 0U;
            msg.AccessSpec.ProtocolID = ENUM_AirProtocols.EPCGlobalClass1Gen2;
            msg.AccessSpec.CurrentState = ENUM_AccessSpecState.Disabled;
            msg.AccessSpec.AccessSpecStopTrigger = new PARAM_AccessSpecStopTrigger();
            if (sequence.SequenceStopTrigger == SequenceTriggerType.ExecutionCount)
            {
                msg.AccessSpec.AccessSpecStopTrigger.AccessSpecStopTrigger = ENUM_AccessSpecStopTriggerType.Operation_Count;
                msg.AccessSpec.AccessSpecStopTrigger.OperationCountValue = sequence.ExecutionCount;
            }
            else
            {
                msg.AccessSpec.AccessSpecStopTrigger.AccessSpecStopTrigger = ENUM_AccessSpecStopTriggerType.Null;
            }
            msg.AccessSpec.AccessSpecID = sequence.Id;
            msg.AccessSpec.AccessCommand = new PARAM_AccessCommand
            {
                AirProtocolTagSpec = new UNION_AirProtocolTagSpec()
            };
            PARAM_C1G2TagSpec paramC1G2TagSpec = new PARAM_C1G2TagSpec
            {
                C1G2TargetTag = new PARAM_C1G2TargetTag[1]
            };
            paramC1G2TagSpec.C1G2TargetTag[0] = new PARAM_C1G2TargetTag();
            if (sequence.TargetTag.Data != null)
            {
                if (sequence.TargetTag.Mask == null)
                {
                    sequence.TargetTag.Mask = "";
                    for (int index = 0; index < sequence.TargetTag.Data.Length; ++index)
                    {
                        sequence.TargetTag.Mask += "F";
                    }
                }
                paramC1G2TagSpec.C1G2TargetTag[0].Match = true;
                paramC1G2TagSpec.C1G2TargetTag[0].MB = new TwoBits((ushort)sequence.TargetTag.MemoryBank);
                paramC1G2TagSpec.C1G2TargetTag[0].Pointer = sequence.TargetTag.BitPointer;
                paramC1G2TagSpec.C1G2TargetTag[0].TagData = LLRPUtil.HexStringToBitList(sequence.TargetTag.Data);
                paramC1G2TagSpec.C1G2TargetTag[0].TagMask = LLRPUtil.HexStringToBitList(sequence.TargetTag.Mask);
            }
            else
            {
                paramC1G2TagSpec.C1G2TargetTag[0].Match = true;
            }
            msg.AccessSpec.AccessCommand.AirProtocolTagSpec.Add(paramC1G2TagSpec);
            msg.AccessSpec.AccessCommand.AccessCommandOpSpec = new UNION_AccessCommandOpSpec();
            foreach (TagOp op in sequence.Ops)
            {
                if (op is TagReadOp tagReadOp)
                {
                    msg.AccessSpec.AccessCommand.AccessCommandOpSpec.Add(new PARAM_C1G2Read()
                    {
                        AccessPassword = tagReadOp.AccessPassword.ToUnsignedInt(),
                        MB = new TwoBits((ushort)tagReadOp.MemoryBank),
                        OpSpecID = tagReadOp.Id,
                        WordCount = tagReadOp.WordCount,
                        WordPointer = tagReadOp.WordPointer
                    });
                }
                else if (op is TagWriteOp tagWriteOp)
                {
                    if (sequence.BlockWriteEnabled)
                    {
                        msg.AccessSpec.AccessCommand.AccessCommandOpSpec.Add(new PARAM_C1G2BlockWrite()
                        {
                            AccessPassword = tagWriteOp.AccessPassword.ToUnsignedInt(),
                            MB = new TwoBits((ushort)tagWriteOp.MemoryBank),
                            OpSpecID = tagWriteOp.Id,
                            WriteData = new UInt16List(tagWriteOp.Data.Words),
                            WordPointer = tagWriteOp.WordPointer
                        });
                    }
                    else
                    {
                        msg.AccessSpec.AccessCommand.AccessCommandOpSpec.Add(new PARAM_C1G2Write()
                        {
                            AccessPassword = tagWriteOp.AccessPassword.ToUnsignedInt(),
                            MB = new TwoBits((ushort)tagWriteOp.MemoryBank),
                            OpSpecID = tagWriteOp.Id,
                            WriteData = new UInt16List(tagWriteOp.Data.Words),
                            WordPointer = tagWriteOp.WordPointer
                        });
                    }
                }
                else if (op is TagLockOp tagLockOp)
                {
                    PARAM_C1G2Lock paramC1G2Lock = new PARAM_C1G2Lock();
                    if (tagLockOp.LockCount == 0)
                    {
                        throw new OctaneSdkException("The TagLockOp does not specify any lock operations.");
                    }
                    paramC1G2Lock.OpSpecID = tagLockOp.Id;
                    paramC1G2Lock.AccessPassword = tagLockOp.AccessPassword.ToUnsignedInt();
                    paramC1G2Lock.C1G2LockPayload = new PARAM_C1G2LockPayload[tagLockOp.LockCount];
                    int index = 0;
                    if (tagLockOp.KillPasswordLockType != TagLockState.None)
                    {
                        paramC1G2Lock.C1G2LockPayload[index++] = new PARAM_C1G2LockPayload()
                        {
                            Privilege = GetLockPrivilege(tagLockOp.KillPasswordLockType),
                            DataField = ENUM_C1G2LockDataField.Kill_Password
                        };
                    }
                    if (tagLockOp.AccessPasswordLockType != TagLockState.None)
                    {
                        paramC1G2Lock.C1G2LockPayload[index++] = new PARAM_C1G2LockPayload()
                        {
                            Privilege = GetLockPrivilege(tagLockOp.AccessPasswordLockType),
                            DataField = ENUM_C1G2LockDataField.Access_Password
                        };
                    }
                    if (tagLockOp.EpcLockType != TagLockState.None)
                    {
                        paramC1G2Lock.C1G2LockPayload[index++] = new PARAM_C1G2LockPayload()
                        {
                            Privilege = GetLockPrivilege(tagLockOp.EpcLockType),
                            DataField = ENUM_C1G2LockDataField.EPC_Memory
                        };
                    }
                    if (tagLockOp.TidLockType != TagLockState.None)
                    {
                        paramC1G2Lock.C1G2LockPayload[index++] = new PARAM_C1G2LockPayload()
                        {
                            Privilege = GetLockPrivilege(tagLockOp.TidLockType),
                            DataField = ENUM_C1G2LockDataField.TID_Memory
                        };
                    }
                    if (tagLockOp.UserLockType != TagLockState.None)
                    {
                        paramC1G2Lock.C1G2LockPayload[index++] = new PARAM_C1G2LockPayload()
                        {
                            Privilege = GetLockPrivilege(tagLockOp.UserLockType),
                            DataField = ENUM_C1G2LockDataField.User_Memory
                        };
                    }
                    msg.AccessSpec.AccessCommand.AccessCommandOpSpec.Add(paramC1G2Lock);
                }
                else if (op is TagQtSetOp tagQtSetOp)
                {
                    PARAM_ImpinjSetQTConfig impinjSetQtConfig = new PARAM_ImpinjSetQTConfig()
                    {
                        DataProfile = ENUM_ImpinjQTDataProfile.Unknown,
                        OpSpecID = tagQtSetOp.Id,
                        AccessPassword = tagQtSetOp.AccessPassword.ToUnsignedInt(),
                        Persistence = (ENUM_ImpinjQTPersistence)tagQtSetOp.Persistence
                    };
                    impinjSetQtConfig.DataProfile = (ENUM_ImpinjQTDataProfile)tagQtSetOp.DataProfile;
                    impinjSetQtConfig.AccessRange = (ENUM_ImpinjQTAccessRange)tagQtSetOp.AccessRange;
                    msg.AccessSpec.AccessCommand.AccessCommandOpSpec.TryAddCustomParameter(impinjSetQtConfig);
                }
                else if (op is TagQtGetOp tagQtGetOp)
                {
                    msg.AccessSpec.AccessCommand.AccessCommandOpSpec.TryAddCustomParameter(new PARAM_ImpinjGetQTConfig()
                    {
                        AccessPassword = tagQtGetOp.AccessPassword.ToUnsignedInt()
                    });
                }
                else if (op is TagKillOp tagKillOp)
                {
                    msg.AccessSpec.AccessCommand.AccessCommandOpSpec.Add(new PARAM_C1G2Kill()
                    {
                        KillPassword = tagKillOp.KillPassword.ToUnsignedInt(),
                        OpSpecID = tagKillOp.Id
                    });
                }
                else if (op is TagBlockPermalockOp blockPermalockOp)
                {
                    msg.AccessSpec.AccessCommand.AccessCommandOpSpec.Add(new PARAM_ImpinjBlockPermalock()
                    {
                        AccessPassword = blockPermalockOp.AccessPassword.ToUnsignedInt(),
                        MB = new TwoBits((ushort)blockPermalockOp.MemoryBank),
                        OpSpecID = blockPermalockOp.Id,
                        BlockPointer = blockPermalockOp.BlockPointer,
                        BlockMask = new UInt16List(blockPermalockOp.BlockMask.Mask),
                    });
                }
                else if (op is TagMarginReadOp tagMarginReadOp)
                {
                    msg.AccessSpec.AccessCommand.AccessCommandOpSpec.Add(new PARAM_ImpinjMarginRead()
                    {
                        AccessPassword = tagMarginReadOp.AccessPassword.ToUnsignedInt(),
                        MB = new TwoBits((ushort)tagMarginReadOp.MemoryBank),
                        OpSpecID = tagMarginReadOp.Id,
                        BitPointer = tagMarginReadOp.BitPointer,
                        BitLength = tagMarginReadOp.MarginMask.BitLength,
                        Mask = new UInt16List(tagMarginReadOp.MarginMask.Mask)
                    });
                }
            }
            msg.AccessSpec.AccessReportSpec = new PARAM_AccessReportSpec
            {
                AccessReportTrigger = ENUM_AccessReportTriggerType.Whenever_ROReport_Is_Generated
            };
            if (sequence.BlockWriteEnabled || sequence.RetryCount > 0)
            {
                PARAM_ImpinjAccessSpecConfiguration specConfiguration = new PARAM_ImpinjAccessSpecConfiguration();
                if (sequence.BlockWriteEnabled)
                {
                    specConfiguration.ImpinjBlockWriteWordCount = new PARAM_ImpinjBlockWriteWordCount
                    {
                        WordCount = sequence.BlockWriteWordCount
                    };
                }
                if (sequence.RetryCount > 0)
                {
                    specConfiguration.ImpinjOpSpecRetryCount = new PARAM_ImpinjOpSpecRetryCount
                    {
                        RetryCount = sequence.RetryCount
                    };
                }
                msg.AccessSpec.TryAddCustomParameter(specConfiguration);
            }
            await ADD_ACCESSSPEC(msg).ConfigureAwait(false);
        }

        public async Task DeleteAccessSpecAsync(uint accessSpecID)
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            MSG_DELETE_ACCESSSPEC msg = new MSG_DELETE_ACCESSSPEC(GetNextMessageID())
            {
                AccessSpecID = accessSpecID
            };
            await DELETE_ACCESSSPEC(msg).ConfigureAwait(false);
        }

        public async Task EnableAccessSpecAsync(uint accessSpecID)
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            MSG_ENABLE_ACCESSSPEC msg = new MSG_ENABLE_ACCESSSPEC(GetNextMessageID())
            {
                AccessSpecID = accessSpecID
            };
            await ENABLE_ACCESSSPEC(msg).ConfigureAwait(false);
        }

        public async Task DisableAccessSpecAsync(uint accessSpecID)
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            MSG_DISABLE_ACCESSSPEC msg = new MSG_DISABLE_ACCESSSPEC(GetNextMessageID())
            {
                AccessSpecID = accessSpecID
            };
            await DISABLE_ACCESSSPEC(msg).ConfigureAwait(false);
        }

        public Task DeleteAllAccessSpecsAsync()
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            return DeleteAccessSpecAsync(0);
        }

        public Task EnableAllAccessSpecsAsync()
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            return EnableAccessSpecAsync(0);
        }

        public Task DisableAllAccessSpecsAsync()
        {
            if (!IsConnected)
                throw new InvalidOperationException("Reader disconnected");

            return DisableAccessSpecAsync(0);
        }





        private ENUM_C1G2LockPrivilege GetLockPrivilege(TagLockState tagLockState)
        {
            switch (tagLockState)
            {
                case TagLockState.Unlock:
                    return ENUM_C1G2LockPrivilege.Unlock;
                case TagLockState.Permaunlock:
                    return ENUM_C1G2LockPrivilege.Perma_Unlock;
                case TagLockState.Lock:
                    return ENUM_C1G2LockPrivilege.Read_Write;
                case TagLockState.Permalock:
                    return ENUM_C1G2LockPrivilege.Perma_Lock;
                default:
                    throw new OctaneSdkException("Invalid TagLockState");
            }
        }

        private MSG_SET_READER_CONFIG BuildSetReaderConfigMessage(ImpinjSettings settings)
        {
            MSG_SET_READER_CONFIG readerConfig = new MSG_SET_READER_CONFIG(GetNextMessageID())
            {
                ROReportSpec = new PARAM_ROReportSpec()
            };
            readerConfig.ROReportSpec.TagReportContentSelector = new PARAM_TagReportContentSelector();
            readerConfig.ROReportSpec.ROReportTrigger = ENUM_ROReportTriggerType.Upon_N_Tags_Or_End_Of_ROSpec;
            readerConfig.ROReportSpec.N = 1;
            if (settings.TxFrequenciesInMhz != null && settings.TxFrequenciesInMhz.Count > 0)
            {
                PARAM_ImpinjFixedFrequencyList fixedFrequencyList = new PARAM_ImpinjFixedFrequencyList
                {
                    FixedFrequencyMode = ENUM_ImpinjFixedFrequencyMode.Channel_List,
                    ChannelList = new UInt16List()
                };
                foreach (double freq in settings.TxFrequenciesInMhz)
                {
                    ushort etsiTxFreqIndex = (ushort)(ReaderCapabilities.TxFrequencies.IndexOf(freq) + 1);
                    if (etsiTxFreqIndex == 0)
                        throw new OctaneSdkException("The specified transmit frequency is invalid : " + freq);
                    fixedFrequencyList.ChannelList.Add(etsiTxFreqIndex);
                }
                PARAM_C1G2InventoryCommand inventoryCommand = new PARAM_C1G2InventoryCommand();
                inventoryCommand.Custom.Add(fixedFrequencyList);
                PARAM_AntennaConfiguration antennaConfiguration = new PARAM_AntennaConfiguration
                {
                    AntennaID = 0
                };
                antennaConfiguration.AirProtocolInventoryCommandSettings.Add(inventoryCommand);
                readerConfig.AntennaConfiguration = new PARAM_AntennaConfiguration[1];
                readerConfig.AntennaConfiguration[0] = antennaConfiguration;
            }
            if (settings.Keepalives.Enabled)
            {
                readerConfig.KeepaliveSpec = new PARAM_KeepaliveSpec()
                {
                    KeepaliveTriggerType = ENUM_KeepaliveTriggerType.Periodic,
                    PeriodicTriggerValue = settings.Keepalives.PeriodInMs
                };
                if (settings.Keepalives.EnableLinkMonitorMode)
                {
                    readerConfig.TryAddCustomParameter(new PARAM_ImpinjLinkMonitorConfiguration()
                    {
                        LinkMonitorMode = ENUM_ImpinjLinkMonitorMode.Enabled,
                        LinkDownThreshold = settings.Keepalives.LinkDownThreshold
                    });
                }
            }
            readerConfig.GPIPortCurrentState = new PARAM_GPIPortCurrentState[ReaderCapabilities.GpiCount];
            readerConfig.ReaderEventNotificationSpec = new PARAM_ReaderEventNotificationSpec
            {
                EventNotificationState = new PARAM_EventNotificationState[4]
            };
            readerConfig.ReaderEventNotificationSpec.EventNotificationState[0] = new PARAM_EventNotificationState
            {
                EventType = ENUM_NotificationEventType.GPI_Event,
                NotificationState = true
            };
            readerConfig.ReaderEventNotificationSpec.EventNotificationState[1] = new PARAM_EventNotificationState
            {
                EventType = ENUM_NotificationEventType.Antenna_Event,
                NotificationState = true
            };
            readerConfig.ReaderEventNotificationSpec.EventNotificationState[2] = new PARAM_EventNotificationState
            {
                EventType = ENUM_NotificationEventType.Report_Buffer_Fill_Warning,
                NotificationState = true
            };
            readerConfig.ReaderEventNotificationSpec.EventNotificationState[3] = new PARAM_EventNotificationState
            {
                EventType = ENUM_NotificationEventType.ROSpec_Event,
                NotificationState = true
            };
            foreach (GpoConfig gpo in settings.Gpos)
            {
                PARAM_ImpinjAdvancedGPOConfiguration gpoConfiguration = new PARAM_ImpinjAdvancedGPOConfiguration
                {
                    GPOPortNum = gpo.PortNumber,
                    GPOMode = (ENUM_ImpinjAdvancedGPOMode)gpo.Mode,
                    GPOPulseDurationMSec = gpo.GpoPulseDurationMsec
                };
                if (gpoConfiguration.GPOMode != ENUM_ImpinjAdvancedGPOMode.Normal)
                    readerConfig.TryAddCustomParameter(gpoConfiguration);
            }
            int index = 0;
            foreach (GpiConfig gpi in settings.Gpis)
            {
                readerConfig.GPIPortCurrentState[index] = new PARAM_GPIPortCurrentState
                {
                    GPIPortNum = gpi.PortNumber
                };
                if (gpi.IsEnabled)
                {
                    readerConfig.GPIPortCurrentState[index].Config = true;
                    if (gpi.DebounceInMs > 0U)
                        readerConfig.TryAddCustomParameter(new PARAM_ImpinjGPIDebounceConfiguration()
                        {
                            GPIDebounceTimerMSec = gpi.DebounceInMs,
                            GPIPortNum = (ushort)(index + 1)
                        });
                }
                else
                    readerConfig.GPIPortCurrentState[index].Config = false;
                ++index;
            }
            readerConfig.EventsAndReports = new PARAM_EventsAndReports
            {
                HoldEventsAndReportsUponReconnect = settings.HoldReportsOnDisconnect
            };
            if (IsSpatialReader)
            {
                readerConfig.TryAddCustomParameter(new PARAM_ImpinjPlacementConfiguration()
                {
                    FacilityXLocationCm = settings.SpatialConfig.Placement.FacilityXLocationCm,
                    FacilityYLocationCm = settings.SpatialConfig.Placement.FacilityYLocationCm,
                    OrientationDegrees = settings.SpatialConfig.Placement.OrientationDegrees,
                    HeightCm = settings.SpatialConfig.Placement.HeightCm
                });
                switch (settings.SpatialConfig.Mode)
                {
                    case SpatialMode.Location:
                        readerConfig.TryAddCustomParameter(new PARAM_ImpinjLocationReporting()
                        {
                            EnableEntryReport = settings.SpatialConfig.Location.EntryReportEnabled,
                            EnableExitReport = settings.SpatialConfig.Location.ExitReportEnabled,
                            EnableUpdateReport = settings.SpatialConfig.Location.UpdateReportEnabled,
                            EnableDiagnosticReport = settings.SpatialConfig.Location.DiagnosticReportEnabled
                        });
                        break;
                    case SpatialMode.Direction:
                        readerConfig.TryAddCustomParameter(new PARAM_ImpinjDirectionReporting()
                        {
                            EnableEntryReport = settings.SpatialConfig.Direction.EntryReportEnabled,
                            EnableExitReport = settings.SpatialConfig.Direction.ExitReportEnabled,
                            EnableUpdateReport = settings.SpatialConfig.Direction.UpdateReportEnabled,
                            EnableDiagnosticReport = settings.SpatialConfig.Direction.DiagnosticReportEnabled
                        });
                        break;
                }
            }
            if (settings.Report.BufferMode == ReportBufferMode.LowLatency)
            {
                readerConfig.TryAddCustomParameter(new PARAM_ImpinjReportBufferConfiguration()
                {
                    ReportBufferMode = ENUM_ImpinjReportBufferMode.Low_Latency,
                });
            }
            readerConfig.TryAddCustomParameter(new PARAM_ImpinjIntelligentAntennaManagement()
            {
                ManagementEnabled = settings.Antennas.EnableIntelligentAntennaManagement ? ENUM_ImpinjIntelligentAntennaMode.Enabled : ENUM_ImpinjIntelligentAntennaMode.Disabled,
            });
            return readerConfig;
        }


        /// <summary>Build up an LTK add ROSpec message.</summary>
        /// <param name="settings">A settings object after which the message will be modeled.</param>
        /// <returns>An LTK add ROSpec message.</returns>
        private MSG_ADD_ROSPEC BuildAddROSpecMessage(ImpinjSettings settings)
        {
            MSG_ADD_ROSPEC msgAddRospec = new MSG_ADD_ROSPEC(GetNextMessageID())
            {
                ROSpec = new PARAM_ROSpec()
            };
            msgAddRospec.ROSpec.CurrentState = ENUM_ROSpecState.Disabled;
            msgAddRospec.ROSpec.ROSpecID = RO_SPEC_ID;
            msgAddRospec.ROSpec.ROBoundarySpec = new PARAM_ROBoundarySpec
            {
                ROSpecStartTrigger = new PARAM_ROSpecStartTrigger()
            };
            msgAddRospec.ROSpec.ROBoundarySpec.ROSpecStartTrigger.ROSpecStartTriggerType = (ENUM_ROSpecStartTriggerType)settings.AutoStart.Mode;
            if (settings.AutoStart.Mode == AutoStartMode.GpiTrigger)
                msgAddRospec.ROSpec.ROBoundarySpec.ROSpecStartTrigger.GPITriggerValue = new PARAM_GPITriggerValue()
                {
                    GPIPortNum = settings.AutoStart.GpiPortNumber,
                    GPIEvent = settings.AutoStart.GpiLevel
                };
            else if (settings.AutoStart.Mode == AutoStartMode.Periodic)
            {
                PARAM_PeriodicTriggerValue periodicTriggerValue = new PARAM_PeriodicTriggerValue
                {
                    Offset = settings.AutoStart.FirstDelayInMs,
                    Period = settings.AutoStart.PeriodInMs
                };
                if (settings.AutoStart.UtcTimestamp != 0UL)
                {
                    periodicTriggerValue.UTCTimestamp = new PARAM_UTCTimestamp
                    {
                        Microseconds = settings.AutoStart.UtcTimestamp
                    };
                }
                msgAddRospec.ROSpec.ROBoundarySpec.ROSpecStartTrigger.PeriodicTriggerValue = periodicTriggerValue;
            }
            msgAddRospec.ROSpec.ROBoundarySpec.ROSpecStopTrigger = new PARAM_ROSpecStopTrigger
            {
                ROSpecStopTriggerType = (ENUM_ROSpecStopTriggerType)settings.AutoStop.Mode
            };
            if (settings.AutoStop.Mode == AutoStopMode.Duration)
                msgAddRospec.ROSpec.ROBoundarySpec.ROSpecStopTrigger.DurationTriggerValue = settings.AutoStop.DurationInMs;
            else if (settings.AutoStop.Mode == AutoStopMode.GpiTrigger)
                msgAddRospec.ROSpec.ROBoundarySpec.ROSpecStopTrigger.GPITriggerValue = new PARAM_GPITriggerValue()
                {
                    GPIPortNum = settings.AutoStop.GpiPortNumber,
                    GPIEvent = settings.AutoStop.GpiLevel,
                    Timeout = settings.AutoStop.Timeout
                };
            msgAddRospec.ROSpec.SpecParameter = new UNION_SpecParameter();
            switch (settings.SpatialConfig.Mode)
            {
                case SpatialMode.Location:
                    msgAddRospec.ROSpec.SpecParameter.Add(GetLISpec(settings));
                    break;
                case SpatialMode.Direction:
                    msgAddRospec.ROSpec.SpecParameter.Add(GetDISpec(settings));
                    break;
                default:
                    msgAddRospec.ROSpec.SpecParameter.Add(GetAiSpec(settings));
                    break;
            }
            msgAddRospec.ROSpec.ROReportSpec = new PARAM_ROReportSpec();
            if (settings.Report.Mode == ReportMode.Individual)
            {
                msgAddRospec.ROSpec.ROReportSpec.ROReportTrigger = ENUM_ROReportTriggerType.Upon_N_Tags_Or_End_Of_ROSpec;
                msgAddRospec.ROSpec.ROReportSpec.N = 1;
            }
            else if (settings.Report.Mode == ReportMode.BatchAfterStop)
            {
                msgAddRospec.ROSpec.ROReportSpec.ROReportTrigger = ENUM_ROReportTriggerType.Upon_N_Tags_Or_End_Of_ROSpec;
                msgAddRospec.ROSpec.ROReportSpec.N = 0;
            }
            else if (settings.Report.Mode == ReportMode.WaitForQuery)
            {
                msgAddRospec.ROSpec.ROReportSpec.ROReportTrigger = ENUM_ROReportTriggerType.None;
                msgAddRospec.ROSpec.ROReportSpec.N = 0;
            }
            msgAddRospec.ROSpec.ROReportSpec.TagReportContentSelector = new PARAM_TagReportContentSelector
            {
                EnableAccessSpecID = true,
                EnableAntennaID = settings.Report.IncludeAntennaPortNumber,
                EnableChannelIndex = settings.Report.IncludeChannel,
                EnableFirstSeenTimestamp = settings.Report.IncludeFirstSeenTime
            };
            if (settings.Report.IncludeCrc || settings.Report.IncludePcBits)
            {
                PARAM_C1G2EPCMemorySelector epcMemorySelector = new PARAM_C1G2EPCMemorySelector
                {
                    EnableCRC = settings.Report.IncludeCrc,
                    EnablePCBits = settings.Report.IncludePcBits
                };
                msgAddRospec.ROSpec.ROReportSpec.TagReportContentSelector.AirProtocolEPCMemorySelector = new UNION_AirProtocolEPCMemorySelector
                {
                    epcMemorySelector
                };
            }
            msgAddRospec.ROSpec.ROReportSpec.TagReportContentSelector.EnableInventoryParameterSpecID = false;
            msgAddRospec.ROSpec.ROReportSpec.TagReportContentSelector.EnableLastSeenTimestamp = settings.Report.IncludeLastSeenTime;
            msgAddRospec.ROSpec.ROReportSpec.TagReportContentSelector.EnablePeakRSSI = false;
            msgAddRospec.ROSpec.ROReportSpec.TagReportContentSelector.EnableROSpecID = false;
            msgAddRospec.ROSpec.ROReportSpec.TagReportContentSelector.EnableSpecIndex = false;
            msgAddRospec.ROSpec.ROReportSpec.TagReportContentSelector.EnableTagSeenCount = settings.Report.IncludeSeenCount;
            PARAM_ImpinjTagReportContentSelector reportContentSelector = new PARAM_ImpinjTagReportContentSelector();
            if (settings.Report.IncludeFastId)
            {
                reportContentSelector.ImpinjEnableSerializedTID = new PARAM_ImpinjEnableSerializedTID
                {
                    SerializedTIDMode = ENUM_ImpinjSerializedTIDMode.Enabled
                };
            }
            if (settings.Report.IncludeDopplerFrequency)
            {
                reportContentSelector.ImpinjEnableRFDopplerFrequency = new PARAM_ImpinjEnableRFDopplerFrequency
                {
                    RFDopplerFrequencyMode = ENUM_ImpinjRFDopplerFrequencyMode.Enabled
                };
            }
            if (settings.Report.IncludePhaseAngle)
            {
                reportContentSelector.ImpinjEnableRFPhaseAngle = new PARAM_ImpinjEnableRFPhaseAngle
                {
                    RFPhaseAngleMode = ENUM_ImpinjRFPhaseAngleMode.Enabled
                };
            }
            if (settings.Report.IncludePeakRssi)
            {
                reportContentSelector.ImpinjEnablePeakRSSI = new PARAM_ImpinjEnablePeakRSSI
                {
                    PeakRSSIMode = ENUM_ImpinjPeakRSSIMode.Enabled
                };
            }
            if (settings.Report.OptimizedReadOps != null)
            {
                if (settings.Report.OptimizedReadOps.Count > 2)
                    throw new OctaneSdkException("A maximum of two optimized read operations may be specified.");
                if (settings.Report.OptimizedReadOps.Count > 0)
                {
                    List<TagReadOp> optimizedReadOps = settings.Report.OptimizedReadOps;
                    PARAM_C1G2Read[] paramC1G2ReadArray = new PARAM_C1G2Read[optimizedReadOps.Count];
                    for (int i = 0; i < optimizedReadOps.Count(); i++)
                    {
                        TagReadOp tagReadOp = optimizedReadOps[i];
                        paramC1G2ReadArray[i] = new PARAM_C1G2Read
                        {
                            AccessPassword = tagReadOp.AccessPassword.ToUnsignedInt(),
                            MB = new TwoBits((ushort)tagReadOp.MemoryBank),
                            OpSpecID = tagReadOp.Id,
                            WordPointer = tagReadOp.WordPointer,
                            WordCount = tagReadOp.WordCount
                        };
                    }
                    reportContentSelector.ImpinjEnableOptimizedRead = new PARAM_ImpinjEnableOptimizedRead
                    {
                        OptimizedReadMode = ENUM_ImpinjOptimizedReadMode.Enabled,
                        C1G2Read = paramC1G2ReadArray
                    };
                }
            }
            if (settings.Report.IncludeGpsCoordinates)
            {
                reportContentSelector.ImpinjEnableGPSCoordinates = new PARAM_ImpinjEnableGPSCoordinates
                {
                    GPSCoordinatesMode = ENUM_ImpinjGPSCoordinatesMode.Enabled
                };
            }
            if (reportContentSelector.ImpinjEnableGPSCoordinates != null || reportContentSelector.ImpinjEnableOptimizedRead != null || (reportContentSelector.ImpinjEnablePeakRSSI != null || reportContentSelector.ImpinjEnableRFDopplerFrequency != null) || (reportContentSelector.ImpinjEnableRFPhaseAngle != null || reportContentSelector.ImpinjEnableSerializedTID != null))
                msgAddRospec.ROSpec.ROReportSpec.Custom.Add(reportContentSelector);
            return msgAddRospec;
        }


        private PARAM_AISpec GetAiSpec(ImpinjSettings settings)
        {
            PARAM_AISpec paramAiSpec = new PARAM_AISpec
            {
                AntennaIDs = new UInt16List()
            };
            foreach (AntennaConfig antenna in settings.Antennas)
            {
                if (antenna.IsEnabled)
                {
                    paramAiSpec.AntennaIDs.Add(antenna.PortNumber);
                }
            }
            paramAiSpec.AISpecStopTrigger = new PARAM_AISpecStopTrigger
            {
                AISpecStopTriggerType = ENUM_AISpecStopTriggerType.Null
            };
            PARAM_C1G2InventoryCommand inventoryCommand = new PARAM_C1G2InventoryCommand
            {
                TagInventoryStateAware = false
            };
            PARAM_C1G2RFControl paramC1G2RfControl = new PARAM_C1G2RFControl();
            inventoryCommand.C1G2Filter = GetC1G2Filters(settings.Filters);
            paramC1G2RfControl.ModeIndex = (ushort)settings.ReaderMode;
            paramC1G2RfControl.Tari = 0;
            inventoryCommand.C1G2RFControl = paramC1G2RfControl;
            inventoryCommand.C1G2SingulationControl = new PARAM_C1G2SingulationControl()
            {
                Session = new TwoBits((ushort)settings.Session),
                TagPopulation = settings.TagPopulationEstimate,
                TagTransitTime = 0U
            };
            if (settings.LowDutyCycle.IsEnabled)
                inventoryCommand.TryAddCustomParameter(new PARAM_ImpinjLowDutyCycle()
                {
                    LowDutyCycleMode = ENUM_ImpinjLowDutyCycleMode.Enabled,
                    EmptyFieldTimeout = settings.LowDutyCycle.EmptyFieldTimeoutInMs,
                    FieldPingInterval = settings.LowDutyCycle.FieldPingIntervalInMs
                });
            if (settings.TruncatedReply.IsEnabled)
                inventoryCommand.TryAddCustomParameter(new PARAM_ImpinjTruncatedReplyConfiguration()
                {
                    Gen2v2TagsOnly = settings.TruncatedReply.Gen2v2TagsOnly,
                    EPCLength = settings.TruncatedReply.EpcLengthInWords,
                    Pointer = settings.TruncatedReply.BitPointer,
                    TagMask = LLRPUtil.HexStringToBitList(settings.TruncatedReply.TagMask)
                });
            inventoryCommand.Custom.Add(new PARAM_ImpinjInventorySearchMode()
            {
                InventorySearchMode = (ENUM_ImpinjInventorySearchType)settings.SearchMode
            });
            if (settings.SearchMode == SearchMode.TagFocus && settings.Session != Session.S1)
                throw new OctaneSdkException("SearchMode.TagFocus can only be used when Session = 1");
            PARAM_ImpinjFixedFrequencyList fixedFrequencyList = new PARAM_ImpinjFixedFrequencyList
            {
                FixedFrequencyMode = ENUM_ImpinjFixedFrequencyMode.Channel_List,
                ChannelList = new UInt16List()
            };
            foreach (double freq in settings.TxFrequenciesInMhz)
            {
                ushort etsiTxFreqIndex = (ushort)(ReaderCapabilities.TxFrequencies.IndexOf(freq) + 1);
                if (etsiTxFreqIndex == 0)
                    throw new OctaneSdkException("The specified transmit frequency is invalid : " + freq);
                fixedFrequencyList.ChannelList.Add(etsiTxFreqIndex);
            }
            if (fixedFrequencyList.ChannelList.Count > 0)
                inventoryCommand.Custom.Add(fixedFrequencyList);
            PARAM_ImpinjReducedPowerFrequencyList powerFrequencyList = new PARAM_ImpinjReducedPowerFrequencyList
            {
                ReducedPowerMode = ENUM_ImpinjReducedPowerMode.Enabled,
                ChannelList = new UInt16List()
            };
            foreach (double freq in settings.ReducedPowerFrequenciesInMhz)
            {
                ushort fccTxFreqIndex = (ushort)(ReaderCapabilities.TxFrequencies.OrderBy(d => d).ToList().IndexOf(freq) + 1);
                if (fccTxFreqIndex == 0)
                    throw new OctaneSdkException("The specified transmit frequency is invalid : " + freq);
                powerFrequencyList.ChannelList.Add(fccTxFreqIndex);
            }
            if (powerFrequencyList.ChannelList.Count > 0)
                inventoryCommand.Custom.Add(powerFrequencyList);
            if (paramAiSpec.AntennaIDs.Count > 0)
            {
                paramAiSpec.InventoryParameterSpec = new PARAM_InventoryParameterSpec[1];
                paramAiSpec.InventoryParameterSpec[0] = new PARAM_InventoryParameterSpec
                {
                    InventoryParameterSpecID = 123,
                    ProtocolID = ENUM_AirProtocols.EPCGlobalClass1Gen2,
                    AntennaConfiguration = new PARAM_AntennaConfiguration[paramAiSpec.AntennaIDs.Count]
                };
                for (int i = 0; i < paramAiSpec.AntennaIDs.Count; i++)
                {
                    AntennaConfig antenna = settings.Antennas.GetAntenna(paramAiSpec.AntennaIDs[i]);
                    if (antenna.IsEnabled)
                    {
                        ushort rxSensitivityIndex;
                        bool error;
                        if (antenna.MaxRxSensitivity)
                        {
                            rxSensitivityIndex = 1;
                        }
                        else
                        {
                            rxSensitivityIndex = GetRxSensitivityIndex(antenna.RxSensitivityInDbm, out error);
                            if (error)
                                throw new OctaneSdkException("Invalid Rx Sensitivity setting for antenna port " + antenna.PortNumber);
                        }
                        ushort txPowerIndex;
                        if (antenna.MaxTxPower)
                        {
                            txPowerIndex = (ushort)ReaderCapabilities.TxPowers.Count;
                        }
                        else
                        {
                            txPowerIndex = GetTxPowerIndex(antenna.TxPowerInDbm, out error);
                            if (error)
                                throw new OctaneSdkException("Invalid Tx Power setting for antenna port" + antenna.PortNumber);
                        }
                        paramAiSpec.InventoryParameterSpec[0].AntennaConfiguration[i] = new PARAM_AntennaConfiguration
                        {
                            AntennaID = antenna.PortNumber
                        };
                        paramAiSpec.InventoryParameterSpec[0].AntennaConfiguration[i].AirProtocolInventoryCommandSettings.Add(inventoryCommand);
                        paramAiSpec.InventoryParameterSpec[0].AntennaConfiguration[i].RFTransmitter = new PARAM_RFTransmitter
                        {
                            TransmitPower = txPowerIndex,
                            HopTableID = 1,
                            ChannelIndex = 1
                        };
                        paramAiSpec.InventoryParameterSpec[0].AntennaConfiguration[i].RFReceiver = new PARAM_RFReceiver
                        {
                            ReceiverSensitivity = rxSensitivityIndex
                        };
                    }
                }
            }
            return paramAiSpec;
        }

        private BitList TruncateTagMask(BitList mask, int len)
        {
            if (len > mask.Count)
                throw new OctaneSdkException("Error setting the tag mask. The value specified for BitCount is greater than the data provided.");
            if (mask.Count % 8 != 0)
                throw new OctaneSdkException("Error setting the tag mask. The tag mask bit length must be divisible by 8. From the LLRP Spec: Integer numbers SHALL be encoded in network byte order with the most significant byte of the integer being sent first (big-Endian). Bit arrays are aligned to the most significant bit. Bit arrays are padded to an octet boundary. Pad bits SHALL be ignored by the Reader and Client.");
            if (len < mask.Count)
                mask.RemoveRange(len, mask.Count - len);
            return mask;
        }

        /// <summary>
        /// Construct a Location Specification that will be pushed to the reader, based off of a user-provided Settings configuration.
        /// </summary>
        /// <param name="settings">The Settings object that comes loaded with a location configuration desired by users.</param>
        /// <returns>LISpec that will be pushed to the reader.</returns>
        private PARAM_ImpinjLISpec GetLISpec(ImpinjSettings settings)
        {
            PARAM_ImpinjLISpec paramImpinjLiSpec = new PARAM_ImpinjLISpec();
            PARAM_ImpinjLocationConfig impinjLocationConfig = new PARAM_ImpinjLocationConfig
            {
                ComputeWindowSeconds = settings.SpatialConfig.Location.ComputeWindowSeconds,
                TagAgeIntervalSeconds = settings.SpatialConfig.Location.TagAgeIntervalSeconds,
                UpdateIntervalSeconds = settings.SpatialConfig.Location.UpdateIntervalSeconds
            };
            if (settings.SpatialConfig.Location.DisabledAntennaList != null && settings.SpatialConfig.Location.DisabledAntennaList.Count != 0)
            {
                PARAM_ImpinjDisabledAntennas disabledAntennas = new PARAM_ImpinjDisabledAntennas();
                UInt16List uint16Array = new UInt16List();
                foreach (ushort disabledAntenna in settings.SpatialConfig.Location.DisabledAntennaList)
                    uint16Array.Add(disabledAntenna);
                disabledAntennas.AntennaIDs = uint16Array;
                impinjLocationConfig.ImpinjDisabledAntennas = disabledAntennas;
            }
            if (settings.SpatialConfig.Location.LocationAlgorithmControl != null && settings.SpatialConfig.Location.LocationAlgorithmControl.Count != 0)
            {
                PARAM_ImpinjLocationAlgorithmControl algorithmControl = new PARAM_ImpinjLocationAlgorithmControl();
                UInt32List uint32Array = new UInt32List();
                foreach (uint val in settings.SpatialConfig.Location.LocationAlgorithmControl)
                    uint32Array.Add(val);
                algorithmControl.ControlData = uint32Array;
                impinjLocationConfig.ImpinjLocationAlgorithmControl = algorithmControl;
            }
            PARAM_ImpinjC1G2LocationConfig g2LocationConfig = new PARAM_ImpinjC1G2LocationConfig
            {
                C1G2Filter = GetC1G2Filters(settings.Filters),
                ModeIndex = (ushort)settings.ReaderMode,
                Session = new TwoBits((ushort)settings.Session)
            };
            if (!settings.SpatialConfig.Location.MaxTxPower)
            {
                PARAM_ImpinjTransmitPower transmitPowerFromDbm = GetTransmitPowerFromDbm(settings.SpatialConfig.Location.TxPowerInDbm);
                g2LocationConfig.ImpinjTransmitPower = transmitPowerFromDbm;
            }
            paramImpinjLiSpec.ImpinjLocationConfig = impinjLocationConfig;
            paramImpinjLiSpec.ImpinjC1G2LocationConfig = g2LocationConfig;
            return paramImpinjLiSpec;
        }

        /// <summary>
        /// Construct a Direction Specification that will be pushed to the reader, based off of a user-provided Settings configuration.
        /// </summary>
        /// <param name="settings">The Settings object that comes loaded with a direction configuration desired by users.</param>
        /// <returns>DISpec that will be pushed to the reader.</returns>
        private PARAM_ImpinjDISpec GetDISpec(ImpinjSettings settings)
        {
            PARAM_ImpinjDISpec paramImpinjDiSpec = new PARAM_ImpinjDISpec();
            PARAM_ImpinjDirectionConfig impinjDirectionConfig = new PARAM_ImpinjDirectionConfig()
            {
                FieldOfView = (ENUM_ImpinjDirectionFieldOfView)settings.SpatialConfig.Direction.FieldOfView,
                TagAgeIntervalSeconds = settings.SpatialConfig.Direction.TagAgeIntervalSeconds,
                UpdateIntervalSeconds = settings.SpatialConfig.Direction.UpdateIntervalSeconds,
                ImpinjDirectionUserTagPopulationLimit = new PARAM_ImpinjDirectionUserTagPopulationLimit()
            };
            impinjDirectionConfig.ImpinjDirectionUserTagPopulationLimit.UserTagPopulationLimit = settings.SpatialConfig.Direction.TagPopulationLimit;
            PARAM_ImpinjC1G2DirectionConfig g2DirectionConfig = new PARAM_ImpinjC1G2DirectionConfig
            {
                C1G2Filter = GetC1G2Filters(settings.Filters),
                RFMode = (ENUM_ImpinjDirectionRFMode)settings.SpatialConfig.Direction.Mode
            };
            if (!settings.SpatialConfig.Direction.MaxTxPower)
            {
                PARAM_ImpinjTransmitPower transmitPowerFromDbm = GetTransmitPowerFromDbm(settings.SpatialConfig.Direction.TxPowerInDbm);
                g2DirectionConfig.ImpinjTransmitPower = transmitPowerFromDbm;
            }
            PARAM_ImpinjDirectionSectors directionSectors = new PARAM_ImpinjDirectionSectors();
            foreach (ushort enabledSectorId in settings.SpatialConfig.Direction.EnabledSectorIDs)
                directionSectors.EnabledSectorIDs.Add(enabledSectorId);
            paramImpinjDiSpec.ImpinjDirectionConfig = impinjDirectionConfig;
            paramImpinjDiSpec.ImpinjC1G2DirectionConfig = g2DirectionConfig;
            paramImpinjDiSpec.ImpinjDirectionSectors = directionSectors;
            return paramImpinjDiSpec;
        }

        /// <summary>
        /// Get an LTK Transmit Power object configured with a power index corresponding to a given dBm value.
        /// Used to set c1g2 configurations that will be pushed down to the reader.
        /// </summary>
        /// <param name="dbm">The desired transmit power in dBm, used to calculate the power index with which the transmit power object will be configured</param>
        /// <returns>LTK transmit power object configured with an appropriate power index</returns>
        private PARAM_ImpinjTransmitPower GetTransmitPowerFromDbm(double dbm)
        {
            ushort txPowerIndex = GetTxPowerIndex(dbm, out bool error);
            if (error)
                throw new OctaneSdkException("Invalid Tx Power setting");
            return new PARAM_ImpinjTransmitPower()
            {
                TransmitPower = txPowerIndex
            };
        }

        private PARAM_C1G2Filter[] GetC1G2Filters(FilterSettings filterSettings)
        {
            PARAM_C1G2Filter[] paramC1G2FilterArray = null;
            if (filterSettings.Mode == TagFilterMode.UseTagSelectFilters)
            {
                int selectFiltersAllowed = ReaderCapabilities.MaxTagSelectFiltersAllowed;
                int count = filterSettings.TagSelectFilters.Count;
                if (count > selectFiltersAllowed)
                    throw new OctaneSdkException("Error parsing tag select filter list. " + string.Format("The tag select filter list has {0} filter(s), ", (object)count) + string.Format("which is more than the maximum supported by the reader which is {0}.", (object)selectFiltersAllowed));
                paramC1G2FilterArray = new PARAM_C1G2Filter[count];
                for (int index = 0; index < count; ++index)
                {
                    TagSelectFilter tagSelectFilter = filterSettings.TagSelectFilters[index];
                    paramC1G2FilterArray[index] = new PARAM_C1G2Filter()
                    {
                        C1G2TagInventoryMask = new PARAM_C1G2TagInventoryMask()
                        {
                            MB = new TwoBits((ushort)tagSelectFilter.MemoryBank),
                            Pointer = tagSelectFilter.BitPointer,
                            TagMask = LLRPUtil.HexStringToBitList(tagSelectFilter.TagMask)
                        }
                    };
                    if (tagSelectFilter.BitCount > 0)
                        paramC1G2FilterArray[index].C1G2TagInventoryMask.TagMask = this.TruncateTagMask(paramC1G2FilterArray[index].C1G2TagInventoryMask.TagMask, tagSelectFilter.BitCount);
                    PARAM_C1G2TagInventoryStateUnawareFilterAction unawareFilterAction = new PARAM_C1G2TagInventoryStateUnawareFilterAction()
                    {
                        Action = StateUnawareActionExtensions.ConvertToC1G2StateUnawareAction(tagSelectFilter.MatchAction, tagSelectFilter.NonMatchAction)
                    };
                    paramC1G2FilterArray[index].C1G2TagInventoryStateUnawareFilterAction = unawareFilterAction;
                }
            }
            else if (filterSettings.Mode == TagFilterMode.Filter1AndFilter2 || filterSettings.Mode == TagFilterMode.Filter1OrFilter2)
            {
                paramC1G2FilterArray = new PARAM_C1G2Filter[2]
                {
                    new PARAM_C1G2Filter()
                    {
                        C1G2TagInventoryMask = new PARAM_C1G2TagInventoryMask()
                        {
                            MB = new TwoBits((ushort) filterSettings.TagFilter1.MemoryBank),
                            Pointer = filterSettings.TagFilter1.BitPointer,
                            TagMask = LLRPUtil.HexStringToBitList(filterSettings.TagFilter1.TagMask)
                        }
                    },
                    new PARAM_C1G2Filter()
                    {
                        C1G2TagInventoryMask = new PARAM_C1G2TagInventoryMask()
                        {
                            MB = new TwoBits((ushort) filterSettings.TagFilter2.MemoryBank),
                            Pointer = filterSettings.TagFilter2.BitPointer,
                            TagMask = LLRPUtil.HexStringToBitList(filterSettings.TagFilter2.TagMask)
                        }
                    }
                };
                if (filterSettings.TagFilter1.BitCount > 0)
                    paramC1G2FilterArray[0].C1G2TagInventoryMask.TagMask = TruncateTagMask(paramC1G2FilterArray[0].C1G2TagInventoryMask.TagMask, filterSettings.TagFilter1.BitCount);
                if (filterSettings.TagFilter2.BitCount > 0)
                    paramC1G2FilterArray[1].C1G2TagInventoryMask.TagMask = TruncateTagMask(paramC1G2FilterArray[1].C1G2TagInventoryMask.TagMask, filterSettings.TagFilter2.BitCount);
                paramC1G2FilterArray[0].C1G2TagInventoryStateUnawareFilterAction = new PARAM_C1G2TagInventoryStateUnawareFilterAction
                {
                    Action = filterSettings.TagFilter1.FilterOp != TagFilterOp.Match ? ENUM_C1G2StateUnawareAction.Unselect_Select : ENUM_C1G2StateUnawareAction.Select_Unselect
                };
                paramC1G2FilterArray[1].C1G2TagInventoryStateUnawareFilterAction = new PARAM_C1G2TagInventoryStateUnawareFilterAction();
                if (filterSettings.Mode == TagFilterMode.Filter1AndFilter2)
                    paramC1G2FilterArray[1].C1G2TagInventoryStateUnawareFilterAction.Action = filterSettings.TagFilter2.FilterOp != TagFilterOp.Match ? ENUM_C1G2StateUnawareAction.Unselect_DoNothing : ENUM_C1G2StateUnawareAction.DoNothing_Unselect;
                else if (filterSettings.Mode == TagFilterMode.Filter1OrFilter2)
                    paramC1G2FilterArray[1].C1G2TagInventoryStateUnawareFilterAction.Action = filterSettings.TagFilter2.FilterOp != TagFilterOp.Match ? ENUM_C1G2StateUnawareAction.DoNothing_Select : ENUM_C1G2StateUnawareAction.Select_DoNothing;
            }
            else if (filterSettings.Mode != TagFilterMode.None)
            {
                paramC1G2FilterArray = new PARAM_C1G2Filter[1]
                {
                    new PARAM_C1G2Filter()
                };
                paramC1G2FilterArray[0].C1G2TagInventoryMask = new PARAM_C1G2TagInventoryMask();
                paramC1G2FilterArray[0].C1G2TagInventoryStateUnawareFilterAction = new PARAM_C1G2TagInventoryStateUnawareFilterAction();
                TagFilter tagFilter = filterSettings.Mode != TagFilterMode.OnlyFilter1 ? filterSettings.TagFilter2 : filterSettings.TagFilter1;
                paramC1G2FilterArray[0].C1G2TagInventoryMask.MB = new TwoBits((ushort)tagFilter.MemoryBank);
                paramC1G2FilterArray[0].C1G2TagInventoryMask.Pointer = tagFilter.BitPointer;
                paramC1G2FilterArray[0].C1G2TagInventoryMask.TagMask = LLRPUtil.HexStringToBitList(tagFilter.TagMask);
                if (tagFilter.BitCount > 0)
                    paramC1G2FilterArray[0].C1G2TagInventoryMask.TagMask = TruncateTagMask(paramC1G2FilterArray[0].C1G2TagInventoryMask.TagMask, tagFilter.BitCount);
                paramC1G2FilterArray[0].C1G2TagInventoryStateUnawareFilterAction.Action = tagFilter.FilterOp != TagFilterOp.Match ? ENUM_C1G2StateUnawareAction.Unselect_Select : ENUM_C1G2StateUnawareAction.Select_Unselect;
            }
            return paramC1G2FilterArray;
        }


        private ushort GetRxSensitivityIndex(double dBm, out bool error)
        {
            error = false;
            foreach (RxSensitivityTableEntry rxSensitivity in ReaderCapabilities.RxSensitivities)
            {
                if (dBm == rxSensitivity.Dbm)
                    return rxSensitivity.Index;
            }
            error = true;
            return 0;
        }

        private ushort GetTxPowerIndex(double dBm, out bool error)
        {
            error = false;
            foreach (TxPowerTableEntry txPower in ReaderCapabilities.TxPowers)
            {
                if (dBm == txPower.Dbm)
                    return txPower.Index;
            }
            error = true;
            return 0;
        }



        private ImpinjSettings ParseRoSpecAndConfig(PARAM_ROSpec rospec, MSG_GET_READER_CONFIG_RESPONSE config)
        {
            ImpinjSettings settings = new ImpinjSettings();
            settings.AutoStart.Mode = (AutoStartMode)rospec.ROBoundarySpec.ROSpecStartTrigger.ROSpecStartTriggerType;
            if (settings.AutoStart.Mode == AutoStartMode.GpiTrigger)
            {
                settings.AutoStart.GpiPortNumber = rospec.ROBoundarySpec.ROSpecStartTrigger.GPITriggerValue.GPIPortNum;
                settings.AutoStart.GpiLevel = rospec.ROBoundarySpec.ROSpecStartTrigger.GPITriggerValue.GPIEvent;
            }
            else if (settings.AutoStart.Mode == AutoStartMode.Periodic)
            {
                settings.AutoStart.FirstDelayInMs = rospec.ROBoundarySpec.ROSpecStartTrigger.PeriodicTriggerValue.Offset;
                settings.AutoStart.PeriodInMs = rospec.ROBoundarySpec.ROSpecStartTrigger.PeriodicTriggerValue.Period;
                settings.AutoStart.UtcTimestamp = rospec.ROBoundarySpec.ROSpecStartTrigger.PeriodicTriggerValue.UTCTimestamp.Microseconds;
            }
            settings.AutoStop.Mode = (AutoStopMode)rospec.ROBoundarySpec.ROSpecStopTrigger.ROSpecStopTriggerType;
            if (settings.AutoStop.Mode == AutoStopMode.Duration)
                settings.AutoStop.DurationInMs = rospec.ROBoundarySpec.ROSpecStopTrigger.DurationTriggerValue;
            else if (settings.AutoStop.Mode == AutoStopMode.GpiTrigger)
            {
                settings.AutoStop.GpiPortNumber = rospec.ROBoundarySpec.ROSpecStopTrigger.GPITriggerValue.GPIPortNum;
                settings.AutoStop.GpiLevel = rospec.ROBoundarySpec.ROSpecStopTrigger.GPITriggerValue.GPIEvent;
                settings.AutoStop.Timeout = rospec.ROBoundarySpec.ROSpecStopTrigger.GPITriggerValue.Timeout;
            }
            if (rospec.SpecParameter[0] is PARAM_AISpec paramAiSpec)
            {
                settings.SpatialConfig.Mode = SpatialMode.Inventory;
                settings.Antennas = new AntennaConfigGroup(paramAiSpec.AntennaIDs);
                PARAM_C1G2InventoryCommand inventoryCommandSetting = (PARAM_C1G2InventoryCommand)paramAiSpec.InventoryParameterSpec[0].AntennaConfiguration[0].AirProtocolInventoryCommandSettings[0];
                if (inventoryCommandSetting.C1G2Filter == null || inventoryCommandSetting.C1G2Filter.Length == 0)
                {
                    settings.Filters.Mode = TagFilterMode.None;
                }
                else if (inventoryCommandSetting.C1G2Filter.Length > 2)
                {
                    foreach (PARAM_C1G2Filter filter in inventoryCommandSetting.C1G2Filter)
                    {
                        TagSelectFilter tagSelectFilter = new TagSelectFilter()
                        {
                            TagMask = LLRPUtil.BitListToHexString(filter.C1G2TagInventoryMask.TagMask),
                            BitPointer = filter.C1G2TagInventoryMask.Pointer,
                            MemoryBank = (MemoryBank)LLRPUtil.TwoBitsToInt32(filter.C1G2TagInventoryMask.MB)
                        };
                        StateUnawareActionPair unawareActionPair = StateUnawareActionExtensions.ConvertFromC1G2StateUnawareAction(filter.C1G2TagInventoryStateUnawareFilterAction.Action);
                        tagSelectFilter.MatchAction = unawareActionPair.MatchingAction;
                        tagSelectFilter.NonMatchAction = unawareActionPair.NonMatchingAction;
                        settings.Filters.TagSelectFilters.Add(tagSelectFilter);
                    }
                    settings.Filters.Mode = TagFilterMode.UseTagSelectFilters;
                }
                else
                {
                    settings.Filters.TagFilter1.MemoryBank = (MemoryBank)LLRPUtil.TwoBitsToInt32(inventoryCommandSetting.C1G2Filter[0].C1G2TagInventoryMask.MB);
                    settings.Filters.TagFilter1.BitPointer = inventoryCommandSetting.C1G2Filter[0].C1G2TagInventoryMask.Pointer;
                    settings.Filters.TagFilter1.TagMask = LLRPUtil.BitListToHexString(inventoryCommandSetting.C1G2Filter[0].C1G2TagInventoryMask.TagMask);
                    settings.Filters.Mode = TagFilterMode.OnlyFilter1;
                    if (inventoryCommandSetting.C1G2Filter.Length > 1)
                    {
                        settings.Filters.TagFilter2.MemoryBank = (MemoryBank)LLRPUtil.TwoBitsToInt32(inventoryCommandSetting.C1G2Filter[1].C1G2TagInventoryMask.MB);
                        settings.Filters.TagFilter2.BitPointer = inventoryCommandSetting.C1G2Filter[1].C1G2TagInventoryMask.Pointer;
                        settings.Filters.TagFilter2.TagMask = LLRPUtil.BitListToHexString(inventoryCommandSetting.C1G2Filter[1].C1G2TagInventoryMask.TagMask);
                        if (inventoryCommandSetting.C1G2Filter[1].C1G2TagInventoryStateUnawareFilterAction.Action == ENUM_C1G2StateUnawareAction.DoNothing_Unselect)
                            settings.Filters.Mode = TagFilterMode.Filter1AndFilter2;
                        else
                            settings.Filters.Mode = TagFilterMode.Filter1OrFilter2;
                    }
                }
                settings.ReaderMode = (ReaderMode)inventoryCommandSetting.C1G2RFControl.ModeIndex;
                settings.TagPopulationEstimate = inventoryCommandSetting.C1G2SingulationControl.TagPopulation;
                settings.Session = (Session)LLRPUtil.TwoBitsToInt32(inventoryCommandSetting.C1G2SingulationControl.Session);
                for (int index = 0; index < inventoryCommandSetting.Custom.Length; ++index)
                {
                    if (inventoryCommandSetting.Custom[index] is PARAM_ImpinjLowDutyCycle impinjLowDutyCycle)
                    {
                        settings.LowDutyCycle.IsEnabled = true;
                        settings.LowDutyCycle.EmptyFieldTimeoutInMs = impinjLowDutyCycle.EmptyFieldTimeout;
                        settings.LowDutyCycle.FieldPingIntervalInMs = impinjLowDutyCycle.FieldPingInterval;
                    }
                    else if (inventoryCommandSetting.Custom[index] is PARAM_ImpinjTruncatedReplyConfiguration replyConfiguration)
                    {
                        settings.TruncatedReply.IsEnabled = true;
                        settings.TruncatedReply.TagMask = LLRPUtil.BitListToHexString(replyConfiguration.TagMask);
                        settings.TruncatedReply.BitPointer = replyConfiguration.Pointer;
                        settings.TruncatedReply.EpcLengthInWords = replyConfiguration.EPCLength;
                        settings.TruncatedReply.Gen2v2TagsOnly = replyConfiguration.Gen2v2TagsOnly;
                    }
                    else if (inventoryCommandSetting.Custom[index] is PARAM_ImpinjInventorySearchMode inventorySearchMode)
                    {
                        settings.SearchMode = (SearchMode)inventorySearchMode.InventorySearchMode;
                    }
                }
                settings.Antennas.DisableAll();
                PARAM_AntennaConfiguration[] antennaConfiguration = paramAiSpec.InventoryParameterSpec[0].AntennaConfiguration;
                for (int index = 0; index < antennaConfiguration.Length; ++index)
                {
                    AntennaConfig antenna = settings.Antennas.GetAntenna(antennaConfiguration[index].AntennaID);
                    antenna.IsEnabled = true;
                    ushort receiverSensitivity = antennaConfiguration[index].RFReceiver.ReceiverSensitivity;
                    antenna.MaxRxSensitivity = receiverSensitivity == 1;
                    antenna.RxSensitivityInDbm = ReaderCapabilities.RxSensitivities[receiverSensitivity - 1].Dbm;
                    ushort txPowerIndex = (ushort)(antennaConfiguration[index].RFTransmitter.TransmitPower - 1U);
                    int maxTxPowerIndex = ReaderCapabilities.TxPowers.Count - 1;
                    antenna.MaxTxPower = txPowerIndex == maxTxPowerIndex;
                    antenna.TxPowerInDbm = ReaderCapabilities.TxPowers[txPowerIndex].Dbm;
                }
            }
            else if (rospec.SpecParameter[0] is PARAM_ImpinjLISpec paramImpinjLiSpec)
            {
                LocationConfig location = settings.SpatialConfig.Location;
                settings.SpatialConfig.Mode = SpatialMode.Location;
                location.ComputeWindowSeconds = paramImpinjLiSpec.ImpinjLocationConfig.ComputeWindowSeconds;
                location.DisabledAntennaList = paramImpinjLiSpec.ImpinjLocationConfig.ImpinjDisabledAntennas.AntennaIDs;
                location.TagAgeIntervalSeconds = paramImpinjLiSpec.ImpinjLocationConfig.TagAgeIntervalSeconds;
                location.UpdateIntervalSeconds = paramImpinjLiSpec.ImpinjLocationConfig.UpdateIntervalSeconds;
                if (paramImpinjLiSpec.ImpinjLocationConfig.ImpinjLocationAlgorithmControl != null)
                    location.LocationAlgorithmControl = paramImpinjLiSpec.ImpinjLocationConfig.ImpinjLocationAlgorithmControl.ControlData;
                else
                    location.LocationAlgorithmControl.Clear();
                settings.ReaderMode = (ReaderMode)paramImpinjLiSpec.ImpinjC1G2LocationConfig.ModeIndex;
                settings.Session = (Session)LLRPUtil.TwoBitsToInt32(paramImpinjLiSpec.ImpinjC1G2LocationConfig.Session);
                if (paramImpinjLiSpec.ImpinjC1G2LocationConfig.C1G2Filter == null || paramImpinjLiSpec.ImpinjC1G2LocationConfig.C1G2Filter.Length == 0)
                {
                    settings.Filters.Mode = TagFilterMode.None;
                }
                else if (paramImpinjLiSpec.ImpinjC1G2LocationConfig.C1G2Filter.Length > 2)
                {
                    foreach (PARAM_C1G2Filter filter in paramImpinjLiSpec.ImpinjC1G2LocationConfig.C1G2Filter)
                    {
                        TagSelectFilter tagSelectFilter = new TagSelectFilter()
                        {
                            TagMask = LLRPUtil.BitListToHexString(filter.C1G2TagInventoryMask.TagMask),
                            BitPointer = filter.C1G2TagInventoryMask.Pointer,
                            MemoryBank = (MemoryBank)LLRPUtil.TwoBitsToInt32(filter.C1G2TagInventoryMask.MB)
                        };
                        StateUnawareActionPair unawareActionPair = StateUnawareActionExtensions.ConvertFromC1G2StateUnawareAction(filter.C1G2TagInventoryStateUnawareFilterAction.Action);
                        tagSelectFilter.MatchAction = unawareActionPair.MatchingAction;
                        tagSelectFilter.NonMatchAction = unawareActionPair.NonMatchingAction;
                        settings.Filters.TagSelectFilters.Add(tagSelectFilter);
                    }
                    settings.Filters.Mode = TagFilterMode.UseTagSelectFilters;
                }
                else
                {
                    settings.Filters.TagFilter1.MemoryBank = (MemoryBank)LLRPUtil.TwoBitsToInt32(paramImpinjLiSpec.ImpinjC1G2LocationConfig.C1G2Filter[0].C1G2TagInventoryMask.MB);
                    settings.Filters.TagFilter1.BitPointer = paramImpinjLiSpec.ImpinjC1G2LocationConfig.C1G2Filter[0].C1G2TagInventoryMask.Pointer;
                    settings.Filters.TagFilter1.TagMask = LLRPUtil.BitListToHexString(paramImpinjLiSpec.ImpinjC1G2LocationConfig.C1G2Filter[0].C1G2TagInventoryMask.TagMask);
                    settings.Filters.Mode = TagFilterMode.OnlyFilter1;
                    if (paramImpinjLiSpec.ImpinjC1G2LocationConfig.C1G2Filter.Length > 1)
                    {
                        settings.Filters.TagFilter2.MemoryBank = (MemoryBank)LLRPUtil.TwoBitsToInt32(paramImpinjLiSpec.ImpinjC1G2LocationConfig.C1G2Filter[1].C1G2TagInventoryMask.MB);
                        settings.Filters.TagFilter2.BitPointer = paramImpinjLiSpec.ImpinjC1G2LocationConfig.C1G2Filter[1].C1G2TagInventoryMask.Pointer;
                        settings.Filters.TagFilter2.TagMask = LLRPUtil.BitListToHexString(paramImpinjLiSpec.ImpinjC1G2LocationConfig.C1G2Filter[1].C1G2TagInventoryMask.TagMask);
                        if (paramImpinjLiSpec.ImpinjC1G2LocationConfig.C1G2Filter[1].C1G2TagInventoryStateUnawareFilterAction.Action == ENUM_C1G2StateUnawareAction.DoNothing_Unselect)
                            settings.Filters.Mode = TagFilterMode.Filter1AndFilter2;
                        else
                            settings.Filters.Mode = TagFilterMode.Filter1OrFilter2;
                    }
                }
                ushort txPowerIndex = (ushort)(paramImpinjLiSpec.ImpinjC1G2LocationConfig.ImpinjTransmitPower.TransmitPower - 1U);
                int maxTxPowerIndex = ReaderCapabilities.TxPowers.Count - 1;
                location.MaxTxPower = txPowerIndex == maxTxPowerIndex;
                location.TxPowerInDbm = ReaderCapabilities.TxPowers[txPowerIndex].Dbm;
            }
            else if (rospec.SpecParameter[0] is PARAM_ImpinjDISpec paramImpinjDiSpec)
            {
                DirectionConfig direction = settings.SpatialConfig.Direction;
                settings.SpatialConfig.Mode = SpatialMode.Direction;
                direction.EnabledSectorIDs = paramImpinjDiSpec.ImpinjDirectionSectors.EnabledSectorIDs;
                direction.TagAgeIntervalSeconds = paramImpinjDiSpec.ImpinjDirectionConfig.TagAgeIntervalSeconds;
                direction.UpdateIntervalSeconds = paramImpinjDiSpec.ImpinjDirectionConfig.UpdateIntervalSeconds;
                direction.FieldOfView = (DirectionFieldOfView)paramImpinjDiSpec.ImpinjDirectionConfig.FieldOfView;
                direction.TagPopulationLimit = paramImpinjDiSpec.ImpinjDirectionConfig.ImpinjDirectionUserTagPopulationLimit.UserTagPopulationLimit;
                if (paramImpinjDiSpec.ImpinjC1G2DirectionConfig.C1G2Filter == null || paramImpinjDiSpec.ImpinjC1G2DirectionConfig.C1G2Filter.Length == 0)
                {
                    settings.Filters.Mode = TagFilterMode.None;
                }
                else if (paramImpinjDiSpec.ImpinjC1G2DirectionConfig.C1G2Filter.Length > 2)
                {
                    foreach (PARAM_C1G2Filter filter in paramImpinjDiSpec.ImpinjC1G2DirectionConfig.C1G2Filter)
                    {
                        TagSelectFilter tagSelectFilter = new TagSelectFilter()
                        {
                            TagMask = LLRPUtil.BitListToHexString(filter.C1G2TagInventoryMask.TagMask),
                            BitPointer = filter.C1G2TagInventoryMask.Pointer,
                            MemoryBank = (MemoryBank)LLRPUtil.TwoBitsToInt32(filter.C1G2TagInventoryMask.MB)
                        };
                        StateUnawareActionPair unawareActionPair = StateUnawareActionExtensions.ConvertFromC1G2StateUnawareAction(filter.C1G2TagInventoryStateUnawareFilterAction.Action);
                        tagSelectFilter.MatchAction = unawareActionPair.MatchingAction;
                        tagSelectFilter.NonMatchAction = unawareActionPair.NonMatchingAction;
                        settings.Filters.TagSelectFilters.Add(tagSelectFilter);
                    }
                    settings.Filters.Mode = TagFilterMode.UseTagSelectFilters;
                }
                else
                {
                    settings.Filters.TagFilter1.MemoryBank = (MemoryBank)LLRPUtil.TwoBitsToInt32(paramImpinjDiSpec.ImpinjC1G2DirectionConfig.C1G2Filter[0].C1G2TagInventoryMask.MB);
                    settings.Filters.TagFilter1.BitPointer = paramImpinjDiSpec.ImpinjC1G2DirectionConfig.C1G2Filter[0].C1G2TagInventoryMask.Pointer;
                    settings.Filters.TagFilter1.TagMask = LLRPUtil.BitListToHexString(paramImpinjDiSpec.ImpinjC1G2DirectionConfig.C1G2Filter[0].C1G2TagInventoryMask.TagMask);
                    settings.Filters.Mode = TagFilterMode.OnlyFilter1;
                    if (paramImpinjDiSpec.ImpinjC1G2DirectionConfig.C1G2Filter.Length > 1)
                    {
                        settings.Filters.TagFilter2.MemoryBank = (MemoryBank)LLRPUtil.TwoBitsToInt32(paramImpinjDiSpec.ImpinjC1G2DirectionConfig.C1G2Filter[1].C1G2TagInventoryMask.MB);
                        settings.Filters.TagFilter2.BitPointer = paramImpinjDiSpec.ImpinjC1G2DirectionConfig.C1G2Filter[1].C1G2TagInventoryMask.Pointer;
                        settings.Filters.TagFilter2.TagMask = LLRPUtil.BitListToHexString(paramImpinjDiSpec.ImpinjC1G2DirectionConfig.C1G2Filter[1].C1G2TagInventoryMask.TagMask);
                        if (paramImpinjDiSpec.ImpinjC1G2DirectionConfig.C1G2Filter[1].C1G2TagInventoryStateUnawareFilterAction.Action == ENUM_C1G2StateUnawareAction.DoNothing_Unselect)
                            settings.Filters.Mode = TagFilterMode.Filter1AndFilter2;
                        else
                            settings.Filters.Mode = TagFilterMode.Filter1OrFilter2;
                    }
                }
                direction.Mode = (DirectionMode)paramImpinjDiSpec.ImpinjC1G2DirectionConfig.RFMode;
                ushort txPowerIndex = (ushort)(paramImpinjDiSpec.ImpinjC1G2DirectionConfig.ImpinjTransmitPower.TransmitPower - 1U);
                int maxTxPowerIndex = ReaderCapabilities.TxPowers.Count - 1;
                direction.MaxTxPower = txPowerIndex == maxTxPowerIndex;
                direction.TxPowerInDbm = ReaderCapabilities.TxPowers[txPowerIndex].Dbm;
            }
            PARAM_ROReportSpec roReportSpec = rospec.ROReportSpec;
            if (roReportSpec.N == 1 && roReportSpec.ROReportTrigger == ENUM_ROReportTriggerType.Upon_N_Tags_Or_End_Of_ROSpec)
                settings.Report.Mode = ReportMode.Individual;
            else if (roReportSpec.N == 0 && roReportSpec.ROReportTrigger == ENUM_ROReportTriggerType.Upon_N_Tags_Or_End_Of_ROSpec)
                settings.Report.Mode = ReportMode.BatchAfterStop;
            else if (roReportSpec.ROReportTrigger == ENUM_ROReportTriggerType.None)
                settings.Report.Mode = ReportMode.WaitForQuery;
            settings.Report.IncludeAntennaPortNumber = roReportSpec.TagReportContentSelector.EnableAntennaID;
            settings.Report.IncludeChannel = roReportSpec.TagReportContentSelector.EnableChannelIndex;
            settings.Report.IncludeFirstSeenTime = roReportSpec.TagReportContentSelector.EnableFirstSeenTimestamp;
            settings.Report.IncludeLastSeenTime = roReportSpec.TagReportContentSelector.EnableLastSeenTimestamp;
            settings.Report.IncludeSeenCount = roReportSpec.TagReportContentSelector.EnableTagSeenCount;
            if (roReportSpec.TagReportContentSelector.AirProtocolEPCMemorySelector.Length > 0)
            {
                PARAM_C1G2EPCMemorySelector epcMemorySelector = (PARAM_C1G2EPCMemorySelector)roReportSpec.TagReportContentSelector.AirProtocolEPCMemorySelector[0];
                settings.Report.IncludeCrc = epcMemorySelector.EnableCRC;
                settings.Report.IncludePcBits = epcMemorySelector.EnablePCBits;
            }
            if (roReportSpec.Custom.Length > 0)
            {
                PARAM_ImpinjTagReportContentSelector reportContentSelector = (PARAM_ImpinjTagReportContentSelector)roReportSpec.Custom[0];
                settings.Report.IncludeFastId = reportContentSelector.ImpinjEnableSerializedTID != null && reportContentSelector.ImpinjEnableSerializedTID.SerializedTIDMode == ENUM_ImpinjSerializedTIDMode.Enabled;
                settings.Report.IncludeDopplerFrequency = reportContentSelector.ImpinjEnableRFDopplerFrequency != null && reportContentSelector.ImpinjEnableRFDopplerFrequency.RFDopplerFrequencyMode == ENUM_ImpinjRFDopplerFrequencyMode.Enabled;
                settings.Report.IncludePhaseAngle = reportContentSelector.ImpinjEnableRFPhaseAngle != null && reportContentSelector.ImpinjEnableRFPhaseAngle.RFPhaseAngleMode == ENUM_ImpinjRFPhaseAngleMode.Enabled;
                settings.Report.IncludePeakRssi = reportContentSelector.ImpinjEnablePeakRSSI != null && reportContentSelector.ImpinjEnablePeakRSSI.PeakRSSIMode == ENUM_ImpinjPeakRSSIMode.Enabled;
                settings.Report.IncludeGpsCoordinates = reportContentSelector.ImpinjEnableGPSCoordinates != null && reportContentSelector.ImpinjEnableGPSCoordinates.GPSCoordinatesMode == ENUM_ImpinjGPSCoordinatesMode.Enabled;
                if (reportContentSelector.ImpinjEnableOptimizedRead != null && reportContentSelector.ImpinjEnableOptimizedRead.OptimizedReadMode == ENUM_ImpinjOptimizedReadMode.Enabled)
                {
                    settings.Report.OptimizedReadOps.Clear();
                    for (int index = 0; index < reportContentSelector.ImpinjEnableOptimizedRead.C1G2Read.Length; ++index)
                    {
                        TagReadOp tagReadOp = new TagReadOp(reportContentSelector.ImpinjEnableOptimizedRead.C1G2Read[index].OpSpecID)
                        {
                            AccessPassword = TagData.FromUnsignedInt(reportContentSelector.ImpinjEnableOptimizedRead.C1G2Read[index].AccessPassword),
                            MemoryBank = (MemoryBank)LLRPUtil.TwoBitsToInt32(reportContentSelector.ImpinjEnableOptimizedRead.C1G2Read[index].MB),
                            WordCount = reportContentSelector.ImpinjEnableOptimizedRead.C1G2Read[index].WordCount,
                            WordPointer = reportContentSelector.ImpinjEnableOptimizedRead.C1G2Read[index].WordPointer
                        };
                        settings.Report.OptimizedReadOps.Add(tagReadOp);
                    }
                }
            }
            if (config.EventsAndReports != null)
                settings.HoldReportsOnDisconnect = config.EventsAndReports.HoldEventsAndReportsUponReconnect;
            if (config.KeepaliveSpec != null)
            {
                settings.Keepalives.Enabled = (uint)config.KeepaliveSpec.KeepaliveTriggerType > 0U;
                settings.Keepalives.PeriodInMs = config.KeepaliveSpec.PeriodicTriggerValue;
            }
            settings.Gpis = new GpiConfigGroup();
            settings.Gpos = new GpoConfigGroup();
            if (config.GPIPortCurrentState != null)
            {
                foreach (PARAM_GPIPortCurrentState portCurrentState in config.GPIPortCurrentState)
                {
                    settings.Gpis.Add(new GpiConfig(portCurrentState.GPIPortNum)
                    {
                        IsEnabled = portCurrentState.Config,
                    });
                }
            }
            for (int index = 0; index < config.Custom.Length; ++index)
            {
                if (config.Custom[index] is PARAM_ImpinjAdvancedGPOConfiguration gpoConfiguration)
                {
                    settings.Gpos.Add(new GpoConfig(gpoConfiguration.GPOPortNum)
                    {
                        Mode = (GpoMode)gpoConfiguration.GPOMode,
                        GpoPulseDurationMsec = gpoConfiguration.GPOPulseDurationMSec,
                    });
                }
                else if (config.Custom[index] is PARAM_ImpinjGPIDebounceConfiguration debounceConfiguration)
                {
                    settings.Gpis.GetGpi(debounceConfiguration.GPIPortNum).DebounceInMs = debounceConfiguration.GPIDebounceTimerMSec;
                }
                else if (config.Custom[index] is PARAM_ImpinjLinkMonitorConfiguration monitorConfiguration)
                {
                    settings.Keepalives.EnableLinkMonitorMode = monitorConfiguration.LinkMonitorMode == ENUM_ImpinjLinkMonitorMode.Enabled;
                    settings.Keepalives.LinkDownThreshold = monitorConfiguration.LinkDownThreshold;
                }
                else if (config.Custom[index] is PARAM_ImpinjPlacementConfiguration placementConfiguration)
                {
                    settings.SpatialConfig.Placement.HeightCm = placementConfiguration.HeightCm;
                    settings.SpatialConfig.Placement.FacilityXLocationCm = placementConfiguration.FacilityXLocationCm;
                    settings.SpatialConfig.Placement.FacilityYLocationCm = placementConfiguration.FacilityYLocationCm;
                    settings.SpatialConfig.Placement.OrientationDegrees = placementConfiguration.OrientationDegrees;
                }
                else if (config.Custom[index] is PARAM_ImpinjLocationReporting locationReporting)
                {
                    settings.SpatialConfig.Location.UpdateReportEnabled = locationReporting.EnableUpdateReport;
                    settings.SpatialConfig.Location.EntryReportEnabled = locationReporting.EnableEntryReport;
                    settings.SpatialConfig.Location.ExitReportEnabled = locationReporting.EnableExitReport;
                    settings.SpatialConfig.Location.DiagnosticReportEnabled = locationReporting.EnableDiagnosticReport;
                }
                else if (config.Custom[index] is PARAM_ImpinjDirectionReporting directionReporting)
                {
                    settings.SpatialConfig.Direction.UpdateReportEnabled = directionReporting.EnableUpdateReport;
                    settings.SpatialConfig.Direction.EntryReportEnabled = directionReporting.EnableEntryReport;
                    settings.SpatialConfig.Direction.ExitReportEnabled = directionReporting.EnableExitReport;
                    settings.SpatialConfig.Direction.DiagnosticReportEnabled = directionReporting.EnableDiagnosticReport;
                }
                else if (config.Custom[index] is PARAM_ImpinjReportBufferConfiguration reportBufferConfiguration)
                {
                    if (reportBufferConfiguration.ReportBufferMode == ENUM_ImpinjReportBufferMode.Low_Latency)
                        settings.Report.BufferMode = ReportBufferMode.LowLatency;
                    else
                        settings.Report.BufferMode = ReportBufferMode.Normal;
                }
                else if (config.Custom[index] is PARAM_ImpinjIntelligentAntennaManagement intelligentAntennaManagement)
                {
                    settings.Antennas.EnableIntelligentAntennaManagement = intelligentAntennaManagement.ManagementEnabled == ENUM_ImpinjIntelligentAntennaMode.Enabled;
                }
            }
            return settings;
        }

    }
}
