using System;

namespace EA.Impinj
{
    /// <summary>
    /// Class used to encapsulate the details of a
    /// <see cref="E:Impinj.OctaneSdk.ImpinjReader.ReportBufferOverflow" /> reader event.
    /// </summary>
    public class ReportBufferOverflowEvent : EventArgs
    {
        /// <summary>Timestamp of when the host received the event.</summary>
        public DateTime HostReceivedTime { get; set; }

        /// <summary>Timestamp of the event.</summary>
        public DateTime ReaderTimestamp { get; set; }
    }
}
