namespace EA.Impinj
{
    /// <summary>Enum defining the possible types of antenna event.</summary>
    public enum AntennaEventType
    {
        /// <summary>Indicates an antenna disconnected event.</summary>
        AntennaDisconnected,
        /// <summary>Indicates an antenna connected event.</summary>
        AntennaConnected,
    }
}
