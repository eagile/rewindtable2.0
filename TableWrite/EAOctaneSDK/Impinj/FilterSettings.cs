using System.Collections.Generic;

namespace EA.Impinj
{
    /// <summary>Class used to define inventory tag filter settings.</summary>
    public class FilterSettings
    {
        /// <summary>Inventory tag filter mode.</summary>
        public TagFilterMode Mode { get; set; }

        /// <summary>Tag filter 1.</summary>
        public TagFilter TagFilter1 { get; set; }

        /// <summary>Tag filter 2.</summary>
        public TagFilter TagFilter2 { get; set; }

        public List<TagSelectFilter> TagSelectFilters { get; set; }

        public FilterSettings()
        {
            Mode = TagFilterMode.None;
            TagFilter1 = new TagFilter();
            TagFilter2 = new TagFilter();
            TagSelectFilters = new List<TagSelectFilter>();
        }
    }
}