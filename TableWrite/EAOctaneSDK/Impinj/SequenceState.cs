namespace EA.Impinj
{
    /// <summary>Enum for defining the operation state for a sequence.</summary>
    public enum SequenceState
    {
        /// <summary>Indicates that all sequence operations are disabled.</summary>
        Disabled,
        /// <summary>Indicates that all sequence operations are active.</summary>
        Active,
    }
}
