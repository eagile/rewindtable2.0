namespace EA.Impinj
{
    /// <summary>
    /// Enum for defining how often inventory reports are sent from the reader.
    /// </summary>
    public enum ReportMode
    {
        /// <summary>
        /// Buffer on the reader and only send the tag report once asked for.
        /// </summary>
        WaitForQuery,
        /// <summary>Generate reports one at a time.</summary>
        Individual,
        /// <summary>Buffer on the reader until the reader has stopped.</summary>
        BatchAfterStop,
    }
}
