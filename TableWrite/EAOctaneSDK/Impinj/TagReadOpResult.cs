namespace EA.Impinj
{
    /// <summary>
    /// Specialization of <see cref="T:Impinj.OctaneSdk.TagOpResult" /> class that contains the
    /// results of an individual tag read operation, as reported in the
    /// <see cref="T:Impinj.OctaneSdk.TagOpReport" /> parameter of a
    /// <see cref="E:Impinj.OctaneSdk.ImpinjReader.TagOpComplete" /> event.
    /// </summary>
    public class TagReadOpResult : TagOpResult
    {
        /// <summary>The data read from the tag.</summary>
        public TagData Data { get; set; }

        /// <summary>The result of the tag read operation.</summary>
        public ReadResultStatus Result { get; set; }
    }
}
