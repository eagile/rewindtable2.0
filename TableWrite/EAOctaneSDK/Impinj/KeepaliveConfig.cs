namespace EA.Impinj
{
    /// <summary>
    /// Helper class used to manage the reader Keep Alive event configuration
    /// </summary>
    public class KeepaliveConfig
    {
        /// <summary>
        /// Parameter indicating whether the Keep Alive event is enabled or not.
        /// </summary>
        public bool Enabled { get; set; }

        /// <summary>Parameter defining the Keep Alive event interval.</summary>
        public uint PeriodInMs { get; set; }

        /// <summary>
        /// If link monitor mode is enabled, the reader will automatically
        /// shut down the network connection if it doesn't receive replies
        /// (ACKs) to keepalive messages. It does this after N missed replies,
        /// where N is specified by the parameter LinkDownThreshold.
        /// </summary>
        public bool EnableLinkMonitorMode { get; set; }

        /// <summary>
        /// Specifies the number of missed keepalive replies before
        /// the reader shuts down the network connection.
        /// </summary>
        public ushort LinkDownThreshold { get; set; }
    }
}
