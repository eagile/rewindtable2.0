using System.Collections.Generic;
using System.Linq;

namespace EA.Impinj
{
    /// <summary>
    /// Container class encapsulating all of the antenna configuration objects.
    /// This is for internal use only, and does not appear in the generated
    /// documentation.
    /// </summary>
    public class AntennaConfigGroup : List<AntennaConfig>
    {
        public static int ANTENNA_PORT_START_INDEX { get; } = 1;

        private readonly bool _createdBySerializer;

        /// <summary>Default Constructor</summary>
        public AntennaConfigGroup()
        {
            _createdBySerializer = true;
        }

        public AntennaConfigGroup(ICollection<ushort> portNumbers)
        {
            _createdBySerializer = true;
            foreach (ushort portNumber in portNumbers.OrderBy(p => p))
                Add(new AntennaConfig(portNumber));
        }

        public AntennaConfigGroup(ushort numAntennas)
        {
            _createdBySerializer = false;
            for (int i = ANTENNA_PORT_START_INDEX; i < ANTENNA_PORT_START_INDEX + numAntennas; i++)
                base.Add(new AntennaConfig((ushort)i));
        }

        /// <summary>
        /// Adds the provided antenna configuration object to the collection.
        /// For internal library use only.
        /// </summary>
        /// <param name="config">The AntennaConfig object to add.</param>
        public new void Add(AntennaConfig config)
        {
            if (!_createdBySerializer)
                throw new OctaneSdkException("Illegal operation - cannot Add to AntennaConfigGroup object!");
            base.Add(config);
        }

        /// <summary>Enable all antennas in the collection.</summary>
        public void EnableAll()
        {
            foreach (AntennaConfig antennaConfig in this)
                antennaConfig.IsEnabled = true;
        }

        /// <summary>Disable all antennas in the collection.</summary>
        public void DisableAll()
        {
            foreach (AntennaConfig antennaConfig in this)
                antennaConfig.IsEnabled = false;
        }

        /// <summary>Enable antennas by providing an array of antenna IDs.</summary>
        /// <param name="ids">An array of antenna IDs to enable.</param>
        public void EnableById(ICollection<ushort> ids)
        {
            foreach (ushort id in ids)
                GetAntenna(id).IsEnabled = true;
        }

        /// <summary>
        /// Disable antennas by providing an array of antenna IDs.
        /// </summary>
        /// <param name="ids">An array of antenna IDs to disable.</param>
        public void DisableById(ICollection<ushort> ids)
        {
            foreach (ushort id in ids)
                GetAntenna(id).IsEnabled = false;
        }

        /// <summary>Sets or gets the transmit power for all antennas</summary>
        public double TxPowerInDbm
        {
            set
            {
                foreach (AntennaConfig antennaConfig in this)
                    antennaConfig.TxPowerInDbm = value;
            }
        }

        /// <summary>Sets or gets the receive sensitivity for all antennas</summary>
        public double RxSensitivityInDbm
        {
            set
            {
                foreach (AntennaConfig antennaConfig in this)
                    antennaConfig.RxSensitivityInDbm = value;
            }
        }

        /// <summary>
        /// Specifies that the maximum antenna transmit power should be used.
        /// </summary>
        public bool TxPowerMax
        {
            set
            {
                foreach (AntennaConfig antennaConfig in this)
                    antennaConfig.MaxTxPower = value;
            }
        }

        /// <summary>
        /// Specifies that the maximum antenna receive sensitivity should be used.
        /// </summary>
        public bool RxSensitivityMax
        {
            set
            {
                foreach (AntennaConfig antennaConfig in this)
                    antennaConfig.MaxRxSensitivity = value;
            }
        }

        /// <summary>
        /// Specifies that the Reader looks for the presence of tags on an antenna before proceeding to inventory tags on it.
        /// </summary>
        public bool EnableIntelligentAntennaManagement { get; set; }

        /// <summary>Returns the settings for the specified antenna port.</summary>
        /// <note>Added virtual to allow for overriding in tests</note>
        /// <returns>
        /// The antenna settings or throws an exception if the port does not exist.
        /// </returns>
        public virtual AntennaConfig GetAntenna(ushort portNumber)
        {
            foreach (AntennaConfig antennaConfig in this)
            {
                if (antennaConfig.PortNumber == portNumber)
                    return antennaConfig;
            }
            throw new OctaneSdkException("Invalid antenna port number specified.");
        }

        /// <summary>The number of antennas in the collection.</summary>
        public int Length => Count;
    }
}
