﻿namespace EA.Impinj
{
    /// <summary>
    /// This is a class for containing a StateUnawareAction pair with a matching and non-matching action
    /// </summary>
    public class StateUnawareActionPair
    {
        /// <summary>The action to perform when the tag filter matches.</summary>
        public StateUnawareAction MatchingAction { get; set; }

        /// <summary>
        /// The action to perform when the tag filter does not match.
        /// </summary>
        public StateUnawareAction NonMatchingAction { get; set; }
    }
}
