namespace EA.Impinj
{
    /// <summary>
    /// Enum for configuring the tag search mode, which controls whether tags
    /// are singulated only once, or repeatedly.
    /// </summary>
    public enum SearchMode
    {
        /// <summary>Reader automatically selects a tag search mode.</summary>
        ReaderSelected = 0,
        /// <summary>
        /// Reader inventories tags that are in the session inventory state A only,
        /// inverting the inventory flag to state B for the appropriate session
        /// interval so that they no longer respond to reader queries; useful when
        /// there is a high tag count, or for a high-throughput application, where
        /// a reduction in repeated tag observation is acceptable.
        /// </summary>
        SingleTarget = 1,
        /// <summary>
        /// Reader inventories tags in session inventory state A first,
        /// inverting their inventory flag to state B.  Once all of the tags in
        /// state A have responded, the reader then inventories tags in state B.
        /// This process continues for as long as the tags stay in the antenna
        /// field of view.  This is useful when there is a low to medium tag
        /// count or for low-throughput applications where repeated tag observation
        /// is desirable.
        /// </summary>
        DualTarget = 2,
        /// <summary>
        /// For Monza tags and Session 1 ONLY; reader inventories tags and then
        /// uses the Monza TagFocus feature to suppress repeated observations
        /// for extended periods of time while tags are energized. This is
        /// useful for high-throughput applications where a single observation
        /// of each tag is acceptable.
        /// </summary>
        TagFocus = 3,
        /// <summary>
        /// Reader inventories tags that are in inventory state B and resets the
        /// inventory flag to state A so that they will respond to reader queries.
        /// This is useful for high-throughput applications when used in conjunction
        /// with SingleTarget search mode and session 2 or 3, which have long
        /// decay times.
        /// </summary>
        SingleTargetReset = 5,
        /// <summary>
        /// Reader inventories tags that are in inventory state A and transitions
        /// the inventory flag to state B. Once all of the tags in state A have
        /// responded, the reader sends a Gen2 Select command to transition tags
        /// back to state A. This is useful when there is a high tag count or for
        /// high-throughput applications where repeated tag observation is
        /// desirable.
        /// </summary>
        DualTargetBtoASelect = 6,
    }
}
