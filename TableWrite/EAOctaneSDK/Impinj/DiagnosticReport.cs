using System;
using System.Collections.Generic;

namespace EA.Impinj
{
    /// <summary>xArray diagnostic report</summary>
    public class DiagnosticReport : EventArgs
    {
        /// <summary>Timestamp of when the host received the event.</summary>
        public DateTime HostReceivedTime { get; set; }

        /// <summary>Diagnostic data. Internal use only.</summary>
        public List<uint> Metrics { get; } = new List<uint>();
    }
}
