namespace EA.Impinj
{
    /// <summary>Contains the current QT status of a tag.</summary>
    public class TagQtGetOpResult : TagOpResult
    {
        /// <summary>The result of the tag QT operation.</summary>
        public QtGetConfigResultStatus Result { get; set; }

        /// <summary>
        /// Determines whether to access the tag private or public
        /// data profile.
        /// </summary>
        public QtDataProfile DataProfile { get; set; }

        /// <summary>
        /// Determines whether to reduce tag access range down to a meter
        /// or so, or not.
        /// </summary>
        public QtAccessRange AccessRange { get; set; }
    }
}
