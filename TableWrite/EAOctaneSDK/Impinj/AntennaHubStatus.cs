namespace EA.Impinj
{
    /// <summary>
    /// Class encapsulating all antenna hub status information.
    /// </summary>
    public class AntennaHubStatus
    {
        /// <summary>
        /// The current hub connection status; either Connected, Disconnected
        /// or Unknown.
        /// </summary>
        public HubConnectedStatus Connected { get; set; }

        /// <summary>
        /// The current hub fault status; either No_Fault, RF_Power,
        /// RF_Power_On_Hub_1, RF_Power_On_Hub_2, RF_Power_On_Hub_3,
        /// RF_Power_On_Hub_4, No_Init, Serial_Overflow or Disconnected.
        /// </summary>
        public HubFaultStatus Fault { get; set; }

        /// <summary>The current hub ID.</summary>
        public ushort HubId { get; set; }
    }
}
