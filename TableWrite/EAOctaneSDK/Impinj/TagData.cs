using EA.Impinj.LLRP;
using System;
using System.Collections.Generic;

namespace EA.Impinj
{
    /// <summary>
    /// Class used to hold tag data in the raw form in which it is stored
    /// on the tag (list of unsigned shorts).
    /// </summary>
    public class TagData
    {
        public UInt16List Words { get; }

        public TagData()
        {
            Words = new UInt16List();
        }

        private TagData(IList<UInt16> words)
        {
            Words = new UInt16List(words);
        }

        private TagData(UInt16 word)
        {
            Words = new UInt16List() { word };
        }

        private TagData(UInt32 value)
        {
            Words = new UInt16List() {
                (ushort)(value >> 16),
                (ushort)(value & ushort.MaxValue)
            };
        }

        private TagData(string hexString)
        {
            Words = LLRPUtil.HexStringToUInt16List(hexString);
        }

        private TagData(IList<bool> bits)
        {
            int cursor = 0;
            int length = bits.Count / 16;
            Words = LLRPUtil.BitListToUInt16List(bits, ref cursor, length);
        }

        /// <summary>The size of the tag data, in bytes.</summary>
        public int CountBytes => Words.Count * 2;

        /// <summary>The size of the tag data, in bytes.</summary>
        public int CountWords => Words.Count;

        /// <summary>
        /// Constructor that creates a TagData object populated with data from a single 16-bit word
        /// </summary>
        /// <param name="data">Word to add to the TagData object.</param>
        /// <returns>Populated TagData object.</returns>
        /// <exception cref="T:Impinj.OctaneSdk.OctaneSdkException">
        /// Thrown when a null word is provided.
        /// </exception>
        public static TagData FromWord(ushort data) => new TagData(data);

        /// <summary>
        /// Conversion utility that outputs the data of the TagData object as a word.
        /// </summary>
        /// <returns>Contents of TagData as a word (ushort).</returns>
        public uint ToWord()
        {
            switch (Words.Count)
            {
                case 0:
                    return 0;
                case 1:
                    return Words[0];
                default:
                    throw new OctaneSdkException("Conversion failed. Data array should only contain up to 16-bits of data.");
            }
        }

        /// <summary>
        /// Constructor that creates a TagData object populated with data from
        /// a list of words (ushort).
        /// </summary>
        /// <param name="data">Populated list of words.</param>
        /// <returns>Populated TagData object</returns>
        public static TagData FromWordList(IList<ushort> data) => new TagData(data);

        /// <summary>
        /// Conversion utility that outputs the data of the TagData object as a list.
        /// </summary>
        /// <returns>Contents of TagData as a List of words (ushort).</returns>
        public List<ushort> ToWordList() => new List<ushort>(Words);

        public ushort[] ToWordArray() => Words.ToArray();

        public ushort WordAt(int index) => Words[index];

        /// <summary>
        /// Constructor that creates a TagData object populated with data from
        /// an unsigned int.
        /// </summary>
        /// <param name="data">Unsigned int.</param>
        /// <returns>Populated TagData object.</returns>
        public static TagData FromUnsignedInt(uint data) => new TagData(data);

        /// <summary>
        /// Conversion utility that outputs the data of the TagData object as an
        /// unsigned int.  This will only work for TagData objects that contain
        /// 1 or 2 16-bit words of data.
        /// </summary>
        /// <returns>Up to 2 words of data as an unsigned int.</returns>
        /// <exception cref="T:Impinj.OctaneSdk.OctaneSdkException">
        /// Thrown when the TagData object contains more than 32-bits of data.
        /// </exception>
        public uint ToUnsignedInt()
        {
            switch (Words.Count)
            {
                case 0:
                    return 0;
                case 1:
                    return Words[0];
                case 2:
                    return (uint)((Words[0] << 16) | Words[1]);
                default:
                    throw new OctaneSdkException("Conversion failed. Data array should only contain up to 32-bits of data.");
            }
        }

        /// <summary>
        /// Constructor that creates a TagData object populated with data from
        /// a Hex string of arbitrary length.
        /// </summary>
        /// <param name="hex">Hex number represented as a string.</param>
        /// <returns>Populated TagData object.</returns>
        /// <exception cref="T:Impinj.OctaneSdk.OctaneSdkException">
        /// Thrown when a null or non-hex string is provided.
        /// </exception>
        public static TagData FromHexString(string hex) => new TagData(hex);

        /// <summary>
        /// Conversion utility that outputs the data of the TagData object as a
        /// string in hexadecimal format, with no spaces between each word.
        /// </summary>
        /// <returns>Contents of TagData represented as a string.</returns>
        public string ToHexString() => LLRPUtil.UInt16ListToHexString(Words);

        /// <summary>
        /// Conversion utility that outputs the data of the TagData object as a
        /// string in hexadecimal format, with each word separated by a space.
        /// </summary>
        /// <returns>Contents of TagData represented as a string.</returns>
        public string ToHexWordString() => LLRPUtil.UInt16ListToHexWordString(Words);

        public static TagData FromBitList(IList<bool> data) => new TagData(data);

        /// <summary>
        /// Conversion utility that outputs the data of the TagData object as a
        /// string in hexadecimal format, with each word separated by a space.
        /// </summary>
        /// <returns>Contents of TagData represented as a string.</returns>
        public override string ToString() => LLRPUtil.UInt16ListToHexWordString(Words);
    }
}
