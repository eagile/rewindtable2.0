namespace EA.Impinj
{
    /// <summary>
    /// Specifies the overflow status of a Direction Report.
    /// It wraps the equivalent LTK type ENUM_ImpinjDirectionTagPopulationStatus.
    /// </summary>
    public enum DirectionTagPopulationStatus
    {
        /// <summary>The report is within all tag population limits.</summary>
        OK,
        /// <summary>The report count is above the user specified limit.</summary>
        UserOverflow,
        /// <summary>The report count is above the system limit.</summary>
        SystemOverflow,
    }
}
