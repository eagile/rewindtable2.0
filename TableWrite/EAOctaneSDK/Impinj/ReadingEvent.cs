using System;

namespace EA.Impinj
{
    public class ReadingEvent : EventArgs
    {
        /// <summary>Timestamp of when the host received the event.</summary>
        public DateTime HostReceivedTime { get; set; }

        /// <summary>Timestamp of the event.</summary>
        public DateTime ReaderTimestamp { get; set; }

        /// <summary>
        /// The state of reading
        /// </summary>
        public bool IsReading { get; set; }
    }
}
