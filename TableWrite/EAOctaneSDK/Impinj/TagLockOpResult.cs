namespace EA.Impinj
{
    /// <summary>
    /// Specialization of <see cref="T:Impinj.OctaneSdk.TagOpResult" /> class that contains the
    /// results of an individual tag lock operation, as reported in the
    /// <see cref="T:Impinj.OctaneSdk.TagOpReport" /> parameter of a
    /// <see cref="E:Impinj.OctaneSdk.ImpinjReader.TagOpComplete" /> event.
    /// </summary>
    public class TagLockOpResult : TagOpResult
    {
        /// <summary>The results of the tag lock operation.</summary>
        public LockResultStatus Result { get; set; }
    }
}
