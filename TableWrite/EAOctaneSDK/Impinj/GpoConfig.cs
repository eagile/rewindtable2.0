namespace EA.Impinj
{
    /// <summary>
    /// Class used to define the configuration settings for an individual
    /// general purpose output (GPO) port.
    /// </summary>
    public class GpoConfig
    {
        public GpoConfig(ushort NewPortNumber)
        {
            PortNumber = NewPortNumber;
            Mode = GpoMode.Normal;
            GpoPulseDurationMsec = 0;
        }

        /// <summary>Defines which Advanced GPO mode to use.</summary>
        public GpoMode Mode { get; set; }

        /// <summary>
        /// Defines the pulse duration when the Pulsed Advanced GPO mode is used.
        /// </summary>
        public uint GpoPulseDurationMsec { get; set; }

        /// <summary>
        /// Specifies the GPI port number (starting at port 1) to configure.
        /// </summary>
        /// <exception cref="T:Impinj.OctaneSdk.OctaneSdkException">
        /// Thrown when PortNumber is set to 0;
        /// </exception>
        public ushort PortNumber { get; set; }
    }
}
