namespace EA.Impinj
{
    /// <summary>
    /// Class that is used to define the configuration settings for an
    /// individual general purpose input (GPI) port.
    /// </summary>
    public class GpiConfig
    {
        public GpiConfig(ushort NewPortNumber)
        {
            PortNumber = NewPortNumber;
            IsEnabled = true;
            DebounceInMs = 0;
        }

        /// <summary>Specifies whether this port should be enabled.</summary>
        public bool IsEnabled { get; set; }

        /// <summary>
        /// Specifies a time interval to wait before the reader will accept
        /// that a GPI state change is persistent.
        /// </summary>
        public uint DebounceInMs { get; set; }

        /// <summary>
        /// Specifies the GPI port number (starting at port 1) to configure.
        /// </summary>
        /// <exception cref="T:Impinj.OctaneSdk.OctaneSdkException">
        /// Thrown when PortNumber is set to 0;
        /// </exception>
        public ushort PortNumber { get; set; }
    }
}
