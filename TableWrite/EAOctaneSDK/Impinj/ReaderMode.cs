namespace EA.Impinj
{
    /// <summary>
    /// Enum defining the configured Reader mode, which controls the modulation
    /// and data rate for the the link between the reader and the tags.
    /// AutoSetDenseReader is recommended as the most robust mode. Not all modes
    /// are available on all models and regions.
    /// </summary>
    public enum ReaderMode
    {
        /// <summary>
        /// Fastest possible data transfer rates between tag and reader when
        /// the population of readers is very low and the likelihood of
        /// interference between readers is very low.
        /// DRValue: DRV_64_3
        /// MValue: MV_FM0
        /// Tari: 6250
        /// </summary>
        MaxThroughput = 0,

        /// <summary>
        /// Provides a fast read rate when there is modest interference from
        /// other readers, but a low likelihood of co-channel, adjacent-channel,
        /// or alternate-channel interferers.
        /// DRValue: DRV_64_3
        /// MValue: MV_2
        /// Tari: 6250
        /// </summary>
        Hybrid = 1,

        /// <summary>
        /// Provides an intermediate read rate when the population of readers is
        /// large and the likelihood of interference is high.
        /// DRValue: DRV_64_3
        /// MValue: MV_4
        /// Tari: 20000
        /// </summary>
        DenseReaderM4 = 2,

        /// <summary>
        /// Provides a low read rate (~half that of DenseReaderM4) for extreme
        /// interference environments where tags are stationary or moving slowly.
        /// DRValue: DRV_64_3
        /// MValue: MV_8
        /// Tari: 20000
        /// </summary>
        DenseReaderM8 = 3,

        /// <summary>
        /// Provides maximum reliability and read rate when the population of
        /// readers is low and the likelihood of interference is low.
        /// DRValue: DRV_64_3
        /// MValue: MV_4
        /// Tari: 7140
        /// </summary>
        MaxMiller = 4,

        /// <summary>
        /// Faster Forward link than Mode 2, available with regions: ETSI,
        /// China, India, Japan (916.7-920.9 MHz and 952-956 MHz), Korea, and South Africa.
        /// </summary>
        DenseReaderM4Two = 5,

        /// <summary>
        /// The reader monitors RF noise and interference and then automatically and
        /// continuously optimizes the reader’s configuration, selecting the appropriate
        /// configuration settings for the best, most reliable performance for environments
        /// where tags might be transient.
        /// </summary>
        AutoSetDenseReader = 1000, // 0x000003E8

        /// <summary>
        /// The reader monitors RF noise and interference and then automatically and
        /// continuously optimizes the reader’s configuration, selecting the appropriate
        /// configuration settings for the best, most reliable performance for environments
        /// where tags are relatively static.
        /// DRValue: DRV_8
        /// MValue: MV_FM0
        /// Tari: 6250
        /// </summary>
        AutoSetDenseReaderDeepScan = 1002, // 0x000003EA

        /// <summary>
        /// A refinement of AutoSetDenseReaderDeepScan, targeted toward static environments
        /// where difficult to read tags are not expected.
        /// DRValue: DRV_8
        /// MValue: MV_FM0
        /// Tari: 6250
        /// </summary>
        AutoSetStaticFast = 1003, // 0x000003EB

        /// <summary>
        /// A refinement of AutoSetDenseReaderDeepScan, targeted toward static environments
        /// where difficult to read tags are expected and we are ready to sacrifice
        /// performance to ensure that they are read
        /// DRValue: DRV_8
        /// MValue: MV_FM0
        /// Tari: 6250
        /// </summary>
        AutoSetStaticDRM = 1004, // 0x000003EC

        /// <summary>Reserved for future use</summary>
        AutoSetCustom = 1005, // 0x000003ED

        /// <summary>
        /// DRValue: DRV_64_3
        /// MValue: MV_FM0
        /// Tari: 6250
        /// </summary>
        R700_100 = 100,

        /// <summary>
        /// DRValue: DRV_64_3
        /// MValue: MV_2
        /// Tari: 6250
        /// </summary>
        R700_120 = 120,

        /// <summary>
        /// DRValue: DRV_64_3
        /// MValue: MV_2
        /// Tari: 14290
        /// </summary>
        R700_121 = 121,

        /// <summary>
        /// DRValue: DRV_64_3
        /// MValue: MV_2
        /// Tari: 20000
        /// </summary>
        R700_122 = 122,

        /// <summary>
        /// DRValue: DRV_64_3
        /// MValue: MV_4
        /// Tari: 7140
        /// </summary>
        R700_140 = 140,

        /// <summary>
        /// DRValue: DRV_64_3
        /// MValue: MV_4
        /// Tari: 20000
        /// </summary>
        R700_141 = 141,

        /// <summary>
        /// DRValue: DRV_64_3
        /// MValue: MV_4
        /// Tari: 20000
        /// </summary>
        R700_142 = 142,

        /// <summary>
        /// DRValue: DRV_64_3
        /// MValue: MV_8
        /// Tari: 20000
        /// </summary>
        R700_180 = 180,

        /// <summary>
        /// DRValue: DRV_64_3
        /// MValue: MV_8
        /// Tari: 20000
        /// </summary>
        R700_181 = 181,

        /// <summary>
        /// DRValue: DRV_64_3
        /// MValue: MV_8
        /// Tari: 6250
        /// </summary>
        R700_184 = 184,

        /// <summary>
        /// DRValue: DRV_64_3
        /// MValue: MV_8
        /// Tari: 20000
        /// </summary>
        R700_185 = 185,

        /// <summary>
        /// DRValue: DRV_8
        /// MValue: MV_FM0
        /// Tari: 6250
        /// </summary>
        R700_1110 = 1110,

        /// <summary>
        /// DRValue: DRV_8
        /// MValue: MV_FM0
        /// Tari: 6250
        /// </summary>
        R700_1111 = 1111,

        /// <summary>
        /// DRValue: DRV_8
        /// MValue: MV_FM0
        /// Tari: 6250
        /// </summary>
        R700_1112 = 1112,
    }
}
