using System.Collections.Generic;

namespace EA.Impinj
{
    /// <summary>
    /// Container class encapsulating all of the antenna hub status objects.
    /// This is for internal use only, and does not appear in the generated
    /// documentation.
    /// </summary>
    public class AntennaHubStatusGroup : List<AntennaHubStatus>
    {
        private readonly bool _ceatedBySerializer;

        public AntennaHubStatusGroup()
        {
            _ceatedBySerializer = true;
        }

        /// <summary>Returns the settings for the specified antenna hub.</summary>
        /// <returns>The antenna hub status or throws an exception if the hub does not exist.</returns>
        public AntennaHubStatus GetAntennaHub(ushort hubId)
        {
            foreach (AntennaHubStatus antHubStatuse in this)
            {
                if (antHubStatuse.HubId == hubId)
                    return antHubStatuse;
            }
            throw new OctaneSdkException("Invalid Antenna Hub number specified : " + hubId);
        }

        /// <summary>
        /// Adds the provided antenna hub status object to the collection.
        /// For internal library use only.
        /// </summary>
        /// <param name="status">The antenna hub status object to add.</param>
        public void Add(object status)
        {
            if (!_ceatedBySerializer)
                throw new OctaneSdkException("Illegal operation - cannot Add to AntennaHubStatusGroup object!");
            base.Add((AntennaHubStatus)status);
        }
    }
}
