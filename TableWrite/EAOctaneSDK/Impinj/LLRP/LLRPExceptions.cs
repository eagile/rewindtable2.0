using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace EA.Impinj.LLRP
{
    [Serializable]
    public class LLRPException : Exception
    {
        public LLRPException() : base() { }
        public LLRPException(string message) : base(message) { }
        public LLRPException(string message, Exception innerException) : base(message, innerException) { }
        protected LLRPException(SerializationInfo serializationInfo, StreamingContext streamingContext) : base(serializationInfo, streamingContext) { }
    }

    [Serializable]
    public class LLRPConnectionException : LLRPException
    {
        public ENUM_ConnectionAttemptStatusType Status => m_status;
        private readonly ENUM_ConnectionAttemptStatusType m_status;

        public LLRPConnectionException() : base() { }
        public LLRPConnectionException(string message) : base(message) { }
        public LLRPConnectionException(string message, Exception innerException) : base(message, innerException) { }
        public LLRPConnectionException(ENUM_ConnectionAttemptStatusType status) : base()
        {
            m_status = status;
        }
        protected LLRPConnectionException(SerializationInfo serializationInfo, StreamingContext streamingContext) : base(serializationInfo, streamingContext)
        {
            string status = serializationInfo.GetString("Status");
            Enum.TryParse<ENUM_ConnectionAttemptStatusType>(status, true, out m_status);
        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo serializationInfo, StreamingContext streamingContext)
        {
            base.GetObjectData(serializationInfo, streamingContext);
            serializationInfo.AddValue("Status", m_status.ToString());
        }
    }

    [Serializable]
    public class LLRPNetworkException : LLRPException
    {
        public LLRPNetworkException() { }
        public LLRPNetworkException(string message) : base(message) { }
        public LLRPNetworkException(string message, Exception innerException) : base(message, innerException) { }
        public LLRPNetworkException(Exception innerException) : base(null, innerException) { }
        protected LLRPNetworkException(SerializationInfo serializationInfo, StreamingContext streamingContext) : base(serializationInfo, streamingContext) { }
    }

    [Serializable]
    public class LLRPTimeoutException : LLRPException
    {
        public LLRPTimeoutException() { }
        public LLRPTimeoutException(string message) : base(message) { }
        public LLRPTimeoutException(string message, Exception innerException) : base(message, innerException) { }
        public LLRPTimeoutException(Exception innerException) : base(null, innerException) { }
        protected LLRPTimeoutException(SerializationInfo serializationInfo, StreamingContext streamingContext) : base(serializationInfo, streamingContext) { }
    }

    [Serializable]
    public class LLRPMalformedPacketException : LLRPException
    {
        public LLRPMalformedPacketException() { }
        public LLRPMalformedPacketException(string message) : base(message) { }
        public LLRPMalformedPacketException(string message, Exception innerException) : base(message, innerException) { }
        protected LLRPMalformedPacketException(SerializationInfo serializationInfo, StreamingContext streamingContext) : base(serializationInfo, streamingContext) { }
    }

    [Serializable]
    public class LLRPMessageException : LLRPException
    {
        public override string Message => m_responseStatus?.ErrorDescription ?? base.Message;

        public PARAM_LLRPStatus ResponseStatus => m_responseStatus;
        private readonly PARAM_LLRPStatus m_responseStatus;

        public virtual ILLRPMessage ResponseMessage => m_responseMessage;
        private readonly ILLRPMessage m_responseMessage;

        public LLRPMessageException() : base() { }
        public LLRPMessageException(string message) : base(message) { }
        public LLRPMessageException(string message, Exception innerException) : base(message, innerException) { }
        public LLRPMessageException(ILLRPResponseMessage responseMessage)
        {
            m_responseStatus = responseMessage?.LLRPStatus;
            m_responseMessage = responseMessage;
        }
        protected LLRPMessageException(SerializationInfo serializationInfo, StreamingContext streamingContext) : base(serializationInfo, streamingContext)
        {
            LLRPXmlParser parser = new LLRPXmlParser();
            LLRPBinaryDecoder decoder = new LLRPBinaryDecoder();

            string responseStatusXml = serializationInfo.GetString("ResponseStatusXml");
            m_responseStatus = string.IsNullOrEmpty(responseStatusXml) ? null : PARAM_LLRPStatus.FromXmlString(parser, responseStatusXml);

            string responseMessageHex = serializationInfo.GetString("ResponseMessageHex");
            m_responseMessage = string.IsNullOrEmpty(responseMessageHex) ? null : decoder.Decode(LLRPUtil.HexStringToByteList(responseMessageHex));
        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo serializationInfo, StreamingContext streamingContext)
        {
            base.GetObjectData(serializationInfo, streamingContext);
            serializationInfo.AddValue("ResponseStatusXml", m_responseStatus.ToXmlString());
            serializationInfo.AddValue("ResponseMessageHex", LLRPUtil.BitListToHexString(m_responseMessage.ToBitList()));
        }
    }
}
