﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace EA.Impinj.LLRP
{
    public partial class LLRPServer : LLRPStream
    {
        public event EventHandler<ListenerChangedEventArgs> ListenerChanged;
        public event EventHandler<ConnectionChangedEventArgs> ConnectionChanged;
        public event EventHandler<ClientRejectedEventArgs> ConnectionRejected;
        public event EventHandler<MessageReceivedEventArgs> MessageReceived;

        protected virtual void OnListenerStatusChanged(bool isListening, Exception exception = null)
        {
            try { ListenerChanged?.Invoke(this, new ListenerChangedEventArgs(DateTime.UtcNow, isListening, exception)); } catch { }
        }

        protected virtual void OnConnectionChanged(bool isConnected, Exception exception = null)
        {
            try { ConnectionChanged?.Invoke(this, new ConnectionChangedEventArgs(DateTime.UtcNow, isConnected, exception)); } catch { }
        }

        protected virtual void OnConnectionRejected(string remoteHost, Exception exception = null)
        {
            try { ConnectionRejected?.Invoke(this, new ClientRejectedEventArgs(DateTime.UtcNow, remoteHost, exception)); } catch { }
        }

        protected virtual void OnMessageReceived(ILLRPMessage message)
        {
            try { MessageReceived?.Invoke(this, new MessageReceivedEventArgs(message)); } catch { }
        }

        public override bool IsConnected => IsStreamConnected && _initialized;
        protected override bool IsStreamConnected => base.IsStreamConnected && _connected;
        public virtual bool IsListening => _listening;

        public string Address { get; set; } = null;
        public int Port { get; set; } = LLRPConstants.LLRP_PORT;
        public bool UseTLS { get; set; } = false;

        public int ResponseTimeout { get; set; } = LLRPConstants.DEFAULT_TIME_OUT;

        private bool _started = false;
        private bool _listening = false;
        private bool _stopped = true;

        private bool _opened = false;
        private bool _connected = false;
        private bool _initialized = false;
        private bool _closed = true;

        private readonly SemaphoreSlim _listenerLock = new SemaphoreSlim(1, 1);
        private readonly SemaphoreSlim _clientLock = new SemaphoreSlim(1, 1);
        private readonly object _statusLock = new object();

        protected CancellationTokenSource ListenerCts { get; private set; } = new CancellationTokenSource(0);
        protected CancellationToken ListenerCt => ListenerCts.Token;

        protected TcpListener TcpListener { get; private set; }
        protected TcpClient TcpClient { get; private set; }

        private readonly Timer _keepaliveTimer;
        private bool _keepaliveEnabled = false;
        private int _keepaliveInterval = -1;
        private int _missedThreshold = 0;
        private int _keepAliveMissedCount = 0;

        private Task _listenTask = Task.CompletedTask;

        public LLRPServer() : base()
        {
            StreamCts.Cancel();

            _keepaliveTimer = new Timer(KeepaliveTimer_Elapsed, null, -1, -1);
        }

        public Task StartAsync() => StartAsync(Address, Port, UseTLS);
        public async Task StartAsync(string address, int port, bool useTls)
        {
            if (useTls)
                throw new NotImplementedException("LLRPServer does not currently support TLS");
            if (string.IsNullOrEmpty(address))
                address = "*";

            IPAddress ipAddress;
            if (address == "*")
                ipAddress = (Socket.OSSupportsIPv6 ? IPAddress.IPv6Any : IPAddress.Any);
            else
                ipAddress = IPAddress.Parse(address);

            await _listenerLock.WaitAsync();
            try
            {
                lock (_statusLock)
                {
                    if (_started || _listening || !_stopped)
                        throw new InvalidOperationException("Already listening");
                    _started = true;
                    _stopped = false;
                }

                Address = address;
                Port = port;
                UseTLS = useTls;

                ListenerCts.Cancel();
                ListenerCts = new CancellationTokenSource();
                _listenTask = Task.Run(() => ListenAsync(ipAddress, port, ListenerCts), ListenerCts.Token);
            }
            finally
            {
                _listenerLock.Release();
            }
        }

        public async Task<bool> StopAsync()
        {
            await _listenerLock.WaitAsync();
            try
            {
                bool stopped = !ListenerCts.IsCancellationRequested;
                ListenerCts.Cancel();
                await _listenTask.ConfigureAwait(false);
                return stopped;
            }
            finally
            {
                _listenerLock.Release();
            }
        }

        public Task<bool> DisconnectAsync() => DisconnectAsync(null);

        protected virtual async Task<bool> DisconnectAsync(Exception exception)
        {
            await _clientLock.WaitAsync().ConfigureAwait(false);
            try
            {
                return await CloseStreamAsync(exception).ConfigureAwait(false);
            }
            finally
            {
                _clientLock.Release();
            }
        }

        private async Task ListenAsync(IPAddress ipAddress, int port, CancellationTokenSource cts)
        {
            TcpClient tcpClient = null;
            CancellationToken ct = cts.Token;

            Exception exception = null;

            try
            {
                while (!ct.IsCancellationRequested)
                {
                    {
                        bool raiseEvent = false;
                        lock (_statusLock)
                        {
                            if (TcpListener == null)
                            {
                                raiseEvent = true;
                                TcpListener = new TcpListener(ipAddress, port);
                                if (Socket.OSSupportsIPv6)
                                {
                                    TcpListener.Server.DualMode = true;
                                }
                                TcpListener.Start();
                                if (!_started || _stopped)
                                    throw new InvalidOperationException("Listener stopped");
                                _listening = true;
                            }
                        }
                        if (raiseEvent)
                        {
                            OnListenerStatusChanged(true);
                        }
                    }

                    using (ct.Register(() =>
                    {
                        lock (_statusLock)
                        {
                            try { TcpListener?.Stop(); } catch { }
                            try { TcpListener?.Server?.Dispose(); } catch { }
                        }
                    }, false))
                    {
                        tcpClient = await TcpListener.AcceptTcpClientAsync().ConfigureAwait(false);
                    }

                    bool accepted = false;
                    await _clientLock.WaitAsync(ct).ConfigureAwait(false);
                    try
                    {
                        lock (_statusLock)
                        {
                            if (!_opened && !_connected && _closed)
                            {
                                accepted = true;
                                _opened = true;
                                _closed = false;
                            }
                        }

                        if (accepted)
                        {
                            await ConnectAsync(tcpClient).ConfigureAwait(false);
                            tcpClient = null;
                        }
                    }
                    finally
                    {
                        _clientLock.Release();
                    }

                    if (!accepted)
                    {
                        await RejectConnectionAsync(tcpClient).ConfigureAwait(false);
                        tcpClient = null;
                    }
                }
            }
            catch (Exception ex)
            {
                exception = ex;
            }
            finally
            {
                bool stopped = cts.IsCancellationRequested;
                bool raiseEvent = false;

                cts.Cancel();
                if (tcpClient != null)
                {
                    try { await RejectConnectionAsync(tcpClient).ConfigureAwait(false); } catch { }
                    tcpClient = null;
                }

                lock (_statusLock)
                {
                    if (TcpListener != null)
                    {
                        raiseEvent = true;
                        try { TcpListener?.Stop(); } catch { }
                        try { TcpListener?.Server?.Dispose(); } catch { }
                        TcpListener = TcpListener = null;
                    }

                    _started = false;
                    _listening = false;
                    _stopped = true;
                }

                if (raiseEvent)
                {
                    OnListenerStatusChanged(false, stopped ? null : exception);
                }
            }
        }

        private async Task RejectConnectionAsync(TcpClient tcpClient)
        {
            if (tcpClient != null)
            {
                string remoteHost = null;
                try { remoteHost = tcpClient.Client?.RemoteEndPoint?.ToString(); } catch { }
                ENUM_ConnectionAttemptStatusType status = ENUM_ConnectionAttemptStatusType.Failed_A_Client_Initiated_Connection_Already_Exists;
                try
                {
                    MSG_READER_EVENT_NOTIFICATION connectResponse = BuildConnectionResponse(status);
                    byte[] messageBytes = LLRPUtil.BitsToBytes(connectResponse.ToBitList());
                    await tcpClient.GetStream().WriteAsync(messageBytes, 0, messageBytes.Length, CancellationToken.None).ConfigureAwait(false);
                }
                catch { }
                finally
                {
                    try { tcpClient?.GetStream()?.Dispose(); } catch { }
                    try { tcpClient?.Dispose(); } catch { }
                }
                OnConnectionRejected(remoteHost, new LLRPConnectionException(status));
            }
        }

        private async Task ConnectAsync(TcpClient tcpClient)
        {
            string remoteHost = null;
            try { remoteHost = tcpClient.Client?.RemoteEndPoint?.ToString(); } catch { }
            try
            {
                StreamCts = new CancellationTokenSource();
                TcpClient = tcpClient;
                TcpClient.Client.NoDelay = true;
                Stream = TcpClient.GetStream();
                await StartProcessingStream(BufferSize, PendingRequestLimit).ConfigureAwait(false);
                lock (_statusLock)
                {
                    if (!_opened || _closed)
                        throw new InvalidOperationException("Connection closed");
                    _connected = true;
                }
                await InitializeStreamAsync().ConfigureAwait(false);
                lock (_statusLock)
                {
                    if (!_opened || !_connected || _closed)
                        throw new InvalidOperationException("Connection closed");
                    _initialized = true;
                    _keepaliveTimer.Change(-1, -1);
                    _keepAliveMissedCount = 0;
                    if (_keepaliveEnabled)
                    {
                        _keepaliveTimer.Change(_keepaliveInterval, -1);
                    }
                }
                OnConnectionChanged(true);
            }
            catch (LLRPException lex)
            {
                await CloseStreamAsync(lex).ConfigureAwait(false);
                OnConnectionRejected(remoteHost, lex);
            }
            catch (Exception ex)
            {
                LLRPNetworkException netex = new LLRPNetworkException(ex.Message, ex);
                await CloseStreamAsync(netex).ConfigureAwait(false);
                OnConnectionRejected(remoteHost, netex);
            }
        }

        protected virtual async Task InitializeStreamAsync()
        {
            MSG_READER_EVENT_NOTIFICATION connectResponse = BuildConnectionResponse(ENUM_ConnectionAttemptStatusType.Success);
            await SendAsync(connectResponse).ConfigureAwait(false);
        }

        protected virtual async Task<bool> CloseStreamAsync(Exception exception)
        {
            bool connected;
            lock (_statusLock)
            {
                if (_closed) return false;
                _closed = true;

                connected = _connected;
                _connected = false;
                _keepaliveTimer.Change(-1, -1);
            }

            try { await FinalizeStreamAsync().ConfigureAwait(false); } catch { }

            StreamCts.Cancel();
            try { Stream?.Dispose(); } catch { }
            Stream = null;
            try { TcpClient?.GetStream()?.Dispose(); } catch { }
            try { TcpClient?.Dispose(); } catch { }
            TcpClient = null;

            try { await WaitForStreamProcessingToCompleteAsync().ConfigureAwait(false); } catch { }

            lock (_statusLock)
            {
                _opened = false;
            }

            if (connected)
            {
                OnConnectionChanged(false, exception);
                return true;
            }
            return false;
        }

        protected virtual Task FinalizeStreamAsync() => Task.CompletedTask;



        private MSG_READER_EVENT_NOTIFICATION BuildConnectionResponse(ENUM_ConnectionAttemptStatusType status)
        {
            MSG_READER_EVENT_NOTIFICATION connectResponse = new MSG_READER_EVENT_NOTIFICATION(GetNextMessageID())
            {
                ReaderEventNotificationData = new PARAM_ReaderEventNotificationData
                {
                    Timestamp = new UNION_Timestamp
                    {
                        new PARAM_UTCTimestamp
                        {
                            Microseconds = LLRPUtil.ConvertDateTimeToMicroseconds(DateTime.UtcNow)
                        }
                    },
                    ConnectionAttemptEvent = new PARAM_ConnectionAttemptEvent
                    {
                        Status = status
                    }
                }
            };
            return connectResponse;
        }

        protected override async Task ProcessMessageAsync(ILLRPMessage message)
        {
            await base.ProcessMessageAsync(message).ConfigureAwait(false);
            if (message is MSG_CLOSE_CONNECTION closeConnection)
            {
                try
                {
                    MSG_CLOSE_CONNECTION_RESPONSE closeResponse = new MSG_CLOSE_CONNECTION_RESPONSE(closeConnection.MsgID)
                    {
                        LLRPStatus = new PARAM_LLRPStatus()
                        {
                            StatusCode = ENUM_StatusCode.M_Success
                        }
                    };
                    await SendAsync(closeResponse).ConfigureAwait(false);
                    await CloseStreamAsync(null).ConfigureAwait(false);
                }
                catch (Exception ex)
                {
                    await CloseStreamAsync(ex).ConfigureAwait(false);
                }
            }
            else if (message is MSG_KEEPALIVE_ACK)
            {
                lock (_statusLock)
                {
                    if (_keepaliveEnabled)
                    {
                        _keepAliveMissedCount--;
                    }
                }
            }
            OnMessageReceived(message);
        }

        public void EnableKeepAlives(int intervalMilliseconds, int missedThreshold)
        {
            lock (_statusLock)
            {
                _keepaliveTimer.Change(-1, -1);
                _keepaliveEnabled = true;
                _keepaliveInterval = intervalMilliseconds;
                _missedThreshold = missedThreshold;
                _keepAliveMissedCount = 0;
                if (_connected)
                {
                    _keepaliveTimer.Change(_keepaliveInterval, -1);
                }
            }
        }

        public void DisableKeepAlives()
        {
            lock (_statusLock)
            {
                _keepaliveEnabled = false;
                _keepaliveTimer.Change(-1, -1);
                _missedThreshold = 0;
                _keepAliveMissedCount = 0;
            }
        }

        public async void KeepaliveTimer_Elapsed(object stateInfo)
        {
            try
            {
                bool connected = false;
                bool threasholdExceeded = false;
                lock (_statusLock)
                {
                    if (_connected)
                    {
                        connected = true;
                        if (_missedThreshold > 0)
                        {
                            if (_keepAliveMissedCount >= _missedThreshold)
                                threasholdExceeded = true;

                            if (_keepAliveMissedCount < 1)
                                _keepAliveMissedCount = 1;
                            else
                                _keepAliveMissedCount++;
                        }
                    }
                }
                if (threasholdExceeded)
                {
                    await CloseStreamAsync(new LLRPTimeoutException("Keep alive timed out")).ConfigureAwait(false);
                }
                if (connected)
                {
                    MSG_KEEPALIVE msg = new MSG_KEEPALIVE(GetNextMessageID());
                    await SendAsync(msg).ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
                await CloseStreamAsync(ex).ConfigureAwait(false);
            }
            finally
            {
                lock (_statusLock)
                {
                    if (_keepaliveEnabled && _connected)
                    {
                        _keepaliveTimer.Change(_keepaliveInterval, -1);
                    }
                }
            }
        }

        protected override async Task OnStreamErrorAsync(Exception exception)
        {
            await CloseStreamAsync(exception).ConfigureAwait(false);
        }

        #region Dispose

        bool disposed = false;
        protected override void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                StreamCts.Cancel();
                try { Stream?.Dispose(); } catch { }
                Stream = null;
                try { TcpClient?.GetStream()?.Dispose(); } catch { }
                try { TcpClient?.Dispose(); } catch { }
                TcpClient = null;
            }

            // Free any unmanaged objects here.

            disposed = true;
            base.Dispose(disposing);
        }

        #endregion Dispose
    }
}
