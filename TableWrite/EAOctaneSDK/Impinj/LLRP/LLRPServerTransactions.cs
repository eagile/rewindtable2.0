
using System;
using System.Threading.Tasks;

namespace EA.Impinj.LLRP
{
    /// <summary>
    /// LLRP server transaction methods
    /// </summary>
    public partial class LLRPServer
    {
    
        /// <summary>
        /// Send customized message to the client
        /// </summary>
        /// <param name="msg">Message to be sent to the client.</param>
        /// <param name="responseVendorID">The vendor of the expected response message</param>
        /// <param name="responseSubTypeID">The sub type of the expected response message</param>
        /// <param name="timeoutMilliseconds">Timeout in milliseconds.</param>
        /// <returns>Custom Message</returns>
        public async Task<MSG_CUSTOM_MESSAGE> CUSTOM_MESSAGE(MSG_CUSTOM_MESSAGE msg, UInt32 responseVendorID, Byte responseSubTypeID, int timeoutMilliseconds)
        {
            return await TransactAsync(msg, (UInt16)ENUM_LLRP_MSG_TYPE.CUSTOM_MESSAGE, responseVendorID, responseSubTypeID, timeoutMilliseconds).ConfigureAwait(false) as MSG_CUSTOM_MESSAGE;
        }
        /// <summary>
        /// Send customized message to the client
        /// </summary>
        /// <param name="msg">Message to be sent to the client.</param>
        /// <param name="responseVendorID">The vendor of the expected response message</param>
        /// <param name="responseSubTypeID">The sub type of the expected response message</param>
        /// <returns>Custom Message</returns>
        public async Task<MSG_CUSTOM_MESSAGE> CUSTOM_MESSAGE(MSG_CUSTOM_MESSAGE msg, UInt32 responseVendorID, Byte responseSubTypeID)
        {
            return await TransactAsync(msg, (UInt16)ENUM_LLRP_MSG_TYPE.CUSTOM_MESSAGE, responseVendorID, responseSubTypeID, ResponseTimeout).ConfigureAwait(false) as MSG_CUSTOM_MESSAGE;
        }
        /// <summary>
        /// Send customized message to the client
        /// </summary>
        /// <param name="msg">Message to be sent to client.</param>
        public async Task CUSTOM_MESSAGE(MSG_CUSTOM_MESSAGE msg)
        {
            await SendAsync(msg).ConfigureAwait(false);
        }
      
        /// <summary>
        /// GET_READER_CAPABILITIES message response.
        /// </summary>
        /// <param name="msg">MSG_GET_READER_CAPABILITIES to send to reader.</param>
        /// <returns>If the function is called successfully, return MSG_GET_READER_CAPABILITIES_RESPONSE. Otherwise, null is returned.</returns>
        public async Task GET_READER_CAPABILITIES_RESPONSE(MSG_GET_READER_CAPABILITIES_RESPONSE msg)
        {
            await SendAsync(msg).ConfigureAwait(false);
        }
      
        /// <summary>
        /// ADD_ROSPEC message response.
        /// </summary>
        /// <param name="msg">MSG_ADD_ROSPEC to send to reader.</param>
        /// <returns>If the function is called successfully, return MSG_ADD_ROSPEC_RESPONSE. Otherwise, null is returned.</returns>
        public async Task ADD_ROSPEC_RESPONSE(MSG_ADD_ROSPEC_RESPONSE msg)
        {
            await SendAsync(msg).ConfigureAwait(false);
        }
      
        /// <summary>
        /// DELETE_ROSPEC message response.
        /// </summary>
        /// <param name="msg">MSG_DELETE_ROSPEC to send to reader.</param>
        /// <returns>If the function is called successfully, return MSG_DELETE_ROSPEC_RESPONSE. Otherwise, null is returned.</returns>
        public async Task DELETE_ROSPEC_RESPONSE(MSG_DELETE_ROSPEC_RESPONSE msg)
        {
            await SendAsync(msg).ConfigureAwait(false);
        }
      
        /// <summary>
        /// START_ROSPEC message response.
        /// </summary>
        /// <param name="msg">MSG_START_ROSPEC to send to reader.</param>
        /// <returns>If the function is called successfully, return MSG_START_ROSPEC_RESPONSE. Otherwise, null is returned.</returns>
        public async Task START_ROSPEC_RESPONSE(MSG_START_ROSPEC_RESPONSE msg)
        {
            await SendAsync(msg).ConfigureAwait(false);
        }
      
        /// <summary>
        /// STOP_ROSPEC message response.
        /// </summary>
        /// <param name="msg">MSG_STOP_ROSPEC to send to reader.</param>
        /// <returns>If the function is called successfully, return MSG_STOP_ROSPEC_RESPONSE. Otherwise, null is returned.</returns>
        public async Task STOP_ROSPEC_RESPONSE(MSG_STOP_ROSPEC_RESPONSE msg)
        {
            await SendAsync(msg).ConfigureAwait(false);
        }
      
        /// <summary>
        /// ENABLE_ROSPEC message response.
        /// </summary>
        /// <param name="msg">MSG_ENABLE_ROSPEC to send to reader.</param>
        /// <returns>If the function is called successfully, return MSG_ENABLE_ROSPEC_RESPONSE. Otherwise, null is returned.</returns>
        public async Task ENABLE_ROSPEC_RESPONSE(MSG_ENABLE_ROSPEC_RESPONSE msg)
        {
            await SendAsync(msg).ConfigureAwait(false);
        }
      
        /// <summary>
        /// DISABLE_ROSPEC message response.
        /// </summary>
        /// <param name="msg">MSG_DISABLE_ROSPEC to send to reader.</param>
        /// <returns>If the function is called successfully, return MSG_DISABLE_ROSPEC_RESPONSE. Otherwise, null is returned.</returns>
        public async Task DISABLE_ROSPEC_RESPONSE(MSG_DISABLE_ROSPEC_RESPONSE msg)
        {
            await SendAsync(msg).ConfigureAwait(false);
        }
      
        /// <summary>
        /// GET_ROSPECS message response.
        /// </summary>
        /// <param name="msg">MSG_GET_ROSPECS to send to reader.</param>
        /// <returns>If the function is called successfully, return MSG_GET_ROSPECS_RESPONSE. Otherwise, null is returned.</returns>
        public async Task GET_ROSPECS_RESPONSE(MSG_GET_ROSPECS_RESPONSE msg)
        {
            await SendAsync(msg).ConfigureAwait(false);
        }
      
        /// <summary>
        /// ADD_ACCESSSPEC message response.
        /// </summary>
        /// <param name="msg">MSG_ADD_ACCESSSPEC to send to reader.</param>
        /// <returns>If the function is called successfully, return MSG_ADD_ACCESSSPEC_RESPONSE. Otherwise, null is returned.</returns>
        public async Task ADD_ACCESSSPEC_RESPONSE(MSG_ADD_ACCESSSPEC_RESPONSE msg)
        {
            await SendAsync(msg).ConfigureAwait(false);
        }
      
        /// <summary>
        /// DELETE_ACCESSSPEC message response.
        /// </summary>
        /// <param name="msg">MSG_DELETE_ACCESSSPEC to send to reader.</param>
        /// <returns>If the function is called successfully, return MSG_DELETE_ACCESSSPEC_RESPONSE. Otherwise, null is returned.</returns>
        public async Task DELETE_ACCESSSPEC_RESPONSE(MSG_DELETE_ACCESSSPEC_RESPONSE msg)
        {
            await SendAsync(msg).ConfigureAwait(false);
        }
      
        /// <summary>
        /// ENABLE_ACCESSSPEC message response.
        /// </summary>
        /// <param name="msg">MSG_ENABLE_ACCESSSPEC to send to reader.</param>
        /// <returns>If the function is called successfully, return MSG_ENABLE_ACCESSSPEC_RESPONSE. Otherwise, null is returned.</returns>
        public async Task ENABLE_ACCESSSPEC_RESPONSE(MSG_ENABLE_ACCESSSPEC_RESPONSE msg)
        {
            await SendAsync(msg).ConfigureAwait(false);
        }
      
        /// <summary>
        /// DISABLE_ACCESSSPEC message response.
        /// </summary>
        /// <param name="msg">MSG_DISABLE_ACCESSSPEC to send to reader.</param>
        /// <returns>If the function is called successfully, return MSG_DISABLE_ACCESSSPEC_RESPONSE. Otherwise, null is returned.</returns>
        public async Task DISABLE_ACCESSSPEC_RESPONSE(MSG_DISABLE_ACCESSSPEC_RESPONSE msg)
        {
            await SendAsync(msg).ConfigureAwait(false);
        }
      
        /// <summary>
        /// GET_ACCESSSPECS message response.
        /// </summary>
        /// <param name="msg">MSG_GET_ACCESSSPECS to send to reader.</param>
        /// <returns>If the function is called successfully, return MSG_GET_ACCESSSPECS_RESPONSE. Otherwise, null is returned.</returns>
        public async Task GET_ACCESSSPECS_RESPONSE(MSG_GET_ACCESSSPECS_RESPONSE msg)
        {
            await SendAsync(msg).ConfigureAwait(false);
        }
      
        /// <summary>
        /// CLIENT_REQUEST_OP message call.
        /// </summary>
        /// <param name="msg">MSG_CLIENT_REQUEST_OP to send to reader.</param>
        /// <param name="timeoutMilliseconds">Fuction call time out in milliseconds.</param>
        /// <returns>MSG_CLIENT_REQUEST_OP_RESPONSE</returns>
        public async Task<MSG_CLIENT_REQUEST_OP_RESPONSE> CLIENT_REQUEST_OP(MSG_CLIENT_REQUEST_OP msg, int timeoutMilliseconds)
        {
            return await TransactAsync(msg, (UInt16)ENUM_LLRP_MSG_TYPE.CLIENT_REQUEST_OP_RESPONSE, 0, 0, timeoutMilliseconds).ConfigureAwait(false) as MSG_CLIENT_REQUEST_OP_RESPONSE;
        }
        /// <summary>
        /// CLIENT_REQUEST_OP message call.
        /// </summary>
        /// <param name="msg">MSG_CLIENT_REQUEST_OP to send to reader.</param>
        /// <returns>MSG_CLIENT_REQUEST_OP_RESPONSE</returns>
        public async Task<MSG_CLIENT_REQUEST_OP_RESPONSE> CLIENT_REQUEST_OP(MSG_CLIENT_REQUEST_OP msg)
        {
            return await TransactAsync(msg, (UInt16)ENUM_LLRP_MSG_TYPE.CLIENT_REQUEST_OP_RESPONSE, 0, 0, ResponseTimeout).ConfigureAwait(false) as MSG_CLIENT_REQUEST_OP_RESPONSE;
        }
      
        /// <summary>
        /// GET_READER_CONFIG message response.
        /// </summary>
        /// <param name="msg">MSG_GET_READER_CONFIG to send to reader.</param>
        /// <returns>If the function is called successfully, return MSG_GET_READER_CONFIG_RESPONSE. Otherwise, null is returned.</returns>
        public async Task GET_READER_CONFIG_RESPONSE(MSG_GET_READER_CONFIG_RESPONSE msg)
        {
            await SendAsync(msg).ConfigureAwait(false);
        }
      
        /// <summary>
        /// SET_READER_CONFIG message response.
        /// </summary>
        /// <param name="msg">MSG_SET_READER_CONFIG to send to reader.</param>
        /// <returns>If the function is called successfully, return MSG_SET_READER_CONFIG_RESPONSE. Otherwise, null is returned.</returns>
        public async Task SET_READER_CONFIG_RESPONSE(MSG_SET_READER_CONFIG_RESPONSE msg)
        {
            await SendAsync(msg).ConfigureAwait(false);
        }
      
        /// <summary>
        /// CLOSE_CONNECTION message response.
        /// </summary>
        /// <param name="msg">MSG_CLOSE_CONNECTION to send to reader.</param>
        /// <returns>If the function is called successfully, return MSG_CLOSE_CONNECTION_RESPONSE. Otherwise, null is returned.</returns>
        public async Task CLOSE_CONNECTION_RESPONSE(MSG_CLOSE_CONNECTION_RESPONSE msg)
        {
            await SendAsync(msg).ConfigureAwait(false);
        }
      
        /// <summary>
        /// Keep alive message
        /// </summary>
        /// <param name="msg">Message to be sent to client.</param>
        public async Task KEEPALIVE(MSG_KEEPALIVE msg)
        {
            await SendAsync(msg).ConfigureAwait(false);
        }
      

        /// <summary>
        /// Enable events and reports
        /// </summary>
        /// <param name="msg">Message to be sent to reader.</param>
        public async Task READER_EVENT_NOTIFICATION(MSG_READER_EVENT_NOTIFICATION msg)
        {
            await SendAsync(msg).ConfigureAwait(false);
        }
      

        /// <summary>
        /// Enable events and reports
        /// </summary>
        /// <param name="msg">Message to be sent to reader.</param>
        public async Task ERROR_MESSAGE(MSG_ERROR_MESSAGE msg)
        {
            await SendAsync(msg).ConfigureAwait(false);
        }
      
    }
}
  