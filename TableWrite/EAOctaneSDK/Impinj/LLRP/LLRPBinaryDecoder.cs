
/*
***************************************************************************
*  Copyright 2008 Impinj, Inc.
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*
***************************************************************************
*/


/*
***************************************************************************
*
*  This code is generated by Impinj LLRP .Net generator. Modification is not
*  recommended.
*
***************************************************************************
*/

/*
***************************************************************************
* File Name:       LLRPBinaryDecoder.cs
*
* Version:         1.0
* Author:          Impinj
* Organization:    Impinj
* Date:            Jan. 18, 2008
*
* Description:     This file contains general XML Decoder for LLRP messages
***************************************************************************
*/

using System;
using System.Collections.Generic;

namespace EA.Impinj.LLRP
{
    public interface ICustomParameterDecoder
    {
        ILLRPCustomParameter DecodeCustomParameter(BitList bitList, ref int cursor, int length);
    }

    /// <summary>
    /// LLRP Binary Decoder
    /// </summary>
    public class LLRPBinaryDecoder : ICustomParameterDecoder
    {
        /// <summary>
        /// Decode binary LLRP packet to LLRP message object
        /// </summary>
        /// <param name="messageBytes">Binary LLRP packet bytes to be decoded</param>
        /// <returns>LLRP message</returns>
        public ILLRPMessage Decode(IList<byte> messageBytes)
        {
            BitList bitList = LLRPUtil.ByteListToBitList(messageBytes);

            int headerCursor = 6;
            UInt16 msgTypeId = LLRPUtil.BitListToUInt16(bitList, ref headerCursor, 10);
            UInt32 msgLength = LLRPUtil.BitListToUInt32(bitList, ref headerCursor, 32);

            int cursor = 0;
            int length = checked((int)(msgLength * 8));

            switch ((ENUM_LLRP_MSG_TYPE)msgTypeId)
            {
            
                case ENUM_LLRP_MSG_TYPE.CUSTOM_MESSAGE:
              
                    return DecodeCustomMessage(bitList, ref cursor, length);
                
                case ENUM_LLRP_MSG_TYPE.GET_READER_CAPABILITIES:
              
                    return MSG_GET_READER_CAPABILITIES.FromBitList(this, bitList, ref cursor, length);
                
                case ENUM_LLRP_MSG_TYPE.GET_READER_CAPABILITIES_RESPONSE:
              
                    return MSG_GET_READER_CAPABILITIES_RESPONSE.FromBitList(this, bitList, ref cursor, length);
                
                case ENUM_LLRP_MSG_TYPE.ADD_ROSPEC:
              
                    return MSG_ADD_ROSPEC.FromBitList(this, bitList, ref cursor, length);
                
                case ENUM_LLRP_MSG_TYPE.ADD_ROSPEC_RESPONSE:
              
                    return MSG_ADD_ROSPEC_RESPONSE.FromBitList(this, bitList, ref cursor, length);
                
                case ENUM_LLRP_MSG_TYPE.DELETE_ROSPEC:
              
                    return MSG_DELETE_ROSPEC.FromBitList(this, bitList, ref cursor, length);
                
                case ENUM_LLRP_MSG_TYPE.DELETE_ROSPEC_RESPONSE:
              
                    return MSG_DELETE_ROSPEC_RESPONSE.FromBitList(this, bitList, ref cursor, length);
                
                case ENUM_LLRP_MSG_TYPE.START_ROSPEC:
              
                    return MSG_START_ROSPEC.FromBitList(this, bitList, ref cursor, length);
                
                case ENUM_LLRP_MSG_TYPE.START_ROSPEC_RESPONSE:
              
                    return MSG_START_ROSPEC_RESPONSE.FromBitList(this, bitList, ref cursor, length);
                
                case ENUM_LLRP_MSG_TYPE.STOP_ROSPEC:
              
                    return MSG_STOP_ROSPEC.FromBitList(this, bitList, ref cursor, length);
                
                case ENUM_LLRP_MSG_TYPE.STOP_ROSPEC_RESPONSE:
              
                    return MSG_STOP_ROSPEC_RESPONSE.FromBitList(this, bitList, ref cursor, length);
                
                case ENUM_LLRP_MSG_TYPE.ENABLE_ROSPEC:
              
                    return MSG_ENABLE_ROSPEC.FromBitList(this, bitList, ref cursor, length);
                
                case ENUM_LLRP_MSG_TYPE.ENABLE_ROSPEC_RESPONSE:
              
                    return MSG_ENABLE_ROSPEC_RESPONSE.FromBitList(this, bitList, ref cursor, length);
                
                case ENUM_LLRP_MSG_TYPE.DISABLE_ROSPEC:
              
                    return MSG_DISABLE_ROSPEC.FromBitList(this, bitList, ref cursor, length);
                
                case ENUM_LLRP_MSG_TYPE.DISABLE_ROSPEC_RESPONSE:
              
                    return MSG_DISABLE_ROSPEC_RESPONSE.FromBitList(this, bitList, ref cursor, length);
                
                case ENUM_LLRP_MSG_TYPE.GET_ROSPECS:
              
                    return MSG_GET_ROSPECS.FromBitList(this, bitList, ref cursor, length);
                
                case ENUM_LLRP_MSG_TYPE.GET_ROSPECS_RESPONSE:
              
                    return MSG_GET_ROSPECS_RESPONSE.FromBitList(this, bitList, ref cursor, length);
                
                case ENUM_LLRP_MSG_TYPE.ADD_ACCESSSPEC:
              
                    return MSG_ADD_ACCESSSPEC.FromBitList(this, bitList, ref cursor, length);
                
                case ENUM_LLRP_MSG_TYPE.ADD_ACCESSSPEC_RESPONSE:
              
                    return MSG_ADD_ACCESSSPEC_RESPONSE.FromBitList(this, bitList, ref cursor, length);
                
                case ENUM_LLRP_MSG_TYPE.DELETE_ACCESSSPEC:
              
                    return MSG_DELETE_ACCESSSPEC.FromBitList(this, bitList, ref cursor, length);
                
                case ENUM_LLRP_MSG_TYPE.DELETE_ACCESSSPEC_RESPONSE:
              
                    return MSG_DELETE_ACCESSSPEC_RESPONSE.FromBitList(this, bitList, ref cursor, length);
                
                case ENUM_LLRP_MSG_TYPE.ENABLE_ACCESSSPEC:
              
                    return MSG_ENABLE_ACCESSSPEC.FromBitList(this, bitList, ref cursor, length);
                
                case ENUM_LLRP_MSG_TYPE.ENABLE_ACCESSSPEC_RESPONSE:
              
                    return MSG_ENABLE_ACCESSSPEC_RESPONSE.FromBitList(this, bitList, ref cursor, length);
                
                case ENUM_LLRP_MSG_TYPE.DISABLE_ACCESSSPEC:
              
                    return MSG_DISABLE_ACCESSSPEC.FromBitList(this, bitList, ref cursor, length);
                
                case ENUM_LLRP_MSG_TYPE.DISABLE_ACCESSSPEC_RESPONSE:
              
                    return MSG_DISABLE_ACCESSSPEC_RESPONSE.FromBitList(this, bitList, ref cursor, length);
                
                case ENUM_LLRP_MSG_TYPE.GET_ACCESSSPECS:
              
                    return MSG_GET_ACCESSSPECS.FromBitList(this, bitList, ref cursor, length);
                
                case ENUM_LLRP_MSG_TYPE.GET_ACCESSSPECS_RESPONSE:
              
                    return MSG_GET_ACCESSSPECS_RESPONSE.FromBitList(this, bitList, ref cursor, length);
                
                case ENUM_LLRP_MSG_TYPE.CLIENT_REQUEST_OP:
              
                    return MSG_CLIENT_REQUEST_OP.FromBitList(this, bitList, ref cursor, length);
                
                case ENUM_LLRP_MSG_TYPE.CLIENT_REQUEST_OP_RESPONSE:
              
                    return MSG_CLIENT_REQUEST_OP_RESPONSE.FromBitList(this, bitList, ref cursor, length);
                
                case ENUM_LLRP_MSG_TYPE.GET_READER_CONFIG:
              
                    return MSG_GET_READER_CONFIG.FromBitList(this, bitList, ref cursor, length);
                
                case ENUM_LLRP_MSG_TYPE.GET_READER_CONFIG_RESPONSE:
              
                    return MSG_GET_READER_CONFIG_RESPONSE.FromBitList(this, bitList, ref cursor, length);
                
                case ENUM_LLRP_MSG_TYPE.SET_READER_CONFIG:
              
                    return MSG_SET_READER_CONFIG.FromBitList(this, bitList, ref cursor, length);
                
                case ENUM_LLRP_MSG_TYPE.SET_READER_CONFIG_RESPONSE:
              
                    return MSG_SET_READER_CONFIG_RESPONSE.FromBitList(this, bitList, ref cursor, length);
                
                case ENUM_LLRP_MSG_TYPE.CLOSE_CONNECTION:
              
                    return MSG_CLOSE_CONNECTION.FromBitList(this, bitList, ref cursor, length);
                
                case ENUM_LLRP_MSG_TYPE.CLOSE_CONNECTION_RESPONSE:
              
                    return MSG_CLOSE_CONNECTION_RESPONSE.FromBitList(this, bitList, ref cursor, length);
                
                case ENUM_LLRP_MSG_TYPE.GET_REPORT:
              
                    return MSG_GET_REPORT.FromBitList(this, bitList, ref cursor, length);
                
                case ENUM_LLRP_MSG_TYPE.RO_ACCESS_REPORT:
              
                    return MSG_RO_ACCESS_REPORT.FromBitList(this, bitList, ref cursor, length);
                
                case ENUM_LLRP_MSG_TYPE.KEEPALIVE:
              
                    return MSG_KEEPALIVE.FromBitList(this, bitList, ref cursor, length);
                
                case ENUM_LLRP_MSG_TYPE.KEEPALIVE_ACK:
              
                    return MSG_KEEPALIVE_ACK.FromBitList(this, bitList, ref cursor, length);
                
                case ENUM_LLRP_MSG_TYPE.READER_EVENT_NOTIFICATION:
              
                    return MSG_READER_EVENT_NOTIFICATION.FromBitList(this, bitList, ref cursor, length);
                
                case ENUM_LLRP_MSG_TYPE.ENABLE_EVENTS_AND_REPORTS:
              
                    return MSG_ENABLE_EVENTS_AND_REPORTS.FromBitList(this, bitList, ref cursor, length);
                
                case ENUM_LLRP_MSG_TYPE.ERROR_MESSAGE:
              
                    return MSG_ERROR_MESSAGE.FromBitList(this, bitList, ref cursor, length);
                
                default:
                    throw new LLRPMalformedPacketException();
            }
        }

        protected virtual ILLRPCustomMessage DecodeCustomMessage(BitList bitList, ref int cursor, int length)
        {
            return MSG_CUSTOM_MESSAGE.FromBitList(this, bitList, ref cursor, length);
        }

        
        /// <summary>
        /// Decode custom parameter from existing bit list.
        /// </summary>
        /// <param name="bitList">Existing bit list to be decoded</param>
        /// <param name="cursor">Current cursor in existing bit list</param>
        /// <param name="length">Total bits to be decoded</param>
        /// <returns>PARAM_</returns>
        public virtual ILLRPCustomParameter DecodeCustomParameter(BitList bitList, ref int cursor, int length)
        {
            return PARAM_Custom.FromBitList(this, bitList, ref cursor, length);
        }
    }
}


  