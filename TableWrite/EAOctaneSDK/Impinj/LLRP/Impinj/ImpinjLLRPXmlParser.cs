
using System.Xml.Linq;

namespace EA.Impinj.LLRP.Impinj
{
    /// <summary>
    /// LLRP XML Parser
    /// </summary>
    public class ImpinjLLRPXmlParser : LLRPXmlParser
    {

        /// <summary>
        /// Parse XML string to LLRP message
        /// </summary>
        /// <param name="xmlstr">XML string to be parsed.</param>
        /// <param name="msg">LLRP message. output</param>
        /// <param name="type">LLRP message type. output</param>
        protected override ILLRPCustomMessage ParseCustomMessage(XElement root)
        {
            XNamespace xmlns = XNamespace.Get(ImpinjLLRPConstants.NAMESPACE_URI);

            if (root.Name.Namespace == xmlns)
            {
                switch (root.Name.LocalName)
                {
                    
                    case "IMPINJ_ENABLE_EXTENSIONS":
                        return MSG_IMPINJ_ENABLE_EXTENSIONS.FromXml(this, root);
                    
                    case "IMPINJ_ENABLE_EXTENSIONS_RESPONSE":
                        return MSG_IMPINJ_ENABLE_EXTENSIONS_RESPONSE.FromXml(this, root);
                    
                    case "IMPINJ_SAVE_SETTINGS":
                        return MSG_IMPINJ_SAVE_SETTINGS.FromXml(this, root);
                    
                    case "IMPINJ_SAVE_SETTINGS_RESPONSE":
                        return MSG_IMPINJ_SAVE_SETTINGS_RESPONSE.FromXml(this, root);
                    
                }
            }


            return base.ParseCustomMessage(root);
        }

        /// <summary>
        /// Parse XML string to LLRP parameter
        /// </summary>
        /// <param name="xmlstr">XML string to be parsed.</param>
        /// <param name="msg">LLRP message. output</param>
        /// <param name="type">LLRP message type. output</param>
        public override ILLRPCustomParameter ParseCustomParameter(XElement root)
        {
            XNamespace xmlns = XNamespace.Get(ImpinjLLRPConstants.NAMESPACE_URI);

            if (root.Name.Namespace == xmlns)
            {
                switch (root.Name.LocalName)
                {
                    
                    case "ImpinjRequestedData":
                        return PARAM_ImpinjRequestedData.FromXml(this, root);
                    
                    case "ImpinjSubRegulatoryRegion":
                        return PARAM_ImpinjSubRegulatoryRegion.FromXml(this, root);
                    
                    case "ImpinjInventorySearchMode":
                        return PARAM_ImpinjInventorySearchMode.FromXml(this, root);
                    
                    case "ImpinjFixedFrequencyList":
                        return PARAM_ImpinjFixedFrequencyList.FromXml(this, root);
                    
                    case "ImpinjReducedPowerFrequencyList":
                        return PARAM_ImpinjReducedPowerFrequencyList.FromXml(this, root);
                    
                    case "ImpinjLowDutyCycle":
                        return PARAM_ImpinjLowDutyCycle.FromXml(this, root);
                    
                    case "ImpinjHubVersions":
                        return PARAM_ImpinjHubVersions.FromXml(this, root);
                    
                    case "ImpinjDetailedVersion":
                        return PARAM_ImpinjDetailedVersion.FromXml(this, root);
                    
                    case "ImpinjFrequencyCapabilities":
                        return PARAM_ImpinjFrequencyCapabilities.FromXml(this, root);
                    
                    case "ImpinjGPIDebounceConfiguration":
                        return PARAM_ImpinjGPIDebounceConfiguration.FromXml(this, root);
                    
                    case "ImpinjReaderTemperature":
                        return PARAM_ImpinjReaderTemperature.FromXml(this, root);
                    
                    case "ImpinjLinkMonitorConfiguration":
                        return PARAM_ImpinjLinkMonitorConfiguration.FromXml(this, root);
                    
                    case "ImpinjReportBufferConfiguration":
                        return PARAM_ImpinjReportBufferConfiguration.FromXml(this, root);
                    
                    case "ImpinjAccessSpecConfiguration":
                        return PARAM_ImpinjAccessSpecConfiguration.FromXml(this, root);
                    
                    case "ImpinjBlockWriteWordCount":
                        return PARAM_ImpinjBlockWriteWordCount.FromXml(this, root);
                    
                    case "ImpinjBlockPermalock":
                        return PARAM_ImpinjBlockPermalock.FromXml(this, root);
                    
                    case "ImpinjBlockPermalockOpSpecResult":
                        return PARAM_ImpinjBlockPermalockOpSpecResult.FromXml(this, root);
                    
                    case "ImpinjGetBlockPermalockStatus":
                        return PARAM_ImpinjGetBlockPermalockStatus.FromXml(this, root);
                    
                    case "ImpinjGetBlockPermalockStatusOpSpecResult":
                        return PARAM_ImpinjGetBlockPermalockStatusOpSpecResult.FromXml(this, root);
                    
                    case "ImpinjSetQTConfig":
                        return PARAM_ImpinjSetQTConfig.FromXml(this, root);
                    
                    case "ImpinjSetQTConfigOpSpecResult":
                        return PARAM_ImpinjSetQTConfigOpSpecResult.FromXml(this, root);
                    
                    case "ImpinjGetQTConfig":
                        return PARAM_ImpinjGetQTConfig.FromXml(this, root);
                    
                    case "ImpinjGetQTConfigOpSpecResult":
                        return PARAM_ImpinjGetQTConfigOpSpecResult.FromXml(this, root);
                    
                    case "ImpinjTagReportContentSelector":
                        return PARAM_ImpinjTagReportContentSelector.FromXml(this, root);
                    
                    case "ImpinjEnableSerializedTID":
                        return PARAM_ImpinjEnableSerializedTID.FromXml(this, root);
                    
                    case "ImpinjEnableRFPhaseAngle":
                        return PARAM_ImpinjEnableRFPhaseAngle.FromXml(this, root);
                    
                    case "ImpinjEnablePeakRSSI":
                        return PARAM_ImpinjEnablePeakRSSI.FromXml(this, root);
                    
                    case "ImpinjEnableGPSCoordinates":
                        return PARAM_ImpinjEnableGPSCoordinates.FromXml(this, root);
                    
                    case "ImpinjSerializedTID":
                        return PARAM_ImpinjSerializedTID.FromXml(this, root);
                    
                    case "ImpinjRFPhaseAngle":
                        return PARAM_ImpinjRFPhaseAngle.FromXml(this, root);
                    
                    case "ImpinjPeakRSSI":
                        return PARAM_ImpinjPeakRSSI.FromXml(this, root);
                    
                    case "ImpinjGPSCoordinates":
                        return PARAM_ImpinjGPSCoordinates.FromXml(this, root);
                    
                    case "ImpinjLoopSpec":
                        return PARAM_ImpinjLoopSpec.FromXml(this, root);
                    
                    case "ImpinjGPSNMEASentences":
                        return PARAM_ImpinjGPSNMEASentences.FromXml(this, root);
                    
                    case "ImpinjGGASentence":
                        return PARAM_ImpinjGGASentence.FromXml(this, root);
                    
                    case "ImpinjRMCSentence":
                        return PARAM_ImpinjRMCSentence.FromXml(this, root);
                    
                    case "ImpinjOpSpecRetryCount":
                        return PARAM_ImpinjOpSpecRetryCount.FromXml(this, root);
                    
                    case "ImpinjAdvancedGPOConfiguration":
                        return PARAM_ImpinjAdvancedGPOConfiguration.FromXml(this, root);
                    
                    case "ImpinjEnableOptimizedRead":
                        return PARAM_ImpinjEnableOptimizedRead.FromXml(this, root);
                    
                    case "ImpinjAccessSpecOrdering":
                        return PARAM_ImpinjAccessSpecOrdering.FromXml(this, root);
                    
                    case "ImpinjEnableRFDopplerFrequency":
                        return PARAM_ImpinjEnableRFDopplerFrequency.FromXml(this, root);
                    
                    case "ImpinjRFDopplerFrequency":
                        return PARAM_ImpinjRFDopplerFrequency.FromXml(this, root);
                    
                    case "ImpinjInventoryConfiguration":
                        return PARAM_ImpinjInventoryConfiguration.FromXml(this, root);
                    
                    case "ImpinjEnableTxPower":
                        return PARAM_ImpinjEnableTxPower.FromXml(this, root);
                    
                    case "ImpinjTxPower":
                        return PARAM_ImpinjTxPower.FromXml(this, root);
                    
                    case "ImpinjArrayVersion":
                        return PARAM_ImpinjArrayVersion.FromXml(this, root);
                    
                    case "ImpinjxArrayCapabilities":
                        return PARAM_ImpinjxArrayCapabilities.FromXml(this, root);
                    
                    case "ImpinjTiltConfiguration":
                        return PARAM_ImpinjTiltConfiguration.FromXml(this, root);
                    
                    case "ImpinjBeaconConfiguration":
                        return PARAM_ImpinjBeaconConfiguration.FromXml(this, root);
                    
                    case "ImpinjAntennaConfiguration":
                        return PARAM_ImpinjAntennaConfiguration.FromXml(this, root);
                    
                    case "ImpinjAntennaEventHysteresis":
                        return PARAM_ImpinjAntennaEventHysteresis.FromXml(this, root);
                    
                    case "ImpinjAntennaEventConfiguration":
                        return PARAM_ImpinjAntennaEventConfiguration.FromXml(this, root);
                    
                    case "ImpinjAntennaAttemptEvent":
                        return PARAM_ImpinjAntennaAttemptEvent.FromXml(this, root);
                    
                    case "ImpinjHubConfiguration":
                        return PARAM_ImpinjHubConfiguration.FromXml(this, root);
                    
                    case "ImpinjDiagnosticReport":
                        return PARAM_ImpinjDiagnosticReport.FromXml(this, root);
                    
                    case "ImpinjPlacementConfiguration":
                        return PARAM_ImpinjPlacementConfiguration.FromXml(this, root);
                    
                    case "ImpinjLISpec":
                        return PARAM_ImpinjLISpec.FromXml(this, root);
                    
                    case "ImpinjLocationConfig":
                        return PARAM_ImpinjLocationConfig.FromXml(this, root);
                    
                    case "ImpinjC1G2LocationConfig":
                        return PARAM_ImpinjC1G2LocationConfig.FromXml(this, root);
                    
                    case "ImpinjLocationReporting":
                        return PARAM_ImpinjLocationReporting.FromXml(this, root);
                    
                    case "ImpinjLocationConfidence":
                        return PARAM_ImpinjLocationConfidence.FromXml(this, root);
                    
                    case "ImpinjLocationReportData":
                        return PARAM_ImpinjLocationReportData.FromXml(this, root);
                    
                    case "ImpinjDISpec":
                        return PARAM_ImpinjDISpec.FromXml(this, root);
                    
                    case "ImpinjDirectionSectors":
                        return PARAM_ImpinjDirectionSectors.FromXml(this, root);
                    
                    case "ImpinjDirectionConfig":
                        return PARAM_ImpinjDirectionConfig.FromXml(this, root);
                    
                    case "ImpinjDirectionUserTagPopulationLimit":
                        return PARAM_ImpinjDirectionUserTagPopulationLimit.FromXml(this, root);
                    
                    case "ImpinjC1G2DirectionConfig":
                        return PARAM_ImpinjC1G2DirectionConfig.FromXml(this, root);
                    
                    case "ImpinjExtendedTagInformation":
                        return PARAM_ImpinjExtendedTagInformation.FromXml(this, root);
                    
                    case "ImpinjDirectionReporting":
                        return PARAM_ImpinjDirectionReporting.FromXml(this, root);
                    
                    case "ImpinjDirectionReportData":
                        return PARAM_ImpinjDirectionReportData.FromXml(this, root);
                    
                    case "ImpinjDirectionDiagnosticData":
                        return PARAM_ImpinjDirectionDiagnosticData.FromXml(this, root);
                    
                    case "ImpinjxArrayDirectionCapabilities":
                        return PARAM_ImpinjxArrayDirectionCapabilities.FromXml(this, root);
                    
                    case "ImpinjIntelligentAntennaManagement":
                        return PARAM_ImpinjIntelligentAntennaManagement.FromXml(this, root);
                    
                    case "ImpinjTransmitPower":
                        return PARAM_ImpinjTransmitPower.FromXml(this, root);
                    
                    case "ImpinjPolarizationControl":
                        return PARAM_ImpinjPolarizationControl.FromXml(this, root);
                    
                    case "ImpinjAntennaCapabilities":
                        return PARAM_ImpinjAntennaCapabilities.FromXml(this, root);
                    
                    case "ImpinjAntennaPolarizationCapability":
                        return PARAM_ImpinjAntennaPolarizationCapability.FromXml(this, root);
                    
                    case "ImpinjDisabledAntennas":
                        return PARAM_ImpinjDisabledAntennas.FromXml(this, root);
                    
                    case "ImpinjTIDParity":
                        return PARAM_ImpinjTIDParity.FromXml(this, root);
                    
                    case "ImpinjMarginRead":
                        return PARAM_ImpinjMarginRead.FromXml(this, root);
                    
                    case "ImpinjMarginReadOpSpecResult":
                        return PARAM_ImpinjMarginReadOpSpecResult.FromXml(this, root);
                    
                    case "ImpinjBLEVersion":
                        return PARAM_ImpinjBLEVersion.FromXml(this, root);
                    
                    case "ImpinjLocationAlgorithmControl":
                        return PARAM_ImpinjLocationAlgorithmControl.FromXml(this, root);
                    
                    case "ImpinjRFPowerSweep":
                        return PARAM_ImpinjRFPowerSweep.FromXml(this, root);
                    
                    case "ImpinjTruncatedReplyConfiguration":
                        return PARAM_ImpinjTruncatedReplyConfiguration.FromXml(this, root);
                    
                }
            }

            return base.ParseCustomParameter(root);
        }
    }
}
  