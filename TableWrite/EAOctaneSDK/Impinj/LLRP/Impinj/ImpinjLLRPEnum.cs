

namespace EA.Impinj.LLRP.Impinj
{
    //LLRP custom enumeration definitions

    
    ///<summary>
    ///
    ///
    ///For more information, please refer to:
    ///
    ///</summary>
    public enum ENUM_ImpinjRequestedDataType
    {
        All_Capabilities = 1000,
        Impinj_Detailed_Version = 1001,
        Impinj_Frequency_Capabilities = 1002,
        Impinj_xArray_Capabilities = 1003,
        Impinj_Antenna_Capabilities = 1004,
        All_Configuration = 2000,
        Impinj_Sub_Regulatory_Region = 2001,
        Impinj_GPI_Debounce_Configuration = 2003,
        Impinj_Reader_Temperature = 2004,
        Impinj_Link_Monitor_Configuration = 2005,
        Impinj_Report_Buffer_Configuration = 2006,
        Impinj_Access_Spec_Configuration = 2007,
        Impinj_GPS_NMEA_Sentences = 2008,
        Impinj_Advanced_GPO_Configuration = 2009,
        Impinj_Tilt_Configuration = 2010,
        Impinj_Beacon_Configuration = 2011,
        Impinj_Antenna_Configuration = 2012,
        Impinj_Location_Configuration = 2013,
        Impinj_Transition_Configuration = 2014,
        Impinj_Hub_Configuration = 2015,
        Impinj_PolarizationControl_Configuration = 2017,
        Impinj_Direction_Configuration = 2018,
        
    }
    
    ///<summary>
    ///
    ///
    ///For more information, please refer to:
    ///
    ///</summary>
    public enum ENUM_ImpinjRegulatoryRegion
    {
        FCC_Part_15_247 = 0,
        ETSI_EN_300_220 = 1,
        ETSI_EN_302_208_With_LBT = 2,
        Hong_Kong_920_925_MHz = 3,
        Taiwan_922_928_MHz = 4,
        ETSI_EN_302_208_v1_2_1 = 7,
        Korea_917_921_MHz = 8,
        Malaysia_919_923_MHz = 9,
        China_920_925_MHz = 10,
        South_Africa_915_919_MHz = 12,
        Brazil_902_907_and_915_928_MHz = 13,
        Thailand_920_925_MHz = 14,
        Singapore_920_925_MHz = 15,
        Australia_920_926_MHz = 16,
        India_865_867_MHz = 17,
        Uruguay_916_928_MHz = 18,
        Vietnam_918_923_MHz = 19,
        Israel_915_917_MHz = 20,
        Philippines_918_920_MHz = 21,
        Vietnam_920_923_MHz = 22,
        Indonesia_920_923_MHz = 23,
        New_Zealand_921p5_928_MHz = 24,
        Japan_916_921_MHz_Without_LBT = 25,
        Latin_America_902_928_MHz = 26,
        Peru_916_928_MHz = 27,
        Bangladesh_925_927_MHz = 28,
        ETSI_915_921_MHz = 29,
        Morocco_867_868_MHz = 30,
        
    }
    
    ///<summary>
    ///
    ///
    ///For more information, please refer to:
    ///
    ///</summary>
    public enum ENUM_ImpinjInventorySearchType
    {
        Reader_Selected = 0,
        Single_Target = 1,
        Dual_Target = 2,
        Single_Target_With_Suppression = 3,
        No_Target = 4,
        Single_Target_BtoA = 5,
        Dual_Target_with_BtoASelect = 6,
        
    }
    
    ///<summary>
    ///
    ///
    ///For more information, please refer to:
    ///
    ///</summary>
    public enum ENUM_ImpinjFixedFrequencyMode
    {
        Disabled = 0,
        Auto_Select = 1,
        Channel_List = 2,
        
    }
    
    ///<summary>
    ///
    ///
    ///For more information, please refer to:
    ///
    ///</summary>
    public enum ENUM_ImpinjReducedPowerMode
    {
        Disabled = 0,
        Enabled = 1,
        
    }
    
    ///<summary>
    ///
    ///
    ///For more information, please refer to:
    ///
    ///</summary>
    public enum ENUM_ImpinjLowDutyCycleMode
    {
        Disabled = 0,
        Enabled = 1,
        
    }
    
    ///<summary>
    ///
    ///
    ///For more information, please refer to:
    ///
    ///</summary>
    public enum ENUM_ImpinjLinkMonitorMode
    {
        Disabled = 0,
        Enabled = 1,
        
    }
    
    ///<summary>
    ///
    ///
    ///For more information, please refer to:
    ///
    ///</summary>
    public enum ENUM_ImpinjReportBufferMode
    {
        Normal = 0,
        Low_Latency = 1,
        
    }
    
    ///<summary>
    ///
    ///
    ///For more information, please refer to:
    ///
    ///</summary>
    public enum ENUM_ImpinjBlockPermalockResultType
    {
        Success = 0,
        Insufficient_Power = 1,
        Nonspecific_Tag_Error = 2,
        No_Response_From_Tag = 3,
        Nonspecific_Reader_Error = 4,
        Incorrect_Password_Error = 5,
        Tag_Memory_Overrun_Error = 6,
        
    }
    
    ///<summary>
    ///
    ///
    ///For more information, please refer to:
    ///
    ///</summary>
    public enum ENUM_ImpinjGetBlockPermalockStatusResultType
    {
        Success = 0,
        Nonspecific_Tag_Error = 1,
        No_Response_From_Tag = 2,
        Nonspecific_Reader_Error = 3,
        Incorrect_Password_Error = 4,
        Tag_Memory_Overrun_Error = 5,
        
    }
    
    ///<summary>
    ///
    ///
    ///For more information, please refer to:
    ///
    ///</summary>
    public enum ENUM_ImpinjQTDataProfile
    {
        Unknown = 0,
        Private = 1,
        Public = 2,
        
    }
    
    ///<summary>
    ///
    ///
    ///For more information, please refer to:
    ///
    ///</summary>
    public enum ENUM_ImpinjQTAccessRange
    {
        Unknown = 0,
        Normal_Range = 1,
        Short_Range = 2,
        
    }
    
    ///<summary>
    ///
    ///
    ///For more information, please refer to:
    ///
    ///</summary>
    public enum ENUM_ImpinjQTPersistence
    {
        Unknown = 0,
        Temporary = 1,
        Permanent = 2,
        
    }
    
    ///<summary>
    ///
    ///
    ///For more information, please refer to:
    ///
    ///</summary>
    public enum ENUM_ImpinjSetQTConfigResultType
    {
        Success = 0,
        Insufficient_Power = 1,
        Nonspecific_Tag_Error = 2,
        No_Response_From_Tag = 3,
        Nonspecific_Reader_Error = 4,
        Incorrect_Password_Error = 5,
        
    }
    
    ///<summary>
    ///
    ///
    ///For more information, please refer to:
    ///
    ///</summary>
    public enum ENUM_ImpinjGetQTConfigResultType
    {
        Success = 0,
        Nonspecific_Tag_Error = 1,
        No_Response_From_Tag = 2,
        Nonspecific_Reader_Error = 3,
        Incorrect_Password_Error = 4,
        
    }
    
    ///<summary>
    ///
    ///
    ///For more information, please refer to:
    ///
    ///</summary>
    public enum ENUM_ImpinjSerializedTIDMode
    {
        Disabled = 0,
        Enabled = 1,
        
    }
    
    ///<summary>
    ///
    ///
    ///For more information, please refer to:
    ///
    ///</summary>
    public enum ENUM_ImpinjRFPhaseAngleMode
    {
        Disabled = 0,
        Enabled = 1,
        
    }
    
    ///<summary>
    ///
    ///
    ///For more information, please refer to:
    ///
    ///</summary>
    public enum ENUM_ImpinjPeakRSSIMode
    {
        Disabled = 0,
        Enabled = 1,
        
    }
    
    ///<summary>
    ///
    ///
    ///For more information, please refer to:
    ///
    ///</summary>
    public enum ENUM_ImpinjGPSCoordinatesMode
    {
        Disabled = 0,
        Enabled = 1,
        
    }
    
    ///<summary>
    ///
    ///
    ///For more information, please refer to:
    ///
    ///</summary>
    public enum ENUM_ImpinjAdvancedGPOMode
    {
        Normal = 0,
        Pulsed = 1,
        Reader_Operational_Status = 2,
        LLRP_Connection_Status = 3,
        Reader_Inventory_Status = 4,
        Network_Connection_Status = 5,
        Reader_Inventory_Tags_Status = 6,
        
    }
    
    ///<summary>
    ///
    ///
    ///For more information, please refer to:
    ///
    ///</summary>
    public enum ENUM_ImpinjOptimizedReadMode
    {
        Disabled = 0,
        Enabled = 1,
        
    }
    
    ///<summary>
    ///
    ///
    ///For more information, please refer to:
    ///
    ///</summary>
    public enum ENUM_ImpinjAccessSpecOrderingMode
    {
        FIFO = 0,
        Ascending = 1,
        
    }
    
    ///<summary>
    ///
    ///
    ///For more information, please refer to:
    ///
    ///</summary>
    public enum ENUM_ImpinjRFDopplerFrequencyMode
    {
        Disabled = 0,
        Enabled = 1,
        
    }
    
    ///<summary>
    ///
    ///
    ///For more information, please refer to:
    ///
    ///</summary>
    public enum ENUM_ImpinjTxPowerReportingModeEnum
    {
        Disabled = 0,
        Enabled = 1,
        
    }
    
    ///<summary>
    ///
    ///
    ///For more information, please refer to:
    ///
    ///</summary>
    public enum ENUM_ImpinjHubConnectedType
    {
        Unknown = 0,
        Disconnected = 1,
        Connected = 2,
        
    }
    
    ///<summary>
    ///
    ///
    ///For more information, please refer to:
    ///
    ///</summary>
    public enum ENUM_ImpinjHubFaultType
    {
        No_Fault = 0,
        RF_Power = 1,
        RF_Power_On_Hub_1 = 2,
        RF_Power_On_Hub_2 = 3,
        RF_Power_On_Hub_3 = 4,
        RF_Power_On_Hub_4 = 5,
        No_Init = 6,
        Serial_Overflow = 7,
        Disconnected = 8,
        
    }
    
    ///<summary>
    ///
    ///
    ///For more information, please refer to:
    ///
    ///</summary>
    public enum ENUM_ImpinjLocationReportType
    {
        Entry = 0,
        Update = 1,
        Exit = 2,
        
    }
    
    ///<summary>
    ///
    ///Description
    ///
    ///For more information, please refer to:
    ///
    ///</summary>
    public enum ENUM_ImpinjDirectionFieldOfView
    {
        ReaderSelected = 0,
        Wide = 1,
        Narrow = 2,
        
    }
    
    ///<summary>
    ///
    ///
    ///For more information, please refer to:
    ///
    ///</summary>
    public enum ENUM_ImpinjDirectionRFMode
    {
        HighSensitivity = 0,
        HighPerformance = 1,
        
    }
    
    ///<summary>
    ///
    ///This parameter specifies the reporting level for direction operation.
    ///
    ///For more information, please refer to:
    ///
    ///</summary>
    public enum ENUM_ImpinjDirectionDiagnosticReportLevel
    {
        Off = 0,
        Basic = 1,
        Extended = 2,
        Debug = 3,
        
    }
    
    ///<summary>
    ///
    ///This parameter specifies the type of direction report.
    ///
    ///For more information, please refer to:
    ///
    ///</summary>
    public enum ENUM_ImpinjDirectionReportType
    {
        Entry = 0,
        Update = 1,
        Exit = 2,
        
    }
    
    ///<summary>
    ///
    ///This parameter indicates the status of the tag population.
    ///
    ///For more information, please refer to:
    ///
    ///</summary>
    public enum ENUM_ImpinjDirectionTagPopulationStatus
    {
        OK = 0,
        UserOverflow = 1,
        SystemOverflow = 2,
        
    }
    
    ///<summary>
    ///
    ///
    ///For more information, please refer to:
    ///
    ///</summary>
    public enum ENUM_ImpinjIntelligentAntennaMode
    {
        Disabled = 0,
        Enabled = 1,
        
    }
    
    ///<summary>
    ///
    ///
    ///For more information, please refer to:
    ///
    ///</summary>
    public enum ENUM_ImpinjAntennaPolarizationType
    {
        LinearHorizontal = 0,
        LinearVertical = 1,
        CircularRight = 2,
        CircularLeft = 3,
        
    }
    
    ///<summary>
    ///
    ///
    ///For more information, please refer to:
    ///
    ///</summary>
    public enum ENUM_ImpinjMarginReadResultType
    {
        Success = 0,
        Failure = 1,
        Insufficient_Power = 2,
        Nonspecific_Tag_Error = 3,
        No_Response_From_Tag = 4,
        Nonspecific_Reader_Error = 5,
        Incorrect_Password_Error = 6,
        Tag_Memory_Overrun_Error = 7,
        Tag_Memory_Locked_Error = 8,
        
    }
    

    public enum ENUM_Impinj_LLRP_MSG_TYPE
    {
        IMPINJ_ENABLE_EXTENSIONS = 21,
        IMPINJ_ENABLE_EXTENSIONS_RESPONSE = 22,
        IMPINJ_SAVE_SETTINGS = 23,
        IMPINJ_SAVE_SETTINGS_RESPONSE = 24,
        
    }

    public enum ENUM_Impinj_LLRP_PARAM_TYPE
    {
        ImpinjRequestedData = 21,
        ImpinjSubRegulatoryRegion = 22,
        ImpinjInventorySearchMode = 23,
        ImpinjFixedFrequencyList = 26,
        ImpinjReducedPowerFrequencyList = 27,
        ImpinjLowDutyCycle = 28,
        ImpinjHubVersions = 1537,
        ImpinjDetailedVersion = 29,
        ImpinjFrequencyCapabilities = 30,
        ImpinjGPIDebounceConfiguration = 36,
        ImpinjReaderTemperature = 37,
        ImpinjLinkMonitorConfiguration = 38,
        ImpinjReportBufferConfiguration = 39,
        ImpinjAccessSpecConfiguration = 40,
        ImpinjBlockWriteWordCount = 41,
        ImpinjBlockPermalock = 42,
        ImpinjBlockPermalockOpSpecResult = 43,
        ImpinjGetBlockPermalockStatus = 44,
        ImpinjGetBlockPermalockStatusOpSpecResult = 45,
        ImpinjSetQTConfig = 46,
        ImpinjSetQTConfigOpSpecResult = 47,
        ImpinjGetQTConfig = 48,
        ImpinjGetQTConfigOpSpecResult = 49,
        ImpinjTagReportContentSelector = 50,
        ImpinjEnableSerializedTID = 51,
        ImpinjEnableRFPhaseAngle = 52,
        ImpinjEnablePeakRSSI = 53,
        ImpinjEnableGPSCoordinates = 54,
        ImpinjSerializedTID = 55,
        ImpinjRFPhaseAngle = 56,
        ImpinjPeakRSSI = 57,
        ImpinjGPSCoordinates = 58,
        ImpinjLoopSpec = 59,
        ImpinjGPSNMEASentences = 60,
        ImpinjGGASentence = 61,
        ImpinjRMCSentence = 62,
        ImpinjOpSpecRetryCount = 63,
        ImpinjAdvancedGPOConfiguration = 64,
        ImpinjEnableOptimizedRead = 65,
        ImpinjAccessSpecOrdering = 66,
        ImpinjEnableRFDopplerFrequency = 67,
        ImpinjRFDopplerFrequency = 68,
        ImpinjInventoryConfiguration = 69,
        ImpinjEnableTxPower = 72,
        ImpinjTxPower = 73,
        ImpinjArrayVersion = 1520,
        ImpinjxArrayCapabilities = 1521,
        ImpinjTiltConfiguration = 1522,
        ImpinjBeaconConfiguration = 1523,
        ImpinjAntennaConfiguration = 1524,
        ImpinjAntennaEventHysteresis = 1526,
        ImpinjAntennaEventConfiguration = 1576,
        ImpinjAntennaAttemptEvent = 1577,
        ImpinjHubConfiguration = 1538,
        ImpinjDiagnosticReport = 1539,
        ImpinjPlacementConfiguration = 1540,
        ImpinjLISpec = 1541,
        ImpinjLocationConfig = 1542,
        ImpinjC1G2LocationConfig = 1543,
        ImpinjLocationReporting = 1544,
        ImpinjLocationConfidence = 1553,
        ImpinjLocationReportData = 1545,
        ImpinjDISpec = 1567,
        ImpinjDirectionSectors = 1568,
        ImpinjDirectionConfig = 1569,
        ImpinjDirectionUserTagPopulationLimit = 1570,
        ImpinjC1G2DirectionConfig = 1571,
        ImpinjExtendedTagInformation = 1552,
        ImpinjDirectionReporting = 1572,
        ImpinjDirectionReportData = 1573,
        ImpinjDirectionDiagnosticData = 1574,
        ImpinjxArrayDirectionCapabilities = 1575,
        ImpinjIntelligentAntennaManagement = 1554,
        ImpinjTransmitPower = 1561,
        ImpinjPolarizationControl = 1562,
        ImpinjAntennaCapabilities = 1563,
        ImpinjAntennaPolarizationCapability = 1564,
        ImpinjDisabledAntennas = 1565,
        ImpinjTIDParity = 1566,
        ImpinjMarginRead = 1578,
        ImpinjMarginReadOpSpecResult = 1579,
        ImpinjBLEVersion = 1580,
        ImpinjLocationAlgorithmControl = 1581,
        ImpinjRFPowerSweep = 1582,
        ImpinjTruncatedReplyConfiguration = 1583,
        
    }
}
  