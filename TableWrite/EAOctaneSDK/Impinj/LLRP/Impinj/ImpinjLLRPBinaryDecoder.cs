
using System;

namespace EA.Impinj.LLRP.Impinj
{
    /// <summary>
    /// LLRP Binary Decoder
    /// </summary>
    public class ImpinjLLRPBinaryDecoder : LLRPBinaryDecoder
    {
        /// <summary>
        /// Decode Binary LLRP packet to LLRP message object
        /// </summary>
        /// <param name="packet">Binary LLRP packet to be decoded</param>
        /// <param name="msg">LLRP message. output</param>
        protected override ILLRPCustomMessage DecodeCustomMessage(BitList bitList, ref int cursor, int length)
        {
            if (cursor >= length) return null;

            // Custom message header is 120 bits
            if (cursor + 120 <= bitList.Count)
            {
                int headerCursor = cursor;
                headerCursor += 6;
                UInt16 msgTypeID = LLRPUtil.BitListToUInt16(bitList, ref headerCursor, 10);
                if ((ENUM_LLRP_MSG_TYPE)msgTypeID == ENUM_LLRP_MSG_TYPE.CUSTOM_MESSAGE)
                {
                    headerCursor += 64;
                    UInt32 msgVendorID = LLRPUtil.BitListToUInt32(bitList, ref headerCursor, 32);
                    Byte msgSubtypeID = LLRPUtil.BitListToByte(bitList, ref headerCursor, 8);

                    if (msgVendorID == 25882)
                    {
                        switch((ENUM_Impinj_LLRP_MSG_TYPE)msgSubtypeID)
                        {
                        
                            case ENUM_Impinj_LLRP_MSG_TYPE.IMPINJ_ENABLE_EXTENSIONS:
                                return MSG_IMPINJ_ENABLE_EXTENSIONS.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_MSG_TYPE.IMPINJ_ENABLE_EXTENSIONS_RESPONSE:
                                return MSG_IMPINJ_ENABLE_EXTENSIONS_RESPONSE.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_MSG_TYPE.IMPINJ_SAVE_SETTINGS:
                                return MSG_IMPINJ_SAVE_SETTINGS.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_MSG_TYPE.IMPINJ_SAVE_SETTINGS_RESPONSE:
                                return MSG_IMPINJ_SAVE_SETTINGS_RESPONSE.FromBitList(this, bitList, ref cursor, length);
                            
                        }
                    }
                }
            }
            
            return base.DecodeCustomMessage(bitList, ref cursor, length);
        }

        
        /// <summary>
        /// Decode Binary LLRP packet to LLRP parameter object
        /// </summary>
        /// <param name="packet">Binary LLRP packet to be decoded</param>
        /// <param name="msg">LLRP message. output</param>
        public override ILLRPCustomParameter DecodeCustomParameter(BitList bitList, ref int cursor, int length)
        {
            if (cursor >= length) return null;
                        
            // Custom parameter header is 96 bits
            if (cursor + 96 <= bitList.Count)
            {
                int headerCursor = cursor;
                
                UInt16 paramTypeID = 0;
                bool isTvEncoding = bitList[headerCursor];
                if (!isTvEncoding)
                {
                    headerCursor += 6;
                    paramTypeID = LLRPUtil.BitListToUInt16(bitList, ref headerCursor, 10);
                }
                if ((ENUM_LLRP_PARAM_TYPE)paramTypeID == ENUM_LLRP_PARAM_TYPE.Custom)
                {
                    headerCursor += 16;
                    UInt32 paramVendorID = LLRPUtil.BitListToUInt32(bitList, ref headerCursor, 32);
                    UInt32 paramSubTypeID = LLRPUtil.BitListToUInt32(bitList, ref headerCursor, 32);

                    if (paramVendorID == 25882)
                    {
                        switch((ENUM_Impinj_LLRP_PARAM_TYPE)paramSubTypeID)
                        {
                        
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjRequestedData:
                                return PARAM_ImpinjRequestedData.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjSubRegulatoryRegion:
                                return PARAM_ImpinjSubRegulatoryRegion.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjInventorySearchMode:
                                return PARAM_ImpinjInventorySearchMode.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjFixedFrequencyList:
                                return PARAM_ImpinjFixedFrequencyList.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjReducedPowerFrequencyList:
                                return PARAM_ImpinjReducedPowerFrequencyList.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjLowDutyCycle:
                                return PARAM_ImpinjLowDutyCycle.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjHubVersions:
                                return PARAM_ImpinjHubVersions.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjDetailedVersion:
                                return PARAM_ImpinjDetailedVersion.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjFrequencyCapabilities:
                                return PARAM_ImpinjFrequencyCapabilities.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjGPIDebounceConfiguration:
                                return PARAM_ImpinjGPIDebounceConfiguration.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjReaderTemperature:
                                return PARAM_ImpinjReaderTemperature.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjLinkMonitorConfiguration:
                                return PARAM_ImpinjLinkMonitorConfiguration.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjReportBufferConfiguration:
                                return PARAM_ImpinjReportBufferConfiguration.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjAccessSpecConfiguration:
                                return PARAM_ImpinjAccessSpecConfiguration.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjBlockWriteWordCount:
                                return PARAM_ImpinjBlockWriteWordCount.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjBlockPermalock:
                                return PARAM_ImpinjBlockPermalock.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjBlockPermalockOpSpecResult:
                                return PARAM_ImpinjBlockPermalockOpSpecResult.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjGetBlockPermalockStatus:
                                return PARAM_ImpinjGetBlockPermalockStatus.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjGetBlockPermalockStatusOpSpecResult:
                                return PARAM_ImpinjGetBlockPermalockStatusOpSpecResult.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjSetQTConfig:
                                return PARAM_ImpinjSetQTConfig.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjSetQTConfigOpSpecResult:
                                return PARAM_ImpinjSetQTConfigOpSpecResult.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjGetQTConfig:
                                return PARAM_ImpinjGetQTConfig.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjGetQTConfigOpSpecResult:
                                return PARAM_ImpinjGetQTConfigOpSpecResult.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjTagReportContentSelector:
                                return PARAM_ImpinjTagReportContentSelector.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjEnableSerializedTID:
                                return PARAM_ImpinjEnableSerializedTID.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjEnableRFPhaseAngle:
                                return PARAM_ImpinjEnableRFPhaseAngle.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjEnablePeakRSSI:
                                return PARAM_ImpinjEnablePeakRSSI.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjEnableGPSCoordinates:
                                return PARAM_ImpinjEnableGPSCoordinates.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjSerializedTID:
                                return PARAM_ImpinjSerializedTID.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjRFPhaseAngle:
                                return PARAM_ImpinjRFPhaseAngle.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjPeakRSSI:
                                return PARAM_ImpinjPeakRSSI.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjGPSCoordinates:
                                return PARAM_ImpinjGPSCoordinates.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjLoopSpec:
                                return PARAM_ImpinjLoopSpec.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjGPSNMEASentences:
                                return PARAM_ImpinjGPSNMEASentences.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjGGASentence:
                                return PARAM_ImpinjGGASentence.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjRMCSentence:
                                return PARAM_ImpinjRMCSentence.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjOpSpecRetryCount:
                                return PARAM_ImpinjOpSpecRetryCount.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjAdvancedGPOConfiguration:
                                return PARAM_ImpinjAdvancedGPOConfiguration.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjEnableOptimizedRead:
                                return PARAM_ImpinjEnableOptimizedRead.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjAccessSpecOrdering:
                                return PARAM_ImpinjAccessSpecOrdering.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjEnableRFDopplerFrequency:
                                return PARAM_ImpinjEnableRFDopplerFrequency.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjRFDopplerFrequency:
                                return PARAM_ImpinjRFDopplerFrequency.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjInventoryConfiguration:
                                return PARAM_ImpinjInventoryConfiguration.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjEnableTxPower:
                                return PARAM_ImpinjEnableTxPower.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjTxPower:
                                return PARAM_ImpinjTxPower.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjArrayVersion:
                                return PARAM_ImpinjArrayVersion.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjxArrayCapabilities:
                                return PARAM_ImpinjxArrayCapabilities.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjTiltConfiguration:
                                return PARAM_ImpinjTiltConfiguration.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjBeaconConfiguration:
                                return PARAM_ImpinjBeaconConfiguration.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjAntennaConfiguration:
                                return PARAM_ImpinjAntennaConfiguration.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjAntennaEventHysteresis:
                                return PARAM_ImpinjAntennaEventHysteresis.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjAntennaEventConfiguration:
                                return PARAM_ImpinjAntennaEventConfiguration.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjAntennaAttemptEvent:
                                return PARAM_ImpinjAntennaAttemptEvent.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjHubConfiguration:
                                return PARAM_ImpinjHubConfiguration.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjDiagnosticReport:
                                return PARAM_ImpinjDiagnosticReport.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjPlacementConfiguration:
                                return PARAM_ImpinjPlacementConfiguration.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjLISpec:
                                return PARAM_ImpinjLISpec.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjLocationConfig:
                                return PARAM_ImpinjLocationConfig.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjC1G2LocationConfig:
                                return PARAM_ImpinjC1G2LocationConfig.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjLocationReporting:
                                return PARAM_ImpinjLocationReporting.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjLocationConfidence:
                                return PARAM_ImpinjLocationConfidence.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjLocationReportData:
                                return PARAM_ImpinjLocationReportData.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjDISpec:
                                return PARAM_ImpinjDISpec.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjDirectionSectors:
                                return PARAM_ImpinjDirectionSectors.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjDirectionConfig:
                                return PARAM_ImpinjDirectionConfig.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjDirectionUserTagPopulationLimit:
                                return PARAM_ImpinjDirectionUserTagPopulationLimit.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjC1G2DirectionConfig:
                                return PARAM_ImpinjC1G2DirectionConfig.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjExtendedTagInformation:
                                return PARAM_ImpinjExtendedTagInformation.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjDirectionReporting:
                                return PARAM_ImpinjDirectionReporting.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjDirectionReportData:
                                return PARAM_ImpinjDirectionReportData.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjDirectionDiagnosticData:
                                return PARAM_ImpinjDirectionDiagnosticData.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjxArrayDirectionCapabilities:
                                return PARAM_ImpinjxArrayDirectionCapabilities.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjIntelligentAntennaManagement:
                                return PARAM_ImpinjIntelligentAntennaManagement.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjTransmitPower:
                                return PARAM_ImpinjTransmitPower.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjPolarizationControl:
                                return PARAM_ImpinjPolarizationControl.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjAntennaCapabilities:
                                return PARAM_ImpinjAntennaCapabilities.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjAntennaPolarizationCapability:
                                return PARAM_ImpinjAntennaPolarizationCapability.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjDisabledAntennas:
                                return PARAM_ImpinjDisabledAntennas.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjTIDParity:
                                return PARAM_ImpinjTIDParity.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjMarginRead:
                                return PARAM_ImpinjMarginRead.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjMarginReadOpSpecResult:
                                return PARAM_ImpinjMarginReadOpSpecResult.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjBLEVersion:
                                return PARAM_ImpinjBLEVersion.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjLocationAlgorithmControl:
                                return PARAM_ImpinjLocationAlgorithmControl.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjRFPowerSweep:
                                return PARAM_ImpinjRFPowerSweep.FromBitList(this, bitList, ref cursor, length);
                            
                            case ENUM_Impinj_LLRP_PARAM_TYPE.ImpinjTruncatedReplyConfiguration:
                                return PARAM_ImpinjTruncatedReplyConfiguration.FromBitList(this, bitList, ref cursor, length);
                            
                        }
                    }
                }
            }

            return base.DecodeCustomParameter(bitList, ref cursor, length);
        }
    }
}
  