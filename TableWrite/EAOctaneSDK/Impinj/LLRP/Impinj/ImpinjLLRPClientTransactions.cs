
using System;
using System.Threading.Tasks;

namespace EA.Impinj.LLRP.Impinj
{
    /// <summary>
    /// Impinj LLRP client transaction methods
    /// </summary>
    public partial class ImpinjLLRPClient
    {
    
        /// <summary>
        /// IMPINJ_ENABLE_EXTENSIONS message call.
        /// </summary>
        /// <param name="msg">MSG_IMPINJ_ENABLE_EXTENSIONS to send to reader.</param>
        /// <param name="timeoutMilliseconds">Fuction call time out in milliseconds.</param>
        /// <returns>Returns MSG_IMPINJ_ENABLE_EXTENSIONS_RESPONSE message.</returns>
        public async Task<MSG_IMPINJ_ENABLE_EXTENSIONS_RESPONSE> IMPINJ_ENABLE_EXTENSIONS(MSG_IMPINJ_ENABLE_EXTENSIONS msg, int timeoutMilliseconds)
        {
            return await TransactAsync(msg, (UInt16)ENUM_LLRP_MSG_TYPE.CUSTOM_MESSAGE, 25882, (Byte)ENUM_Impinj_LLRP_MSG_TYPE.IMPINJ_ENABLE_EXTENSIONS_RESPONSE, timeoutMilliseconds).ConfigureAwait(false) as MSG_IMPINJ_ENABLE_EXTENSIONS_RESPONSE;
        }
        public async Task<MSG_IMPINJ_ENABLE_EXTENSIONS_RESPONSE> IMPINJ_ENABLE_EXTENSIONS(MSG_IMPINJ_ENABLE_EXTENSIONS msg)
        {
            return await TransactAsync(msg, (UInt16)ENUM_LLRP_MSG_TYPE.CUSTOM_MESSAGE, 25882, (Byte)ENUM_Impinj_LLRP_MSG_TYPE.IMPINJ_ENABLE_EXTENSIONS_RESPONSE, ResponseTimeout).ConfigureAwait(false) as MSG_IMPINJ_ENABLE_EXTENSIONS_RESPONSE;
        }
      
        /// <summary>
        /// IMPINJ_SAVE_SETTINGS message call.
        /// </summary>
        /// <param name="msg">MSG_IMPINJ_SAVE_SETTINGS to send to reader.</param>
        /// <param name="timeoutMilliseconds">Fuction call time out in milliseconds.</param>
        /// <returns>Returns MSG_IMPINJ_SAVE_SETTINGS_RESPONSE message.</returns>
        public async Task<MSG_IMPINJ_SAVE_SETTINGS_RESPONSE> IMPINJ_SAVE_SETTINGS(MSG_IMPINJ_SAVE_SETTINGS msg, int timeoutMilliseconds)
        {
            return await TransactAsync(msg, (UInt16)ENUM_LLRP_MSG_TYPE.CUSTOM_MESSAGE, 25882, (Byte)ENUM_Impinj_LLRP_MSG_TYPE.IMPINJ_SAVE_SETTINGS_RESPONSE, timeoutMilliseconds).ConfigureAwait(false) as MSG_IMPINJ_SAVE_SETTINGS_RESPONSE;
        }
        public async Task<MSG_IMPINJ_SAVE_SETTINGS_RESPONSE> IMPINJ_SAVE_SETTINGS(MSG_IMPINJ_SAVE_SETTINGS msg)
        {
            return await TransactAsync(msg, (UInt16)ENUM_LLRP_MSG_TYPE.CUSTOM_MESSAGE, 25882, (Byte)ENUM_Impinj_LLRP_MSG_TYPE.IMPINJ_SAVE_SETTINGS_RESPONSE, ResponseTimeout).ConfigureAwait(false) as MSG_IMPINJ_SAVE_SETTINGS_RESPONSE;
        }
      
    }
}
  