
using System.Threading.Tasks;

namespace EA.Impinj.LLRP.Impinj
{
    /// <summary>
    /// Impinj LLRP server transaction methods
    /// </summary>
    public partial class ImpinjLLRPServer
    {
    
        /// <summary>
        /// IMPINJ_ENABLE_EXTENSIONS message response.
        /// </summary>
        /// <param name="msg">MSG_IMPINJ_ENABLE_EXTENSIONS to send to reader.</param>
        public async Task IMPINJ_ENABLE_EXTENSIONS_RESPONSE(MSG_IMPINJ_ENABLE_EXTENSIONS_RESPONSE msg)
        {
            await SendAsync(msg).ConfigureAwait(false);
        }
      
        /// <summary>
        /// IMPINJ_SAVE_SETTINGS message response.
        /// </summary>
        /// <param name="msg">MSG_IMPINJ_SAVE_SETTINGS to send to reader.</param>
        public async Task IMPINJ_SAVE_SETTINGS_RESPONSE(MSG_IMPINJ_SAVE_SETTINGS_RESPONSE msg)
        {
            await SendAsync(msg).ConfigureAwait(false);
        }
      
    }
}
  