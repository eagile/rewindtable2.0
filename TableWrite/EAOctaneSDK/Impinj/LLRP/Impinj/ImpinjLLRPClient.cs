
namespace EA.Impinj.LLRP.Impinj
{
    /// <summary>
    /// Impinj LLRP Client
    /// </summary>
    public partial class ImpinjLLRPClient : LLRPClient
    {
        public ImpinjLLRPClient() : base()
        {
            MessageDecoder = new ImpinjLLRPBinaryDecoder();
        }
    }
}
  