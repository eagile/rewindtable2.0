
namespace EA.Impinj.LLRP.Impinj
{
    /// <summary>
    /// Impinj LLRP Client
    /// </summary>
    public partial class ImpinjLLRPServer : LLRPServer
    {
        public ImpinjLLRPServer() : base()
        {
            MessageDecoder = new ImpinjLLRPBinaryDecoder();
        }
    }
}
  