
using System;
using System.Linq;
using System.Xml.Linq;

namespace EA.Impinj.LLRP.Impinj
{
    #region Custom Message Interface
    
    public interface IIMPINJ_ENABLE_EXTENSIONS_Custom_Param : ILLRPCustomParameter { }
        
    public interface IIMPINJ_ENABLE_EXTENSIONS_RESPONSE_Custom_Param : ILLRPCustomParameter { }
        
    public interface IIMPINJ_SAVE_SETTINGS_Custom_Param : ILLRPCustomParameter { }
        
    public interface IIMPINJ_SAVE_SETTINGS_RESPONSE_Custom_Param : ILLRPCustomParameter { }
        
    #endregion

    // LLRP custom message definitions

    
    ///<summary>
    ///
    ///The IMPINJ_ENABLE_EXTENSIONS message only applies for the duration of the current LLRP connection. If the LLRP connection is broken and re-established, the application must re-issue this command.
    ///
    ///For more information, please refer to:
    ///
    ///</summary>
    public class MSG_IMPINJ_ENABLE_EXTENSIONS : MSG_CUSTOM_MESSAGE
    {
        public new const UInt32 VENDOR_ID = 25882;
        public new const Byte SUBTYPE_ID = 21;

        public MSG_IMPINJ_ENABLE_EXTENSIONS(UInt32 msgId) : base(msgId)
        {
            VendorIdentifier = VENDOR_ID;
            MessageSubtype = SUBTYPE_ID;
        }
      
        private const UInt16 reserved_len2 = 32;
        
        public LLRPCustomParameterList Custom { get; } = new LLRPCustomParameterList();

        public bool TryAddCustomParameter(ILLRPCustomParameter param)
        {
            if (param is IIMPINJ_ENABLE_EXTENSIONS_Custom_Param)
            {
                Custom.Add(param);
                return true;
            }

            if (param.GetType() == typeof(PARAM_Custom))
            {
                Custom.Add(param);
                return true;
            }

            return false;
        }
            

        /// <summary>
        /// Serialize message to XML
        /// </summary>
        /// <returns>XML element</returns>
        public override XElement ToXml()
        {
            XNamespace xmlnsLlrp = XNamespace.Get(LLRPConstants.NAMESPACE_URI);

            XNamespace xmlns = XNamespace.Get(ImpinjLLRPConstants.NAMESPACE_URI);
            XNamespace xsi = XNamespace.Get(LLRPConstants.NAMESPACE_SCHEMAINSTANCE);
            XNamespace schemaLocation = XNamespace.Get($"{LLRPConstants.NAMESPACE_URI} {LLRPConstants.NAMESPACE_SCHEMALOCATION} {ImpinjLLRPConstants.NAMESPACE_URI} {ImpinjLLRPConstants.NAMESPACE_SCHEMALOCATION}");
            string prefix = ImpinjLLRPConstants.NAMESPACE_PREFIX;

            XElement rootElement = new XElement(xmlns + "IMPINJ_ENABLE_EXTENSIONS",
                new XAttribute(XNamespace.Xmlns + "xsi", xsi),
                new XAttribute(xsi + "schemaLocation", schemaLocation),
                new XAttribute(XNamespace.Xmlns + prefix, xmlns),
                new XAttribute("xmlns", xmlnsLlrp),
                new XAttribute("Version", "1"),
                new XAttribute("MessageID", MsgID)
            );
    
            if (Custom != null)
            {
          
                int len = Custom.Length;
                for (int i = 0; i < len; i++)
                {
                    rootElement.Add(Custom[i].ToXml());
                }
            
            }
        
            if (rootElement.IsEmpty)
            {
                rootElement.Value = string.Empty;
            }

            return rootElement;
        }

        /// <summary>
        /// Deserialize XML to message
        /// </summary>
        /// <param name="parser">XML parser for parsing custom parameters</param>
        /// <param name="rootElement">XML element to be deserialized</param>
        /// <returns>MSG_IMPINJ_ENABLE_EXTENSIONS</returns>
        public new static MSG_IMPINJ_ENABLE_EXTENSIONS FromXml(ICustomParameterParser parser, XElement rootElement)
        {
            if (parser == null)
                throw new ArgumentNullException(nameof(parser));
            if (rootElement == null)
                throw new ArgumentNullException(nameof(rootElement));

            UInt32 msgId = 0;
            {
                XAttribute attribute = rootElement.Attribute("MessageID");
                if (attribute != null)
                {
                    msgId = Convert.ToUInt32(attribute.Value);
                }
            }
            MSG_IMPINJ_ENABLE_EXTENSIONS obj = new MSG_IMPINJ_ENABLE_EXTENSIONS(msgId);

            var elements = rootElement.Elements().ToList();

    
            {
                XNamespace xmlnsLlrp = XNamespace.Get(LLRPConstants.NAMESPACE_URI);
                for (int i = 0; i < elements.Count; i++)
                {
                    if (elements[i] == null) continue;
                    if (elements[i].Name.LocalName == "Custom" || elements[i].Name.Namespace != xmlnsLlrp)
                    {
                        ILLRPCustomParameter custom = parser.ParseCustomParameter(elements[i]);
                        if (custom != null)
                        {
                            if (obj.TryAddCustomParameter(custom))
                            {
                                elements[i] = null;
                            }
                        }
                    }
                }
            }
            
            elements.Clear();

            return obj;
        }

        /// <summary>
        /// Deserialize XML string to message
        /// </summary>
        /// <param name="parser">XML parser for parsing custom parameters</param>
        /// <param name="xmlString">XML string to be deserialized</param>
        /// <returns>PARAM_IMPINJ_ENABLE_EXTENSIONS</returns>
        public new static MSG_IMPINJ_ENABLE_EXTENSIONS FromXmlString(ICustomParameterParser parser, string xmlString)
        {
            XDocument xDoc = XDocument.Load(xmlString);
            return FromXml(parser, xDoc.Root);
        }

        /// <summary>
        /// Encode message to bit list
        /// </summary>
        /// <returns>bit list</returns>
        public override BitList ToBitList()
        {
            BitList bitList = new BitList(10000);
            LLRPUtil.UInt16ToBitList(0, bitList, 3);
            LLRPUtil.UInt16ToBitList(MsgVersion, bitList, 3);
            LLRPUtil.UInt16ToBitList(MsgTypeID, bitList, 10);
            LLRPUtil.UInt32ToBitList(MsgLength, bitList, 32);
            LLRPUtil.UInt32ToBitList(MsgID, bitList, 32);
            LLRPUtil.UInt32ToBitList(VendorIdentifier, bitList, 32);
            LLRPUtil.ByteToBitList(MessageSubtype, bitList, 8);
    
            LLRPUtil.Int32ToBitList(0, bitList, reserved_len2);
        
            if (Custom != null)
            {
          
                int len = Custom.Length;
                for (int i = 0; i < len; i++)
                {
                    LLRPUtil.ParameterToBitList(Custom[i], bitList);
                }
            
            }
        

            UInt32 msgLength = (UInt32)(bitList.Count / 8);
            LLRPUtil.UInt32ToBitList(msgLength, bitList, 16, 32);

            return bitList;
        }

        /// <summary>
        /// Decode message from existing bit list.
        /// </summary>
        /// <param name="decoder">Binary decoder for decoding custom parameters</param>
        /// <param name="bitList">input bit list</param>
        /// <param name="cursor">pointer to current position</param>
        /// <param name="length">data length</param>
        /// <returns>MSG_IMPINJ_ENABLE_EXTENSIONS</returns>
        public new static MSG_IMPINJ_ENABLE_EXTENSIONS FromBitList(ICustomParameterDecoder decoder, BitList bitList, ref int cursor, int length)
        {
            if (decoder == null)
                throw new ArgumentNullException(nameof(decoder));
            if (bitList == null)
                throw new ArgumentNullException(nameof(bitList));
            if (cursor < 0)
                throw new ArgumentOutOfRangeException(nameof(cursor));
            if (length < 0)
                throw new ArgumentOutOfRangeException(nameof(length));

            if (cursor >= length) return null;

            int originalCursor = cursor;

            cursor += 6;
            UInt16 msgType = LLRPUtil.BitListToUInt16(bitList, ref cursor, 10);

            if (msgType != TYPE_ID)
            {
                cursor = originalCursor;
                return null;
            }

            UInt32 msgLength = LLRPUtil.BitListToUInt32(bitList, ref cursor, 32);
            UInt32 msgId = LLRPUtil.BitListToUInt32(bitList, ref cursor, 32);

            UInt32 vendorIdentifier = LLRPUtil.BitListToUInt32(bitList, ref cursor, 32);
            if (vendorIdentifier != VENDOR_ID)
            {
                cursor = originalCursor;
                return null;
            }

            Byte messageSubtype = LLRPUtil.BitListToByte(bitList, ref cursor, 8);
            if (messageSubtype != SUBTYPE_ID)
            {
                cursor = originalCursor;
                return null;
            }

            MSG_IMPINJ_ENABLE_EXTENSIONS obj = new MSG_IMPINJ_ENABLE_EXTENSIONS(msgId)
            {
                MsgLength = msgLength
            };

            int maxCursor = (Int32)(originalCursor + obj.MsgLength * 8);

            if (cursor > maxCursor)
            {
                throw new LLRPMalformedPacketException();
            }

    
            cursor += reserved_len2;
        
            {
                int temp_cursor = cursor;
                ILLRPCustomParameter custom;
                while (cursor < maxCursor &&
                    (custom = decoder.DecodeCustomParameter(bitList, ref cursor, length)) != null &&
                    cursor <= maxCursor &&
                    obj.TryAddCustomParameter(custom))
                {
                    temp_cursor = cursor;
                }
                cursor = temp_cursor;
            }
            
            return obj;
        }

    }
    
    ///<summary>
    ///
    ///This custom message is the response by the Reader to an IMPINJ_ENABLE_EXTENSIONS message. If the Reader was capable of enabling the Impinj extensions the reader returns the success code in the LLRPStatus parameter. If there is an error, the reader returns an appropriate error code.
    ///
    ///For more information, please refer to:
    ///
    ///</summary>
    public class MSG_IMPINJ_ENABLE_EXTENSIONS_RESPONSE : MSG_CUSTOM_MESSAGE, ILLRPResponseMessage
    {
        public new const UInt32 VENDOR_ID = 25882;
        public new const Byte SUBTYPE_ID = 22;

        public MSG_IMPINJ_ENABLE_EXTENSIONS_RESPONSE(UInt32 msgId) : base(msgId)
        {
            VendorIdentifier = VENDOR_ID;
            MessageSubtype = SUBTYPE_ID;
        }
      
        public PARAM_LLRPStatus LLRPStatus { get; set; }
                
        public LLRPCustomParameterList Custom { get; } = new LLRPCustomParameterList();

        public bool TryAddCustomParameter(ILLRPCustomParameter param)
        {
            if (param is IIMPINJ_ENABLE_EXTENSIONS_RESPONSE_Custom_Param)
            {
                Custom.Add(param);
                return true;
            }

            if (param.GetType() == typeof(PARAM_Custom))
            {
                Custom.Add(param);
                return true;
            }

            return false;
        }
            

        /// <summary>
        /// Serialize message to XML
        /// </summary>
        /// <returns>XML element</returns>
        public override XElement ToXml()
        {
            XNamespace xmlnsLlrp = XNamespace.Get(LLRPConstants.NAMESPACE_URI);

            XNamespace xmlns = XNamespace.Get(ImpinjLLRPConstants.NAMESPACE_URI);
            XNamespace xsi = XNamespace.Get(LLRPConstants.NAMESPACE_SCHEMAINSTANCE);
            XNamespace schemaLocation = XNamespace.Get($"{LLRPConstants.NAMESPACE_URI} {LLRPConstants.NAMESPACE_SCHEMALOCATION} {ImpinjLLRPConstants.NAMESPACE_URI} {ImpinjLLRPConstants.NAMESPACE_SCHEMALOCATION}");
            string prefix = ImpinjLLRPConstants.NAMESPACE_PREFIX;

            XElement rootElement = new XElement(xmlns + "IMPINJ_ENABLE_EXTENSIONS_RESPONSE",
                new XAttribute(XNamespace.Xmlns + "xsi", xsi),
                new XAttribute(xsi + "schemaLocation", schemaLocation),
                new XAttribute(XNamespace.Xmlns + prefix, xmlns),
                new XAttribute("xmlns", xmlnsLlrp),
                new XAttribute("Version", "1"),
                new XAttribute("MessageID", MsgID)
            );
    
            if (LLRPStatus != null)
            {
          
                rootElement.Add(LLRPStatus.ToXml());
                
            }
        
            if (Custom != null)
            {
          
                int len = Custom.Length;
                for (int i = 0; i < len; i++)
                {
                    rootElement.Add(Custom[i].ToXml());
                }
            
            }
        
            if (rootElement.IsEmpty)
            {
                rootElement.Value = string.Empty;
            }

            return rootElement;
        }

        /// <summary>
        /// Deserialize XML to message
        /// </summary>
        /// <param name="parser">XML parser for parsing custom parameters</param>
        /// <param name="rootElement">XML element to be deserialized</param>
        /// <returns>MSG_IMPINJ_ENABLE_EXTENSIONS_RESPONSE</returns>
        public new static MSG_IMPINJ_ENABLE_EXTENSIONS_RESPONSE FromXml(ICustomParameterParser parser, XElement rootElement)
        {
            if (parser == null)
                throw new ArgumentNullException(nameof(parser));
            if (rootElement == null)
                throw new ArgumentNullException(nameof(rootElement));

            UInt32 msgId = 0;
            {
                XAttribute attribute = rootElement.Attribute("MessageID");
                if (attribute != null)
                {
                    msgId = Convert.ToUInt32(attribute.Value);
                }
            }
            MSG_IMPINJ_ENABLE_EXTENSIONS_RESPONSE obj = new MSG_IMPINJ_ENABLE_EXTENSIONS_RESPONSE(msgId);

            var elements = rootElement.Elements().ToList();

    
            for (int i = 0; i < elements.Count; i++)
            {
                if (elements[i] == null) continue;
                if (elements[i].Name.LocalName == "LLRPStatus")
                {
                    obj.LLRPStatus = PARAM_LLRPStatus.FromXml(parser, elements[i]);
                    elements[i] = null;
                    break;
                }
            }
                
            {
                XNamespace xmlnsLlrp = XNamespace.Get(LLRPConstants.NAMESPACE_URI);
                for (int i = 0; i < elements.Count; i++)
                {
                    if (elements[i] == null) continue;
                    if (elements[i].Name.LocalName == "Custom" || elements[i].Name.Namespace != xmlnsLlrp)
                    {
                        ILLRPCustomParameter custom = parser.ParseCustomParameter(elements[i]);
                        if (custom != null)
                        {
                            if (obj.TryAddCustomParameter(custom))
                            {
                                elements[i] = null;
                            }
                        }
                    }
                }
            }
            
            elements.Clear();

            return obj;
        }

        /// <summary>
        /// Deserialize XML string to message
        /// </summary>
        /// <param name="parser">XML parser for parsing custom parameters</param>
        /// <param name="xmlString">XML string to be deserialized</param>
        /// <returns>PARAM_IMPINJ_ENABLE_EXTENSIONS_RESPONSE</returns>
        public new static MSG_IMPINJ_ENABLE_EXTENSIONS_RESPONSE FromXmlString(ICustomParameterParser parser, string xmlString)
        {
            XDocument xDoc = XDocument.Load(xmlString);
            return FromXml(parser, xDoc.Root);
        }

        /// <summary>
        /// Encode message to bit list
        /// </summary>
        /// <returns>bit list</returns>
        public override BitList ToBitList()
        {
            BitList bitList = new BitList(10000);
            LLRPUtil.UInt16ToBitList(0, bitList, 3);
            LLRPUtil.UInt16ToBitList(MsgVersion, bitList, 3);
            LLRPUtil.UInt16ToBitList(MsgTypeID, bitList, 10);
            LLRPUtil.UInt32ToBitList(MsgLength, bitList, 32);
            LLRPUtil.UInt32ToBitList(MsgID, bitList, 32);
            LLRPUtil.UInt32ToBitList(VendorIdentifier, bitList, 32);
            LLRPUtil.ByteToBitList(MessageSubtype, bitList, 8);
    
            if (LLRPStatus != null)
            {
          
                LLRPUtil.ParameterToBitList(LLRPStatus, bitList);
            
            }
        
            if (Custom != null)
            {
          
                int len = Custom.Length;
                for (int i = 0; i < len; i++)
                {
                    LLRPUtil.ParameterToBitList(Custom[i], bitList);
                }
            
            }
        

            UInt32 msgLength = (UInt32)(bitList.Count / 8);
            LLRPUtil.UInt32ToBitList(msgLength, bitList, 16, 32);

            return bitList;
        }

        /// <summary>
        /// Decode message from existing bit list.
        /// </summary>
        /// <param name="decoder">Binary decoder for decoding custom parameters</param>
        /// <param name="bitList">input bit list</param>
        /// <param name="cursor">pointer to current position</param>
        /// <param name="length">data length</param>
        /// <returns>MSG_IMPINJ_ENABLE_EXTENSIONS_RESPONSE</returns>
        public new static MSG_IMPINJ_ENABLE_EXTENSIONS_RESPONSE FromBitList(ICustomParameterDecoder decoder, BitList bitList, ref int cursor, int length)
        {
            if (decoder == null)
                throw new ArgumentNullException(nameof(decoder));
            if (bitList == null)
                throw new ArgumentNullException(nameof(bitList));
            if (cursor < 0)
                throw new ArgumentOutOfRangeException(nameof(cursor));
            if (length < 0)
                throw new ArgumentOutOfRangeException(nameof(length));

            if (cursor >= length) return null;

            int originalCursor = cursor;

            cursor += 6;
            UInt16 msgType = LLRPUtil.BitListToUInt16(bitList, ref cursor, 10);

            if (msgType != TYPE_ID)
            {
                cursor = originalCursor;
                return null;
            }

            UInt32 msgLength = LLRPUtil.BitListToUInt32(bitList, ref cursor, 32);
            UInt32 msgId = LLRPUtil.BitListToUInt32(bitList, ref cursor, 32);

            UInt32 vendorIdentifier = LLRPUtil.BitListToUInt32(bitList, ref cursor, 32);
            if (vendorIdentifier != VENDOR_ID)
            {
                cursor = originalCursor;
                return null;
            }

            Byte messageSubtype = LLRPUtil.BitListToByte(bitList, ref cursor, 8);
            if (messageSubtype != SUBTYPE_ID)
            {
                cursor = originalCursor;
                return null;
            }

            MSG_IMPINJ_ENABLE_EXTENSIONS_RESPONSE obj = new MSG_IMPINJ_ENABLE_EXTENSIONS_RESPONSE(msgId)
            {
                MsgLength = msgLength
            };

            int maxCursor = (Int32)(originalCursor + obj.MsgLength * 8);

            if (cursor > maxCursor)
            {
                throw new LLRPMalformedPacketException();
            }

    
            obj.LLRPStatus = PARAM_LLRPStatus.FromBitList(decoder, bitList, ref cursor, length);
              
            {
                int temp_cursor = cursor;
                ILLRPCustomParameter custom;
                while (cursor < maxCursor &&
                    (custom = decoder.DecodeCustomParameter(bitList, ref cursor, length)) != null &&
                    cursor <= maxCursor &&
                    obj.TryAddCustomParameter(custom))
                {
                    temp_cursor = cursor;
                }
                cursor = temp_cursor;
            }
            
            return obj;
        }

    }
    
    ///<summary>
    ///
    ///This custom message instructs the Reader to save its current settings to persistent storage. The saved parameters will then become the Reader's power-on and reset settings.
    ///On Speedway the settings include only the current configuration set through SET_READER_CONFIG.
    ///On Speedway Revolution the settings include the current configuration, ROSpecs, and AccessSpecs. The enabled or disabled state of ROSpecs and AccessSpecs are saved. An active ROSpec is saved as enabled. The current countdown of AccessSpecs, if any, are saved. The enable/disable state and countdown are not automatically updated in persistent storage during reader operation.
    ///Note that there is no way to recall the persistent settings during runtime. Only after a Reader power-on or reset are the persistent settings applied. When ResetToFactoryDefault in SET_READER_CONFIG is true the persistent settings are deleted. The SaveConfiguration Boolean must be set TRUE. When FALSE this message does nothing.
    ///
    ///For more information, please refer to:
    ///
    ///</summary>
    public class MSG_IMPINJ_SAVE_SETTINGS : MSG_CUSTOM_MESSAGE
    {
        public new const UInt32 VENDOR_ID = 25882;
        public new const Byte SUBTYPE_ID = 23;

        public MSG_IMPINJ_SAVE_SETTINGS(UInt32 msgId) : base(msgId)
        {
            VendorIdentifier = VENDOR_ID;
            MessageSubtype = SUBTYPE_ID;
        }
      
        public Boolean SaveConfiguration { get; set; } = false;
        private const UInt16 reserved_len3 = 7;
        
        public LLRPCustomParameterList Custom { get; } = new LLRPCustomParameterList();

        public bool TryAddCustomParameter(ILLRPCustomParameter param)
        {
            if (param is IIMPINJ_SAVE_SETTINGS_Custom_Param)
            {
                Custom.Add(param);
                return true;
            }

            if (param.GetType() == typeof(PARAM_Custom))
            {
                Custom.Add(param);
                return true;
            }

            return false;
        }
            

        /// <summary>
        /// Serialize message to XML
        /// </summary>
        /// <returns>XML element</returns>
        public override XElement ToXml()
        {
            XNamespace xmlnsLlrp = XNamespace.Get(LLRPConstants.NAMESPACE_URI);

            XNamespace xmlns = XNamespace.Get(ImpinjLLRPConstants.NAMESPACE_URI);
            XNamespace xsi = XNamespace.Get(LLRPConstants.NAMESPACE_SCHEMAINSTANCE);
            XNamespace schemaLocation = XNamespace.Get($"{LLRPConstants.NAMESPACE_URI} {LLRPConstants.NAMESPACE_SCHEMALOCATION} {ImpinjLLRPConstants.NAMESPACE_URI} {ImpinjLLRPConstants.NAMESPACE_SCHEMALOCATION}");
            string prefix = ImpinjLLRPConstants.NAMESPACE_PREFIX;

            XElement rootElement = new XElement(xmlns + "IMPINJ_SAVE_SETTINGS",
                new XAttribute(XNamespace.Xmlns + "xsi", xsi),
                new XAttribute(xsi + "schemaLocation", schemaLocation),
                new XAttribute(XNamespace.Xmlns + prefix, xmlns),
                new XAttribute("xmlns", xmlnsLlrp),
                new XAttribute("Version", "1"),
                new XAttribute("MessageID", MsgID)
            );
    
            {
          
                rootElement.Add(new XElement(xmlns + "SaveConfiguration", LLRPUtil.BooleanToTextString(SaveConfiguration)));
                
            }
        
            if (Custom != null)
            {
          
                int len = Custom.Length;
                for (int i = 0; i < len; i++)
                {
                    rootElement.Add(Custom[i].ToXml());
                }
            
            }
        
            if (rootElement.IsEmpty)
            {
                rootElement.Value = string.Empty;
            }

            return rootElement;
        }

        /// <summary>
        /// Deserialize XML to message
        /// </summary>
        /// <param name="parser">XML parser for parsing custom parameters</param>
        /// <param name="rootElement">XML element to be deserialized</param>
        /// <returns>MSG_IMPINJ_SAVE_SETTINGS</returns>
        public new static MSG_IMPINJ_SAVE_SETTINGS FromXml(ICustomParameterParser parser, XElement rootElement)
        {
            if (parser == null)
                throw new ArgumentNullException(nameof(parser));
            if (rootElement == null)
                throw new ArgumentNullException(nameof(rootElement));

            UInt32 msgId = 0;
            {
                XAttribute attribute = rootElement.Attribute("MessageID");
                if (attribute != null)
                {
                    msgId = Convert.ToUInt32(attribute.Value);
                }
            }
            MSG_IMPINJ_SAVE_SETTINGS obj = new MSG_IMPINJ_SAVE_SETTINGS(msgId);

            var elements = rootElement.Elements().ToList();

    
            for (int i = 0; i < elements.Count; i++)
            {
                if (elements[i] == null) continue;
                if (elements[i].Name.LocalName == "SaveConfiguration")
                {
          
                    obj.SaveConfiguration = LLRPUtil.TextStringToBoolean(elements[i].Value);
                
                    elements[i] = null;
                    break;
                }
            }
        
            {
                XNamespace xmlnsLlrp = XNamespace.Get(LLRPConstants.NAMESPACE_URI);
                for (int i = 0; i < elements.Count; i++)
                {
                    if (elements[i] == null) continue;
                    if (elements[i].Name.LocalName == "Custom" || elements[i].Name.Namespace != xmlnsLlrp)
                    {
                        ILLRPCustomParameter custom = parser.ParseCustomParameter(elements[i]);
                        if (custom != null)
                        {
                            if (obj.TryAddCustomParameter(custom))
                            {
                                elements[i] = null;
                            }
                        }
                    }
                }
            }
            
            elements.Clear();

            return obj;
        }

        /// <summary>
        /// Deserialize XML string to message
        /// </summary>
        /// <param name="parser">XML parser for parsing custom parameters</param>
        /// <param name="xmlString">XML string to be deserialized</param>
        /// <returns>PARAM_IMPINJ_SAVE_SETTINGS</returns>
        public new static MSG_IMPINJ_SAVE_SETTINGS FromXmlString(ICustomParameterParser parser, string xmlString)
        {
            XDocument xDoc = XDocument.Load(xmlString);
            return FromXml(parser, xDoc.Root);
        }

        /// <summary>
        /// Encode message to bit list
        /// </summary>
        /// <returns>bit list</returns>
        public override BitList ToBitList()
        {
            BitList bitList = new BitList(10000);
            LLRPUtil.UInt16ToBitList(0, bitList, 3);
            LLRPUtil.UInt16ToBitList(MsgVersion, bitList, 3);
            LLRPUtil.UInt16ToBitList(MsgTypeID, bitList, 10);
            LLRPUtil.UInt32ToBitList(MsgLength, bitList, 32);
            LLRPUtil.UInt32ToBitList(MsgID, bitList, 32);
            LLRPUtil.UInt32ToBitList(VendorIdentifier, bitList, 32);
            LLRPUtil.ByteToBitList(MessageSubtype, bitList, 8);
    
            {
          
                LLRPUtil.BooleanToBitList(SaveConfiguration, bitList);
                
            }
        
            LLRPUtil.Int32ToBitList(0, bitList, reserved_len3);
        
            if (Custom != null)
            {
          
                int len = Custom.Length;
                for (int i = 0; i < len; i++)
                {
                    LLRPUtil.ParameterToBitList(Custom[i], bitList);
                }
            
            }
        

            UInt32 msgLength = (UInt32)(bitList.Count / 8);
            LLRPUtil.UInt32ToBitList(msgLength, bitList, 16, 32);

            return bitList;
        }

        /// <summary>
        /// Decode message from existing bit list.
        /// </summary>
        /// <param name="decoder">Binary decoder for decoding custom parameters</param>
        /// <param name="bitList">input bit list</param>
        /// <param name="cursor">pointer to current position</param>
        /// <param name="length">data length</param>
        /// <returns>MSG_IMPINJ_SAVE_SETTINGS</returns>
        public new static MSG_IMPINJ_SAVE_SETTINGS FromBitList(ICustomParameterDecoder decoder, BitList bitList, ref int cursor, int length)
        {
            if (decoder == null)
                throw new ArgumentNullException(nameof(decoder));
            if (bitList == null)
                throw new ArgumentNullException(nameof(bitList));
            if (cursor < 0)
                throw new ArgumentOutOfRangeException(nameof(cursor));
            if (length < 0)
                throw new ArgumentOutOfRangeException(nameof(length));

            if (cursor >= length) return null;

            int originalCursor = cursor;

            cursor += 6;
            UInt16 msgType = LLRPUtil.BitListToUInt16(bitList, ref cursor, 10);

            if (msgType != TYPE_ID)
            {
                cursor = originalCursor;
                return null;
            }

            UInt32 msgLength = LLRPUtil.BitListToUInt32(bitList, ref cursor, 32);
            UInt32 msgId = LLRPUtil.BitListToUInt32(bitList, ref cursor, 32);

            UInt32 vendorIdentifier = LLRPUtil.BitListToUInt32(bitList, ref cursor, 32);
            if (vendorIdentifier != VENDOR_ID)
            {
                cursor = originalCursor;
                return null;
            }

            Byte messageSubtype = LLRPUtil.BitListToByte(bitList, ref cursor, 8);
            if (messageSubtype != SUBTYPE_ID)
            {
                cursor = originalCursor;
                return null;
            }

            MSG_IMPINJ_SAVE_SETTINGS obj = new MSG_IMPINJ_SAVE_SETTINGS(msgId)
            {
                MsgLength = msgLength
            };

            int maxCursor = (Int32)(originalCursor + obj.MsgLength * 8);

            if (cursor > maxCursor)
            {
                throw new LLRPMalformedPacketException();
            }

    
            if (cursor > length || cursor > maxCursor)
            {
                throw new LLRPMalformedPacketException();
            }
            else
            {
          
                int field_len = 1;
          
                obj.SaveConfiguration = LLRPUtil.BitListToBoolean(bitList, ref cursor, field_len);
            
            }
        
            cursor += reserved_len3;
        
            {
                int temp_cursor = cursor;
                ILLRPCustomParameter custom;
                while (cursor < maxCursor &&
                    (custom = decoder.DecodeCustomParameter(bitList, ref cursor, length)) != null &&
                    cursor <= maxCursor &&
                    obj.TryAddCustomParameter(custom))
                {
                    temp_cursor = cursor;
                }
                cursor = temp_cursor;
            }
            
            return obj;
        }

    }
    
    ///<summary>
    ///
    ///This custom message is the response by the Reader to an IMPINJ_SAVE_SETTINGS message. If the Reader was capable of saving the current configuration to persistent storage, the Reader returns the success code in the LLRPStatus parameter. If there is an error, the Reader returns an appropriate error code.
    ///
    ///For more information, please refer to:
    ///
    ///</summary>
    public class MSG_IMPINJ_SAVE_SETTINGS_RESPONSE : MSG_CUSTOM_MESSAGE, ILLRPResponseMessage
    {
        public new const UInt32 VENDOR_ID = 25882;
        public new const Byte SUBTYPE_ID = 24;

        public MSG_IMPINJ_SAVE_SETTINGS_RESPONSE(UInt32 msgId) : base(msgId)
        {
            VendorIdentifier = VENDOR_ID;
            MessageSubtype = SUBTYPE_ID;
        }
      
        public PARAM_LLRPStatus LLRPStatus { get; set; }
                
        public LLRPCustomParameterList Custom { get; } = new LLRPCustomParameterList();

        public bool TryAddCustomParameter(ILLRPCustomParameter param)
        {
            if (param is IIMPINJ_SAVE_SETTINGS_RESPONSE_Custom_Param)
            {
                Custom.Add(param);
                return true;
            }

            if (param.GetType() == typeof(PARAM_Custom))
            {
                Custom.Add(param);
                return true;
            }

            return false;
        }
            

        /// <summary>
        /// Serialize message to XML
        /// </summary>
        /// <returns>XML element</returns>
        public override XElement ToXml()
        {
            XNamespace xmlnsLlrp = XNamespace.Get(LLRPConstants.NAMESPACE_URI);

            XNamespace xmlns = XNamespace.Get(ImpinjLLRPConstants.NAMESPACE_URI);
            XNamespace xsi = XNamespace.Get(LLRPConstants.NAMESPACE_SCHEMAINSTANCE);
            XNamespace schemaLocation = XNamespace.Get($"{LLRPConstants.NAMESPACE_URI} {LLRPConstants.NAMESPACE_SCHEMALOCATION} {ImpinjLLRPConstants.NAMESPACE_URI} {ImpinjLLRPConstants.NAMESPACE_SCHEMALOCATION}");
            string prefix = ImpinjLLRPConstants.NAMESPACE_PREFIX;

            XElement rootElement = new XElement(xmlns + "IMPINJ_SAVE_SETTINGS_RESPONSE",
                new XAttribute(XNamespace.Xmlns + "xsi", xsi),
                new XAttribute(xsi + "schemaLocation", schemaLocation),
                new XAttribute(XNamespace.Xmlns + prefix, xmlns),
                new XAttribute("xmlns", xmlnsLlrp),
                new XAttribute("Version", "1"),
                new XAttribute("MessageID", MsgID)
            );
    
            if (LLRPStatus != null)
            {
          
                rootElement.Add(LLRPStatus.ToXml());
                
            }
        
            if (Custom != null)
            {
          
                int len = Custom.Length;
                for (int i = 0; i < len; i++)
                {
                    rootElement.Add(Custom[i].ToXml());
                }
            
            }
        
            if (rootElement.IsEmpty)
            {
                rootElement.Value = string.Empty;
            }

            return rootElement;
        }

        /// <summary>
        /// Deserialize XML to message
        /// </summary>
        /// <param name="parser">XML parser for parsing custom parameters</param>
        /// <param name="rootElement">XML element to be deserialized</param>
        /// <returns>MSG_IMPINJ_SAVE_SETTINGS_RESPONSE</returns>
        public new static MSG_IMPINJ_SAVE_SETTINGS_RESPONSE FromXml(ICustomParameterParser parser, XElement rootElement)
        {
            if (parser == null)
                throw new ArgumentNullException(nameof(parser));
            if (rootElement == null)
                throw new ArgumentNullException(nameof(rootElement));

            UInt32 msgId = 0;
            {
                XAttribute attribute = rootElement.Attribute("MessageID");
                if (attribute != null)
                {
                    msgId = Convert.ToUInt32(attribute.Value);
                }
            }
            MSG_IMPINJ_SAVE_SETTINGS_RESPONSE obj = new MSG_IMPINJ_SAVE_SETTINGS_RESPONSE(msgId);

            var elements = rootElement.Elements().ToList();

    
            for (int i = 0; i < elements.Count; i++)
            {
                if (elements[i] == null) continue;
                if (elements[i].Name.LocalName == "LLRPStatus")
                {
                    obj.LLRPStatus = PARAM_LLRPStatus.FromXml(parser, elements[i]);
                    elements[i] = null;
                    break;
                }
            }
                
            {
                XNamespace xmlnsLlrp = XNamespace.Get(LLRPConstants.NAMESPACE_URI);
                for (int i = 0; i < elements.Count; i++)
                {
                    if (elements[i] == null) continue;
                    if (elements[i].Name.LocalName == "Custom" || elements[i].Name.Namespace != xmlnsLlrp)
                    {
                        ILLRPCustomParameter custom = parser.ParseCustomParameter(elements[i]);
                        if (custom != null)
                        {
                            if (obj.TryAddCustomParameter(custom))
                            {
                                elements[i] = null;
                            }
                        }
                    }
                }
            }
            
            elements.Clear();

            return obj;
        }

        /// <summary>
        /// Deserialize XML string to message
        /// </summary>
        /// <param name="parser">XML parser for parsing custom parameters</param>
        /// <param name="xmlString">XML string to be deserialized</param>
        /// <returns>PARAM_IMPINJ_SAVE_SETTINGS_RESPONSE</returns>
        public new static MSG_IMPINJ_SAVE_SETTINGS_RESPONSE FromXmlString(ICustomParameterParser parser, string xmlString)
        {
            XDocument xDoc = XDocument.Load(xmlString);
            return FromXml(parser, xDoc.Root);
        }

        /// <summary>
        /// Encode message to bit list
        /// </summary>
        /// <returns>bit list</returns>
        public override BitList ToBitList()
        {
            BitList bitList = new BitList(10000);
            LLRPUtil.UInt16ToBitList(0, bitList, 3);
            LLRPUtil.UInt16ToBitList(MsgVersion, bitList, 3);
            LLRPUtil.UInt16ToBitList(MsgTypeID, bitList, 10);
            LLRPUtil.UInt32ToBitList(MsgLength, bitList, 32);
            LLRPUtil.UInt32ToBitList(MsgID, bitList, 32);
            LLRPUtil.UInt32ToBitList(VendorIdentifier, bitList, 32);
            LLRPUtil.ByteToBitList(MessageSubtype, bitList, 8);
    
            if (LLRPStatus != null)
            {
          
                LLRPUtil.ParameterToBitList(LLRPStatus, bitList);
            
            }
        
            if (Custom != null)
            {
          
                int len = Custom.Length;
                for (int i = 0; i < len; i++)
                {
                    LLRPUtil.ParameterToBitList(Custom[i], bitList);
                }
            
            }
        

            UInt32 msgLength = (UInt32)(bitList.Count / 8);
            LLRPUtil.UInt32ToBitList(msgLength, bitList, 16, 32);

            return bitList;
        }

        /// <summary>
        /// Decode message from existing bit list.
        /// </summary>
        /// <param name="decoder">Binary decoder for decoding custom parameters</param>
        /// <param name="bitList">input bit list</param>
        /// <param name="cursor">pointer to current position</param>
        /// <param name="length">data length</param>
        /// <returns>MSG_IMPINJ_SAVE_SETTINGS_RESPONSE</returns>
        public new static MSG_IMPINJ_SAVE_SETTINGS_RESPONSE FromBitList(ICustomParameterDecoder decoder, BitList bitList, ref int cursor, int length)
        {
            if (decoder == null)
                throw new ArgumentNullException(nameof(decoder));
            if (bitList == null)
                throw new ArgumentNullException(nameof(bitList));
            if (cursor < 0)
                throw new ArgumentOutOfRangeException(nameof(cursor));
            if (length < 0)
                throw new ArgumentOutOfRangeException(nameof(length));

            if (cursor >= length) return null;

            int originalCursor = cursor;

            cursor += 6;
            UInt16 msgType = LLRPUtil.BitListToUInt16(bitList, ref cursor, 10);

            if (msgType != TYPE_ID)
            {
                cursor = originalCursor;
                return null;
            }

            UInt32 msgLength = LLRPUtil.BitListToUInt32(bitList, ref cursor, 32);
            UInt32 msgId = LLRPUtil.BitListToUInt32(bitList, ref cursor, 32);

            UInt32 vendorIdentifier = LLRPUtil.BitListToUInt32(bitList, ref cursor, 32);
            if (vendorIdentifier != VENDOR_ID)
            {
                cursor = originalCursor;
                return null;
            }

            Byte messageSubtype = LLRPUtil.BitListToByte(bitList, ref cursor, 8);
            if (messageSubtype != SUBTYPE_ID)
            {
                cursor = originalCursor;
                return null;
            }

            MSG_IMPINJ_SAVE_SETTINGS_RESPONSE obj = new MSG_IMPINJ_SAVE_SETTINGS_RESPONSE(msgId)
            {
                MsgLength = msgLength
            };

            int maxCursor = (Int32)(originalCursor + obj.MsgLength * 8);

            if (cursor > maxCursor)
            {
                throw new LLRPMalformedPacketException();
            }

    
            obj.LLRPStatus = PARAM_LLRPStatus.FromBitList(decoder, bitList, ref cursor, length);
              
            {
                int temp_cursor = cursor;
                ILLRPCustomParameter custom;
                while (cursor < maxCursor &&
                    (custom = decoder.DecodeCustomParameter(bitList, ref cursor, length)) != null &&
                    cursor <= maxCursor &&
                    obj.TryAddCustomParameter(custom))
                {
                    temp_cursor = cursor;
                }
                cursor = temp_cursor;
            }
            
            return obj;
        }

    }
    
}
  