
namespace EA.Impinj.LLRP.Impinj
{
    public class ImpinjLLRPConstants
    {
        public const string NAMESPACE_PREFIX = "Impinj";
        public const string NAMESPACE_URI = "http://developer.impinj.com/ltk/schema/encoding/xml/1.38";
        public const string NAMESPACE_SCHEMALOCATION = "http://developer.impinj.com/ltk/schema/encoding/xml/1.38/impinj.xsd";
        public const string VENDOR_NAME = "Impinj";
        public const string VENDOR_ID = "25882";
    }
}
  