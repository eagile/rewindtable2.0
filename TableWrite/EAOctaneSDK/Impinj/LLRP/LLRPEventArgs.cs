﻿using System;

namespace EA.Impinj.LLRP
{
    public class ListenerChangedEventArgs : EventArgs
    {
        public DateTime Timestamp { get; set; }
        public bool IsListening { get; set; }
        public Exception Error { get; set; }

        public ListenerChangedEventArgs(DateTime timestamp, bool isListening, Exception error)
        {
            Timestamp = timestamp;
            IsListening = isListening;
            Error = error;
        }
    }

    public class ConnectionChangedEventArgs : EventArgs
    {
        public DateTime Timestamp { get; set; }
        public bool IsConnected { get; set; }
        public Exception Error { get; set; }

        public ConnectionChangedEventArgs(DateTime timestamp, bool isConnected, Exception error)
        {
            Timestamp = timestamp;
            IsConnected = isConnected;
            Error = error;
        }
    }

    public class ClientRejectedEventArgs : EventArgs
    {
        public DateTime Timestamp { get; set; }
        public string RemoteHost { get; set; }
        public Exception Error { get; set; }

        public ClientRejectedEventArgs(DateTime timestamp, string remoteHost, Exception error)
        {
            Timestamp = timestamp;
            RemoteHost = remoteHost;
            Error = error;
        }
    }

    public class MessageReceivedEventArgs : EventArgs
    {
        public ILLRPMessage Message { get; set; }

        public MessageReceivedEventArgs(ILLRPMessage message)
        {
            Message = message;
        }
    }
}