﻿using System;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace EA.Impinj.LLRP
{
    public partial class LLRPClient : LLRPStream
    {
        public event EventHandler<ConnectionChangedEventArgs> ConnectionChanged;
        public event EventHandler<MessageReceivedEventArgs> MessageReceived;

        protected virtual void OnConnectionChanged(bool isConnected, Exception exception = null)
        {
            try { ConnectionChanged?.Invoke(this, new ConnectionChangedEventArgs(DateTime.UtcNow, isConnected, exception)); } catch { }
        }

        protected virtual void OnMessageReceived(ILLRPMessage message)
        {
            try { MessageReceived?.Invoke(this, new MessageReceivedEventArgs(message)); } catch { }
        }

        public override bool IsConnected => IsStreamConnected && _initialized;
        protected override bool IsStreamConnected => base.IsStreamConnected && _connected;

        public string Address { get; set; } = null;
        public int Port { get; set; } = LLRPConstants.LLRP_PORT;
        public bool UseTLS { get; set; } = false;

        public int ConnectTimeout { get; set; } = LLRPConstants.DEFAULT_TIME_OUT;
        public int ResponseTimeout { get; set; } = LLRPConstants.DEFAULT_TIME_OUT;
        public int CloseTimeout { get; set; } = LLRPConstants.DEFAULT_TIME_OUT;

        private bool _opened = false;
        private bool _connected = false;
        private bool _initialized = false;
        private bool _closed = true;

        private readonly SemaphoreSlim _clientLock = new SemaphoreSlim(1, 1);
        private readonly object _statusLock = new object();

        protected TcpClient TcpClient { get; private set; }

        private TaskCompletionSource<ENUM_ConnectionAttemptStatusType> _connectionTcs = new TaskCompletionSource<ENUM_ConnectionAttemptStatusType>(TaskCreationOptions.RunContinuationsAsynchronously);

        private readonly Timer _keepaliveTimer;
        private bool _keepaliveEnabled = false;
        private int _keepaliveInterval = -1;

        public LLRPClient() : base()
        {
            StreamCts.Cancel();
            _connectionTcs.TrySetCanceled();
            _keepaliveTimer = new Timer(KeepaliveTimer_Elapsed, null, -1, -1);
        }

        public Task ConnectAsync() => ConnectAsync(Address, Port, UseTLS, ConnectTimeout);
        public Task ConnectAsync(string address, int port, bool useTls) => ConnectAsync(address, port, useTls, ConnectTimeout);
        public async Task ConnectAsync(string address, int port, bool useTls, int timeoutMilliseconds)
        {
            if (string.IsNullOrWhiteSpace(address))
                throw new ArgumentNullException(nameof(address));
            if (port <= 0)
                throw new ArgumentOutOfRangeException(nameof(port));

            await _clientLock.WaitAsync();
            try
            {
                lock (_statusLock)
                {
                    if (_connected || !_closed)
                        throw new InvalidOperationException("Already connected");
                    if (_opened)
                        throw new InvalidOperationException("Already connecting");
                    _opened = true;
                    _closed = false;
                }

                Address = address;
                Port = port;
                UseTLS = useTls;

                try
                {
                    StreamCts = new CancellationTokenSource();
                    _connectionTcs = new TaskCompletionSource<ENUM_ConnectionAttemptStatusType>(TaskCreationOptions.RunContinuationsAsynchronously);
                    Task openTask = OpenStreamAsync();
                    TaskCompletionSource<bool> cancelTcs = new TaskCompletionSource<bool>(TaskCreationOptions.RunContinuationsAsynchronously);
                    using (CancellationTokenSource timeoutCts = new CancellationTokenSource(timeoutMilliseconds))
                    using (StreamCt.Register(() => cancelTcs.TrySetException(new LLRPNetworkException("Connection closed")), false))
                    using (timeoutCts.Token.Register(() => cancelTcs.TrySetException(new LLRPTimeoutException("Connection timed out")), false))
                    {
                        await Task.WhenAny(openTask, cancelTcs.Task).ConfigureAwait(false);
                        cancelTcs.TrySetResult(true);
                        if (cancelTcs.Task.IsFaulted)
                        {
                            _connectionTcs.TrySetException(cancelTcs.Task.Exception.InnerException);
                            try { Stream?.Dispose(); } catch { }
                            try { TcpClient?.GetStream()?.Dispose(); } catch { }
                            try { TcpClient?.Dispose(); } catch { }
                        }
                        else if (openTask.IsFaulted)
                        {
                            _connectionTcs.TrySetException(openTask.Exception.InnerException);
                        }
                    }

                    await Task.WhenAll(_connectionTcs.Task, cancelTcs.Task, openTask).ConfigureAwait(false);

                    lock (_statusLock)
                    {
                        if (!_opened || _closed)
                            throw new InvalidOperationException("Connection closed");
                        _connected = true;
                    }

                    await InitializeStreamAsync().ConfigureAwait(false);

                    lock (_statusLock)
                    {
                        if (!_opened || !_connected || _closed)
                            throw new InvalidOperationException("Connection closed");
                        _initialized = true;
                        if (_keepaliveEnabled)
                        {
                            _keepaliveTimer.Change(_keepaliveInterval, -1);
                        }
                    }

                    OnConnectionChanged(true);
                }
                catch (LLRPException lex)
                {
                    await CloseStreamAsync(lex, CloseTimeout).ConfigureAwait(false);
                    throw;
                }
            }
            finally
            {
                _clientLock.Release();
            }
        }

        private async Task OpenStreamAsync()
        {
            IPAddress[] hostAddresses = await Dns.GetHostAddressesAsync(Address).ConfigureAwait(false);

            TcpClient = new TcpClient(AddressFamily.InterNetworkV6);
            TcpClient.Client.DualMode = true;
            TcpClient.Client.NoDelay = true;

            await TcpClient.ConnectAsync(hostAddresses, Port).ConfigureAwait(false);

            if (UseTLS)
            {
                Stream = await SecureStreamAsync(TcpClient.GetStream()).ConfigureAwait(false);
            }
            else
            {
                Stream = TcpClient.GetStream();
            }

            await StartProcessingStream(BufferSize, PendingRequestLimit).ConfigureAwait(false);

            await _connectionTcs.Task.ConfigureAwait(false);
        }

        private async Task<Stream> SecureStreamAsync(Stream stream)
        {
            SslStream sslStream = null;
            try
            {
                sslStream = new SslStream(stream, false, (sender, certificate, chain, sslPolicyErrors) => { return true; });
                await sslStream.AuthenticateAsClientAsync(Address).ConfigureAwait(false);
                return sslStream;
            }
            catch
            {
                try { sslStream?.Dispose(); } catch { }
                throw;
            }
        }

        protected virtual async Task InitializeStreamAsync()
        {
            ENUM_ConnectionAttemptStatusType status = await _connectionTcs.Task.ConfigureAwait(false);

            if (status != ENUM_ConnectionAttemptStatusType.Success)
                throw new LLRPConnectionException(status);
        }

        public Task<bool> DisconnectAsync() => DisconnectAsync(null, CloseTimeout);
        public Task<bool> DisconnectAsync(int timeoutMilliseconds) => DisconnectAsync(null, timeoutMilliseconds);
        public Task<bool> DisconnectAsync(Exception exception) => DisconnectAsync(exception, CloseTimeout);
        public async Task<bool> DisconnectAsync(Exception exception, int timeoutMilliseconds)
        {
            await _clientLock.WaitAsync().ConfigureAwait(false);
            try
            {
                return await CloseStreamAsync(exception, timeoutMilliseconds).ConfigureAwait(false);
            }
            finally
            {
                _clientLock.Release();
            }
        }

        protected async Task<bool> CloseStreamAsync(Exception exception, int timeoutMilliseconds)
        {
            bool initialized;
            lock (_statusLock)
            {
                if (_closed) return false;
                _closed = true;

                initialized = _initialized;
                _connected = false;
                _initialized = false;

                _keepaliveTimer.Change(-1, -1);
            }

            _connectionTcs.TrySetException(exception ?? new LLRPNetworkException("Connection closed"));

            try { await FinalizeStreamAsync().ConfigureAwait(false); } catch { }

            try
            {
                if (TcpClient?.Connected == true)
                {
                    await CLOSE_CONNECTION(new MSG_CLOSE_CONNECTION(GetNextMessageID()), timeoutMilliseconds).ConfigureAwait(false);
                }
            }
            catch { }

            StreamCts.Cancel();
            try { Stream?.Dispose(); } catch { }
            Stream = null;
            try { TcpClient?.GetStream()?.Dispose(); } catch { }
            try { TcpClient?.Dispose(); } catch { }
            TcpClient = null;

            try { await WaitForStreamProcessingToCompleteAsync().ConfigureAwait(false); } catch { }

            lock (_statusLock)
            {
                _opened = false;
            }

            if (initialized)
            {
                OnConnectionChanged(false, exception);
                return true;
            }
            return false;
        }

        protected virtual Task FinalizeStreamAsync() => Task.CompletedTask;



        protected override async Task ProcessMessageAsync(ILLRPMessage message)
        {
            await base.ProcessMessageAsync(message).ConfigureAwait(false);
            if (!_connectionTcs.Task.IsCompleted
                && message is MSG_READER_EVENT_NOTIFICATION readerEventNotification
                && readerEventNotification.ReaderEventNotificationData?.ConnectionAttemptEvent != null)
            {
                _connectionTcs.TrySetResult(readerEventNotification.ReaderEventNotificationData.ConnectionAttemptEvent.Status);
            }
            if (message is MSG_KEEPALIVE keepAlive)
            {
                lock (_statusLock)
                {
                    _keepaliveTimer.Change(-1, -1);
                }
                MSG_KEEPALIVE_ACK msgAck = new MSG_KEEPALIVE_ACK(keepAlive.MsgID);
                try
                {
                    await SendAsync(msgAck).ConfigureAwait(false);
                }
                catch { }

                lock (_statusLock)
                {
                    if (_keepaliveEnabled)
                    {
                        _keepaliveTimer.Change(_keepaliveInterval, -1);
                    }
                }
            }
            OnMessageReceived(message);
        }

        public void EnableKeepAlives(int timeoutMilliseconds)
        {
            lock (_statusLock)
            {
                _keepaliveTimer.Change(-1, -1);
                _keepaliveEnabled = true;
                _keepaliveInterval = timeoutMilliseconds;
                if (_connected)
                {
                    _keepaliveTimer.Change(_keepaliveInterval, -1);
                }
            }
        }

        public void DisableKeepAlives()
        {
            lock (_statusLock)
            {
                _keepaliveEnabled = false;
                _keepaliveTimer.Change(-1, -1);
            }
        }

        public async void KeepaliveTimer_Elapsed(object stateInfo)
        {
            await CloseStreamAsync(new LLRPTimeoutException("Keep alive timed out"), CloseTimeout).ConfigureAwait(false);
        }

        protected override async Task OnStreamErrorAsync(Exception exception)
        {
            await CloseStreamAsync(exception, CloseTimeout).ConfigureAwait(false);
        }

        #region Dispose

        bool disposed = false;
        protected override void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                StreamCts.Cancel();
                try { Stream?.Dispose(); } catch { }
                Stream = null;
                try { TcpClient?.GetStream()?.Dispose(); } catch { }
                try { TcpClient?.Dispose(); } catch { }
                TcpClient = null;
            }

            // Free any unmanaged objects here.

            disposed = true;
            base.Dispose(disposing);
        }

        #endregion Dispose
    }
}
