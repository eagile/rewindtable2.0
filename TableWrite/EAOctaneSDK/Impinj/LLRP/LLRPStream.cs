﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace EA.Impinj.LLRP
{
    public abstract class LLRPStream : IDisposable
    {
        public virtual bool IsConnected => IsStreamConnected;
        protected virtual bool IsStreamConnected => Stream != null && !StreamCt.IsCancellationRequested;

        public LLRPBinaryDecoder MessageDecoder { get; protected set; } = new LLRPBinaryDecoder();

        public int BufferSize { get; set; } = LLRPConstants.DEFAULT_BUFFER_SIZE;

        /// <summary>
        /// Maximum number of pending requests at a time
        /// </summary>
        public int PendingRequestLimit { get; set; } = LLRPConstants.DEFAULT_PENDING_REQUEST_LIMIT;

        protected Stream Stream { get; set; }

        protected CancellationTokenSource StreamCts { get; set; } = new CancellationTokenSource();
        protected CancellationToken StreamCt => StreamCts.Token;

        private readonly ConcurrentDictionary<TransactionResponseKey, TaskCompletionSource<ILLRPMessage>> _transactions = new ConcurrentDictionary<TransactionResponseKey, TaskCompletionSource<ILLRPMessage>>();

        private Channel<SendChannelItem> _sendChannel;
        private Channel<ReceiveChannelItem> _receiveChannel;

        private SemaphoreSlim _pendingRequestLimitLock = null;

        private SemaphoreSlim _streamProcessingLock = new SemaphoreSlim(1,1);

        private Task _receiveTask = Task.CompletedTask;
        private Task _receiveTaskWrapper = Task.CompletedTask;

        private Task _sendChannelTask = Task.CompletedTask;
        private Task _sendChannelTaskWrapper = Task.CompletedTask;

        private Task _receiveChannelTask = Task.CompletedTask;
        private Task _receiveChannelTaskWrapper = Task.CompletedTask;



        private int _messageIdSequence = 0;
        protected UInt32 GetNextMessageID()
        {
            UInt32 nextId;
            do
            {
                nextId = unchecked((UInt32)Interlocked.Increment(ref _messageIdSequence));
            } while (nextId == 0);
            return nextId;
        }


        public LLRPStream()
        {
        }

        public async Task SendAsync(ILLRPMessage message)
        {
            if (!IsStreamConnected)
                throw new InvalidOperationException("Connection closed");

            TaskCompletionSource<bool> sendTcs = new TaskCompletionSource<bool>(TaskCreationOptions.RunContinuationsAsynchronously);
            TaskCompletionSource<bool> lockTcs = null;
            SemaphoreSlim pendingRequestLimitLock = _pendingRequestLimitLock;

            try
            {
                byte[] messageBytes = LLRPUtil.BitsToBytes(message.ToBitList());

                if (pendingRequestLimitLock != null)
                {
                    lockTcs = new TaskCompletionSource<bool>(TaskCreationOptions.RunContinuationsAsynchronously);
                }

                try
                {
                    if (!_sendChannel.Writer.TryWrite(new SendChannelItem()
                    {
                        MessageBytes = messageBytes,
                        SendTcs = sendTcs,
                        LockTcs = lockTcs,
                        PendingRequestLimitLock = pendingRequestLimitLock
                    }))
                    {
                        throw new InvalidOperationException("Connection closed");
                    }
                }
                catch (Exception)
                {
                    lockTcs?.TrySetResult(false);
                    throw;
                }

                await sendTcs.Task.ConfigureAwait(false);

            }
            catch (Exception ex)
            {
                sendTcs.TrySetException(ex);
                await OnStreamErrorAsync(ex).ConfigureAwait(false);
                throw;
            }
            finally
            {
                sendTcs.TrySetCanceled();
                if (pendingRequestLimitLock != null)
                {
                    if (await lockTcs.Task.ConfigureAwait(false))
                    {
                        pendingRequestLimitLock.Release();
                    }
                }
            }
        }

        public async Task<ILLRPMessage> TransactAsync(ILLRPMessage message, UInt16 responseTypeID, UInt32 responseVendorID, Byte responseSubTypeID, int timeout)
        {
            if (!IsStreamConnected)
                throw new InvalidOperationException("Connection closed");

            TaskCompletionSource<bool> sendTcs = new TaskCompletionSource<bool>(TaskCreationOptions.RunContinuationsAsynchronously);
            TaskCompletionSource<ILLRPMessage> transTcs = new TaskCompletionSource<ILLRPMessage>(TaskCreationOptions.RunContinuationsAsynchronously);
            TaskCompletionSource<bool> lockTcs = null;
            SemaphoreSlim pendingRequestLimitLock = _pendingRequestLimitLock;

            TransactionResponseKey transactionResponseKey = new TransactionResponseKey(message.MsgID, responseTypeID, responseVendorID, responseSubTypeID);

            try
            {
                _transactions.TryAdd(transactionResponseKey, transTcs);
                using (CancellationTokenSource timeoutCts = new CancellationTokenSource(timeout))
                using (timeoutCts.Token.Register(() => { transTcs.TrySetException(new LLRPTimeoutException("Transaction timed out")); }, false))
                using (StreamCt.Register(() => { transTcs.TrySetException(new InvalidOperationException("Connection closed")); }, false))
                {
                    byte[] messageBytes = LLRPUtil.BitsToBytes(message.ToBitList());

                    if (pendingRequestLimitLock != null)
                    {
                        lockTcs = new TaskCompletionSource<bool>(TaskCreationOptions.RunContinuationsAsynchronously);
                    }

                    try
                    {
                        if (!_sendChannel.Writer.TryWrite(new SendChannelItem()
                        {
                            MessageBytes = messageBytes,
                            SendTcs = sendTcs,
                            LockTcs = lockTcs,
                            PendingRequestLimitLock = pendingRequestLimitLock
                        }))
                        {
                            throw new LLRPNetworkException("Connection closed");
                        }
                    }
                    catch (Exception)
                    {
                        lockTcs?.TrySetResult(false);
                        throw;
                    }

                    await sendTcs.Task.ConfigureAwait(false);
                    return await transTcs.Task.ConfigureAwait(false);
                }
            }
            catch (LLRPMessageException mex)
            {
                sendTcs.TrySetException(mex);
                transTcs.TrySetException(mex);
                throw;
            }
            catch (Exception ex)
            {
                sendTcs.TrySetException(ex);
                transTcs.TrySetException(ex);
                await OnStreamErrorAsync(ex).ConfigureAwait(false);
                throw;
            }
            finally
            {
                _transactions.TryRemove(transactionResponseKey, out _);
                sendTcs.TrySetCanceled();
                transTcs.TrySetCanceled();
                if (pendingRequestLimitLock != null)
                {
                    if (await lockTcs.Task.ConfigureAwait(false))
                    {
                        pendingRequestLimitLock.Release();
                    }
                }
            }
        }

        protected async Task StartProcessingStream(int bufferSize, int pendingRequestLimit)
        {
            try
            {
                if (bufferSize <= 0)
                    throw new ArgumentOutOfRangeException(nameof(bufferSize));
                if (pendingRequestLimit < -1 || pendingRequestLimit == 0)
                    throw new ArgumentOutOfRangeException(nameof(pendingRequestLimit));

                await _streamProcessingLock.WaitAsync().ConfigureAwait(false);
                try
                {
                    if (Stream == null || StreamCt.IsCancellationRequested)
                        throw new InvalidOperationException("Connection closed");

                    if (!_sendChannelTask.IsCompleted
                        || !_receiveChannelTask.IsCompleted
                        || !_receiveTask.IsCompleted)
                        throw new InvalidOperationException("Already processing");

                    await Task.WhenAll(_sendChannelTaskWrapper, _receiveTaskWrapper, _receiveChannelTaskWrapper).ConfigureAwait(false);

                    BufferSize = bufferSize;
                    PendingRequestLimit = pendingRequestLimit;

                    if (pendingRequestLimit > 0)
                    {
                        _pendingRequestLimitLock = new SemaphoreSlim(pendingRequestLimit, pendingRequestLimit);
                    }
                    else
                    {
                        _pendingRequestLimitLock = null;
                    }

                    _sendChannel = Channel.CreateUnbounded<SendChannelItem>(new UnboundedChannelOptions() { AllowSynchronousContinuations = true, SingleReader = true, SingleWriter = false, });
                    _receiveChannel = Channel.CreateUnbounded<ReceiveChannelItem>(new UnboundedChannelOptions() { AllowSynchronousContinuations = false, SingleReader = true, SingleWriter = true, });

                    _sendChannelTask = Task.Run(() => ProcessSendChannelAsync(_sendChannel, Stream, StreamCt), StreamCt);
                    _sendChannelTaskWrapper = StreamErrorTaskWrapper(_sendChannelTask);

                    _receiveTask = Task.Run(() => ReceiveAsync(_receiveChannel, Stream, bufferSize, StreamCt), StreamCt);
                    _receiveTaskWrapper = StreamErrorTaskWrapper(_receiveTask);

                    _receiveChannelTask = Task.Run(() => ProcessReceiveChannelAsync(_receiveChannel), StreamCt);
                    _receiveChannelTaskWrapper = StreamErrorTaskWrapper(_receiveChannelTask);
                }
                finally
                {
                    _streamProcessingLock.Release();
                }
            }
            catch (Exception ex)
            {
                await OnStreamErrorAsync(ex).ConfigureAwait(false);
            }
        }

        protected async Task WaitForStreamProcessingToCompleteAsync()
        {
            await _streamProcessingLock.WaitAsync();
            try
            {
                if (Stream != null && !StreamCt.IsCancellationRequested)
                    throw new InvalidOperationException("Connection opened");

                _sendChannel?.Writer.TryComplete();
                try { await _sendChannelTask.ConfigureAwait(false); } catch { }
                try { await _receiveTask.ConfigureAwait(false); } catch { }
                _receiveChannel?.Writer.TryComplete();
                try { await _receiveChannelTask.ConfigureAwait(false); } catch { }
            }
            finally
            {
                _streamProcessingLock.Release();
            }
        }

        private async Task ProcessSendChannelAsync(Channel<SendChannelItem> channel, Stream stream, CancellationToken ct)
        {
            using (ct.Register(() => { try { channel.Writer.TryComplete(); } catch { } }, false))
            {
                while (await channel.Reader.WaitToReadAsync().ConfigureAwait(false))
                {
                    while (channel.Reader.TryRead(out SendChannelItem sendChannelItem))
                    {
                        byte[] bytes = sendChannelItem.MessageBytes;
                        TaskCompletionSource<bool> sendTcs = sendChannelItem.SendTcs;
                        TaskCompletionSource<bool> lockTcs = sendChannelItem.LockTcs;
                        SemaphoreSlim pendingRequestLimitLock = sendChannelItem.PendingRequestLimitLock;

                        try
                        {
                            if (pendingRequestLimitLock != null)
                            {
                                await pendingRequestLimitLock.WaitAsync(ct).ConfigureAwait(false);
                                lockTcs?.TrySetResult(true);
                            }

                            await stream.WriteAsync(bytes, 0, bytes.Length, ct).ConfigureAwait(false);

                            sendTcs?.TrySetResult(true);
                        }
                        catch (Exception ex)
                        {
                            sendTcs?.TrySetException(ex);
                        }
                        finally
                        {
                            lockTcs?.TrySetResult(false);
                        }
                    }
                }
            }
        }

        private async Task ReceiveAsync(Channel<ReceiveChannelItem> channel, Stream stream, int bufferSize, CancellationToken ct)
        {
            try
            {
                byte[] bufferBytes = new byte[bufferSize];
                int bufferCursor = 0;
                int bufferCount = 0;

                byte[] headerBytes = new byte[LLRPConstants.HEADER_SIZE];
                int headerCursor = 0;

                byte[] messageBytes = null;
                int messageCursor = 0;

                DateTime msgReceivedTimestamp = DateTime.MinValue;

                int expectedCount = 0;

                while (!ct.IsCancellationRequested)
                {
                    if (headerCursor < headerBytes.Length)
                    {
                        expectedCount = Math.Min(bufferSize, headerBytes.Length - headerCursor);
                    }
                    else
                    {
                        expectedCount = Math.Min(bufferSize, messageBytes.Length - messageCursor);
                    }

                    bufferCursor = 0;
                    bufferCount = await stream.ReadAsync(bufferBytes, 0, expectedCount, ct).ConfigureAwait(false);

                    if (bufferCount == 0)
                    {
                        throw new LLRPNetworkException("Connection lost");
                    }

                    while (bufferCount > 0)
                    {
                        if (headerCursor < headerBytes.Length)
                        {
                            int headerCount = Math.Min(bufferCount, headerBytes.Length - headerCursor);
                            Array.Copy(bufferBytes, bufferCursor, headerBytes, headerCursor, headerCount);
                            headerCursor += headerCount;
                            bufferCursor += headerCount;
                            bufferCount -= headerCount;
                            if (headerCursor == headerBytes.Length)
                            {
                                msgReceivedTimestamp = DateTime.UtcNow;

                                UInt16 msgHeader = (UInt16)((headerBytes[0] << 8) + headerBytes[1]);
                                UInt16 msgVersion = (UInt16)((msgHeader >> 10) & 0x07);
                                UInt16 msgType = (UInt16)(msgHeader & 0x03FF);
                                UInt32 msgLength = (UInt32)((headerBytes[2] << 24) + (headerBytes[3] << 16) + (headerBytes[4] << 8) + headerBytes[5]);

                                if (msgVersion != 1 && msgVersion != 2) throw new LLRPMalformedPacketException("Invalid LLRP message version: " + msgVersion);
                                if (!Enum.IsDefined(typeof(ENUM_LLRP_MSG_TYPE), (int)msgType)) throw new LLRPMalformedPacketException("Invalid LLRP message type: " + msgType);
                                if (msgLength < 10 || msgLength > 200000000) throw new LLRPMalformedPacketException("Invalid LLRP message length: " + msgLength);

                                messageBytes = new byte[msgLength];
                                Array.Copy(headerBytes, 0, messageBytes, 0, headerBytes.Length);
                                messageCursor += headerBytes.Length;
                            }
                        }

                        if (messageBytes != null && messageCursor <= messageBytes.Length)
                        {
                            int messageCount = Math.Min(bufferCount, messageBytes.Length - messageCursor);
                            Array.Copy(bufferBytes, bufferCursor, messageBytes, messageCursor, messageCount);
                            messageCursor += messageCount;
                            bufferCursor += messageCount;
                            bufferCount -= messageCount;
                            if (messageCursor == messageBytes.Length)
                            {
                                if (!channel.Writer.TryWrite(new ReceiveChannelItem()
                                {
                                    MessageBytes = messageBytes,
                                    ReceivedTimestamp = msgReceivedTimestamp
                                }))
                                {
                                    throw new InvalidOperationException("Connection closed");
                                }

                                messageBytes = null;
                                headerCursor = 0;
                                messageCursor = 0;
                            }
                        }
                    }
                }
            }
            finally
            {
                try { channel.Writer.TryComplete(); } catch { }
            }
        }

        private async Task ProcessReceiveChannelAsync(Channel<ReceiveChannelItem> channel)
        {
            try
            {
                while (await channel.Reader.WaitToReadAsync().ConfigureAwait(false))
                {
                    while (channel.Reader.TryRead(out ReceiveChannelItem receiveChannelItem))
                    {
                        ILLRPMessage message = MessageDecoder.Decode(receiveChannelItem.MessageBytes);
                        message.MsgReceivedTimestamp = receiveChannelItem.ReceivedTimestamp;

                        TransactionResponseKey transactionResponseKey;
                        if (message is MSG_CUSTOM_MESSAGE customMessage)
                        {
                            transactionResponseKey = new TransactionResponseKey(customMessage.MsgID, customMessage.MsgTypeID, customMessage.VendorIdentifier, customMessage.MessageSubtype);
                        }
                        else
                        {
                            transactionResponseKey = new TransactionResponseKey(message.MsgID, message.MsgTypeID, 0, 0);
                        }

                        if (_transactions.TryRemove(transactionResponseKey, out TaskCompletionSource<ILLRPMessage> transTcs))
                        {
                            if (message is ILLRPResponseMessage response
                                && response.LLRPStatus.StatusCode != ENUM_StatusCode.M_Success
                                && !transTcs.Task.IsCompleted)
                            {
                                transTcs.TrySetException(new LLRPMessageException(response));
                            }
                            transTcs.TrySetResult(message);
                        }
                        else if (message is MSG_ERROR_MESSAGE errorMsg)
                        {
                            foreach (var key in _transactions.Keys.ToList())
                            {
                                if (_transactions.TryRemove(key, out TaskCompletionSource<ILLRPMessage> transErrorTcs)
                                    && !transErrorTcs.Task.IsCompleted)
                                {
                                    transErrorTcs.TrySetException(new LLRPMessageException(errorMsg));
                                }
                            }
                        }

                        await ProcessMessageAsync(message).ConfigureAwait(false);
                    }
                }
            }
            finally
            {
                await _sendChannel.Reader.Completion.ConfigureAwait(false);
                foreach (var key in _transactions.Keys.ToList())
                {
                    if (_transactions.TryRemove(key, out TaskCompletionSource<ILLRPMessage> transTcs)
                        && !transTcs.Task.IsCompleted)
                    {
                        transTcs.TrySetException(new LLRPNetworkException("Connection closed"));
                    }
                }
            }
        }

        protected virtual Task ProcessMessageAsync(ILLRPMessage message) => Task.CompletedTask;

        private async Task StreamErrorTaskWrapper(Task streamTask)
        {
            try
            {
                await streamTask.ConfigureAwait(false);
            }
            catch (OperationCanceledException) { }
            catch (Exception ex)
            {
                await OnStreamErrorAsync(ex).ConfigureAwait(false);
            }
        }

        protected abstract Task OnStreamErrorAsync(Exception exception);

        private struct TransactionResponseKey : IEquatable<TransactionResponseKey>, IEqualityComparer<TransactionResponseKey>
        {
            public readonly UInt32 MsgID;
            public readonly UInt16 TypeID;
            public readonly UInt32 VendorID;
            public readonly Byte SubTypeID;

            public TransactionResponseKey(UInt32 msgID, UInt16 typeID, UInt32 vendorID, Byte subTypeID)
            {
                MsgID = msgID;
                TypeID = typeID;
                VendorID = vendorID;
                SubTypeID = subTypeID;
            }

            public override bool Equals(object obj)
            {
                return obj is TransactionResponseKey key && Equals(key);
            }

            public bool Equals(TransactionResponseKey other)
            {
                return MsgID == other.MsgID &&
                       TypeID == other.TypeID &&
                       VendorID == other.VendorID &&
                       SubTypeID == other.SubTypeID;
            }

            public bool Equals(TransactionResponseKey x, TransactionResponseKey y)
            {
                return x.Equals(y);
            }

            public int GetHashCode(TransactionResponseKey obj)
            {
                return obj.GetHashCode();
            }

            public override int GetHashCode()
            {
                int hashCode = 849591013;
                hashCode = hashCode * -1521134295 + MsgID.GetHashCode();
                hashCode = hashCode * -1521134295 + TypeID.GetHashCode();
                hashCode = hashCode * -1521134295 + VendorID.GetHashCode();
                hashCode = hashCode * -1521134295 + SubTypeID.GetHashCode();
                return hashCode;
            }
        }

        private class SendChannelItem
        {
            public byte[] MessageBytes { get; set; }
            public TaskCompletionSource<bool> SendTcs { get; set; }
            public TaskCompletionSource<bool> LockTcs { get; set; }
            public SemaphoreSlim PendingRequestLimitLock { get; set; }
        }

        private class ReceiveChannelItem
        {
            public byte[] MessageBytes { get; set; }
            public DateTime ReceivedTimestamp { get; set; }
        }

        #region Dispose

        private bool disposed = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                // Free any managed objects here.
                StreamCts.Cancel();
                try { Stream?.Dispose(); } catch { }
                Stream = null;
            }

            // Free any unmanaged objects here

            disposed = true;
        }
        ~LLRPStream()
        {
            Dispose(false);
        }

        #endregion Dispose
    }
}
