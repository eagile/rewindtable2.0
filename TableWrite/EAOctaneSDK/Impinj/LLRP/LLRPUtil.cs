/*
 ***************************************************************************
 *  Copyright 2007 Impinj, Inc.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 ***************************************************************************
 */


/*
***************************************************************************
 * File Name:       LLRPUtil.cs
 *
 * Author:          Impinj
 * Organization:    Impinj
 * Date:            June, 2008
 *
 * Description:     This file contains data convertion and data operattion
 *                  methods
***************************************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EA.Impinj.LLRP
{
    /// <summary>
    /// Utility class contains data type conversion functions
    /// </summary>
    public static class LLRPUtil
    {
        private const int BITS_PER_HEX = 4;
        private const int BITS_PER_BYTE = 8;

        private const int HEX_PER_BYTE = 2;
        private const int HEX_PER_INT16 = 4;
        private const int HEX_PER_INT32 = 8;
        private const int HEX_PER_INT64 = 16;

        private const int BYTES_PER_INT8 = 1;
        private const int BYTES_PER_INT16 = 2;
        private const int BYTES_PER_INT32 = 4;
        private const int BYTES_PER_INT64 = 8;

        private static readonly DateTime UnixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);



        #region ToHexString

        public static string StringToHexString(string ascii)
        {
            char[] chars = new char[ascii.Length * HEX_PER_BYTE];
            for (int iBytes = 0, iChar = 0; iBytes < ascii.Length; iBytes++)
            {
                for (int iShift = HEX_PER_BYTE - 1; iShift >= 0; iChar++, iShift--)
                {
                    chars[iChar] = (char)(((Byte)ascii[iBytes] >> (BITS_PER_HEX * iShift)) & 0xF);
                    chars[iChar] += (char)(chars[iChar] < 10 ? 0x30 : 0x37);
                }
            }
            return new string(chars);
        }

        public static string BitListToHexString(BitList bits)
        {
            int byteCount = bits.Count / BITS_PER_BYTE + (bits.Count % BITS_PER_BYTE == 0 ? 0 : 1);
            char[] chars = new char[byteCount * HEX_PER_BYTE];
            for (int iBit = 0, iChar = 0; iChar < chars.Length; iChar++)
            {
                for (int iShift = BITS_PER_HEX - 1; iShift >= 0; iBit++, iShift--)
                {
                    if (iBit < bits.Count && bits[iBit])
                        chars[iChar] |= (char)(1 << iShift);
                }
                chars[iChar] += (char)(chars[iChar] < 10 ? 0x30 : 0x37);
            }
            return new string(chars);
        }

        public static string SByteListToHexString(SByteList data)
        {
            char[] chars = new char[data.Count * HEX_PER_BYTE];
            for (int iBytes = 0, iChar = 0; iBytes < data.Count; iBytes++)
            {
                for (int iShift = HEX_PER_BYTE - 1; iShift >= 0; iChar++, iShift--)
                {
                    chars[iChar] = (char)(((Byte)data[iBytes] >> (BITS_PER_HEX * iShift)) & 0xF);
                    chars[iChar] += (char)(chars[iChar] < 10 ? 0x30 : 0x37);
                }
            }
            return new string(chars);
        }

        public static string ByteListToHexString(ByteList data)
        {
            char[] chars = new char[data.Count * HEX_PER_BYTE];
            for (int iBytes = 0, iChar = 0; iBytes < data.Count; iBytes++)
            {
                for (int iShift = HEX_PER_BYTE - 1; iShift >= 0; iChar++, iShift--)
                {
                    chars[iChar] = (char)((data[iBytes] >> (BITS_PER_HEX * iShift)) & 0xF);
                    chars[iChar] += (char)(chars[iChar] < 10 ? 0x30 : 0x37);
                }
            }
            return new string(chars);
        }

        public static string Int16ListToHexString(Int16List data)
        {
            char[] chars = new char[data.Count * HEX_PER_INT16];
            for (int iData = 0, iChar = 0; iData < data.Count; iData++)
            {
                for (int iShift = HEX_PER_INT16 - 1; iShift >= 0; iChar++, iShift--)
                {
                    chars[iChar] = (char)(((UInt16)data[iData] >> (BITS_PER_HEX * iShift)) & 0xF);
                    chars[iChar] += (char)(chars[iChar] < 10 ? 0x30 : 0x37);
                }
            }
            return new string(chars);
        }

        public static string UInt16ListToHexString(UInt16List data)
        {
            char[] chars = new char[data.Count * HEX_PER_INT16];
            for (int iData = 0, iChar = 0; iData < data.Count; iData++)
            {
                for (int iShift = HEX_PER_INT16 - 1; iShift >= 0; iChar++, iShift--)
                {
                    chars[iChar] = (char)((data[iData] >> (BITS_PER_HEX * iShift)) & 0xF);
                    chars[iChar] += (char)(chars[iChar] < 10 ? 0x30 : 0x37);
                }
            }
            return new string(chars);
        }

        public static string UInt16ListToHexWordString(UInt16List data)
        {
            int spaces = data.Count > 1 ? data.Count - 1 : 0;
            char[] chars = new char[data.Count * HEX_PER_INT16 + spaces];
            for (int iData = 0, iChar = 0; iData < data.Count; iData++)
            {
                if (iChar > 0)
                {
                    chars[iChar++] = ' ';
                }
                for (int iShift = HEX_PER_INT16 - 1; iShift >= 0; iChar++, iShift--)
                {
                    chars[iChar] = (char)((data[iData] >> (BITS_PER_HEX * iShift)) & 0xF);
                    chars[iChar] += (char)(chars[iChar] < 10 ? 0x30 : 0x37);
                }
            }
            return new string(chars);
        }

        public static string Int32ListToHexString(Int32List data)
        {
            char[] chars = new char[data.Count * HEX_PER_INT32];
            for (int iData = 0, iChar = 0; iData < data.Count; iData++)
            {
                for (int iShift = HEX_PER_INT32 - 1; iShift >= 0; iChar++, iShift--)
                {
                    chars[iChar] = (char)(((UInt32)data[iData] >> (BITS_PER_HEX * iShift)) & 0xF);
                    chars[iChar] += (char)(chars[iChar] < 10 ? 0x30 : 0x37);
                }
            }
            return new string(chars);
        }

        public static string UInt32ListToHexString(UInt32List data)
        {
            char[] chars = new char[data.Count * HEX_PER_INT32];
            for (int iData = 0, iChar = 0; iData < data.Count; iData++)
            {
                for (int iShift = HEX_PER_INT32 - 1; iShift >= 0; iChar++, iShift--)
                {
                    chars[iChar] = (char)((data[iData] >> (BITS_PER_HEX * iShift)) & 0xF);
                    chars[iChar] += (char)(chars[iChar] < 10 ? 0x30 : 0x37);
                }
            }
            return new string(chars);
        }

        public static string Int64ListToHexString(Int64List data)
        {
            char[] chars = new char[data.Count * HEX_PER_INT64];
            for (int iData = 0, iChar = 0; iData < data.Count; iData++)
            {
                for (int iShift = HEX_PER_INT64 - 1; iShift >= 0; iChar++, iShift--)
                {
                    chars[iChar] = (char)(((UInt64)data[iData] >> (BITS_PER_HEX * iShift)) & 0xF);
                    chars[iChar] += (char)(chars[iChar] < 10 ? 0x30 : 0x37);
                }
            }
            return new string(chars);
        }

        public static string UInt64ListToHexString(UInt64List data)
        {
            char[] chars = new char[data.Count * HEX_PER_INT64];
            for (int iData = 0, iChar = 0; iData < data.Count; iData++)
            {
                for (int iShift = HEX_PER_INT64 - 1; iShift >= 0; iChar++, iShift--)
                {
                    chars[iChar] = (char)((data[iData] >> (BITS_PER_HEX * iShift)) & 0xF);
                    chars[iChar] += (char)(chars[iChar] < 10 ? 0x30 : 0x37);
                }
            }
            return new string(chars);
        }

        #endregion ToHexString



        #region FromHexString

        public static Byte HexStringToByte(string value) => Convert.ToByte(value.Trim(), 16);

        public static SByte HexStringToSByte(string value) => Convert.ToSByte(value.Trim(), 16);

        public static UInt16 HexStringToUInt16(string value) => Convert.ToUInt16(value.Trim(), 16);

        public static Int16 HexStringToInt16(string value) => Convert.ToInt16(value.Trim(), 16);

        public static UInt16 HexStringToUInt32(string value) => Convert.ToUInt16(value.Trim(), 16);

        public static Int16 HexStringToInt32(string value) => Convert.ToInt16(value.Trim(), 16);

        public static UInt16 HexStringToUInt64(string value) => Convert.ToUInt16(value.Trim(), 16);

        public static Int16 HexStringToInt64(string value) => Convert.ToInt16(value.Trim(), 16);

        private static List<byte> GetHexValues(string hex, int hexPerType)
        {
            List<byte> vals = new List<byte>(hex.Length + hexPerType - 1);
            foreach (char c in hex)
            {
                if (c >= '0' && c <= '9')
                    vals.Add((byte)(c - 0x30));
                else if (c >= 'A' && c <= 'F')
                    vals.Add((byte)(c - 0x37));
                else if (c >= 'a' && c <= 'f')
                    vals.Add((byte)(c - 0x57));
                else if (c != ' ' && c != '-')
                {
                    throw new FormatException("Invalid hex string supplied. (" + hex + ")");
                }
            }
            while (vals.Count % hexPerType != 0)
            {
                vals.Add(0);
            }
            return vals;
        }

        public static BitList HexStringToBitList(string hex)
        {
            //List<byte> vals = GetHexValues(hex.Trim(), HEX_PER_BYTE);
            //BitList data = new BitList(vals.Count * BITS_PER_HEX);
            //for (int iVal = 0; iVal < vals.Count; iVal++)
            //{
            //    for (int iShift = BITS_PER_HEX - 1; iShift >= 0; iShift--)
            //    {
            //        data.Add(((1 << iShift) & vals[iVal]) > 0);
            //    }
            //}
            //return data;

            List<byte> vals = GetHexValues(hex.Trim(), HEX_PER_BYTE);
            BitList bitList = new BitList(vals.Count * BITS_PER_HEX);
            for (int iVal = 0; iVal < vals.Count; iVal++)
            {
                for (int iShift = BITS_PER_HEX - 1; iShift >= 0; iShift--)
                {
                    bitList.Add(((vals[iVal] >> iShift) & 1) == 1);
                }
            }
            return bitList;
        }

        public static SByteList HexStringToSByteList(string hex)
        {
            List<byte> vals = GetHexValues(hex.Trim(), HEX_PER_BYTE);
            SByteList data = new SByteList(vals.Count / HEX_PER_BYTE);
            int val = 0;
            for (int iHex = 0; iHex < vals.Count; val = 0)
            {
                for (int iShift = HEX_PER_BYTE - 1; iShift >= 0; iHex++, iShift--)
                {
                    val |= vals[iHex] << (iShift * BITS_PER_HEX);
                }
                data.Add((SByte)val);
            }
            return data;
        }

        public static ByteList HexStringToByteList(string hex)
        {
            List<byte> vals = GetHexValues(hex.Trim(), HEX_PER_BYTE);
            ByteList data = new ByteList(vals.Count / HEX_PER_BYTE);
            int val = 0;
            for (int iHex = 0; iHex < vals.Count; val = 0)
            {
                for (int iShift = HEX_PER_BYTE - 1; iShift >= 0; iHex++, iShift--)
                {
                    val |= vals[iHex] << (iShift * BITS_PER_HEX);
                }
                data.Add((Byte)val);
            }
            return data;
        }

        public static Int16List HexStringToInt16List(string hex)
        {
            List<byte> vals = GetHexValues(hex.Trim(), HEX_PER_INT16);
            Int16List data = new Int16List(vals.Count / HEX_PER_INT16);
            int val = 0;
            for (int iHex = 0; iHex < vals.Count; val = 0)
            {
                for (int iShift = HEX_PER_INT16 - 1; iShift >= 0; iHex++, iShift--)
                {
                    val |= vals[iHex] << (iShift * BITS_PER_HEX);
                }
                data.Add((Int16)val);
            }
            return data;
        }

        public static UInt16List HexStringToUInt16List(string hex)
        {
            List<byte> vals = GetHexValues(hex.Trim(), HEX_PER_INT16);
            UInt16List data = new UInt16List(vals.Count / HEX_PER_INT16);
            int val = 0;
            for (int iHex = 0; iHex < vals.Count; val = 0)
            {
                for (int iShift = HEX_PER_INT16 - 1; iShift >= 0; iHex++, iShift--)
                {
                    val |= vals[iHex] << (iShift * BITS_PER_HEX);
                }
                data.Add((UInt16)val);
            }
            return data;
        }

        public static Int32List HexStringToInt32List(string hex)
        {
            List<byte> vals = GetHexValues(hex.Trim(), HEX_PER_INT32);
            Int32List data = new Int32List(vals.Count / HEX_PER_INT32);
            int val = 0;
            for (int iHex = 0; iHex < vals.Count; val = 0)
            {
                for (int iShift = HEX_PER_INT32 - 1; iShift >= 0; iHex++, iShift--)
                {
                    val |= vals[iHex] << (iShift * BITS_PER_HEX);
                }
                data.Add((Int32)val);
            }
            return data;
        }

        public static UInt32List HexStringToUInt32List(string hex)
        {
            List<byte> vals = GetHexValues(hex.Trim(), HEX_PER_INT32);
            UInt32List data = new UInt32List(vals.Count / HEX_PER_INT32);
            UInt32 val = 0;
            for (int iHex = 0; iHex < vals.Count; val = 0)
            {
                for (int iShift = HEX_PER_INT32 - 1; iShift >= 0; iHex++, iShift--)
                {
                    val |= (UInt32)vals[iHex] << (iShift * BITS_PER_HEX);
                }
                data.Add((UInt32)val);
            }
            return data;
        }

        public static Int64List HexStringToInt64List(string hex)
        {
            List<byte> vals = GetHexValues(hex.Trim(), HEX_PER_INT64);
            Int64List data = new Int64List(vals.Count / HEX_PER_INT64);
            Int64 val = 0;
            for (int iHex = 0; iHex < vals.Count; val = 0)
            {
                for (int iShift = HEX_PER_INT64 - 1; iShift >= 0; iHex++, iShift--)
                {
                    val |= (Int64)vals[iHex] << (iShift * BITS_PER_HEX);
                }
                data.Add((Int64)val);
            }
            return data;
        }

        public static UInt64List HexStringToUInt64List(string hex)
        {
            List<byte> vals = GetHexValues(hex.Trim(), HEX_PER_INT64);
            UInt64List data = new UInt64List(vals.Count / HEX_PER_INT64);
            UInt64 val = 0;
            for (int iHex = 0; iHex < vals.Count; val = 0)
            {
                for (int iShift = HEX_PER_INT64 - 1; iShift >= 0; iHex++, iShift--)
                {
                    val |= (UInt64)vals[iHex] << (iShift * BITS_PER_HEX);
                }
                data.Add((UInt64)val);
            }
            return data;
        }

        #endregion FromHexString



        #region ToTextString

        public static string StringToTextString(String value) => value;

        public static string BooleanToTextString(Boolean value) => Convert.ToString(value).ToLower();

        public static string ByteToTextString(Byte value) => value.ToString();

        public static string SByteToTextString(SByte value) => value.ToString();

        public static string UInt16ToTextString(UInt16 value) => value.ToString();

        public static string Int16ToTextString(Int16 value) => value.ToString();

        public static string UInt32ToTextString(UInt32 value) => value.ToString();

        public static string Int32ToTextString(Int32 value) => value.ToString();

        public static string UInt64ToTextString(UInt64 value) => value.ToString();

        public static string Int64ToTextString(Int64 value) => value.ToString();

        public static string TwoBitsToTextString(TwoBits value) => TwoBitsToInt32(value).ToString();

        public static string BitListToTextString(BitList value) => BitListToHexString(value);

        public static string ByteListToTextString(ByteList value) => string.Join(" ", value);

        public static string ByteListToTextString(ByteList value, Type type) => string.Join(" ", value.Select(b => Enum.GetName(type, b)));

        public static string SByteListToTextString(SByteList value) => string.Join(" ", value);

        public static string UInt16ListToTextString(UInt16List value) => string.Join(" ", value);

        public static string Int16ListToTextString(Int16List value) => string.Join(" ", value);

        public static string UInt32ListToTextString(UInt32List value) => string.Join(" ", value);

        public static string Int32ListToTextString(Int32List value) => string.Join(" ", value);

        public static string UInt64ListToTextString(UInt64List value) => string.Join(" ", value);

        public static string Int64ListToTextString(Int64List value) => string.Join(" ", value);

        #endregion ToTextString




        #region FromTextString

        public static Boolean TextStringToBoolean(string value)
        {
            if (value == "1")
                return true;
            else if (value == "0")
                return false;
            else
                return Convert.ToBoolean(value);
        }

        public static Byte TextStringToByte(string value) => Convert.ToByte(value.Trim(), 10);

        public static SByte TextStringToSByte(string value) => Convert.ToSByte(value.Trim(), 10);

        public static UInt16 TextStringToUInt16(string value) => Convert.ToUInt16(value.Trim(), 10);

        public static Int16 TextStringToInt16(string value) => Convert.ToInt16(value.Trim(), 10);

        public static UInt32 TextStringToUInt32(string value) => Convert.ToUInt32(value.Trim(), 10);

        public static Int32 TextStringToInt32(string value) => Convert.ToInt32(value.Trim(), 10);

        public static UInt64 TextStringToUInt64(string value) => Convert.ToUInt64(value.Trim(), 10);

        public static Int64 TextStringToInt64(string value) => Convert.ToInt64(value.Trim(), 10);

        public static TwoBits TextStringToTwoBits(string value) => new TwoBits(Convert.ToUInt16(value.Trim(), 10));

        public static BitList TextStringToBitList(string value) => HexStringToBitList(value);

        public static ByteList TextStringToByteList(string value) => new ByteList(value.Split(_delimiters, StringSplitOptions.RemoveEmptyEntries).Select(s => Byte.Parse(s)));

        public static ByteList TextStringToByteList(string value, Type type) => new ByteList(value.Split(_delimiters, StringSplitOptions.RemoveEmptyEntries).Select(s => (Byte)(Int32)Enum.Parse(type, s, true)));

        public static SByteList TextStringToSByteList(string value) => new SByteList(value.Split(_delimiters, StringSplitOptions.RemoveEmptyEntries).Select(s => SByte.Parse(s)));

        public static UInt16List TextStringToUInt16List(string value) => new UInt16List(value.Split(_delimiters, StringSplitOptions.RemoveEmptyEntries).Select(s => UInt16.Parse(s)));

        public static Int16List TextStringToInt16List(string value) => new Int16List(value.Split(_delimiters, StringSplitOptions.RemoveEmptyEntries).Select(s => Int16.Parse(s)));

        public static UInt32List TextStringToUInt32List(string value) => new UInt32List(value.Split(_delimiters, StringSplitOptions.RemoveEmptyEntries).Select(s => UInt32.Parse(s)));

        public static Int32List TextStringToInt32List(string value) => new Int32List(value.Split(_delimiters, StringSplitOptions.RemoveEmptyEntries).Select(s => Int32.Parse(s)));

        public static UInt64List TextStringToUInt64List(string value) => new UInt64List(value.Split(_delimiters, StringSplitOptions.RemoveEmptyEntries).Select(s => UInt64.Parse(s)));

        public static Int64List TextStringToInt64List(string value) => new Int64List(value.Split(_delimiters, StringSplitOptions.RemoveEmptyEntries).Select(s => Int64.Parse(s)));

        public static String TextStringToString(string value) => value.Trim();

        private readonly static char[] _delimiters = new char[] { ',', ' ', '\t', '\n', '\r' };

        #endregion FromTextString

        public static Int32 TwoBitsToInt32(TwoBits value) => ((value[0] ? 2 : 0) + (value[1] ? 1 : 0));

        public static byte[] BitsToBytes(IList<bool> bits)
        {
            int byteCount = bits.Count / BITS_PER_BYTE + (bits.Count % BITS_PER_BYTE == 0 ? 0 : 1);
            byte[] bytes = new byte[byteCount];
            for (int iBit = 0, iByte = 0; iBit < bits.Count; iByte++)
            {
                int byteVal = 0;
                for (int iShift = BITS_PER_BYTE - 1; iShift >= 0 && iBit < bits.Count; iBit++, iShift--)
                {
                    if (bits[iBit])
                    {
                        byteVal |= (1 << iShift);
                    }
                }
                bytes[iByte] = (byte)byteVal;
            }
            return bytes;
        }

        public static String BitListToString(IList<Boolean> data, ref int cursor, int len)
        {
            StringBuilder sb = new StringBuilder(len);
            for (int i = 0; i < len; i++)
            {
                Byte b = BitListToByte(data, ref cursor, 8);
                sb.Append((char)b);
            }
            if (len > 1 && sb[len - 1] == 0)
            {
                // remove trailing NULL and replace so packet length is consistent
                sb[len - 1] = '.';
            }
            return sb.ToString();
        }

        public static TwoBits BitListToTwoBits(IList<Boolean> data, ref int cursor, int len)
        {
            int index = cursor;
            TwoBits value;
            if (len > 2)
            {
                index += len - 2;
            }
            if (len == 1)
            {
                value = new TwoBits(false, data[index]);
            }
            else
            {
                value = new TwoBits(data[index], data[index + 1]);
            }
            cursor += len;
            return value;
        }

        public static Boolean BitListToBoolean(IList<Boolean> data, ref int cursor, int len)
        {
            int index = cursor;
            if (len > 1)
            {
                index += len - 1;
            }
            bool value = data[index];
            cursor += len;
            return value;
        }

        public static Byte BitListToByte(IList<Boolean> data, ref int cursor, int len)
        {
            int index = cursor;
            int shift = len - 1;
            int value = 0;
            for (; shift >= 0; index++, shift--)
            {
                if (data[index])
                    value |= (1 << shift);
            }
            cursor += len;
            return (Byte)value;
        }

        public static SByte BitListToSByte(IList<Boolean> data, ref int cursor, int len)
        {
            int index = cursor;
            int shift = len - 1;
            int value = 0;
            for (; shift >= 0; index++, shift--)
            {
                if (data[index])
                    value |= (1 << shift);
            }
            cursor += len;
            return (SByte)value;
        }

        public static Int16 BitListToInt16(IList<Boolean> data, ref int cursor, int len)
        {
            int index = cursor;
            int shift = len - 1;
            int value = 0;
            for (; shift >= 0; index++, shift--)
            {
                if (data[index])
                    value |= (1 << shift);
            }
            cursor += len;
            return (Int16)value;
        }

        public static UInt16 BitListToUInt16(IList<Boolean> data, ref int cursor, int len)
        {
            int index = cursor;
            int shift = len - 1;
            int value = 0;
            for (; shift >= 0; index++, shift--)
            {
                if (data[index])
                    value |= (1 << shift);
            }
            cursor += len;
            return (UInt16)value;
        }

        public static Int32 BitListToInt32(IList<Boolean> data, ref int cursor, int len)
        {
            int index = cursor;
            int shift = len - 1;
            Int32 value = 0;
            for (; shift >= 0; index++, shift--)
            {
                if (data[index])
                    value |= (1 << shift);
            }
            cursor += len;
            return value;
        }

        public static UInt32 BitListToUInt32(IList<Boolean> data, ref int cursor, int len)
        {
            int index = cursor;
            int shift = len - 1;
            UInt32 value = 0;
            for (; shift >= 0; index++, shift--)
            {
                if (data[index])
                    value |= (1U << shift);
            }
            cursor += len;
            return value;
        }

        public static Int64 BitListToInt64(IList<Boolean> data, ref int cursor, int len)
        {
            int index = cursor;
            int shift = len - 1;
            Int64 value = 0;
            for (; shift >= 0; index++, shift--)
            {
                if (data[index])
                    value |= (1L << shift);
            }
            cursor += len;
            return value;
        }

        public static UInt64 BitListToUInt64(IList<Boolean> data, ref int cursor, int len)
        {
            int index = cursor;
            int shift = len - 1;
            UInt64 value = 0;
            for (; shift >= 0; index++, shift--)
            {
                if (data[index])
                    value |= (1UL << shift);
            }
            cursor += len;
            return value;
        }

        public static BitList BitListToBitList(IList<Boolean> data, ref int cursor, int len)
        {
            BitList list = new BitList(len);
            for (int i = 0; i < len; i++)
            {
                list.Add(data[cursor++]);
            }
            return list;
        }

        public static ByteList BitListToByteList(IList<Boolean> data, ref int cursor, int len)
        {
            ByteList list = new ByteList(len);
            for (int i = 0; i < len; i++)
            {
                list.Add(BitListToByte(data, ref cursor, 8));
            }
            return list;
        }

        public static UInt16List BitListToUInt16List(IList<Boolean> data, ref int cursor, int len)
        {
            UInt16List list = new UInt16List(len);
            for (int i = 0; i < len; i++)
            {
                list.Add(BitListToUInt16(data, ref cursor, 16));
            }
            return list;
        }

        public static UInt32List BitListToUInt32List(IList<Boolean> data, ref int cursor, int len)
        {
            UInt32List list = new UInt32List(len);
            for (int i = 0; i < len; i++)
            {
                list.Add(BitListToUInt32(data, ref cursor, 32));
            }
            return list;
        }

        #region ToBitList

        public static void ParameterToBitList(ILLRPParameter parameter, BitList bitList)
        {
            parameter.ToBitList(bitList);
        }

        public static void EnumToBitList(Enum value, BitList bitList, int length)
        {
            Int32 intValue = Convert.ToInt32(value);
            for (int shift = length - 1; shift >= 0; shift--)
            {
                bitList.Add(((intValue >> shift) & 1) == 1);
            }
        }

        public static void BooleanToBitList(Boolean value, BitList bitList)
        {
            bitList.Add(value);
        }

        public static void SByteToBitList(SByte value, BitList bitList, int length = BITS_PER_BYTE)
        {
            for (int shift = length - 1; shift >= 0; shift--)
            {
                bitList.Add(((value >> shift) & 1) == 1);
            }
        }

        public static void ByteToBitList(Byte value, BitList bitList, int length = BYTES_PER_INT8 * BITS_PER_BYTE)
        {
            for (int shift = length - 1; shift >= 0; shift--)
            {
                bitList.Add(((value >> shift) & 1) == 1);
            }
        }

        public static void Int16ToBitList(Int16 value, BitList bitList, int length = BYTES_PER_INT16 * BITS_PER_BYTE)
        {
            for (int shift = length - 1; shift >= 0; shift--)
            {
                bitList.Add(((value >> shift) & 1) == 1);
            }
        }

        public static void UInt16ToBitList(UInt16 value, BitList bitList, int length = BYTES_PER_INT16 * BITS_PER_BYTE)
        {
            for (int shift = length - 1; shift >= 0; shift--)
            {
                bitList.Add(((value >> shift) & 1) == 1);
            }
        }

        public static void Int32ToBitList(Int32 value, BitList bitList, int length = BYTES_PER_INT32 * BITS_PER_BYTE)
        {
            for (int shift = length - 1; shift >= 0; shift--)
            {
                bitList.Add(((value >> shift) & 1) == 1);
            }
        }

        public static void UInt32ToBitList(UInt32 value, BitList bitList, int index, int length)
        {
            for (int shift = length - 1; shift >= 0; shift--, index++)
            {
                bitList[index] = (((value >> shift) & 1) == 1);
            }
        }

        public static void UInt32ToBitList(UInt32 value, BitList bitList, int length = BYTES_PER_INT32 * BITS_PER_BYTE)
        {
            for (int shift = length - 1; shift >= 0; shift--)
            {
                bitList.Add(((value >> shift) & 1) == 1);
            }
        }

        public static void Int64ToBitList(Int64 value, BitList bitList, int length = BYTES_PER_INT64 * BITS_PER_BYTE)
        {
            for (int shift = length - 1; shift >= 0; shift--)
            {
                bitList.Add(((value >> shift) & 1) == 1);
            }
        }

        public static void UInt64ToBitList(UInt64 value, BitList bitList, int length = BYTES_PER_INT64 * BITS_PER_BYTE)
        {
            for (int shift = length - 1; shift >= 0; shift--)
            {
                bitList.Add(((value >> shift) & 1) == 1);
            }
        }



        //public static BitList TwoBitsToBitList(TwoBits data)
        //{
        //    BitList bitList = new BitList(2) { data[0], data[1] };
        //    return bitList;
        //}

        //public static BitList BitListToBitList(BitList data)
        //{
        //    return new BitList(data);
        //}

        //public static BitList UInt16ListToBitList(UInt16List data)
        //{
        //    BitList bitList = new BitList(data.Count * BYTES_PER_INT16 * BITS_PER_BYTE);
        //    for (int iData = 0; iData < data.Count; iData++)
        //    {
        //        for (int iShift = (BYTES_PER_INT16 * BITS_PER_BYTE) - 1; iShift >= 0; iShift--)
        //        {
        //            bitList.Add(((1 << iShift) & data[iData]) > 0);
        //        }
        //    }
        //    return bitList;
        //}

        //public static BitList UInt32ListToBitList(UInt32List data)
        //{
        //    BitList bitList = new BitList(data.Count * BYTES_PER_INT32 * BITS_PER_BYTE);
        //    for (int iData = 0; iData < data.Count; iData++)
        //    {
        //        for (int iShift = (BYTES_PER_INT32 * BITS_PER_BYTE) - 1; iShift >= 0; iShift--)
        //        {
        //            bitList.Add(((1 << iShift) & data[iData]) > 0);
        //        }
        //    }
        //    return bitList;
        //}

        public static BitList ByteListToBitList(IList<Byte> data)
        {
            //BitList bitList = new BitList(data.Count * BITS_PER_BYTE);
            //for (int iData = 0; iData < data.Count; iData++)
            //{
            //    for (int iShift = BITS_PER_BYTE - 1; iShift >= 0; iShift--)
            //    {
            //        bitList.Add(((1 << iShift) & data[iData]) > 0);
            //    }
            //}
            //return bitList;

            BitList bitList = new BitList(data.Count * BITS_PER_BYTE);
            for (int iData = 0; iData < data.Count; iData++)
            {
                for (int iShift = BITS_PER_BYTE - 1; iShift >= 0; iShift--)
                {
                    bitList.Add(((data[iData] >> iShift) & 1) == 1);
                }
            }
            return bitList;

            //bool[] bits = new bool[data.Count * BITS_PER_BYTE];
            //for (int i = 0, iData = 0; iData < data.Count; iData++)
            //{
            //    for (int iShift = BITS_PER_BYTE - 1; iShift >= 0; iShift--)
            //    {
            //        bits[i++] = ((data[iData] >> iShift) & 1) == 1;
            //    }
            //}
            //return new BitList(bits);
        }




        public static void TwoBitsToBitList(TwoBits data, BitList bitList)
        {
            bitList.Add(data[0]);
            bitList.Add(data[1]);
        }

        public static void BitListToBitList(BitList data, BitList bitList)
        {
            bitList.AddRange(data);
        }

        public static void ByteListToBitList(ByteList data, BitList bitList)
        {
            for (int iData = 0; iData < data.Count; iData++)
            {
                for (int iShift = (BYTES_PER_INT8 * BITS_PER_BYTE) - 1; iShift >= 0; iShift--)
                {
                    bitList.Add(((data[iData] >> iShift) & 1) == 1);
                }
            }
        }

        public static void UInt16ListToBitList(UInt16List data, BitList bitList)
        {
            for (int iData = 0; iData < data.Count; iData++)
            {
                for (int iShift = (BYTES_PER_INT16 * BITS_PER_BYTE) - 1; iShift >= 0; iShift--)
                {
                    bitList.Add(((data[iData] >> iShift) & 1) == 1);
                }
            }
        }

        public static void UInt32ListToBitList(UInt32List data, BitList bitList)
        {
            for (int iData = 0; iData < data.Count; iData++)
            {
                for (int iShift = (BYTES_PER_INT32 * BITS_PER_BYTE) - 1; iShift >= 0; iShift--)
                {
                    bitList.Add(((data[iData] >> iShift) & 1) == 1);
                }
            }
        }


        public static void StringToBitList(string value, BitList bitList)
        {
            for (int iData = 0; iData < value.Length; iData++)
            {
                for (int iShift = BITS_PER_BYTE - 1; iShift >= 0; iShift--)
                {
                    bitList.Add(((value[iData] >> iShift) & 1) == 1);
                }
            }
        }


        #endregion ToBitList


        public static BitList StringToBitList(string value)
        {
            BitList bitList = new BitList(value.Length * BITS_PER_BYTE);
            for (int iData = 0; iData < value.Length; iData++)
            {
                for (int iShift = BITS_PER_BYTE - 1; iShift >= 0; iShift--)
                {
                    bitList.Add(((1 << iShift) & value[iData]) > 0);
                }
            }
            return bitList;
        }


















        public static string ConvertMicrosecondsToUTCTimeString(ulong microseconds)
        {
            DateTime dt = ConvertMicrosecondsToDateTime(microseconds);
            return String.Format("{00}.{1:000000}Z", dt.ToString("s"), microseconds % 1000000UL);
        }

        public static UInt64 ConvertUTCTimeStringToMicroseconds(string utcTime)
        {
            try
            {
                DateTime dt = DateTime.Parse(utcTime, null, System.Globalization.DateTimeStyles.AdjustToUniversal | System.Globalization.DateTimeStyles.AssumeUniversal);
                return ConvertDateTimeToMicroseconds(dt);
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        /// Convert LLRP Timestamp to DateTime
        /// LLRP timestamps are microseconds since the Epoch (00:00:00 UTC, January 1, 1970)
        /// </summary>
        public static DateTime ConvertMicrosecondsToDateTime(ulong microseconds)
        {
            return UnixEpoch.AddTicks((long)microseconds * 10L);
        }

        public static DateTime ConvertTimestampToDateTime(UNION_Timestamp timestamp)
        {
            if (timestamp != null)
            {
                foreach (ILLRPParameter param in timestamp)
                {
                    if (param is PARAM_UTCTimestamp utcTimestamp)
                    {
                        return ConvertMicrosecondsToDateTime(utcTimestamp.Microseconds);
                    }
                }
                foreach (ILLRPParameter param in timestamp)
                {
                    if (param is PARAM_Uptime uptime)
                    {
                        return ConvertMicrosecondsToDateTime(uptime.Microseconds);
                    }
                }
            }
            return DateTime.MinValue;
        }

        /// <summary>
        /// Convert LLRP Timestamp to DateTime
        /// LLRP timestamps are microseconds since the Epoch (00:00:00 UTC, January 1, 1970)
        /// </summary>
        public static ulong ConvertDateTimeToMicroseconds(DateTime dateTime)
        {
            return (ulong)(dateTime.ToUniversalTime() - UnixEpoch).Ticks / 10L;
        }
    }

}
