
using System;
using System.Threading.Tasks;

namespace EA.Impinj.LLRP
{
    /// <summary>
    /// LLRP client transaction methods
    /// </summary>
    public partial class LLRPClient
    {
    
        /// <summary>
        /// Send customized message to the reader
        /// </summary>
        /// <param name="msg">Message to be sent to reader.</param>
        /// <param name="responseVendorID">The vendor of the expected response message</param>
        /// <param name="responseSubTypeID">The sub type of the expected response message</param>
        /// <param name="timeoutMilliseconds">Timeout in milliseconds.</param>
        /// <returns>Custom Message</returns>
        public async Task<MSG_CUSTOM_MESSAGE> CUSTOM_MESSAGE(MSG_CUSTOM_MESSAGE msg, UInt32 responseVendorID, Byte responseSubTypeID, int timeoutMilliseconds)
        {
            return await TransactAsync(msg, (UInt16)ENUM_LLRP_MSG_TYPE.CUSTOM_MESSAGE, responseVendorID, responseSubTypeID, timeoutMilliseconds).ConfigureAwait(false) as MSG_CUSTOM_MESSAGE;
        }
        /// <summary>
        /// Send customized message to the reader
        /// </summary>
        /// <param name="msg">Message to be sent to reader.</param>
        /// <param name="responseVendorID">The vendor of the expected response message</param>
        /// <param name="responseSubTypeID">The sub type of the expected response message</param>
        /// <returns>Custom Message</returns>
        public async Task<MSG_CUSTOM_MESSAGE> CUSTOM_MESSAGE(MSG_CUSTOM_MESSAGE msg, UInt32 responseVendorID, Byte responseSubTypeID)
        {
            return await TransactAsync(msg, (UInt16)ENUM_LLRP_MSG_TYPE.CUSTOM_MESSAGE, responseVendorID, responseSubTypeID, ResponseTimeout).ConfigureAwait(false) as MSG_CUSTOM_MESSAGE;
        }
        /// <summary>
        /// Send customized message to the reader
        /// </summary>
        /// <param name="msg">Message to be sent to reader.</param>
        public async Task CUSTOM_MESSAGE(MSG_CUSTOM_MESSAGE msg)
        {
            await SendAsync(msg).ConfigureAwait(false);
        }
      
        /// <summary>
        /// GET_READER_CAPABILITIES message call.
        /// </summary>
        /// <param name="msg">MSG_GET_READER_CAPABILITIES to send to reader.</param>
        /// <param name="timeoutMilliseconds">Fuction call time out in milliseconds.</param>
        /// <returns>MSG_GET_READER_CAPABILITIES_RESPONSE</returns>
        public async Task<MSG_GET_READER_CAPABILITIES_RESPONSE> GET_READER_CAPABILITIES(MSG_GET_READER_CAPABILITIES msg, int timeoutMilliseconds)
        {
            return await TransactAsync(msg, (UInt16)ENUM_LLRP_MSG_TYPE.GET_READER_CAPABILITIES_RESPONSE, 0, 0, timeoutMilliseconds).ConfigureAwait(false) as MSG_GET_READER_CAPABILITIES_RESPONSE;
        }
        /// <summary>
        /// GET_READER_CAPABILITIES message call.
        /// </summary>
        /// <param name="msg">MSG_GET_READER_CAPABILITIES to send to reader.</param>
        /// <returns>MSG_GET_READER_CAPABILITIES_RESPONSE</returns>
        public async Task<MSG_GET_READER_CAPABILITIES_RESPONSE> GET_READER_CAPABILITIES(MSG_GET_READER_CAPABILITIES msg)
        {
            return await TransactAsync(msg, (UInt16)ENUM_LLRP_MSG_TYPE.GET_READER_CAPABILITIES_RESPONSE, 0, 0, ResponseTimeout).ConfigureAwait(false) as MSG_GET_READER_CAPABILITIES_RESPONSE;
        }
      
        /// <summary>
        /// ADD_ROSPEC message call.
        /// </summary>
        /// <param name="msg">MSG_ADD_ROSPEC to send to reader.</param>
        /// <param name="timeoutMilliseconds">Fuction call time out in milliseconds.</param>
        /// <returns>MSG_ADD_ROSPEC_RESPONSE</returns>
        public async Task<MSG_ADD_ROSPEC_RESPONSE> ADD_ROSPEC(MSG_ADD_ROSPEC msg, int timeoutMilliseconds)
        {
            return await TransactAsync(msg, (UInt16)ENUM_LLRP_MSG_TYPE.ADD_ROSPEC_RESPONSE, 0, 0, timeoutMilliseconds).ConfigureAwait(false) as MSG_ADD_ROSPEC_RESPONSE;
        }
        /// <summary>
        /// ADD_ROSPEC message call.
        /// </summary>
        /// <param name="msg">MSG_ADD_ROSPEC to send to reader.</param>
        /// <returns>MSG_ADD_ROSPEC_RESPONSE</returns>
        public async Task<MSG_ADD_ROSPEC_RESPONSE> ADD_ROSPEC(MSG_ADD_ROSPEC msg)
        {
            return await TransactAsync(msg, (UInt16)ENUM_LLRP_MSG_TYPE.ADD_ROSPEC_RESPONSE, 0, 0, ResponseTimeout).ConfigureAwait(false) as MSG_ADD_ROSPEC_RESPONSE;
        }
      
        /// <summary>
        /// DELETE_ROSPEC message call.
        /// </summary>
        /// <param name="msg">MSG_DELETE_ROSPEC to send to reader.</param>
        /// <param name="timeoutMilliseconds">Fuction call time out in milliseconds.</param>
        /// <returns>MSG_DELETE_ROSPEC_RESPONSE</returns>
        public async Task<MSG_DELETE_ROSPEC_RESPONSE> DELETE_ROSPEC(MSG_DELETE_ROSPEC msg, int timeoutMilliseconds)
        {
            return await TransactAsync(msg, (UInt16)ENUM_LLRP_MSG_TYPE.DELETE_ROSPEC_RESPONSE, 0, 0, timeoutMilliseconds).ConfigureAwait(false) as MSG_DELETE_ROSPEC_RESPONSE;
        }
        /// <summary>
        /// DELETE_ROSPEC message call.
        /// </summary>
        /// <param name="msg">MSG_DELETE_ROSPEC to send to reader.</param>
        /// <returns>MSG_DELETE_ROSPEC_RESPONSE</returns>
        public async Task<MSG_DELETE_ROSPEC_RESPONSE> DELETE_ROSPEC(MSG_DELETE_ROSPEC msg)
        {
            return await TransactAsync(msg, (UInt16)ENUM_LLRP_MSG_TYPE.DELETE_ROSPEC_RESPONSE, 0, 0, ResponseTimeout).ConfigureAwait(false) as MSG_DELETE_ROSPEC_RESPONSE;
        }
      
        /// <summary>
        /// START_ROSPEC message call.
        /// </summary>
        /// <param name="msg">MSG_START_ROSPEC to send to reader.</param>
        /// <param name="timeoutMilliseconds">Fuction call time out in milliseconds.</param>
        /// <returns>MSG_START_ROSPEC_RESPONSE</returns>
        public async Task<MSG_START_ROSPEC_RESPONSE> START_ROSPEC(MSG_START_ROSPEC msg, int timeoutMilliseconds)
        {
            return await TransactAsync(msg, (UInt16)ENUM_LLRP_MSG_TYPE.START_ROSPEC_RESPONSE, 0, 0, timeoutMilliseconds).ConfigureAwait(false) as MSG_START_ROSPEC_RESPONSE;
        }
        /// <summary>
        /// START_ROSPEC message call.
        /// </summary>
        /// <param name="msg">MSG_START_ROSPEC to send to reader.</param>
        /// <returns>MSG_START_ROSPEC_RESPONSE</returns>
        public async Task<MSG_START_ROSPEC_RESPONSE> START_ROSPEC(MSG_START_ROSPEC msg)
        {
            return await TransactAsync(msg, (UInt16)ENUM_LLRP_MSG_TYPE.START_ROSPEC_RESPONSE, 0, 0, ResponseTimeout).ConfigureAwait(false) as MSG_START_ROSPEC_RESPONSE;
        }
      
        /// <summary>
        /// STOP_ROSPEC message call.
        /// </summary>
        /// <param name="msg">MSG_STOP_ROSPEC to send to reader.</param>
        /// <param name="timeoutMilliseconds">Fuction call time out in milliseconds.</param>
        /// <returns>MSG_STOP_ROSPEC_RESPONSE</returns>
        public async Task<MSG_STOP_ROSPEC_RESPONSE> STOP_ROSPEC(MSG_STOP_ROSPEC msg, int timeoutMilliseconds)
        {
            return await TransactAsync(msg, (UInt16)ENUM_LLRP_MSG_TYPE.STOP_ROSPEC_RESPONSE, 0, 0, timeoutMilliseconds).ConfigureAwait(false) as MSG_STOP_ROSPEC_RESPONSE;
        }
        /// <summary>
        /// STOP_ROSPEC message call.
        /// </summary>
        /// <param name="msg">MSG_STOP_ROSPEC to send to reader.</param>
        /// <returns>MSG_STOP_ROSPEC_RESPONSE</returns>
        public async Task<MSG_STOP_ROSPEC_RESPONSE> STOP_ROSPEC(MSG_STOP_ROSPEC msg)
        {
            return await TransactAsync(msg, (UInt16)ENUM_LLRP_MSG_TYPE.STOP_ROSPEC_RESPONSE, 0, 0, ResponseTimeout).ConfigureAwait(false) as MSG_STOP_ROSPEC_RESPONSE;
        }
      
        /// <summary>
        /// ENABLE_ROSPEC message call.
        /// </summary>
        /// <param name="msg">MSG_ENABLE_ROSPEC to send to reader.</param>
        /// <param name="timeoutMilliseconds">Fuction call time out in milliseconds.</param>
        /// <returns>MSG_ENABLE_ROSPEC_RESPONSE</returns>
        public async Task<MSG_ENABLE_ROSPEC_RESPONSE> ENABLE_ROSPEC(MSG_ENABLE_ROSPEC msg, int timeoutMilliseconds)
        {
            return await TransactAsync(msg, (UInt16)ENUM_LLRP_MSG_TYPE.ENABLE_ROSPEC_RESPONSE, 0, 0, timeoutMilliseconds).ConfigureAwait(false) as MSG_ENABLE_ROSPEC_RESPONSE;
        }
        /// <summary>
        /// ENABLE_ROSPEC message call.
        /// </summary>
        /// <param name="msg">MSG_ENABLE_ROSPEC to send to reader.</param>
        /// <returns>MSG_ENABLE_ROSPEC_RESPONSE</returns>
        public async Task<MSG_ENABLE_ROSPEC_RESPONSE> ENABLE_ROSPEC(MSG_ENABLE_ROSPEC msg)
        {
            return await TransactAsync(msg, (UInt16)ENUM_LLRP_MSG_TYPE.ENABLE_ROSPEC_RESPONSE, 0, 0, ResponseTimeout).ConfigureAwait(false) as MSG_ENABLE_ROSPEC_RESPONSE;
        }
      
        /// <summary>
        /// DISABLE_ROSPEC message call.
        /// </summary>
        /// <param name="msg">MSG_DISABLE_ROSPEC to send to reader.</param>
        /// <param name="timeoutMilliseconds">Fuction call time out in milliseconds.</param>
        /// <returns>MSG_DISABLE_ROSPEC_RESPONSE</returns>
        public async Task<MSG_DISABLE_ROSPEC_RESPONSE> DISABLE_ROSPEC(MSG_DISABLE_ROSPEC msg, int timeoutMilliseconds)
        {
            return await TransactAsync(msg, (UInt16)ENUM_LLRP_MSG_TYPE.DISABLE_ROSPEC_RESPONSE, 0, 0, timeoutMilliseconds).ConfigureAwait(false) as MSG_DISABLE_ROSPEC_RESPONSE;
        }
        /// <summary>
        /// DISABLE_ROSPEC message call.
        /// </summary>
        /// <param name="msg">MSG_DISABLE_ROSPEC to send to reader.</param>
        /// <returns>MSG_DISABLE_ROSPEC_RESPONSE</returns>
        public async Task<MSG_DISABLE_ROSPEC_RESPONSE> DISABLE_ROSPEC(MSG_DISABLE_ROSPEC msg)
        {
            return await TransactAsync(msg, (UInt16)ENUM_LLRP_MSG_TYPE.DISABLE_ROSPEC_RESPONSE, 0, 0, ResponseTimeout).ConfigureAwait(false) as MSG_DISABLE_ROSPEC_RESPONSE;
        }
      
        /// <summary>
        /// GET_ROSPECS message call.
        /// </summary>
        /// <param name="msg">MSG_GET_ROSPECS to send to reader.</param>
        /// <param name="timeoutMilliseconds">Fuction call time out in milliseconds.</param>
        /// <returns>MSG_GET_ROSPECS_RESPONSE</returns>
        public async Task<MSG_GET_ROSPECS_RESPONSE> GET_ROSPECS(MSG_GET_ROSPECS msg, int timeoutMilliseconds)
        {
            return await TransactAsync(msg, (UInt16)ENUM_LLRP_MSG_TYPE.GET_ROSPECS_RESPONSE, 0, 0, timeoutMilliseconds).ConfigureAwait(false) as MSG_GET_ROSPECS_RESPONSE;
        }
        /// <summary>
        /// GET_ROSPECS message call.
        /// </summary>
        /// <param name="msg">MSG_GET_ROSPECS to send to reader.</param>
        /// <returns>MSG_GET_ROSPECS_RESPONSE</returns>
        public async Task<MSG_GET_ROSPECS_RESPONSE> GET_ROSPECS(MSG_GET_ROSPECS msg)
        {
            return await TransactAsync(msg, (UInt16)ENUM_LLRP_MSG_TYPE.GET_ROSPECS_RESPONSE, 0, 0, ResponseTimeout).ConfigureAwait(false) as MSG_GET_ROSPECS_RESPONSE;
        }
      
        /// <summary>
        /// ADD_ACCESSSPEC message call.
        /// </summary>
        /// <param name="msg">MSG_ADD_ACCESSSPEC to send to reader.</param>
        /// <param name="timeoutMilliseconds">Fuction call time out in milliseconds.</param>
        /// <returns>MSG_ADD_ACCESSSPEC_RESPONSE</returns>
        public async Task<MSG_ADD_ACCESSSPEC_RESPONSE> ADD_ACCESSSPEC(MSG_ADD_ACCESSSPEC msg, int timeoutMilliseconds)
        {
            return await TransactAsync(msg, (UInt16)ENUM_LLRP_MSG_TYPE.ADD_ACCESSSPEC_RESPONSE, 0, 0, timeoutMilliseconds).ConfigureAwait(false) as MSG_ADD_ACCESSSPEC_RESPONSE;
        }
        /// <summary>
        /// ADD_ACCESSSPEC message call.
        /// </summary>
        /// <param name="msg">MSG_ADD_ACCESSSPEC to send to reader.</param>
        /// <returns>MSG_ADD_ACCESSSPEC_RESPONSE</returns>
        public async Task<MSG_ADD_ACCESSSPEC_RESPONSE> ADD_ACCESSSPEC(MSG_ADD_ACCESSSPEC msg)
        {
            return await TransactAsync(msg, (UInt16)ENUM_LLRP_MSG_TYPE.ADD_ACCESSSPEC_RESPONSE, 0, 0, ResponseTimeout).ConfigureAwait(false) as MSG_ADD_ACCESSSPEC_RESPONSE;
        }
      
        /// <summary>
        /// DELETE_ACCESSSPEC message call.
        /// </summary>
        /// <param name="msg">MSG_DELETE_ACCESSSPEC to send to reader.</param>
        /// <param name="timeoutMilliseconds">Fuction call time out in milliseconds.</param>
        /// <returns>MSG_DELETE_ACCESSSPEC_RESPONSE</returns>
        public async Task<MSG_DELETE_ACCESSSPEC_RESPONSE> DELETE_ACCESSSPEC(MSG_DELETE_ACCESSSPEC msg, int timeoutMilliseconds)
        {
            return await TransactAsync(msg, (UInt16)ENUM_LLRP_MSG_TYPE.DELETE_ACCESSSPEC_RESPONSE, 0, 0, timeoutMilliseconds).ConfigureAwait(false) as MSG_DELETE_ACCESSSPEC_RESPONSE;
        }
        /// <summary>
        /// DELETE_ACCESSSPEC message call.
        /// </summary>
        /// <param name="msg">MSG_DELETE_ACCESSSPEC to send to reader.</param>
        /// <returns>MSG_DELETE_ACCESSSPEC_RESPONSE</returns>
        public async Task<MSG_DELETE_ACCESSSPEC_RESPONSE> DELETE_ACCESSSPEC(MSG_DELETE_ACCESSSPEC msg)
        {
            return await TransactAsync(msg, (UInt16)ENUM_LLRP_MSG_TYPE.DELETE_ACCESSSPEC_RESPONSE, 0, 0, ResponseTimeout).ConfigureAwait(false) as MSG_DELETE_ACCESSSPEC_RESPONSE;
        }
      
        /// <summary>
        /// ENABLE_ACCESSSPEC message call.
        /// </summary>
        /// <param name="msg">MSG_ENABLE_ACCESSSPEC to send to reader.</param>
        /// <param name="timeoutMilliseconds">Fuction call time out in milliseconds.</param>
        /// <returns>MSG_ENABLE_ACCESSSPEC_RESPONSE</returns>
        public async Task<MSG_ENABLE_ACCESSSPEC_RESPONSE> ENABLE_ACCESSSPEC(MSG_ENABLE_ACCESSSPEC msg, int timeoutMilliseconds)
        {
            return await TransactAsync(msg, (UInt16)ENUM_LLRP_MSG_TYPE.ENABLE_ACCESSSPEC_RESPONSE, 0, 0, timeoutMilliseconds).ConfigureAwait(false) as MSG_ENABLE_ACCESSSPEC_RESPONSE;
        }
        /// <summary>
        /// ENABLE_ACCESSSPEC message call.
        /// </summary>
        /// <param name="msg">MSG_ENABLE_ACCESSSPEC to send to reader.</param>
        /// <returns>MSG_ENABLE_ACCESSSPEC_RESPONSE</returns>
        public async Task<MSG_ENABLE_ACCESSSPEC_RESPONSE> ENABLE_ACCESSSPEC(MSG_ENABLE_ACCESSSPEC msg)
        {
            return await TransactAsync(msg, (UInt16)ENUM_LLRP_MSG_TYPE.ENABLE_ACCESSSPEC_RESPONSE, 0, 0, ResponseTimeout).ConfigureAwait(false) as MSG_ENABLE_ACCESSSPEC_RESPONSE;
        }
      
        /// <summary>
        /// DISABLE_ACCESSSPEC message call.
        /// </summary>
        /// <param name="msg">MSG_DISABLE_ACCESSSPEC to send to reader.</param>
        /// <param name="timeoutMilliseconds">Fuction call time out in milliseconds.</param>
        /// <returns>MSG_DISABLE_ACCESSSPEC_RESPONSE</returns>
        public async Task<MSG_DISABLE_ACCESSSPEC_RESPONSE> DISABLE_ACCESSSPEC(MSG_DISABLE_ACCESSSPEC msg, int timeoutMilliseconds)
        {
            return await TransactAsync(msg, (UInt16)ENUM_LLRP_MSG_TYPE.DISABLE_ACCESSSPEC_RESPONSE, 0, 0, timeoutMilliseconds).ConfigureAwait(false) as MSG_DISABLE_ACCESSSPEC_RESPONSE;
        }
        /// <summary>
        /// DISABLE_ACCESSSPEC message call.
        /// </summary>
        /// <param name="msg">MSG_DISABLE_ACCESSSPEC to send to reader.</param>
        /// <returns>MSG_DISABLE_ACCESSSPEC_RESPONSE</returns>
        public async Task<MSG_DISABLE_ACCESSSPEC_RESPONSE> DISABLE_ACCESSSPEC(MSG_DISABLE_ACCESSSPEC msg)
        {
            return await TransactAsync(msg, (UInt16)ENUM_LLRP_MSG_TYPE.DISABLE_ACCESSSPEC_RESPONSE, 0, 0, ResponseTimeout).ConfigureAwait(false) as MSG_DISABLE_ACCESSSPEC_RESPONSE;
        }
      
        /// <summary>
        /// GET_ACCESSSPECS message call.
        /// </summary>
        /// <param name="msg">MSG_GET_ACCESSSPECS to send to reader.</param>
        /// <param name="timeoutMilliseconds">Fuction call time out in milliseconds.</param>
        /// <returns>MSG_GET_ACCESSSPECS_RESPONSE</returns>
        public async Task<MSG_GET_ACCESSSPECS_RESPONSE> GET_ACCESSSPECS(MSG_GET_ACCESSSPECS msg, int timeoutMilliseconds)
        {
            return await TransactAsync(msg, (UInt16)ENUM_LLRP_MSG_TYPE.GET_ACCESSSPECS_RESPONSE, 0, 0, timeoutMilliseconds).ConfigureAwait(false) as MSG_GET_ACCESSSPECS_RESPONSE;
        }
        /// <summary>
        /// GET_ACCESSSPECS message call.
        /// </summary>
        /// <param name="msg">MSG_GET_ACCESSSPECS to send to reader.</param>
        /// <returns>MSG_GET_ACCESSSPECS_RESPONSE</returns>
        public async Task<MSG_GET_ACCESSSPECS_RESPONSE> GET_ACCESSSPECS(MSG_GET_ACCESSSPECS msg)
        {
            return await TransactAsync(msg, (UInt16)ENUM_LLRP_MSG_TYPE.GET_ACCESSSPECS_RESPONSE, 0, 0, ResponseTimeout).ConfigureAwait(false) as MSG_GET_ACCESSSPECS_RESPONSE;
        }
      
        /// <summary>
        /// CLIENT_REQUEST_OP message response.
        /// </summary>
        /// <param name="msg">MSG_CLIENT_REQUEST_OP to send to reader.</param>
        public async Task CLIENT_REQUEST_OP_RESPONSE(MSG_CLIENT_REQUEST_OP_RESPONSE msg)
        {
            await SendAsync(msg).ConfigureAwait(false);
        }
      
        /// <summary>
        /// GET_READER_CONFIG message call.
        /// </summary>
        /// <param name="msg">MSG_GET_READER_CONFIG to send to reader.</param>
        /// <param name="timeoutMilliseconds">Fuction call time out in milliseconds.</param>
        /// <returns>MSG_GET_READER_CONFIG_RESPONSE</returns>
        public async Task<MSG_GET_READER_CONFIG_RESPONSE> GET_READER_CONFIG(MSG_GET_READER_CONFIG msg, int timeoutMilliseconds)
        {
            return await TransactAsync(msg, (UInt16)ENUM_LLRP_MSG_TYPE.GET_READER_CONFIG_RESPONSE, 0, 0, timeoutMilliseconds).ConfigureAwait(false) as MSG_GET_READER_CONFIG_RESPONSE;
        }
        /// <summary>
        /// GET_READER_CONFIG message call.
        /// </summary>
        /// <param name="msg">MSG_GET_READER_CONFIG to send to reader.</param>
        /// <returns>MSG_GET_READER_CONFIG_RESPONSE</returns>
        public async Task<MSG_GET_READER_CONFIG_RESPONSE> GET_READER_CONFIG(MSG_GET_READER_CONFIG msg)
        {
            return await TransactAsync(msg, (UInt16)ENUM_LLRP_MSG_TYPE.GET_READER_CONFIG_RESPONSE, 0, 0, ResponseTimeout).ConfigureAwait(false) as MSG_GET_READER_CONFIG_RESPONSE;
        }
      
        /// <summary>
        /// SET_READER_CONFIG message call.
        /// </summary>
        /// <param name="msg">MSG_SET_READER_CONFIG to send to reader.</param>
        /// <param name="timeoutMilliseconds">Fuction call time out in milliseconds.</param>
        /// <returns>MSG_SET_READER_CONFIG_RESPONSE</returns>
        public async Task<MSG_SET_READER_CONFIG_RESPONSE> SET_READER_CONFIG(MSG_SET_READER_CONFIG msg, int timeoutMilliseconds)
        {
            return await TransactAsync(msg, (UInt16)ENUM_LLRP_MSG_TYPE.SET_READER_CONFIG_RESPONSE, 0, 0, timeoutMilliseconds).ConfigureAwait(false) as MSG_SET_READER_CONFIG_RESPONSE;
        }
        /// <summary>
        /// SET_READER_CONFIG message call.
        /// </summary>
        /// <param name="msg">MSG_SET_READER_CONFIG to send to reader.</param>
        /// <returns>MSG_SET_READER_CONFIG_RESPONSE</returns>
        public async Task<MSG_SET_READER_CONFIG_RESPONSE> SET_READER_CONFIG(MSG_SET_READER_CONFIG msg)
        {
            return await TransactAsync(msg, (UInt16)ENUM_LLRP_MSG_TYPE.SET_READER_CONFIG_RESPONSE, 0, 0, ResponseTimeout).ConfigureAwait(false) as MSG_SET_READER_CONFIG_RESPONSE;
        }
      
        /// <summary>
        /// CLOSE_CONNECTION message call.
        /// </summary>
        /// <param name="msg">MSG_CLOSE_CONNECTION to send to reader.</param>
        /// <param name="timeoutMilliseconds">Fuction call time out in milliseconds.</param>
        /// <returns>MSG_CLOSE_CONNECTION_RESPONSE</returns>
        public async Task<MSG_CLOSE_CONNECTION_RESPONSE> CLOSE_CONNECTION(MSG_CLOSE_CONNECTION msg, int timeoutMilliseconds)
        {
            return await TransactAsync(msg, (UInt16)ENUM_LLRP_MSG_TYPE.CLOSE_CONNECTION_RESPONSE, 0, 0, timeoutMilliseconds).ConfigureAwait(false) as MSG_CLOSE_CONNECTION_RESPONSE;
        }
        /// <summary>
        /// CLOSE_CONNECTION message call.
        /// </summary>
        /// <param name="msg">MSG_CLOSE_CONNECTION to send to reader.</param>
        /// <returns>MSG_CLOSE_CONNECTION_RESPONSE</returns>
        public async Task<MSG_CLOSE_CONNECTION_RESPONSE> CLOSE_CONNECTION(MSG_CLOSE_CONNECTION msg)
        {
            return await TransactAsync(msg, (UInt16)ENUM_LLRP_MSG_TYPE.CLOSE_CONNECTION_RESPONSE, 0, 0, CloseTimeout).ConfigureAwait(false) as MSG_CLOSE_CONNECTION_RESPONSE;
        }
      
        /// <summary>
        /// Get LLRP report
        /// </summary>
        /// <param name="msg">Message to be sent to reader.</param>
        public async Task GET_REPORT(MSG_GET_REPORT msg)
        {
            await SendAsync(msg).ConfigureAwait(false);
        }
      
        /// <summary>
        /// Keep alive acknowledgement
        /// </summary>
        /// <param name="msg">Message to be sent to reader.</param>
        public async Task KEEPALIVE_ACK(MSG_KEEPALIVE_ACK msg)
        {
            await SendAsync(msg).ConfigureAwait(false);
        }
      

        /// <summary>
        /// Enable events and reports
        /// </summary>
        /// <param name="msg">Message to be sent to reader.</param>
        public async Task ENABLE_EVENTS_AND_REPORTS(MSG_ENABLE_EVENTS_AND_REPORTS msg)
        {
            await SendAsync(msg).ConfigureAwait(false);
        }
      
    }
}
  