
/*
 ***************************************************************************
 *  Copyright 2007 Impinj, Inc.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 ***************************************************************************
 */

/*
***************************************************************************
 * File Name:       LLRPDataType.cs
 *
 * Author:          Impinj
 * Organization:    Impinj
 * Date:            18 June, 2008
 *
 * Description:     This file contains supporting data types for encoding/
 *                  decoding LLRP messages/parameters
***************************************************************************
*/


using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace EA.Impinj.LLRP
{
    public interface ILLRPMessage
    {
        UInt16 MsgVersion { get; }
        UInt16 MsgTypeID { get; }
        UInt32 MsgLength { get; }
        UInt32 MsgID { get; }

        DateTime MsgReceivedTimestamp { get; set; }

        BitList ToBitList();
        XElement ToXml();
    }

    public interface ILLRPCustomMessage : ILLRPMessage
    {
        UInt32 VendorIdentifier { get; }
        Byte MessageSubtype { get; }
    }

    public interface ILLRPResponseMessage : ILLRPMessage
    {
        PARAM_LLRPStatus LLRPStatus { get; }
    }

    /// <summary>
    /// Parameter interface
    /// </summary>
    public interface ILLRPParameter
    {
        UInt16 ParamTypeID { get; }
        UInt16 ParamLength { get; }

        void ToBitList(BitList bitList);
        XElement ToXml();
    }

    public interface ILLRPCustomParameter : ILLRPParameter
    {
        UInt32 VendorIdentifier { get; }
        UInt32 ParameterSubtype { get; }
    }

    /// <summary>
    /// LLRP Message
    /// </summary>
    public abstract class LLRPMessage : ILLRPMessage
    {
        public const UInt16 TYPE_ID = 0;

        public UInt16 MsgVersion { get; protected set; } = 1;
        public UInt16 MsgTypeID { get; protected set; }
        public UInt32 MsgLength { get; set; }
        public UInt32 MsgID { get; set; }

        public DateTime MsgReceivedTimestamp { get; set; }

        protected LLRPMessage(UInt32 msgId)
        {
            MsgID = msgId;
        }

        public abstract XElement ToXml();
        public static LLRPMessage FromXml(ICustomParameterParser parser, XElement root)
        {
            if (parser == null)
                throw new ArgumentNullException(nameof(parser));
            if (root == null)
                throw new ArgumentNullException(nameof(root));

            throw new NotImplementedException();
        }
        public abstract BitList ToBitList();
        public static LLRPMessage FromBitList(ICustomParameterDecoder decoder, BitList bitList, ref int cursor, int length)
        {
            if (decoder == null)
                throw new ArgumentNullException(nameof(decoder));
            if (bitList == null)
                throw new ArgumentNullException(nameof(bitList));
            if (cursor < 0)
                throw new ArgumentOutOfRangeException(nameof(cursor));
            if (length < 0)
                throw new ArgumentOutOfRangeException(nameof(length));

            throw new NotImplementedException();
        }

        public override string ToString() => ToXmlString();

        public string ToXmlString()
        {
            XDocument xDoc = new XDocument(ToXml());
            return xDoc.ToString();
        }

        public static LLRPMessage FromXmlString(ICustomParameterParser parser, string xmlString)
        {
            XDocument xDoc = XDocument.Load(xmlString);
            return FromXml(parser, xDoc.Root);
        }
    }

    /// <summary>
    /// LLRP parameter
    /// </summary>
    public abstract class LLRPParameter : ILLRPParameter
    {
        public const UInt16 TYPE_ID = 0;

        public UInt16 ParamTypeID { get; protected set; }
        public UInt16 ParamLength { get; protected set; }

        protected bool ParamIsTVCoding { get; set; }

        public abstract XElement ToXml();
        public static LLRPParameter FromXml(ICustomParameterParser parser, XElement root)
        {
            if (parser == null)
                throw new ArgumentNullException(nameof(parser));
            if (root == null)
                throw new ArgumentNullException(nameof(root));

            throw new NotImplementedException();
        }
        public abstract void ToBitList(BitList bitList);
        public static LLRPParameter FromBitList(ICustomParameterDecoder decoder, BitList bitList, ref int cursor, int length)
        {
            if (decoder == null)
                throw new ArgumentNullException(nameof(decoder));
            if (bitList == null)
                throw new ArgumentNullException(nameof(bitList));
            if (cursor < 0)
                throw new ArgumentOutOfRangeException(nameof(cursor));
            if (length < 0)
                throw new ArgumentOutOfRangeException(nameof(length));

            throw new NotImplementedException();
        }

        public override string ToString() => ToXmlString();

        public string ToXmlString()
        {
            XDocument xDoc = new XDocument(ToXml());
            return xDoc.ToString();
        }

        public static LLRPParameter FromXmlString(ICustomParameterParser parser, string xmlString)
        {
            XDocument xDoc = XDocument.Load(xmlString);
            return FromXml(parser, xDoc.Root);
        }
    }

    /// <summary>
    /// BitList used to store "u1v" type
    /// </summary>
    public class BitList : List<Boolean>
    {
        public BitList() : base() { }
        public BitList(IEnumerable<bool> collection) : base(collection) { }
        public BitList(int capacity) : base(capacity) { }

        public override string ToString() => LLRPUtil.BitListToTextString(this);
    }

    /// <summary>
    /// Used to store "u8v" "endofbytes"
    /// </summary>
    public class ByteList : List<Byte>
    {
        public ByteList() : base() { }
        public ByteList(IEnumerable<Byte> collection) : base(collection) { }
        public ByteList(int capacity) : base(capacity) { }

        public override string ToString() => LLRPUtil.ByteListToTextString(this);
    }

    /// <summary>
    /// Used to store "s8v"
    /// </summary>
    public class SByteList : List<SByte>
    {
        public SByteList() : base() { }
        public SByteList(IEnumerable<SByte> collection) : base(collection) { }
        public SByteList(int capacity) : base(capacity) { }

        public override string ToString() => LLRPUtil.SByteListToTextString(this);
    }

    /// <summary>
    /// Used to store "u16v"
    /// </summary>
    public class UInt16List : List<UInt16>
    {
        public UInt16List() : base() { }
        public UInt16List(IEnumerable<UInt16> collection) : base(collection) { }
        public UInt16List(int capacity) : base(capacity) { }

        public override string ToString() => LLRPUtil.UInt16ListToTextString(this);
    }

    /// <summary>
    /// Used to store "s16v"
    /// </summary>
    public class Int16List : List<Int16>
    {
        public Int16List() : base() { }
        public Int16List(IEnumerable<Int16> collection) : base(collection) { }
        public Int16List(int capacity) : base(capacity) { }

        public override string ToString() => LLRPUtil.Int16ListToTextString(this);
    }

    /// <summary>
    /// Used to store "u32v"
    /// </summary>
    public class UInt32List : List<UInt32>
    {
        public UInt32List() : base() { }
        public UInt32List(IEnumerable<UInt32> collection) : base(collection) { }
        public UInt32List(int capacity) : base(capacity) { }

        public override string ToString() => LLRPUtil.UInt32ListToTextString(this);
    }

    /// <summary>
    /// Used to store "s32v"
    /// </summary>
    public class Int32List : List<Int32>
    {
        public Int32List() : base() { }
        public Int32List(IEnumerable<Int32> collection) : base(collection) { }
        public Int32List(int capacity) : base(capacity) { }

        public override string ToString() => LLRPUtil.Int32ListToTextString(this);
    }

    /// <summary>
    /// Used to store "u64v"
    /// </summary>
    public class UInt64List : List<UInt64>
    {
        public UInt64List() : base() { }
        public UInt64List(IEnumerable<UInt64> collection) : base(collection) { }
        public UInt64List(int capacity) : base(capacity) { }

        public override string ToString() => LLRPUtil.UInt64ListToTextString(this);
    }

    /// <summary>
    /// Used to store "s64v"
    /// </summary>
    public class Int64List : List<Int64>
    {
        public Int64List() : base() { }
        public Int64List(IEnumerable<Int64> collection) : base(collection) { }
        public Int64List(int capacity) : base(capacity) { }

        public override string ToString() => LLRPUtil.Int64ListToTextString(this);
    }

    /// <summary>
    /// Used to store "u2"
    /// </summary>
    public class TwoBits
    {
        private readonly bool[] bits;

        public TwoBits(bool bit1, bool bit2)
        {
            bits = new bool[] { bit1, bit2 };
        }

        public TwoBits() : this(false, false) { }

        public TwoBits(UInt16 value)
            : this((value & 0x0002) == 2, (value & 0x0001) == 1) { }

        public bool this[int index]
        {
            get => bits[index];
            set => bits[index] = value;
        }

        public override string ToString() => LLRPUtil.TwoBitsToTextString(this);
    }

    /// <summary>
    /// Custom Parameter list
    /// </summary>
    public class LLRPCustomParameterList : List<ILLRPCustomParameter>
    {
        public int Length => Count;
    }

    /// <summary>
    /// LLRP parameter list.
    /// </summary>
    public class LLRPParameterList : List<ILLRPParameter>
    {
        public int Length => Count;
    }
}
