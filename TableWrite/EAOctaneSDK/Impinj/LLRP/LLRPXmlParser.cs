
/*
***************************************************************************
*  Copyright 2007 Impinj, Inc.
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*
***************************************************************************
*/


/*
***************************************************************************
*
*  This code is generated by Impinj LLRP .Net generator. Modification is not
*  recommended.
*
***************************************************************************
*/

/*
***************************************************************************
* File Name:       LLRPXmlParser.cs
*
* Version:         1.0
* Author:          Impinj
* Organization:    Impinj
* Date:            Jan. 18, 2008
*
* Description:     This file contains general XML parser for LLRP messages
***************************************************************************
*/

using System.Xml.Linq;

namespace EA.Impinj.LLRP
{
    public interface ICustomParameterParser
    {
        ILLRPCustomParameter ParseCustomParameter(XElement root);
    }

    /// <summary>
    /// LLRP XML Parser
    /// </summary>
    public class LLRPXmlParser : ICustomParameterParser
    {
        /// <summary>
        /// Parse XML string to LLRP message
        /// </summary>
        /// <param name="xmlstr">XML string to be parsed.</param>
        /// <param name="msg">LLRP message. output</param>
        /// <param name="type">LLRP message type. output</param>
        public ILLRPMessage Parse(string xmlstr)
        {
            XNamespace xmlns = XNamespace.Get(LLRPConstants.NAMESPACE_URI);

            XDocument xDoc = XDocument.Parse(xmlstr);
            XElement root = xDoc.Root;

            if (root.Name.Namespace == xmlns)
            {
                switch (root.Name.LocalName)
                {
                
                    case "CUSTOM_MESSAGE":
                        return MSG_CUSTOM_MESSAGE.FromXml(this, root);
                
                    case "GET_READER_CAPABILITIES":
                        return MSG_GET_READER_CAPABILITIES.FromXml(this, root);
                
                    case "GET_READER_CAPABILITIES_RESPONSE":
                        return MSG_GET_READER_CAPABILITIES_RESPONSE.FromXml(this, root);
                
                    case "ADD_ROSPEC":
                        return MSG_ADD_ROSPEC.FromXml(this, root);
                
                    case "ADD_ROSPEC_RESPONSE":
                        return MSG_ADD_ROSPEC_RESPONSE.FromXml(this, root);
                
                    case "DELETE_ROSPEC":
                        return MSG_DELETE_ROSPEC.FromXml(this, root);
                
                    case "DELETE_ROSPEC_RESPONSE":
                        return MSG_DELETE_ROSPEC_RESPONSE.FromXml(this, root);
                
                    case "START_ROSPEC":
                        return MSG_START_ROSPEC.FromXml(this, root);
                
                    case "START_ROSPEC_RESPONSE":
                        return MSG_START_ROSPEC_RESPONSE.FromXml(this, root);
                
                    case "STOP_ROSPEC":
                        return MSG_STOP_ROSPEC.FromXml(this, root);
                
                    case "STOP_ROSPEC_RESPONSE":
                        return MSG_STOP_ROSPEC_RESPONSE.FromXml(this, root);
                
                    case "ENABLE_ROSPEC":
                        return MSG_ENABLE_ROSPEC.FromXml(this, root);
                
                    case "ENABLE_ROSPEC_RESPONSE":
                        return MSG_ENABLE_ROSPEC_RESPONSE.FromXml(this, root);
                
                    case "DISABLE_ROSPEC":
                        return MSG_DISABLE_ROSPEC.FromXml(this, root);
                
                    case "DISABLE_ROSPEC_RESPONSE":
                        return MSG_DISABLE_ROSPEC_RESPONSE.FromXml(this, root);
                
                    case "GET_ROSPECS":
                        return MSG_GET_ROSPECS.FromXml(this, root);
                
                    case "GET_ROSPECS_RESPONSE":
                        return MSG_GET_ROSPECS_RESPONSE.FromXml(this, root);
                
                    case "ADD_ACCESSSPEC":
                        return MSG_ADD_ACCESSSPEC.FromXml(this, root);
                
                    case "ADD_ACCESSSPEC_RESPONSE":
                        return MSG_ADD_ACCESSSPEC_RESPONSE.FromXml(this, root);
                
                    case "DELETE_ACCESSSPEC":
                        return MSG_DELETE_ACCESSSPEC.FromXml(this, root);
                
                    case "DELETE_ACCESSSPEC_RESPONSE":
                        return MSG_DELETE_ACCESSSPEC_RESPONSE.FromXml(this, root);
                
                    case "ENABLE_ACCESSSPEC":
                        return MSG_ENABLE_ACCESSSPEC.FromXml(this, root);
                
                    case "ENABLE_ACCESSSPEC_RESPONSE":
                        return MSG_ENABLE_ACCESSSPEC_RESPONSE.FromXml(this, root);
                
                    case "DISABLE_ACCESSSPEC":
                        return MSG_DISABLE_ACCESSSPEC.FromXml(this, root);
                
                    case "DISABLE_ACCESSSPEC_RESPONSE":
                        return MSG_DISABLE_ACCESSSPEC_RESPONSE.FromXml(this, root);
                
                    case "GET_ACCESSSPECS":
                        return MSG_GET_ACCESSSPECS.FromXml(this, root);
                
                    case "GET_ACCESSSPECS_RESPONSE":
                        return MSG_GET_ACCESSSPECS_RESPONSE.FromXml(this, root);
                
                    case "CLIENT_REQUEST_OP":
                        return MSG_CLIENT_REQUEST_OP.FromXml(this, root);
                
                    case "CLIENT_REQUEST_OP_RESPONSE":
                        return MSG_CLIENT_REQUEST_OP_RESPONSE.FromXml(this, root);
                
                    case "GET_READER_CONFIG":
                        return MSG_GET_READER_CONFIG.FromXml(this, root);
                
                    case "GET_READER_CONFIG_RESPONSE":
                        return MSG_GET_READER_CONFIG_RESPONSE.FromXml(this, root);
                
                    case "SET_READER_CONFIG":
                        return MSG_SET_READER_CONFIG.FromXml(this, root);
                
                    case "SET_READER_CONFIG_RESPONSE":
                        return MSG_SET_READER_CONFIG_RESPONSE.FromXml(this, root);
                
                    case "CLOSE_CONNECTION":
                        return MSG_CLOSE_CONNECTION.FromXml(this, root);
                
                    case "CLOSE_CONNECTION_RESPONSE":
                        return MSG_CLOSE_CONNECTION_RESPONSE.FromXml(this, root);
                
                    case "GET_REPORT":
                        return MSG_GET_REPORT.FromXml(this, root);
                
                    case "RO_ACCESS_REPORT":
                        return MSG_RO_ACCESS_REPORT.FromXml(this, root);
                
                    case "KEEPALIVE":
                        return MSG_KEEPALIVE.FromXml(this, root);
                
                    case "KEEPALIVE_ACK":
                        return MSG_KEEPALIVE_ACK.FromXml(this, root);
                
                    case "READER_EVENT_NOTIFICATION":
                        return MSG_READER_EVENT_NOTIFICATION.FromXml(this, root);
                
                    case "ENABLE_EVENTS_AND_REPORTS":
                        return MSG_ENABLE_EVENTS_AND_REPORTS.FromXml(this, root);
                
                    case "ERROR_MESSAGE":
                        return MSG_ERROR_MESSAGE.FromXml(this, root);
                
                    default:
                        return ParseCustomMessage(root);
                }
            }
            else
            {
                return ParseCustomMessage(root);
            }
        }

        protected virtual ILLRPCustomMessage ParseCustomMessage(XElement root)
        {
            return null;
        }

        public virtual ILLRPCustomParameter ParseCustomParameter(XElement root)
        {
            XNamespace xmlns = XNamespace.Get(LLRPConstants.NAMESPACE_URI);

            if (root.Name.Namespace == xmlns)
            {
                switch (root.Name.LocalName)
                {
                    case "Custom":
                        return PARAM_Custom.FromXml(this, root);
                }
            }

            return null;
        }

    }
}
  