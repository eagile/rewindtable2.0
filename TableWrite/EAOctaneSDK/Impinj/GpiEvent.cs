using System;

namespace EA.Impinj
{
    /// <summary>
    /// Class used to encapsulate the details of a
    /// <see cref="E:Impinj.OctaneSdk.ImpinjReader.GpiChanged" /> reader event.
    /// </summary>
    public class GpiEvent : EventArgs
    {
        /// <summary>Timestamp of when the host received the event.</summary>
        public DateTime HostReceivedTime { get; set; }
        /// <summary>Timestamp of the event.</summary>
        public DateTime ReaderTimestamp { get; set; }
        /// <summary>Parameter defining GPI port number for event</summary>
        public ushort PortNumber { get; set; }
        /// <summary>Parameter defining GPI state for event</summary>
        public bool State { get; set; }
    }
}
