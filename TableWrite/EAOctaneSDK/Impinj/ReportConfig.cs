using System.Collections.Generic;

namespace EA.Impinj
{
    /// <summary>
    /// Class for configuring the tag inventory reports returned by the reader.
    /// </summary>
    public class ReportConfig
    {
        /// <summary>Include Antenna Port Number in the inventory report</summary>
        public bool IncludeAntennaPortNumber { get; set; }

        /// <summary>Include the Channel ID in the report</summary>
        public bool IncludeChannel { get; set; }

        /// <summary>Include the first seen timestamp of the tag.</summary>
        public bool IncludeFirstSeenTime { get; set; }

        /// <summary>Include the last seen timestamp of the tag.</summary>
        public bool IncludeLastSeenTime { get; set; }

        /// <summary>Include the Peak RSSI for the tag response.</summary>
        public bool IncludePeakRssi { get; set; }

        /// <summary>Include the number of times a tag was seen within that report</summary>
        public bool IncludeSeenCount { get; set; }

        /// <summary>Include the TID using FastID on Monza 4 and Monza 5 chips</summary>
        public bool IncludeFastId { get; set; }

        /// <summary>Include the Phase Angle with each tag read.</summary>
        public bool IncludePhaseAngle { get; set; }

        /// <summary>Include the Doppler Frequency shift over the transmission of the EPC.
        /// NOTE : ReaderMode must be set to DenseReaderM8 to receive valid Doppler values.
        /// </summary>
        public bool IncludeDopplerFrequency { get; set; }

        /// <summary>Include the GPS coordinates with each read. Must require proper external hardware.</summary>
        public bool IncludeGpsCoordinates { get; set; }

        /// <summary>Include the Protocol Control (PC) bits with each tag read.</summary>
        public bool IncludePcBits { get; set; }

        /// <summary>Enable the reporting of the CRC bits for the EPC.</summary>
        public bool IncludeCrc { get; set; }

        /// <summary>
        /// The Mode in which inventory tag reports are generated.
        /// </summary>
        public ReportMode Mode { get; set; }

        /// <summary>
        /// The Mode in which inventory tag reports are buffered.
        /// </summary>
        public ReportBufferMode BufferMode { get; set; }

        /// <summary>A list of up to two optimized read operations.</summary>
        public List<TagReadOp> OptimizedReadOps { get; set; }

        public ReportConfig()
        {
            OptimizedReadOps = new List<TagReadOp>();
        }
    }
}
