using System;

namespace EA.Impinj
{
    /// <summary>
    /// Used to set Monza QT tag chip features.
    /// Through QT technology, a tag owner/user can maintain two data profiles
    /// (one public, one private), allowing confidentiality of business-sensitive
    /// data while assuring consumers of privacy. The tag owner stores
    /// confidential data in the private data profile, which is protected by a
    /// password-controlled command and may only be accessed at very short read
    /// distances.
    /// </summary>
    public class TagQtSetOp : TagOp
    {
        /// <summary>
        /// Tag access password. Required only if the tag's access password
        /// has been set.
        /// </summary>
        public TagData AccessPassword
        {
            get => _accessPassword;
            set => _accessPassword = value ?? new TagData();
        }
        private TagData _accessPassword = new TagData();

        /// <summary>
        /// Determines whether switching to the tag private Data Profile
        /// should be persistent, or whether the tag should revert to
        /// the private state once the tag loses power.
        /// </summary>
        public QtPersistence Persistence { get; set; }

        /// <summary>
        /// Determines whether to access the tag private or public
        /// data profile.
        /// </summary>
        public QtDataProfile DataProfile { get; set; }

        /// <summary>
        /// Determines whether to reduce tag access range down to a meter
        /// or so, or not.
        /// </summary>
        public QtAccessRange AccessRange { get; set; }



        public TagQtSetOp() : this(GetNextTagOpID()) { }
        public TagQtSetOp(UInt16 tagOpId) : base(tagOpId) { }
    }
}
