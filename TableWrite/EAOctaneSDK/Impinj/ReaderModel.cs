namespace EA.Impinj
{
    /// <summary>
    /// All the possible reader models, mapped to their model numbers.
    /// </summary>
    public enum ReaderModel
    {
        /// <summary>Unknown reader model</summary>
        Unknown = 0,
        /// <summary>Speedway R220</summary>
        SpeedwayR220 = 2001001, // 0x001E8869
        /// <summary>Speedway R420</summary>
        SpeedwayR420 = 2001002, // 0x001E886A
        /// <summary>xPortal</summary>
        XPortal = 2001003, // 0x001E886B
        /// <summary>xArrayWM</summary>
        XArrayWM = 2001004, // 0x001E886C
        /// <summary>xArrayEAP</summary>
        XArrayEAP = 2001006, // 0x001E886E
        /// <summary>xArray</summary>
        XArray = 2001007, // 0x001E886F
        /// <summary>xSpan</summary>
        XSpan = 2001008, // 0x001E8870
        /// <summary>Speedway R120</summary>
        SpeedwayR120 = 2001009, // 0x001E8871
        /// <summary>Speedway-based xSpan</summary>
        XSpanR703 = 2001010, // 0x001E8872
        /// <summary>R700</summary>
        R700 = 2001052, // 0x001E889C
        /// <summary>R700-based xSpan</summary>
        XSpanR705 = 2001060, // 0x001E88A4
    }
}
