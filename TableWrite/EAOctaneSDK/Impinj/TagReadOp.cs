using System;

namespace EA.Impinj
{
    /// <summary>Class used to specify a tag read operation.</summary>
    public class TagReadOp : TagOp
    {
        /// <summary>
        /// Parameter defining Access Password to use for operation.
        /// </summary>
        public TagData AccessPassword
        {
            get => _accessPassword;
            set => _accessPassword = value ?? new TagData();
        }
        private TagData _accessPassword = new TagData();

        /// <summary>
        /// Parameter defining Memory Bank to access for operation.
        /// </summary>
        public MemoryBank MemoryBank { get; set; }

        /// <summary>
        /// Parameter defining Word Pointer to access for operation.
        /// </summary>
        public ushort WordPointer { get; set; }

        /// <summary>
        /// Parameter defining the number of words to access for operation.
        /// </summary>
        public ushort WordCount { get; set; }



        public TagReadOp() : this(GetNextTagOpID()) { }
        public TagReadOp(UInt16 tagOpId) : base(tagOpId) { }
    }
}
