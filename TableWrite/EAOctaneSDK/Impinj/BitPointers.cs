namespace EA.Impinj
{
    /// <summary>Helper class used when defining EPC memory accesses.</summary>
    public static class BitPointers
    {
        /// <summary>Constant used to point to the first bit of EPC data.</summary>
        public const ushort Epc = 32;
    }
}
