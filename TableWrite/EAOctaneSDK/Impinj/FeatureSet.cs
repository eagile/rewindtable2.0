using EA.Impinj.LLRP;
using EA.Impinj.LLRP.Impinj;
using System.Collections.Generic;

namespace EA.Impinj
{
    /// <summary>
    /// Container class used to encapsulate the features supported by a given
    /// Impinj reader.  An object of this type is returned by a call to
    /// <see cref="M:Impinj.OctaneSdk.ImpinjReader.QueryFeatureSet" />.
    /// </summary>
    public class FeatureSet
    {
        /// <summary>
        /// EPC Global model number identifier for the Impinj Reader,
        /// e.g. 2001002 for a 4 port Speedway Revolution.
        /// </summary>
        public uint ModelNumber { get; set; }

        /// <summary>
        /// Model name of the Impinj Reader, e.g. "Speedway R420" for a
        /// 4 port Speedway Revolution.
        /// </summary>
        public string ModelName { get; set; }

        /// <summary>Reader model of the ImpinjReader</summary>
        public ReaderModel ReaderModel { get; set; }

        /// <summary>Serial number of the Impinj reader.</summary>
        public string SerialNumber { get; set; }

        /// <summary>Firmware version running on Impinj reader.</summary>
        public string FirmwareVersion { get; set; }

        /// <summary>Number of antennas supported by Impinj reader.</summary>
        public ushort AntennaCount { get; set; }

        /// <summary>
        /// Number of general purpose input (GPI) ports supported by Impinj reader.
        /// </summary>
        public ushort GpiCount { get; set; }

        /// <summary>
        /// Number of general purpose output (GPO) ports supported by
        /// Impinj reader.
        /// </summary>
        public ushort GpoCount { get; set; }

        /// <summary>
        /// The maximum number of tag operation sequences supported by the
        /// Impinj reader.
        /// </summary>
        public uint MaxOperationSequences { get; set; }

        /// <summary>
        /// The maximum number of individual tag operations per sequence
        /// supported by the Impinj reader.
        /// </summary>
        public uint MaxOperationsPerSequence { get; set; }

        /// <summary>The Regulatory standard supported by the reader.</summary>
        public CommunicationsStandardType CommunicationsStandard { get; set; }

        /// <summary>Always returns true.</summary>
        public bool IsTagAccessAvailable => true;

        /// <summary>Always returns true.</summary>
        public bool IsFilteringAvailable => true;

        /// <summary>The maximum number of tag select filters allowed per query.</summary>
        public int MaxTagSelectFiltersAllowed { get; set; }


        /// <summary>Always returns true.</summary>
        public bool IsGpiDebounceAvailable => true;

        /// <summary>Always returns true.</summary>
        public bool IsLinkMonitorAvailable => true;

        /// <summary>Always returns true.</summary>
        public bool IsMultiwordBlockWriteAvailable => true;

        /// <summary>Always returns true.</summary>
        public bool IsPhaseAngleReportingAvailable => true;

        /// <summary>Always returns true.</summary>
        public bool IsFastIdAvailable => true;

        /// <summary>Always returns true.</summary>
        public bool IsImpinjRssiReportingAvailable => true;

        /// <summary>
        /// Table that correlates transmit powers in dBm to an index in the
        /// reader internal power table.
        /// </summary>
        public List<TxPowerTableEntry> TxPowers { get; set; }

        /// <summary>
        /// Table that correlates a receive sensitivity in dBm to an index
        /// in the reader internal receive sensitivity table.
        /// </summary>
        public List<RxSensitivityTableEntry> RxSensitivities { get; set; }

        /// <summary>
        /// Indicates whether frequency hopping supported by the current
        /// CommunicationsStandard.
        /// </summary>
        public bool IsHoppingRegion { get; set; }

        /// <summary>Holds the transmit frequencies the reader can hop on.</summary>
        public List<double> TxFrequencies { get; private set; }

        /// <summary>
        /// A readonly list of reader modes supported by the connected reader.
        /// </summary>
        public List<ReaderMode> ReaderModes { get; }

        /// <summary>
        /// A readonly list of RF modes supported by the connected reader.
		/// This is a bad change, for now sticking with ReaderModes unless there becomes a reason not to
        /// </summary>
        //public List<uint?> RfModes { get; }

        /// <summary>Default Constructor</summary>
        public FeatureSet()
        {
        }

        public FeatureSet(MSG_GET_READER_CAPABILITIES_RESPONSE capabilities)
        {
            AntennaCount = capabilities.GeneralDeviceCapabilities.MaxNumberOfAntennaSupported;
            FirmwareVersion = capabilities.GeneralDeviceCapabilities.ReaderFirmwareVersion;
            GpiCount = capabilities.GeneralDeviceCapabilities.GPIOCapabilities.NumGPIs;
            GpoCount = capabilities.GeneralDeviceCapabilities.GPIOCapabilities.NumGPOs;
            MaxOperationSequences = capabilities.LLRPCapabilities.MaxNumAccessSpecs;
            MaxOperationsPerSequence = capabilities.LLRPCapabilities.MaxNumOpSpecsPerAccessSpec;
            ModelNumber = capabilities.GeneralDeviceCapabilities.ModelName;
            ReaderModel = (ReaderModel)ModelNumber;
            IsHoppingRegion = capabilities.RegulatoryCapabilities.UHFBandCapabilities.FrequencyInformation.Hopping;
            CommunicationsStandard = (CommunicationsStandardType)capabilities.RegulatoryCapabilities.CommunicationsStandard;
            for (int i = 0; i < capabilities.AirProtocolLLRPCapabilities.Count; i++)
            {
                if (capabilities.AirProtocolLLRPCapabilities[i] is PARAM_C1G2LLRPCapabilities c1G2LLRPCapabilities)
                {
                    MaxTagSelectFiltersAllowed = c1G2LLRPCapabilities.MaxNumSelectFiltersPerQuery;
                }
            }
            for (int i = 0; i < capabilities.Custom.Length; i++)
            {
                if (capabilities.Custom[i] is PARAM_ImpinjDetailedVersion impinjDetailedVersion)
                {
                    ModelName = impinjDetailedVersion.ModelName;
                    SerialNumber = impinjDetailedVersion.SerialNumber;
                }
            }

            ReaderModes = new List<ReaderMode>();
            UNION_AirProtocolUHFRFModeTable protocolUhfrfModeTable = capabilities.RegulatoryCapabilities.UHFBandCapabilities.AirProtocolUHFRFModeTable;
            for (int i = 0; i < protocolUhfrfModeTable.Length; i++)
            {
                if (protocolUhfrfModeTable[i] is PARAM_C1G2UHFRFModeTable g2UhfrfModeTable)
                {
                    foreach (PARAM_C1G2UHFRFModeTableEntry uhfrfModeTableEntry in g2UhfrfModeTable.C1G2UHFRFModeTableEntry)
                    {
                        ReaderModes.Add((ReaderMode)uhfrfModeTableEntry.ModeIdentifier);
                    }
                }
            }

            UInt32List frequencies = !IsHoppingRegion
                ? capabilities.RegulatoryCapabilities.UHFBandCapabilities.FrequencyInformation.FixedFrequencyTable.Frequency
                : capabilities.RegulatoryCapabilities.UHFBandCapabilities.FrequencyInformation.FrequencyHopTable[0].Frequency;
            TxFrequencies = new List<double>(frequencies.Count);
            for (int i = 0; i < frequencies.Count; i++)
            {
                TxFrequencies.Add(frequencies[i] / 1000.0);
            }

            TxPowers = new List<TxPowerTableEntry>(capabilities.RegulatoryCapabilities.UHFBandCapabilities.TransmitPowerLevelTableEntry.Length);
            foreach (PARAM_TransmitPowerLevelTableEntry powerLevelTableEntry in capabilities.RegulatoryCapabilities.UHFBandCapabilities.TransmitPowerLevelTableEntry)
            {
                TxPowerTableEntry txPowerTableEntry;
                txPowerTableEntry.Dbm = powerLevelTableEntry.TransmitPowerValue / 100.0;
                txPowerTableEntry.Index = powerLevelTableEntry.Index;
                TxPowers.Add(txPowerTableEntry);
            }

            RxSensitivities = new List<RxSensitivityTableEntry>(capabilities.GeneralDeviceCapabilities.ReceiveSensitivityTableEntry.Length);
            double sensitivityOffset = (ReaderModel == ReaderModel.R700 || ReaderModel == ReaderModel.XSpanR705) ? -90.0 : -80.0;
            foreach (PARAM_ReceiveSensitivityTableEntry sensitivityTableEntry in capabilities.GeneralDeviceCapabilities.ReceiveSensitivityTableEntry)
            {
                RxSensitivityTableEntry rxSensitivityTableEntry;
                rxSensitivityTableEntry.Dbm = sensitivityTableEntry.ReceiveSensitivityValue + sensitivityOffset;
                rxSensitivityTableEntry.Index = sensitivityTableEntry.Index;
                RxSensitivities.Add(rxSensitivityTableEntry);
            }
        }
    }
}
