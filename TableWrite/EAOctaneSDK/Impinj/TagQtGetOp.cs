using System;

namespace EA.Impinj
{
    /// <summary>Used to retrieve the current QT status of a tag.</summary>
    public class TagQtGetOp : TagOp
    {
        /// <summary>
        /// Tag access password. Required only if the
        /// tag's access password has been set.
        /// </summary>
        public TagData AccessPassword
        {
            get => _accessPassword;
            set => _accessPassword = value ?? new TagData();
        }
        private TagData _accessPassword = new TagData();



        public TagQtGetOp() : this(GetNextTagOpID()) { }
        public TagQtGetOp(UInt16 tagOpId) : base(tagOpId) { }
    }
}
