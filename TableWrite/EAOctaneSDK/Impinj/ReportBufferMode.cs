﻿namespace EA.Impinj
{
    /// <summary>
    /// Enum for defining how inventory reports are buffered before being sent to the client.
    /// </summary>
    public enum ReportBufferMode
    {
        /// <summary>
        /// Buffer tag reports internally for an optimal time period before transmission over the network.
        /// </summary>
        Normal,
        /// <summary>
        /// Send tag reports as soon as they are available.
        /// </summary>
        LowLatency,
    }
}
