namespace EA.Impinj
{
    /// <summary>
    /// Contains the results of an individual Monza QT operation, as reported in the
    /// <see cref="T:Impinj.OctaneSdk.TagOpReport" /> parameter of a
    /// <see cref="E:Impinj.OctaneSdk.ImpinjReader.TagOpComplete" /> event.
    /// </summary>
    public class TagQtSetOpResult : TagOpResult
    {
        /// <summary>The result of the tag QT operation.</summary>
        public QtSetConfigResultStatus Result { get; set; }
    }
}
