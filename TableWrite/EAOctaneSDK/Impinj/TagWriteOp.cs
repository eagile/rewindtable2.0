using System;

namespace EA.Impinj
{
    /// <summary>Class used to specify a tag write operation.</summary>
    public class TagWriteOp : TagOp
    {
        /// <summary>
        /// Tag access password.  Required only if the tag target memory bank
        /// is locked and an access password has been set.
        /// </summary>
        public TagData AccessPassword
        {
            get => _accessPassword;
            set => _accessPassword = value ?? new TagData();
        }
        private TagData _accessPassword = new TagData();

        /// <summary>
        /// Data to write into the tag memory bank, starting at the word pointed to by WordPointer.
        /// </summary>
        public TagData Data
        {
            get => _data;
            set => _data = value ?? new TagData();
        }
        private TagData _data = new TagData();

        /// <summary>Tag memory bank to write to.</summary>
        public MemoryBank MemoryBank { get; set; }

        /// <summary>
        /// Numeric pointer to first word to write to (e.g. the first word would be word 0).
        /// </summary>
        public ushort WordPointer { get; set; }



        public TagWriteOp() : this(GetNextTagOpID()) { }
        public TagWriteOp(UInt16 tagOpId) : base(tagOpId) { }
    }
}
