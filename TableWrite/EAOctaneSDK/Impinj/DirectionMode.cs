namespace EA.Impinj
{
    /// <summary>
    /// Provides the options available for tweaking the xArray's operation when in direction mode.
    /// It wraps the equivilent LTK type ENUM_ImpinjDirectionRFMode.
    /// </summary>
    public enum DirectionMode
    {
        /// <summary>
        /// Tells the xArray to operate a higher sensitivity at the expense of slower tag reads.
        /// </summary>
        HighSensitivity,
        /// <summary>
        /// Tells the xArray to perform reads more quickly at the expense of picking up fewer tags.
        /// </summary>
        HighPerformance,
    }
}
