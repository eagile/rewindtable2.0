namespace EA.Impinj
{
    /// <summary>Contains the results of a margin read operation</summary>
    public class TagMarginReadOpResult : TagOpResult
    {
        /// <summary>The results of the margin read operation.</summary>
        public MarginReadResult Result { get; set; }
    }
}
