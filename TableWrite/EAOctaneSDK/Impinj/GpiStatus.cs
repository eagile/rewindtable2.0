namespace EA.Impinj
{
    /// <summary>
    /// Class containing the status information for a general purpose input (GPI) port.
    /// </summary>
    public class GpiStatus
    {
        /// <summary>The current input level of the GPI port.</summary>
        public bool State { get; set; }

        /// <summary>The GPI port number.</summary>
        public ushort PortNumber { get; set; }
    }
}
