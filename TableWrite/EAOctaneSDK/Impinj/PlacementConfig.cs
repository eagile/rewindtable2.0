namespace EA.Impinj
{
    /// <summary>
    /// Class used to define the placement configuration for an xArray.
    /// </summary>
    public class PlacementConfig
    {
        /// <summary>
        /// Used to initialize the Array mounting height above the floor. When not specified, the default value is 457 cm.
        /// </summary>
        public ushort HeightCm { get; set; }

        /// <summary>
        /// Used to locate an xArray in a facility. The default value is 0 and is not required with a single xArray.
        /// </summary>
        public int FacilityXLocationCm { get; set; }

        /// <summary>
        /// Used to locate an xArray in a facility. The default value is 0 and is not required with a single xArray.
        /// </summary>
        public int FacilityYLocationCm { get; set; }

        /// <summary>
        /// Specifies the orientation of the array in degrees (-180 - 180) with respect to the X orientation of the device.
        /// Default is 0 and is not required with a single xArray deployment.
        /// </summary>
        public short OrientationDegrees { get; set; }
    }
}
