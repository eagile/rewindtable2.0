namespace EA.Impinj
{
    /// <summary>Contains the results of a block permalock operation.</summary>
    public class TagBlockPermalockOpResult : TagOpResult
    {
        /// <summary>The results of the block permalock operation.</summary>
        public BlockPermalockResult Result { get; set; }
    }
}
