namespace EA.Impinj
{
    /// <summary>
    /// Class for defining the trigger conditions that will prompt an Impinj
    /// reader to automatically start operation. Even when an auto-start trigger
    /// is set, the Reader may be explicitly started using the
    /// <see cref="M:Impinj.OctaneSdk.ImpinjReader.Stop" /> method.
    /// </summary>
    public class AutoStartConfig
    {
        /// <summary>
        /// Indicates whether and how the Reader should automatically start;
        /// either None, Immediate, Periodic or GpiTrigger.
        /// </summary>
        public AutoStartMode Mode { get; set; }

        /// <summary>
        /// When using GpiTrigger mode, this is the port number of a GPI; when
        /// it changes to the GpiLevel value the Reader starts. Possible values
        /// are 1-4. Note that the GPI port must be enabled
        /// </summary>
        public ushort GpiPortNumber { get; set; }

        /// <summary>
        /// When using GpiTrigger mode, and when the GPI Port changes to this
        /// level, then the reader starts. True for high, false for low.
        /// </summary>
        public bool GpiLevel { get; set; }

        /// <summary>
        /// When using Periodic mode, this specifies a time offset, or how long
        /// the Reader delays before it first starts, either after the UTCTimestamp,
        /// or after receiving the Settings. After that, the Reader will, if stopped,
        /// start according to PeriodInMs. Zero (0) means there is no initial delay.
        /// Possible values are 0 to 1000000000.
        /// </summary>
        public uint FirstDelayInMs { get; set; }

        /// <summary>
        /// When using Periodic mode, this is how often the Reader tries to start
        /// if it is stopped. The period is carefully maintained, and is relative
        /// to the previous periodic start. It has nothing to do with when the
        /// Reader stops. Zero (0) means there is no periodic start, only one
        /// start after the FirstDelayInMs. Possible values are 0 to 1000000000.
        /// </summary>
        public uint PeriodInMs { get; set; }

        /// <summary>
        /// UTC timestamp establishing the zero time for autostart periodic
        /// mode timers.  If a UTC time is specified, the the first start time
        /// is determined as (UTC time + offset).  Otherwise, the first start
        /// time is determined as (time of message receipt + offset).
        /// Subsequent start times = first start time + k * period (where, k &gt; 0)
        /// </summary>
        public ulong UtcTimestamp { get; set; }
    }
}
