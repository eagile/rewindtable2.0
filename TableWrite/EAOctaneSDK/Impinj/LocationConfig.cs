using System.Collections.Generic;

namespace EA.Impinj
{
    /// <summary>
    /// Class used to define the location configuration for an xArray.
    /// </summary>
    public class LocationConfig
    {
        private static double DEFAULT_TX_POWER_IN_DBM { get; } = 30.0;

        /// <summary>
        /// Contains the the smoothing window for averaging the location estimates. Default is 10 seconds.
        /// </summary>
        public ushort ComputeWindowSeconds { get; set; }

        /// <summary>
        /// Specifies how long a tag needs to go unseen before its removed from the location tracking. Default is 20 seconds.
        /// </summary>
        public ushort TagAgeIntervalSeconds { get; set; }

        /// <summary>
        /// Specifies how often to send out a location report. Default is 5 seconds.
        /// </summary>
        public ushort UpdateIntervalSeconds { get; set; }

        /// <summary>Enables or disables the location update report.</summary>
        public bool UpdateReportEnabled { get; set; }

        /// <summary>Enables or disables the location entry report.</summary>
        public bool EntryReportEnabled { get; set; }

        /// <summary>Enables or disables the location exit report.</summary>
        public bool ExitReportEnabled { get; set; }

        /// <summary>Enables or disables the diagnostic report.</summary>
        public bool DiagnosticReportEnabled { get; set; }

        /// <summary>
        /// If true, the reader will make antennas use their maximum transmit power.
        /// If false, the TxPowerInDbm value will be used as the antenna transmit power.
        /// </summary>
        public bool MaxTxPower { get; set; }

        /// <summary>Sets the antenna transmit power in dbm</summary>
        public double TxPowerInDbm { get; set; }

        /// <summary>
        /// Antennas can be disabled when configuring location. Set the
        /// list of currently disabled antennas.
        /// </summary>
        public List<ushort> DisabledAntennaList { get; set; }

        /// <summary>
        /// Custom location algorithm control parameters can be set when configuring location. Sets the list
        /// of key-value pairs to pass on to the location algorithm.
        /// </summary>
        public List<uint> LocationAlgorithmControl { get; set; }

        public LocationConfig()
        {
            MaxTxPower = true;
            TxPowerInDbm = DEFAULT_TX_POWER_IN_DBM;
            DisabledAntennaList = new List<ushort>();
            LocationAlgorithmControl = new List<uint>();
        }
    }
}
