namespace EA.Impinj
{
    /// <summary>Enum that specifies the direction report type.</summary>
    public enum DirectionReportType
    {
        /// <summary>
        /// This report type indicates that a tag has entered the tracked population of tags.
        /// </summary>
        Entry,
        /// <summary>
        /// This report type indicates that the tag has been seen within the amount of time specified by
        /// DirectionConfig.setTagAgeIntervalSeconds.
        /// </summary>
        Update,
        /// <summary>
        /// This report type indicates that the tag has not been read within the amount of time specified by
        /// setTagAgeIntervalSeconds and is no longer considered part of the tracked tag population.
        /// </summary>
        Exit,
    }
}
