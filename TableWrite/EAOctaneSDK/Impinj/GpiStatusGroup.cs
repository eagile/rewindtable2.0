using System.Collections.Generic;

namespace EA.Impinj
{
    /// <summary>
    /// Container class encapsulating all of the GPI status objects.
    /// This is for internal use only, and does not appear in the generated
    /// documentation.
    /// </summary>
    public class GpiStatusGroup : List<GpiStatus>
    {
        private readonly bool _createdBySerializer;

        public GpiStatusGroup()
        {
            _createdBySerializer = true;
        }

        /// <summary>Returns the status for the specified GPI port.</summary>
        /// <returns>The GPI status or throws an exception if the port does not exist.</returns>
        public GpiStatus GetGpi(ushort gpiNumber)
        {
            foreach (GpiStatus gpiStatuse in this)
            {
                if (gpiStatuse.PortNumber == gpiNumber)
                    return gpiStatuse;
            }
            throw new OctaneSdkException("Invalid GPI number specified : " + gpiNumber);
        }

        /// <summary>
        /// Adds the provided GPI status object to the collection.
        /// For internal library use only.
        /// </summary>
        /// <param name="status">The GPIStatus object to add.</param>
        public new void Add(GpiStatus status)
        {
            if (!_createdBySerializer)
                throw new OctaneSdkException("Illegal operation - cannot Add to GpiStatusGroup object!");
            base.Add(status);
        }
    }
}
