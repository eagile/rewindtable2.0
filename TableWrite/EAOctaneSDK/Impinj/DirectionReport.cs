using System;
using System.Collections.Generic;

namespace EA.Impinj
{
    /// <summary>xArray direction report</summary>
    public class DirectionReport : EventArgs
    {
        /// <summary>Timestamp of when the host received the event.</summary>
        public DateTime HostReceivedTime { get; set; }

        /// <summary>Contents of the tag EPC memory bank.</summary>
        public TagData Epc { get; }

        /// <summary>The time stamp of the transition event</summary>
        public DateTime LastReadTimestamp { get; set; }

        /// <summary>The time stamp of the transition event</summary>
        public short LastReadSector { get; set; }

        /// <summary>The direction report type.</summary>
        public DirectionReportType ReportType { get; set; }

        /// <summary>The UTC timestamp of when tag was first seen</summary>
        public DateTime FirstSeenTimestamp { get; set; }

        /// <summary>The ID of the sector in which this tag was first seen</summary>
        public byte FirstSeenSector { get; set; }

        /// <summary>The overflow status given the current tag population</summary>
        public DirectionTagPopulationStatus TagPopulationStatus { get; set; }

        /// <summary>Diagnostic data. Internal use only.</summary>
        public List<UInt32> DirectionDiagnosticDataMetric { get; set; }

        public DirectionReport(TagData epc)
        {
            Epc = epc ?? throw new ArgumentNullException(nameof(epc));
        }
    }
}
