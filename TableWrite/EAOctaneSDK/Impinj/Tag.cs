using System;
using System.Collections.Generic;

namespace EA.Impinj
{
    /// <summary>Class used to contain the details for a specific tag.</summary>
    public class Tag
    {
        /// <summary>Timestamp of when the host received the event.</summary>
        public DateTime HostReceivedTime { get; set; }

        /// <summary>Contents of the tag EPC memory bank.</summary>
        public TagData Epc { get; }

        /// <summary>
        /// The reader antenna port number for the antenna that last saw the tag;
        /// requires this option to be enabled in the reader settings report configuration.
        /// </summary>
        public ushort AntennaPortNumber { get; set; }

        /// <summary>
        /// The Reader channel, defined in Megahertz, that was being used when
        /// the tag was last seen; requires this option to be enabled in the
        /// reader settings report configuration.
        /// </summary>
        public double ChannelInMhz { get; set; }

        /// <summary>
        /// The time that the reader first saw the tag; requires this
        /// option to be enabled in the reader settings report configuration.
        /// </summary>
        public DateTime FirstSeenTime { get; set; }

        /// <summary>
        /// The time that the reader last saw the tag; requires this
        /// option to be enabled in the reader settings report configuration.
        /// </summary>
        public DateTime LastSeenTime { get; set; }

        /// <summary>
        /// The maximum RSSI, in dBm, that was seen for this tag; requires this
        /// option to be enabled in the reader settings report configuration.
        /// </summary>
        public double PeakRssiInDbm { get; set; }

        /// <summary>
        /// The number of times the reader has seen this tag; requires this
        /// option to be enabled in the reader settings report configuration.
        /// </summary>
        public ushort TagSeenCount { get; set; }

        /// <summary>The contents of the tag TID memory bank.</summary>
        public TagData Tid { get; set; }

        /// <summary>
        /// The value of the tag Doppler Frequency parameter; requires this
        /// option to be enabled in the reader settings report configuration.
        /// </summary>
        public double RfDopplerFrequency { get; set; }

        /// <summary>
        /// The value of the tag Phase Angle parameter; requires this
        /// option to be enabled in the reader settings report configuration.
        /// </summary>
        public double PhaseAngleInRadians { get; set; }

        /// <summary>
        /// Contents of the CRC 16-bit word (word 0) in the tag EPC memory bank; requires this
        /// option to be enabled in the reader settings report configuration.
        /// </summary>
        public ushort Crc { get; set; }

        /// <summary>
        /// Contents of the PC Bits 16-bit word (word 1) in the tag EPC memory
        /// bank; requires this option to be enabled in the reader settings
        /// report configuration.
        /// </summary>
        public ushort PcBits { get; set; }

        /// <summary>
        /// The value of the GPSCoordinates parameter, if this option
        /// is enabled in the reader settings report configuration.
        /// </summary>
        public GpsCoordinates GpsCoodinates { get; set; } = new GpsCoordinates();

        /// <summary>Results of an Optimized Read operation.</summary>
        public List<TagOpResult> TagOpResults { get; } = new List<TagOpResult>();

        /// <summary>Does the tag data include antenna port number data?</summary>
        public bool IsAntennaPortNumberPresent { get; set; }

        /// <summary>Does the tag data include channel frequency data?</summary>
        public bool IsChannelInMhzPresent { get; set; }

        /// <summary>
        /// Does the tag data include the first seen timestamp data?
        /// </summary>
        public bool IsFirstSeenTimePresent { get; set; }

        /// <summary>
        /// Does the tag data include the last seen timestamp data?
        /// </summary>
        public bool IsLastSeenTimePresent { get; set; }

        /// <summary>Does the tag data include the peak RSSI data?</summary>
        public bool IsPeakRssiInDbmPresent { get; set; }

        /// <summary>
        /// Does the tag data include the TID data from a FastId query?
        /// </summary>
        public bool IsFastIdPresent { get; set; }

        /// <summary>Does the tag data include Phase Angle data?</summary>
        public bool IsRfPhaseAnglePresent { get; set; }

        /// <summary>
        /// Does the tag data include data on the number of times the
        /// tag has been seen?
        /// </summary>
        public bool IsSeenCountPresent { get; set; }

        /// <summary>Does the tag data include the EPC CRC data?</summary>
        public bool IsCrcPresent { get; set; }

        /// <summary>Does the tag data include the EPC PC Bits data?</summary>
        public bool IsPcBitsPresent { get; set; }

        /// <summary>Does the tag data include Doppler Frequency data?</summary>
        public bool IsRfDopplerFrequencyPresent { get; set; }

        /// <summary>Does the tag data include GPS Coordinate data?</summary>
        public bool IsGpsCoordinatesPresent { get; set; }

        public Tag(TagData epc)
        {
            Epc = epc ?? throw new ArgumentNullException(nameof(epc));
        }
    }
}
