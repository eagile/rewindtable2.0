namespace EA.Impinj
{
    /// <summary>
    /// Enumeration used to indicate the results of a
    /// QtGetConfig operation.
    /// </summary>
    public enum QtGetConfigResultStatus
    {
        /// <summary>The tag QT operation was successful.</summary>
        Success,
        /// <summary>An unidentified tag error occurred.</summary>
        NonspecificTagError,
        /// <summary>
        /// No ACK response was received from the tag in response to the operation.
        /// </summary>
        NoResponseFromTag,
        /// <summary>An unidentified reader error occurred.</summary>
        NonspecificReaderError,
        /// <summary>An incorrect QT password was used.</summary>
        IncorrectPasswordError,
    }
}
