﻿using System;

namespace EA.Impinj
{
    public class ConnectionEvent : EventArgs
    {
        public DateTime Timestamp { get; }
        public bool IsConnected { get; }
        public Exception Error { get; }

        public ConnectionEvent(DateTime timestamp, bool isConnected, Exception error)
        {
            Timestamp = timestamp;
            IsConnected = isConnected;
            Error = error;
        }
    }
}
