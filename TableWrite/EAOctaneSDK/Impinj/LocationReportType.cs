using System;

namespace EA.Impinj
{
    /// <summary>Enum that specifies the location report type.</summary>
    public enum LocationReportType
    {
        /// <summary>An entry report</summary>
        Entry,
        /// <summary>An update report</summary>
        Update,
        /// <summary>An exit report</summary>
        Exit,
    }
}
