namespace EA.Impinj
{
    /// <summary>
    /// Enumeration used to determine which QT data profile to use.
    /// The Private/Public profile capability, available in Monza 4QT tag chips,
    /// provides two memory configurations (i.e. profiles) in a single chip;
    /// one Private and one Public. Only one profile is exposed at a time.
    /// </summary>
    public enum QtDataProfile
    {
        /// <summary>Leave the tag data profile as-is.</summary>
        Unknown,
        /// <summary>
        /// Select the Private, or confidential, Data Profile, where users have
        /// access to a 128-bit EPC, 512-bit User Memory, 16-bit TID header,
        /// and 48 bit serial number.
        /// </summary>
        Private,
        /// <summary>
        /// Select the Public Data Profile, where users have access to the 96-bit
        /// public EPC and the 32 bit base TID.
        /// </summary>
        Public,
    }
}
