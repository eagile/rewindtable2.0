namespace EA.Impinj
{
    /// <summary>
    /// Enum defining the possible Impinj reader autostart modes.
    /// </summary>
    public enum AutoStartMode
    {
        /// <summary>No autostart mode specified</summary>
        None,
        /// <summary>Start immediately after the reader is configured.</summary>
        Immediate,
        /// <summary>Periodic autostart mode specified.</summary>
        Periodic,
        /// <summary>GPI trigger autostart mode specified.</summary>
        GpiTrigger,
    }
}
