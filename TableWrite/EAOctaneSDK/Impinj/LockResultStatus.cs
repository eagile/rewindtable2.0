namespace EA.Impinj
{
    /// <summary>
    /// Enum defining the possible status outcomes for the tag lock operation.
    /// </summary>
    public enum LockResultStatus
    {
        /// <summary>The tag lock operation completed successfully.</summary>
        Success,
        /// <summary>The tag did not meet the required RSSI threshold.</summary>
        InsufficientPower,
        /// <summary>An unidentified tag error occurred.</summary>
        NonspecificTagError,
        /// <summary>
        /// No ACK response was received from the tag in response to the operation.
        /// </summary>
        NoResponseFromTag,
        /// <summary>An unidentified reader error occurred.</summary>
        NonspecificReaderError,
    }
}
