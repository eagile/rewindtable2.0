using System;
using System.Runtime.Serialization;

namespace EA.Impinj
{
    /// <summary>
    /// Exception class for uniquely identifying Impinj Octane SDK exceptions.
    /// </summary>
    [Serializable]
    public class OctaneSdkException : Exception
    {
        /// <summary>Creates an OctaneSdkException object with no message.</summary>
        public OctaneSdkException() : base() { }

        /// <summary>Creates an OctandSdkException object with a message.</summary>
        /// <param name="message">The message to include with the exception.</param>
        public OctaneSdkException(string message) : base(message) { }

        public OctaneSdkException(string message, Exception innerException) : base(message, innerException) { }

        protected OctaneSdkException(SerializationInfo serializationInfo, StreamingContext streamingContext) : base(serializationInfo, streamingContext) { }
    }
}
