﻿using System;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Collections.Generic;

namespace EAgile.SettingsXml
{
    [Serializable]
    public class ESettingsException : Exception
    {
        private const string DefaultMessage = "An error occurred with a setting.";
        public string SettingName { get; set; }
        public string SettingValue { get; set; }

        public ESettingsException()
            : base(DefaultMessage)
        {
        }

        public ESettingsException(string settingName)
            : base(DefaultMessage)
        {
            SettingName = settingName;
        }

        public ESettingsException(string settingName, string message)
            : base(message)
        {
            SettingName = settingName;
        }

        public ESettingsException(string settingName, string settingValue, string message)
            : base(message)
        {
            SettingName = settingName;
            SettingValue = settingValue;
        }

        public ESettingsException(string settingName, string message, Exception innerException)
            : base(message, innerException)
        {
            SettingName = settingName;
        }

        public ESettingsException(string settingName, string settingValue, string message, Exception innerException)
            : base(message, innerException)
        {
            SettingName = settingName;
            SettingValue = settingValue;
        }

        protected ESettingsException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            if (info == null)
            {
                throw new ArgumentNullException("info");
            }
            SettingName = info.GetString("SettingName");
            SettingValue = info.GetString("SettingValue");
        }

        [System.Security.SecurityCritical]  // auto-generated_required
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
            {
                throw new ArgumentNullException("info");
            }
            base.GetObjectData(info, context);
            info.AddValue("SettingName", SettingName, typeof(string));
            info.AddValue("SettingValue", SettingValue, typeof(string));
        }
    }

    public class ESettingsValidationException : ESettingsException
    {
        public ESettingsValidationException(string settingName, string message) : base(settingName, message) { }
        public ESettingsValidationException(string settingName, string settingValue, string message) : base(settingName, settingValue, message) { }
        public ESettingsValidationException(string settingName, string message, Exception innerException) : base(settingName, message, innerException) { }
        public ESettingsValidationException(string settingName, string settingValue, string message, Exception innerException) : base(settingName, settingValue, message, innerException) { }
        protected ESettingsValidationException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }

    public class ESettingsProtectionException : ESettingsException
    {
        public ESettingsProtectionException(string settingName, string message) : base(settingName, message) { }
        public ESettingsProtectionException(string settingName, string settingValue, string message) : base(settingName, settingValue, message) { }
        public ESettingsProtectionException(string settingName, string message, Exception innerException) : base(settingName, message, innerException) { }
        public ESettingsProtectionException(string settingName, string settingValue, string message, Exception innerException) : base(settingName, settingValue, message, innerException) { }
        protected ESettingsProtectionException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }

    public class ESettingsParseException : ESettingsException
    {
        public ESettingsParseException(string settingName, string message) : base(settingName, message) { }
        public ESettingsParseException(string settingName, string settingValue, string message) : base(settingName, settingValue, message) { }
        public ESettingsParseException(string settingName, string message, Exception innerException) : base(settingName, message, innerException) { }
        public ESettingsParseException(string settingName, string settingValue, string message, Exception innerException) : base(settingName, settingValue, message, innerException) { }
        protected ESettingsParseException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }


    [Serializable]
    public class ESettingsAggregateException : Exception
    {
        private const string DefaultMessage = "One or more errors occurred with settings.";
        private ReadOnlyCollection<ESettingsException> m_innerExceptions;

        public ESettingsAggregateException(string message, IEnumerable<ESettingsException> innerExceptions)
            : base(DefaultMessage)
        {
            if (innerExceptions == null)
            {
                m_innerExceptions = new ReadOnlyCollection<ESettingsException>(new ESettingsException[0]);
            }
            else
            {
                IList<ESettingsException> list = innerExceptions as IList<ESettingsException> ?? new List<ESettingsException>(innerExceptions);
                ESettingsException[] exceptionsCopy = new ESettingsException[list.Count];

                for (int i = 0; i < exceptionsCopy.Length; i++)
                {
                    if (list[i] != null)
                    {
                        exceptionsCopy[i] = list[i];
                    }
                }

                m_innerExceptions = new ReadOnlyCollection<ESettingsException>(exceptionsCopy);
            }
        }

        protected ESettingsAggregateException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            if (info == null)
            {
                throw new ArgumentNullException("info");
            }
            ESettingsException[] innerExceptions = info.GetValue("InnerExceptions", typeof(ESettingsException[])) as ESettingsException[];
            if (innerExceptions == null)
            {
                throw new SerializationException("ESettingsAggregateException Deserialization Failure");
            }
            m_innerExceptions = new ReadOnlyCollection<ESettingsException>(innerExceptions);
        }

        public ReadOnlyCollection<ESettingsException> InnerExceptions
        {
            get { return m_innerExceptions; }
        }

        [System.Security.SecurityCritical]  // auto-generated_required
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
            {
                throw new ArgumentNullException("info");
            }
            base.GetObjectData(info, context);
            ESettingsException[] innerExceptions = new ESettingsException[m_innerExceptions.Count];
            m_innerExceptions.CopyTo(innerExceptions, 0);
            info.AddValue("InnerExceptions", innerExceptions, typeof(ESettingsException[]));
        }
    }
}
