﻿using EAgile.SettingsXml;

using EStreamWrapper.Com;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DFD
{
    [XmlRoot(ElementName = "Settings")]
    public class DFDSettings : ESettings
    {
        [XmlElement(ElementName = "BarcodePort")]
        [XmlComment("Serial port parameters as PortName,BaudRate,Parity,DataBits,StopBits eg: COM1,9600,None,8,One")]
        [DefaultValue("COM5,115200,None,8,One")]
        public string BarcodePort { get; set; }
        public EComPort BarcodeEComPort { get { return string.IsNullOrEmpty(BarcodePort) ? new EComPort("COM5") : new EComPort(BarcodePort); } }

        [XmlElement(ElementName = "BarcodePrefixHex")]
        [XmlComment("Hex representation of characters that are sent by the barcode scanner before barcodes")]
        [DefaultValue("")]
        public string BarcodePrefixHex { get; set; }

        [XmlElement(ElementName = "BarcodeSuffixHex")]
        [XmlComment("Hex representation of characters that are sent by the barcode scanner after barcodes eg 0D for Carriage Return")]
        [DefaultValue("0D")]
        public string BarcodeSuffixHex { get; set; }

        [XmlElement(ElementName = "RfidPort")]
        [XmlComment("COM port name of the RFID reader", Inline = true)]
        [DefaultValue("AUTO")]
        public string RfidPort { get; set; }
        public string AutoRfidPort { get; set; }

        [XmlElement(ElementName = "Antenna")]
        [XmlComment("Antenna port number to use", Inline = true)]
        [DefaultValue(2)]
        public int Antenna { get; set; }

        [XmlElement(ElementName = "HFDemo")]
        [XmlComment("HF Demo Settings")]
        public DFDModeSettings HFDemo { get; set; }

        [XmlElement(ElementName = "UHFDemo")]
        [XmlComment("UHF Demo Settings")]
        public DFDInventoryModeSettings UHFDemo { get; set; }

        [XmlElement(ElementName = "EPCPrefixFilter")]
        [XmlComment("Cross reference or products to EPCs")]
        public string EPCPrefixFilter { get; set; }


    }

    public class DFDModeSettings : ESettings
    {

        [XmlElement(ElementName = "ReadPower")]
        [XmlComment("Read power in dBm", Inline = true)]
        [DefaultValue(24)]
        public int Power { get; set; }

        [XmlElement(ElementName = "ReadInterval")]
        [XmlComment("Milliseconds to pause between reads", Inline = true)]
        [DefaultValue(100)]
        public int Interval { get; set; }

        [XmlElement(ElementName = "ReadDuration")]
        [XmlComment("Milliseconds to read", Inline = true)]
        [DefaultValue(100)]
        public int Duration { get; set; }
    }

    public class DFDInventoryModeSettings : DFDModeSettings
    {
        [XmlElement(ElementName = "ReportPercent")]
        [XmlComment("The percent of reads required per sample to include to inventory", Inline = true)]
        [DefaultValue(20)]
        public int ReportPercent { get; set; }

        [XmlElement(ElementName = "ReportSample")]
        [XmlComment("How many samples to used when applying the ReportPercent", Inline = true)]
        [DefaultValue(0)]
        public int ReportSample { get; set; }

        [XmlElement(ElementName = "MinReadCount")]
        [XmlComment("Number of sequential reads required to add to inventory", Inline = true)]
        [DefaultValue(1)]
        public int MinReadCount { get; set; }

        [XmlElement(ElementName = "MinNonReadCount")]
        [XmlComment("Number of sequential non-reads required to remove from inventory", Inline = true)]
        [DefaultValue(5)]
        public int MinNonReadCount { get; set; }

        [XmlElement(ElementName = "DecodeSGTIN")]
        [XmlComment("If the EPC is an SGTIN then decode it and display the values", Inline = true)]
        [DefaultValue(false)]
        public bool DecodeSGTIN { get; set; }

        [XmlElement(ElementName = "DisplayLotExpiration")]
        [XmlComment("If the EPC has a lot or expiration in the products file they will be displayed", Inline = true)]
        [DefaultValue(true)]
        public bool DisplayLotExpiration { get; set; }

        [XmlElement(ElementName = "ProductsOnly")]
        [XmlComment("Only display EPCs that are in the products file", Inline = true)]
        [DefaultValue(false)]
        public bool ProductsOnly { get; set; }

    }
}
