﻿using EStreamWrapper;
using System;
using System.IO.Ports;

namespace EStreamWrapper.Com
{
    public class EComStream : EStream
    {
        public override bool IsConnected { get { try { return base.IsConnected && _serialPort != null && _serialPort.IsOpen; } catch { return false; } } }

        protected SerialPort _serialPort;

        private volatile bool _comOpened = false;

        public EComStream(string portName, int baudRate, Parity parity, int dataBits, StopBits stopBits,
            EStreamMessageFramingSettings framingSettings, EStreamHeartbeatSettings heartbeatSettings, int bufferSize = DEFAULT_BUFFER_SIZE)
            : base(framingSettings, heartbeatSettings, bufferSize)
        {
            _serialPort = new SerialPort(portName, baudRate, parity, dataBits, stopBits);
            _serialPort.ReadTimeout = _serialPort.WriteTimeout = 1000;
        }

        public override void Open()
        {
            lock (_streamLock)
            {
                if (_closed) throw new InvalidOperationException("Stream closed");
                if (_opened || _comOpened) throw new InvalidOperationException("Stream already open");
                _comOpened = true;
                _serialPort.Open();
                _stream = _serialPort.BaseStream;
                base.Open();
            }
        }

        public override void Close()
        {
            base.Close();
            ClosePort(_serialPort);
        }

        protected void ClosePort(SerialPort serialPort)
        {
            if (serialPort != null)
            {
                try { serialPort.BaseStream.Close(); } catch { }
                try { serialPort.Close(); } catch { }
            }
        }

        #region Dispose

        private bool disposed = false;

        protected override void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                // Free any managed objects here.
                try { Close(); } catch { }
                ClosePort(_serialPort);
                _serialPort = null;
            }

            // Free any unmanaged objects here
            disposed = true;

            base.Dispose(disposing);
        }
        ~EComStream()
        {
            Dispose(false);
        }

        #endregion Dispose
    }
}
