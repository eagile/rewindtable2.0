﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace EStreamWrapper
{
    public abstract class EStreamClient : IDisposable
    {
        public virtual bool IsConnected { get { try { return _eStream != null && _eStream.IsConnected; } catch { return false; } } }
        public virtual bool IsRunning { get { return !_clientCt.IsCancellationRequested; } }

        public int BufferSize { get; set; }
        public bool AutoReconnect { get; set; }
        public TimeSpan AutoReconnectInterval { get; set; }
        public TimeSpan ConnectionTimeout { get; set; }

        public EStreamHeartbeatSettings HeartbeatSettings { get; set; }
        public EStreamMessageFramingSettings MessageFramingSettings { get; set; }

        private EStream _eStream;

        private System.Timers.Timer _connectTimer;

        private volatile bool _connecting = false;
        private bool _firstPoll = true;

        private readonly object _clientLock = new object();

        private CancellationTokenSource _clientCts = new CancellationTokenSource();
        protected CancellationToken _clientCt { get { return _clientCts.Token; } }

        public EStreamClient()
        {
            HeartbeatSettings = new EStreamHeartbeatSettings();
            MessageFramingSettings = new EStreamMessageFramingSettings();

            BufferSize = EStream.DEFAULT_BUFFER_SIZE;
            AutoReconnect = true;
            AutoReconnectInterval = new TimeSpan(0, 0, 30);
            ConnectionTimeout = new TimeSpan(0, 0, 30);

            _connectTimer = new System.Timers.Timer();
            _connectTimer.AutoReset = false;
            _connectTimer.Elapsed += ConnectTimer_Elapsed;

            _clientCts.Cancel();
        }

        #region StartStop

        public virtual void Start()
        {
            lock (_clientLock)
            {
                if (_clientCts.IsCancellationRequested)
                {
                    _clientCts = new CancellationTokenSource();
                    BeginConnectClient(TimeSpan.FromMilliseconds(1));
                }
            }
        }

        public virtual void Stop()
        {
            lock (_clientLock)
            {
                _connectTimer.Stop();
                _clientCts.Cancel();
                _eStream.Close();
            }
        }

        #endregion StartStop

        #region Timers

        private void ConnectTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            _connectTimer.Stop();
            BeginConnectClient(_clientCt);
        }

        protected void BeginConnectClient(TimeSpan delay)
        {
            _connectTimer.Interval = delay.TotalMilliseconds;
            _connectTimer.Start();
        }

        #endregion Timers

        #region Connect

        protected virtual void BeginConnectClient(CancellationToken ct)
        {
            bool firstPoll = false;
            EStream eStream = null;
            try
            {
                lock (_clientLock)
                {
                    if (_firstPoll)
                    {
                        firstPoll = true;
                        _firstPoll = false;
                    }

                    eStream = _eStream;

                    if (ct.IsCancellationRequested)
                        return;

                    if (_connecting || IsConnected)
                        return;

                    _connecting = true;
                    DisposeStream(_eStream);
                    eStream = _eStream = GetNewClientStream();
                }

                eStream.Open();
                lock (_clientLock)
                {
                    _connecting = false;
                }
            }
            catch (Exception ex)
            {
                lock (_clientLock)
                {
                    _connecting = false;
                }
                if (eStream != null)
                {
                    try { eStream.Close(); ; } catch { }
                }
                if (!ct.IsCancellationRequested)
                {
                    if (firstPoll)
                    {
                        OnClientDisconnected(new EStreamEventArgs(eStream));
                    }
                    OnClientError(new EStreamExceptionEventArgs(eStream, ex));
                    ReconnectIfNeeded();
                }
            }
        }

        protected virtual void ReconnectIfNeeded()
        {
            if (!IsConnected && !_connectTimer.Enabled)
            {
                if (AutoReconnect && !_clientCt.IsCancellationRequested)
                {
                    BeginConnectClient(AutoReconnectInterval);
                }
                else
                {
                    Stop();
                }
            }
        }

        #endregion Connect

        #region PushBytes

        public Task SendBytes(byte[] bytes, CancellationToken ct)
        {
            return _eStream.SendBytes(bytes, ct);
        }

        #endregion PushBytes

        #region Client

        public virtual void Close()
        {
            EStream client = null;
            lock (_clientLock)
            {
                client = _eStream;
            }
            if (client != null)
            {
                try { client.Close(); ; } catch { }
            }
            ReconnectIfNeeded();
        }

        public EStream GetClientConnection()
        {
            return _eStream;
        }
        
        protected EStream GetNewClientStream()
        {
            EStream client = CreateStream();
            AttatchListeners(client);
            return client;
        }

        protected abstract EStream CreateStream();

        protected void DisposeStream(EStream eStream)
        {
            if (eStream != null)
            {
                DetachListeners(eStream);
                try { eStream.Close(); } catch { }
                try { eStream.Dispose(); } catch { }
            }
        }

        protected virtual void AttatchListeners(EStream eStream)
        {
            eStream.Connected += Client_Connected;
            eStream.Disconnected += Client_Disconnected;
            eStream.HeartbeatSent += Client_HeartbeatSent;
            eStream.HeartbeatReceived += Client_HeartbeatReceived;
            eStream.BytesReceived += Client_BytesReceived;
            eStream.Error += Client_Error;
        }

        protected virtual void DetachListeners(EStream eStream)
        {
            eStream.Connected -= Client_Connected;
            eStream.Disconnected -= Client_Disconnected;
            eStream.HeartbeatSent -= Client_HeartbeatSent;
            eStream.HeartbeatReceived -= Client_HeartbeatReceived;
            eStream.BytesReceived -= Client_BytesReceived;
            eStream.Error -= Client_Error;
        }

        protected virtual void Client_Connected(object sender, EStreamEventArgs e)
        {
            OnClientConnected(e);
        }

        protected virtual void Client_Disconnected(object sender, EStreamEventArgs e)
        {
            OnClientDisconnected(e);
            ReconnectIfNeeded();
        }

        protected virtual void Client_HeartbeatSent(object sender, EStreamEventArgs e)
        {
            OnClientHeartbeatSent(e);
        }

        protected virtual void Client_HeartbeatReceived(object sender, EStreamEventArgs e)
        {
            OnClientHeartbeatReceived(e);
        }

        protected virtual void Client_BytesReceived(object sender, EStreamBytesEventArgs e)
        {
            OnClientBytesReceived(e);
        }

        protected virtual void Client_Error(object sender, EStreamExceptionEventArgs e)
        {
            OnClientError(e);
            ReconnectIfNeeded();
        }

        #endregion Client

        #region Events

        public event EventHandler<EStreamEventArgs> ClientConnected;
        protected virtual void OnClientConnected(EStreamEventArgs e)
        {
            EventHandler<EStreamEventArgs> handler = ClientConnected;
            if (handler != null) handler(this, e);
        }

        public event EventHandler<EStreamEventArgs> ClientDisconnected;
        protected virtual void OnClientDisconnected(EStreamEventArgs e)
        {
            EventHandler<EStreamEventArgs> handler = ClientDisconnected;
            if (handler != null) handler(this, e);
        }

        public event EventHandler<EStreamEventArgs> ClientHeartbeatSent;
        protected virtual void OnClientHeartbeatSent(EStreamEventArgs e)
        {
            EventHandler<EStreamEventArgs> handler = ClientHeartbeatSent;
            if (handler != null) handler(this, e);
        }

        public event EventHandler<EStreamEventArgs> ClientHeartbeatReceived;
        protected virtual void OnClientHeartbeatReceived(EStreamEventArgs e)
        {
            EventHandler<EStreamEventArgs> handler = ClientHeartbeatReceived;
            if (handler != null) handler(this, e);
        }

        public event EventHandler<EStreamBytesEventArgs> ClientBytesReceived;
        protected virtual void OnClientBytesReceived(EStreamBytesEventArgs e)
        {
            EventHandler<EStreamBytesEventArgs> handler = ClientBytesReceived;
            if (handler != null) handler(this, e);
        }

        public event EventHandler<EStreamExceptionEventArgs> ClientError;
        protected virtual void OnClientError(EStreamExceptionEventArgs e)
        {
            EventHandler<EStreamExceptionEventArgs> handler = ClientError;
            if (handler != null) handler(this, e);
        }

        #endregion Events
        
        #region Dispose

        private bool disposed = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                // Free any managed objects here.
                try { Stop(); } catch { }
                try { Close(); } catch { }
                try { DisposeStream(_eStream); } catch { }
                if (_connectTimer != null)
                {
                    try { _connectTimer.Stop(); } catch { }
                    try { _connectTimer.Elapsed -= ConnectTimer_Elapsed; } catch { }
                    try { _connectTimer.Dispose(); } catch { }
                }
            }

            // Free any unmanaged objects here
            disposed = true;
        }
        ~EStreamClient()
        {
            Dispose(false);
        }

        #endregion Dispose
    }
}
