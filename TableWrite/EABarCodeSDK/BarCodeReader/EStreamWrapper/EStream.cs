﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace EStreamWrapper
{
    public abstract class EStream : IDisposable
    {
        public const int DEFAULT_BUFFER_SIZE = 1024;

        public virtual bool IsConnected { get { try { return _opened && !_closed; } catch { return false; } } }

        public int ID { get; protected set; }

        protected Stream _stream;

        private int _bufferSize;
        private EStreamHeartbeatSettings _heartbeatSettings;
        private EStreamMessageFramingSettings _sendFramingSettings;
        private EStreamMessageFramingSettings _receiveFramingSettings;
        
        private DateTime _nextSendHeartbeat;
        private DateTime _lastSendHeartbeat;

        private System.Timers.Timer _heartbeatIntervalTimer;
        private System.Timers.Timer _heartbeatTimeoutTimer;

        protected readonly object _streamLock = new object();

        private readonly Queue<QueuedMessage> _writeQueue = new Queue<QueuedMessage>();
        private volatile bool _writing = false;

        protected volatile bool _opened = false;
        protected volatile bool _connected = false;
        protected volatile bool _closed = false;

        public EStream(EStreamMessageFramingSettings framingSettings, int bufferSize = DEFAULT_BUFFER_SIZE) : this(framingSettings, null, bufferSize) { }
        public EStream(EStreamMessageFramingSettings framingSettings, EStreamHeartbeatSettings heartbeatSettings, int bufferSize = DEFAULT_BUFFER_SIZE) : this(framingSettings, framingSettings, heartbeatSettings, bufferSize) { }
        public EStream(EStreamMessageFramingSettings sendFramingSettings, EStreamMessageFramingSettings receiveFramingSettings, EStreamHeartbeatSettings heartbeatSettings, int bufferSize = DEFAULT_BUFFER_SIZE)
        {
            if (bufferSize <= 0)
                throw new ArgumentOutOfRangeException("bufferSize");
            
            _bufferSize = bufferSize;
            _sendFramingSettings = sendFramingSettings != null ? sendFramingSettings.GetDeepCopy() : new EStreamMessageFramingSettings();
            _receiveFramingSettings = receiveFramingSettings != null ? receiveFramingSettings.GetDeepCopy() : new EStreamMessageFramingSettings();
            _heartbeatSettings = heartbeatSettings != null ? heartbeatSettings.GetDeepCopy() : new EStreamHeartbeatSettings();
            _heartbeatIntervalTimer = new System.Timers.Timer();
            _heartbeatTimeoutTimer = new System.Timers.Timer();
            _nextSendHeartbeat = DateTime.MaxValue.ToUniversalTime();
            _lastSendHeartbeat = DateTime.MinValue.ToUniversalTime();
        }

        public virtual void Open()
        {
            if (_stream == null)
                throw new NullReferenceException("Stream is null");

            lock (_streamLock)
            {
                if (_closed) throw new InvalidOperationException("Stream closed");
                if (_opened) throw new InvalidOperationException("Stream already open");
                _opened = true;

                if (_heartbeatSettings != null && _heartbeatSettings.SendInterval != TimeSpan.Zero
                    && _heartbeatSettings.SendBytes != null && _heartbeatSettings.SendBytes.Length > 0)
                {
                    _heartbeatIntervalTimer.Interval = _heartbeatSettings.SendInterval.TotalMilliseconds;
                    _heartbeatIntervalTimer.AutoReset = true;
                    _heartbeatIntervalTimer.Elapsed += _heartbeatIntervalTimer_Elapsed;
                    _heartbeatIntervalTimer.Start();
                }
                if (_heartbeatSettings != null && _heartbeatSettings.ReceiveTimeout != TimeSpan.Zero
                    && _heartbeatSettings.ReceiveBytes != null && _heartbeatSettings.ReceiveBytes.Length > 0)
                {
                    _heartbeatTimeoutTimer.Interval = _heartbeatSettings.ReceiveTimeout.TotalMilliseconds;
                    _heartbeatTimeoutTimer.AutoReset = false;
                    _heartbeatTimeoutTimer.Elapsed += _heartbeatTimeoutTimer_Elapsed;
                    _heartbeatTimeoutTimer.Start();
                }
            }
            BeginReadingMessage(null);
            _connected = true;
            OnConnected();
        }

        public virtual void Close()
        {
            lock (_streamLock)
            {
                if (_closed) return;
                _closed = true;

                if (_stream != null)
                {
                    try { _stream.Close(); } catch { }
                }
                if (_heartbeatIntervalTimer != null)
                {
                    _heartbeatIntervalTimer.Stop();
                    _heartbeatIntervalTimer.Elapsed -= _heartbeatIntervalTimer_Elapsed;
                }
                if (_heartbeatTimeoutTimer != null)
                {
                    _heartbeatTimeoutTimer.Stop();
                    _heartbeatTimeoutTimer.Elapsed -= _heartbeatTimeoutTimer_Elapsed;
                }

                while (_writeQueue.Count > 0)
                {
                    QueuedMessage qm = _writeQueue.Dequeue();
                    if (qm.MessageCT.IsCancellationRequested)
                        qm.TCS.TrySetCanceled();
                    else
                        qm.TCS.TrySetException(new InvalidOperationException("Stream closed"));
                }
            }
            if (_connected)
            {
                OnDisconnected();
            }
            else if (_opened)
            {
                OnError(new InvalidOperationException("Stream aborted"));
            }
        }

        public virtual void Close(Exception ex)
        {
            if (!_closed)
            {
                OnError(ex);
                Close();
            }
        }

        public Task SendBytes(byte[] data, CancellationToken ct)
        {
            TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool>();
            if (ct.IsCancellationRequested)
            {
                tcs.TrySetCanceled();
            }
            else if (data == null)
            {
                tcs.TrySetException(new ArgumentNullException("data"));
            }
            else if (data.Length > _sendFramingSettings.MaxMessageSize)
            {
                tcs.TrySetException(new ArgumentOutOfRangeException("data", "Message size exceeds maximum message size"));
            }
            else if (_sendFramingSettings.FramingType == EStreamMessageFraming.FixedSize
                && data.Length != _sendFramingSettings.FixedSize)
            {
                tcs.TrySetException(new ArgumentOutOfRangeException("data", "Message size does not match fixed message size"));
            }
            else
            {
                lock (_streamLock)
                {
                    if (!IsConnected)
                        tcs.TrySetException(new InvalidOperationException("Stream closed"));
                    else
                        _writeQueue.Enqueue(new QueuedMessage(data, tcs, ct, false));
                }
                BeginWritingQueuedMessage();
            }
            return tcs.Task;
        }

        private void _heartbeatIntervalTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            lock (_streamLock)
            {
                _nextSendHeartbeat = DateTime.UtcNow;
            }
            BeginWritingQueuedMessage();
        }

        private void _heartbeatTimeoutTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Close(new TimeoutException("Stream timed out"));
        }

        private void BeginWritingQueuedMessage()
        {
            QueuedMessage qm = null;
            try
            {
                while (qm == null)
                {
                    lock (_streamLock)
                    {
                        if (_writing)
                            return;
                        _writing = true;

                        DateTime utcNow = DateTime.UtcNow;
                        if (utcNow >= _nextSendHeartbeat && _lastSendHeartbeat < _nextSendHeartbeat)
                        {
                            _lastSendHeartbeat = utcNow;
                            qm = new QueuedMessage(_heartbeatSettings.SendBytes, new TaskCompletionSource<bool>(), CancellationToken.None, true);
                        }
                        else if (_writeQueue.Count > 0)
                        {
                            qm = _writeQueue.Dequeue();
                        }
                        else
                        {
                            _writing = false;
                            return;
                        }
                    }

                    if (qm.MessageCT.IsCancellationRequested)
                    {
                        qm.TCS.TrySetCanceled();
                    }
                    else if (!IsConnected)
                    {
                        qm.TCS.TrySetException(new InvalidOperationException("Stream closed"));
                        qm = null;
                    }
                }

                FrameMessage(ref qm);
                _stream.BeginWrite(qm.MessageBytes, 0, qm.MessageBytes.Length, EndWritingQueuedMessage, new { QM = qm });
            }
            catch (Exception ex)
            {
                if (qm != null && qm.TCS != null)
                {
                    qm.TCS.TrySetException(ex);
                }
                lock (_streamLock)
                {
                    _writing = false;
                }
                Close(ex);
            }
        }

        private void EndWritingQueuedMessage(IAsyncResult asyncResult)
        {
            QueuedMessage qm = null;
            try
            {
                dynamic state = asyncResult.AsyncState;
                qm = state.QM;

                _stream.EndWrite(asyncResult);

                qm.TCS.TrySetResult(true);

                if (qm.IsHeartbeat)
                    OnHeartbeatSent();
            }
            catch (Exception ex)
            {
                if (qm != null && qm.TCS != null)
                {
                    qm.TCS.TrySetException(ex);
                }
                Close(ex);
            }
            finally
            {
                lock (_streamLock)
                {
                    _writing = false;
                }
                BeginWritingQueuedMessage();
            }
        }

        private void BeginReadingMessage(ReadingMessage rm)
        {
            try
            {
                if (rm == null)
                    rm = new ReadingMessage(_bufferSize);

                _stream.BeginRead(rm.buffer, 0, rm.buffer.Length, EndReadingMessage, new { RM = rm });
            }
            catch (Exception ex)
            {
                if (rm != null && rm.dataBuilder != null)
                {
                    try { rm.dataBuilder.Dispose(); } catch { }
                    rm.dataBuilder = null;
                }
                Close(ex);
            }
        }

        private void EndReadingMessage(IAsyncResult asyncResult)
        {
            ReadingMessage rm = null;
            try
            {
                dynamic state = asyncResult.AsyncState;
                rm = state.RM;

                rm.bufferStart = 0;
                rm.bufferCount = _stream.EndRead(asyncResult);

                if (_heartbeatTimeoutTimer != null && _heartbeatTimeoutTimer.Enabled)
                {
                    _heartbeatTimeoutTimer.Stop();
                    _heartbeatTimeoutTimer.Start();
                }

                if (rm.bufferCount == 0)
                {
                    Close();
                    return;
                }

                ProcessFramedMessage(ref rm);

                BeginReadingMessage(rm);
            }
            catch (Exception ex)
            {
                if (rm != null && rm.dataBuilder != null)
                {
                    try { rm.dataBuilder.Dispose(); } catch { }
                    rm.dataBuilder = null;
                }
                Close(ex);
            }
        }

        private void FrameMessage(ref QueuedMessage qm)
        {
            byte[] message = qm.MessageBytes;
            byte[] framedMessage;
            switch (_sendFramingSettings.FramingType)
            {
                case EStreamMessageFraming.SizePrefixed:
                    byte[] size;
                    switch (_sendFramingSettings.SizePrefixedType)
                    {
                        case EStreamPrefixTypeSize.Int16:
                            size = BitConverter.GetBytes(IPAddress.HostToNetworkOrder(Convert.ToInt16(message.Length)));
                            break;
                        case EStreamPrefixTypeSize.Int32:
                            size = BitConverter.GetBytes(IPAddress.HostToNetworkOrder(Convert.ToInt32(message.Length)));
                            break;
                        case EStreamPrefixTypeSize.Int64:
                            size = BitConverter.GetBytes(IPAddress.HostToNetworkOrder(Convert.ToInt64(message.Length)));
                            break;
                        default:
                            throw new InvalidOperationException("Invalid size prefix type");
                    }
                    framedMessage = new byte[size.Length + message.Length];
                    Array.Copy(size, 0, framedMessage, 0, size.Length);
                    Array.Copy(message, 0, framedMessage, size.Length, message.Length);
                    break;
                case EStreamMessageFraming.Delimited:
                    byte[] prefix = _sendFramingSettings.DelimitedPrefix ?? new byte[0];
                    byte[] suffix = _sendFramingSettings.DelimitedSuffix ?? new byte[0];
                    if (suffix.Length == 0)
                        throw new InvalidOperationException("Invalid delimeter suffix");
                    framedMessage = new byte[prefix.Length + message.Length + suffix.Length];
                    Array.Copy(prefix, 0, framedMessage, 0, prefix.Length);
                    Array.Copy(message, 0, framedMessage, prefix.Length, message.Length);
                    Array.Copy(suffix, 0, framedMessage, prefix.Length + message.Length, suffix.Length);
                    break;
                case EStreamMessageFraming.FixedSize:
                    if (message.Length != _sendFramingSettings.FixedSize)
                        throw new InvalidOperationException("Invalid message length");
                    framedMessage = new byte[message.Length];
                    Array.Copy(message, 0, framedMessage, 0, message.Length);
                    break;
                case EStreamMessageFraming.Raw:
                    framedMessage = new byte[message.Length];
                    Array.Copy(message, 0, framedMessage, 0, message.Length);
                    break;
                default:
                    throw new InvalidOperationException("Invalid message framing type");
            }
            qm.MessageBytes = framedMessage;
        }

        private void ProcessFramedMessage(ref ReadingMessage rm)
        {
            switch (_receiveFramingSettings.FramingType)
            {
                case EStreamMessageFraming.SizePrefixed:
                    while (rm.bufferCount > 0)
                    {
                        if (rm.size == null)
                        {
                            switch (_receiveFramingSettings.SizePrefixedType)
                            {
                                case EStreamPrefixTypeSize.Int16:
                                    rm.size = new byte[sizeof(Int16)];
                                    break;
                                case EStreamPrefixTypeSize.Int32:
                                    rm.size = new byte[sizeof(Int32)];
                                    break;
                                case EStreamPrefixTypeSize.Int64:
                                    rm.size = new byte[sizeof(Int64)];
                                    break;
                                default:
                                    throw new InvalidOperationException("Invalid size prefix type");
                            }
                        }
                        if (rm.sizeRead < rm.size.Length)
                        {
                            UseBuffer(ref rm.buffer, ref rm.bufferStart, ref rm.size, ref rm.sizeRead, ref rm.bufferCount);
                            if (rm.sizeRead == rm.size.Length)
                            {
                                int size = IPAddress.NetworkToHostOrder(BitConverter.ToInt32(rm.size, 0));
                                if (size > _receiveFramingSettings.MaxMessageSize)
                                    throw new InvalidOperationException("Received message size exceeds maximum message size");
                                rm.data = new byte[size];
                                rm.dataRead = 0;
                            }
                        }
                        if (rm.bufferCount > 0)
                        {
                            UseBuffer(ref rm.buffer, ref rm.bufferStart, ref rm.data, ref rm.dataRead, ref rm.bufferCount);
                            if (rm.dataRead == rm.data.Length)
                            {
                                MessageComplete(rm.data);
                                rm.sizeRead = 0;
                                rm.size = null;
                                rm.dataRead = 0;
                                rm.data = null;
                            }
                        }
                    }
                    break;
                case EStreamMessageFraming.Delimited:
                    byte[] prefix = _receiveFramingSettings.DelimitedPrefix ?? new byte[0];
                    byte[] suffix = _receiveFramingSettings.DelimitedSuffix ?? new byte[0];
                    if (suffix.Length == 0)
                        throw new InvalidOperationException("Invalid delimeter suffix");
                    if (rm.dataBuilder == null)
                        rm.dataBuilder = new MemoryStream();
                    while (rm.bufferCount > 0)
                    {
                        if (rm.prefixRead < prefix.Length)
                        {
                            if (rm.buffer[rm.bufferStart] == prefix[rm.prefixRead])
                                rm.prefixRead++;
                            else
                                rm.prefixRead = 0;
                            rm.bufferStart++;
                            rm.bufferCount--;
                        }
                        else if (rm.suffixRead < suffix.Length)
                        {
                            if (rm.buffer[rm.bufferStart] == suffix[rm.suffixRead])
                                rm.suffixRead++;
                            else
                                rm.suffixRead = 0;
                            rm.dataBuilder.WriteByte(rm.buffer[rm.bufferStart]);
                            rm.dataRead++;
                            rm.bufferStart++;
                            rm.bufferCount--;
                        }
                        if (rm.suffixRead == suffix.Length)
                        {
                            rm.dataRead -= suffix.Length;
                            rm.dataBuilder.Position = 0;
                            rm.dataBuilder.SetLength(rm.dataRead);
                            MessageComplete(rm.dataBuilder.ToArray());
                            rm.dataBuilder.SetLength(0);
                            rm.dataRead = 0;
                            rm.prefixRead = 0;
                            rm.suffixRead = 0;
                        }
                        if (rm.dataBuilder.Length > _receiveFramingSettings.MaxMessageSize)
                            throw new InvalidOperationException("Received message size exceeds maximum message size");
                    }
                    break;
                case EStreamMessageFraming.FixedSize:
                    int fixedSize = _receiveFramingSettings.FixedSize;
                    if (fixedSize <= 0)
                        throw new InvalidOperationException("Invalid fixed message size");
                    if (fixedSize > _receiveFramingSettings.MaxMessageSize)
                        throw new InvalidOperationException("Fixed message size exceeds maximum message size");
                    while (rm.bufferCount > 0)
                    {
                        if (rm.data == null)
                        {
                            rm.data = new byte[fixedSize];
                        }
                        UseBuffer(ref rm.buffer, ref rm.bufferStart, ref rm.data, ref rm.dataRead, ref rm.bufferCount);
                        if (rm.dataRead == rm.data.Length)
                        {
                            MessageComplete(rm.data);
                            rm.dataRead = 0;
                            rm.data = null;
                        }
                    }
                    break;
                case EStreamMessageFraming.Raw:
                    rm.data = new byte[rm.bufferCount];
                    Array.Copy(rm.buffer, rm.bufferStart, rm.data, 0, rm.bufferCount);
                    MessageComplete(rm.data);
                    rm.data = null;
                    break;
                default:
                    throw new InvalidOperationException("Invalid message framing type");
            }
        }

        private void MessageComplete(byte[] message)
        {
            if (message != null)
            {
                if (ByteSequenceEqual(message, _heartbeatSettings.ReceiveBytes)
                    && _heartbeatSettings.ReceiveBytes.Length > 0)
                {
                    OnHeartbeatReceived();
                }
                else
                {
                    OnBytesReceived(message);
                }
            }
        }

        private static bool ByteSequenceEqual(byte[] bytes1, byte[] bytes2)
        {
            if (bytes1 == null || bytes2 == null || bytes1.Length != bytes2.Length)
                return false;
            for (int i = 0; i < bytes1.Length; i++)
            {
                if (bytes1[i] != bytes2[i])
                    return false;
            }
            return true;
        }

        private static void UseBuffer(ref byte[] buffer, ref int bufferStart, ref byte[] data, ref int dataStart, ref int count)
        {
            int countToUse = Math.Min(count, data.Length - dataStart);
            Array.Copy(buffer, bufferStart, data, dataStart, countToUse);
            bufferStart += countToUse;
            dataStart += countToUse;
            count -= countToUse;
        }

        #region Events

        public event EventHandler<EStreamEventArgs> Connected;
        protected virtual void OnConnected()
        {
            EventHandler<EStreamEventArgs> handler = Connected;
            if (handler != null) handler(this, new EStreamEventArgs(this));
        }

        public event EventHandler<EStreamEventArgs> Disconnected;
        protected virtual void OnDisconnected()
        {
            EventHandler<EStreamEventArgs> handler = Disconnected;
            if (handler != null) handler(this, new EStreamEventArgs(this));
        }

        public event EventHandler<EStreamEventArgs> HeartbeatSent;
        protected virtual void OnHeartbeatSent()
        {
            EventHandler<EStreamEventArgs> handler = HeartbeatSent;
            if (handler != null) handler(this, new EStreamEventArgs(this));
        }

        public event EventHandler<EStreamEventArgs> HeartbeatReceived;
        protected virtual void OnHeartbeatReceived()
        {
            EventHandler<EStreamEventArgs> handler = HeartbeatReceived;
            if (handler != null) handler(this, new EStreamEventArgs(this));
        }

        public event EventHandler<EStreamBytesEventArgs> BytesReceived;
        protected virtual void OnBytesReceived(byte[] bytes)
        {
            EventHandler<EStreamBytesEventArgs> handler = BytesReceived;
            if (handler != null) handler(this, new EStreamBytesEventArgs(this, bytes));
        }

        public event EventHandler<EStreamExceptionEventArgs> Error;
        protected virtual void OnError(Exception exception)
        {
            EventHandler<EStreamExceptionEventArgs> handler = Error;
            if (handler != null) handler(this, new EStreamExceptionEventArgs(this, exception));
        }

        #endregion Events

        #region Dispose

        private bool disposed = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                // Free any managed objects here.
                try { Close(); } catch { }
                if (_stream != null)
                {
                    try { _stream.Close(); } catch { }
                    try { _stream.Dispose(); } catch { }
                }
                if (_heartbeatIntervalTimer != null)
                {
                    try { _heartbeatIntervalTimer.Stop(); } catch { }
                    try { _heartbeatIntervalTimer.Elapsed -= _heartbeatIntervalTimer_Elapsed; } catch { }
                    try { _heartbeatIntervalTimer.Dispose(); } catch { }
                }
                if (_heartbeatTimeoutTimer != null)
                {
                    try { _heartbeatTimeoutTimer.Stop(); } catch { }
                    try { _heartbeatTimeoutTimer.Elapsed -= _heartbeatIntervalTimer_Elapsed; } catch { }
                    try { _heartbeatTimeoutTimer.Dispose(); } catch { }
                }
            }

            // Free any unmanaged objects here
            disposed = true;
        }
        ~EStream()
        {
            Dispose(false);
        }

        #endregion Dispose

        #region Private Classes

        protected class QueuedMessage
        {
            public byte[] MessageBytes;
            public TaskCompletionSource<bool> TCS;
            public CancellationToken MessageCT;
            public bool IsHeartbeat;
            public QueuedMessage(byte[] messageBytes, TaskCompletionSource<bool> tcs, CancellationToken messageCt, bool isHeartbeat)
            {
                MessageBytes = messageBytes;
                TCS = tcs;
                MessageCT = messageCt;
                IsHeartbeat = isHeartbeat;
            }
        };

        protected class ReadingMessage
        {
            public int prefixRead;
            public int suffixRead;
            public MemoryStream dataBuilder;
            public int sizeRead;
            public byte[] size;
            public int dataRead;
            public byte[] data;
            public byte[] buffer;
            public int bufferStart;
            public int bufferCount;

            public ReadingMessage(int bufferSize)
            {
                buffer = new byte[bufferSize];
            }
        }

        #endregion Private Classes
    }

    #region Connection Settings

    public enum EStreamMessageFraming
    {
        Delimited = 0,
        SizePrefixed = 1,
        FixedSize = 2,
        Raw = 3,
    }

    public enum EStreamPrefixTypeSize
    {
        Int16 = 2,
        Int32 = 4,
        Int64 = 8,
    }

    public class EStreamHeartbeatSettings
    {
        public byte[] SendBytes { get; set; }
        public TimeSpan SendInterval { get; set; }
        public byte[] ReceiveBytes { get; set; }
        public TimeSpan ReceiveTimeout { get; set; }

        public EStreamHeartbeatSettings()
        {
            SendBytes = new byte[0];
            SendInterval = TimeSpan.Zero;
            ReceiveBytes = new byte[0];
            ReceiveTimeout = TimeSpan.Zero;
        }

        public EStreamHeartbeatSettings GetDeepCopy()
        {
            return new EStreamHeartbeatSettings()
            {
                SendBytes = SendBytes.ToArray(),
                SendInterval = SendInterval,
                ReceiveBytes = ReceiveBytes.ToArray(),
                ReceiveTimeout = ReceiveTimeout,
            };
        }
    }

    public class EStreamMessageFramingSettings
    {
        public EStreamMessageFraming FramingType { get; set; }
        public EStreamPrefixTypeSize SizePrefixedType { get; set; }
        public byte[] DelimitedPrefix { get; set; }
        public byte[] DelimitedSuffix { get; set; }
        public int FixedSize { get; set; }
        public int MaxMessageSize { get; set; }

        public EStreamMessageFramingSettings()
        {
            FramingType = EStreamMessageFraming.Delimited;
            SizePrefixedType = EStreamPrefixTypeSize.Int32;
            DelimitedPrefix = new byte[0];
            DelimitedSuffix = new byte[1] { 0x04 }; // 0x04 = EOT = End of transmission
            FixedSize = 0;
            MaxMessageSize = 1048576;
        }

        public EStreamMessageFramingSettings GetDeepCopy()
        {
            return new EStreamMessageFramingSettings()
            {
                FramingType = FramingType,
                SizePrefixedType = SizePrefixedType,
                DelimitedPrefix = DelimitedPrefix.ToArray(),
                DelimitedSuffix = DelimitedSuffix.ToArray(),
                FixedSize = FixedSize,
                MaxMessageSize = MaxMessageSize,
            };
        }

    }

    #endregion Connection Settings

    #region EventArgs

    public class EStreamEventArgs : EventArgs
    {
        public EStream EStream { get; set; }

        public EStreamEventArgs(EStream eStream)
        {
            EStream = eStream;
        }
    }

    public class EStreamBytesEventArgs : EventArgs
    {
        public EStream EStream { get; set; }
        public byte[] Bytes { get; set; }

        public EStreamBytesEventArgs(EStream eStream, byte[] bytes)
        {
            EStream = eStream;
            Bytes = bytes;
        }
    }

    public class EStreamExceptionEventArgs : EventArgs
    {
        public EStream EStream { get; set; }
        public Exception Exception { get; set; }

        public EStreamExceptionEventArgs(EStream eStream, Exception exception)
        {
            EStream = eStream;
            Exception = exception;
        }
    }

    public class EStreamItemReceivedEventArgs<T> : EventArgs
    {
        public EStream EStream { get; set; }
        public T Item { get; set; }
        public byte[] Bytes { get; set; }
        public Exception SerializationException { get; set; }

        public EStreamItemReceivedEventArgs(EStream eStream, T item, byte[] bytes, Exception serializationException)
        {
            EStream = eStream;
            Item = item;
            Bytes = bytes;
            SerializationException = serializationException;
        }
    }

    #endregion EventArgs

}
