﻿using BarCodeReader.Device;
using EAgile.Extentions.Byte;
using DFD;
using EStreamWrapper;
using System;
using System.Threading;
using System.IO;
using EAgile.SettingsXml;
using EStreamWrapper.Com;
using System.Threading.Tasks;

namespace EABarCodeSDK
{
    public class BarCodeReader
    {
        public SerialBarcodeReader BarcodeReader = null;
        private DFDSettings Settings { get; set; }
        private CancellationTokenSource BarcodeReaderCts = new CancellationTokenSource();

        public static DirectoryInfo SettingsFolder = new DirectoryInfo(@"C:\ProgramData\eAgile\AssociationKiosk\Settings");
        public static FileInfo SettingsFile = new FileInfo(Path.Combine(SettingsFolder.FullName, "Settings.xml"));
        private string ReaderErrorMsg { get; set; }
        private string SettingsErrorMsg { get; set; }

        public bool IsConnected => false;

        public BarCodeReader()
        {
            Settings = new DFDSettings();
            Settings.SetCreateIfNotFound(true);
            Settings.SetProtectIfUnprotected(true);
            Settings.SettingsChanged += Settings_SettingsChanged; ;
            Settings.StartMonitoring(SettingsFile, TimeSpan.FromSeconds(5));
        }

        private void Settings_SettingsChanged(object sender, ESettingsChangedEventArgs e)
        {
            if (e.Error != null)
            {
                SettingsErrorMsg = "Application settings could not be loaded:";
                SettingsErrorMsg += Environment.NewLine;
                SettingsErrorMsg += e.Error.Message;
            }
            else if (e.ESettingsAggregateException != null)
            {
                SettingsErrorMsg = "Invalid Settings:";
                foreach (ESettingsException ese in e.ESettingsAggregateException.InnerExceptions)
                {
                    SettingsErrorMsg += Environment.NewLine;
                    SettingsErrorMsg += ese.SettingName + ": " + ese.Message;
                }
            }
            else
            {
                SettingsErrorMsg = string.Empty;
            }

            if (e.OldSettings != null && e.OldSettings is DFDSettings oldSettings)
            {

                if (BarcodeReader != null &&
                    (!EComPort.Equals(BarcodeReader.EComPort, Settings.BarcodeEComPort)
                    || !BarcodeReader.MessageFramingSettings.DelimitedPrefix.EqualsBytes(Settings.BarcodePrefixHex.AsHex().ToBytes())
                    || !BarcodeReader.MessageFramingSettings.DelimitedSuffix.EqualsBytes(Settings.BarcodeSuffixHex.AsHex().ToBytes())
                    ))
                {
                    CloseBarcodeReader();
                }
            }
        }

        public void OpenBarcodeReader()
        {
            if (BarcodeReader == null)
            {
                BarcodeReader = new SerialBarcodeReader(Settings.BarcodeEComPort)
                {
                    AutoReconnect = true,
                    AutoReconnectInterval = TimeSpan.FromSeconds(1),
                    HeartbeatSettings = new EStreamHeartbeatSettings()
                    {
                        ReceiveBytes = new byte[0],
                        ReceiveTimeout = TimeSpan.Zero,
                        SendBytes = new byte[0],
                        SendInterval = TimeSpan.Zero,
                    },
                    MessageFramingSettings = new EStreamMessageFramingSettings()
                    {
                        FramingType = EStreamMessageFraming.Delimited,
                        DelimitedPrefix = Settings.BarcodePrefixHex.AsHex().ToBytes(),
                        DelimitedSuffix = Settings.BarcodeSuffixHex.AsHex().ToBytes(),
                    },
                    ConnectionTimeout = TimeSpan.FromSeconds(1),
                };
                BarcodeReader.ClientConnected += BarcodeReader_ClientConnected;
                BarcodeReader.ClientDisconnected += BarcodeReader_ClientDisconnected;
                BarcodeReader.ClientBytesReceived += BarcodeReader_ClientBytesReceived;
                BarcodeReader.ClientError += BarcodeReader_ClientError;


                BarcodeReader.Start();
            }
        }

        public async Task CloseBarcodeReader()
        {
            if (BarcodeReader != null)
            {
                BarcodeReader.ClientBytesReceived -= BarcodeReader_ClientBytesReceived;
                BarcodeReader.ClientConnected -= BarcodeReader_ClientConnected;
                BarcodeReader.ClientDisconnected -= BarcodeReader_ClientDisconnected;
                BarcodeReader.ClientError -= BarcodeReader_ClientError;
                try { BarcodeReader.Dispose(); } catch { } finally { BarcodeReader = null; }
            }
        }

        private void BarcodeReader_ClientError(object sender, EStreamExceptionEventArgs e)
        {

        }
        public delegate void BarcodeReader_ClientBytesReceivedDelegate(object sender, EStreamBytesEventArgs e);
        private void BarcodeReader_ClientBytesReceived(object sender, EStreamBytesEventArgs e)
        {
            if (BarcodeReaderCts.IsCancellationRequested)
            {
                return;
            }

            int length = 0;
            for (length = 0; length < e.Bytes.Length; length++)
            {
                // break at the fisrt non-printable ascii character
                if (e.Bytes[length] < 0x20 || e.Bytes[length] > 0x7E)
                    break;
            }

            string barcode = null;
            if (length > 0)
            {

                barcode = e.Bytes.ToAscii(0, length);
                OnChangeBarcodeOccurred(barcode);
                Console.Beep();
            }
        }

        public event EventHandler<string> BarCodeEvent;
        protected virtual void OnChangeBarcodeOccurred(string newBarcode)
        {
            try { BarCodeEvent?.Invoke(this, newBarcode); } catch { }
        }


        private void BarcodeReader_ClientDisconnected(object sender, EStreamEventArgs e)
        {
            // IsConnected = false;
        }

        private void BarcodeReader_ClientConnected(object sender, EStreamEventArgs e)
        {
            // IsConnected = true;
        }


    }
}
