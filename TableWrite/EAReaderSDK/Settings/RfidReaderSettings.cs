﻿
using EA.Impinj;
using System;
using System.Collections.Generic;
using System.Text;
using static EAReaderSDK.SupportClasses.DeviceTypes;

namespace EAReaderSDK.Settings
{
    public class RfidReaderSettings
    {
        public string DeviceName { get; set; } = "RFIDIndexer";
        public DeviceType DeviceType { get; set; } = DeviceType.rfid;
        public bool IsEnabled { get; set; } = true;
         public string Address { get; set; } = "10.1.6.104";
       // public string Address { get; set; } = "192.168.106.100";
        public int Port { get; set; } = 5084;
        public int HeartbeatInterval { get; set; } = 5000;
        public int ConnectTimeout { get; set; } = 5000;
        public int CommandTimeout { get; set; } = 5000;
        public bool UseETSI { get; set; } = false;
        public List<double> EtsiTxFrequenciesInMhz { get; set; } = new List<double>();
        public ReaderMode ReaderMode { get; set; } = ReaderMode.AutoSetDenseReaderDeepScan;
        public SearchMode SearchMode { get; set; } = SearchMode.SingleTarget;
        public Session Session { get; set; } = Session.S0;
        public ushort TagPopulationEstimate { get; set; } = 20;
        public double TxPowerInDbm { get; set; } = 30;
        public double RxSensitivityInDbm { get; set; } = -70;
        public List<int> Antennas { get; set; } = new List<int>() { 1 };
        public int ScanDuration { get; set; } = 15000;
        public bool LockCheck { get; set; } = true;
        public bool UserCheck { get; set; } = true;
    }
}
