﻿namespace EAReaderSDK.Settings
{
    using EA.Impinj;
    using System.Collections.Generic;
    using static EAReaderSDK.SupportClasses.DeviceTypes;

    public class RfidValidatorSettings
    {
        public string DeviceName { get; set; } = "RFIDValidator";
        public DeviceType DeviceType { get; set; } = DeviceType.validator;
        public bool IsEnabled { get; set; } = true;
        public string Address { get; set; } = "10.1.6.105";
        public int Port { get; set; } = 5084;
        public int HeartbeatInterval { get; set; } = 5000;
        public int ConnectTimeout { get; set; } = 5000;
        public int CommandTimeout { get; set; } = 5000;
        public bool UseETSI { get; set; } = false;
        public List<double> EtsiTxFrequenciesInMhz { get; set; } = new List<double>();
        public ReaderMode ReaderMode { get; set; } = ReaderMode.AutoSetDenseReaderDeepScan;
        public SearchMode SearchMode { get; set; } = SearchMode.DualTarget;
        public Session Session { get; set; } = Session.S1;
        public ushort TagPopulationEstimate { get; set; } = 20;
        public double TxPowerInDbm { get; set; } = 25;
        public double RxSensitivityInDbm { get; set; } = -60;
        public List<int> Antennas { get; set; } = new List<int>() { 1 };
        public int ScanDuration { get; set; } = 5000;
        public bool LockCheck { get; set; } = false;
        public bool UserCheck { get; set; } = false;

    }
}
