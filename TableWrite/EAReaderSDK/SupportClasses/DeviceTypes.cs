﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EAReaderSDK.SupportClasses
{
    public class DeviceTypes
    {

        public enum DeviceType
        {
            kiosk = 10,
            rfid = 20,
            writer = 30,
            validator = 40,

        }
    }
}
