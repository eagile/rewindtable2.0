﻿
using EA.Impinj;
using EAReaderSDK.Settings;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace EAReaderSDK.RFIDDevices
{
    public class RFIDReader : ImpinjReader
    {
        public bool AntennaDisconnected { get; protected set; }

        /// <summary>
        /// The reserved op identifier
        /// </summary>
        public const int RESERVED_OP_ID = 31;
        /// <summary>
        /// The user op identifier
        /// </summary>
        public const int USER_OP_ID = 32;

        /// <summary>
        /// Gets the line settings.
        /// </summary>
        /// <value>The line settings.</value>
        public RfidReaderSettings DeviceSettings { get; private set; } = new RfidReaderSettings();
        public RfidValidatorSettings ValidatorDeviceSettings { get; private set; } = new RfidValidatorSettings();
        public RfidWriterSettings WriterDeviceSettings { get; private set; } = new RfidWriterSettings();

        /// <summary>
        /// Gets or sets the reconnect delay.
        /// </summary>
        /// <value>The reconnect delay.</value>
        public int ReconnectDelay { get; set; } = 1000;

        /// <summary>
        /// The configuration lock
        /// </summary>
        private readonly SemaphoreSlim _configLock = new SemaphoreSlim(1, 1);
        /// <summary>
        /// The client configuration lock
        /// </summary>
        private readonly SemaphoreSlim _configClientLock = new SemaphoreSlim(1, 1);
        /// <summary>
        /// The connect CTS
        /// </summary>
        private CancellationTokenSource _connectCts = new CancellationTokenSource(0);
        /// <summary>
        /// The connect task
        /// </summary>
        private Task _connectTask = Task.CompletedTask;

        /// <summary>
        /// Gets the initialized.
        /// </summary>
        /// <value>The initialized.</value>
        public Task Initialized => _initialized.Task;

        /// <summary>
        /// The initialized
        /// </summary>
        private TaskCompletionSource<bool> _initialized = new TaskCompletionSource<bool>();

        public bool IsEnabled { get; private set; }

        public RFIDReader()
        {

        }

        public enum ConfigType
        {
            Main,
            Validator,
            Writer

        }


        /// <summary>
        /// configure as an asynchronous operation.
        /// </summary>
        /// <param name="config">The configuration.</param>
        public async Task ConfigureAsync(ConfigType configType)
        {

            //need to finish
            var config = new object();

            try
            {
                switch (configType)
                {
                    case ConfigType.Main:
                        config = DeviceSettings;
                        break;
                    case ConfigType.Validator:
                        config = ValidatorDeviceSettings;
                        break;
                    case ConfigType.Writer:
                        config = WriterDeviceSettings;
                        break;

                };
                await _configLock.WaitAsync().ConfigureAwait(false);

                try
                {
                    IsEnabled = (bool)config.GetType().GetProperty("IsEnabled").GetValue(config, null);
                    var Address = config.GetType().GetProperty("Address").GetValue(config, null).ToString();
                    var Port = (int)config.GetType().GetProperty("Port").GetValue(config, null);


                    if (Client?.IsConnected == true)
                    {
                        if (!IsEnabled || Address != Client.Address
                            || Port != Client.Port)
                        {
                            _connectCts.Cancel();
                            await _connectTask.ConfigureAwait(false);
                            _initialized.TrySetResult(true);
                            _initialized = new TaskCompletionSource<bool>();
                            await ResetAndDisconnectAsync().ConfigureAwait(false);
                        }
                    }

                    ConnectTimeout = (int)config.GetType().GetProperty("ConnectTimeout").GetValue(config, null);
                    ResponseTimeout = (int)config.GetType().GetProperty("CommandTimeout").GetValue(config, null);

                    if (IsEnabled)
                    {
                        if (_connectCts.IsCancellationRequested)
                        {
                            _connectCts = new CancellationTokenSource();
                            _connectTask = Task.Run(() => ConnectAsync(config, _connectCts.Token), _connectCts.Token);
                            await Initialized.ConfigureAwait(false);
                        }
                        else
                        {
                            await _configClientLock.WaitAsync(_connectCts.Token).ConfigureAwait(false);
                            try
                            {
                                await ConfigureClientAsync(config, _connectCts.Token).ConfigureAwait(false);

                            }
                            finally
                            {
                                _configClientLock.Release();
                            }
                        }
                    }
                    else
                    {
                        _connectCts.Cancel();
                        await _connectTask.ConfigureAwait(false);
                        await ResetAndDisconnectAsync().ConfigureAwait(false);
                        _initialized.TrySetResult(true);
                    }
                }
                finally
                {
                    _configLock.Release();
                }
            }
            catch (OperationCanceledException) { }
            catch (Exception ex)
            {
                await ResetAndDisconnectAsync(ex).ConfigureAwait(false);
            }
        }

        /// <summary>
        /// connect as an asynchronous operation.
        /// </summary>
        /// <param name="config">The configuration.</param>
        /// <param name="ct">The cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
        private async Task ConnectAsync(object config, CancellationToken ct)
        {
            try
            {
                while (!ct.IsCancellationRequested)
                {
                    try
                    {
                        if (!IsConnected)
                        {
                            await _configClientLock.WaitAsync(ct).ConfigureAwait(false);
                            try
                            {
                                if (!ct.IsCancellationRequested && !IsConnected)
                                {
                                    var Address = config.GetType().GetProperty("Address").GetValue(config, null).ToString();
                                    var Port = (int)config.GetType().GetProperty("Port").GetValue(config, null);

                                    await ConnectAsync(Address, Port, false).ConfigureAwait(false);
                                    await ConfigureClientAsync(config, ct).ConfigureAwait(false);
                                }
                            }
                            finally
                            {
                                _configClientLock.Release();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        await ResetAndDisconnectAsync(ex).ConfigureAwait(false);
                    }
                    finally
                    {
                        _initialized.TrySetResult(true);
                    }
                    await Task.Delay(ReconnectDelay, ct).ConfigureAwait(false);
                }
            }
            catch (OperationCanceledException) { }
            catch (Exception ex)
            {
                await ResetAndDisconnectAsync(ex).ConfigureAwait(false);
            }
            finally
            {
                await ResetAndDisconnectAsync().ConfigureAwait(false);
            }
        }

        /// <summary>
        /// configure client as an asynchronous operation.
        /// </summary>
        /// <param name="config">The configuration.</param>
        /// <exception cref="Exception">Incorrect reader region</exception>
        /// <exception cref="Exception">No ETSI Frequencies Specified</exception>
        /// <exception cref="Exception">Missing Antenna</exception>
        /// <exception cref="Exception">Incorrect reader region</exception>
        /// <exception cref="Exception">No ETSI Frequencies Specified</exception>
        /// <exception cref="Exception">Missing Antenna</exception>
        private async Task ConfigureClientAsync(object config, CancellationToken ct)
        {
            ct.ThrowIfCancellationRequested();
            try
            {
                await StopReadingIfReadingAsync().ConfigureAwait(false);
                await DeleteAllOpSequencesAsync().ConfigureAwait(false);

                //AntennaStatusGroup antennaStatusGroup = await QueryAntennasAsync().ConfigureAwait(false);

                ImpinjSettings settings = QueryDefaultSettings();
                
                settings.ReaderMode = (ReaderMode)config.GetType().GetProperty("ReaderMode").GetValue(config, null);
                settings.SearchMode = (SearchMode)config.GetType().GetProperty("SearchMode").GetValue(config, null);
                settings.Session = (Session)config.GetType().GetProperty("Session").GetValue(config, null);
                settings.TagPopulationEstimate = (ushort)config.GetType().GetProperty("TagPopulationEstimate").GetValue(config, null); 

                settings.Report.Mode = ReportMode.Individual;
                settings.Report.BufferMode = ReportBufferMode.LowLatency;
                settings.Report.IncludeFastId = true;
                settings.Report.IncludeFirstSeenTime = true;
                settings.Report.IncludePeakRssi = true;
                settings.Report.IncludeAntennaPortNumber = true;

                if ((bool)config.GetType().GetProperty("UseETSI").GetValue(config, null) && ReaderCapabilities.CommunicationsStandard != CommunicationsStandardType.ETSI_302_208
                    || !(bool)config.GetType().GetProperty("UseETSI").GetValue(config, null) && ReaderCapabilities.CommunicationsStandard == CommunicationsStandardType.ETSI_302_208)
                {
                    throw new Exception("Incorrect reader region"); // TODO: Configuration Exception
                }

                if ((bool)config.GetType().GetProperty("UseETSI").GetValue(config, null))
                {
                    List<double> etsi = (List<double>)config.GetType().GetProperty("EtsiTxFrequenciesInMhz").GetValue(config, null);
                    if (etsi.Count > 0)
                        settings.TxFrequenciesInMhz = new List<double>(etsi);
                    else
                        settings.TxFrequenciesInMhz = new List<double>() { 865.7, 866.3, 866.9, 867.5 }; // Default to all frequencies
                }

                settings.Gpis.DisableAll();

                settings.Antennas.DisableAll();
                List<int> antennas = (List<int>)config.GetType().GetProperty("Antennas").GetValue(config, null);
                
                foreach (int num in antennas)
                {
                    //if (!antennaStatusGroup.GetAntenna((ushort)num).IsConnected)
                    //{
                    //    AntennaDisconnected = true;
                    //    throw new Exception("Missing Antenna"); // TODO: Configuration Exception, Missing Antenna
                    //}

                    var antenna = settings.Antennas.GetAntenna((ushort)num);
                    antenna.IsEnabled = true;
                    antenna.TxPowerInDbm =(double) config.GetType().GetProperty("TxPowerInDbm").GetValue(config, null);
                    antenna.RxSensitivityInDbm = (double)config.GetType().GetProperty("RxSensitivityInDbm").GetValue(config, null);
                }
                AntennaDisconnected = false;

                if ((bool)config.GetType().GetProperty("LockCheck").GetValue(config, null))
                {
                    settings.Report.OptimizedReadOps.Add(
                        new TagReadOp()
                        {
                            MemoryBank = MemoryBank.Reserved,
                            Id = RESERVED_OP_ID,
                            WordPointer = 4,
                            WordCount = 1,
                        });
                }

                if ((bool)config.GetType().GetProperty("UserCheck").GetValue(config, null))
                {
                    settings.Report.OptimizedReadOps.Add(
                        new TagReadOp()
                        {
                            MemoryBank = MemoryBank.User,
                            Id = USER_OP_ID,
                            WordPointer = 0,
                            WordCount = 16, // 256 bits
                        });
                }

                int heartbeatInterval = (int)config.GetType().GetProperty("HeartbeatInterval").GetValue(config, null) ;
                if (heartbeatInterval > 0)
                {
                    settings.Keepalives.Enabled = true;
                    settings.Keepalives.PeriodInMs = (uint)heartbeatInterval;
                    settings.Keepalives.EnableLinkMonitorMode = true;
                    settings.Keepalives.LinkDownThreshold = 2;
                }

                await ApplySettingsAsync(settings).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to Configure Client", ex);
            }
        }

        public async Task StopReadingIfReadingAsync()
        {
            try
            {
                if (IsConnected)
                {
                    bool isSingulating = true;
                    //try { isSingulating = await QuerySingulatingAsync().ConfigureAwait(false); } catch { isSingulating = true; }
                    //if (isSingulating)
                    //{
                    //    await StopAsync().ConfigureAwait(false);
                    //}
                }
            }
            catch { }
        }

        public async Task<List<uint>> GetOpSequenceIDsAsync()
        {
          
                return await Client.GetOpSequenceIDsAsync().ConfigureAwait(false);
          

           
        }

        public async Task ResetAndDisconnectAsync(Exception ex = null)
        {
            if (IsConnected)
            {
                try { await StopReadingIfReadingAsync().ConfigureAwait(false); } catch { }
                try { await ApplyDefaultSettingsAsync().ConfigureAwait(false); } catch { }
            }
            bool result = false;
            try { result = await DisconnectAsync(ex).ConfigureAwait(false); } catch { }
            if (!result)
            {
                OnConnectionChanged(new ConnectionEvent(DateTime.UtcNow, IsConnected, ex));
            }
        }
    }
}
