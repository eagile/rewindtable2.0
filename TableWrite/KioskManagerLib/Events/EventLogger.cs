﻿using System;

namespace KioskManager.Events
{
    public class EventLogger
    {
        public void Error(KioskManagerException lme)
            => Log(FkErrorLevel.Error, lme.InnerException, lme.Message, lme.EventCode);
        public void Error(FkEventCode eventCode)
            => Log(FkErrorLevel.Error, null, eventCode.GetMessage(), eventCode);
        public void Error(Exception ex, FkEventCode eventCode)
            => Log(FkErrorLevel.Error, ex, eventCode.GetMessage(), eventCode);
        public void Error(FkEventCode eventCode, params object[] messageArgs)
            => Log(FkErrorLevel.Error, null, eventCode.GetMessage(messageArgs), eventCode);
        public void Error(Exception ex, FkEventCode eventCode, params object[] messageArgs)
            => Log(FkErrorLevel.Error, ex, eventCode.GetMessage(messageArgs), eventCode);

        public void Warn(FkEventCode eventCode)
            => Log(FkErrorLevel.Warn, null, eventCode.GetMessage(), eventCode);
        public void Warn(Exception ex, FkEventCode eventCode)
            => Log(FkErrorLevel.Warn, ex, eventCode.GetMessage(), eventCode);
        public void Warn(FkEventCode eventCode, params object[] messageArgs)
            => Log(FkErrorLevel.Warn, null, eventCode.GetMessage(messageArgs), eventCode);
        public void Warn(Exception ex, FkEventCode eventCode, params object[] messageArgs)
            => Log(FkErrorLevel.Warn, ex, eventCode.GetMessage(messageArgs), eventCode);

        public void Info(FkEventCode eventCode)
            => Log(FkErrorLevel.Info, null, eventCode.GetMessage(), eventCode);
        public void Info(Exception ex, FkEventCode eventCode)
            => Log(FkErrorLevel.Info, ex, eventCode.GetMessage(), eventCode);
        public void Info(FkEventCode eventCode, params object[] messageArgs)
            => Log(FkErrorLevel.Info, null, eventCode.GetMessage(messageArgs), eventCode);
        public void Info(Exception ex, FkEventCode eventCode, params object[] messageArgs)
            => Log(FkErrorLevel.Info, ex, eventCode.GetMessage(messageArgs), eventCode);

        public void Debug(FkEventCode eventCode)
            => Log(FkErrorLevel.Debug, null, eventCode.GetMessage(), eventCode);
        public void Debug(Exception ex, FkEventCode eventCode)
            => Log(FkErrorLevel.Debug, ex, eventCode.GetMessage(), eventCode);
        public void Debug(FkEventCode eventCode, params object[] messageArgs)
            => Log(FkErrorLevel.Debug, null, eventCode.GetMessage(messageArgs), eventCode);
        public void Debug(Exception ex, FkEventCode eventCode, params object[] messageArgs)
            => Log(FkErrorLevel.Debug, ex, eventCode.GetMessage(messageArgs), eventCode);

        public void Trace(FkEventCode eventCode)
            => Log(FkErrorLevel.Trace, null, eventCode.GetMessage(), eventCode);
        public void Trace(Exception ex, FkEventCode eventCode)
            => Log(FkErrorLevel.Trace, ex, eventCode.GetMessage(), eventCode);
        public void Trace(FkEventCode eventCode, params object[] messageArgs)
            => Log(FkErrorLevel.Trace, null, eventCode.GetMessage(messageArgs), eventCode);
        public void Trace(Exception ex, FkEventCode eventCode, params object[] messageArgs)
            => Log(FkErrorLevel.Trace, ex, eventCode.GetMessage(messageArgs), eventCode);

        public void Log(FkErrorLevel errorLevel, Exception ex, string message, FkEventCode eventCode)
        {
            OnLogEvent(errorLevel, ex, message, eventCode);
        }

        public event EventHandler<LogEventArgs> LogEvent;
        protected virtual void OnLogEvent(FkErrorLevel errorLevel, Exception ex, string message, FkEventCode eventCode)
        {
            try { LogEvent?.Invoke(this, new LogEventArgs(errorLevel, ex, message, eventCode)); } catch { }
        }
    }
}
