﻿using System;

namespace KioskManager.Events
{
    public class LogEventArgs : EventArgs
    {
        public DateTimeOffset Timestamp { get; set; }
        public FkErrorLevel ErrorLevel { get; set; }
        public FkEventCode EventCode { get; set; }
        public string Message { get; set; }
        public Exception Error { get; set; }

        public LogEventArgs(FkErrorLevel errorLevel, Exception ex, string message, FkEventCode eventCode)
        {
            Timestamp = DateTimeOffset.Now;
            ErrorLevel = errorLevel;
            EventCode = eventCode;
            Message = message ?? eventCode.GetMessage();
            Error = ex;
        }

        public LogEventArgs(FkErrorLevel errorLevel, Exception ex, FkEventCode eventCode, string[] messageArgs)
            : this(errorLevel, ex, eventCode.GetMessage(messageArgs), eventCode) { }
    }
}
