﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace KioskManager.Events
{
    [Serializable]
    public class AggregateKioskManagerException : AggregateException
    {
        public ReadOnlyCollection<KioskManagerException> InnerKioskManagerExceptions { get; }

        public AggregateKioskManagerException(params KioskManagerException[] innerExceptions) : base(innerExceptions)
        {
            InnerKioskManagerExceptions = GetInnerKioskManagerExceptions();
        }

        public AggregateKioskManagerException(IEnumerable<KioskManagerException> innerExceptions) : base(innerExceptions)
        {
            InnerKioskManagerExceptions = GetInnerKioskManagerExceptions();
        }

        protected AggregateKioskManagerException(SerializationInfo serializationInfo, StreamingContext streamingContext) : base(serializationInfo, streamingContext)
        {
            InnerKioskManagerExceptions = GetInnerKioskManagerExceptions();
        }

        private ReadOnlyCollection<KioskManagerException> GetInnerKioskManagerExceptions()
        {
            if (InnerExceptions.Count == 0)
                throw new ArgumentNullException(nameof(InnerExceptions));
            List<KioskManagerException> kioskManagerExceptions = new List<KioskManagerException>(InnerExceptions.Count);
            foreach (Exception exception in InnerExceptions)
            {
                if (exception != null && exception is KioskManagerException kioskManagerException)
                    kioskManagerExceptions.Add(kioskManagerException);
                else
                    throw new ArgumentNullException(nameof(InnerExceptions));
            }
            if (kioskManagerExceptions.Count == 0)
                throw new ArgumentNullException(nameof(InnerExceptions));
            return new ReadOnlyCollection<KioskManagerException>(kioskManagerExceptions);
        }
    }

    [Serializable()]
    public class KioskManagerException : Exception
    {
        public FkEventCode EventCode => _eventCode;
        protected FkEventCode _eventCode;

        public int EventCodeNumber => (int)_eventCode;

        public DateTimeOffset Timestamp => _timestamp;
        private DateTimeOffset _timestamp;

        public KioskManagerException(FkEventCode eventCode, params object[] messageArgs) : base(eventCode.GetMessage(messageArgs))
        {
            _eventCode = eventCode;
            _timestamp = DateTimeOffset.Now;
        }

        public KioskManagerException(Exception innerException, FkEventCode eventCode, params object[] messageArgs) : base(eventCode.GetMessage(messageArgs), innerException)
        {
            _eventCode = eventCode;
            _timestamp = DateTimeOffset.Now;
        }

        protected KioskManagerException(SerializationInfo serializationInfo, StreamingContext streamingContext) : base(serializationInfo, streamingContext)
        {
            _eventCode = (FkEventCode)serializationInfo.GetInt32("EventCode");
            long timstampTicks = serializationInfo.GetInt64("Timestamp");
            long timstampOffsetTicks = serializationInfo.GetInt64("TimestampOffset");
            _timestamp = new DateTimeOffset(timstampTicks, TimeSpan.FromTicks(timstampOffsetTicks));
        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo serializationInfo, StreamingContext streamingContext)
        {
            base.GetObjectData(serializationInfo, streamingContext);
            serializationInfo.AddValue("EventCode", (int)_eventCode);
            serializationInfo.AddValue("Timestamp", _timestamp.Ticks);
            serializationInfo.AddValue("TimestampOffset", _timestamp.Offset.Ticks);

        }
    }
}
