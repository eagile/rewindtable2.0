﻿using EAManagerLib.Cultures;
using System;
using System.ComponentModel;

namespace KioskManager.Events
{
    public static class EventCodeExt
    {
        /// <summary>
        /// Get the localized event message.
        /// Include any string format arguments starting with argument {1}.
        /// String format argument {0} is always the event code and does not need to be included.
        /// </summary>
        /// <param name="eventCode">The event code for which to get the localized message</param>
        /// <param name="eventMessageArgs">Any string format arguments to be applied to the localized message starting at {1}</param>
        /// <returns></returns>
        public static string GetMessage(this FkEventCode eventCode, params object[] eventMessageArgs)
        {
            string localizedString = StringResources.GetLocalizedString(EventStrings.ResourceManager, eventCode.ToString());
            if (localizedString != null)
            {
                try
                {
                    if (eventMessageArgs == null || eventMessageArgs.Length == 0)
                    {
                        localizedString = string.Format(localizedString, (int)eventCode);
                    }
                    else
                    {
                        object[] args = new object[eventMessageArgs.Length + 1];
                        args[0] = (int)eventCode;
                        Array.Copy(eventMessageArgs, 0, args, 1, eventMessageArgs.Length);
                        localizedString = string.Format(localizedString, args);
                    }
                }
                catch (Exception)
                {
                    localizedString = null;
                }
            }
            if (localizedString == null)
            {
                // Resouce string not found
                if (eventMessageArgs == null || eventMessageArgs.Length == 0)
                    return $"[{(int)eventCode},{eventCode.ToString()}]";
                else
                    return $"[{(int)eventCode},{eventCode.ToString()},{string.Join(',', eventMessageArgs)}]";
            }
            return localizedString;
        }

        public static int GetNumber(this FkEventCode eventCode)
        {
            return (int)eventCode;
        }
    }

    public enum FkErrorLevel
    {
        Error,
        Warn,
        Info,
        Debug,
        Trace,
    }

    public enum FkEventCode
    {
        ConfigurationFilesLoaded = 9003,

        Close_Application = 9900,
        Shutdown_Computer = 9901,

        Rfid_Read_Started = 9010,
        Rfid_Read_Finished = 9011,
        Rfid_Read_Error = 9012,
        

        Kiosk_Rfid_AntennaConnected = 9013,
        Kiosk_Rfid_AntennaDisconnected = 9014,
        Kiosk_Rfid_ReadingStarted = 9015,
        Kiosk_Rfid_ReadingStopped = 9016,
        Kiosk_Rfid_Connected = 9017,
        Kiosk_Rfid_Disconnected = 9018,
        Kiosk_Rfid_Disconnected_Error = 9019,

        Log_Queue_Processor_Recovered = 9120,

        Rfid_Read_Lookup_Success = 9130,
        Rfid_Read_Lookup_Fail = 9131,

        Database_Backup_Success = 9200,
        Database_Backup_Failed = 9201,
        Database_Truncate_Success = 9202,
        Database_Truncate_Failed = 9203,
        Database_Unavailable = 9204,
        Database_Error = 9205,
        System_Backup_Success = 9210,
        System_Backup_Fail = 9211,
        System_LogBackup_Success = 9212,
        System_LogBackup_Fail = 9213,

        Database_Size_Exceeded = 1101,

        

        //User_Saved = 1102,
        User_Updated_Name = 1103,
        User_Updated_Description = 1104,
        User_Updated_Active = 1105,
        //User_Updated_Rights = 1106,
        User_save_database_error = 1107,
        User_save_error = 1108,
        User_save_success = 1109,
        User_Removed_Rights = 1110,
        User_Added_Rights = 1111,
        User_save_Password = 1112,
        User_Updated_Name_Failed = 1113,
        User_Updated_Desc_Failed = 1114,
        User_Updated_Password_Failed = 1115,
        User_Updated_PasswordLength_Failed = 1116,
        User_Updated_Password_Mismatch = 1117,
        User_No_Rights_Selected = 1118,

        Log_Batch_Printed = 1120,
        Log_Batch_Print_Fail = 1121,
        //plm_local_database_not_configured = 1102,
        //rlm_global_database_not_configured = 1103,
        //rlm_local_database_not_configured = 1104,
        //plm_line_name_not_configured = 1105,
        //rlm_line_name_not_configured = 1106,
        //RLM_Local_Database_Unavailable = 1107,
        //Main_Window_Starting_Error = 1108,
        //RLM_Local_Database_Created = 1109,
        //Error_creating_RFID_cache_database = 1110,
        //Error_starting_RFID_Line_Manager = 1111,
        //Production_Start = 1112,
        //Production_Stop = 1113,
        //Production_Finish = 1114,
        //RLM_GLobal_Database_Unavailable = 1115,
        //RLM_GLobal_Database_Server_Unavailable = 1116,
        //RLM_Local_Database_Server_Unavailable = 1117,

        //Username_Cannot_Be_Blank = 1124,
        Invalid_Login = 1125,
        User_account_locked = 1126,
        Password_is_expired_and_must_be_updated_through_PCE = 1127,
        //Invalid_password = 1128,
        User_does_not_have_login_rights = 1129,
        Login_Successfull = 1130,
        ///// <summary>
        ///// {1} = Username
        ///// </summary>
        //Login_Attempted = 1131,
        //Error_printing_settings = 1132,
        Logout_Successfull = 1133,


        //RF_device_exception = 1136,
        //Error_communicating_with_RF_device = 1137,
        //PCE_Not_Ready = 1138,
        //OCS_system_not_ready = 1139,
        //Dividella_system_not_ready = 1140,
        //OCS_critical_error = 1141,
        //Hardware_Bypass_Enabled = 1142,
        //PLC_Disconnected = 1143,
        //ValidationService_Error = 1144,
        //WriteService_Error = 1145,
        //Software_Bypass_Enabled = 1146,
        //Software_Bypass_Disabled = 1147,
        //Order_missing_GTIN = 1148,
        //Hardware_Bypass_Disabled = 1149,
        //Current_Order_is_Invalid = 1150,

        ///// <summary>
        ///// {1} = Order Name
        ///// {2} = SKU
        ///// {3} = Lot
        ///// {4} = Expiry
        ///// </summary>
        //Order_Loaded = 1603,
        //RFID_Line_Manager_Stopping = 1604,
        //RFID_Line_Manager_Starting = 1605,
        //Error_Loading_Existing_RLM_Order = 1606,

        //Maintenance_Mode_Enabled = 1607,
        //Maintenance_Mode_Disabled = 1608,
        //Maintenance_Order_Loaded = 1609,
        //Maintenance_Order_Unloaded = 1610,

        //Error_Loading_Batch_to_PLC = 1613,
        //Maint_Mode_Enabled = 1614,
        //Maint_Mode_Disabled = 1615,
        //Error_Enabling_Bypass_Mode_on_PLC = 1616,
        //Error_Enabling_Maint_Mode_on_PLC = 1617,
        //Error_sending_operating_mode_to_PLC = 1618,
        //Error_Enabling_HMI_Ready_to_PLC = 1619,
        //Error_Sending_Stop_to_PLC = 1620,
        //Error_Sending_Start_to_PLC = 1621,
        //Error_Sending_Start_Batch_to_PLC = 1622,
        //Error_Sending_Stop_Batch_to_PLC = 1670,
        //Error_Reseting_PLC_Fault = 1623,
        //Error_Reseting_All_PLC_Faults = 1624,
        //Error_Reading_Updating_PLC_Status = 1625,
        //Error_Sending_Settings_To_PLC = 1626,
        //Error_Configuring_PLC = 1627,
        //Error_Connecting_To_PLC = 1628,
        //Error_Polling_PLC = 1629,
        //PLC_Heartbeat_read_data_size_invalid = 1630,
        //PLC_Heartbeat_timeout = 1631,
        //PLC_Heartbeat_Error = 1632,
        //Wipotec_OCS_Disconnected = 1633,
        //WriteService_Configuration_Error = 1634,
        //ValidationService_Configuration_Error = 1635,
        //Error_Processing_Write_Tag_Data = 1636,
        //Error_Processing_Validation_Tag_Data = 1637,
        //Error_Processing_Validation_Trigger_Data = 1638,
        //Error_Loading_Batch = 1639,
        //Error_Starting_Controller = 1640,
        Error_with_Log_Queue_Processor = 1641,
        //Error_with_Action_Queue_Processor = 1642,
        //Error_Checking_Controller_State = 1643,
        ///// <summary>
        ///// {1} = Error to clear
        ///// </summary>
        //Error_Confirmed = 1644,
        //All_Errors_Confirmed = 1645,
        //One_or_more_services_are_not_ready = 1646,
        //Production_Error_Starting_Batch = 1647,
        //Production_Error_Stopping_Batch = 1648,
        //Production_Error_Starting_Production = 1649,
        //Production_Error_Stopping_Production = 1650,
        //Production_Error_with_Production_Task = 1651,
        //Production_Error_with_Upload_Task = 1652,
        //Order_Monitor_Error_Monitor_Orders_Task = 1653,
        //Order_Monitor_Error_Check_Order = 1654,
        //Error_sending_operating_mode_to_PLC_PLC_Not_Connected = 1655,
        //Error_starting_batch_on_PLC_PLC_Not_Connected = 1656,
        //Error_stopping_batch_on_PLC_PLC_Not_Connected = 1657,
        //Error_sending_start_production_to_PLC_PLC_Not_Connected = 1658,
        //Error_sending_stop_production_to_PLC_PLC_Not_Connected = 1659,
        //Error_Reseting_All_PLC_Faults_PLC_Not_Connected = 1660,
        //Error_Reseting_PLC_Fault_PLC_Not_Connected = 1661,
        //Error_Timed_out_waiting_for_pending_PLC_Resets_for_Faults = 1662,
        //Error_Timed_out_waiting_for_pending_PLC_requests = 1663,
        //Error_Reading_Status_From_PLC_PLC_Not_Connected = 1664,
        //Error_Reading_Status_From_PLC = 1665,
        //Error_Writing_Status_To_PLC_PLC_Not_Connected = 1666,
        //Error_Writing_Status_To_PLC = 1667,
        //Power_Lost_Shutting_Down = 1668,
        //Application_Exiting = 1669,
        //User_Exiting_Application = 1670,
        //User_Restarting_Application = 1671,

        //Error_Message_not_found_in_Resource = 2000,

        //ValidationService_RefreshStatus_Failed = 3000,
        //ValidationService_ExecuteAsync_Failed = 3001,
        //ValidationService_ApplySettingsAsync_Failed = 3002,
        //ValidationService_Validation_Reader_Connected = 3003,
        //ValidationService_Validation_Reader_Disconnected = 3004,
        //ValidationService_TagsReported_Error = 3006,
        //ValidationService_GpiChanged_Error = 3007,
        //ValidationService_TagRead_Error = 3008,
        //ValidationService_TriggerStop_Error = 3009,
        ///// <summary>
        ///// {1} = Antenna Number
        ///// </summary>
        //ValidationService_Reader_AntennaDisconnected = 3069,
        //ValidationService_Reader_AntennaChanged_Error = 3070,
        //ValidationService_Reader_ValidationTriggerDataReported_Error = 3010,
        //ValidationService_ProcessTagChannelAsync_Error = 3011,
        //ValidationService_Reader_ValidationTagDataReported_Error = 3012,
        ///// <summary>
        ///// {1} = state
        ///// </summary>
        //ValidationService_PLCReset = 3014,
        ///// <summary>
        ///// {1} = state
        ///// </summary>
        //ValidationService_PLCRun = 3015,
        ///// <summary>
        ///// {1} = batch name
        ///// </summary>
        //ValidationService_PLCBatchChanged = 3016,
        //ValidationService_PLC_Connected = 3017,
        //ValidationService_PLC_Disconnected = 3018,
        //ValidationService_MessageReceived_Error = 3019,
        //ValidationService_Server_Listener_Started = 3020,
        //ValidationService_Server_Listener_Stopped = 3021,
        //ValidationService_Power_Lost_Shutting_Down = 3068,
        //User_Restarting_ValidationService = 3069,

        //WriteService_RefreshStatus_Failed = 3500,
        //WriteService_ExecuteAsync_Failed = 3501,
        //WriteService_ApplySettingsAsync_Failed = 3502,
        //WriteService_ProcessTagData_Error = 3503,
        //WriteService_ProcessTagAction_Error = 3504,
        //WriteService_Server_Listener_Started = 3505,
        //WriteService_Server_Listener_Stopped = 3506,
        //WriteService_Server_MessageReceived_Error = 3507,
        //WriteService_PLC_Connected = 3508,
        //WriteService_PLC_Disconnected = 3509,
        ///// <summary>
        ///// {1} = batch name
        ///// </summary>
        //WriteService_PLCBatchChanged = 3510,
        ///// <summary>
        ///// {1} = state
        ///// </summary>
        //WriteService_PLCRun = 3511,
        ///// <summary>
        ///// {1} = state
        ///// </summary>
        //WriteService_PLCReset = 3512,
        //WriteService_Index_Reader_Connected = 3513,
        //WriteService_Index_Reader_Disconnected = 3514,
        //WriteService_Write_Reader_Connected = 3515,
        //WriteService_Write_Reader_Disconnected = 3516,
        //WriteService_RfidIndexer_AntennaChanged_Error = 3517,
        //WriteService_RfidWriter_AntennaChanged_Error = 3518,
        //WriteService_HandleIndexTag_Error = 3519,
        //WriteService_HandleWriteTag_Error = 3520,
        //WriteService_Power_Lost_Shutting_Down = 3568,
        //WriteService_Indexer_TagsReported_Error = 3569,
        //WriteService_Writer_TagsReported_Error = 3570,
        //User_Restarting_WriteService = 3571,


        //PLC_Fault_Consecutive_Failures = 5500,
        //PLC_Fault_Consecutive_No_Replies = 5501,
        //PLC_Fault_HMI_Heartbeat_Timeout = 5502,
        //PLC_Fault_HMI_Not_Ready = 5503,
        //PLC_Fault_ValidationService_Heartbeat_Timeout = 5504,
        //PLC_Fault_ValidationService_Not_Ready = 5505,
        //PLC_Fault_WriteService_Heartbeat_Timeout = 5506,
        //PLC_Fault_WriteService_Not_Ready = 5507,
        //PLC_Fault_Early_Trigger = 5508,
        //PLC_Fault_Trigger_ID_Echo_Failure = 5509,
        //PLC_Fault_Wipotec_Not_Ready = 5510,
        //PLC_Fault_Flt_11 = 5511,
        //PLC_Fault_PCE_Not_Ready = 5512,
        //PLC_Fault_Wipotec_Faulted = 5513,
        //PLC_Fault_Flt_14 = 5514,
        //PLC_Fault_Flt_15 = 5515,
        //PLC_Fault_Flt_16 = 5516,
        //PLC_Fault_Wrong_Tag_Data = 5517,
        //PLC_Fault_Duplicate_Serial = 5518,
        //PLC_Fault_Flt_20 = 5519,
        //PLC_Fault_ValidationService_Faulted = 5520,
        //PLC_Fault_ValidationService_failed_to_load_batch = 5521,
        //PLC_Fault_ValidationService_RFID_not_running = 5522,
        //PLC_Fault_ValidationService_not_connected_to_HMI = 5523,
        //PLC_Fault_ValidationService_Validation_Reader_not_connected = 5524,
        //PLC_Fault_Flt_25 = 5525,
        //PLC_Fault_WriteService_Faulted = 5526,
        //PLC_Fault_WriteService_failed_to_load_batch = 5527,
        //PLC_Fault_WriteService_RFID_not_running = 5528,
        //PLC_Fault_WriteService_Index_Reader_not_connected = 5529,
        //PLC_Fault_WriteService_Write_Reader_not_connected = 5530,
        //PLC_Fault_Trigger_Out_Of_Sequence = 5531,

        //PLC_Warning_bNoReply = 5600,
        //PLC_Warning_Dividella_Not_Ready = 5601,
        //PLC_Warning_ReservedForFutureUse_5602 = 5602,
        //PLC_Warning_ReservedForFutureUse_5603 = 5603,
        //PLC_Warning_ReservedForFutureUse_5604 = 5604,
        //PLC_Warning_ReservedForFutureUse_5605 = 5605,
        //PLC_Warning_ReservedForFutureUse_5606 = 5606,
        //PLC_Warning_ReservedForFutureUse_5607 = 5607,
        //PLC_Warning_ReservedForFutureUse_5608 = 5608,
        //PLC_Warning_ReservedForFutureUse_5609 = 5609,
        //PLC_Warning_ReservedForFutureUse_5610 = 5610,
        //PLC_Warning_ReservedForFutureUse_5611 = 5611,
        //PLC_Warning_ReservedForFutureUse_5612 = 5612,
        //PLC_Warning_ReservedForFutureUse_5613 = 5613,
        //PLC_Warning_ReservedForFutureUse_5614 = 5614,
        //PLC_Warning_ReservedForFutureUse_5615 = 5615,
        //PLC_Warning_ReservedForFutureUse_5616 = 5616,
        //PLC_Warning_ReservedForFutureUse_5617 = 5617,
        //PLC_Warning_ReservedForFutureUse_5618 = 5618,
        //PLC_Warning_ReservedForFutureUse_5619 = 5619,
        //PLC_Warning_ReservedForFutureUse_5620 = 5620,
        //PLC_Warning_ReservedForFutureUse_5621 = 5621,
        //PLC_Warning_ReservedForFutureUse_5622 = 5622,
        //PLC_Warning_ReservedForFutureUse_5623 = 5623,
        //PLC_Warning_ReservedForFutureUse_5624 = 5624,
        //PLC_Warning_ReservedForFutureUse_5625 = 5625,
        //PLC_Warning_ReservedForFutureUse_5626 = 5626,
        //PLC_Warning_ReservedForFutureUse_5627 = 5627,
        //PLC_Warning_ReservedForFutureUse_5628 = 5628,
        //PLC_Warning_ReservedForFutureUse_5629 = 5629,
        //PLC_Warning_ReservedForFutureUse_5630 = 5630,
        //PLC_Warning_ReservedForFutureUse_5631 = 5631,

        ///// <summary>
        ///// {1} = Old Batch
        ///// {2} = New Batch
        ///// </summary>
        //PLC_Batch_Changed = 5700,
        ///// <summary>
        ///// {1} = Old Operating Mode
        ///// {2} = New Operating Mode
        ///// </summary>
        //PLC_Operating_Mode_Changed = 5701,
        //PLC_Batch_Not_Loaded = 5702,
        //PLC_Batch_Loaded = 5703,
        //PLC_Faulted = 5704,
        //PLC_Has_No_Faults = 5705,
        //PLC_Ready = 5706,
        //PLC_Not_Ready = 5707,
        //PLC_Running = 5708,
        //PLC_Not_Running = 5709,
        //PLC_Status_Changed = 5710,
        //PLC_Fault_Changed = 5711,
        //PLC_Warning_Changed = 5712,

        ///// <summary>
        ///// {1} = new sku
        ///// </summary>
        Sku_Added = 5713,

        //Error_printing_logs = 5714,
        //User_Logout = 5715,
        User_Logout_Due_To_Inactivity = 5716,
        //Error_viewing_batch_order_report = 5717,
        //Error_preparing_viewable_batch_order_report = 5718,
        //Error_printing_batch_order_Report = 5719,
        //Error_preparing_batch_order_report = 5720,
        //Error_retreiving_write_zone_data = 5721,
        //Error_retreiving_validation_zone_data = 5722,
        //Error_retreiving_batch_order_report_data = 5723,
        //Error_saving_sku = 5724,
        Unable_to_save_settings = 9000,
        Unable_to_save_settings_missingReason = 9001,
        Error_saving_settings_changes = 5725,
        Settings_saved = 9002,

        /// <summary>
        /// {1} = device name
        /// {2} = setting name
        /// {3} = old value
        /// {4} = new value
        /// </summary>
        system_settings_view_updated_line_setting = 5726,
        /// <summary>
        /// {1} = device name
        /// {2} = setting name
        /// {3} = old value
        /// {4} = new value
        /// {5} = reason
        /// </summary>
        system_settings_view_updated_line_setting_with_reason = 5727,
        /// <summary>
        /// {1} = product name
        /// {2} = device name
        /// {3} = setting name
        /// {4} = old value
        /// {5} = new value
        /// </summary>
        system_settings_view_updated_product_setting = 5728,
        /// <summary>
        /// {1} = product name
        /// {2} = device name
        /// {3} = setting name
        /// {4} = old value
        /// {5} = new value
        /// {6} = reason
        /// </summary>
        system_settings_view_updated_product_setting_with_reason = 5729,

        //Error_Processing_Log_Tag_Data = 5730,
        Error_Logging_In_User = 5731,
        Error_Logging_Out_User = 5732,
        Login_Database_Unavailable = 5733,

        //ValidationService_Server_HMI_Connected = 5734,
        //ValidationService_Server_HMI_Disconnected = 5735,
        //ValidationService_Closing = 5736,
        //ValidationService_Ready = 5737,
        ///// <summary>
        ///// {1} = state
        ///// </summary>
        //ValidationService_PLCPowerLost = 5738,

        ///// <summary>
        ///// {1} = state
        ///// </summary>
        //ValidationService_HasBatch_Changed = 5739,
        ///// <summary>
        ///// {1} = state
        ///// </summary>
        //ValidationService_BatchMatch_Changed = 5740,
        ///// <summary>
        ///// {1} = state
        ///// </summary>
        //ValidationService_ServiceReady_Changed = 5741,

        //WriteService_RfidIndexer_AntennaDisconnected = 5742,
        //WriteService_RfidWriter_AntennaDisconnected = 5743,

        //WriteService_Server_HMI_Connected = 5744,
        //WriteService_Server_HMI_Disconnected = 5745,
        //WriteService_Closing = 5746,
        //WriteService_Ready = 5747,
        ///// <summary>
        ///// {1} = state
        ///// </summary>
        //WriteService_PLCPowerLost = 5748,

        ///// <summary>
        ///// {1} = state
        ///// </summary>
        //WriteService_HasBatch_Changed = 5749,
        ///// <summary>
        ///// {1} = state
        ///// </summary>
        //WriteService_BatchMatch_Changed = 5750,
        ///// <summary>
        ///// {1} = state
        ///// </summary>
        //WriteService_ServiceReady_Changed = 5751,

        //ValidationService_TriggerProcessed = 5752, // info to console
        //WriteService_TagIndexed = 5753, // info to console
        //WriteService_TagWritten = 5754, // info to console

        //Error_Confirmed_Error = 5755,
        //All_Errors_Confirmed_Error = 5756,

        //PLC_Connected = 5757,
        //WriteService_Connected = 5758,
        //ValidationService_Connected = 5759,
        //Wipotec_OCS_Connected = 5760,

        //Invalid_Upload_Batch_Size = 5761,
        //OCS_Article_Not_Found_For_Sku = 5762,
        //OCS_Error_Setting_Article = 5763,
        //OCS_Error_Starting_Batch = 5764,
        //OCS_Error_Stopping_Batch = 5765,
        //OCS_Error_Checking_Stats = 5766,

        //Order_missing_HierarchyLevel = 5767,
        //Order_invalid_HierarchyLevel = 5768,
        //Order_invalid_gtin = 5769,
        //Just_Because = 5770,
    }
}
