﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EAManagerLib.ConfigurationModels
{
    public class AssociationModel
    {

        public string SkuNumber { get; set; }
        public string RFIDNumber { get; set; }
        public string Associate { get; set; }
        public bool IsError { get; set; }

    }
}
