﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EAManagerLib.ConfigurationModels
{
    public class AppsettingModel
    {
        public string KioskID { get; set; }
        public string DataSource { get; set; }
        public string DataBase { get; set; }
        public string ConnectionType { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
