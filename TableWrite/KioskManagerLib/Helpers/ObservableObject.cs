﻿// ***********************************************************************
// Assembly         : LineManager
// Author           : eAgile
// Created          : 01-28-2020
//
// Last Modified By : eAgile
// Last Modified On : 02-12-2020
// ***********************************************************************
// <copyright file="ObservableObject.cs" company="eAgile Inc">
//     eAgile Inc.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace EA.Helpers
{
    /// <summary>
    /// Class ObservableObject.
    /// Implements the <see cref="System.ComponentModel.INotifyPropertyChanged" />
    /// </summary>
    /// <seealso cref="System.ComponentModel.INotifyPropertyChanged" />
    public abstract class ObservableObject : INotifyPropertyChanged
    {
        #region SetProperty
        /// <summary>
        /// Sets the property.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="backingField">The backing field.</param>
        /// <param name="newValue">The new value.</param>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="dependentProperties">The dependent properties.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        protected virtual bool SetProperty<T>(ref T backingField, T newValue, [CallerMemberName] string propertyName = null, params string[] dependentProperties)
        {
            if (EqualityComparer<T>.Default.Equals(backingField, newValue))
            {
                return false;
            }

            backingField = newValue;

            OnPropertyChanged(propertyName, dependentProperties);

            return true;
        }
        #endregion SetProperty

        #region INotifyPropertyChanged
        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Called when [property changed].
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="propertyNames">The property names.</param>
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null, params string[] propertyNames)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            foreach (string p in propertyNames)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(p));
            }
        }
        /// <summary>
        /// Refreshes the properties.
        /// </summary>
        public virtual void RefreshProperties()
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(string.Empty));
        }
        #endregion INotifyPropertyChanged
    }

    /// <summary>
    /// Class ObservableEventArgs.
    /// Implements the <see cref="System.EventArgs" />
    /// Implements the <see cref="System.ComponentModel.INotifyPropertyChanged" />
    /// </summary>
    /// <seealso cref="System.EventArgs" />
    /// <seealso cref="System.ComponentModel.INotifyPropertyChanged" />
    public abstract class ObservableEventArgs : EventArgs, INotifyPropertyChanged
    {
        #region SetProperty
        /// <summary>
        /// Sets the property.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="backingField">The backing field.</param>
        /// <param name="newValue">The new value.</param>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="dependentProperties">The dependent properties.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        protected virtual bool SetProperty<T>(ref T backingField, T newValue, [CallerMemberName] string propertyName = null, params string[] dependentProperties)
        {
            if (EqualityComparer<T>.Default.Equals(backingField, newValue))
            {
                return false;
            }

            backingField = newValue;

            OnPropertyChanged(propertyName, dependentProperties);

            return true;
        }
        #endregion SetProperty

        #region INotifyPropertyChanged
        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Called when [property changed].
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="propertyNames">The property names.</param>
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null, params string[] propertyNames)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            foreach (string p in propertyNames)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(p));
            }
        }
        /// <summary>
        /// Refreshes the properties.
        /// </summary>
        public virtual void RefreshProperties()
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(string.Empty));
        }
        #endregion INotifyPropertyChanged
    }
}
