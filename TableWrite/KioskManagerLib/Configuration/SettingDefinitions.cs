﻿// ***********************************************************************
// Assembly         : LineManagerLib
// Author           : eAgile
// Created          : 01-28-2020
//
// Last Modified By : eAgile
// Last Modified On : 05-14-2020
// ***********************************************************************
// <copyright file="SettingDefinitions.cs" company="LineManagerLib">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

using EA.Impinj;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KioskManagerLib.Configuration
{
    /// <summary>
    /// Class SettingDefinition.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class SettingDefinition<T>
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>The type.</value>
        public SettingType Type { get; set; }
        /// <summary>
        /// Gets or sets the parse.
        /// </summary>
        /// <value>The parse.</value>
        public Func<string, T> Parse { get; set; }
        /// <summary>
        /// Gets or sets the un parse.
        /// </summary>
        /// <value>The un parse.</value>
        public Func<T, string> UnParse { get; set; }
        /// <summary>
        /// Validates the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>System.String.</returns>
        public string Validate(string value)
        {
            // Exception will be thrown if value is invalid
            return UnParse(Parse(value));
        }
        /// <summary>
        /// Gets or sets the sort by.
        /// </summary>
        /// <value>The sort by.</value>
        public int SortBy { get; set; }
    }

    /// <summary>
    /// Class SettingDefinitions.
    /// </summary>
    public static class SettingDefinitions
    {
        ///// <summary>
        ///// The log level
        ///// </summary>
        //public static SettingDefinition<LineManagerLogLevel> LOG_LEVEL = new SettingDefinition<LineManagerLogLevel>()
        //{
        //    Name = "Log Level",
        //    Type = SettingType.STRING,
        //    Parse = (val) =>
        //    {
        //        if (Enum.TryParse(val, true, out LineManagerLogLevel parsed))
        //            return parsed;
        //        else
        //            throw new ParseSettingException("Setting: \"" + LOG_LEVEL.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
        //    },
        //    UnParse = (val) =>
        //    {
        //        return val.ToString();
        //    },
        //    SortBy = 150,
        //};

        ///// <summary>
        ///// The writezone log level
        ///// </summary>
        //public static SettingDefinition<LineManagerLogLevel> WRITEZONE_LOG_LEVEL = new SettingDefinition<LineManagerLogLevel>()
        //{
        //    Name = "Write Zone Log Level",
        //    Type = SettingType.STRING,
        //    Parse = ( val ) =>
        //    {
        //        if ( Enum.TryParse( val, true, out LineManagerLogLevel parsed ) )
        //            return parsed;
        //        else
        //            throw new ParseSettingException( "Setting: \"" + WRITEZONE_LOG_LEVEL.Name + "\" has invalid value: \"" + ( val ?? "(null)" ) + "\"" );
        //    },
        //    UnParse = ( val ) =>
        //    {
        //        return val.ToString();
        //    },
        //    SortBy = 60,
        //};

        ///// <summary>
        ///// The validationzone log level
        ///// </summary>
        //public static SettingDefinition<LineManagerLogLevel> VALIDATIONZONE_LOG_LEVEL = new SettingDefinition<LineManagerLogLevel>()
        //{
        //    Name = "Validation Zone Log Level",
        //    Type = SettingType.STRING,
        //    Parse = ( val ) =>
        //    {
        //        if ( Enum.TryParse( val, true, out LineManagerLogLevel parsed ) )
        //            return parsed;
        //        else
        //            throw new ParseSettingException( "Setting: \"" + VALIDATIONZONE_LOG_LEVEL.Name + "\" has invalid value: \"" + ( val ?? "(null)" ) + "\"" );
        //    },
        //    UnParse = ( val ) =>
        //    {
        //        return val.ToString();
        //    },
        //    SortBy = 130,
        //};

        ///// <summary>
        ///// The hmi log level
        ///// </summary>
        //public static SettingDefinition<LineManagerLogLevel> HMI_LOG_LEVEL = new SettingDefinition<LineManagerLogLevel>()
        //{
        //    Name = "HMI Log Level",
        //    Type = SettingType.STRING,
        //    Parse = ( val ) =>
        //    {
        //        if ( Enum.TryParse( val, true, out LineManagerLogLevel parsed ) )
        //            return parsed;
        //        else
        //            throw new ParseSettingException( "Setting: \"" + HMI_LOG_LEVEL.Name + "\" has invalid value: \"" + ( val ?? "(null)" ) + "\"" );
        //    },
        //    UnParse = ( val ) =>
        //    {
        //        return val.ToString();
        //    },
        //    SortBy = 260,
        //};

        /// <summary>
        /// The ip address
        /// </summary>
        public static SettingDefinition<string> IP_ADDRESS = new SettingDefinition<string>()
        {
            Name = "IP Address",
            Type = SettingType.STRING,
            Parse = (val) =>
            {
                if (val?.Trim() is string parsed && !string.IsNullOrEmpty(parsed))
                    return parsed;
                else
                    throw new ParseSettingException("Setting: \"" + IP_ADDRESS.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
            },
            UnParse = (val) =>
            {
                return val?.ToString() ?? string.Empty;
            },
            SortBy = 20,
        };

        /// <summary>
        /// The writezone ip address
        /// </summary>
        public static SettingDefinition<string> WRITEZONE_IP_ADDRESS = new SettingDefinition<string>()
        {
            Name = "Write Zone IP Address",
            Type = SettingType.STRING,
            Parse = ( val ) =>
            {
                if ( val?.Trim() is string parsed && !string.IsNullOrEmpty( parsed ) )
                    return parsed;
                else
                    throw new ParseSettingException( "Setting: \"" + WRITEZONE_IP_ADDRESS.Name + "\" has invalid value: \"" + ( val ?? "(null)" ) + "\"" );
            },
            UnParse = ( val ) =>
            {
                return val?.ToString() ?? string.Empty;
            },
            SortBy = 10,
        };

        /// <summary>
        /// The validationzone ip address
        /// </summary>
        public static SettingDefinition<string> VALIDATIONZONE_IP_ADDRESS = new SettingDefinition<string>()
        {
            Name = "Validation Zone IP Address",
            Type = SettingType.STRING,
            Parse = ( val ) =>
            {
                if ( val?.Trim() is string parsed && !string.IsNullOrEmpty( parsed ) )
                    return parsed;
                else
                    throw new ParseSettingException( "Setting: \"" + VALIDATIONZONE_IP_ADDRESS.Name + "\" has invalid value: \"" + ( val ?? "(null)" ) + "\"" );
            },
            UnParse = ( val ) =>
            {
                return val?.ToString() ?? string.Empty;
            },
            SortBy = 80,
        };

        /// <summary>
        /// The port
        /// </summary>
        public static SettingDefinition<int> PORT = new SettingDefinition<int>()
        {
            Name = "Port",
            Type = SettingType.INT,
            Parse = (val) =>
            {
                if (int.TryParse(val, out int parsed) && parsed > 0)
                    return parsed;
                else
                    throw new ParseSettingException("Setting: \"" + PORT.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
            },
            UnParse = (val) =>
            {
                return val.ToString();
            },
            SortBy = 30,
        };

        /// <summary>
        /// The writezone port
        /// </summary>
        public static SettingDefinition<int> WRITEZONE_PORT = new SettingDefinition<int>()
        {
            Name = "Write Zone Port",
            Type = SettingType.INT,
            Parse = ( val ) =>
            {
                if ( int.TryParse( val, out int parsed ) && parsed > 0 )
                    return parsed;
                else
                    throw new ParseSettingException( "Setting: \"" + WRITEZONE_PORT.Name + "\" has invalid value: \"" + ( val ?? "(null)" ) + "\"" );
            },
            UnParse = ( val ) =>
            {
                return val.ToString();
            },
            SortBy = 20,
        };

        /// <summary>
        /// The validationzone port
        /// </summary>
        public static SettingDefinition<int> VALIDATIONZONE_PORT = new SettingDefinition<int>()
        {
            Name = "Validation Zone Port",
            Type = SettingType.INT,
            Parse = ( val ) =>
            {
                if ( int.TryParse( val, out int parsed ) && parsed > 0 )
                    return parsed;
                else
                    throw new ParseSettingException( "Setting: \"" + VALIDATIONZONE_PORT.Name + "\" has invalid value: \"" + ( val ?? "(null)" ) + "\"" );
            },
            UnParse = ( val ) =>
            {
                return val.ToString();
            },
            SortBy = 90,
        };

        /// <summary>
        /// The rack
        /// </summary>
        public static SettingDefinition<int> RACK = new SettingDefinition<int>()
        {
            Name = "Rack",
            Type = SettingType.INT,
            Parse = ( val ) =>
            {
                if ( int.TryParse( val, out int parsed ) && parsed >= 0 )
                    return parsed;
                else
                    throw new ParseSettingException( "Setting: \"" + RACK.Name + "\" has invalid value: \"" + ( val ?? "(null)" ) + "\"" );
            },
            UnParse = ( val ) =>
            {
                return val.ToString();
            },
            SortBy = 33,
        };

        /// <summary>
        /// The slot
        /// </summary>
        public static SettingDefinition<int> SLOT = new SettingDefinition<int>()
        {
            Name = "Slot",
            Type = SettingType.INT,
            Parse = ( val ) =>
            {
                if ( int.TryParse( val, out int parsed ) && parsed > 0 )
                    return parsed;
                else
                    throw new ParseSettingException( "Setting: \"" + SLOT.Name + "\" has invalid value: \"" + ( val ?? "(null)" ) + "\"" );
            },
            UnParse = ( val ) =>
            {
                return val.ToString();
            },
            SortBy = 36,
        };

        /// <summary>
        /// The buffer size
        /// </summary>
        public static SettingDefinition<int> BUFFER_SIZE = new SettingDefinition<int>()
        {
            Name = "Buffer Size",
            Type = SettingType.INT,
            Parse = ( val ) =>
            {
                if ( int.TryParse( val, out int parsed ) && parsed > 0 )
                    return parsed;
                else
                    throw new ParseSettingException( "Setting: \"" + BUFFER_SIZE.Name + "\" has invalid value: \"" + ( val ?? "(null)" ) + "\"" );
            },
            UnParse = ( val ) =>
            {
                return val.ToString();
            },
            SortBy = 39
        };

        /// <summary>
        /// The connect timeout
        /// </summary>
        public static SettingDefinition<int> CONNECT_TIMEOUT = new SettingDefinition<int>()
        {
            Name = "Connect Timeout",
            Type = SettingType.INT,
            Parse = (val) =>
            {
                if (int.TryParse(val, out int parsed) && parsed >= -1)
                    return parsed;
                else
                    throw new ParseSettingException("Setting: \"" + CONNECT_TIMEOUT.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
            },
            UnParse = (val) =>
            {
                return val.ToString();
            },
            SortBy = 50,
        };

        /// <summary>
        /// The writezone connect timeout
        /// </summary>
        public static SettingDefinition<int> WRITEZONE_CONNECT_TIMEOUT = new SettingDefinition<int>()
        {
            Name = "Write Zone Connect Timeout",
            Type = SettingType.INT,
            Parse = ( val ) =>
            {
                if ( int.TryParse( val, out int parsed ) && parsed >= -1 )
                    return parsed;
                else
                    throw new ParseSettingException( "Setting: \"" + WRITEZONE_CONNECT_TIMEOUT.Name + "\" has invalid value: \"" + ( val ?? "(null)" ) + "\"" );
            },
            UnParse = ( val ) =>
            {
                return val.ToString();
            },
            SortBy = 40,
        };

        /// <summary>
        /// The validationzone connect timeout
        /// </summary>
        public static SettingDefinition<int> VALIDATIONZONE_CONNECT_TIMEOUT = new SettingDefinition<int>()
        {
            Name = "Validation Zone Connect Timeout",
            Type = SettingType.INT,
            Parse = ( val ) =>
            {
                if ( int.TryParse( val, out int parsed ) && parsed >= -1 )
                    return parsed;
                else
                    throw new ParseSettingException( "Setting: \"" + VALIDATIONZONE_CONNECT_TIMEOUT.Name + "\" has invalid value: \"" + ( val ?? "(null)" ) + "\"" );
            },
            UnParse = ( val ) =>
            {
                return val.ToString();
            },
            SortBy = 110,
        };

        /// <summary>
        /// The command timeout
        /// </summary>
        public static SettingDefinition<int> COMMAND_TIMEOUT = new SettingDefinition<int>()
        {
            Name = "Command Timeout",
            Type = SettingType.INT,
            Parse = (val) =>
            {
                if (int.TryParse(val, out int parsed) && parsed >= -1)
                    return parsed;
                else
                    throw new ParseSettingException("Setting: \"" + COMMAND_TIMEOUT.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
            },
            UnParse = (val) =>
            {
                return val.ToString();
            },
            SortBy = 60,
        };

        /// <summary>
        /// The writezone command timeout
        /// </summary>
        public static SettingDefinition<int> WRITEZONE_COMMAND_TIMEOUT = new SettingDefinition<int>()
        {
            Name = "Write Zone Command Timeout",
            Type = SettingType.INT,
            Parse = ( val ) =>
            {
                if ( int.TryParse( val, out int parsed ) && parsed >= -1 )
                    return parsed;
                else
                    throw new ParseSettingException( "Setting: \"" + WRITEZONE_COMMAND_TIMEOUT.Name + "\" has invalid value: \"" + ( val ?? "(null)" ) + "\"" );
            },
            UnParse = ( val ) =>
            {
                return val.ToString();
            },
            SortBy = 50,
        };

        /// <summary>
        /// The validationzone command timeout
        /// </summary>
        public static SettingDefinition<int> VALIDATIONZONE_COMMAND_TIMEOUT = new SettingDefinition<int>()
        {
            Name = "Validation Zone Command Timeout",
            Type = SettingType.INT,
            Parse = ( val ) =>
            {
                if ( int.TryParse( val, out int parsed ) && parsed >= -1 )
                    return parsed;
                else
                    throw new ParseSettingException( "Setting: \"" + VALIDATIONZONE_COMMAND_TIMEOUT.Name + "\" has invalid value: \"" + ( val ?? "(null)" ) + "\"" );
            },
            UnParse = ( val ) =>
            {
                return val.ToString();
            },
            SortBy = 120,
        };

        /// <summary>
        /// The heartbeat interval
        /// </summary>
        public static SettingDefinition<int> HEARTBEAT_INTERVAL = new SettingDefinition<int>()
        {
            Name = "Heartbeat",
            Type = SettingType.INT,
            Parse = (val) =>
            {
                if (int.TryParse(val, out int parsed) && parsed >= 0)
                    return parsed;
                else
                    throw new ParseSettingException("Setting: \"" + HEARTBEAT_INTERVAL.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
            },
            UnParse = (val) =>
            {
                return val.ToString();
            },
            SortBy = 40,
        };

        /// <summary>
        /// The heartbeat timeout
        /// </summary>
        public static SettingDefinition<int> HEARTBEAT_TIMEOUT = new SettingDefinition<int>()
        {
            Name = "Heartbeat Timeout",
            Type = SettingType.INT,
            Parse = ( val ) =>
            {
                if ( int.TryParse( val, out int parsed ) && parsed >= 0 )
                    return parsed;
                else
                    throw new ParseSettingException( "Setting: \"" + HEARTBEAT_TIMEOUT.Name + "\" has invalid value: \"" + ( val ?? "(null)" ) + "\"" );
            },
            UnParse = ( val ) =>
            {
                return val.ToString();
            },
            SortBy = 45,
        };

        /// <summary>
        /// The writezone heartbeat interval
        /// </summary>
        public static SettingDefinition<int> WRITEZONE_HEARTBEAT_INTERVAL = new SettingDefinition<int>()
        {
            Name = "Write Zone Heartbeat",
            Type = SettingType.INT,
            Parse = ( val ) =>
            {
                if ( int.TryParse( val, out int parsed ) && parsed >= 0 )
                    return parsed;
                else
                    throw new ParseSettingException( "Setting: \"" + WRITEZONE_HEARTBEAT_INTERVAL.Name + "\" has invalid value: \"" + ( val ?? "(null)" ) + "\"" );
            },
            UnParse = ( val ) =>
            {
                return val.ToString();
            },
            SortBy = 30,
        };

        /// <summary>
        /// The validationzone heartbeat interval
        /// </summary>
        public static SettingDefinition<int> VALIDATIONZONE_HEARTBEAT_INTERVAL = new SettingDefinition<int>()
        {
            Name = "Validation Zone Heartbeat",
            Type = SettingType.INT,
            Parse = ( val ) =>
            {
                if ( int.TryParse( val, out int parsed ) && parsed >= 0 )
                    return parsed;
                else
                    throw new ParseSettingException( "Setting: \"" + VALIDATIONZONE_HEARTBEAT_INTERVAL.Name + "\" has invalid value: \"" + ( val ?? "(null)" ) + "\"" );
            },
            UnParse = ( val ) =>
            {
                return val.ToString();
            },
            SortBy = 100,
        };

        /// <summary>
        /// The order monitor interval
        /// </summary>
        public static SettingDefinition<int> ORDER_MONITOR_INTERVAL = new SettingDefinition<int>()
        {
            Name = "Order Monitor",
            Type = SettingType.INT,
            Parse = (val) =>
            {
                if (int.TryParse(val, out int parsed) && parsed >= 0)
                    return parsed;
                else
                    throw new ParseSettingException("Setting: \"" + ORDER_MONITOR_INTERVAL.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
            },
            UnParse = (val) =>
            {
                return val.ToString();
            },
            SortBy = 220,
        };

        ///// <summary>
        ///// The gtin
        ///// </summary>
        //public static SettingDefinition<string> GTIN = new SettingDefinition<string>()
        //{
        //    Name = "GTIN",
        //    Type = SettingType.STRING,
        //    Parse = (val) =>
        //    {
        //        if (val?.Trim() is string parsed && (parsed.Length == 0 || RLMOrder.IsValidGtin(parsed)))
        //        {
        //            return parsed;
        //        }
        //        else
        //            throw new ParseSettingException("Setting: \"" + GTIN.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
        //    },
        //    UnParse = (val) =>
        //    {
        //        return val?.ToString() ?? string.Empty;
        //    },
        //    SortBy = 270,
        //};

        public static SettingDefinition<string> SELECTED_PRODUCT = new SettingDefinition<string>()
        {
            Name = "Selected Product",
            Type = SettingType.STRING,
            Parse = (val) =>
            {
                if (val?.Trim() is string parsed && parsed.Length > 0)
                {
                    return parsed;
                }
                else
                    return SettingsParser.DefaultProductName;
            },
            UnParse = (val) =>
            {
                if (SettingsParser.DefaultProductName == val)
                    return string.Empty;
                else
                    return val?.ToString() ?? string.Empty;
            },
            SortBy = 270,
        };

        ///// <summary>
        ///// The hierarchy level
        ///// </summary>
        //public static SettingDefinition<int?> HIERARCHY_LEVEL = new SettingDefinition<int?>()
        //{
        //    Name = "Hierarchy Level",
        //    Type = SettingType.INT,
        //    Parse = (val) =>
        //    {
        //        if (int.TryParse(val?.Trim(), out int parsed) && RLMOrder.IsValidHierarchyLevel(parsed))
        //            return parsed;
        //        else if (val?.Trim() is string parsedEmpty && parsedEmpty.Length == 0)
        //            return null;
        //        else
        //            throw new ParseSettingException("Setting: \"" + HIERARCHY_LEVEL.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
        //    },
        //    UnParse = (val) =>
        //    {
        //        return val?.ToString() ?? string.Empty;
        //    },
        //    SortBy = 271,
        //};

        //public static SettingDefinition<string> EPC_HEADER = new SettingDefinition<string>()
        //{
        //    Name = "EPC Header",
        //    Type = SettingType.STRING,
        //    Parse = (val) =>
        //    {
        //        if (val?.Trim() is string parsed && parsed != null && IsHex(parsed))
        //            return parsed;
        //        else
        //            throw new ParseSettingException("Setting: \"" + EPC_HEADER.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
        //    },
        //    UnParse = (val) =>
        //    {
        //        return val?.ToString() ?? string.Empty;
        //    },
        //    SortBy = 280,
        //};

        //public static SettingDefinition<int> EPC_SERIAL_START = new SettingDefinition<int>()
        //{
        //    Name = "EPC Serial Start",
        //    Type = SettingType.INT,
        //    Parse = (val) =>
        //    {
        //        if (int.TryParse(val, out int parsed) && parsed >= 0)
        //            return parsed;
        //        else
        //            throw new ParseSettingException("Setting: \"" + EPC_SERIAL_START.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
        //    },
        //    UnParse = (val) =>
        //    {
        //        return val.ToString();
        //    },
        //    SortBy = 200,
        //};

        //public static SettingDefinition<int> EPC_SERIAL_LENGTH = new SettingDefinition<int>()
        //{
        //    Name = "EPC Serial Length",
        //    Type = SettingType.INT,
        //    Parse = (val) =>
        //    {
        //        if (int.TryParse(val, out int parsed) && parsed >= 0)
        //            return parsed;
        //        else
        //            throw new ParseSettingException("Setting: \"" + EPC_SERIAL_LENGTH.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
        //    },
        //    UnParse = (val) =>
        //    {
        //        return val.ToString();
        //    },
        //    SortBy = 210,
        //};

        /// <summary>
        /// The serial minimum
        /// </summary>
        public static SettingDefinition<long> SERIAL_MIN = new SettingDefinition<long>()
        {
            Name = "Serial Minimum",
            Type = SettingType.INT,
            Parse = (val) =>
            {
                if (long.TryParse(val, out long parsed) && parsed > 0)
                    return parsed;
                else
                    throw new ParseSettingException("Setting: \"" + SERIAL_MIN.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
            },
            UnParse = (val) =>
            {
                return val.ToString();
            },
            SortBy = 211,
        };

        /// <summary>
        /// The serial maximum
        /// </summary>
        public static SettingDefinition<long> SERIAL_MAX = new SettingDefinition<long>()
        {
            Name = "Serial Maximum",
            Type = SettingType.INT,
            Parse = (val) =>
            {
                if (long.TryParse(val, out long parsed) && parsed > 0)
                    return parsed;
                else
                    throw new ParseSettingException("Setting: \"" + SERIAL_MAX.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
            },
            UnParse = (val) =>
            {
                return val.ToString();
            },
            SortBy = 212,
        };

        ///// <summary>
        ///// The user lock
        ///// </summary>
        //public static SettingDefinition<LockState> USER_LOCK = new SettingDefinition<LockState>()
        //{
        //    Name = "User Lock",
        //    Type = SettingType.INT,
        //    Parse = (val) =>
        //    {
        //        if (Enum.TryParse(val, true, out LockState parsed))
        //            return parsed;
        //        else
        //            throw new ParseSettingException("Setting: \"" + USER_LOCK.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
        //    },
        //    UnParse = (val) =>
        //    {
        //        return ((int)val).ToString();
        //    },
        //    SortBy = 190,
        //};

        /// <summary>
        /// The language
        /// </summary>
        public static SettingDefinition<string> LANGUAGE = new SettingDefinition<string>()
        {
            Name = "Language",
            Type = SettingType.STRING,
            Parse = (val) =>
            {
                if (val?.Trim()?.StartsWith("en", StringComparison.OrdinalIgnoreCase) ?? false)
                    return "en-US";
                else if (val?.Trim()?.StartsWith("sv", StringComparison.OrdinalIgnoreCase) ?? false)
                    return "sv-SE";
                else
                    throw new ParseSettingException("Setting: \"" + LANGUAGE.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
            },
            UnParse = (val) =>
            {
                if (val?.Trim()?.StartsWith("sv", StringComparison.OrdinalIgnoreCase) ?? false)
                    return "sv-SE";
                else
                    return "en-US";
            },
            SortBy = 180,
        };

        /// <summary>
        /// The track setting change reason
        /// </summary>
        public static SettingDefinition<bool> TRACK_SETTING_CHANGE_REASON = new SettingDefinition<bool>()
        {
            Name = "Track Update Reason",
            Type = SettingType.BIT,
            Parse = (val) =>
            {
                if (int.TryParse(val, out int parsed))
                    return parsed > 0;
                else if (true.ToString().Equals(val, StringComparison.OrdinalIgnoreCase))
                    return true;
                else if (false.ToString().Equals(val, StringComparison.OrdinalIgnoreCase))
                    return false;
                else
                    throw new ParseSettingException("Setting: \"" + TRACK_SETTING_CHANGE_REASON.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
            },
            UnParse = (val) =>
            {
                return val ? "1" : "0";
            },
            SortBy = 170,
        };

        public static SettingDefinition<string> SERVER_LOOKUP = new SettingDefinition<string>()
        {
            Name = "LookUp Server",
            Type = SettingType.STRING,
            Parse = (val) =>
            {
                if (val?.Trim() is string parsed && !string.IsNullOrEmpty(parsed))
                    return parsed;
                else
                    throw new ParseSettingException("Setting: \"" + SERVER_LOOKUP.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
            },
            UnParse = (val) =>
            {
                return val?.ToString() ?? string.Empty;
            },
            SortBy = 170,
        };

        public static SettingDefinition<string> DATABASE_LOOKUP = new SettingDefinition<string>()
        {
            Name = "LookUp Database",
            Type = SettingType.STRING,
            Parse = (val) =>
            {
                if (val?.Trim() is string parsed && !string.IsNullOrEmpty(parsed))
                    return parsed;
                else
                    throw new ParseSettingException("Setting: \"" + DATABASE_LOOKUP.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
            },
            UnParse = (val) =>
            {
                return val?.ToString() ?? string.Empty;
            },
            SortBy = 170,
        };
        public static SettingDefinition<string> USERNAME_LOOKUP = new SettingDefinition<string>()
        {
            Name = "LookUp UserName",
            Type = SettingType.STRING,
            Parse = (val) =>
            {
                if (val?.Trim() is string parsed && !string.IsNullOrEmpty(parsed))
                    return parsed;
                else
                    throw new ParseSettingException("Setting: \"" + USERNAME_LOOKUP.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
            },
            UnParse = (val) =>
            {
                return val?.ToString() ?? string.Empty;
            },
            SortBy = 170,
        };
        public static SettingDefinition<string> PASSWORD_LOOKUP = new SettingDefinition<string>()
        {
            Name = "LookUp Password",
            Type = SettingType.STRING,
            Parse = (val) =>
            {
                if (val?.Trim() is string parsed && !string.IsNullOrEmpty(parsed))
                    return parsed;
                else
                    throw new ParseSettingException("Setting: \"" + PASSWORD_LOOKUP.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
            },
            UnParse = (val) =>
            {
                return val?.ToString() ?? string.Empty;
            },
            SortBy = 170,
        };

        

        public static SettingDefinition<bool> ALLOW_LOOKUP = new SettingDefinition<bool>()
        {
            Name = "Allow Look up",
            Type = SettingType.BIT,
            Parse = (val) =>
            {
                if (int.TryParse(val, out int parsed))
                    return parsed > 0;
                else if (true.ToString().Equals(val, StringComparison.OrdinalIgnoreCase))
                    return true;
                else if (false.ToString().Equals(val, StringComparison.OrdinalIgnoreCase))
                    return false;
                else
                    throw new ParseSettingException("Setting: \"" + ALLOW_LOOKUP.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
            },
            UnParse = (val) =>
            {
                return val ? "1" : "0";
            },
            SortBy = 170,
        };

        /// <summary>
        /// The upload batch size
        /// </summary>
        public static SettingDefinition<int> UPLOAD_BATCH_SIZE = new SettingDefinition<int>()
        {
            Name = "Upload Batch Size",
            Type = SettingType.INT,
            Parse = (val) =>
            {
                if (int.TryParse(val, out int parsed) && parsed > 0)
                    return parsed;
                else
                    throw new ParseSettingException("Setting: \"" + UPLOAD_BATCH_SIZE.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
            },
            UnParse = (val) =>
            {
                return val.ToString();
            },
            SortBy = 160,
        };

        /// <summary>
        /// The enabled
        /// </summary>
        public static SettingDefinition<bool> ENABLED = new SettingDefinition<bool>()
        {
            Name = "Enabled",
            Type = SettingType.BIT,
            Parse = (val) =>
            {
                if (int.TryParse(val, out int parsed))
                    return parsed > 0;
                else if (true.ToString().Equals(val, StringComparison.OrdinalIgnoreCase))
                    return true;
                else if (false.ToString().Equals(val, StringComparison.OrdinalIgnoreCase))
                    return false;
                else
                    throw new ParseSettingException("Setting: \"" + ENABLED.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
            },
            UnParse = (val) =>
            {
                return val ? "1" : "0";
            },
            SortBy = 10,
        };

        public static SettingDefinition<bool> LOCK_CHECK = new SettingDefinition<bool>()
        {
            Name = "Lock Check",
            Type = SettingType.BIT,
            Parse = (val) =>
            {
                if (int.TryParse(val, out int parsed))
                    return parsed > 0;
                else if (true.ToString().Equals(val, StringComparison.OrdinalIgnoreCase))
                    return true;
                else if (false.ToString().Equals(val, StringComparison.OrdinalIgnoreCase))
                    return false;
                else
                    throw new ParseSettingException("Setting: \"" + LOCK_CHECK.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
            },
            UnParse = (val) =>
            {
                return val ? "1" : "0";
            },
            SortBy = 20,
        };

        public static SettingDefinition<bool> USER_CHECK = new SettingDefinition<bool>()
        {
            Name = "User Check",
            Type = SettingType.BIT,
            Parse = (val) =>
            {
                if (int.TryParse(val, out int parsed))
                    return parsed > 0;
                else if (true.ToString().Equals(val, StringComparison.OrdinalIgnoreCase))
                    return true;
                else if (false.ToString().Equals(val, StringComparison.OrdinalIgnoreCase))
                    return false;
                else
                    throw new ParseSettingException("Setting: \"" + USER_CHECK.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
            },
            UnParse = (val) =>
            {
                return val ? "1" : "0";
            },
            SortBy = 30,
        };

        /// <summary>
        /// The writezone enabled
        /// </summary>
        public static SettingDefinition<bool> WRITEZONE_ENABLED = new SettingDefinition<bool>()
        {
            Name = "Write Zone Enabled",
            Type = SettingType.BIT,
            Parse = ( val ) =>
            {
                if ( int.TryParse( val, out int parsed ) )
                    return parsed > 0;
                else if ( true.ToString().Equals( val, StringComparison.OrdinalIgnoreCase ) )
                    return true;
                else if ( false.ToString().Equals( val, StringComparison.OrdinalIgnoreCase ) )
                    return false;
                else
                    throw new ParseSettingException( "Setting: \"" + WRITEZONE_ENABLED.Name + "\" has invalid value: \"" + ( val ?? "(null)" ) + "\"" );
            },
            UnParse = ( val ) =>
            {
                return val ? "1" : "0";
            },
            SortBy = 10,
        };

        /// <summary>
        /// The validationzone enabled
        /// </summary>
        public static SettingDefinition<bool> VALIDATIONZONE_ENABLED = new SettingDefinition<bool>()
        {
            Name = "Validation Zone Enabled",
            Type = SettingType.BIT,
            Parse = ( val ) =>
            {
                if ( int.TryParse( val, out int parsed ) )
                    return parsed > 0;
                else if ( true.ToString().Equals( val, StringComparison.OrdinalIgnoreCase ) )
                    return true;
                else if ( false.ToString().Equals( val, StringComparison.OrdinalIgnoreCase ) )
                    return false;
                else
                    throw new ParseSettingException( "Setting: \"" + VALIDATIONZONE_ENABLED.Name + "\" has invalid value: \"" + ( val ?? "(null)" ) + "\"" );
            },
            UnParse = ( val ) =>
            {
                return val ? "1" : "0";
            },
            SortBy = 75,
        };

        /// <summary>
        /// The ready input number
        /// </summary>
        public static SettingDefinition<int> READY_INPUT_NUMBER = new SettingDefinition<int>()
        {
            Name = "Ready Input Terminal",
            Type = SettingType.INT,
            Parse = (val) =>
            {
                if (int.TryParse(val, out int parsed) && parsed >= 0)
                    return parsed;
                else
                    throw new ParseSettingException("Setting: \"" + READY_INPUT_NUMBER.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
            },
            UnParse = (val) =>
            {
                return val.ToString();
            },
            SortBy = 50,
        };

        /// <summary>
        /// The ready output number
        /// </summary>
        public static SettingDefinition<int> READY_OUTPUT_NUMBER = new SettingDefinition<int>()
        {
            Name = "Ready Output Terminal",
            Type = SettingType.INT,
            Parse = (val) =>
            {
                if (int.TryParse(val, out int parsed) && parsed >= 0)
                    return parsed;
                else
                    throw new ParseSettingException("Setting: \"" + READY_OUTPUT_NUMBER.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
            },
            UnParse = (val) =>
            {
                return val.ToString();
            },
            SortBy = 60,
        };

        /// <summary>
        /// The status polling interval
        /// </summary>
        public static SettingDefinition<int> STATUS_POLLING_INTERVAL = new SettingDefinition<int>()
        {
            Name = "Status Polling Interval",
            Type = SettingType.INT,
            Parse = (val) =>
            {
                if (int.TryParse(val, out int parsed) && parsed >= 0)
                    return parsed;
                else
                    throw new ParseSettingException("Setting: \"" + STATUS_POLLING_INTERVAL.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
            },
            UnParse = (val) =>
            {
                return val.ToString();
            },
            SortBy = 100,
        };

        /// <summary>
        /// The writezone PLC polling interval
        /// </summary>
        public static SettingDefinition<int> WRITEZONE_PLC_POLLING_INTERVAL = new SettingDefinition<int>()
        {
            Name = "Write Zone PLC Polling Interval",
            Type = SettingType.INT,
            Parse = ( val ) =>
            {
                if ( int.TryParse( val, out int parsed ) && parsed >= 0 )
                    return parsed;
                else
                    throw new ParseSettingException( "Setting: \"" + WRITEZONE_PLC_POLLING_INTERVAL.Name + "\" has invalid value: \"" + ( val ?? "(null)" ) + "\"" );
            },
            UnParse = ( val ) =>
            {
                return val.ToString();
            },
            SortBy = 70,
        };

        /// <summary>
        /// The validationzone PLC polling interval
        /// </summary>
        public static SettingDefinition<int> VALIDATIONZONE_PLC_POLLING_INTERVAL = new SettingDefinition<int>()
        {
            Name = "Validation Zone PLC Polling Interval",
            Type = SettingType.INT,
            Parse = ( val ) =>
            {
                if ( int.TryParse( val, out int parsed ) && parsed >= 0 )
                    return parsed;
                else
                    throw new ParseSettingException( "Setting: \"" + VALIDATIONZONE_PLC_POLLING_INTERVAL.Name + "\" has invalid value: \"" + ( val ?? "(null)" ) + "\"" );
            },
            UnParse = ( val ) =>
            {
                return val.ToString();
            },
            SortBy = 140,
        };

        /// <summary>
        /// The validationzone PLC polling interval
        /// </summary>
        public static SettingDefinition<int> SCAN_DURATION = new SettingDefinition<int>()
        {
            Name = "Scan Duration",
            Type = SettingType.INT,
            Parse = (val) =>
            {
                if (int.TryParse(val, out int parsed) && parsed >= 0)
                    return parsed;
                else
                    throw new ParseSettingException("Setting: \"" + SCAN_DURATION.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
            },
            UnParse = (val) =>
            {
                return val.ToString();
            },
            SortBy = 140,
        };

        /// <summary>
        /// The reader mode
        /// </summary>
        public static SettingDefinition<ReaderMode> READER_MODE = new SettingDefinition<ReaderMode>()
        {
            Name = "Reader Mode",
            Type = SettingType.INT,
            Parse = (val) =>
            {
                if (Enum.TryParse(val, true, out ReaderMode parsed))
                    return parsed;
                else
                    throw new ParseSettingException("Setting: \"" + READER_MODE.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
            },
            UnParse = (val) =>
            {
                return ((int)val).ToString();
            },
            SortBy = 100,
        };

        /// <summary>
        /// The search mode
        /// </summary>
        public static SettingDefinition<SearchMode> SEARCH_MODE = new SettingDefinition<SearchMode>()
        {
            Name = "Search Mode",
            Type = SettingType.INT,
            Parse = (val) =>
            {
                if (Enum.TryParse(val, true, out SearchMode parsed))
                    return parsed;
                else
                    throw new ParseSettingException("Setting: \"" + SEARCH_MODE.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
            },
            UnParse = (val) =>
            {
                return ((int)val).ToString();
            },
            SortBy = 110,
        };

        /// <summary>
        /// The session
        /// </summary>
        public static SettingDefinition<Session> SESSION = new SettingDefinition<Session>()
        {
            Name = "Session",
            Type = SettingType.INT,
            Parse = (val) =>
            {
                if (Enum.TryParse(val, true, out Session parsed))
                    return parsed;
                else
                    throw new ParseSettingException("Setting: \"" + SESSION.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
            },
            UnParse = (val) =>
            {
                return ((int)val).ToString();
            },
            SortBy = 120,
        };

        /// <summary>
        /// The tag population
        /// </summary>
        public static SettingDefinition<int> TAG_POPULATION = new SettingDefinition<int>()
        {
            Name = "Tag Population",
            Type = SettingType.INT,
            Parse = (val) =>
            {
                if (int.TryParse(val, out int parsed) && parsed >= 0)
                    return parsed;
                else
                    throw new ParseSettingException("Setting: \"" + TAG_POPULATION.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
            },
            UnParse = (val) =>
            {
                return val.ToString();
            },
            SortBy = 130,
        };

        /// <summary>
        /// The use etsi
        /// </summary>
        public static SettingDefinition<bool> USE_ETSI = new SettingDefinition<bool>()
        {
            Name = "ETSI",
            Type = SettingType.BIT,
            Parse = (val) =>
            {
                if (int.TryParse(val, out int parsed))
                    return parsed > 0;
                else if (true.ToString().Equals(val, StringComparison.OrdinalIgnoreCase))
                    return true;
                else if (false.ToString().Equals(val, StringComparison.OrdinalIgnoreCase))
                    return false;
                else
                    throw new ParseSettingException("Setting: \"" + USE_ETSI.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
            },
            UnParse = (val) =>
            {
                return val ? "1" : "0";
            },
            SortBy = 70,
        };

        /// <summary>
        /// The etsi frequencies
        /// </summary>
        public static SettingDefinition<List<double>> ETSI_FREQUENCIES = new SettingDefinition<List<double>>()
        {
            Name = "Frequencies",
            Type = SettingType.STRING,
            Parse = (val) =>
            {
                List<double> validFrequencies = new List<double>() { 865.7, 866.3, 866.9, 867.5 };
                List<double> frequencies = new List<double>();
                if (val != null)
                {
                    foreach (string s in val.Split(',').Select(p => p.Trim()).Where(p => p.Length > 0))
                    {
                        double.TryParse(s, out double parsed);
                        if (validFrequencies.Contains(parsed) && validFrequencies.Remove(parsed))
                            frequencies.Add(parsed);
                        else
                            throw new ParseSettingException("Setting: \"" + ETSI_FREQUENCIES.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
                    }
                }
                return frequencies;
            },
            UnParse = (val) =>
            {
                return val != null ? string.Join(",", val) : string.Empty;
            },
            SortBy = 80,
        };

        /// <summary>
        /// The antennas
        /// </summary>
        public static SettingDefinition<List<int>> ANTENNAS = new SettingDefinition<List<int>>()
        {
            Name = "Antenna Mux",
            Type = SettingType.STRING,
            Parse = (val) =>
            {
                List<int> validAntennas = new List<int>() { 1, 2, 3, 4 };
                List<int> antennas = new List<int>();
                if (val != null)
                {
                    foreach (string s in val.Split(',').Select(p => p.Trim()).Where(p => p.Length > 0))
                    {
                        int.TryParse(s, out int parsed);
                        if (validAntennas.Contains(parsed) && validAntennas.Remove(parsed))
                            antennas.Add(parsed);
                        else
                            throw new ParseSettingException("Setting: \"" + ANTENNAS.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
                    }
                }
                if (antennas.Count > 0)
                    return antennas;
                else
                    throw new ParseSettingException("Setting: \"" + ANTENNAS.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
            },
            UnParse = (val) =>
            {
                return val != null ? string.Join(",", val) : string.Empty;
            },
            SortBy = 140,
        };

        /// <summary>
        /// The tx power
        /// </summary>
        public static SettingDefinition<double> TX_POWER = new SettingDefinition<double>()
        {
            Name = "Antenna Power",
            Type = SettingType.FLOAT,
            Parse = (val) =>
            {
                if (double.TryParse(val, out double parsed)
                        && parsed >= 10.0
                        && parsed <= 32.5
                        && (parsed % 0.25) == 0)
                    return parsed;
                else
                    throw new ParseSettingException("Setting: \"" + TX_POWER.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
            },
            UnParse = (val) =>
            {
                return val.ToString();
            },
            SortBy = 150,
        };

        /// <summary>
        /// The rx sensitivity
        /// </summary>
        public static SettingDefinition<double> RX_SENSITIVITY = new SettingDefinition<double>()
        {
            Name = "RX Sensitivity",
            Type = SettingType.DOUBLE,
            Parse = (val) =>
            {
                if (double.TryParse(val, out double parsed)
                    && parsed >= -80
                    && parsed <= -20
                    && (parsed % 10) == 0)
                    return parsed;
                else
                    throw new ParseSettingException("Setting: \"" + RX_SENSITIVITY.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
            },
            UnParse = (val) =>
            {
                return val.ToString();
            },
            SortBy = 160,
        };

        /// <summary>
        /// The tag queue size
        /// </summary>
        public static SettingDefinition<int> TAG_QUEUE_SIZE = new SettingDefinition<int>()
        {
            Name = "TID Queue Size",
            Type = SettingType.INT,
            Parse = (val) =>
            {
                if (int.TryParse(val, out int parsed) && parsed > 0)
                    return parsed;
                else
                    throw new ParseSettingException("Setting: \"" + TAG_QUEUE_SIZE.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
            },
            UnParse = (val) =>
            {
                return val.ToString();
            },
            SortBy = 90,
        };

        /// <summary>
        /// The tag max read interval
        /// </summary>
        public static SettingDefinition<int> TAG_MAX_READ_INTERVAL = new SettingDefinition<int>()
        {
            Name = "TID maximum read interval",
            Type = SettingType.INT,
            Parse = (val) =>
            {
                if (int.TryParse(val, out int parsed) && parsed > 0)
                    return parsed;
                else
                    throw new ParseSettingException("Setting: \"" + TAG_MAX_READ_INTERVAL.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
            },
            UnParse = (val) =>
            {
                return val.ToString();
            },
            SortBy = 91,
        };

        /// <summary>
        /// The use block write
        /// </summary>
        public static SettingDefinition<bool> USE_BLOCK_WRITE = new SettingDefinition<bool>()
        {
            Name = "Block Write",
            Type = SettingType.BIT,
            Parse = (val) =>
            {
                if (int.TryParse(val, out int parsed))
                    return parsed > 0;
                else if (true.ToString().Equals(val, StringComparison.OrdinalIgnoreCase))
                    return true;
                else if (false.ToString().Equals(val, StringComparison.OrdinalIgnoreCase))
                    return false;
                else
                    throw new ParseSettingException("Setting: \"" + USE_BLOCK_WRITE.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
            },
            UnParse = (val) =>
            {
                return val ? "1" : "0";
            },
            SortBy = 180,
        };

        /// <summary>
        /// The block write count
        /// </summary>
        public static SettingDefinition<int> BLOCK_WRITE_COUNT = new SettingDefinition<int>()
        {
            Name = "Block Word Count",
            Type = SettingType.INT,
            Parse = (val) =>
            {
                if (int.TryParse(val, out int parsed) && parsed > 0)
                    return parsed;
                else
                    throw new ParseSettingException("Setting: \"" + BLOCK_WRITE_COUNT.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
            },
            UnParse = (val) =>
            {
                return val.ToString();
            },
            SortBy = 190,
        };

        /// <summary>
        /// The retry count
        /// </summary>
        public static SettingDefinition<int> RETRY_COUNT = new SettingDefinition<int>()
        {
            Name = "Retry Count",
            Type = SettingType.INT,
            Parse = (val) =>
            {
                if (int.TryParse(val, out int parsed) && parsed >= 0)
                    return parsed;
                else
                    throw new ParseSettingException("Setting: \"" + RETRY_COUNT.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
            },
            UnParse = (val) =>
            {
                return val.ToString();
            },
            SortBy = 170,
        };

        /// <summary>
        /// The pre trigger
        /// </summary>
        public static SettingDefinition<int> PRE_TRIGGER = new SettingDefinition<int>()
        {
            Name = "Pre Trigger",
            Type = SettingType.INT,
            Parse = (val) =>
            {
                if (int.TryParse(val, out int parsed))
                    return parsed;
                else
                    throw new ParseSettingException("Setting: \"" + PRE_TRIGGER.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
            },
            UnParse = (val) =>
            {
                return val.ToString();
            },
            SortBy = 220,
        };

        /// <summary>
        /// The trigger input number
        /// </summary>
        public static SettingDefinition<int> TRIGGER_INPUT_NUMBER = new SettingDefinition<int>()
        {
            Name = "Trigger Input Terminal",
            Type = SettingType.INT,
            Parse = (val) =>
            {
                if (int.TryParse(val, out int parsed))
                    return parsed;
                else
                    throw new ParseSettingException("Setting: \"" + TRIGGER_INPUT_NUMBER.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
            },
            UnParse = (val) =>
            {
                return val.ToString();
            },
            SortBy = 170,
        };

        /// <summary>
        /// The trigger i d1 input number
        /// </summary>
        public static SettingDefinition<int> TRIGGER_ID1_INPUT_NUMBER = new SettingDefinition<int>()
        {
            Name = "Trigger ID1 Input Terminal",
            Type = SettingType.INT,
            Parse = ( val ) =>
            {
                if ( int.TryParse( val, out int parsed ) )
                    return parsed;
                else
                    throw new ParseSettingException( "Setting: \"" + TRIGGER_ID1_INPUT_NUMBER.Name + "\" has invalid value: \"" + ( val ?? "(null)" ) + "\"" );
            },
            UnParse = ( val ) =>
            {
                return val.ToString();
            },
            SortBy = 180,
        };

        /// <summary>
        /// The trigger i d2 input number
        /// </summary>
        public static SettingDefinition<int> TRIGGER_ID2_INPUT_NUMBER = new SettingDefinition<int>()
        {
            Name = "Trigger ID2 Input Terminal",
            Type = SettingType.INT,
            Parse = ( val ) =>
            {
                if ( int.TryParse( val, out int parsed ) )
                    return parsed;
                else
                    throw new ParseSettingException( "Setting: \"" + TRIGGER_ID2_INPUT_NUMBER.Name + "\" has invalid value: \"" + ( val ?? "(null)" ) + "\"" );
            },
            UnParse = ( val ) =>
            {
                return val.ToString();
            },
            SortBy = 190,
        };

        /// <summary>
        /// The trigger i d3 input number
        /// </summary>
        public static SettingDefinition<int> TRIGGER_ID3_INPUT_NUMBER = new SettingDefinition<int>()
        {
            Name = "Trigger ID3 Input Terminal",
            Type = SettingType.INT,
            Parse = ( val ) =>
            {
                if ( int.TryParse( val, out int parsed ) )
                    return parsed;
                else
                    throw new ParseSettingException( "Setting: \"" + TRIGGER_ID3_INPUT_NUMBER.Name + "\" has invalid value: \"" + ( val ?? "(null)" ) + "\"" );
            },
            UnParse = ( val ) =>
            {
                return val.ToString();
            },
            SortBy = 200,
        };

        /// <summary>
        /// The post trigger
        /// </summary>
        public static SettingDefinition<int> POST_TRIGGER = new SettingDefinition<int>()
        {
            Name = "Post Trigger",
            Type = SettingType.INT,
            Parse = (val) =>
            {
                if (int.TryParse(val, out int parsed) && parsed >= 5 && parsed <= 100)
                    return parsed;
                else
                    throw new ParseSettingException("Setting: \"" + POST_TRIGGER.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
            },
            UnParse = (val) =>
            {
                return val.ToString();
            },
            SortBy = 230,
        };

        /// <summary>
        /// The trigger on delay
        /// </summary>
        public static SettingDefinition<int> TRIGGER_ON_DELAY = new SettingDefinition<int>()
        {
            Name = "PLC Trigger On Delay",
            Type = SettingType.INT,
            Parse = ( val ) =>
            {
                if ( int.TryParse( val, out int parsed ) && parsed >= 0 && parsed <= 100 )
                    return parsed;
                else
                    throw new ParseSettingException( "Setting: \"" + TRIGGER_ON_DELAY.Name + "\" has invalid value: \"" + ( val ?? "(null)" ) + "\"" );
            },
            UnParse = ( val ) =>
            {
                return val.ToString();
            },
            SortBy = 230,
        };

        /// <summary>
        /// The validation response timeout
        /// </summary>
        public static SettingDefinition<int> VALIDATION_RESPONSE_TIMEOUT = new SettingDefinition<int>()
        {
            Name = "Validation Response Timeout",
            Type = SettingType.INT,
            Parse = ( val ) =>
            {
                if ( int.TryParse( val, out int parsed ) && parsed >= 20 && parsed <= 150 )
                    return parsed;
                else
                    throw new ParseSettingException( "Setting: \"" + VALIDATION_RESPONSE_TIMEOUT.Name + "\" has invalid value: \"" + ( val ?? "(null)" ) + "\"" );
            },
            UnParse = ( val ) =>
            {
                return val.ToString();
            },
            SortBy = 230,
        };

        /// <summary>
        /// The cycle end time
        /// </summary>
        public static SettingDefinition<int> CYCLE_END_TIME = new SettingDefinition<int>()
        {
            Name = "Cycle End Time",
            Type = SettingType.INT,
            Parse = ( val ) =>
            {
                if ( int.TryParse( val, out int parsed ) && parsed >= 30 && parsed <= 150 )
                    return parsed;
                else
                    throw new ParseSettingException( "Setting: \"" + CYCLE_END_TIME.Name + "\" has invalid value: \"" + ( val ?? "(null)" ) + "\"" );
            },
            UnParse = ( val ) =>
            {
                return val.ToString();
            },
            SortBy = 230,
        };

        /// <summary>
        /// The response delay
        /// </summary>
        public static SettingDefinition<int> RESPONSE_DELAY = new SettingDefinition<int>()
        {
            Name = "Response Timeout",
            Type = SettingType.INT,
            Parse = (val) =>
            {
                if (int.TryParse(val, out int parsed) && parsed >= 0)
                    return parsed;
                else
                    throw new ParseSettingException("Setting: \"" + RESPONSE_DELAY.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
            },
            UnParse = (val) =>
            {
                return val.ToString();
            },
            SortBy = 210,
        };

        /// <summary>
        /// The consecutive errors
        /// </summary>
        public static SettingDefinition<int> CONSECUTIVE_ERRORS = new SettingDefinition<int>()
        {
            Name = "Consecutive Errors",
            Type = SettingType.INT,
            Parse = ( val ) =>
            {
                if ( int.TryParse( val, out int parsed ) && parsed >= 1 && parsed <= 10 )
                    return parsed;
                else
                    throw new ParseSettingException( "Setting: \"" + CONSECUTIVE_ERRORS.Name + "\" has invalid value: \"" + ( val ?? "(null)" ) + "\"" );
            },
            UnParse = ( val ) =>
            {
                return val.ToString();
            },
            SortBy = 240,
        };

        /// <summary>
        /// The consecutive no replies limit
        /// </summary>
        public static SettingDefinition<int> CONSECUTIVE_NO_REPLIES_LIMIT = new SettingDefinition<int>()
        {
            Name = "Consecutive No Replies Limit",
            Type = SettingType.INT,
            Parse = ( val ) =>
            {
                if ( int.TryParse( val, out int parsed ) && parsed >= 1 && parsed <= 10 )
                    return parsed;
                else
                    throw new ParseSettingException( "Setting: \"" + CONSECUTIVE_NO_REPLIES_LIMIT.Name + "\" has invalid value: \"" + ( val ?? "(null)" ) + "\"" );
            },
            UnParse = ( val ) =>
            {
                return val.ToString();
            },
            SortBy = 240,
        };

        /// <summary>
        /// The consecutive failures
        /// </summary>
        public static SettingDefinition<int> CONSECUTIVE_FAILURES = new SettingDefinition<int>()
        {
            Name = "Consecutive Failures",
            Type = SettingType.INT,
            Parse = ( val ) =>
            {
                if ( int.TryParse( val, out int parsed ) && parsed >= 1 && parsed <= 10 )
                    return parsed;
                else
                    throw new ParseSettingException( "Setting: \"" + CONSECUTIVE_FAILURES.Name + "\" has invalid value: \"" + ( val ?? "(null)" ) + "\"" );
            },
            UnParse = ( val ) =>
            {
                return val.ToString();
            },
            SortBy = 250,
        };

        //public static SettingDefinition<int> MAX_LOGS_TO_PRINT = new SettingDefinition<int>()
        //{
        //    Name = "Max Logs to Print",
        //    Type = SettingType.INT,
        //    Parse = (val) =>
        //    {
        //        if (int.TryParse(val, out int parsed) && parsed > 0)
        //            return parsed;
        //        else
        //            throw new ParseSettingException("Setting: \"" + MAX_LOGS_TO_PRINT.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
        //    },
        //    UnParse = (val) =>
        //    {
        //        return val.ToString();
        //    },
        //    SortBy = 150,
        //};

        /// <summary>
        /// The enable batch management
        /// </summary>
        public static SettingDefinition<bool> ENABLE_BATCH_MANAGEMENT = new SettingDefinition<bool>()
        {
            Name = "Manage Batch",
            Type = SettingType.BIT,
            Parse = (val) =>
            {
                if (int.TryParse(val, out int parsed))
                    return parsed > 0;
                else if (true.ToString().Equals(val, StringComparison.OrdinalIgnoreCase))
                    return true;
                else if (false.ToString().Equals(val, StringComparison.OrdinalIgnoreCase))
                    return false;
                else
                    throw new ParseSettingException("Setting: \"" + ENABLE_BATCH_MANAGEMENT.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
            },
            UnParse = (val) =>
            {
                return val ? "1" : "0";
            },
            SortBy = 999,
        };

        /// <summary>
        /// The batch sum rep template
        /// </summary>
        public static SettingDefinition<string> BATCH_SUM_REP_TEMPLATE = new SettingDefinition<string>()
        {
            Name = "Batch Summary Report Template",
            Type = SettingType.STRING,
            Parse = ( val ) =>
            {
                if ( val?.Trim() is string parsed && !string.IsNullOrEmpty( parsed ) )
                    return parsed;
                else
                    throw new ParseSettingException( "Setting: \"" + BATCH_SUM_REP_TEMPLATE.Name + "\" has invalid value: \"" + ( val ?? "(null)" ) + "\"" );
            },
            UnParse = ( val ) =>
            {
                return val?.ToString() ?? string.Empty;
            },
            SortBy = 250,
        };

        /// <summary>
        /// The logs rep template
        /// </summary>
        public static SettingDefinition<string> LOGS_REP_TEMPLATE = new SettingDefinition<string>()
        {
            Name = "Logs Report Template",
            Type = SettingType.STRING,
            Parse = ( val ) =>
            {
                if ( val?.Trim() is string parsed && !string.IsNullOrEmpty( parsed ) )
                    return parsed;
                else
                    throw new ParseSettingException( "Setting: \"" + LOGS_REP_TEMPLATE.Name + "\" has invalid value: \"" + ( val ?? "(null)" ) + "\"" );
            },
            UnParse = ( val ) =>
            {
                return val?.ToString() ?? string.Empty;
            },
            SortBy = 240,
        };

        /// <summary>
        /// The settings rep template
        /// </summary>
        public static SettingDefinition<string> SETTINGS_REP_TEMPLATE = new SettingDefinition<string>()
        {
            Name = "Settings Report Template",
            Type = SettingType.STRING,
            Parse = ( val ) =>
            {
                if ( val?.Trim() is string parsed && !string.IsNullOrEmpty( parsed ) )
                    return parsed;
                else
                    throw new ParseSettingException( "Setting: \"" + SETTINGS_REP_TEMPLATE.Name + "\" has invalid value: \"" + ( val ?? "(null)" ) + "\"" );
            },
            UnParse = ( val ) =>
            {
                return val?.ToString() ?? string.Empty;
            },
            SortBy = 230,
        };

        public static SettingDefinition<string> SETTINGS_DBBACKUPSAVE_LOCATION = new SettingDefinition<string>()
        {
            Name = "Database Save Location",
            Type = SettingType.STRING,
            Parse = (val) =>
            {
                if (val?.Trim() is string parsed && !string.IsNullOrEmpty(parsed))
                    return parsed;
                else
                    throw new ParseSettingException("Setting: \"" + SETTINGS_DBBACKUPSAVE_LOCATION.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
            },
            UnParse = (val) =>
            {
                return val?.ToString() ?? string.Empty;
            },
            SortBy = 230,
        };



        /// <summary>
        /// The report save location
        /// </summary>
        public static SettingDefinition<string> REPORT_SAVE_LOCATION = new SettingDefinition<string>()
        {
            Name = "Report Save Location",
            Type = SettingType.STRING,
            Parse = ( val ) =>
            {
                if ( val?.Trim() is string parsed && !string.IsNullOrEmpty( parsed ) )
                    return parsed;
                else
                    throw new ParseSettingException( "Setting: \"" + REPORT_SAVE_LOCATION.Name + "\" has invalid value: \"" + ( val ?? "(null)" ) + "\"" );
            },
            UnParse = ( val ) =>
            {
                return val?.ToString() ?? string.Empty;
            },
            SortBy = 229,
        };


        public static SettingDefinition<float> DATABASE_MAX_SIZE = new SettingDefinition<float>()
        {
            Name = "Database Max Size",
            Type = SettingType.FLOAT,
            Parse = (val) =>
            {
                if (float.TryParse(val, out float parsed))
                    return parsed;
                else
                    throw new ParseSettingException("Setting: \"" + DATABASE_MAX_SIZE.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
            },
            UnParse = (val) =>
            {
                return val.ToString();
            },
            SortBy = 800,
        };



        /// <summary>
        /// The ocs data ready output delay
        /// </summary>
        public static SettingDefinition<int> OCS_DATA_READY_OUTPUT_DELAY = new SettingDefinition<int>()
        {
            Name = "OCS Data Ready Output Delay",
            Type = SettingType.INT,
            Parse = ( val ) =>
            {
                if ( int.TryParse( val, out int parsed ) && parsed >= 0 && parsed <= 10 )
                    return parsed;
                else
                    throw new ParseSettingException( "Setting: \"" + OCS_DATA_READY_OUTPUT_DELAY.Name + "\" has invalid value: \"" + ( val ?? "(null)" ) + "\"" );
            },
            UnParse = ( val ) =>
            {
                return val.ToString();
            },
            SortBy = 330,
        };

        /// <summary>
        /// The ocs data ready output pulse
        /// </summary>
        public static SettingDefinition<int> OCS_DATA_READY_OUTPUT_PULSE = new SettingDefinition<int>()
        {
            Name = "OCS Data Ready Output Pulse",
            Type = SettingType.INT,
            Parse = ( val ) =>
            {
                if ( int.TryParse( val, out int parsed ) && parsed >= 1 && parsed <= 20 )
                    return parsed;
                else
                    throw new ParseSettingException( "Setting: \"" + OCS_DATA_READY_OUTPUT_PULSE.Name + "\" has invalid value: \"" + ( val ?? "(null)" ) + "\"" );
            },
            UnParse = ( val ) =>
            {
                return val.ToString();
            },
            SortBy = 340,
        };

        /// <summary>
        /// The ocs data result output pulse
        /// </summary>
        public static SettingDefinition<int> OCS_DATA_RESULT_OUTPUT_PULSE = new SettingDefinition<int>()
        {
            Name = "OCS Data Result Output Pulse",
            Type = SettingType.INT,
            Parse = ( val ) =>
            {
                if ( int.TryParse( val, out int parsed ) && parsed >= 1 && parsed <= 20 )
                    return parsed;
                else
                    throw new ParseSettingException( "Setting: \"" + OCS_DATA_RESULT_OUTPUT_PULSE.Name + "\" has invalid value: \"" + ( val ?? "(null)" ) + "\"" );
            },
            UnParse = ( val ) =>
            {
                return val.ToString();
            },
            SortBy = 360,
        };

        /// <summary>
        /// The idle timeout
        /// </summary>
        public static SettingDefinition<int> IDLE_LOGIN_TIMEOUT = new SettingDefinition<int>()
        {
            Name = "Idle Login Timeout",
            Type = SettingType.INT,
            Parse = (val) =>
            {
                if (int.TryParse(val, out int parsed) && parsed >= 1 && parsed <= 99)
                    return parsed;
                else
                    throw new ParseSettingException("Setting: \"" + IDLE_LOGIN_TIMEOUT.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
            },
            UnParse = (val) =>
            {
                return val.ToString();
            },
            SortBy = 161,
        };

        /// <summary>
        /// Check for duplicate serials
        /// </summary>
        public static SettingDefinition<bool> CHECK_FOR_DUPLICATE_SERIALS = new SettingDefinition<bool>()
        {
            Name = "Check For Duplicate Serials",
            Type = SettingType.BIT,
            Parse = (val) =>
            {
                if (int.TryParse(val, out int parsed))
                    return parsed > 0;
                else if (true.ToString().Equals(val, StringComparison.OrdinalIgnoreCase))
                    return true;
                else if (false.ToString().Equals(val, StringComparison.OrdinalIgnoreCase))
                    return false;
                else
                    throw new ParseSettingException("Setting: \"" + USE_BLOCK_WRITE.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
            },
            UnParse = (val) =>
            {
                return val ? "1" : "0";
            },
            SortBy = 168,
        };

        /// <summary>
        /// Check for duplicate TIDs
        /// </summary>
        public static SettingDefinition<bool> CHECK_FOR_DUPLICATE_TIDS = new SettingDefinition<bool>()
        {
            Name = "Check For Duplicate TIDs",
            Type = SettingType.BIT,
            Parse = (val) =>
            {
                if (int.TryParse(val, out int parsed))
                    return parsed > 0;
                else if (true.ToString().Equals(val, StringComparison.OrdinalIgnoreCase))
                    return true;
                else if (false.ToString().Equals(val, StringComparison.OrdinalIgnoreCase))
                    return false;
                else
                    throw new ParseSettingException("Setting: \"" + USE_BLOCK_WRITE.Name + "\" has invalid value: \"" + (val ?? "(null)") + "\"");
            },
            UnParse = (val) =>
            {
                return val ? "1" : "0";
            },
            SortBy = 169,
        };

        /// <summary>
        /// Determines whether the specified chars is hexadecimal.
        /// </summary>
        /// <param name="chars">The chars.</param>
        /// <returns><c>true</c> if the specified chars is hexadecimal; otherwise, <c>false</c>.</returns>
        private static bool IsHex(IEnumerable<char> chars)
        {
            bool isHex;
            foreach (var c in chars)
            {
                isHex = ((c >= '0' && c <= '9') ||
                         (c >= 'a' && c <= 'f') ||
                         (c >= 'A' && c <= 'F'));

                if (!isHex)
                    return false;
            }
            return true;
        }

    }
}
