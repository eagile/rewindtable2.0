﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace KioskManagerLib.Configuration
{
    /// <summary>
    /// Class ParseSettingException.
    /// Implements the <see cref="System.Exception" />
    /// </summary>
    /// <seealso cref="System.Exception" />
    [Serializable]
    public class ParseSettingException : Exception
    {
        /// <summary>
        /// Gets or sets the invalid setting.
        /// </summary>
        /// <value>The invalid setting.</value>
        public DbSetting InvalidSetting { get; set; }
        /// <summary>
        /// Gets or sets the error message.
        /// </summary>
        /// <value>The error message.</value>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ParseSettingException" /> class.
        /// </summary>
        public ParseSettingException() : base() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="ParseSettingException" /> class.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public ParseSettingException(string message) : base(message) { ErrorMessage = message; }
        /// <summary>
        /// Initializes a new instance of the <see cref="ParseSettingException" /> class.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="innerException">The exception that is the cause of the current exception, or a null reference (<see langword="Nothing" /> in Visual Basic) if no inner exception is specified.</param>
        public ParseSettingException(string message, Exception innerException) : base(message, innerException) { ErrorMessage = message; }
        /// <summary>
        /// Initializes a new instance of the <see cref="ParseSettingException" /> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="invalidSetting">The invalid setting.</param>
        public ParseSettingException(string message, DbSetting invalidSetting) : base(message)
        {
            InvalidSetting = invalidSetting;
            ErrorMessage = message;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="ParseSettingException" /> class.
        /// </summary>
        /// <param name="serializationInfo">The serialization information.</param>
        /// <param name="streamingContext">The streaming context.</param>
        protected ParseSettingException(SerializationInfo serializationInfo, StreamingContext streamingContext) : base(serializationInfo, streamingContext)
        {
            bool invalidSetting = serializationInfo.GetBoolean( "InvalidSetting" );
            if ( serializationInfo.GetBoolean( "InvalidSetting" ) )
            {
                InvalidSetting = new DbSetting()
                {
                    KioskName = serializationInfo.GetString("KioskName"),
                    DeviceName = serializationInfo.GetString( "DeviceName" ),
                    DeviceType = serializationInfo.GetString( "DeviceType" ),
                    ProductName = serializationInfo.GetString( "ProductName" ),
                    Name = serializationInfo.GetString( "Name" ),
                    Type = serializationInfo.GetString( "Type" ),
                    Value = serializationInfo.GetString( "Value" ),
                };
            }
            else
            {
                InvalidSetting = null;
            }
            ErrorMessage = serializationInfo.GetString( "ErrorMessage" );
        }

        /// <summary>
        /// Gets the object data.
        /// </summary>
        /// <param name="serializationInfo">The serialization information.</param>
        /// <param name="streamingContext">The streaming context.</param>
        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo serializationInfo, StreamingContext streamingContext)
        {
            base.GetObjectData( serializationInfo, streamingContext );
            serializationInfo.AddValue( "InvalidSetting", InvalidSetting != null );
            serializationInfo.AddValue( "KioskName", InvalidSetting?.KioskName);
            serializationInfo.AddValue( "DeviceName", InvalidSetting?.DeviceName );
            serializationInfo.AddValue( "DeviceType", InvalidSetting?.DeviceType );
            serializationInfo.AddValue( "ProductName", InvalidSetting?.ProductName );
            serializationInfo.AddValue( "Name", InvalidSetting?.Name );
            serializationInfo.AddValue( "Type", InvalidSetting?.Type );
            serializationInfo.AddValue( "Value", InvalidSetting?.Value );
            serializationInfo.AddValue( "ErrorMessage", ErrorMessage );
        }
    }
}
