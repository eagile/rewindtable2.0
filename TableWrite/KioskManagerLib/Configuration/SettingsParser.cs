﻿using EAManagerLib.Configuration.Kiosk;
using EAReaderSDK.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace KioskManagerLib.Configuration
{

    /*
    select 
      l.name as KioskName, 
      l.active as LineActive,
      d.name as DeviceName, 
      d.type as DeviceType, 
      d.active as DeviceActive,
      p.name as ProductName, 
      p.active as ProductActive,
      s.name,
      s.type,
      s.value,
      s.active,
      s.syncStatus
    from settings as s
      left join devices as d on d.id = s.device_id
      left join lines as l on l.id = d.line_id
      left join skus as p on p.id = s.sku_id
    order by l.name, d.name, s.name, p.name
    */


    /// <summary>
    /// Enum SettingType
    /// </summary>
    public enum SettingType
    {
        /// <summary>
        /// The string
        /// </summary>
        STRING,
        /// <summary>
        /// The int
        /// </summary>
        INT,
        /// <summary>
        /// The float
        /// </summary>
        FLOAT,
        /// <summary>
        /// The enum
        /// </summary>
        ENUM,
        /// <summary>
        /// The bit
        /// </summary>
        BIT,

        DOUBLE,
        
    }


    /// <summary>
    /// Class SettingsParser.
    /// </summary>
    public static class SettingsParser
    {
        /// <summary>
        /// Gets the default name of the product.
        /// </summary>
        /// <value>The default name of the product.</value>
        public static string DefaultProductName { get; } = "DEFAULT";
        public static bool IncludeDefaultSkuSettings { get; } = true;

        /// <summary>
        /// Determines whether [is default product] [the specified product name].
        /// </summary>
        /// <param name="productName">Name of the product.</param>
        /// <returns><c>true</c> if [is default product] [the specified product name]; otherwise, <c>false</c>.</returns>
        public static bool IsDefaultProduct(string productName)
        {
            return string.Equals(productName, DefaultProductName, StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Parses the database settings.
        /// </summary>
        /// <param name="kioskName">Name of the kiosk.</param>
        /// <param name="productNames">The product names.</param>
        /// <param name="srcDbSettings">The source database settings.</param>
        /// <returns>ParseSettingsResult.</returns>
        /// <exception cref="ArgumentNullException">kioskName</exception>
        /// <exception cref="ArgumentNullException">productNames</exception>
        /// <exception cref="ArgumentNullException">srcDbSettings</exception>
        public static ParseSettingsResult ParseDbSettings(string kioskName, IList<string> productNames, IList<DbSetting> srcDbSettings)
        {
            if (kioskName == null)
                throw new ArgumentNullException(nameof(kioskName));
            if (productNames == null)
                throw new ArgumentNullException(nameof(productNames));
            if (srcDbSettings == null)
                throw new ArgumentNullException(nameof(srcDbSettings));

            string parsedKioskName = kioskName;
            List<string> parsedProductNames = productNames.Distinct(StringComparer.OrdinalIgnoreCase).OrderBy(p => p).ToList();
            List<KioskManagerSettings> parsedSettings = new List<KioskManagerSettings>();
            List<ParseSettingException> parseSettingExceptions = new List<ParseSettingException>();
            List<ParsedDbSetting> parsedDbSettings = new List<ParsedDbSetting>();

            // Add default profile
            parsedSettings.Add(new KioskManagerSettings(parsedKioskName, DefaultProductName, true));
            // Add product specific profiles
            foreach (string productName in parsedProductNames)
            {
                if (!parsedSettings.Any(p => string.Equals(p.ProductName, productName, StringComparison.OrdinalIgnoreCase)))
                {
                    parsedSettings.Add(new KioskManagerSettings(parsedKioskName, productName, IsDefaultProduct(productName)));
                }
            }

            // Parse database settings
            foreach (KioskManagerSettings kms in parsedSettings)
            {
                ParseParams parseParams = new ParseParams()
                {
                    KioskName = kms.KioskName,
                    ProductName = kms.ProductName,
                    dbSettings = srcDbSettings,
                    parseExceptions = parseSettingExceptions,
                    ValidateValue = true,
                    parsedDbSettings = parsedDbSettings,
                    IsDefault = IsDefaultProduct(kms.ProductName),
                };

                #region Kiosk

                parseParams.DeviceType = DeviceType.kiosk;
                parseParams.DeviceName = kms.Kiosk.DeviceName;
                parseParams.ValidateValue = true;
                kms.Kiosk = new KioskSettings()
                {
                    DeviceName = kms.Kiosk.DeviceName,
                    DeviceType = DeviceType.kiosk,
                    Language = ParseDbSetting(SettingLevel.Kiosk, kms.Kiosk.Language, SettingDefinitions.LANGUAGE, parseParams),
                    //SelectedProductName = ParseDbSetting(SettingLevel.Kiosk, kms.Kiosk.SelectedProductName, SettingDefinitions.SELECTED_PRODUCT, parseParams),
                    TrackUpdateReason = ParseDbSetting(SettingLevel.Kiosk, kms.Kiosk.TrackUpdateReason, SettingDefinitions.TRACK_SETTING_CHANGE_REASON, parseParams),
                    BatchSumRepTemplate = ParseDbSetting(SettingLevel.Kiosk, kms.Kiosk.BatchSumRepTemplate, SettingDefinitions.BATCH_SUM_REP_TEMPLATE, parseParams),
                    LogsRepTemplate = ParseDbSetting(SettingLevel.Kiosk, kms.Kiosk.LogsRepTemplate, SettingDefinitions.LOGS_REP_TEMPLATE, parseParams),
                    SettingsRepTemplate = ParseDbSetting(SettingLevel.Kiosk, kms.Kiosk.SettingsRepTemplate, SettingDefinitions.SETTINGS_REP_TEMPLATE, parseParams),
                    ReportSaveLocation = ParseDbSetting(SettingLevel.Kiosk, kms.Kiosk.ReportSaveLocation, SettingDefinitions.REPORT_SAVE_LOCATION, parseParams),
                    DatabaseMaxSizeGB = ParseDbSetting(SettingLevel.Kiosk, kms.Kiosk.DatabaseMaxSizeGB, SettingDefinitions.DATABASE_MAX_SIZE, parseParams),
                    IdleLoginTimeout = ParseDbSetting(SettingLevel.Kiosk, kms.Kiosk.IdleLoginTimeout, SettingDefinitions.IDLE_LOGIN_TIMEOUT, parseParams),
                    DbBackupSaveLocation = ParseDbSetting(SettingLevel.Kiosk, kms.Kiosk.DbBackupSaveLocation, SettingDefinitions.SETTINGS_DBBACKUPSAVE_LOCATION, parseParams),
                    AllowLookUp = ParseDbSetting(SettingLevel.Kiosk, kms.Kiosk.AllowLookUp, SettingDefinitions.ALLOW_LOOKUP, parseParams),
                    LookUpServer = ParseDbSetting(SettingLevel.Kiosk, kms.Kiosk.LookUpServer, SettingDefinitions.SERVER_LOOKUP, parseParams),
                    LookUpDb = ParseDbSetting(SettingLevel.Kiosk, kms.Kiosk.LookUpDb, SettingDefinitions.DATABASE_LOOKUP, parseParams),
                    LookUpUserName = ParseDbSetting(SettingLevel.Kiosk, kms.Kiosk.LookUpUserName, SettingDefinitions.USERNAME_LOOKUP, parseParams),
                    LookUpPassword = ParseDbSetting(SettingLevel.Kiosk, kms.Kiosk.LookUpPassword, SettingDefinitions.PASSWORD_LOOKUP, parseParams),

                };

                #endregion Kiosk

                #region RfidReader

                parseParams.DeviceType = DeviceType.rfid;
                parseParams.DeviceName = kms.RfidReader.DeviceName;
                parseParams.ValidateValue = true;
                kms.RfidReader.IsEnabled = ParseDbSetting(SettingLevel.Kiosk, kms.RfidReader.IsEnabled, SettingDefinitions.ENABLED, parseParams);
                parseParams.ValidateValue = kms.RfidReader.IsEnabled;
                kms.RfidReader = new RfidReaderSettings()
                {
                    DeviceName = kms.RfidReader.DeviceName,
                    DeviceType = (EAReaderSDK.SupportClasses.DeviceTypes.DeviceType)DeviceType.rfid,
                    IsEnabled = kms.RfidReader.IsEnabled,
                    Address = ParseDbSetting(SettingLevel.Kiosk, kms.RfidReader.Address, SettingDefinitions.IP_ADDRESS, parseParams),
                    Port = ParseDbSetting(SettingLevel.Kiosk, kms.RfidReader.Port, SettingDefinitions.PORT, parseParams),
                    HeartbeatInterval = ParseDbSetting(SettingLevel.Kiosk, kms.RfidReader.HeartbeatInterval, SettingDefinitions.HEARTBEAT_INTERVAL, parseParams),
                    ConnectTimeout = ParseDbSetting(SettingLevel.Kiosk, kms.RfidReader.ConnectTimeout, SettingDefinitions.CONNECT_TIMEOUT, parseParams),
                    CommandTimeout = ParseDbSetting(SettingLevel.Kiosk, kms.RfidReader.CommandTimeout, SettingDefinitions.COMMAND_TIMEOUT, parseParams),
                    UseETSI = ParseDbSetting(SettingLevel.Kiosk, kms.RfidReader.UseETSI, SettingDefinitions.USE_ETSI, parseParams),
                    EtsiTxFrequenciesInMhz = ParseDbSetting(SettingLevel.Kiosk, kms.RfidReader.EtsiTxFrequenciesInMhz, SettingDefinitions.ETSI_FREQUENCIES, parseParams),
                    ReaderMode = ParseDbSetting(SettingLevel.Sku, kms.RfidReader.ReaderMode, SettingDefinitions.READER_MODE, parseParams),
                    SearchMode = ParseDbSetting(SettingLevel.Sku, kms.RfidReader.SearchMode, SettingDefinitions.SEARCH_MODE, parseParams),
                    Session = ParseDbSetting(SettingLevel.Sku, kms.RfidReader.Session, SettingDefinitions.SESSION, parseParams),
                    TagPopulationEstimate = (ushort)ParseDbSetting(SettingLevel.Sku, kms.RfidReader.TagPopulationEstimate, SettingDefinitions.TAG_POPULATION, parseParams),
                    Antennas = ParseDbSetting(SettingLevel.Sku, kms.RfidReader.Antennas, SettingDefinitions.ANTENNAS, parseParams),
                    TxPowerInDbm = ParseDbSetting(SettingLevel.Sku, kms.RfidReader.TxPowerInDbm, SettingDefinitions.TX_POWER, parseParams),
                    RxSensitivityInDbm = ParseDbSetting(SettingLevel.Sku, kms.RfidReader.RxSensitivityInDbm, SettingDefinitions.RX_SENSITIVITY, parseParams),
                    ScanDuration = ParseDbSetting(SettingLevel.Sku, kms.RfidReader.ScanDuration, SettingDefinitions.SCAN_DURATION, parseParams),
                    LockCheck = ParseDbSetting(SettingLevel.Sku, kms.RfidReader.LockCheck, SettingDefinitions.LOCK_CHECK, parseParams),
                    UserCheck = ParseDbSetting(SettingLevel.Sku, kms.RfidReader.UserCheck, SettingDefinitions.USER_CHECK, parseParams),
                };
                #endregion RfidReader
                #region RfidWriter

                parseParams.DeviceType = DeviceType.writer;
                parseParams.DeviceName = kms.RfidWriter.DeviceName;
                parseParams.ValidateValue = true;
                kms.RfidWriter.IsEnabled = ParseDbSetting(SettingLevel.Kiosk, kms.RfidWriter.IsEnabled, SettingDefinitions.ENABLED, parseParams);
                parseParams.ValidateValue = kms.RfidWriter.IsEnabled;
                kms.RfidWriter = new RfidWriterSettings()
                {
                    DeviceName = kms.RfidWriter.DeviceName,
                    DeviceType = (EAReaderSDK.SupportClasses.DeviceTypes.DeviceType)DeviceType.writer,
                    IsEnabled = kms.RfidWriter.IsEnabled,
                    Address = ParseDbSetting(SettingLevel.Kiosk, kms.RfidWriter.Address, SettingDefinitions.IP_ADDRESS, parseParams),
                    Port = ParseDbSetting(SettingLevel.Kiosk, kms.RfidWriter.Port, SettingDefinitions.PORT, parseParams),
                    HeartbeatInterval = ParseDbSetting(SettingLevel.Kiosk, kms.RfidWriter.HeartbeatInterval, SettingDefinitions.HEARTBEAT_INTERVAL, parseParams),
                    ConnectTimeout = ParseDbSetting(SettingLevel.Kiosk, kms.RfidWriter.ConnectTimeout, SettingDefinitions.CONNECT_TIMEOUT, parseParams),
                    CommandTimeout = ParseDbSetting(SettingLevel.Kiosk, kms.RfidWriter.CommandTimeout, SettingDefinitions.COMMAND_TIMEOUT, parseParams),
                    UseETSI = ParseDbSetting(SettingLevel.Kiosk, kms.RfidWriter.UseETSI, SettingDefinitions.USE_ETSI, parseParams),
                    EtsiTxFrequenciesInMhz = ParseDbSetting(SettingLevel.Kiosk, kms.RfidWriter.EtsiTxFrequenciesInMhz, SettingDefinitions.ETSI_FREQUENCIES, parseParams),
                    ReaderMode = ParseDbSetting(SettingLevel.Sku, kms.RfidWriter.ReaderMode, SettingDefinitions.READER_MODE, parseParams),
                    SearchMode = ParseDbSetting(SettingLevel.Sku, kms.RfidWriter.SearchMode, SettingDefinitions.SEARCH_MODE, parseParams),
                    Session = ParseDbSetting(SettingLevel.Sku, kms.RfidWriter.Session, SettingDefinitions.SESSION, parseParams),
                    TagPopulationEstimate = (ushort)ParseDbSetting(SettingLevel.Sku, kms.RfidWriter.TagPopulationEstimate, SettingDefinitions.TAG_POPULATION, parseParams),
                    Antennas = ParseDbSetting(SettingLevel.Sku, kms.RfidWriter.Antennas, SettingDefinitions.ANTENNAS, parseParams),
                    TxPowerInDbm = ParseDbSetting(SettingLevel.Sku, kms.RfidWriter.TxPowerInDbm, SettingDefinitions.TX_POWER, parseParams),
                    RxSensitivityInDbm = ParseDbSetting(SettingLevel.Sku, kms.RfidWriter.RxSensitivityInDbm, SettingDefinitions.RX_SENSITIVITY, parseParams),
                    ScanDuration = ParseDbSetting(SettingLevel.Sku, kms.RfidWriter.ScanDuration, SettingDefinitions.SCAN_DURATION, parseParams),
                    LockCheck = ParseDbSetting(SettingLevel.Sku, kms.RfidWriter.LockCheck, SettingDefinitions.LOCK_CHECK, parseParams),
                    UserCheck = ParseDbSetting(SettingLevel.Sku, kms.RfidWriter.UserCheck, SettingDefinitions.USER_CHECK, parseParams),
                };
                #endregion

                #region RfidVerifier
                parseParams.DeviceType = DeviceType.validator;
                parseParams.DeviceName = kms.RfidValidator.DeviceName;
                parseParams.ValidateValue = true;
                kms.RfidValidator.IsEnabled = ParseDbSetting(SettingLevel.Kiosk, kms.RfidValidator.IsEnabled, SettingDefinitions.ENABLED, parseParams);
                parseParams.ValidateValue = kms.RfidValidator.IsEnabled;
                kms.RfidValidator = new RfidValidatorSettings()
                {
                    DeviceName = kms.RfidValidator.DeviceName,
                    DeviceType = (EAReaderSDK.SupportClasses.DeviceTypes.DeviceType)DeviceType.validator,
                    IsEnabled = kms.RfidValidator.IsEnabled,
                    Address = ParseDbSetting(SettingLevel.Kiosk, kms.RfidValidator.Address, SettingDefinitions.IP_ADDRESS, parseParams),
                    Port = ParseDbSetting(SettingLevel.Kiosk, kms.RfidValidator.Port, SettingDefinitions.PORT, parseParams),
                    HeartbeatInterval = ParseDbSetting(SettingLevel.Kiosk, kms.RfidValidator.HeartbeatInterval, SettingDefinitions.HEARTBEAT_INTERVAL, parseParams),
                    ConnectTimeout = ParseDbSetting(SettingLevel.Kiosk, kms.RfidValidator.ConnectTimeout, SettingDefinitions.CONNECT_TIMEOUT, parseParams),
                    CommandTimeout = ParseDbSetting(SettingLevel.Kiosk, kms.RfidValidator.CommandTimeout, SettingDefinitions.COMMAND_TIMEOUT, parseParams),
                    UseETSI = ParseDbSetting(SettingLevel.Kiosk, kms.RfidValidator.UseETSI, SettingDefinitions.USE_ETSI, parseParams),
                    EtsiTxFrequenciesInMhz = ParseDbSetting(SettingLevel.Kiosk, kms.RfidValidator.EtsiTxFrequenciesInMhz, SettingDefinitions.ETSI_FREQUENCIES, parseParams),
                    ReaderMode = ParseDbSetting(SettingLevel.Sku, kms.RfidValidator.ReaderMode, SettingDefinitions.READER_MODE, parseParams),
                    SearchMode = ParseDbSetting(SettingLevel.Sku, kms.RfidValidator.SearchMode, SettingDefinitions.SEARCH_MODE, parseParams),
                    Session = ParseDbSetting(SettingLevel.Sku, kms.RfidValidator.Session, SettingDefinitions.SESSION, parseParams),
                    TagPopulationEstimate = (ushort)ParseDbSetting(SettingLevel.Sku, kms.RfidValidator.TagPopulationEstimate, SettingDefinitions.TAG_POPULATION, parseParams),
                    Antennas = ParseDbSetting(SettingLevel.Sku, kms.RfidValidator.Antennas, SettingDefinitions.ANTENNAS, parseParams),
                    TxPowerInDbm = ParseDbSetting(SettingLevel.Sku, kms.RfidValidator.TxPowerInDbm, SettingDefinitions.TX_POWER, parseParams),
                    RxSensitivityInDbm = ParseDbSetting(SettingLevel.Sku, kms.RfidValidator.RxSensitivityInDbm, SettingDefinitions.RX_SENSITIVITY, parseParams),
                    ScanDuration = ParseDbSetting(SettingLevel.Sku, kms.RfidValidator.ScanDuration, SettingDefinitions.SCAN_DURATION, parseParams),
                    LockCheck = ParseDbSetting(SettingLevel.Sku, kms.RfidValidator.LockCheck, SettingDefinitions.LOCK_CHECK, parseParams),
                    UserCheck = ParseDbSetting(SettingLevel.Sku, kms.RfidValidator.UserCheck, SettingDefinitions.USER_CHECK, parseParams),
                };

                #endregion
            }

            //if (parsedDbSettings != null && parsedDbSettings.Count > 0)
            //{
            //    // Remove product settings that match the default
            //    outputDbSettings.RemoveAll(
            //        p => !IsDefaultProduct(p.ProductName)
            //        && outputDbSettings.Any(
            //            d => IsDefaultProduct(d.ProductName)
            //            && string.Equals(d.KioskName, p.KioskName, StringComparison.OrdinalIgnoreCase)
            //            && string.Equals(d.DeviceName, p.DeviceName, StringComparison.OrdinalIgnoreCase)
            //            && string.Equals(d.Name, p.Name, StringComparison.OrdinalIgnoreCase)
            //            && string.Equals(d.Value, p.Value, StringComparison.OrdinalIgnoreCase)));
            //}

            return new ParseSettingsResult(parsedKioskName, parsedProductNames, parsedSettings, parseSettingExceptions, parsedDbSettings);
        }

        /// <summary>
        /// Parses the database setting.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="level">The level.</param>
        /// <param name="originalValue">The original value.</param>
        /// <param name="settingParam">The setting parameter.</param>
        /// <param name="parseParams">The parse parameters.</param>
        /// <returns>T.</returns>
        private static T ParseDbSetting<T>(SettingLevel level, T originalValue, SettingDefinition<T> settingParam, ParseParams parseParams)
        {
            T value = originalValue;
            T defaultValue = originalValue;
            //bool isOverridden = false;
            int sortBy = settingParam?.SortBy ?? 0;
            try
            {
                List<DbSetting> filteredDbSettings = parseParams.dbSettings?.Where(
                    p => string.Equals(p.KioskName, parseParams?.KioskName, StringComparison.OrdinalIgnoreCase)
                    && string.Equals(p.DeviceName, parseParams?.DeviceName, StringComparison.OrdinalIgnoreCase)
                    && string.Equals(p.DeviceType, parseParams?.DeviceType.ToString(), StringComparison.OrdinalIgnoreCase)
                    && string.Equals(p.Name, settingParam.Name, StringComparison.OrdinalIgnoreCase)).ToList();

                DbSetting defaultDbSetting = filteredDbSettings?.FirstOrDefault(s => IsDefaultProduct(s.ProductName));
                DbSetting dbSetting = null;

                if (level == SettingLevel.Sku)
                {
                    dbSetting = filteredDbSettings?.FirstOrDefault(s => string.Equals(s.ProductName, parseParams?.ProductName, StringComparison.OrdinalIgnoreCase));
                }
                else
                {
                    dbSetting = defaultDbSetting;
                }

                if (defaultDbSetting == null)
                {
                    defaultDbSetting = new DbSetting()
                    {
                        KioskName = parseParams?.KioskName,
                        DeviceType = parseParams?.DeviceType.ToString(),
                        DeviceName = parseParams?.DeviceName,
                        ProductName = DefaultProductName,
                        Name = settingParam?.Name,
                        Type = settingParam?.Type.ToString(),
                        Value = settingParam.UnParse(originalValue),
                        SortBy = settingParam?.SortBy ?? 0,
                    };
                }

                //if (dbSetting != null)
                //{
                //    isOverridden = true;
                //}
                //else
                //{
                //    dbSetting = defaultDbSetting;
                //}
                
                sortBy = dbSetting?.SortBy ?? defaultDbSetting.SortBy;

                try
                {
                    defaultValue = defaultDbSetting?.Value != null ? settingParam.Parse(defaultDbSetting.Value) : originalValue;
                }
                catch (ParseSettingException)
                {
                    defaultValue = originalValue;
                }

                string val = dbSetting?.Value ?? settingParam.UnParse(originalValue);
                try
                {
                    value = settingParam.Parse(val);
                }
                catch (ParseSettingException pse)
                {
                    if (parseParams.ValidateValue)
                    {
                        pse.InvalidSetting = dbSetting;
                        parseParams.parseExceptions.Add(pse);
                    }
                }
            }
            catch (Exception ex)
            {
                if (parseParams.ValidateValue)
                {
                    ParseSettingException pse = new ParseSettingException("Error parsing setting", ex);
                    parseParams.parseExceptions.Add(pse);
                }
            }

            bool saveDbSetting = true;
            if (parseParams.parsedDbSettings == null)
            {
                saveDbSetting = false;
            }
            //else if (!parseParams.IsDefault && !isOverridden)
            //{
            //    saveDbSetting = false;
            //}
            //else if (parseParams.DeviceType == DeviceType.rlm
            //    && settingParam.Name == SettingDefinitions.GTIN.Name
            //    && parseParams.IsDefault)
            //{
            //    saveDbSetting = false;
            //}
            else if (!SettingsParser.IncludeDefaultSkuSettings && parseParams.IsDefault && level == SettingLevel.Sku)
            {
                saveDbSetting = false;
            }
            else if (!parseParams.IsDefault && level == SettingLevel.Kiosk)
            {
                saveDbSetting = false;
            }

            if (saveDbSetting)
            {
                ParsedDbSetting outputDbSetting = new ParsedDbSetting(settingParam.Validate)
                {
                    KioskName = parseParams.KioskName,
                    DeviceName = parseParams.DeviceName,
                    DeviceType = parseParams.DeviceType.ToString(),
                    ProductName = parseParams.ProductName,
                    Name = settingParam.Name,
                    Type = settingParam.Type.ToString(),
                    Value = settingParam.UnParse(value),
                    DefaultValue = settingParam.UnParse(defaultValue),
                    //IsDefault = parseParams.IsDefault,
                    //Overridden = isOverridden,
                    SortBy = sortBy,
                    Level = level,
                    IsValidationRequired = parseParams.ValidateValue,
                };
                parseParams.parsedDbSettings.Add(outputDbSetting);
            }

            return value;
        }
    }

    /// <summary>
    /// Class ParseSettingsResult.
    /// </summary>
    public class ParseSettingsResult
    {
        /// <summary>
        /// Gets the name of the kiosk.
        /// </summary>
        /// <value>The name of the kiosk.</value>
        public string KioskName { get; private set; }
        /// <summary>
        /// Gets the product names.
        /// </summary>
        /// <value>The product names.</value>
        public List<string> ProductNames { get; }
        /// <summary>
        /// Parsed and organized settings
        /// </summary>
        /// <value>The parsed settings.</value>
        public List<KioskManagerSettings> ParsedSettings { get; }
        /// <summary>
        /// Errors that occured while parsing the database settings
        /// </summary>
        /// <value>The parse settings exceptions.</value>
        public List<ParseSettingException> ParseSettingsExceptions { get; }
        /// <summary>
        /// Raw database settings
        /// </summary>
        /// <value>The parsed database settings.</value>
        public List<ParsedDbSetting> ParsedDbSettings { get; }

        /// <summary>
        /// The synchronize root
        /// </summary>
        public readonly object SyncRoot = new object();

        /// <summary>
        /// Initializes a new instance of the <see cref="ParseSettingsResult" /> class.
        /// </summary>
        public ParseSettingsResult()
        {
            KioskName = null;
            ProductNames = new List<string>();
            ParsedSettings = new List<KioskManagerSettings>();
            ParseSettingsExceptions = new List<ParseSettingException>();
            ParsedDbSettings = new List<ParsedDbSetting>();
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="ParseSettingsResult" /> class.
        /// </summary>
        /// <param name="parseSettingsResult">The parse settings result.</param>
        public ParseSettingsResult(ParseSettingsResult parseSettingsResult)
        {
            KioskName = parseSettingsResult.KioskName;
            ProductNames = new List<string>(parseSettingsResult.ProductNames);
            ParsedSettings = new List<KioskManagerSettings>(parseSettingsResult.ParsedSettings);
            ParseSettingsExceptions = new List<ParseSettingException>(parseSettingsResult.ParseSettingsExceptions);
            ParsedDbSettings = new List<ParsedDbSetting>(parseSettingsResult.ParsedDbSettings);
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="ParseSettingsResult" /> class.
        /// </summary>
        /// <param name="kioskName">Name of the kiosk.</param>
        /// <param name="productNames">The product names.</param>
        /// <param name="parsedSettings">The parsed settings.</param>
        /// <param name="parseSettingsExceptions">The parse settings exceptions.</param>
        /// <param name="parsedDbSettings">The parsed database settings.</param>
        public ParseSettingsResult(string kioskName, IList<string> productNames, IList<KioskManagerSettings> parsedSettings, IList<ParseSettingException> parseSettingsExceptions, IList<ParsedDbSetting> parsedDbSettings)
        {
            KioskName = kioskName;
            ProductNames = new List<string>(productNames);
            ParsedSettings = new List<KioskManagerSettings>(parsedSettings);
            ParseSettingsExceptions = new List<ParseSettingException>(parseSettingsExceptions);
            ParsedDbSettings = new List<ParsedDbSetting>(parsedDbSettings);
        }

        /// <summary>
        /// Gets the product settings.
        /// </summary>
        /// <param name="productName">Name of the product.</param>
        /// <returns>KioskManagerSettings.</returns>
        public KioskManagerSettings GetProductSettings(string productName)
        {
            lock (SyncRoot)
            {
                KioskManagerSettings productSettings = null;
                if (!string.IsNullOrEmpty(productName))
                {
                    productSettings = ParsedSettings?
                        .SingleOrDefault(p => string.Equals(p.ProductName, productName, StringComparison.OrdinalIgnoreCase)
                        && string.Equals(p.KioskName, KioskName, StringComparison.OrdinalIgnoreCase));
                }

                if (productSettings == null)
                {
                    productSettings = GetDefaultSettings();
                }

                return productSettings;
            }
        }

        /// <summary>
        /// Gets the default settings.
        /// </summary>
        /// <returns>KioskManagerSettings.</returns>
        public KioskManagerSettings GetDefaultSettings()
        {
            lock (SyncRoot)
            {
                KioskManagerSettings productSettings =
                    ParsedSettings.SingleOrDefault(p => p.IsDefault && string.Equals(p.KioskName, KioskName, StringComparison.OrdinalIgnoreCase));
                return productSettings;
            }
        }

        /// <summary>
        /// Imports the result.
        /// </summary>
        /// <param name="parseSettingsResult">The parse settings result.</param>
        public void ImportResult(ParseSettingsResult parseSettingsResult)
        {
            lock (SyncRoot)
            {
                KioskName = parseSettingsResult.KioskName;
                ProductNames.Clear();
                ProductNames.AddRange(parseSettingsResult.ProductNames);
                ParsedSettings.Clear();
                ParsedSettings.AddRange(parseSettingsResult.ParsedSettings);
                ParseSettingsExceptions.Clear();
                ParseSettingsExceptions.AddRange(parseSettingsResult.ParseSettingsExceptions);
                ParsedDbSettings.Clear();
                ParsedDbSettings.AddRange(parseSettingsResult.ParsedDbSettings);
            }
        }
    }

    //public class UnParseSettingsResult
    //{
    //    public List<DbSetting> DbSettings { get; }
    //    public UnParseSettingsResult(List<DbSetting> dbSettings)
    //    {
    //        DbSettings = new List<DbSetting>(dbSettings);
    //    }
    //}

    //public class ProcessSettingsResult
    //{
    //    public List<KioskManagerSettings> KioskManagerSettings { get; set; } = new List<KioskManagerSettings>();
    //    public List<ParseSettingException> ParseSettingsExceptions { get; set; } = new List<ParseSettingException>();
    //    public List<DbSetting> DbSettings { get; set; } = new List<DbSetting>();
    //}

    /// <summary>
    /// Class ParseParams.
    /// </summary>
    public class ParseParams
    {
        /// <summary>
        /// Gets or sets the name of the kiosk.
        /// </summary>
        /// <value>The name of the kiosk.</value>
        public string KioskName { get; set; }
        /// <summary>
        /// Gets or sets the name of the device.
        /// </summary>
        /// <value>The name of the device.</value>
        public string DeviceName { get; set; }
        /// <summary>
        /// Gets or sets the type of the device.
        /// </summary>
        /// <value>The type of the device.</value>
        public DeviceType DeviceType { get; set; }
        /// <summary>
        /// Gets or sets the name of the product.
        /// </summary>
        /// <value>The name of the product.</value>
        public string ProductName { get; set; }
        /// <summary>
        /// Gets or sets the database settings.
        /// </summary>
        /// <value>The database settings.</value>
        public IEnumerable<DbSetting> dbSettings { get; set; }
        /// <summary>
        /// Gets or sets the parse exceptions.
        /// </summary>
        /// <value>The parse exceptions.</value>
        public List<ParseSettingException> parseExceptions { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether [validate value].
        /// </summary>
        /// <value><c>true</c> if [validate value]; otherwise, <c>false</c>.</value>
        public bool ValidateValue { get; set; }
        /// <summary>
        /// Gets or sets the parsed database settings.
        /// </summary>
        /// <value>The parsed database settings.</value>
        public List<ParsedDbSetting> parsedDbSettings { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether this instance is default.
        /// </summary>
        /// <value><c>true</c> if this instance is default; otherwise, <c>false</c>.</value>
        public bool IsDefault { get; set; }
    }
}
