﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EAManagerLib.Configuration.Kiosk
{
    public class KioskSettings
    {
        public string DeviceName { get; set; } = "Kiosk";
        public DeviceType DeviceType { get; set; } = DeviceType.kiosk;
        public string Language { get; set; } = "en-US";
        //public string SelectedProductName { get; set; } = string.Empty;
        public bool TrackUpdateReason { get; set; } = false;
        public string BatchSumRepTemplate { get; set; } = "BatchSummaryTemplate.dlex";
        public string LogsRepTemplate { get; set; } = "LogsReportTemplate.dlex";
        public string SettingsRepTemplate { get; set; } = "SettingsReportTemplate.dlex";
        public string ReportSaveLocation { get; set; } = "C:\\Kiosk\\Reports\\";
        public string DbBackupSaveLocation { get; set; } = "C:\\Kiosk\\DatabaseBackups\\";
        public float DatabaseMaxSizeGB { get; set; } = 0;
        public bool AllowLookUp { get; set; } = false;
        public string LookUpServer { get; set; } = "ea-sqldev";
        public string LookUpDb { get; set; } = "RLM_GLOBAL_HMI";
        public string LookUpUserName { get; set; } = "WINDOWS";
        public string LookUpPassword { get; set; } = "WINDOWS";

        /// <summary>
        /// Number of idle minutes after which to log out the user
        /// </summary>
        public int IdleLoginTimeout { get; set; } = 15;

        public string DefaultReportPath { get; set; } = "C:\\Reports\\";
    }
}
