﻿using EAReaderSDK.Settings;
using System;
using System.Collections.Generic;
using System.Text;

namespace EAManagerLib.Configuration.Kiosk
{
    public enum DeviceType
    {
        kiosk = 10,
        rfid = 20,
        writer = 30,
        validator = 40,
    }

    public class KioskManagerSettings
    {
        public string KioskName { get; set; }

        public string ProductName { get; set; } = null;

        public bool IsDefault { get; set; } = false;

        public KioskSettings Kiosk { get; set; } = new KioskSettings();

        public RfidReaderSettings RfidReader { get; set; } = new RfidReaderSettings();

        public RfidWriterSettings RfidWriter { get; set; } = new RfidWriterSettings();
        public RfidValidatorSettings RfidValidator { get; set; } = new RfidValidatorSettings();


        public KioskManagerSettings(string kioskName, string productName, bool isDefault)
        {
            KioskName = kioskName;
            ProductName = productName;
            IsDefault = isDefault;
        }
    }
}
