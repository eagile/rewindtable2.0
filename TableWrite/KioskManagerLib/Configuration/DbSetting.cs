﻿using System;
using System.Collections.Generic;

namespace KioskManagerLib.Configuration
{
    public enum SettingLevel
    {
        Kiosk,
        Sku,
    }
    public enum SaveState
    {
        Update,
        Add,
    }
    /// <summary>
    /// Class DbSetting.
    /// </summary>
    public class DbSetting
    {
        /// <summary>
        /// Gets or sets the name of the kiosk.
        /// </summary>
        /// <value>The name of the kiosk.</value>
        public string KioskName { get; set; }
        /// <summary>
        /// Gets or sets the name of the device.
        /// </summary>
        /// <value>The name of the device.</value>
        public string DeviceName { get; set; }
        /// <summary>
        /// Gets or sets the type of the device.
        /// </summary>
        /// <value>The type of the device.</value>
        public string DeviceType { get; set; }
        /// <summary>
        /// Gets or sets the name of the product.
        /// </summary>
        /// <value>The name of the product.</value>
        public string ProductName { get; set; }
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>The type.</value>
        public string Type { get; set; }
        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>The value.</value>
        public string Value { get; set; }
        /// <summary>
        /// Gets or sets the sort by.
        /// </summary>
        /// <value>The sort by.</value>
        public int SortBy { get; set; }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
        public override string ToString()
        {
            return
                (KioskName ?? "(KioskName)") + "\\" +
                (ProductName ?? "(ProductName)") + "\\" +
                (DeviceName ?? "(DeviceName)") + "\\" +
                (Name ?? "(Name)") + "=" +
                (Value ?? "(Value)");
        }
    }

    /// <summary>
    /// Class ParsedDbSetting.
    /// Implements the <see cref="LineManagerLib.Configuration.DbSetting" />
    /// </summary>
    /// <seealso cref="LineManagerLib.Configuration.DbSetting" />
    public class ParsedDbSetting : DbSetting
    {
        /// <summary>
        /// Gets or sets the default value.
        /// </summary>
        /// <value>The default value.</value>
        public string DefaultValue { get; set; }
        //public bool Overridden { get; set; }
        //public bool IsDefault { get; set; }
        /// <summary>
        /// Gets or sets the level.
        /// </summary>
        /// <value>The level.</value>
        public SettingLevel Level { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether this instance is validation required.
        /// </summary>
        /// <value><c>true</c> if this instance is validation required; otherwise, <c>false</c>.</value>
        public bool IsValidationRequired { get; set; }
        /// <summary>
        /// Gets the validate.
        /// </summary>
        /// <value>The validate.</value>
        public Func<string, string> Validate { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ParsedDbSetting" /> class.
        /// </summary>
        /// <param name="validateFunction">The validate function.</param>
        public ParsedDbSetting(Func<string, string> validateFunction)
        {
            Validate = validateFunction;
        }
    }

    /// <summary>
    /// Class ChangedDbSetting.
    /// Implements the <see cref="LineManagerLib.Configuration.ParsedDbSetting" />
    /// </summary>
    /// <seealso cref="LineManagerLib.Configuration.ParsedDbSetting" />
    public class ChangedDbSetting : ParsedDbSetting
    {
        /// <summary>
        /// Gets or sets the old value.
        /// </summary>
        /// <value>The old value.</value>
        public string OldValue { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether this instance is reason for change required.
        /// </summary>
        /// <value><c>true</c> if this instance is reason for change required; otherwise, <c>false</c>.</value>
        public bool IsReasonForChangeRequired { get; set; }
        /// <summary>
        /// Gets or sets the reason for change.
        /// </summary>
        /// <value>The reason for change.</value>
        public string ReasonForChange { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChangedDbSetting" /> class.
        /// </summary>
        /// <param name="validateFunction">The validate function.</param>
        public ChangedDbSetting(Func<string, string> validateFunction) 
            : base(validateFunction) { }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
        public override string ToString()
        {
            return
                (KioskName ?? "(KioskName)") + "\\" +
                (ProductName ?? "(ProductName)") + "\\" +
                (DeviceName ?? "(DeviceName)") + "\\" +
                (Name ?? "(Name)") + "=" +
                (OldValue ?? "(OldValue)") + "->" +
                (Value ?? "(NewValue)") + ":" +
                (ReasonForChange ?? "(NoReason)");
        }
    }
}
