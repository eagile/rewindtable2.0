﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KioskManagerLib.Models
{
    public class FilePathSettingModel
    {
        public string FilePath { get; set; }
        public string FilePathSettingPath { get; set; }
        public string DBConnectionPath { get; set; }
        public string ErrorLogPath { get; set; }
        public string SystemErrorLogPath { get; set; }
        

    }
}
