﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KioskManagerLib.Models
{
    public class MasterList
    {
        public List<MasterModel> MasterListItems { get; set; }

        public MasterList()
        {
            MasterListItems = new List<MasterModel>();
        }
        public void AddMasterItem(string[] values)
        {
            if (values.Count() == 13)
                MasterListItems.Add(new MasterModel()
                {
                    //EPC = values[0],
                    //EPC_LOCK = values[1],
                    //USER = values[2],
                    //USER_LOCK = values[3],
                    //ACCESS = values[4],
                    //ACCESS_LOCK = values[5],
                    //KILL = values[6],
                    //KILL_LOCK = values[7],
                    //PRINT1 = values[8],
                    //PRINT2 = values[9],
                    //PRINT3 = values[10],
                    //PRINT4 = values[11],
                    //PRINT5 = values[12],
                });

        }

    }
    public class MasterModel
    {
        public string TID { get; set; }
        public string EPC { get; set; }
        public string ResultTime { get; set; }
        public bool VerifySuccess { get; set; }
        public bool WriteSuccess { get; set; }
        public bool EPC_LOCK { get; set; }
        public string USER { get; set; }
        public bool USER_LOCK { get; set; }
        public string ACCESS { get; set; }
        public bool ACCESS_LOCK { get; set; }
        public string KILL { get; set; }
        public bool KILL_LOCK { get; set; }
        public string PRINT1 { get; set; }
        public string PRINT2 { get; set; }
        public string PRINT3 { get; set; }
        public string PRINT4 { get; set; }
        public string PRINT5 { get; set; }
    }
}
