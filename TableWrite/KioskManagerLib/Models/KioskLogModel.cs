﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KioskManagerLib.Models
{
    public class KioskLogModel
    {
        public string LogTime { get; set; }
        public string User { get; set; }
        public string KioskID { get; set; }
        public string Message { get; set; }



    }
}
