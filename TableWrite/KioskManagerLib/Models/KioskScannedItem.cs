﻿using EA.Helpers;
using EAManagerLib.Cultures;



namespace KioskManagerLib.Models
{
    public class KioskScannedItem : ObservableObject
    {
        private string _noData => LibStrings.no_data;// StringResources.GetLocalizedString(LibStrings.ResourceManager, "no_data");

        public string TidDisplay { get => string.IsNullOrEmpty(_tid) ? _noData : _tid; }
        public string Tid { get => _tid; set => SetProperty(ref _tid, value, nameof(Tid), nameof(TidDisplay)); }
        private string _tid;

        public string GtinDisplay { get => string.IsNullOrEmpty(_gtin) ? _noData : _gtin; }
        public string Gtin { get => _gtin; set => SetProperty(ref _gtin, value, nameof(Gtin), nameof(GtinDisplay)); }
        private string _gtin;

        public string SerialNumberDisplay { get => string.IsNullOrEmpty(_serialNumber) ? _noData : _serialNumber; }
        public string SerialNumber { get => _serialNumber; set => SetProperty(ref _serialNumber, value, nameof(SerialNumber), nameof(SerialNumberDisplay)); }
        private string _serialNumber;

        public string BatchLotDisplay { get => string.IsNullOrEmpty(_batchLot) ? _noData : _batchLot; }
        public string BatchLot { get => _batchLot; set => SetProperty(ref _batchLot, value, nameof(BatchLot), nameof(BatchLotDisplay)); }
        private string _batchLot;

        public string ExpiryDisplay { get => string.IsNullOrEmpty(_expry) ? _noData : _expry; }
        public string Expiry { get => _expry; set => SetProperty(ref _expry, value, nameof(Expiry), nameof(ExpiryDisplay)); }
        private string _expry;

        public string LockStateDisplay { get => string.IsNullOrEmpty(_lockState) ? _noData : _lockState; }
        public string LockState { get => _lockState; set => SetProperty(ref _lockState, value, nameof(LockState), nameof(LockStateDisplay)); }
        private string _lockState;

        public string RlmStatusDisplay { get => string.IsNullOrEmpty(_expry) ? "0" : _rlmStatus; }
        public string RlmStatus { get => _rlmStatus; set => SetProperty(ref _rlmStatus, value, nameof(RlmStatus), nameof(RlmStatusDisplay)); }
        private string _rlmStatus;


    }
}
