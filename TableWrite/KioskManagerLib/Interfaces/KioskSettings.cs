﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using EAManagerLib.ConfigurationModels;
using EAManagerLib.Configuration.Kiosk;

namespace KioskManagerLib.Interfaces
{
    public interface IKioskSettings
    {
        public KioskManagerSettings Kiosksettings { get; set; }
        public AWSConnectionModel KioskConnectionSettings { get; set; }

       


    }
}
