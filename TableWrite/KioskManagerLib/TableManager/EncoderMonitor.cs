﻿using System;
using System.IO.Ports;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel;
using NLog;

namespace TableManager
{
    public class EncoderMonitor 
    {
        Logger Logger = LogManager.GetCurrentClassLogger();
        SerialPort port;
        public void Reset()
        {
            value = newvalue;
            _distance = 0;
        }

        public void Open(CancellationToken ct)
        {
            port = new SerialPort()
            {
                PortName = ComPort,
                BaudRate = 230400,
                Parity = Parity.None,
                DataBits = 8,
                StopBits = StopBits.One,
                Handshake = Handshake.None,
                DtrEnable = true,
                RtsEnable = false,
                ReadTimeout = 500,
            };

            port.DataReceived += DataReceived;
            port.Open();

            port.WriteLine("W0000"); // Quadrature mode
            port.WriteLine("W04000"); // Count Up
            port.WriteLine("W04100"); // Count Up
            port.WriteLine("W092"); // clear counter
            port.WriteLine("W0363"); //Sync to index pulse if present
            port.WriteLine("W034F"); // Count range is set by the next value
            Logger.Info("{CLASS} Setup: W0000, W04100, W092, W0363, W034F", "EncoderMonitor");
            string countermax = EncoderPulsePerR.ToString("X");
            port.WriteLine($"W08{countermax}"); // W08 Sets the counter max value - set using hex Set this value to the encoder pulse max for 1 R
            Logger.Info("{CLASS} Streaming on", "EncoderMonitor");

            port.WriteLine("S0E"); // Setups up streaming mode
            port.WriteLine("W0B0001"); // Resolution min of 1 pulse in streaming
            port.WriteLine("W0C0005"); // 9.5ms polling interval
        //    Task.Run(() => WorkLoop(ct));
            Stream();
        }
        //<summary> Turn on the data stream mode from the QSB - this will update the RPM property automatically
        public void Stream()
        {
            Logger.Info("{CLASS} Streaming on", "EncoderMonitor");

            port.WriteLine("S0E"); // Setups up streaming mode
            port.WriteLine("W0B0001"); // Resolution min of 1 pulse in streaming
            port.WriteLine("W0C0005"); // 9.5ms polling interval
        }

        //<Summary> intiates a read, this will be processed and the property will be updated
        //public void Read()
        //{
        //    if (!reading)
        //    {
        //        Logger.Info("{CLASS} Read Triggered", "EncoderMonitor");
        //        reading = true;
        //        try
        //        {
        //            port.WriteLine("R0E"); // Setups up streaming mode
        //        }
        //        catch (Exception ex) { Logger.Error(ex); }
        //    }
        //}

        private uint value = 0;
        private uint newvalue = 0;
        public bool update = false;
        public bool reading = false;

        //<summary>Interval in MS to calculate the RPM based on encoder input
        public uint EncoderPulsePerR { get; set; } = 6000; // 5DC0
        
        public string ComPort { get; set; } = "COM6";
        public double Circumfrence { get; } = 6; // Math.PI * EncoderDiam; } }
        public double ContactRatio { get; } = .64;

        private double _distance = 0;
        public double distance
        {
            get
            {
                double inches = (((double)_distance / (double)EncoderPulsePerR) * (double)Circumfrence);

                return inches * ContactRatio;// _distance;
            }
        }

        // Worker loop deals with the data that is coming in and publishes the calculated RPM.
        public void SerialPortLoop(CancellationToken ct)
        {
            while (!ct.IsCancellationRequested)
            {
                // don't want too much jitter in the reading by over sampling
                //Thread.Sleep(RefreshMS);
                if (update)
                {
                    uint travel = 0;
                    if (newvalue < value && value - newvalue > 4000)
                    {
                        travel = newvalue + EncoderPulsePerR - value;
                        Logger.Debug($"New: {newvalue} Pulse{EncoderPulsePerR} Value{value} = Travel{travel}");


                    }

                    else
                    {

                        travel = newvalue - value;
                        Logger.Debug($"New: {newvalue} Value{value} = Travel{travel}");
                    } 

                    //Logger.Debug($"Update: {_distance} += {newvalue} - {value} = {_distance + travel}");


                    _distance += travel;
                    value = newvalue;
                    update = false;
                }
            }
            Logger.Info($"Loop: {ct.IsCancellationRequested}");
            port.DataReceived -= DataReceived;
            port.Close();
        }

        // DataReceived receives data from an outside thread, so all fwork here is non blocking on the worker loop
        void DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                string data = port.ReadLine();
                
                string[] sections = data.Split(" ");
                uint tempvalue = uint.Parse(sections[2], System.Globalization.NumberStyles.HexNumber);
                if (tempvalue != value )
                {
                    newvalue = tempvalue;
                    Logger.Info($"DataReceived New Value: {newvalue}");
                    update = true;
                }
                reading = false;
            }
            catch (Exception ex)
            {
                var aa = ex.ToString();
            }
        }

        public void Close()
        {
            port.DataReceived -= DataReceived;
            port.Close();
        }

      

    }
}
