﻿using EA.Impinj;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace EAManagerLib.TableManager.UHF_Factories
{
    [Serializable()]
    public class TestClass
    {
        [XmlElement]
        public string EPC { get; set; }
        [XmlElement]
        public TagLockState EPC_LOCK { get; set; }
        [XmlElement]
        public string USER { get; set; }
        [XmlElement]
        public TagLockState USER_LOCK { get; set; }
        [XmlElement]
        public string ACCESS { get; set; }
        [XmlElement]
        public TagLockState ACCESS_LOCK { get; set; }
        [XmlElement]
        public string KILL { get; set; }
        [XmlElement]
        public TagLockState KILL_LOCK { get; set; }
        [XmlElement]
        public string PRINT1 { get; set; }
        [XmlElement]
        public string PRINT2 { get; set; }
        [XmlElement]
        public string PRINT3 { get; set; }
        [XmlElement]
        public string PRINT4 { get; set; }
        [XmlElement]
        public string PRINT5 { get; set; }
    }
}
