﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace KioskManagerLib.TableManager.UHF_Factories
{
        public class DynamicClassBuilder
        {
            AssemblyName asemblyName;
            public DynamicClassBuilder(string ClassName)
            {
                this.asemblyName = new AssemblyName(ClassName);
            }
            public List< object> CreateObject(string[] PropertyNames, Type[] Types,int linecount)
            {
            List<object> newlist = new List<object>();
                if (PropertyNames.Length != Types.Length)
                {
                    // The number of property names should match their corresopnding types number

                }

                TypeBuilder DynamicClass = this.CreateClass();
                this.CreateConstructor(DynamicClass);

                for (int ind = 0; ind < PropertyNames.Count(); ind++)
                    CreateProperty(DynamicClass, PropertyNames[ind], Types[ind]);

                Type type = DynamicClass.CreateType();

            for (var i = 0; i < linecount; i++)
            {
                newlist.Add( Activator.CreateInstance(type));
            };
            return newlist;

              //  return Activator.CreateInstance(type);
        }
            private TypeBuilder CreateClass()
            {
            AssemblyBuilder assemblyBuilder = AssemblyBuilder.DefineDynamicAssembly(new AssemblyName(Guid.NewGuid().ToString()),
            AssemblyBuilderAccess.Run);
            //AssemblyBuilder assemblyBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(this.asemblyName, AssemblyBuilderAccess.Run);
                ModuleBuilder moduleBuilder = assemblyBuilder.DefineDynamicModule("MainModule");
                TypeBuilder typeBuilder = moduleBuilder.DefineType(this.asemblyName.FullName
                                    , TypeAttributes.Public |
                                    TypeAttributes.Class |
                                    TypeAttributes.AutoClass |
                                    TypeAttributes.AnsiClass |
                                    TypeAttributes.BeforeFieldInit |
                                    TypeAttributes.AutoLayout
                                    ,null);
                return typeBuilder;
            }
            private void CreateConstructor(TypeBuilder typeBuilder)
            {

            typeBuilder.DefineDefaultConstructor(MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.RTSpecialName);
            //   System.Reflection.MethodAttributes methodAttributes =
            //  System.Reflection.MethodAttributes.Public
            //| System.Reflection.MethodAttributes.HideBySig;

            // typeBuilder.DefineDefaultConstructor(methodAttributes);

            }
        private void CreateProperty(TypeBuilder typeBuilder, string propertyName, Type propertyType)
            {
                FieldBuilder fieldBuilder = typeBuilder.DefineField("_" + propertyName, propertyType, FieldAttributes.Public);

                PropertyBuilder propertyBuilder = typeBuilder.DefineProperty(propertyName, PropertyAttributes.HasDefault, propertyType, null);
                MethodBuilder getPropMthdBldr = typeBuilder.DefineMethod("get_" + propertyName, MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.HideBySig, propertyType, Type.EmptyTypes);
                ILGenerator getIl = getPropMthdBldr.GetILGenerator();

                getIl.Emit(OpCodes.Ldarg_0);
                getIl.Emit(OpCodes.Ldfld, fieldBuilder);
                getIl.Emit(OpCodes.Ret);

                MethodBuilder setPropMthdBldr = typeBuilder.DefineMethod("set_" + propertyName,
                      MethodAttributes.Public |
                      MethodAttributes.SpecialName |
                      MethodAttributes.HideBySig,
                      null, new[] { propertyType });

                ILGenerator setIl = setPropMthdBldr.GetILGenerator();
                Label modifyProperty = setIl.DefineLabel();
                Label exitSet = setIl.DefineLabel();

                setIl.MarkLabel(modifyProperty);
                setIl.Emit(OpCodes.Ldarg_0);
                setIl.Emit(OpCodes.Ldarg_1);
                setIl.Emit(OpCodes.Stfld, fieldBuilder);

                setIl.Emit(OpCodes.Nop);
                setIl.MarkLabel(exitSet);
                setIl.Emit(OpCodes.Ret);

                propertyBuilder.SetGetMethod(getPropMthdBldr);
                propertyBuilder.SetSetMethod(setPropMthdBldr);
                
            }
        }
    }
