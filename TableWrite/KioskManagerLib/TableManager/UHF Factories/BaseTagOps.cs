﻿using System;
using System.ComponentModel;
using EA.Impinj;
using SupportClasses;

namespace WriterFactories
{
    [Description("EPC and Locking")]
    public class BaseTagOps : VirtualOpsBase
    {
        public override double TagPitch { get; set; } = .875;
        public override double TagGap { get; set; }
        public override ushort writeAntenna { get; set; }
        public override ReaderMode rfMode { get; set; } = ReaderMode.Hybrid;
        public override SearchMode SearchMode { get; set; } = SearchMode.DualTarget;
        public override short rxFilterRead { get; set; } = -50;
        public override short rxFilterWrite { get; set; } = -48;
        public override double SFM { get; set; } = 12;

        public override Session Session { get; set; } = Session.S3;

        public override TagOpSequence BuildWriteOps(TagInfo tag, string tid, DataBaseEntry dataentry)
        {
            TagOpSequence tagOpSequence = new TagOpSequence()
            {
                AntennaId = writeAntenna,
                TargetTag = new TargetTag()
                {
                    MemoryBank = MemoryBank.Tid,
                    BitPointer = 0,
                    Data = tid
                },
                BlockWriteEnabled = true,
                BlockWriteWordCount = 2,
                RetryCount = 2
            };
            TagWriteOp epcOP = new TagWriteOp()
            {
                Id = 111,
                MemoryBank = MemoryBank.Epc,
                Data = TagData.FromHexString(dataentry.epc),
                WordPointer = WordPointers.Epc
            };
            tagOpSequence.Ops.Add(epcOP);

            if (tag.epc.Length != dataentry.epc.Length)
            {
                // We need adjust the PC bits and write them back to the 
                // tag because the length of the EPC has changed.
                ushort newEpcLenWords = (ushort)(dataentry.epc.Length / 4);
                ushort newPcBits = PcBits.AdjustPcBits(tag.PcBits, newEpcLenWords);

                TagWriteOp writePc = new TagWriteOp();
                writePc.Id = 321;
                // The PC bits are in the EPC memory bank.
                writePc.MemoryBank = MemoryBank.Epc;
                // Specify the data to write (the modified PC bits).
                writePc.Data = TagData.FromWord(newPcBits);
                // Start writing at the start of the PC bits.
                writePc.WordPointer = WordPointers.PcBits;

                // Add this tag write op to the tag operation sequence.
                tagOpSequence.Ops.Add(writePc);
            }

            //TODO: this is currently ignoring all passwords
            TagLockOp lop = new TagLockOp()
            {
                Id = 222,
                AccessPassword = TagData.FromHexString(dataentry.accesspw),
                AccessPasswordLockType = dataentry.access_lock,
                EpcLockType = dataentry.epc_lock,
                KillPasswordLockType = dataentry.kill_lock,
                UserLockType = dataentry.user_lock
            };
            tagOpSequence.Ops.Add(lop);
            return tagOpSequence;
        }
        public override TagOpSequence BuildReadOps(string tid, DataBaseEntry dataentry)
        {
            TagOpSequence tagOpSequence = new TagOpSequence()
            {
                TargetTag = new TargetTag()
                {
                    MemoryBank = MemoryBank.Tid,
                    BitPointer = 0,
                    Data = tid,
                }
            };
            if (!dataentry.user.Equals("?"))
            {
                TagReadOp top = new TagReadOp()
                {
                    Id = 111,
                    MemoryBank = MemoryBank.User,
                    WordPointer = 0,
                    WordCount = (ushort)(dataentry.user.Length / 4)
                };
                tagOpSequence.Ops.Add(top);
            }

            if (dataentry.access_lock < TagLockState.None || dataentry.epc_lock < TagLockState.None || dataentry.access_lock < TagLockState.None || dataentry.kill_lock < TagLockState.None)
            {
                TagReadOp readReserved = new TagReadOp();
                readReserved.Id = 112;
                readReserved.MemoryBank = MemoryBank.Reserved;
                readReserved.WordPointer = 4;
                readReserved.WordCount = 1; //lock bits

                tagOpSequence.Ops.Add(readReserved);
            }

            return tagOpSequence;
        }

        public override TagInfo TagOpProcess(Tag tag, TagInfo ti, DataBaseEntry resultsdata, DataBaseEntry dataentry)
        {
            foreach (TagOpResult result in tag.TagOpResults)
            {
                if (result is TagReadOpResult)
                {
                    TagReadOpResult readresult = result as TagReadOpResult;
                    switch (result.OpId)
                    {
                        case 112:
                            resultsdata = DetermineLockState(resultsdata, readresult.Data.ToHexString());
                            break;
                        case 111:
                            resultsdata.user = readresult.Data.ToHexString();
                            break;
                        default:
                            break;
                    }
                }
            }

            if (resultsdata != null)
            {
                if (dataentry.epc == resultsdata.epc)
                {
                    ti.verifyEPC = true;
                }
                if (dataentry.kill_lock == resultsdata.kill_lock && dataentry.access_lock == resultsdata.access_lock
                         && dataentry.epc_lock == resultsdata.epc_lock)
                {
                    ti.verifyLock = true;
                }
                if (!dataentry.user.Equals("?"))
                {
                    if (dataentry.user.Equals(resultsdata.user))
                        ti.verifyUser = true;
                }
                else ti.verifyUser = true;

            }

            if (ti.verifyEPC && ti.verifyLock && ti.verifyUser)
                ti.verifySuccess = true;

            return ti;
        }

        internal static DataBaseEntry DetermineLockState(DataBaseEntry results, string state)
        {
            //DataBaseEntry results = new DataBaseEntry();
            string strBinary;
            // state contains word 4 from the reserved memory bank. The first hex character 
            // defines the kill and access lock status while the second hex character defines the
            // epc and user lock status. This only works with Monza 4 & 5 tags. 

            // Set Kill and Access Status
            if (string.IsNullOrEmpty(state) == false)
            {
                strBinary = Convert.ToString(Convert.ToInt32(state.Substring(0, 1), 16), 2).PadLeft(4, '0');
                switch (strBinary.Substring(0, 2))
                {
                    case "10":
                        results.kill_lock = TagLockState.Lock;
                        break;
                    case "11":
                        results.kill_lock = TagLockState.Permalock;
                        break;
                    case "01":
                        results.kill_lock = TagLockState.Permaunlock;
                        break;
                    default:
                        results.kill_lock = TagLockState.None;
                        break;
                }
                switch (strBinary.Substring(2, 2))
                {
                    case "10":
                        results.access_lock = TagLockState.Lock;
                        break;
                    case "11":
                        results.access_lock = TagLockState.Permalock;
                        break;
                    case "01":
                        results.access_lock = TagLockState.Permaunlock;
                        break;
                    default:
                        results.access_lock = TagLockState.None;
                        break;
                }
                // Set EPC and USER Status
                strBinary = Convert.ToString(Convert.ToInt32(state.Substring(1, 1), 16), 2).PadLeft(4, '0');
                switch (strBinary.Substring(0, 2))
                {
                    case "10":
                        results.epc_lock = TagLockState.Lock;
                        break;
                    case "11":
                        results.epc_lock = TagLockState.Permalock;
                        break;
                    case "01":
                        results.epc_lock = TagLockState.Permaunlock;
                        break;
                    default:
                        results.epc_lock = TagLockState.None;
                        break;
                }
                switch (strBinary.Substring(2, 2))
                {
                    case "10":
                        results.user_lock = TagLockState.Lock;
                        break;
                    case "11":
                        results.user_lock = TagLockState.Permalock;
                        break;
                    case "01":
                        results.user_lock = TagLockState.Permaunlock;
                        break;
                    default:
                        results.user_lock = TagLockState.None;
                        break;
                }
            }
            else
            {
                results.user_lock = TagLockState.None;
                results.epc_lock = TagLockState.None;
                results.access_lock = TagLockState.None;
                results.kill_lock = TagLockState.None;
            }
            return results;
        }
    }

}


