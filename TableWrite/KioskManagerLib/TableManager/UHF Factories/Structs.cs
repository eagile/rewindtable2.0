﻿using EA.Impinj;
using EAManagerLib.Database.Models;
using System;

namespace SupportClasses
{
    //Operation success/fail booleans are nullable so that false isn't default
    public class TagInfo
    {
        public string tid;
        public string epc;
        public DateTime IndexTimeStamp;
        public uint writeSequenceId;
        public DateTime writeReportTimeStamp;
        public bool? writeSuccess;
        public uint verifySequenceId;
        public bool? verifySuccess;
        public bool verifyEPC;
        public bool verifyLock;
        public bool verifyUser;
        public DateTime verifyReportTimeStamp;
        public object dataentry;
        public ushort PcBits;
        public string OrigEPC; 
        public int Scalar;

        public static explicit operator TagInfo(RecoveryData v)
        {
            throw new NotImplementedException();
        }
    }

    public class DataBaseEntry
    {
        public string epc;
        public TagLockState epc_lock;
        public string user;
        public TagLockState user_lock;
        public string accesspw;
        public TagLockState access_lock;
        public string killpw;
        public TagLockState kill_lock;
        public string hf_user;
        public int offset;
        public string hf_static_lock;
        public string hf_dynamic_lock;
        public string nfc_wshare;
        public string uhf_wshare;
        public ushort PcBits;
        public string Print1;
        public string Print2;
        public string Print3;
        public string Print4;
        public string Print5;
    }


}
