﻿using System.ComponentModel;
using EA.Impinj;
using SupportClasses;

namespace WriterFactories
{
    [Description("Amway ThingID implmentation for em4425")]
    public class ThingID4425TagOps : VirtualOpsBase
    {
        public override double TagPitch { get; set; }
        public override double TagGap { get; set; }
        public override ushort writeAntenna { get; set; }
        public override ReaderMode rfMode { get; set; } = ReaderMode.MaxMiller;
        public override short rxFilterRead { get; set; } = -45;
        public override short rxFilterWrite { get; set; } = -40;

        public override SearchMode SearchMode { get; set; } = SearchMode.DualTarget;
        public override Session Session { get; set; } = Session.S3;
        public override double SFM { get; set; } = 12;

        public override TagOpSequence BuildWriteOps(TagInfo tag, string tid, DataBaseEntry dataentry)
        {
            TagOpSequence tagOpSequence = new TagOpSequence()
            {
                AntennaId = writeAntenna,
                TargetTag = new TargetTag()
                {
                    MemoryBank = MemoryBank.Tid,
                    BitPointer = 0,
                    Data = tid,
                },
                BlockWriteEnabled = true,
                BlockWriteWordCount = 2,
                RetryCount = 2,
            };
            TagWriteOp epcOP = new TagWriteOp()
            {
                Id = 111,
                MemoryBank = MemoryBank.Epc,
                Data = TagData.FromHexString(dataentry.epc),
                WordPointer = WordPointers.Epc
            };
            tagOpSequence.Ops.Add(epcOP);

            TagWriteOp userOp = new TagWriteOp()
            {
                Id = 112,
                MemoryBank = MemoryBank.User,
                Data = TagData.FromHexString($"E1431309{dataentry.hf_user}"),
                WordPointer = (ushort)(dataentry.offset)
            };
            tagOpSequence.Ops.Add(userOp);

            ushort[] myblocks = {8, 9, 10, 11, 12, 13, 14, 15};

            TagBlockPermalockOp bplop = new TagBlockPermalockOp()
            {
                MemoryBank = MemoryBank.User,
                BlockPointer = 2,
                BlockMask = BlockPermalockMask.FromBlockNumberArray(myblocks)
            };

            tagOpSequence.Ops.Add(bplop);

            //TODO: this is currently ignoring all passwords, and hard locking. Anything that would prevent the same tags from being used in testing again. 
            TagLockOp lop = new TagLockOp()
            {
                AccessPassword = dataentry.accesspw.Equals("?") ? null : TagData.FromHexString(dataentry.accesspw),
                AccessPasswordLockType = dataentry.access_lock,
                EpcLockType = dataentry.epc_lock,
                KillPasswordLockType = dataentry.kill_lock,
                UserLockType = dataentry.user_lock
            };
            tagOpSequence.Ops.Add(lop);

            return tagOpSequence;
        }

        public override TagOpSequence BuildReadOps(string tid, DataBaseEntry dataentry)
        {
            TagOpSequence tagOpSequence = new TagOpSequence()
            {
                TargetTag = new TargetTag()
                {
                    MemoryBank = MemoryBank.Tid,
                    BitPointer = 0,
                    Data = tid,
                },
                RetryCount = 2,
            };

            TagReadOp readReserved = new TagReadOp();
            readReserved.Id = 113;
            readReserved.MemoryBank = MemoryBank.User;
            readReserved.WordPointer = (ushort)(dataentry.offset+2);
            readReserved.WordCount = 16; //lock bits

            tagOpSequence.Ops.Add(readReserved);

            return tagOpSequence;
        }

        public override TagInfo TagOpProcess(Tag tag, TagInfo ti, DataBaseEntry resultsdata, DataBaseEntry dataentry)
        {
            foreach (TagOpResult result in tag.TagOpResults)
            {
                if (result is TagReadOpResult)
                {
                    TagReadOpResult readresult = result as TagReadOpResult;
                    switch (result.OpId)
                    {
                        case 112:
                            //resultsdata = DetermineLockState(resultsdata, readresult.Data.ToHexString());
                            break;
                        case 111:
                            resultsdata.user = readresult.Data.ToHexString();
                            break;
                        case 113:
                            resultsdata.user = readresult.Data.ToHexString();
                            break;
                        default:
                            break;
                    }
                }
            }

            if (dataentry.epc == resultsdata.epc)
                ti.verifyEPC = true;

            ti.verifyLock = true;

            //if (dataentry.user.Equals(resultsdata.user))
            if (resultsdata.user.Length > 12)
            {
                if (dataentry.hf_user.Equals(resultsdata.user))
                    ti.verifyUser = true;
            }
            if (ti.verifyEPC && ti.verifyLock && ti.verifyUser)
                ti.verifySuccess = true;

            return ti;
        }
    }

}


