﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

using SupportClasses;
using EA.Impinj;

namespace WriterFactories
{
    public class WriterTypeFactory
    {
        internal static Dictionary<int, Type> factoryData = new Dictionary<int, Type>
        {
            { 0, typeof(BaseTagOps) },
            { 1, typeof(ThingID4423TagOps) },
            { 2, typeof(ThingID4425TagOps) },
            //{ 2, typeof(BarcodeTagOps) }
        };

        /// <summary>Returns the System.Type for the specified int
        /// <returns>The System.Type of the class using the specified key value - throws out of range exception if an incorrect value is specified.
        public static Type GetType(int e)
        {
            return factoryData[e];
        }
        public static int Count
        { get { return factoryData.Count; } }

        // <summary>This uses the metadata to allow programs to pull information about the class managed by the TypeFactory
        // <returns>a string describing the underlying logic of the class type.
        public static string TypeDescription(int e)
        {
            var descriptions = factoryData[e].GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (descriptions.Length == 0)
                return "";
            return ((DescriptionAttribute)descriptions[0]).Description;
        }
    }

    public abstract class VirtualOpsBase
    {
        //public virtual List<TagOperation> DBTagOps { get; set; }
        public abstract double TagPitch { get; set; }
        public abstract double TagGap { get; set; }
        public abstract ushort writeAntenna { get; set; }
        public abstract ReaderMode rfMode { get; set; }
        public abstract SearchMode SearchMode { get; set; }
        public abstract Session Session { get; set; }
        public abstract short rxFilterRead { get; set; }
        public abstract short rxFilterWrite { get; set; }
        public abstract double SFM { get; set; }
        public abstract TagOpSequence BuildReadOps(string tid, DataBaseEntry dataentry);
        public abstract TagOpSequence BuildWriteOps(TagInfo tag, string tid, DataBaseEntry dataentry);
        public abstract TagInfo TagOpProcess(Tag tag, TagInfo ti, DataBaseEntry resultsdata, DataBaseEntry dataentry);
    }
}


