﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.Specialized;

namespace SupportClasses
{
    //<summary>Regulary dictionary annoyingly cannot be subscribed to thus making using it in the Model to inform the View doesn't work well.
    public class ObservableDictionary<String, Object> : Dictionary<String, Object>, INotifyPropertyChanged, INotifyCollectionChanged
    {

        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        public event NotifyCollectionChangedEventHandler CollectionChanged = delegate { };

        private void OnCollectionChanged(object sender, NotifyCollectionChangedAction action, object value)
        {
            CollectionChanged?.Invoke(sender, new NotifyCollectionChangedEventArgs(action, value));
        }

        private void OnPropertyChanged(object sender, string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public new void Add(String key, Object value)
        {
            base.Add(key, value);
            OnCollectionChanged(this, NotifyCollectionChangedAction.Add, new KeyValuePair<String, Object>(key, value));
        }

        public new bool Remove(String key)  
        {
            var kvp = base[key];
            var result = base.Remove(key);
            if (result)
            {
                OnCollectionChanged(this, NotifyCollectionChangedAction.Remove, kvp);
            }
            return result;
        }

        public new Object this[String key]
        {
            get
            {
                return base[key];
            }
            set
            {
                base[key] = value;
                PropertyChanged(this, new PropertyChangedEventArgs(key.ToString()));
            }
        }
    }

}