﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TableManager
{
    public struct lrLine
    {
        public float slope;
        public float intercept;
    };
    public class LinearRegression
    {
        public static lrLine Calculate(float[] data, float[] xdata)
        {
            lrLine results = new lrLine();
            float xsum = 0;
            float ysum = 0;
            float xysum = 0, xsquaresum = 0, ysquaresum = 0;

            for (int i = 0; i < data.Length; i++)
            {
                xsum += xdata[i];
                xsquaresum += (xdata[i] * xdata[i]);
                ysum += data[i];
                ysquaresum += (data[i] * data[i]);
                xysum += xdata[i] * data[i];
            }
            float denom = data.Length * xsquaresum - (xsum * xsum);
            results.slope = (data.Length * xysum - xsum * ysum) / denom;
            results.intercept = (xsquaresum * ysum - xsum * xysum) / denom;

            return results;
        }
    }
}
