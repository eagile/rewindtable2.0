﻿using System;
using System.IO;
using System.Threading;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using WriterFactories;
using SupportClasses;
using NLog;
using EA.Impinj;

namespace BuildTidList
{
    public class TidListBuilder
    {
        const int DATABASESIZE = 20000;
        // List that maintains the sequence of found tags in case we need later verification
        public List<string> TidList { get; set; } = new List<string>(DATABASESIZE);

        public object TagLock { get; set; }
        public Queue<TagInfo> WriteQueue { get; set; }
        public Queue<TagInfo> VerifyQueue { get; set; }
        public TagInfo NewTag { get; set; } = null;
        Logger Logger = LogManager.GetCurrentClassLogger();
        //Settings settings;

        public ushort Antenna { get; set; }
        public Dictionary<string, TagInfo> MasterList { get; set; } = new Dictionary<string, TagInfo>(DATABASESIZE);
        public VirtualOpsBase TagType { get; set; }

        private const string STR_IPADDRESS_FILE = @"C:\ImpinjAddress.txt"; // File location of Impinj reader IP address file.

        public ImpinjReader reader { get; set; }
        public Queue<object> database { get; set; } = new Queue<object>(DATABASESIZE);
        

        ~TidListBuilder()
        {
            if (reader.IsConnected)
            {
                // Reader shold be stopped and closed before destructor so we don't hve to use Wait()
                try { reader.StopAsync().Wait(); } catch { }
                try { reader.DisconnectAsync().Wait(); } catch { }
            }
        }

        public async Task StartWorker(CancellationToken ct)
        {
            reader.TagsReported += TagReport;
            await reader.StartAsync().ConfigureAwait(false);

            //LoadDatabase(datasource);
            CancelationLoop(ct);

            reader.TagsReported -= TagReport;
            try { reader.StopAsync().Wait(); } catch { }
            try { reader.DisconnectAsync().Wait(); } catch { }
        }

        void CancelationLoop(CancellationToken ct)
        {
            while (!ct.IsCancellationRequested)
                Thread.Sleep(1);
/*
            // Simple log of the TID sequence as detected at Read Antenna
            using (StreamWriter sw = new StreamWriter("C:\\eAgile\\ReadTags.txt"))
            {
                //Full Log of TagInfo in sequence
                using (StreamWriter log = new StreamWriter("C:\\eAgile\\FullLog.txt"))
                {
                    foreach (string tid in TidList)
                    {
                        sw.WriteLine($"{tid}");
                        if (MasterList.ContainsKey(tid))
                        {
                            TagInfo ti = MasterList[tid];
                            log.WriteLine($"{ti.tid}|{ti.epc}|{ti.foundTimeStamp}|{ti.writeSequenceId}|{ti.writeSuccess}|{ti.writeReportTimeStamp}|{ti.verifySequenceId}|{ti.verifySuccess}|{ti.verifyEPC}|{ti.verifyLock}|{ti.verifyUser}|{ti.verifyReportTimeStamp}");
                        }
                        else
                            log.WriteLine($"{tid}|TID Not Found in MasterList");
                    }
                }
            }
*/
        }

        private void TagReport(object sender, TagReport e)
        {
            foreach (Tag tag in e.Tags)
            {
                if (tag.AntennaPortNumber == Antenna)
                {
                    string tid = tag.Tid.ToHexString();
                    if (!MasterList.ContainsKey(tid))
                    {
                        if (database.Count > 0)
                        {
                            TagInfo ti = new TagInfo();
                            ti.tid = tid;
                            ti.epc = tag.Epc.ToHexString();
                            ti.foundTimeStamp = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
                            ti.PcBits = tag.PcBits;
                         //   ti.dataentry = database.Dequeue();
                            try
                            {
                                // We want to maintain a structured list of the TID order for later verification
                                lock (TagLock)
                                {
                                    NewTag = ti;

                                    AddTag(tid, ti);
                                    WriteQueue.Enqueue(ti);
                                }
                            }
                            catch (Exception ex)
                            { //TODO: handle report up to stop job.}
                                Logger.Error(ex);
                            }
                        }
                    }
                }
            }
        }

        public void AddTag(string tid, TagInfo ti)
        {
            TidList.Add(tid);
            if (!tid.Equals("DEADTAG"))
            {
                MasterList.Add(tid, ti);
                Logger.Info("New Added: {TID}, {TIMESTAMP}",tid, ti.foundTimeStamp);
            }
            else Logger.Info($"Dead Tag Added: {tid}");
        }
    }
}

//void LoadDatabase(string FileName)
//{
//    if (FileName != string.Empty)
//    {
//        Logger.Info($"TidCOnsumer: Using database {FileName}");
//        using (StreamReader sr = new StreamReader(FileName))
//        {
//            string strLine;
//            string[] strValues;

//            strLine = sr.ReadLine(); // Header line

//            while ((strLine = sr.ReadLine()) != null)
//            {
//                strValues = strLine.Split('|');

//                if (strValues[0] != "?") // Skip SPLICE tags
//                {
//                    DataBaseEntry dataentry = new DataBaseEntry()
//                    {
//                        epc = strValues[0],
//                        epc_lock = strValues[1].Equals("PERMALOCK") ? TagLockState.Permalock : (strValues[1].Equals("?") ? TagLockState.None : TagLockState.Lock),
//                        user = strValues[2],
//                        user_lock = strValues[3].Equals("PERMALOCK") ? TagLockState.Permalock : (strValues[3].Equals("?") ? TagLockState.None : TagLockState.Lock),
//                        accesspw = strValues[4],
//                        access_lock = strValues[5].Equals("PERMALOCK") ? TagLockState.Permalock : (strValues[5].Equals("?") ? TagLockState.None : TagLockState.Lock),
//                        killpw = strValues[6],
//                        kill_lock = strValues[7].Equals("PERMALOCK") ? TagLockState.Permalock : (strValues[7].Equals("?") ? TagLockState.None : TagLockState.Lock),
//                    };

//                    if (strValues.Length >= 14)
//                    {
//                        dataentry.hf_user = strValues[8];
//                        dataentry.offset = int.Parse(strValues[9]);
//                        dataentry.hf_static_lock = strValues[10];
//                        dataentry.hf_dynamic_lock = strValues[11];
//                        dataentry.nfc_wshare = strValues[12];
//                        dataentry.uhf_wshare = strValues[13];
//                    }

//                    builder.database.Enqueue(dataentry);
//                }
//            }

//        }
//    }
//    else
//        Logger.Error($"TidConsumer: No Encoding Database Found");
//}
