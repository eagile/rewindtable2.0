﻿// ***********************************************************************
// Assembly         : LineManagerLib
// Author           : eAgile
// Created          : 01-28-2020
//
// Last Modified By : eAgile
// Last Modified On : 04-30-2020
// ***********************************************************************
// <copyright file="UserLogin.cs" company="LineManagerLib">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

using EA.Helpers;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace KioskManagerLib
{
    /// <summary>
    /// Class UserLogin.
    /// Implements the <see cref="EA.Helpers.ObservableObject" />
    /// </summary>
    /// <seealso cref="EA.Helpers.ObservableObject" />
    public class UserLogin : ObservableObject
    {
        /// <summary>
        /// Gets or sets a value indicating whether this instance is logged in.
        /// </summary>
        /// <value><c>true</c> if this instance is logged in; otherwise, <c>false</c>.</value>
        public bool IsLoggedIn { get => _isLoggedIn; set => SetProperty(ref _isLoggedIn, value, nameof(IsLoggedIn), nameof(IsNotLoggedIn)); }
        /// <summary>
        /// The is logged in
        /// </summary>
        private bool _isLoggedIn;
        /// <summary>
        /// Gets a value indicating whether this instance is not logged in.
        /// </summary>
        /// <value><c>true</c> if this instance is not logged in; otherwise, <c>false</c>.</value>
        public bool IsNotLoggedIn => !IsLoggedIn;

        /// <summary>
        /// Gets or sets the username.
        /// </summary>
        /// <value>The username.</value>
        public string Username { get => _username; set => SetProperty(ref _username, value); }
        /// <summary>
        /// The username
        /// </summary>
        private string _username;

        public ObservableCollection<KioskRight> KioskRights { get; } = new ObservableCollection<KioskRight>();
        public object KioskRightsSyncRoot { get; } = new object();



        public bool HasRight(KioskRight right)
        {
            lock (KioskRightsSyncRoot)
            {
                return KioskRights.Contains(right);
            }
        }

        /// <summary>
        /// Resets this instance.
        /// </summary>
        public void Reset()
        {
            IsLoggedIn = false;
            Username = null;
            lock (KioskRightsSyncRoot)
            {
                KioskRights.Clear();
            }
        }

        /// <summary>
        /// Copies to.
        /// </summary>
        /// <param name="userLogin">The user login.</param>
        public void CopyTo(UserLogin userLogin)
        {
            userLogin.IsLoggedIn = IsLoggedIn;
            userLogin.Username = Username;

            List<KioskRight> kioskRights;
            lock (KioskRightsSyncRoot)
            {
                kioskRights = new List<KioskRight>(KioskRights);
            }

            lock (userLogin.KioskRightsSyncRoot)
            {
                userLogin.KioskRights.Clear();
                foreach (KioskRight kioskRight in kioskRights)
                {
                    userLogin.KioskRights.Add(kioskRight);
                }
            }
        }
    }
}
