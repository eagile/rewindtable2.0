﻿using System;

namespace KioskManagerLib
{
    public enum KioskRight
    {
        mayReadRFID,
        mayViewSettings,
        mayEditSettings,
        mayEditUser,
        mayManageDatabase,
        mayViewLogs,
        mayExportLogs,
    }
}
