﻿using System;

namespace KioskManagerLib
{
    public class DatabaseException : Exception
    {
        public DatabaseException(Exception innerException) : base(null, innerException) { }
    }

    public class InvalidUsernameOrPasswordException : Exception
    {
        public InvalidUsernameOrPasswordException() : base() { }
    }

    public class UserLockedException : Exception
    {
        public UserLockedException() : base() { }
    }

    public class UserRightsException : Exception
    {
        public UserRightsException() : base() { }
    }

    public class UserExpiredException : Exception
    {
        public UserExpiredException() : base() { }
    }
}
