﻿namespace KioskManagerLib
{
    using System.IO.Compression;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.IO;
    using KioskManagerLib.Configuration;
    using KioskManagerLib.Models;
    using KioskManagerLib.Database;
    using System.Collections.ObjectModel;
    using System.Threading.Tasks;
    using System.Threading;

    using KioskManager.Events;
    using System.Globalization;

    using KioskManagerLib.Database.Models;
    using Microsoft.Extensions.Configuration;
    using NLog.Extensions.Logging;
    using NLog;
    using Microsoft.Extensions.Configuration.Json;
    using Microsoft.Extensions.FileProviders;
    using System.Threading.Channels;
    using System.Data;

    using EA.Helpers;
   // using BuildTidList;
  //  using TableManager;
    using SupportClasses;
    using WriterFactories;
    using RewindDriver;

    using KioskManagerLib.Database.DataContexts;
    using EA.Impinj;
    using EA.Impinj.LLRP;
    using global::TableManager;
    using KioskManagerLib.TableManager.UHF_Factories;
    using System.Reflection;
    using System.ComponentModel;
    using System.Text;

    using EABarCodeSDK;
    using EAReaderSDK.RFIDDevices;
    using EAManagerLib.Cultures;
    using EAManagerLib.Configuration.Kiosk;
    using System.IO.Ports;
    using EAManagerLib.TableManager.UHF_Factories;
    using EATags;

    public sealed class TableController : ObservableObject, IDisposable
    {
        private static TableController _instance = null;
        private static readonly object _instanceLock = new object();
        public static TableController Instance
        {
            get
            {
                lock (_instanceLock)
                {
                    if (_instance == null)
                    {
                        _instance = new TableController();
                    }
                    return _instance;
                }
            }
        }

        public string BarcodeResult { get => _barcodeResult; set => SetProperty(ref _barcodeResult, value); }
        private string _barcodeResult = "20-01518_001";
        
        public int ProductQty { get => _productQty; set => SetProperty(ref _productQty, value); }
        private int _productQty = 0;


        public int IndexerOverAll_Total { get => _indexerOverAll_Total; set => SetProperty(ref _indexerOverAll_Total, value); }
        private int _indexerOverAll_Total = 0;

        public int IndexerOverAll_Success { get => _indexerOverAll_Success; set => SetProperty(ref _indexerOverAll_Success, value); }
        private int _indexerOverAll_Success = 0;
        public int IndexerOverAll_Fail { get => _indexerOverAll_Fail; set => SetProperty(ref _indexerOverAll_Fail, value); }
        private int _indexerOverAll_Fail = 0;
        public double IndexerOverAll_Percent { get => _indexerOverAll_Percent; set => SetProperty(ref _indexerOverAll_Percent, value); }
        private double _indexerOverAll_Percent = 0;
        //
        public int WriterOverAll_Total { get => _writerOverAll_Total; set => SetProperty(ref _writerOverAll_Total, value); }
        private int _writerOverAll_Total = 0;

        public int WriterOverAll_Success { get => _writerOverAll_Success; set => SetProperty(ref _writerOverAll_Success, value); }
        private int _writerOverAll_Success = 0;
        public int WriterOverAll_Fail { get => _writerOverAll_Fail; set => SetProperty(ref _writerOverAll_Fail, value); }
        private int _writerOverAll_Fail = 0;
        public double WriterOverAll_Percent { get => _writerOverAll_Percent; set => SetProperty(ref _writerOverAll_Percent, value); }
        private double _writerOverAll_Percent = 0;
        //
        public int ValidatorOverAll_Total { get => _validatorOverAll_Total; set => SetProperty(ref _validatorOverAll_Total, value); }
        private int _validatorOverAll_Total = 0;

        public int ValidatorOverAll_Success { get => _validatorOverAll_Success; set => SetProperty(ref _validatorOverAll_Success, value); }
        private int _validatorOverAll_Success = 0;
        public int ValidatorOverAll_Fail { get => _validatorOverAll_Fail; set => SetProperty(ref _validatorOverAll_Fail, value); }
        private int _validatorOverAll_Fail = 0;
        public double ValidatorOverAll_Percent { get => _validatorOverAll_Percent; set => SetProperty(ref _validatorOverAll_Percent, value); }
        private double _validatorOverAll_Percent = 0;

        ProductionPartWrapper ProductionResults { get; set; }

        #region Main Reader objects

        public ObservableDictionary<string, TagInfo> MasterList = new ObservableDictionary<string, TagInfo>();
        public Queue<object> Database { get; set; } = new Queue<object>(20000);
        public ushort MasterAntenna { get; set; } = 1;
        public List<string> TidList { get; set; } = new List<string>(20000);
        
        readonly IProgress<TagInfo> TagCollectionProgress;
        public OrderedHashSet<TagInfo> TagCollection { get; set; } = new OrderedHashSet<TagInfo>();
        public int TagType { get; set; } = 0;
        public object TagLock { get; } = new object();
        public object RecoverLock { get; } = new object();
        #endregion

        #region Write Reader objects
        public Queue<TagInfo> WriteQueue { get; set; } = new Queue<TagInfo>();

        #endregion

        #region Verify Reader objects
        public VirtualOpsBase VOBTagType { get; set; }

        private static readonly NLog.Logger ResultsLogger = NLog.LogManager.GetLogger("ResultsFile");

        static List<TagInfo> results = new List<TagInfo>();

        #endregion 

        MotorDriver OutSpoolMotor;
        MotorCallBackFunction portCallBack;


        public bool CanStart { get; set; } = false; //Can the class be started
        public int scalar { get; set; } = (1333); // With current pulley setup 1 R = 3" with a standard core;
        public int scalar2 { get; set; } = 400; // Somewhat arbitrary, calibration will correct this.
        public bool calibrate = false;

        Logger Logger = LogManager.GetCurrentClassLogger();

        // These are the worker classes that exist on threads
        List<Task> tasklist = new List<Task>();
      //  TidListBuilder builder;
    //    TidConsumer consumer;
     //   ContentVerifier verifier;
        EncoderMonitor encoder;

        // Reader object for each worker that interacts with the Tag data
        //ImpinjReader impinjMainReader;
        public readonly RFIDReader MainReader;
        //ImpinjReader impinjWriter;
        public readonly RFIDReader WriterReader;
        //ImpinjReader impinjVerifyReader;
        public readonly RFIDReader VerifyReader;

        public BarCodeReader BarCodeReader { get; }

        //Queue<TagInfo> writeQueue = new Queue<TagInfo>();
        Queue<TagInfo> VerifyQueue = new Queue<TagInfo>();
        Queue<double> MoveQueue = new Queue<double>();
        Queue<double> MoveData = new Queue<double>(10);

        //Uses the WriterTypeFactory to generate an instance of the appropriate class based on configuration.
        VirtualOpsBase tagOps;

        CancellationToken cancellationToken;

        UInt32 _movecount = 0;
        int _initialTags = 0;
        int _lasttagcount = 0;


        private readonly ILogger _logger;

        public EventLogger Log { get; } = new EventLogger();

        public bool IsLoaded { get; private set; } = false;

        /// <summary>
        /// Gets the selected culture.
        /// </summary>
        /// <value>The selected culture.</value>
        public CultureInfo SelectedCulture { get; private set; }

        /// <summary>
        /// Gets the parsed settings.
        /// </summary>
        /// <value>The parsed settings.</value>
        public ParseSettingsResult ParsedSettings { get; } = new ParseSettingsResult();

        public KioskManagerSettings CurrentSettings { get; set; } = null;
        
        public TableManagerDAL DAL { get; }
        public RemoteTableManagerDAL RemoteDAL { get; set; }

        public ProductionDAL ProductionDAL { get; set; } = new ProductionDAL();

        public ObservableCollection<Notification> Alarms { get; } = new ObservableCollection<Notification>();
        public object AlarmsSyncRoot { get; } = new object();
        public object TagsSyncRoot { get; } = new object();
        //public Notification InfoTip { get; set; }
        public Notification CurrentMessage { get; set; }

        //private const string ClientSharedSecret = "8bGToMI7xUt2H1d";
        //public AppsettingModel ConnectionItems { get; set; } = new AppsettingModel();

        //public string ActiveSKU { get; set; } = string.Empty;
        //private string CurrentThingID { get; set; } = string.Empty;
        //public DailyLogger KioskLogger { get; set; }

        //public KioskManagerSettings Kiosksettings { get; set; } = new KioskManagerSettings();
        //public AWSConnectionModel KioskConnectionSettings { get; set; }
        //private NewErrorModel NewErrorItem { get; set; }
        //private SingleSkuAssociationModel SingleSKUAdd { get; set; }
        //public FilePathSettingModel NewfilePathSettings { get; set; }
        //public SkusModel CurrentSkusList { get; set; }
        //public TotalSkuAssociationModel CurrentAssociatedSkus { get; set; } = new TotalSkuAssociationModel();

        public DateTime LastReadTime { get; set; }

        public bool TopMost { get; set; } = true;
        public string MotorPort { get; set; } = "COM7";

        public SerialPort CurrentSerialPort { get; set; }



        /// <summary>
        /// The CTS
        /// </summary>
        private CancellationTokenSource _cts = new CancellationTokenSource();
        /// <summary>
        /// Gets the ct.
        /// </summary>
        /// <value>The ct.</value>
        private CancellationToken _ct => _cts?.Token ?? CancellationToken.None;

        /// <summary>
        /// Gets the current user.
        /// </summary>
        /// <value>The current user.</value>
        public UserLogin CurrentUser { get; } = new UserLogin();

        public double CurrentDatabaseSizeGB { get; set; } = -1;

        //private System.Timers.Timer PayloadTimer;

      //  public readonly KioskReader RfidReader;

        public ObservableCollection<KioskScannedItem> Tags { get; } = new ObservableCollection<KioskScannedItem>();
        public ObservableCollection<MasterModel> CurrentLogList { get; set; } = new ObservableCollection<MasterModel>();
        public object CurrentLogListSyncRoot { get; } = new object();

        private Channel<Logs> _logChannel = Channel.CreateUnbounded<Logs>(new UnboundedChannelOptions() { AllowSynchronousContinuations = false, SingleReader = true, SingleWriter = false });
        private Task _logChannelTask = Task.CompletedTask;

        private readonly SemaphoreSlim _loadControllerLock = new SemaphoreSlim(1, 1);


        private System.Timers.Timer _idleTimer = new System.Timers.Timer();
        private string SavedconnectionString { get; set; }

        private TableController()
        {
            try
            {
                // Use the built-in configuration provider
                // First it will check for a local appsettings.json file
                // If ExternalAppSettingsPath is set then it will load that path too
                // Any settings in ExternalAppSettingsPath will override those in appsettings.json
                // appsettings.json settings will be used when they don't exist in ExternalAppSettingsPath
                // ExternalAppSettingsPath can be a copy of appsettings.json and then change the settings
                const string appsettings = "appsettings.json";

                IConfigurationRoot configuration = new ConfigurationBuilder()
                        .AddJsonFile(appsettings, optional: true)
                        .Build();

                string externalConfigPath = configuration["ExternalAppSettingsPath"];
                if (!string.IsNullOrWhiteSpace(externalConfigPath) && File.Exists(externalConfigPath))
                {
                    configuration = new ConfigurationBuilder()
                     .AddJsonFile(appsettings, optional: true)
                     .AddJsonFile(externalConfigPath, optional: true)
                     .Build();
                }

                LogManager.Configuration = new NLogLoggingConfiguration(configuration.GetSection("NLog"));
                _logger = LogManager.GetCurrentClassLogger();

                Log.LogEvent += Log_LogEvent;

                Log.Debug(FkEventCode.ConfigurationFilesLoaded, string.Join(',', configuration.Providers.OfType<JsonConfigurationProvider>().Select(p => (p.Source.FileProvider as PhysicalFileProvider)?.Root + p.Source.Path)));

                string kioskId = configuration["KioskID"];
                string connectionString = configuration.GetConnectionString("Kiosk");
                SavedconnectionString = connectionString;
                
                TableContext.ConnectionString = connectionString;

                DAL = new TableManagerDAL(kioskId);


                if (!configuration["TopMost"].ToUpper().Contains("TRUE"))
                {
                    TopMost = false;
                }

                TopMost = false;

                _idleTimer.Interval = 1000;
                _idleTimer.AutoReset = false;
                _idleTimer.Elapsed += IdleTimer_Elapsed;
                _idleTimer.Start();

                // Setup the Encoder module
                SetupEncoder();
                ConfigureMotor(configuration);
                // Generate the delagate for the sFoundation code to callback on events.
                //portCallback = new Port.CallbackFunction(PortAttnHandler);
                portCallBack = new MotorCallBackFunction(PortAttnHandler);
                OutSpoolMotor.setCallback(portCallBack);

                var remoteconnectionString = "Server = EA-SQLDEV; Database = TableEncoder; Integrated Security = True";
                ProductionContext.ConnectionString = remoteconnectionString;
                // Setup The ImpinjReaders
                // Todo Abstract the readers for UHF/HF
                // We connect here but the individual classes setup the reader to their liking.
                CanStart = true;

                BarCodeReader = new BarCodeReader();
                BarCodeReader.BarCodeEvent += BarCodeReader_BarCodeEvent;

                MainReader = new RFIDReader();
                MainReader.TagsReported += MainReader_TagsReported;
                
                WriterReader = new RFIDReader();
                WriterReader.TagsReported += WriterReader_TagsReported;
    
                VerifyReader = new RFIDReader();
                VerifyReader.TagsReported += VerifyReader_TagsReported;

                TagCollectionProgress = new Progress<TagInfo>(p => TagCollection.Add(p));
                BarCodeReader.OpenBarcodeReader();

            }
            catch (Exception ex)
            {
                // TODO: Error Handling
                string s = ex.Message;
            }

        }

        private void ConfigureMotor(IConfigurationRoot configuration)
        {
            try
            {
                OutSpoolMotor = new MotorDriver();

                MotorPort = configuration["MotorPort"];

                Parity tmpParity = Parity.None;
                if (configuration["MotorParity"].ToUpper().Contains("NONE"))
                {
                    tmpParity = Parity.None;
                }

                bool tmpDtrEnable;
                if (!configuration["MotorDtrEnable"].ToUpper().Contains("TRUE"))
                {
                    tmpDtrEnable = false;
                }
                else
                {
                    tmpDtrEnable = true;
                }

                bool tmpRtsEnable;
                if (!configuration["MotorRtsEnable"].ToUpper().Contains("TRUE"))
                {
                    tmpRtsEnable = false;
                }
                else
                {
                    tmpRtsEnable = true;
                }


                CurrentSerialPort = new SerialPort(MotorPort)
                {
                    BaudRate = Convert.ToInt32(configuration["MotorBaudRate"]),
                    Parity = tmpParity,
                    DtrEnable = tmpDtrEnable,
                    RtsEnable = tmpRtsEnable,
                    WriteBufferSize = Convert.ToInt32(configuration["MotorWriteBufferSize"]),
                };
                OutSpoolMotor.Open(CurrentSerialPort);  
            }
            catch
            {
                Logger.Error("Error intializing Motor");
            }

        }

        public async Task Start()
        {
            CanStart = false;
            


            await MainReader.StartAsync().ConfigureAwait(false);
            await WriterReader.StartAsync().ConfigureAwait(false);
            await VerifyReader.StartAsync().ConfigureAwait(false);

            tasklist.Add(Task.Run(() => MoveLoop(cancellationToken)));
            tasklist.Add(Task.Run(() => encoder.SerialPortLoop(cancellationToken)));
            
          //  tasklist.Add(Task.Run(() => WriterLoop(cancellationToken)));
          // tasklist.Add(Task.Run(() => VerifyLoop(cancellationToken)));
        }

        private void BarCodeReader_BarCodeEvent(object sender, string e)
        {
            if (!string.IsNullOrWhiteSpace(e))
            {
                BarcodeResult = e;
            }
        }

        private void MainReader_TagsReported(object sender, TagReport e)
        {
            foreach (Tag tag in e.Tags)
            {
                if (tag.AntennaPortNumber == MasterAntenna)
                {
                    string tid = tag.Tid.ToHexString();
                    if (!MasterList.ContainsKey(tid))
                    {
                        if (Database.Count > 0)
                        {
                            var d_que =  Database.Dequeue();
                            TagInfo ti = new TagInfo();
                            ti.tid = tid;
                            ti.epc = d_que.GetType().GetProperty("EPC").GetValue(d_que, null).ToString();
                            ti.IndexTimeStamp = DateTime.Now;
                            ti.PcBits = tag.PcBits;
                            ti.dataentry = d_que;
                            ti.OrigEPC = tag.Epc.ToHexString();
                            ti.writeSuccess = false;
                            ti.verifySuccess = false;
                            
                            try
                            {
                                // We want to maintain a structured list of the TID order for later verification
                                
                                    
                                lock (TagLock)
                                {
                                    _ = AddTag(tid, tag, ti);

                                    IndexerOverAll_Success += 1;
                                    IndexerOverAll_Total = IndexerOverAll_Success + IndexerOverAll_Fail;
                                    if (IndexerOverAll_Total >0)
                                    {
                                        IndexerOverAll_Percent = IndexerOverAll_Success / IndexerOverAll_Total * 100;
                                    }
                                    
                                }
                              _ =  WriteRecoveryData(ti);
                                
                            }
                            catch (Exception ex)
                            { //TODO: handle report up to stop job.}
                                IndexerOverAll_Fail += 1;
                                Logger.Error(ex);
                            }
                        }
                    }
                }
            }
        }


        public async Task AddTag(string tid, Tag tag, TagInfo ti)
        {

            if (ti != null)
            {
                List<uint> W_SeqCount = await WriterReader.GetOpSequenceIDsAsync();
                if(W_SeqCount.Count() >800)
                {
                    W_SeqCount.Sort();

                   await WriterReader.DisableOpSequenceAsync(W_SeqCount[0]);
                }

                ti.writeSequenceId = await BuildWriterOpsAsync(ti, tag);

                Logger.Info($"Added write seq to: {ti.tid} with write EPC/seq:{ti.epc}-{ti.writeSequenceId} on  {DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond}");

                List<uint> V_SeqCount = await VerifyReader.GetOpSequenceIDsAsync();
                if (V_SeqCount.Count() > 800)
                {
                    V_SeqCount.Sort();

                    await VerifyReader.DisableOpSequenceAsync(V_SeqCount[0]);
                }


                ti.verifySequenceId = await BuildVerifyOpsAsync(ti);

                Logger.Info($"Add Verify seq to: {ti.tid} with seq :{ti.verifySequenceId} on {DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond}");

                MasterList.Add(tid, ti);

                Logger.Info($"Added to MasterList: {ti.tid} with write write seq:{ti.writeSequenceId}|Verify seq:{ti.verifySequenceId} ");

            }
            else
            {
                Logger.Info($"Dead Tag Added: Dead Tag");
            }
        }

        private async void WriterReader_TagsReported(object sender, TagReport e)
        {
            try
            {
                foreach (Tag tag in e.Tags)
                {
                     
                    if (tag.TagOpResults.Any())
                    {
                        if (MasterList.Count() > 0)
                        {

                            TagInfo ti = MasterList[tag.Tid.ToHexString()];
                            if (ti != null)
                            {
                                var SAVEepc = ti.epc;
                               // ti.epc = tag.Epc.ToHexString();
                                Logger.Info($"Writing TID:{ti.tid} with EPC:{SAVEepc} Write Op RSSI {tag.PeakRssiInDbm}");

                                bool redo = false;
                                long timestamp = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;

                                bool WriteOpsuccess = true;
                                string tid = string.Empty;
                                ushort opid = 0;
                                string errortext = string.Empty;

                                foreach (TagOpResult result in tag.TagOpResults)
                                {
                                    if (result is TagWriteOpResult)
                                    {
                                        TagWriteOpResult wor = result as TagWriteOpResult;
                                        if (wor.Result != 0)
                                        {
                                           
                                            errortext = wor.Result.ToString();
                                            opid = wor.OpId;
                                            WriteOpsuccess = false;
                                            // Only retry once - don't retry Lock errors
                                            if (ti.writeSuccess == null && wor.Result != WriteResultStatus.TagMemoryLockedError)
                                            {
                                                redo = true;
                                            }

                                        }
                                    }
                                    else if (result is TagLockOpResult)
                                    {

                                        TagLockOpResult wor = result as TagLockOpResult;
                                        // Only retry once
                                        if (ti.writeSuccess == null && wor.Result != 0)
                                        {
                                            errortext = wor.Result.ToString();
                                            opid = wor.OpId;
                                            WriteOpsuccess = false;
                                            redo = true;
                                        }
                                    }
                                }

                                ti.writeReportTimeStamp = DateTime.Now;
                                ti.writeSuccess = WriteOpsuccess;
                                if (WriteOpsuccess)
                                {
                                    Logger.Info($"Successful Write Result: {ti.tid}|{ti.epc}|{ti.writeReportTimeStamp}");
                                    //ti.verifySequenceId = await BuildVerifyOpsAsync(ti);
                                    //Logger.Info($"Add Verify seq to: {ti.tid} with seq :{ti.verifySequenceId} on {DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond}");
                                    WriterOverAll_Success += 1;

                                    
                                }
                                else
                                {
                                    Logger.Info("Failed OPList Result: {TID}|{ID}|{ERROR} {TIMESTAMP}", tag.Tid.ToHexString(), opid, errortext, DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond);
                                    if (redo)
                                    {
                                        ti.epc = SAVEepc;
                                        Logger.Warn($"Requeueing write operator for {ti.tid} with { ti.epc} on {ti.writeReportTimeStamp}");
                                       
                                        ti.writeSequenceId = await BuildWriterOpsAsync(ti, tag);
                                        
                                       
                                    }
                                }

                                _ = UpdateRecoveryData(ti);

                                MasterList[tag.Tid.ToHexString()] = ti;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }


        private async Task<uint> BuildWriterOpsAsync( TagInfo ti, Tag tag = null)
        {
            string tid = ti.tid;
            long timestamp = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;

            TagOpSequence tagOpSequence = BuildWriteOps(ti, tag);
            
            try
            {
                if (WriterReader.IsConnected)
                {
                    await WriterReader.AddOpSequenceAsync(tagOpSequence).ConfigureAwait(false);
                    


                }
            }

            catch (Exception ex)
            {
                Logger.Error(ex);
            }
           
            return tagOpSequence.Id;
        }

        private TagOpSequence BuildWriteOps(TagInfo ti, Tag tag =null)
        {

            try
            {
                TagOpSequence tagOpSequence = new TagOpSequence()
                {
                    AntennaId = 1,
                    TargetTag = new TargetTag()
                    {
                        MemoryBank = MemoryBank.Tid,
                        BitPointer = 0,
                        Data = ti.tid
                    },
                    BlockWriteEnabled = true,
                    BlockWriteWordCount = 2,
                    RetryCount = 2
                };



                var epcOP = new TagWriteOp()
                {
                    Id = 111,
                    MemoryBank = MemoryBank.Epc,
                    Data = TagData.FromHexString(ti.dataentry.GetType().GetProperty("EPC").GetValue(ti.dataentry, null).ToString()),
                    
                    
                    WordPointer = WordPointers.Epc
                };
                tagOpSequence.Ops.Add(epcOP);

               // Logger.Info($"BuildWriterOps: {epcOP.Data}length1:{ti.epc.Length}length2:{ti.dataentry.GetType().GetProperty("EPC").GetValue(ti.dataentry, null).ToString().Length}");

                //if (tag.epc.Length != dataentry.epc.Length)
                if (tag == null)
                {
                    ushort newEpcLenWords = (ushort)(ti.dataentry.GetType().GetProperty("EPC").GetValue(ti.dataentry, null).ToString().Length / 4);
                    ushort newPcBits = PcBits.AdjustPcBits(ti.PcBits, newEpcLenWords);
                    TagWriteOp writePc = new TagWriteOp();
                    writePc.Id = 321;
                    // The PC bits are in the EPC memory bank.
                    writePc.MemoryBank = MemoryBank.Epc;
                    // Specify the data to write (the modified PC bits).
                    writePc.Data = TagData.FromWord(newPcBits);
                    // Start writing at the start of the PC bits.
                    writePc.WordPointer = WordPointers.PcBits;

                    // Add this tag write op to the tag operation sequence.
                    tagOpSequence.Ops.Add(writePc);


                }
                else
                {
                    if (tag.Epc.ToString().Length != ti.dataentry.GetType().GetProperty("EPC").GetValue(ti.dataentry, null).ToString().Length)
                    {
                        //  Logger.Info($"PCBitsIN");
                        // We need adjust the PC bits and write them back to the 
                        // tag because the length of the EPC has changed.
                        ushort newEpcLenWords = (ushort)(ti.dataentry.GetType().GetProperty("EPC").GetValue(ti.dataentry, null).ToString().Length / 4);
                        //   Logger.Info($"PCBitsIN:{newEpcLenWords}");
                        ushort newPcBits = PcBits.AdjustPcBits(tag.PcBits, newEpcLenWords);

                        TagWriteOp writePc = new TagWriteOp();
                        writePc.Id = 321;
                        // The PC bits are in the EPC memory bank.
                        writePc.MemoryBank = MemoryBank.Epc;
                        // Specify the data to write (the modified PC bits).
                        writePc.Data = TagData.FromWord(newPcBits);
                        // Start writing at the start of the PC bits.
                        writePc.WordPointer = WordPointers.PcBits;

                        // Add this tag write op to the tag operation sequence.
                        tagOpSequence.Ops.Add(writePc);
                        //          Logger.Info($"PCBits: {newPcBits}");
                    }
                }
                

                //TODO: this is currently ignoring all passwords
                TagLockOp lop = new TagLockOp()
                {
                    Id = 222,

                    AccessPassword = TagData.FromHexString(ti.dataentry.GetType().GetProperty("ACCESS").GetValue(ti.dataentry, null).ToString()),
                    AccessPasswordLockType = (TagLockState)ti.dataentry.GetType().GetProperty("ACCESS_LOCK").GetValue(ti.dataentry, null),
                    EpcLockType = (TagLockState)ti.dataentry.GetType().GetProperty("EPC_LOCK").GetValue(ti.dataentry, null),
                    KillPasswordLockType = (TagLockState)ti.dataentry.GetType().GetProperty("KILL_LOCK").GetValue(ti.dataentry, null),
                    UserLockType = (TagLockState)ti.dataentry.GetType().GetProperty("USER_LOCK").GetValue(ti.dataentry, null),
                };
                tagOpSequence.Ops.Add(lop);
                return tagOpSequence;
            }
            catch (Exception ex)
            {
                Logger.Info($"EPC Entry out: {ex}");
            }
            return new TagOpSequence();
        }

        private async Task WriteRecoveryData(TagInfo tagdata)
        {
            await ProductionDAL.WriteRecoveryData(tagdata, scalar, _ct).ConfigureAwait(false);

        }

        private async Task UpdateRecoveryData(TagInfo tagdata)
        {
           var results =  await ProductionDAL.UpdateRecoveryData(tagdata,scalar, _ct).ConfigureAwait(false);
            if(!results)
            {
                Logger.Info($"Update to recovery failed for :{tagdata.tid}");
            }
        }

        private void VerifyReader_TagsReported(object sender, TagReport e)
        {
            try
            {
                foreach (var tag in e.Tags)
                {
                    if (tag.TagOpResults.Any())
                    {
                        if (MasterList.Count() > 0)
                        {
                            TagInfo ti = MasterList[tag.Tid.ToHexString()];
                            if (ti != null)
                            {
                                Logger.Info($"Verifing:{ti.tid} with EPC:{tag.Epc.ToHexString()} Write Op RSSI {tag.PeakRssiInDbm}");
                                //   ti.epc = tag.Epc.ToHexString();
                                DataBaseEntry resultsdata = new DataBaseEntry()
                                { epc = tag.Epc.ToHexString() };

                                // come back too.
                                ti = TagOpProcess(tag, ti, resultsdata, ti.dataentry);

                                Logger.Info($"Verifing Compare:{ti.epc}|{resultsdata.epc} VerifyEPC: {ti.verifyEPC} VerifyLock: {ti.verifyLock} User: {ti.verifyUser}");


                                ti.verifyReportTimeStamp = DateTime.Now;
                                
                                MasterList[tag.Tid.ToHexString()] = ti;
                                var resulttime = DateTime.Now.ToString();

                                
                               
                                bool verifySuccess;
                                if (ti.verifyEPC == true)
                                {
                                    verifySuccess = true;
                                    ValidatorOverAll_Success += 1;
                                    
                                }
                                else
                                {
                                    ti.verifySuccess = false;
                                    verifySuccess = false;
                                    ValidatorOverAll_Fail += 1;
                                }

                                bool writeSuccess;
                                if (ti.writeSuccess == true)
                                {
                                    writeSuccess = true;
                                }
                                else
                                {
                                    ti.writeSuccess = false;
                                    writeSuccess = false;
                                }

                                ResultsLogger.Info($"{resulttime}|{ti.tid}|{ti.epc}|{ti.verifySequenceId}|{ti.verifySuccess}|{ti.verifyEPC}|{ti.verifyLock}|{ti.verifyUser}|{ti.writeSuccess}|{ti.writeSequenceId}");

                                lock (CurrentLogListSyncRoot)
                                { 
                                    var Logitem = new MasterModel()
                                    {
                                        ResultTime = resulttime ,
                                        EPC = ti.epc,
                                        TID = ti.tid,
                                        VerifySuccess = verifySuccess,
                                        WriteSuccess = writeSuccess,
                                        EPC_LOCK = ti.verifyEPC,
                                        USER = "",
                                        USER_LOCK = ti.verifyUser,
                                        ACCESS = "",
                                        ACCESS_LOCK = false,
                                        KILL = "",
                                        KILL_LOCK = false,
                                        PRINT1 = "",
                                        PRINT2 = "",
                                        PRINT3 = "",
                                        PRINT4 = "",
                                        PRINT5 = "",
                                    };
                                    CurrentLogList.Add(Logitem);
                                }

                                _ = UpdateRecoveryData(ti);
                                ValidatorOverAll_Total = ValidatorOverAll_Success + ValidatorOverAll_Fail; 
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                ValidatorOverAll_Fail += 1;
                Logger.Error(ex);
            }
        }

        private async Task<uint> BuildVerifyOpsAsync( TagInfo ti)
        {
            
            //long timestamp = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;

            TagOpSequence tagOpSequence = BuildVerifyOps(ti);
            if (VerifyReader != null)
            {
                await VerifyReader.AddOpSequenceAsync(tagOpSequence).ConfigureAwait(false);
            }
            return tagOpSequence.Id;
            
        }

        public TagOpSequence BuildVerifyOps(TagInfo ti)
        {
            TagOpSequence tagOpSequence = new TagOpSequence()
            {
                TargetTag = new TargetTag()
                {
                    MemoryBank = MemoryBank.Tid,
                    BitPointer = 0,
                    Data = ti.tid,
                }
            };
            if (!ti.dataentry.GetType().GetProperty("USER").GetValue(ti.dataentry, null).Equals("?"))
            {
                TagReadOp top = new TagReadOp()
                {
                    Id = 111,
                    MemoryBank = MemoryBank.User,
                    WordPointer = 0,
                    WordCount = (ushort)(ti.dataentry.GetType().GetProperty("USER").GetValue(ti.dataentry, null).ToString().Length / 4)
                };
                tagOpSequence.Ops.Add(top);
            }

            if ((TagLockState)ti.dataentry.GetType().GetProperty("ACCESS_LOCK").GetValue(ti.dataentry, null) < TagLockState.None || (TagLockState)ti.dataentry.GetType().GetProperty("EPC_LOCK").GetValue(ti.dataentry, null) < TagLockState.None || (TagLockState)ti.dataentry.GetType().GetProperty("USER_LOCK").GetValue(ti.dataentry, null) < TagLockState.None || (TagLockState)ti.dataentry.GetType().GetProperty("KILL_LOCK").GetValue(ti.dataentry, null) < TagLockState.None)
            {
                TagReadOp readReserved = new TagReadOp();
                readReserved.Id = 112;
                readReserved.MemoryBank = MemoryBank.Reserved;
                readReserved.WordPointer = 4;
                readReserved.WordCount = 1; //lock bits
                tagOpSequence.Ops.Add(readReserved);
            }

            return tagOpSequence;
        }

        public TagInfo TagOpProcess(Tag tag, TagInfo ti, DataBaseEntry resultsdata, object dataentry)
        {
            foreach (TagOpResult result in tag.TagOpResults)
            {
                if (result is TagReadOpResult)
                {
                    TagReadOpResult readresult = result as TagReadOpResult;
                    switch (result.OpId)
                    {
                        case 112:
                            resultsdata = DetermineLockState(resultsdata, readresult.Data.ToHexString());
                            break;
                        case 111:
                            resultsdata.user = readresult.Data.ToHexString();
                            break;
                        default:
                            break;
                    }
                }
            }


            if (resultsdata != null)
            {
//                byte[] ba = Encoding.Default.GetBytes();
 //               var hexString = BitConverter.ToString(ba);
  //              hexString = hexString.Replace("-", "");

                if (ti.epc == resultsdata.epc.ToString())
                {
                    ti.verifyEPC = true;
                }
                if ((TagLockState)dataentry.GetType().GetProperty("KILL_LOCK").GetValue(dataentry, null) == resultsdata.kill_lock
                    && (TagLockState)dataentry.GetType().GetProperty("ACCESS_LOCK").GetValue(dataentry, null) == resultsdata.access_lock
                    && (TagLockState)dataentry.GetType().GetProperty("EPC_LOCK").GetValue(dataentry, null) == resultsdata.epc_lock)
                {
                    ti.verifyLock = true;
                }
                if (!dataentry.GetType().GetProperty("USER").GetValue(dataentry, null).ToString().Equals("?"))
                {
                    if (dataentry.GetType().GetProperty("USER").GetValue(dataentry, null).ToString().Equals(resultsdata.user))
                        ti.verifyUser = true;
                }
                else ti.verifyUser = true;

            }

            if (ti.verifyEPC && ti.verifyLock && ti.verifyUser)
            {
                ti.verifySuccess = true;
            }


            return ti;
        }

        internal static DataBaseEntry DetermineLockState(DataBaseEntry results, string state)
        {
            //DataBaseEntry results = new DataBaseEntry();
            string strBinary;
            // state contains word 4 from the reserved memory bank. The first hex character 
            // defines the kill and access lock status while the second hex character defines the
            // epc and user lock status. This only works with Monza 4 & 5 tags. 

            // Set Kill and Access Status
            if (string.IsNullOrEmpty(state) == false)
            {
                strBinary = Convert.ToString(Convert.ToInt32(state.Substring(0, 1), 16), 2).PadLeft(4, '0');
                switch (strBinary.Substring(0, 2))
                {
                    case "10":
                        results.kill_lock = TagLockState.Lock;
                        break;
                    case "11":
                        results.kill_lock = TagLockState.Permalock;
                        break;
                    case "01":
                        results.kill_lock = TagLockState.Permaunlock;
                        break;
                    default:
                        results.kill_lock = TagLockState.None;
                        break;
                }
                switch (strBinary.Substring(2, 2))
                {
                    case "10":
                        results.access_lock = TagLockState.Lock;
                        break;
                    case "11":
                        results.access_lock = TagLockState.Permalock;
                        break;
                    case "01":
                        results.access_lock = TagLockState.Permaunlock;
                        break;
                    default:
                        results.access_lock = TagLockState.None;
                        break;
                }
                // Set EPC and USER Status
                strBinary = Convert.ToString(Convert.ToInt32(state.Substring(1, 1), 16), 2).PadLeft(4, '0');
                switch (strBinary.Substring(0, 2))
                {
                    case "10":
                        results.epc_lock = TagLockState.Lock;
                        break;
                    case "11":
                        results.epc_lock = TagLockState.Permalock;
                        break;
                    case "01":
                        results.epc_lock = TagLockState.Permaunlock;
                        break;
                    default:
                        results.epc_lock = TagLockState.None;
                        break;
                }
                switch (strBinary.Substring(2, 2))
                {
                    case "10":
                        results.user_lock = TagLockState.Lock;
                        break;
                    case "11":
                        results.user_lock = TagLockState.Permalock;
                        break;
                    case "01":
                        results.user_lock = TagLockState.Permaunlock;
                        break;
                    default:
                        results.user_lock = TagLockState.None;
                        break;
                }
            }
            else
            {
                results.user_lock = TagLockState.None;
                results.epc_lock = TagLockState.None;
                results.access_lock = TagLockState.None;
                results.kill_lock = TagLockState.None;
            }
            return results;
        }


        private void SetupEncoder()
        {
            try
            {
                // TODO: These all need to be moved to a settings file
                encoder = new EncoderMonitor()
                {
                    ComPort = "COM6",
                };


                encoder.Open(_ct);

            }
            catch
            {
                Logger.Error("Error opening Encoder Port");
                //return false;
            }
        }

        public async Task Stop()
        {
            await MainReader.StopAsync().ConfigureAwait(false);
            await WriterReader.StopAsync().ConfigureAwait(false);
            await VerifyReader.StopAsync().ConfigureAwait(false);

            Task.WaitAll(tasklist.ToArray());
        }

        //TODO: Dan, this is mostly conceptual - need to make this work with your arch.
        public void Calibrate()
        {
            // Setup calibration of the roll and start movement.
            calibrate = true;
            OutSpoolMotor.SetDIrection(Direction.FEED);
            MoveQueue.Enqueue(tagOps.TagPitch);
        }

        public void StartMotor()
        {
            _initialTags = TidList.Count;
            Type myOps = WriterTypeFactory.GetType(TagType);
            tagOps = (VirtualOpsBase)Activator.CreateInstance(myOps);

            OutSpoolMotor.SetDistance(80,90);

            OutSpoolMotor.SetDIrection(Direction.OUT);

            double timeinseconds = (tagOps.TagPitch / (tagOps.SFM / 60 / 12));
           // MoveTime = (1 / (TagsPerHour / 60 / 60)) * 1000000
            long movetime = (long)(timeinseconds * (double)10000); // convert desired sfm to a timevalue per tag in mircoseconds
            OutSpoolMotor.SetTime(movetime);
            //OutSpoolMotor.SetTime(1000000);
            MoveQueue.Enqueue(tagOps.TagPitch);
            MoveQueue.Enqueue(tagOps.TagPitch);
             _movecount = (uint)_initialTags + 1;
        }
    
        public void StopMotor()
        {
            OutSpoolMotor.Stop();
          
        }

        public void MoveLoop(CancellationToken ct)
        {
         //   encoder.Reset();
            while (!ct.IsCancellationRequested)
            {
                try
                {
                    if (MoveQueue.Count > 0)
                    {
                        // Todo, this should be a property thats uses the sfm feedback from the encoder.
                        double move = MoveQueue.Dequeue();
                        uint servomove = (uint)(move * scalar);
                        uint servo2move = (uint)(move * scalar2);
                        OutSpoolMotor.SetMove(servomove, servo2move);
                        OutSpoolMotor.Move();
                        // encoder.Read();
                    }
                    System.Threading.Thread.Sleep(1);
                }
                catch(Exception ex)
                {
                    var aa = ex.ToString();
                }
            }
            OutSpoolMotor.Stop();
        }

        // Handle servo events
        public void PortAttnHandler(UInt32 Attn)
        {
            switch(Attn)
            {
                case 'A':
                   // encoder.Read();
                    MoveQueue.Enqueue(tagOps.TagPitch);
                    break;
                case 'B':
                  

                    break;
                case 'C':
                    Logger.Info($"PortAttnHandler:  Move Done");
                    Logger.Info($"Move: {_movecount}, Good Tags: {MasterList.Count}, Total Tags:{TidList.Count}");
                    if (TidList.Count - _lasttagcount > 1)
                    {
                        if (TidList.Last().Equals("DEADTAG"))
                        {
                            Logger.Info("Removing last DEADTAG 2 tags found");
                            TidList.RemoveAt(TidList.Count - 1);
                            _lasttagcount = TidList.Count;
                        }
                    }

                    if (_movecount > (TidList.Count))
                    {
                        _lasttagcount = TidList.Count;
                        _ = AddTag("DEADTAG", null, null);
                    }
                    else
                        _lasttagcount = TidList.Count;

                    _movecount++;
                    DistanceCalibration();
                    break;
                case 'S':
                    Logger.Info($"PortAttnHandler:  Motor Stopped");
                    break;
                default:
                    Logger.Warn($"PortAttnHandler: {(char)Attn}");
                    break;
            }
        }
        private void DistanceCalibration()
        {
            Logger.Info($"DistanceCalibration: New Distance {encoder.distance}");
            // Encoder updated its values - do whatever you need to with them.
            try
            {

                Logger.Info($"E distance: {encoder.distance}");
                MoveData.Enqueue(encoder.distance);
                encoder.Reset();
                if (MoveData.Count() == 6)
                {

                    double average = MoveData.Average();
                    Logger.Info($"Average move distance: {average}");


                    if (average > (tagOps.TagPitch + .005) || average < (tagOps.TagPitch - .005)) // TODO: May need to make the .005 adjustable based on tag? Don't want to chase tiny variations
                    {

                        double ratio = tagOps.TagPitch / average;
                        Logger.Info($"Move Average: {average} - Adjusting scalar with ratio {ratio}");
                        scalar = (int)((double)scalar * ratio);
                        Logger.Info($"Scalar: {scalar}");
                        MoveData.Clear();

                    }

                }
            }
            catch (Exception ex)
            {
                var AA = ex.ToString();
            }
        }

        private async Task FindMOPath(string ProductName)
        {
            ProductionResults = new ProductionPartWrapper();
           
                MoReturnItems moSearch = RemoteDAL.FindMOPath(ProductName);

                if (moSearch.Parts_DBb1.Contains(@"W:\PL"))
                {
                    ProductQty = Convert.ToInt32(moSearch.Parts_ItemSize);
                    var searchDir = $@"{moSearch.Parts_DBb1}\{moSearch.Item_Part}_{moSearch.Items_ID}_PL.csv";
                    using var reader = new StreamReader($@"{searchDir}");
                    string strLine = reader.ReadLine(); // Header line
                    ProductionResults.PartProductionAttrib = new PartAttributes()
                    {
                        PartId = moSearch.Item_Part,
                        PartHeader = strLine,
                        TagOp = "4425"
                    };
                    ProductionResults.PartProductionDB = new PartDatabase()
                    {
                        PartId = moSearch.Item_Part,
                        DatabaseFile = reader.ReadToEnd().ToString(),
                    };
                }
                else
                {
                    ProductionResults = await ProductionDAL.GetPartAttribute(moSearch.Item_Part, _ct).ConfigureAwait(false);
                }
           
        }

        private TagLockState lockstate(string lockstring)
        {
            if (lockstring.ToUpper().Contains("PERMALOCK"))
            {
                //temp
                return TagLockState.Lock;
            }
            else if (lockstring.ToUpper().Contains("?") || string.IsNullOrEmpty(lockstring))
            {
                return TagLockState.None;
            }
            else if (lockstring.ToUpper().Contains("PERMAUNLOCK"))
            {
                return TagLockState.Permaunlock;
            }
            else if (lockstring.ToUpper().Contains("UNLOCK"))
            {
                return TagLockState.Unlock;
            }
            else if (lockstring.ToUpper().StartsWith("LOCK"))
            {
                return TagLockState.Lock;
            }
            return TagLockState.None;
        }

        public async Task LoadRecoveryData()
        {
            try
            {

                List<TagInfo> RecoveryItems = await ProductionDAL.GetRecoveryData(_ct);

                await  GetProductionData(RecoveryItems);

                foreach (var r_Item in RecoveryItems)
                {

                    byte[] ba = Encoding.Default.GetBytes(r_Item.tid);
                    var hexString = BitConverter.ToString(ba);
                    hexString = hexString.Replace("-", "");

                    MasterList.Add(hexString, r_Item);
                    if(r_Item.writeSuccess ==false)
                    {
                        await BuildWriterOpsAsync(r_Item);


                    }
                    if (r_Item.writeSuccess == false)
                    {
                        await BuildVerifyOpsAsync(r_Item);
                    }

                }

                scalar = RecoveryItems.Max(s => s.Scalar);


            }
            catch(Exception ex )
            {
                //var aa = ex.ToString();
            }

        }

        public async Task GetProductionData(List<TagInfo> recoveryList = null)
        {

            await FindMOPath(BarcodeResult);

            string strLine;
            string[] strValues;
            strValues = ProductionResults.PartProductionAttrib.PartHeader.Split('|');
            
            string[] headers = new string[strValues.Count()];
            Type[] types = new Type[strValues.Count()];
            for (var i = 0; i < strValues.Count(); i++)
            {
                headers[i] = strValues[i].ToString();
                if (strValues[i].ToUpper().Contains("_LOCK"))
                {
                    types[i] = typeof(TagLockState);
                }
                else if (strValues[i].ToUpper().Contains("OFFSET"))
                {
                    types[i] = typeof(int);
                }
                else if (strValues[i].ToUpper().Contains("PCBITS"))
                {
                    types[i] = typeof(ushort);
                }
                else
                {
                    types[i] = typeof(string);
                }
            }
           
            // convert string to stream
            byte[] byteArray = Encoding.ASCII.GetBytes(ProductionResults.PartProductionDB.DatabaseFile);
            MemoryStream stream = new MemoryStream(byteArray);
            StreamReader reader = new StreamReader(stream);
            var linecount = 0;
            while ((strLine = reader.ReadLine()) != null)
            {
                linecount++;
            };
            stream.Position = 0;
            reader.DiscardBufferedData();


            DynamicClassBuilder dynamicClass = new DynamicClassBuilder("DataBaseEntry");
            List<object> dyn_class = dynamicClass.CreateObject(headers, types, linecount);
  

            if (Database == null)
            {
                Database = new Queue<object>();
            }

            try
            {
                var count = -1;
                var bTest = false;
                while ((strLine = reader.ReadLine()) != null)
                {

                    count++;
                    strValues = strLine.Split('|');
                    if (recoveryList != null)
                    {
                        var found = recoveryList.FirstOrDefault(r => r.epc == strValues[0]);
                        if (found == null)
                        {
                            if (bTest == true)
                            {
                                var replaceepc = strValues[0].Replace("3614", "1414");
                                dyn_class[count].GetType().GetProperty("EPC").SetValue(dyn_class[count], replaceepc);
                            }
                            else
                            {
                                dyn_class[count].GetType().GetProperty("EPC").SetValue(dyn_class[count], strValues[0]);
                            }
                            
                            dyn_class[count].GetType().GetProperty("EPC_LOCK").SetValue(dyn_class[count], lockstate(strValues[1].ToString()));
                            dyn_class[count].GetType().GetProperty("USER").SetValue(dyn_class[count], strValues[2]);
                            dyn_class[count].GetType().GetProperty("USER_LOCK").SetValue(dyn_class[count], lockstate(strValues[3].ToString()));
                            dyn_class[count].GetType().GetProperty("ACCESS").SetValue(dyn_class[count], strValues[4]);
                            dyn_class[count].GetType().GetProperty("ACCESS_LOCK").SetValue(dyn_class[count], lockstate(strValues[5].ToString()));
                            dyn_class[count].GetType().GetProperty("KILL").SetValue(dyn_class[count], strValues[6]);
                            dyn_class[count].GetType().GetProperty("KILL_LOCK").SetValue(dyn_class[count], lockstate(strValues[7].ToString()));
                            dyn_class[count].GetType().GetProperty("PRINT1").SetValue(dyn_class[count], strValues[8]);
                            dyn_class[count].GetType().GetProperty("PRINT2").SetValue(dyn_class[count], strValues[9]);
                            dyn_class[count].GetType().GetProperty("PRINT3").SetValue(dyn_class[count], strValues[10]);
                            dyn_class[count].GetType().GetProperty("PRINT4").SetValue(dyn_class[count], strValues[11]);
                            dyn_class[count].GetType().GetProperty("PRINT5").SetValue(dyn_class[count], strValues[12]);
                            Database.Enqueue(dyn_class[count]);

                        }
                    }
                    else
                    {
                        if (bTest == true)
                        {
                            var replaceepc = strValues[0].Replace("3614", "1414");
                            dyn_class[count].GetType().GetProperty("EPC").SetValue(dyn_class[count], replaceepc);
                        }
                        else
                        {
                            dyn_class[count].GetType().GetProperty("EPC").SetValue(dyn_class[count], strValues[0]);
                        }
                        dyn_class[count].GetType().GetProperty("EPC_LOCK").SetValue(dyn_class[count], lockstate(strValues[1].ToString()));
                        dyn_class[count].GetType().GetProperty("USER").SetValue(dyn_class[count], strValues[2]);
                        dyn_class[count].GetType().GetProperty("USER_LOCK").SetValue(dyn_class[count], lockstate(strValues[3].ToString()));
                        dyn_class[count].GetType().GetProperty("ACCESS").SetValue(dyn_class[count], strValues[4]);
                        dyn_class[count].GetType().GetProperty("ACCESS_LOCK").SetValue(dyn_class[count], lockstate(strValues[5].ToString()));
                        dyn_class[count].GetType().GetProperty("KILL").SetValue(dyn_class[count], strValues[6]);
                        dyn_class[count].GetType().GetProperty("KILL_LOCK").SetValue(dyn_class[count], lockstate(strValues[7].ToString()));
                        dyn_class[count].GetType().GetProperty("PRINT1").SetValue(dyn_class[count], strValues[8]);
                        dyn_class[count].GetType().GetProperty("PRINT2").SetValue(dyn_class[count], strValues[9]);
                        dyn_class[count].GetType().GetProperty("PRINT3").SetValue(dyn_class[count], strValues[10]);
                        dyn_class[count].GetType().GetProperty("PRINT4").SetValue(dyn_class[count], strValues[11]);
                        dyn_class[count].GetType().GetProperty("PRINT5").SetValue(dyn_class[count], strValues[12]);
                        Database.Enqueue(dyn_class[count]);
                        
                    }
                    
                    //if (strValues[0] != "?") // Skip SPLICE tags
                    //{
                    //    for (var i = 0; i < headers.Count(); i++)
                    //    {
                    //        PropertyInfo piInstance = TP.GetProperty(headers[i].ToString());

                    //        if (piInstance.Name.Contains("LOCK"))
                    //        {
                    //            if (strValues[i].ToUpper().Contains("PERMALOCK"))
                    //            {
                    //                piInstance.SetValue(myclass[count], TagLockState.Permalock);
                    //            }
                    //            else if (strValues[i].ToUpper().Contains("?") || string.IsNullOrEmpty(strValues[i]))
                    //            {
                    //                piInstance.SetValue(myclass[count], TagLockState.None);
                    //            }
                    //            else if (strValues[i].ToUpper().Contains("PERMAUNLOCK"))
                    //            {
                    //                piInstance.SetValue(myclass[count], TagLockState.Permaunlock);
                    //            }
                    //            else if (strValues[i].ToUpper().Contains("UNLOCK"))
                    //            {
                    //                piInstance.SetValue(myclass[count], TagLockState.Unlock);
                    //            }
                    //            else if (strValues[i].ToUpper().StartsWith("LOCK"))
                    //            {
                    //                piInstance.SetValue(myclass[count], TagLockState.Lock);
                    //            }
                    //        }
                    //        else if (piInstance.Name.ToUpper().Contains("OFFSET"))
                    //        {
                    //            piInstance.SetValue(myclass[count], int.Parse(strValues[i]));

                    //        }
                    //        else if (piInstance.Name.ToUpper().Contains("PCBITS"))
                    //        {
                    //            piInstance.SetValue(myclass, Int16.Parse(strValues[i]));
                    //        }
                    //        else
                    //        {
                    //            piInstance.SetValue(myclass[count], strValues[i].ToString());
                    //        }
                    //    };


                    //Database.Enqueue(testc);
                }
            }
            catch (Exception ex)
            {
                var aa = ex.ToString();
            }




            await Start();

            // configure slow speeed for hand walking the tags to new roll
           StartMotor();
        }
       
        public async Task BackUpDatabase()
        {
            try
            {
                string backupDir = CurrentSettings.Kiosk?.DbBackupSaveLocation;
                string backupName = $"KioskDatabaseBackup_{DateTime.Now:yyyyMMdd_HHmmss}.bak"; // Configurable name? Localized?

                if (!Directory.Exists(backupDir))
                {
                    Directory.CreateDirectory(backupDir);
                }

                await DAL.BackupDatabase(Path.Combine(backupDir, backupName), _ct).ConfigureAwait(false);

                Log.Info(FkEventCode.Database_Backup_Success);
            }
            catch (OperationCanceledException) { }
            catch (Exception ex)
            {
                Log.Error(ex, FkEventCode.Database_Backup_Failed, ex.Message);
            }
        }

        public async Task TruncateDatabase()
        {
            try
            {
                await DAL.TruncateDatabaseLog(_ct).ConfigureAwait(false);
                
                await GetDBSize().ConfigureAwait(false);
                Log.Info(FkEventCode.Database_Truncate_Success);
            }
            catch (OperationCanceledException) { }
            catch (Exception ex)
            {
                Log.Error(ex, FkEventCode.Database_Truncate_Failed, ex.Message);
            }
        }

        private async Task GetDBSize()
        {
            try
            {
                double sizeMB = await DAL.GetDatabaseSizeMB(_ct).ConfigureAwait(false);
                double sizeGB = sizeMB / 1024.0;
                double roundedGB = Math.Ceiling(sizeGB * 100.0) / 100.0; // Round up to two decimal places

                CurrentDatabaseSizeGB = roundedGB;
                OnDatabaseSizeUpdated();
            }
            catch (OperationCanceledException) { }
            catch (Exception ex)
            {
                string s = ex.Message;
                CurrentDatabaseSizeGB = -1;
            }

            float maxSize = CurrentSettings?.Kiosk?.DatabaseMaxSizeGB ?? 0;
            if (maxSize > 0 && CurrentDatabaseSizeGB > maxSize)
            {
                Log.Warn(FkEventCode.Database_Size_Exceeded, CurrentDatabaseSizeGB, maxSize);
            }

            bool notificationsChanged = false;
            lock (AlarmsSyncRoot)
            {
                List<Notification> notifications = Alarms.Where(p => p.EventCode == FkEventCode.Database_Size_Exceeded).ToList();
                if ((maxSize == 0 || CurrentDatabaseSizeGB < maxSize) && notifications.Any())
                {
                    notifications.ForEach(p => Alarms.Remove(p));
                    notificationsChanged = true;
                }
                else if (maxSize > 0 && CurrentDatabaseSizeGB >= maxSize && !notifications.Any())
                {
                    Notification notification = new Notification()
                    {
                        Timestamp = DateTimeOffset.Now,
                        Message = FkEventCode.Database_Size_Exceeded.GetMessage(),
                        EventCode = FkEventCode.Database_Size_Exceeded,
                        Level = FkErrorLevel.Warn,
                    };
                    Alarms.Add(notification);
                    notificationsChanged = true;
                }
            }
            if (notificationsChanged)
            {
                OnNotificationOccurred();
            }
        }

        private async void IdleTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                if (CurrentUser.IsLoggedIn)
                {
                    int idleTimeoutMinutes = ParsedSettings?.GetDefaultSettings()?.Kiosk?.IdleLoginTimeout ?? 0;
                    int idleTimeoutMiliseconds = idleTimeoutMinutes * 60 * 1000;
                    int idleMilliseconds = WinApi.GetIdleMilliseconds();
                    if (idleMilliseconds > idleTimeoutMiliseconds && CurrentUser.IsLoggedIn)
                    {
                        Log.Info(FkEventCode.User_Logout_Due_To_Inactivity, CurrentUser.Username ?? "?");
                        await LogoutAsync().ConfigureAwait(false);
                    }
                }
            }
            catch { }
            finally
            {
                _idleTimer.Start();
            }
        }

        public List<FkEventCode> AlarmEvents { get; } = new List<FkEventCode>()
        {
            FkEventCode.Kiosk_Rfid_Disconnected,
            FkEventCode.Kiosk_Rfid_Disconnected_Error,
            FkEventCode.Database_Size_Exceeded,
        };

        private void Log_LogEvent(object sender, LogEventArgs e)
        {
            if (LogManager.Configuration != null)
            {
                string userName = CurrentUser?.Username ?? "";
                string lineName = ParsedSettings?.KioskName ?? "";
                string orderName = null;

                LogManager.Configuration.Variables["user"] = userName;
                LogManager.Configuration.Variables["code"] = e.EventCode.GetNumber().ToString();

                string message = e.Message ?? e.EventCode.GetMessage();
                Exception ex = e.Error;

                switch (e.ErrorLevel)
                {
                    case FkErrorLevel.Error:
                        _logger.Error(ex, message);
                        break;
                    case FkErrorLevel.Warn:
                        _logger.Warn(ex, message);
                        break;
                    case FkErrorLevel.Info:
                        _logger.Info(ex, message);
                        break;
                    case FkErrorLevel.Trace:
                        _logger.Trace(ex, message);
                        break;
                    default:
                        _logger.Debug(ex, message);
                        break;
                }

                if (e.ErrorLevel != FkErrorLevel.Debug
                    && e.ErrorLevel != FkErrorLevel.Trace)
                {
                    _logChannel.Writer.TryWrite(new Logs()
                    {
                        OrderId = null,
                        LogTime = e.Timestamp.UtcDateTime,
                        User = userName,
                        Line = lineName,
                        OrderName = orderName,
                        Message = message,
                        Code = e.EventCode.GetNumber(),
                        LogLevel = e.ErrorLevel.ToString(), // Localize error level?
                        SyncStatus = 0,
                    });
                }

                if (e.ErrorLevel != FkErrorLevel.Trace
                    && e.ErrorLevel != FkErrorLevel.Debug
                    && !AlarmEvents.Contains(e.EventCode))
                {
                    Notification notification = new Notification()
                    {
                        Error = e.Error,
                        EventCode = e.EventCode,
                        Level = e.ErrorLevel,
                        Message = e.Message,
                        Timestamp = e.Timestamp,
                    };
                    CurrentMessage = notification;
                    OnNotificationOccurred();
                }
            }
        }

        public async Task LoadControllerAsync()
        {
            await _loadControllerLock.WaitAsync();
            try
            {
                await UnloadControllerInternalAsync().ConfigureAwait(false);

                _logChannel = Channel.CreateUnbounded<Logs>(new UnboundedChannelOptions() { AllowSynchronousContinuations = false, SingleReader = true, SingleWriter = false });

                CancellationTokenSource cts = _cts = new CancellationTokenSource();
                CancellationToken ct = cts.Token;

                bool databaseCreated = await DAL.EnsureKioskDatabaseCreatedAsync(ct).ConfigureAwait(false);

                ParseSettingsResult settings = await ReadSettings(ct).ConfigureAwait(false);
                await SaveSettings(settings, ct).ConfigureAwait(false);
                await LoadSettings(ct).ConfigureAwait(false);

                _logChannelTask = Task.Run(() => ProcessLogChannelAsync(_logChannel.Reader));

                var users = await DAL.GetUsers(ct).ConfigureAwait(false);
                var rights = await DAL.GetRights(ct).ConfigureAwait(false);

                await GetDBSize().ConfigureAwait(false);

                IsLoaded = true;
            }
            catch (OperationCanceledException) { await UnloadControllerInternalAsync().ConfigureAwait(false); }
            catch (Exception ex)
            {
                await UnloadControllerInternalAsync().ConfigureAwait(false);
                string msg = ex.Message;
            }
            finally
            {
                _loadControllerLock.Release();
                OnControllerStatusChanged();
                OnNotificationOccurred();
            }
        }

        private async Task UnloadControllerInternalAsync()
        {
            _cts.Cancel();

           // try { await MainReader.ConfigureAsync(RFIDReader.ConfigType.Main).ConfigureAwait(false); } catch { }
           // try { await WriterReader.ConfigureAsync(RFIDReader.ConfigType.Writer).ConfigureAwait(false); } catch { }
           // try { await VerifyReader.ConfigureAsync(RFIDReader.ConfigType.Validator).ConfigureAwait(false); } catch { }

            _logChannel.Writer.Complete();

            await Task.WhenAny(_logChannelTask, Task.Delay(10000)).ConfigureAwait(false);

            if (IsLoaded)
            {
                IsLoaded = false;
                OnControllerStatusChanged();
            }
        }

        public async Task UnloadControllerAsync()
        {
            await _loadControllerLock.WaitAsync();
            try
            {
                await UnloadControllerInternalAsync().ConfigureAwait(false);
            }
            finally
            {
                _loadControllerLock.Release();
            }
        }

        public event EventHandler ControllerStatusChanged;
        private void OnControllerStatusChanged()
        {
            try { ControllerStatusChanged?.Invoke(this, EventArgs.Empty); } catch { }
        }

        public async Task BackUpSystem(string message)
        {
            Notification notification = new Notification();
            
            try
            {
                string zipPath = @$"{CurrentSettings.Kiosk.ReportSaveLocation}SystemBackUp.zip";
                notification.Message = string.Format(message, zipPath);
                if (File.Exists(zipPath))
                {
                    File.Delete(zipPath);
                }
                System.Reflection.Assembly a = System.Reflection.Assembly.GetEntryAssembly();
                string baseDir = System.IO.Path.GetDirectoryName(a.Location);

                ZipFile.CreateFromDirectory(baseDir, zipPath);
                notification.EventCode = FkEventCode.System_Backup_Success;
            }
            catch { notification.EventCode = FkEventCode.System_Backup_Fail; };

            notification.Level = FkErrorLevel.Info;
            notification.Timestamp = DateTime.Now;
            Alarms.Add(notification);
            OnNotificationOccurred();

        }

        public async Task BackUpLogFiles(string message)
        {
            Notification notification = new Notification();
            
            try
            {

                string baseDir = @"C:\Kiosk\errorlogs\";
                string zipPath = @$"{ CurrentSettings.Kiosk.ReportSaveLocation}errorLog.zip";
                notification.Message = string.Format(message, zipPath);
                
                if (File.Exists(zipPath))
                {
                    File.Delete(zipPath);
                }

                ZipFile.CreateFromDirectory(baseDir, zipPath);

            }
            catch
            {
                notification.EventCode = FkEventCode.System_LogBackup_Fail;
            };

            try
            {
                string baseDir = @"C:\Kiosk\Log\";
                string zipPath = @$"{ CurrentSettings.Kiosk.ReportSaveLocation}Log.zip";
                if (File.Exists(zipPath))
                {
                    File.Delete(zipPath);
                }
                var temp= string.Format(message, zipPath);
                notification.Message = $"{notification.Message} and {temp}";
                ZipFile.CreateFromDirectory(baseDir, zipPath);
                notification.EventCode = FkEventCode.System_LogBackup_Success;
            }
            catch 
            {
                notification.EventCode = FkEventCode.System_LogBackup_Fail; 
            };


            notification.Level = FkErrorLevel.Info;
            notification.Timestamp = DateTime.Now;
            Alarms.Add(notification);
            OnNotificationOccurred();
        }

        /// <summary>
        /// login as an asynchronous operation.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="passwordHash">The password hash.</param>
        /// <returns>UserLogin.</returns>
        public async Task LoginAsync(string username, string passwordHash)
        {
            try
            {
                try
                {
                    UserLogin userLogin = await DAL.GetUserLogin(username, passwordHash, _ct).ConfigureAwait(false);
                    userLogin.CopyTo(CurrentUser);
                    Log.Info(FkEventCode.Login_Successfull, username);
                    OnLoginChanged();
                }
                catch (DatabaseException dex)
                {
                    throw new KioskManagerException(dex, FkEventCode.Login_Database_Unavailable, username);
                }
                catch (InvalidUsernameOrPasswordException iuex)
                {
                    throw new KioskManagerException(iuex, FkEventCode.Invalid_Login, username);
                }
                catch (UserLockedException ulex)
                {
                    throw new KioskManagerException(ulex, FkEventCode.User_account_locked, username);
                }
                catch (UserExpiredException upex)
                {
                    throw new KioskManagerException(upex, FkEventCode.Password_is_expired_and_must_be_updated_through_PCE, username);
                }
                catch (UserRightsException urex)
                {
                    throw new KioskManagerException(urex, FkEventCode.User_does_not_have_login_rights, username);
                }
                catch (Exception ex)
                {
                    throw new KioskManagerException(ex, FkEventCode.Error_Logging_In_User, username, ex.Message);
                }
            }
            catch (KioskManagerException lme)
            {
                Log.Error(lme);
                OnLoginChanged(lme);
                throw lme;
            }
        }

        public async Task<List<Rights>> GetAllRightsAsync()
        {
            try
            {
                var returnUsers = await DAL.GetRights(_ct).ConfigureAwait(false);
                return returnUsers;
            }
            catch (DatabaseException dex)
            {
                throw new KioskManagerException(dex, FkEventCode.Database_Unavailable);
            }
            catch (Exception ex)
            {
                throw new KioskManagerException(ex, FkEventCode.Database_Error, ex.Message);
            }

        }

        public async Task<List<Users>> GetAllUsersAsync()
        {
            try
            {
                var returnUsers = await DAL.GetUsers(_ct).ConfigureAwait(false);
                return returnUsers;
            }
            catch (DatabaseException dex)
            {
                throw new KioskManagerException(dex, FkEventCode.Database_Unavailable);
            }
            catch (Exception ex)
            {
                throw new KioskManagerException(ex, FkEventCode.Database_Error, ex.Message);
            }

        }

        public async Task SaveUserAsync(Users saveuser, List<UsersRights> newrights, SaveState savestate, Users founduser)
        {
            List<UsersRights> addlist = new List<UsersRights>();
            List<UsersRights> removelist = new List<UsersRights>();
            try
            {
                foreach (var userR in newrights)
                {
                    var found = saveuser.UsersRights.FirstOrDefault(f => f.IdRight == userR.IdRight);
                    if (found == null)
                    {
                        addlist.Add(userR);
                    }
                }
                foreach (var userR in saveuser.UsersRights)
                {
                    var found = newrights.FirstOrDefault(f => f.IdRight == userR.IdRight);
                    if (found == null)
                    {
                        removelist.Add(userR);
                    }
                }

                await DAL.SaveUser(saveuser, newrights, savestate, _ct).ConfigureAwait(false);
                switch (savestate)
                {
                    case SaveState.Add:

                        Log.Info(FkEventCode.User_Updated_Name, saveuser.StrName);
                        Log.Info(FkEventCode.User_Updated_Description, saveuser.StrName, saveuser.StrDescription);
                        Log.Info(FkEventCode.User_Updated_Active, saveuser.StrName, saveuser.BitValid);
                        Log.Info(FkEventCode.User_save_Password, saveuser.StrName);
                        foreach (var userR in addlist)
                        {
                            Log.Info(FkEventCode.User_Added_Rights, userR.IdRightNavigation.StrName, saveuser.StrName);
                        }

                        break;
                    case SaveState.Update:
                        if (string.Compare(saveuser.StrName, founduser.StrName) > 0)
                        {
                            Log.Info(FkEventCode.User_Updated_Name, saveuser.StrName);
                        }
                        if (string.Compare(saveuser.StrPassword, founduser.StrPassword) > 0)
                        {
                            Log.Info(FkEventCode.User_save_Password, saveuser.StrName);
                        }

                        if (string.Compare(saveuser.StrDescription, founduser.StrDescription) > 0)
                        {
                            Log.Info(FkEventCode.User_Updated_Description, saveuser.StrName, saveuser.StrDescription);
                        }

                        if (saveuser.BitValid != founduser.BitValid)
                        {
                            Log.Info(FkEventCode.User_Updated_Active, saveuser.StrName, saveuser.BitValid);
                        }
                        foreach (var userR in addlist)
                        {
                            Log.Info(FkEventCode.User_Added_Rights, userR.IdRightNavigation.StrName, saveuser.StrName);
                        }

                        foreach (var userR in removelist)
                        {
                            Log.Info(FkEventCode.User_Removed_Rights, userR.IdRightNavigation.StrName, saveuser.StrName);
                        }
                        break;
                }
                Log.Info(FkEventCode.User_save_success, saveuser.StrName);
            }
            catch (DatabaseException dex)
            {
                throw new KioskManagerException(dex, FkEventCode.User_save_database_error, saveuser.StrName);

            }
            catch (Exception ex)
            {
                throw new KioskManagerException(ex, FkEventCode.User_save_error, saveuser.StrName, ex.Message);
            }
        }

        public async Task<List<Logs>> GetLogsAsync()
        {
            try
            {

                var tempitems = await DAL.GetLogItems(_ct).ConfigureAwait(false);

                return tempitems;

            }
            catch (DatabaseException dex)
            {
                throw new KioskManagerException(dex, FkEventCode.Database_Unavailable);
            }
            catch (Exception ex)
            {
                throw new KioskManagerException(ex, FkEventCode.Database_Error, ex.Message);
            }
        }

        public Task LogoutAsync()
        {
            string username = CurrentUser?.Username ?? "?";
            try
            {
                if (CurrentUser.IsLoggedIn)
                {
                    CurrentUser.Reset();
                    OnLoginChanged();
                }
                else
                {
                    CurrentUser.Reset(); // Just in case
                }
                Log.Info(FkEventCode.Logout_Successfull, username);
                return Task.CompletedTask;
            }
            catch (Exception ex)
            {
                KioskManagerException lme = new KioskManagerException(ex, FkEventCode.Error_Logging_Out_User, username, ex.Message);
                throw lme;
            }
        }

        public event EventHandler<KioskManagerException> LoginChanged;
        private void OnLoginChanged(KioskManagerException ex = null)
        {
            try { LoginChanged?.Invoke(this, ex); } catch { }
        }

        /// <summary>
        /// Read settings without loading them
        /// </summary>
        /// <param name="ct">The cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
        /// <returns>ParseSettingsResult.</returns>
        private async Task<ParseSettingsResult> ReadSettings(CancellationToken ct)
        {
            var dbSettingsResult = await DAL.GetSettingsAsync(ct).ConfigureAwait(false);
            ParseSettingsResult parseSettingsResult = SettingsParser.ParseDbSettings(dbSettingsResult.LineName, dbSettingsResult.ProductNames, dbSettingsResult.DbSettings);
            return parseSettingsResult;
        }

        /// <summary>
        /// Loads the settings.
        /// </summary>
        /// <param name="ct">The cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
        private async Task LoadSettings(CancellationToken ct)
        {
            ParseSettingsResult parseSettingsResult = await ReadSettings(ct);
            ParsedSettings.ImportResult(parseSettingsResult);
            await RefreshDeviceSettings().ConfigureAwait(false);
            await GetDBSize().ConfigureAwait(false);
            OnSettingsLoaded();
        }

        /// <summary>
        /// Adds the product settings.
        /// </summary>
        /// <param name="newProductName">New name of the product.</param>
        //public async Task AddProductSettings(string newProductName)
        //{
        //    CancellationToken ct = _ct;
        //    try
        //    {
        //        string lineName;
        //        List<string> productNames;
        //        List<DbSetting> dbSettings;
        //        lock (ParsedSettings.SyncRoot)
        //        {
        //            lineName = ParsedSettings.KioskName;
        //            productNames = new List<string>(ParsedSettings.ProductNames);
        //            dbSettings = new List<DbSetting>(ParsedSettings.ParsedDbSettings);
        //        }
        //        if (!productNames.Contains(newProductName, StringComparer.OrdinalIgnoreCase))
        //        {
        //            productNames.Add(newProductName);
        //            // Save existing settings with just the new Sku added
        //            await DAL.SetSettingsAsync(lineName, productNames, dbSettings, ct).ConfigureAwait(false);
        //            // Read settings back which will include the new sku settings
        //            ParseSettingsResult settings = await ReadSettings(_ct).ConfigureAwait(false);
        //            // Save the settings with the new sku settings included
        //            await SaveSettings(settings, ct).ConfigureAwait(false);
        //            Log.Info(FkEventCode.Sku_Added, newProductName);
        //            // Reload all settings with the new skus settings
        //            await LoadSettings(ct).ConfigureAwait(false);
        //            OnProductNameAdded(newProductName);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        // TODO: Log Error after logging resources created in lib
        //        throw;
        //    }
        //}

        /// <summary>
        /// Changes the settings.
        /// </summary>
        /// <param name="changedDbSettings">The changed database settings.</param>
        public async Task ChangeSettings(IList<ChangedDbSetting> changedDbSettings)
        {
            CancellationToken ct = _ct;
            try
            {
                string lineName;
                List<string> productNames;
                List<DbSetting> dbSettings;
                lock (ParsedSettings.SyncRoot)
                {
                    lineName = ParsedSettings.KioskName;
                    productNames = new List<string>(ParsedSettings.ProductNames);
                    dbSettings = new List<DbSetting>(ParsedSettings.ParsedDbSettings);
                }
                foreach (ChangedDbSetting changedDbSetting in changedDbSettings)
                {
                    int settingIndex = dbSettings.FindIndex(p => string.Equals(p.KioskName, changedDbSetting.KioskName, StringComparison.OrdinalIgnoreCase)
                        && string.Equals(p.DeviceName, changedDbSetting.DeviceName, StringComparison.OrdinalIgnoreCase)
                        && string.Equals(p.ProductName, changedDbSetting.ProductName, StringComparison.OrdinalIgnoreCase)
                        && string.Equals(p.Name, changedDbSetting.Name, StringComparison.OrdinalIgnoreCase));

                    if (settingIndex >= 0)
                    {
                        dbSettings[settingIndex] = changedDbSetting;
                    }
                    else
                    {
                        dbSettings.Add(changedDbSetting);
                    }


                    if (changedDbSetting.Level == SettingLevel.Kiosk)
                    {
                        if (changedDbSetting.IsReasonForChangeRequired)
                        {
                            Log.Info(FkEventCode.system_settings_view_updated_line_setting_with_reason,
                                changedDbSetting.DeviceName,
                                changedDbSetting.Name,
                                changedDbSetting.OldValue,
                                changedDbSetting.Value,
                                changedDbSetting.ReasonForChange);
                        }
                        else
                        {
                            Log.Info(FkEventCode.system_settings_view_updated_line_setting,
                                changedDbSetting.DeviceName,
                                changedDbSetting.Name,
                                changedDbSetting.OldValue,
                                changedDbSetting.Value);
                        }
                    }
                    else // Product settings
                    {
                        if (changedDbSetting.IsReasonForChangeRequired)
                        {
                            Log.Info(FkEventCode.system_settings_view_updated_product_setting,
                                changedDbSetting.ProductName,
                                changedDbSetting.DeviceName,
                                changedDbSetting.Name,
                                changedDbSetting.OldValue,
                                changedDbSetting.Value);
                        }
                        else
                        {
                            Log.Info(FkEventCode.system_settings_view_updated_product_setting_with_reason,
                                changedDbSetting.ProductName,
                                changedDbSetting.DeviceName,
                                changedDbSetting.Name,
                                changedDbSetting.OldValue,
                                changedDbSetting.Value,
                                changedDbSetting.ReasonForChange);
                        }
                    }
                }
                await DAL.SetSettingsAsync(lineName, productNames, dbSettings, ct).ConfigureAwait(false);
                Log.Info(FkEventCode.Settings_saved, changedDbSettings.Count.ToString());
                OnSettingsSaved(changedDbSettings);
            }
            catch (Exception ex)
            {
                // TODO: Log exception after logging resources created in lib
                Log.Error(ex, FkEventCode.Error_saving_settings_changes, ex.Message);
                throw;
            }
            await LoadSettings(ct).ConfigureAwait(false);
        }

        /// <summary>
        /// Saves the settings.
        /// </summary>
        /// <param name="parseSettingsResult">The parse settings result.</param>
        /// <param name="ct">The cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
        private async Task SaveSettings(ParseSettingsResult parseSettingsResult, CancellationToken ct)
        {
            string lineName;
            List<string> productNames;
            List<DbSetting> dbSettings;
            lock (parseSettingsResult.SyncRoot)
            {
                lineName = parseSettingsResult.KioskName;
                productNames = new List<string>(parseSettingsResult.ProductNames);
                dbSettings = new List<DbSetting>(parseSettingsResult.ParsedDbSettings);
            }
            await DAL.SetSettingsAsync(lineName, productNames, dbSettings, ct).ConfigureAwait(false);
        }

        private async Task RefreshDeviceSettings()
        {
            KioskManagerSettings skuSettings = null;
            CancellationToken ct = _ct;

            string selectedProduct = null; //ParsedSettings.GetDefaultSettings()?.Kiosk?.SelectedProductName;
            skuSettings = ParsedSettings.GetProductSettings(selectedProduct);

            CurrentSettings = skuSettings;

            await Task.WhenAll(VerifyReader.ConfigureAsync(RFIDReader.ConfigType.Validator)
            ).ConfigureAwait(true);

            await Task.WhenAll(MainReader.ConfigureAsync(RFIDReader.ConfigType.Main)
            ).ConfigureAwait(false);

            await Task.WhenAll(WriterReader.ConfigureAsync(RFIDReader.ConfigType.Writer)
                ).ConfigureAwait(false);



            _ = MainReader.DeleteAllOpSequencesAsync();
            _ = WriterReader.DeleteAllOpSequencesAsync();
            _ = VerifyReader.DeleteAllOpSequencesAsync();
           

            if (CurrentSettings.Kiosk.AllowLookUp && RemoteDAL == null)
            {
                string remoteconnectionString = string.Empty ;

                if(CurrentSettings.Kiosk.LookUpUserName.ToUpper()=="WINDOWS")
                {

                   
                    //    Server = "192.168.23.100"; Database = "qc_log"; Uid = "dandrus"; 

                    remoteconnectionString = $"server=192.168.23.100;user=dandrus;database=qc_log;port=3306;password=pfmq@eA1";
                }
                else 
                {
                    remoteconnectionString = $"server={CurrentSettings.Kiosk.LookUpServer};user=dandrus;database={CurrentSettings.Kiosk.LookUpDb};port=3306;password=pfmq@eA1";
                }


                RemoteTableContext.ConnectionString = remoteconnectionString;
                RemoteDAL = new RemoteTableManagerDAL();

            }
            else
            {
                RemoteDAL = null;
            }
            
        }

        /// <summary>
        /// Occurs when [settings saved].
        /// </summary>
        public event EventHandler<ChangedSettingsEventArgs> SettingsSaved;
        /// <summary>
        /// Called when [settings saved].
        /// </summary>
        /// <param name="changedDbSettings">The changed database settings.</param>
        private void OnSettingsSaved(IList<ChangedDbSetting> changedDbSettings)
        {
            try { SettingsSaved?.Invoke(this, new ChangedSettingsEventArgs(changedDbSettings)); } catch { }
        }

        /// <summary>
        /// Occurs when [product name added].
        /// </summary>
        public event EventHandler<ProductNameEventArgs> ProductNameAdded;
        /// <summary>
        /// Called when [product name added].
        /// </summary>
        /// <param name="sku">The sku.</param>
        private void OnProductNameAdded(string sku)
        {
            try { ProductNameAdded?.Invoke(this, new ProductNameEventArgs(sku)); } catch { }
        }

        public event EventHandler<CultureInfo> LanguageChanged;
        private void OnLanguageChanged(CultureInfo cultureInfo)
        {
            try { LanguageChanged?.Invoke(this, cultureInfo); } catch { }
        }

        public event EventHandler SettingsLoaded;
        /// <summary>
        /// Called when [settings loaded].
        /// </summary>
        private void OnSettingsLoaded()
        {
            KioskManagerSettings defaultSettings = ParsedSettings.GetDefaultSettings();

            CultureInfo defaultCulture = CultureInfo.GetCultureInfo("en-US");
            CultureInfo selectedCulture = defaultCulture;
            if (defaultSettings?.Kiosk?.Language != null)
            {
                try
                {
                    selectedCulture = CultureInfo.GetCultureInfo(defaultSettings.Kiosk.Language);
                }
                catch
                {
                    selectedCulture = defaultCulture;
                }
            }
            if (SelectedCulture != selectedCulture)
            {
                SelectedCulture = selectedCulture;
                Thread.CurrentThread.CurrentCulture = selectedCulture;
                CultureInfo.DefaultThreadCurrentCulture = selectedCulture;
                StringResources.CurrentCulture = selectedCulture;
                EventStrings.Culture = selectedCulture;
                LibStrings.Culture = selectedCulture;

                lock (TagsSyncRoot)
                {
                    foreach (KioskScannedItem tag in Tags)
                    {
                        tag.RefreshProperties();
                    }
                }

                OnLanguageChanged(selectedCulture);
            }

            try { SettingsLoaded?.Invoke(this, EventArgs.Empty); } catch { }
        }

        private async Task ProcessLogChannelAsync(ChannelReader<Logs> logsReader)
        {
            try
            {
                Logs log = null;
                Exception lastError = null;
                while (!logsReader.Completion.IsCompleted)
                {
                    try
                    {
                        if (log == null)
                        {
                            log = await logsReader.ReadAsync().ConfigureAwait(false);
                        }
                        if (log != null)
                        {
                            await DAL.WriteLog(log, CancellationToken.None).ConfigureAwait(false);
                            log = null;
                        }
                        if (lastError != null)
                        {
                            lastError = null;
                            Log.Info(FkEventCode.Log_Queue_Processor_Recovered);
                        }
                    }
                    catch (ChannelClosedException) { }
                    catch (OperationCanceledException) { }
                    catch (Exception ex)
                    {
                        if (lastError == null || ex.Message != lastError.Message)
                        {
                            Log.Error(ex, FkEventCode.Error_with_Log_Queue_Processor, ex.Message);
                        }
                        lastError = ex;
                        await Task.Delay(1000).ConfigureAwait(false);
                    }
                }
            }
            catch (OperationCanceledException) { }
            catch (Exception ex)
            {
                Log.Error(ex, FkEventCode.Error_with_Log_Queue_Processor, ex.Message);
            }
        }

        public event EventHandler NotificationOccurred;
        private void OnNotificationOccurred()
        {
            try { NotificationOccurred?.Invoke(this, EventArgs.Empty); } catch { }
        }

        public event EventHandler DatabaseSizeUpdated;
        private void OnDatabaseSizeUpdated()
        {
            try { DatabaseSizeUpdated?.Invoke(this, EventArgs.Empty); } catch { }
        }

        #region Dispose

        private bool disposed = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        private void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                // Free any managed objects here.
                _cts.Cancel();
                try { LogManager.Shutdown(); } catch { } // Flush and close down internal NLog threads and timers
            }

            // Free any unmanaged objects here

            disposed = true;
        }
        ~TableController()
        {
            Dispose(false);
        }

        #endregion Dispose
    }

    public class Notification
    {
        public DateTimeOffset Timestamp { get; set; } = DateTimeOffset.Now;

        public string Message { get; set; }

        public FkEventCode EventCode { get; set; }

        public int EventCodeNumber => (int)EventCode;

        public FkErrorLevel Level { get; set; }

        public Exception Error { get; set; }

    }

    /// <summary>
    /// Class ChangedSettingsEventArgs.
    /// Implements the <see cref="System.EventArgs" />
    /// </summary>
    /// <seealso cref="System.EventArgs" />
    public class ChangedSettingsEventArgs : EventArgs
    {
        /// <summary>
        /// Gets the changed database settings.
        /// </summary>
        /// <value>The changed database settings.</value>
        public List<ChangedDbSetting> ChangedDbSettings { get; }
        /// <summary>
        /// Initializes a new instance of the <see cref="ChangedSettingsEventArgs" /> class.
        /// </summary>
        /// <param name="changedDbSettings">The changed database settings.</param>
        public ChangedSettingsEventArgs(IList<ChangedDbSetting> changedDbSettings)
        {
            ChangedDbSettings = new List<ChangedDbSetting>(changedDbSettings);
        }
    }

    /// <summary>
    /// Class ProductNameEventArgs.
    /// Implements the <see cref="System.EventArgs" />
    /// </summary>
    /// <seealso cref="System.EventArgs" />
    public class ProductNameEventArgs : EventArgs
    {
        /// <summary>
        /// Gets the name of the product.
        /// </summary>
        /// <value>The name of the product.</value>
        public string ProductName { get; }
        /// <summary>
        /// Initializes a new instance of the <see cref="ProductNameEventArgs" /> class.
        /// </summary>
        /// <param name="productName">Name of the product.</param>
        public ProductNameEventArgs(string productName)
        {
            ProductName = productName;
        }
    }
}
//private async Task SetupReaders()
//{



//    //  impinjMainReader = new ImpinjReader();
//    // impinjWriter = impinjMainReader; // in case we decide to go with 3 readers
//    // impinjVerifyReader = new ImpinjReader();

//    //string line;
//    //using (StreamReader sr = new StreamReader(STR_IPADDRESS_FILE))
//    //{
//    //    line = sr.ReadLine();
//    //    try
//    //    {
//    //        await impinjMainReader.ConnectAsync(line, LLRPConstants.LLRP_PORT, false).ConfigureAwait(false);
//    //    }
//    //    catch (OctaneSdkException ex)
//    //    {
//    //        Logger.Error(ex.Message);
//    //        CanStart = false;
//    //        return;
//    //    }
//    //}
//    //using (StreamReader sr = new StreamReader(STR_IPADDRESS_FILE))
//    //{
//    //    line = sr.ReadLine(); // Primary reader on 1st line, our is on 2nd.
//    //    line = sr.ReadLine();
//    //    try
//    //    {
//    //        await impinjVerifyReader.ConnectAsync(line, LLRPConstants.LLRP_PORT, false).ConfigureAwait(false);
//    //    }
//    //    catch (OctaneSdkException ex)
//    //    {
//    //        Logger.Error(ex.Message);
//    //        CanStart = false;
//    //        return;
//    //    }
//    //}
//    CanStart = true;
//}
//public static List<string> GetDirectories(string path, string searchPattern = "*",
//SearchOption searchOption = SearchOption.AllDirectories)
//{
//    if (searchOption == SearchOption.TopDirectoryOnly)
//        return Directory.GetDirectories(path, searchPattern).ToList();

//    var directories = new List<string>(GetDirectories(path, searchPattern));

//    //for (var i = 0; i < directories.Count; i++)
//    //    directories.AddRange(GetDirectories(directories[i], searchPattern));

//    return directories;
//}
//private bool CheckForInternetConnection()
//{
//    try
//    {
//        using (var client = new WebClient())
//        using (var stream = client.OpenRead("http://www.google.com"))
//        {
//            return true;
//        }
//    }
//    catch (Exception ex)
//    {
//        KioskLogger.WriteToErrorLog(ErrorLevel.Info, new LogMessage() { Error = ex, ErrorCode = 0, Messagetext = "Error Connecting to internet", Timestamp = DateTime.Now });
//        return false;
//    }
//}

//public void CreateErrorLog()
//{
//    try
//    {
//        var firstError = Kiosksettings.ErrorSettings.FirstOrDefault();
//        if (!string.IsNullOrWhiteSpace(firstError.PurgeDelay))
//        {
//            var newdate = DateTime.Now.ToString("yyyyMMdd");

//            var newfiledateFile = $"{NewfilePathSettings.ErrorLogPath}AK_ERRORLOG_{newdate}.json";

//            if (!File.Exists(newfiledateFile))
//            {
//                var newErrorFile = new ErrorLogModel()
//                {
//                    CreateDate = DateTime.Now.ToString("yyyy/MM/dd"),
//                    Errors = new List<ErrorItems>()
//                };

//                var json = JsonConvert.SerializeObject(newErrorFile);
//                System.IO.File.WriteAllText(newfiledateFile, json);
//            }

//            foreach (var file in Directory.EnumerateFiles(NewfilePathSettings.ErrorLogPath, "*.json"))
//            {

//                var json1 = File.ReadAllText(file);
//                ErrorLogModel errorLogModel = JsonConvert.DeserializeObject<ErrorLogModel>(json1);

//                DateTime ogDate = Convert.ToDateTime(errorLogModel.CreateDate);

//                var Addeddays = ogDate.AddDays(Convert.ToDouble(firstError.PurgeDelay));

//                int result = DateTime.Compare(Addeddays, DateTime.Now);

//                if (result < 0)
//                {
//                    File.Delete(file);
//                }

//            }
//        }
//    }
//    catch (Exception ex)
//    {
//        KioskLogger.WriteToErrorLog(ErrorLevel.Info, new LogMessage() { Error = ex, ErrorCode = 0, Messagetext = "Error Creating ErrorLog", Timestamp = DateTime.Now });

//    }
//}
//public void WriteToErrorLog(NewErrorModel NewErrorItem)
//{
//    try
//    {
//        CreateErrorLog();

//        if (NewErrorItem != null)
//        {
//            var newdate = DateTime.Now.ToString("yyyyMMdd");

//            var newfiledateFile = $"{NewfilePathSettings.ErrorLogPath}AK_ERRORLOG_{newdate}.json";


//            var json = File.ReadAllText(newfiledateFile);
//            ErrorLogModel errorLogModel = JsonConvert.DeserializeObject<ErrorLogModel>(json);

//            errorLogModel.Errors.Add(new ErrorItems()
//            {

//                ErrorDateTime = NewErrorItem.ErrorDateTime.ToString("yyyy/MM/dd"),
//                Error = NewErrorItem.Error,
//                SKU = NewErrorItem.SKU,
//                RFID = NewErrorItem.RFID
//            }
//            );
//            // Add any Errors
//            // Update json data string
//            json = JsonConvert.SerializeObject(errorLogModel);
//            System.IO.File.WriteAllText(newfiledateFile, json);
//        }

//    }
//    catch (Exception ex)
//    {
//        KioskLogger.WriteToErrorLog(ErrorLevel.Info, new LogMessage() { Error = ex, ErrorCode = 0, Messagetext = $"Error in {ex.TargetSite}", Timestamp = DateTime.Now });
//    }


//    //    WriteToErrorLog();
//}
//public void WriteToErrorLog()
//{

//}
//public void ReadTotalSKUFile()
//{
//    try
//    { 

//        if (!string.IsNullOrWhiteSpace(ActiveSKU))
//        {
//            var readpath = $"{NewfilePathSettings.TotalAssociationPath}{ActiveSKU}-{DateTime.Now.ToString("yyyyMMdd")}.json";

//            if (File.Exists(readpath))
//            {
//                var json = File.ReadAllText(readpath);
//                CurrentAssociatedSkus = JsonConvert.DeserializeObject<TotalSkuAssociationModel>(json);
//            }

//            else
//            {
//                CurrentAssociatedSkus = new TotalSkuAssociationModel()
//                {
//                    SKU = ActiveSKU,
//                    ScannedItems = new List<ScannedItem>()
//                {                     
//                    }
//                };

//                var json = JsonConvert.SerializeObject(CurrentAssociatedSkus);
//                System.IO.File.WriteAllText(readpath, json);
//            }

//            CurrentThingID = string.Empty;

//        }

//    }
//    catch (Exception ex)
//    {
//        KioskLogger.WriteToErrorLog(ErrorLevel.Info, new LogMessage() { Error = ex, ErrorCode = 0, Messagetext = $"Error in {ex.TargetSite}", Timestamp = DateTime.Now });
//    }




//}
//public void WriteTotalSkuAssociationLog(string ThingID)
//{
//    if (CurrentAssociatedSkus != null)
//    {
//        var foundThingID = CurrentAssociatedSkus.ScannedItems.FirstOrDefault(f => f.ThingID == ThingID);

//        if(foundThingID ==null || foundThingID.ThingID == null)
//        {
//            CurrentThingID = ThingID;
//            WriteTotalSkuAssociationLog();
//        }
//    }


//}
//public void WriteTotalSkuAssociationLog()
//{

//    var savesinglepath = $"{NewfilePathSettings.TotalAssociationPath}{ActiveSKU}-{DateTime.Now.ToString("yyyyMMdd")}.json";

//    if (File.Exists(savesinglepath))
//    {
//        var json = File.ReadAllText(savesinglepath);
//        CurrentAssociatedSkus = JsonConvert.DeserializeObject<TotalSkuAssociationModel>(json);

//        var newThingid = new ScannedItem()
//        {
//            ThingID = CurrentThingID
//        };

//        CurrentAssociatedSkus.ScannedItems.Add(newThingid);

//        json = JsonConvert.SerializeObject(CurrentAssociatedSkus);
//        System.IO.File.WriteAllText(savesinglepath, json);


//    }

//    else
//    {
//        CurrentAssociatedSkus = new TotalSkuAssociationModel()
//        {
//            SKU = ActiveSKU,
//            ScannedItems = new List<ScannedItem>()
//            { new ScannedItem()
//                {
//                    ThingID = CurrentThingID
//                }
//            }
//        };

//        var json = JsonConvert.SerializeObject(CurrentAssociatedSkus);
//        System.IO.File.WriteAllText(savesinglepath, json);
//    }

//    CurrentThingID = string.Empty;

//}
//public void WriteSingleSkuAssociationLog(SingleSkuAssociationModel SingleSku)
//{

//    if (SingleSku != null)
//    {
//        var savesinglepath = $"{NewfilePathSettings.SingleAssociationPath}{DateTime.Now.ToString("yyyyMMddhhmmss")}.json";
//        var json = JsonConvert.SerializeObject(SingleSku);
//        System.IO.File.WriteAllText(savesinglepath, json);
//    }
//    SingleSKUAdd = null;

//}
//public void WriteSingleSkuAssociationLog()
//{

//}
//public void ReadSettings()
//{
//    if (File.Exists(NewfilePathSettings.SettingsPath))
//    {
//        var json = File.ReadAllText(NewfilePathSettings.SettingsPath);
//        Kiosksettings = JsonConvert.DeserializeObject<KioskManagerSettings>(json);
//    }
//    else
//    {
//        Kiosksettings = new KioskManagerSettings()
//        {
//            KioskID = "eAgile01",
//            AWSSendInterval = "30",
//            RfidReader = new List<ReaderSetting>()
//            {
//                new ReaderSetting()
//                {
//                    DeviceName = "RFID Reader",
//                    IsEnabled = true,
//                    Address = "192.168.0.42",
//                    Port = 5084,
//                    HeartbeatInterval = 5000,
//                    ConnectTimeout = 5000,
//                    CommandTimeout = 1000,
//                    UseETSI = false,
//                    EtsiTxFrequenciesInMhz = new List<double>(),
//                    ReaderMode = ReaderMode.AutoSetDenseReaderDeepScan,
//                    SearchMode = SearchMode.SingleTarget,
//                    Session = Session.S1,
//                    TagPopulationEstimate = 32,
//                    TxPowerInDbm = 20,
//                    RxSensitivityInDbm = -60,
//                    Antennas = new List<int>() { 1 },
//                    ScanDuration = 5000,
//                    LockCheck = true,
//                    UserCheck = true,
//                },
//            },
//            BarCodeSettings = new List<BarCodeSetting>
//            {
//                new BarCodeSetting ()
//                {
//                    BarcodeAddress = "COM4",
//                    BarcodeLength= "11"
//                }
//            },

//            ErrorSettings = new List<ErrorSetting>
//            {
//                new ErrorSetting
//                {
//                    Level="error",
//                    PurgeDelay="90",
//                    submit= "Daily"

//                }
//            },

//            LogSettings = new List<LogSetting>
//            {
//                new LogSetting()
//                {
//                    Submit="daily",
//                    PurgeDelay="90"
//                }
//            },
//            tagSettings = new List<TagSetting>()
//            {
//                new TagSetting()
//                {
//                    TidHeader= "daily",
//                    epcHeader="01"
//                }
//            }

//        };

//        var json = JsonConvert.SerializeObject(Kiosksettings);
//        System.IO.File.WriteAllText(NewfilePathSettings.SettingsPath, json);
//    }

//    ReaderSetting testReaderSettings = new ReaderSetting()
//    {
//        DeviceName = "RFID Reader",
//        IsEnabled = true,
//        Address = "192.168.0.42",
//        Port = 5084,
//        HeartbeatInterval = 5000,
//        ConnectTimeout = 5000,
//        CommandTimeout = 5000,
//        UseETSI = false,
//        EtsiTxFrequenciesInMhz = new List<double>(),
//        ReaderMode = ReaderMode.AutoSetDenseReaderDeepScan,
//        SearchMode = SearchMode.SingleTarget,
//        Session = Session.S1,
//        TagPopulationEstimate = 32,
//        TxPowerInDbm = 20,
//        RxSensitivityInDbm = -60,
//        Antennas = new List<int>() { 1 },
//        ScanDuration = 5000,
//        LockCheck = true,
//        UserCheck = true,
//    };

//    try
//    {
//        // TODO: move this to an async loader of some sort
//        RfidReader.ConfigureAsync(testReaderSettings).GetAwaiter().GetResult();
//    }
//    catch (Exception ex)
//    {
//        string msg = ex.Message;
//    }
//}
//public void ReadConnectionSettings()
//{
//    try { 
//        if (File.Exists(NewfilePathSettings.ConnectionPath))
//        {

//            var firstjson = File.ReadAllText(NewfilePathSettings.ConnectionPath);
//            KioskConnectionSettings = JsonConvert.DeserializeObject<AWSConnectionModel>(firstjson);

//            if(KioskConnectionSettings.NewConnection.Contains("True"))
//            {
//                WriteConnectionSettings(KioskConnectionSettings);
//            }


//            var json = File.ReadAllText(NewfilePathSettings.ConnectionPath);
//            KioskConnectionSettings = JsonConvert.DeserializeObject<AWSConnectionModel>(json);


//            if (!string.IsNullOrEmpty(KioskConnectionSettings.ConnectionAmway))
//            {
//                KioskConnectionSettings.ConnectionAmway = Encryption.Encryption.DecryptStringAES(KioskConnectionSettings.ConnectionAmway, ClientSharedSecret);
//                KioskConnectionSettings.connectionEA= Encryption.Encryption.DecryptStringAES(KioskConnectionSettings.connectionEA, ClientSharedSecret);
//                KioskConnectionSettings.UserEA = Encryption.Encryption.DecryptStringAES(KioskConnectionSettings.UserEA, ClientSharedSecret);
//                KioskConnectionSettings.UserAmway = Encryption.Encryption.DecryptStringAES(KioskConnectionSettings.UserAmway, ClientSharedSecret);
//            }
//            KioskConnectionSettings.NewConnection = KioskConnectionSettings.NewConnection;
//            KioskConnectionSettings.WriteStreamName = Encryption.Encryption.DecryptStringAES(KioskConnectionSettings.WriteStreamName, ClientSharedSecret);
//            KioskConnectionSettings.WriteAccessKey = Encryption.Encryption.DecryptStringAES(KioskConnectionSettings.WriteAccessKey, ClientSharedSecret);
//            KioskConnectionSettings.WriteSecretKey = Encryption.Encryption.DecryptStringAES(KioskConnectionSettings.WriteSecretKey, ClientSharedSecret);
//            KioskConnectionSettings.WriteRegion = KioskConnectionSettings.WriteRegion;
//            KioskConnectionSettings.WriteFormat = KioskConnectionSettings.WriteFormat;
//            KioskConnectionSettings.WriteRoleARN = KioskConnectionSettings.WriteRoleARN;
//            KioskConnectionSettings.SupportStreamName = Encryption.Encryption.EncryptStringAES(KioskConnectionSettings.SupportStreamName, ClientSharedSecret);
//            KioskConnectionSettings.SupportAccessKey = Encryption.Encryption.EncryptStringAES(KioskConnectionSettings.SupportAccessKey, ClientSharedSecret);
//            KioskConnectionSettings.SupportSecretKey = Encryption.Encryption.EncryptStringAES(KioskConnectionSettings.SupportSecretKey, ClientSharedSecret);
//            KioskConnectionSettings.SupportRegion = KioskConnectionSettings.SupportRegion;
//            KioskConnectionSettings.SupportFormat = KioskConnectionSettings.SupportFormat;
//            KioskConnectionSettings.SupportRoleARN = KioskConnectionSettings.SupportRoleARN;


//        }
//        else
//        {
//            AWSConnectionModel CreateNewConnection = new AWSConnectionModel()
//            {
//                NewConnection  ="True",
//                WriteStreamName = "thing-event-input-stream-test",
//                WriteAccessKey = "AKIAXQU5NULGPXCDLL6Z",
//                WriteSecretKey = "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",
//                WriteRegion = "us-east-1",
//                WriteFormat = "json",
//                WriteRoleARN = "arn:aws:iam::983054836469:role/thing-event-input-stream-role-test",

//                ConnectionAmway = "ConnectionAmway",
//                UserAmway = "UserAmway",
//                connectionEA = "connectionEA",
//                UserEA = "UserEA",
//                SupportStreamName = "eagile_test",
//                SupportAccessKey = "AKIAXQU5NULGDTB3UT6N",
//                SupportSecretKey = "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",
//                SupportRegion = "us-east-2",
//                SupportFormat = "json",
//                SupportRoleARN = "SupportRoleARN"
//            };

//            var json = JsonConvert.SerializeObject(CreateNewConnection);
//            System.IO.File.WriteAllText(NewfilePathSettings.ConnectionPath, json);

//            ReadConnectionSettings();
//        }

//    }
//    catch (Exception ex)
//    {
//        KioskLogger.WriteToErrorLog(ErrorLevel.Info, new LogMessage() { Error = ex, ErrorCode = 0, Messagetext = $"Error in {ex.TargetSite}", Timestamp = DateTime.Now });
//    }

//}
//public void WriteConnectionSettings(AWSConnectionModel kioskConnectionSettings)
//{
//    try
//    { 
//        KioskConnectionSettings.NewConnection = "false";
//        KioskConnectionSettings.ConnectionAmway = Encryption.Encryption.EncryptStringAES(kioskConnectionSettings.ConnectionAmway, ClientSharedSecret);
//        KioskConnectionSettings.UserAmway = Encryption.Encryption.EncryptStringAES(kioskConnectionSettings.ConnectionAmway, ClientSharedSecret);
//        KioskConnectionSettings.connectionEA = Encryption.Encryption.EncryptStringAES(kioskConnectionSettings.connectionEA, ClientSharedSecret);
//        KioskConnectionSettings.UserEA = Encryption.Encryption.EncryptStringAES(kioskConnectionSettings.UserEA, ClientSharedSecret);
//        KioskConnectionSettings.WriteStreamName = Encryption.Encryption.EncryptStringAES(kioskConnectionSettings.WriteStreamName, ClientSharedSecret);
//        KioskConnectionSettings.WriteAccessKey = Encryption.Encryption.EncryptStringAES(kioskConnectionSettings.WriteAccessKey, ClientSharedSecret);
//        KioskConnectionSettings.WriteSecretKey = Encryption.Encryption.EncryptStringAES(kioskConnectionSettings.WriteSecretKey, ClientSharedSecret);
//        KioskConnectionSettings.WriteRegion = kioskConnectionSettings.WriteRegion;
//        KioskConnectionSettings.WriteFormat = kioskConnectionSettings.WriteFormat;
//        KioskConnectionSettings.WriteRoleARN = kioskConnectionSettings.WriteRoleARN;
//        KioskConnectionSettings.SupportStreamName = Encryption.Encryption.EncryptStringAES(kioskConnectionSettings.SupportStreamName, ClientSharedSecret);
//        KioskConnectionSettings.SupportAccessKey = Encryption.Encryption.EncryptStringAES(kioskConnectionSettings.SupportAccessKey, ClientSharedSecret);
//        KioskConnectionSettings.SupportSecretKey = Encryption.Encryption.EncryptStringAES(kioskConnectionSettings.SupportSecretKey, ClientSharedSecret);
//        KioskConnectionSettings.SupportRegion = kioskConnectionSettings.SupportRegion;
//        KioskConnectionSettings.SupportFormat = kioskConnectionSettings.SupportFormat;
//        KioskConnectionSettings.SupportRoleARN = kioskConnectionSettings.SupportRoleARN;

//        var json = JsonConvert.SerializeObject(KioskConnectionSettings);
//        System.IO.File.WriteAllText(NewfilePathSettings.FilePathSettingPath, json);

//    }
//    catch (Exception ex)
//    {
//        KioskLogger.WriteToErrorLog(ErrorLevel.Info, new LogMessage() { Error = ex, ErrorCode = 0, Messagetext = $"Error in {ex.TargetSite}", Timestamp = DateTime.Now });
//    }


//}
//private void ReadFilePathSetting()
//{
//    try
//    {
//        FileInfo filePathSettingsFile = new FileInfo(@"C:\Kiosk\settings\FilePathSetting.json");

//        if (filePathSettingsFile.Exists)
//        {
//            NewfilePathSettings = JsonGeneric.JsonDeserialize<FilePathSettingModel>(filePathSettingsFile.FullName);
//        }
//        else
//        {
//            NewfilePathSettings = new FilePathSettingModel()
//            {
//                DBConnectionPath = "C:\\Kiosk\\Settings\\Appsettings.json",
//                ErrorLogPath = "C:\\Kiosk\\errorlog\\",
//                RFIDSettingsPath = "C:\\Kiosk\\Settings\\Settings.xml",
//                SettingsPath = "C:\\ProgramData\\eAgile\\AssociationKiosk\\Settings\\AK_Settings.json",
//                SystemErrorLogPath = "C:\\Kiosk\\SystemLogs\\",
//            };

//            if (!filePathSettingsFile.Directory.Exists)
//            {
//                filePathSettingsFile.Directory.Create();
//            }

//            JsonGeneric.JsonSerializer(NewfilePathSettings, NewfilePathSettings.FilePathSettingPath);
//        }
//    }
//    catch (Exception ex)
//    {
//        //TODO: KioskLogger could throw an exception if file paths are invalid
//        //KioskLogger.WriteToErrorLog(ErrorLevel.Info, new LogMessage() { Error = ex, ErrorCode = 0, Messagetext = $"Error in {ex.TargetSite}", Timestamp = DateTime.Now });
//    }
//}

//public void WriteSKUFile(SkusItem newSku)
//{
//    try
//    { 
//        var json = File.ReadAllText(NewfilePathSettings.SKUSettingsPath);
//        SkusModel Skus = JsonConvert.DeserializeObject<SkusModel>(json);
//        SkusItem foundsku  = Skus.SkusList.FirstOrDefault(s => s.Name == newSku.Name);
//        if (foundsku == null)
//        {

//            Skus.SkusList.Add(newSku);
//            json = JsonConvert.SerializeObject(Skus);
//            System.IO.File.WriteAllText(NewfilePathSettings.SKUSettingsPath, json);
//            ActiveSKU = newSku.Name;
//            ReadSKUFile();
//        }
//    }
//    catch (Exception ex)
//    {
//        KioskLogger.WriteToErrorLog(ErrorLevel.Info, new LogMessage() { Error = ex, ErrorCode = 0, Messagetext = $"Error in {ex.TargetSite}", Timestamp = DateTime.Now });
//    }

//}
//public void ReadSKUFile()
//{
//    try 
//    {
//        CurrentSkusList = new SkusModel();
//        CurrentSkusList.SkusList = new List<SkusItem>();

//        if (File.Exists(NewfilePathSettings.SKUSettingsPath))
//        {
//            var json = File.ReadAllText(NewfilePathSettings.SKUSettingsPath);
//            CurrentSkusList = JsonConvert.DeserializeObject<SkusModel>(json);
//        }
//        else
//        {
//            var newSKU = new SkusModel()
//            {
//                SkusList = new List<SkusItem>()
//            };


//            var json = JsonConvert.SerializeObject(newSKU);
//            System.IO.File.WriteAllText(NewfilePathSettings.SKUSettingsPath, json);

//        }
//    }
//    catch (Exception ex)
//    {
//        KioskLogger.WriteToErrorLog(ErrorLevel.Info, new LogMessage() { Error = ex, ErrorCode = 0, Messagetext = $"Error in {ex.TargetSite}", Timestamp = DateTime.Now });
//    }

//}

//public event EventHandler<string> KioskEvent;
//private void OnChangeKioskOccurred(string newKioskMessage)
//{
//    try { KioskEvent?.Invoke(this, newKioskMessage); } catch { }
//}

//public void CreateDirectories()
//{
//    try
//    {
//    //System.IO.Directory.CreateDirectory(@"C:\ProgramData\eAgile\AssociationKiosk\Settings");
//    //System.IO.Directory.CreateDirectory(@"C:\ProgramData\eAgile\AssociationKiosk\TagLog");
//    //System.IO.Directory.CreateDirectory(@"C:\ProgramData\eAgile\AssociationKiosk\TagLog\Cache");
//    //System.IO.Directory.CreateDirectory(@"C:\ProgramData\eAgile\AssociationKiosk\TagLog\Archive");
//    //System.IO.Directory.CreateDirectory(@"C:\ProgramData\eAgile\AssociationKiosk\ErrorLog");
//    }
//    catch (Exception ex)
//    {
//        KioskLogger.WriteToErrorLog(ErrorLevel.Info, new LogMessage() { Error = ex, ErrorCode = 0, Messagetext = $"Error in {ex.TargetSite}", Timestamp = DateTime.Now });
//    }
//}
//public async Task ReadTags(CancellationToken ct = default)
//{
//    int count = 0;
//    try
//    {
//        lock (TagsSyncRoot)
//        {
//            Tags.Clear();
//            _reading = true;
//        }
//        Log.Info(FkEventCode.Rfid_Read_Started);
//        //await RfidReader.StartAsync().ConfigureAwait(false);
//        //await Task.Delay(RfidReader.DeviceSettings.ScanDuration, ct).ConfigureAwait(false);
//    }
//    catch (OperationCanceledException) { }
//    catch (Exception ex)
//    {
//        Log.Error(ex, FkEventCode.Rfid_Read_Error, ex.Message);
//        throw;
//    }
//    finally
//    {
//        try { await RfidReader.StopAsync().ConfigureAwait(false); } catch { }
//        lock (TagsSyncRoot)
//        {
//            _reading = false;
//            count = Tags.Count;
//        }
//        Log.Info(FkEventCode.Rfid_Read_Finished, count);
//        LastReadTime = DateTime.Now;
//    }
//}

//private void RfidReader_TagsReported(object sender, TagReport tagReport)
//{
//    try
//    {
//        foreach (Tag tag in tagReport.Tags)
//        {
//            if (tag.Tid != null && tag.Tid.CountWords > 0)
//            {
//                string tid = tag.Tid.ToHexString();

//                KioskScannedItem tagModel = new KioskScannedItem()
//                {
//                    Tid = tid,
//                };

//                try
//                {
//                    EpcData epcData = EpcEncoder.Decode(tag.Epc.ToList());
//                    tagModel.Gtin = epcData.GTIN;
//                    tagModel.SerialNumber = epcData.Serial;
//                }
//                catch { }

//                foreach (TagOpResult result in tag.TagOpResults)
//                {
//                    if (result is TagReadOpResult readResult)
//                    {
//                        if (readResult.OpId == KioskReader.RESERVED_OP_ID)
//                        {
//                            if (readResult.Result == ReadResultStatus.Success && readResult.Data != null && readResult.Data.CountWords > 0)
//                            {
//                                tagModel.LockState = readResult.Data.ToHexString();
//                            }
//                        }
//                        else if (readResult.OpId == KioskReader.USER_OP_ID)
//                        {
//                            if (readResult.Result == ReadResultStatus.Success && readResult.Data != null && readResult.Data.CountWords > 0)
//                            {
//                                try
//                                {
//                                    UserData userData = UserEncoder.Decode(readResult.Data.ToList());
//                                    tagModel.BatchLot = userData?.Batch;
//                                    tagModel.Expiry = userData?.Expiry;
//                                }
//                                catch { }
//                            }
//                        }
//                    }
//                }

//                lock (TagsSyncRoot)
//                {
//                    if (_reading)
//                    {
//                        KioskScannedItem existingTagModel = Tags.FirstOrDefault(p => string.Equals(p.Tid, tagModel.Tid));
//                        if (existingTagModel == null)
//                        {
//                            Tags.Add(tagModel);
//                        }
//                        else
//                        {
//                            if (string.IsNullOrEmpty(existingTagModel.Gtin) && !string.IsNullOrEmpty(tagModel.Gtin))
//                                existingTagModel.Gtin = tagModel.Gtin;
//                            if (string.IsNullOrEmpty(existingTagModel.SerialNumber) && !string.IsNullOrEmpty(tagModel.SerialNumber))
//                                existingTagModel.SerialNumber = tagModel.SerialNumber;
//                            if (string.IsNullOrEmpty(existingTagModel.BatchLot) && !string.IsNullOrEmpty(tagModel.BatchLot))
//                                existingTagModel.BatchLot = tagModel.BatchLot;
//                            if (string.IsNullOrEmpty(existingTagModel.Expiry) && !string.IsNullOrEmpty(tagModel.Expiry))
//                                existingTagModel.Expiry = tagModel.Expiry;
//                            if (string.IsNullOrEmpty(existingTagModel.LockState) && !string.IsNullOrEmpty(tagModel.LockState))
//                                existingTagModel.LockState = tagModel.LockState;
//                        }
//                    }
//                }
//            }
//        }

//    }
//    catch (Exception)
//    {
//        // Log?
//    }
//}

///// <summary>
///// Based on system Kiosk setting Look up. Search by TID  
///// </summary>
///// <param name="lookingForTID"></param>
///// <returns>TID last result</returns>
//public async Task LookUpScanItem(string messageString)
//{
//    List<Notification> notifications = Alarms.Where(p => p.EventCode == FkEventCode.Rfid_Read_Lookup_Success || p.EventCode == FkEventCode.Rfid_Read_Lookup_Fail).ToList();
//    if (notifications.Any())
//    {
//        notifications.ForEach(p => Alarms.Remove(p));
//    }

//    Notification notification = new Notification();
//    notification.Message = messageString;

//    if (Tags.Count > 1)
//    {
//        notification.Level = FkErrorLevel.Error;
//        notification.EventCode = FkEventCode.Rfid_Read_Lookup_Fail;
//        notification.Timestamp = DateTime.Now;
//        Alarms.Add(notification);
//        OnNotificationOccurred();
//        return;
//    }

//    ValidationResult result = await RemoteDAL.GetTID(Tags.FirstOrDefault().Tid, CurrentSettings.Kiosk.LookUpDb, _ct);

//    string lookUpResult= "PASSED";
//    switch (result)
//    {
//        case ValidationResult.NoRead:
//            lookUpResult = "NO RESULT";
//            break;
//        case ValidationResult.Success:
//            lookUpResult = "PASSED";
//            break;
//        case ValidationResult.WrongData:
//            lookUpResult = "REJECTED";
//            break;
//    }

//    Tags.FirstOrDefault().RlmStatus = lookUpResult;
//    Alarms.Add(notification);
//    notification.Level = FkErrorLevel.Info;
//    notification.EventCode = FkEventCode.Rfid_Read_Lookup_Success;
//    OnNotificationOccurred();

//}

//private void RfidReader_AntennaChanged(object sender, AntennaEvent e)
//{
//    if (e.State == AntennaEventType.AntennaConnected)
//    {
//        Log.Info(FkEventCode.Kiosk_Rfid_AntennaConnected, e.PortNumber);
//    }
//    else
//    {
//        Log.Warn(FkEventCode.Kiosk_Rfid_AntennaDisconnected, e.PortNumber);
//    }
//}

//private bool _hasReaderDisconnected = false;
//private Exception _lastRfidReaderConnectionException = null;
//private void RfidReader_ConnectionChanged(object sender, ConnectionEvent e)
//{
//    if (e.IsConnected)
//    {
//        if (_hasReaderDisconnected)
//        {
//            Log.Info(FkEventCode.Kiosk_Rfid_Connected);
//        }
//    }
//    else if (!e.IsConnected && !_ct.IsCancellationRequested)
//    {
//        _hasReaderDisconnected = true;
//        if (e.Error == null)
//        {
//            Log.Info(FkEventCode.Kiosk_Rfid_Disconnected);
//        }
//        else
//        {
//            if (_lastRfidReaderConnectionException == null || _lastRfidReaderConnectionException.Message != e.Error.Message)
//            {
//                Log.Error(e.Error, FkEventCode.Kiosk_Rfid_Disconnected_Error, e.Error.Message);
//            }
//            else
//            {
//                Log.Debug(e.Error, FkEventCode.Kiosk_Rfid_Disconnected_Error, e.Error.Message);
//            }
//        }
//    }
//    _lastRfidReaderConnectionException = e.Error;

//    bool notificationsChanged = false;
//    lock (AlarmsSyncRoot)
//    {
//        List<Notification> notifications = Alarms.Where(p => p.EventCode == FkEventCode.Kiosk_Rfid_Disconnected || p.EventCode == FkEventCode.Kiosk_Rfid_Disconnected_Error).ToList();
//        if (e.IsConnected && notifications.Any())
//        {
//            notifications.ForEach(p => Alarms.Remove(p));
//            notificationsChanged = true;
//        }
//        else if (!e.IsConnected && !notifications.Any())
//        {
//            Notification notification = new Notification();
//            notification.Level = FkErrorLevel.Error;
//            if (e.Error != null)
//            {
//                notification.EventCode = FkEventCode.Kiosk_Rfid_Disconnected_Error;
//                notification.Message = notification.EventCode.GetMessage(e.Error.Message);
//                notification.Error = e.Error;
//            }
//            else
//            {
//                notification.EventCode = FkEventCode.Kiosk_Rfid_Disconnected;
//                notification.Message = notification.EventCode.GetMessage();
//            }
//            Alarms.Add(notification);
//            notificationsChanged = true;
//        }
//    }
//    if (notificationsChanged)
//    {
//        OnNotificationOccurred();
//    }
//}

//private void RfidReader_ReadingChanged(object sender, ReadingEvent e)
//{
//    if (e.IsReading)
//    {
//        Log.Debug(FkEventCode.Kiosk_Rfid_ReadingStarted);
//    }
//    else
//    {
//        Log.Debug(FkEventCode.Kiosk_Rfid_ReadingStopped);
//    }
//}
//private void MasterList_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
//{
//    NotifyPropertyChanged(nameof(TagCollection));
//}
//private void MasterList_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
//{
//    foreach (object o in e.NewItems)
//    {
//        KeyValuePair<string, TagInfo> p = (KeyValuePair<string, TagInfo>)o;
//        try
//        {
//            TagCollectionProgress.Report(p.Value);
//        }
//        catch (Exception ex) { Logger.Error($"{ex}"); }
//    }
//    NotifyPropertyChanged(nameof(TagCollection));
//}

//private void NotifyPropertyChanged(string propertyName)
//{
//    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
//}

//public event PropertyChangedEventHandler PropertyChanged;
//private async Task VerifyLoop(CancellationToken ct)
//{
//    List<Task<uint>> tasks = new List<Task<uint>>();

//    Logger.Info($"ContentVerifier: Entering Verify Loop");
//    while (!ct.IsCancellationRequested)
//    {
//        try
//        {
//            if (VerifyQueue.Count > 0)
//            {
//                TagInfo info = VerifyQueue.Dequeue();
//                string worktid = info.tid;
//                Logger.Info("Content Verifier Dequeing : {TID}, {TIMESTAMP}", worktid, DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond);
//                info.verifySequenceId = await BuildVerifyOpsAsync(worktid, info);
//                MasterList[info.tid] = info;
//            }
//            else
//                Thread.Sleep(1);
//        }
//        catch (InvalidOperationException)
//        {
//            Logger.Error("Error Accessing WorkQueue in ContentVerifier.WorkerLoop");
//        }
//    }

//    Task.WaitAll(tasks.ToArray());
//    return;
//}
//private async Task WriterLoop(CancellationToken ct)
//{
    //Logger.Info($"TidConsumer: Entering Work Loop");
    //while (!ct.IsCancellationRequested)
    //{
    //    try
    //    {

    //        if (WriteQueue.Count > 0)
    //        {
    //            TagInfo info = WriteQueue.Dequeue();
    //            string worktid = info.tid;
    //            object dataentry = info.dataentry;

    //            Logger.Info("Writer Dequeing : {TID} {TIMESTAMP}", worktid, DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond);
    //            info.writeSequenceId = await BuildWriterOpsAsync(info);
    //            info.dataentry = dataentry;
    //            MasterList[info.tid] = info;
    //            VerifyQueue.Enqueue(info);
    //            Logger.Info("Add to Verify Queue: {TID} {TIMESTAMP}", worktid, DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond);

    //        }
    //        //else
    //        //    Thread.Sleep(1);
    //    }
    //    catch (InvalidOperationException ex)
    //    {
    //        Logger.Error(ex);
    //        return;
    //    }
    //}

    //Logger.Info($"TidConsumer: EXiting Work Loop");
//}