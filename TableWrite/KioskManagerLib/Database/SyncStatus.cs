﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KioskManagerLib.Database
{
    /// <summary>
    /// Enum SyncStatus
    /// </summary>
    public enum SyncStatus : int
    {
        /// <summary>
        /// The unsynced
        /// </summary>
        Unsynced = 0,
        /// <summary>
        /// The syncing
        /// </summary>
        Syncing = 1,
        /// <summary>
        /// The synced
        /// </summary>
        Synced = 2,
    }
}
