﻿namespace KioskManagerLib.Database.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("validationTriggerData")]
    public partial class ValidationTriggerData
    {
        [Key]
        [Column("id")]
        public Guid Id { get; set; }

        [Column("orderId")]
        public Guid OrderId { get; set; }

        /// <summary>
        /// Host time of the trigger
        /// </summary>
        [Column("hostTime")]
        public DateTime HostTime { get; set; }

        /// <summary>
        /// Reader time of the trigger
        /// </summary>
        [Column("readerTime")]
        public DateTime ReaderTime { get; set; }

        /// <summary>
        /// PLC time of the trigger
        /// </summary>
        [Column("plcTime")]
        public DateTime? PlcTime { get; set; }

        /// <summary>
        /// Host time of the result generation
        /// </summary>
        [Column("resultTime")]
        public DateTime ResultTime { get; set; }

        [Column("tid")]
        [StringLength(64)]
        public string TID { get; set; }

        [Column("epc")]
        [StringLength(64)]
        public string EPC { get; set; }

        [Column("rawUser")]
        [StringLength(128)]
        public string RawUser { get; set; }

        [Column("decodedGTIN")]
        [StringLength(64)]
        public string DecodedGtin { get; set; }

        [Column("decodedLot")]
        [StringLength(64)]
        public string DecodedLot { get; set; }

        [Column("decodedExpiry")]
        [StringLength(64)]
        public string DecodedExpiry { get; set; }

        [Column("decodedSerial")]
        [StringLength(64)]
        public string DecodedSerial { get; set; }

        //[Column("epcSerial")]
        //public long? EPCSerial { get; set; }

        [Column("lockState")]
        public int? LockState { get; set; }

        [Column("readCount")]
        public double? ReadCount { get; set; }

        [Column("maxRssi")]
        public double? MaxRSSI { get; set; }

        [Column("flags")]
        public int Flags { get; set; }

        [Column("result")]
        public int Result { get; set; }

        [Column("syncStatus")]
        public int SyncStatus { get; set; }

        [Column("syncSAP")]
        public int SyncSAP { get; set; }
    }
}
