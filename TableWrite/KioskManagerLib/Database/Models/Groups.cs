﻿using System;
using System.Collections.Generic;

namespace KioskManagerLib.Database.Models
{
    public partial class Groups
    {
        public Groups()
        {
            GroupsRights = new HashSet<GroupsRights>();
            UsersGroups = new HashSet<UsersGroups>();
        }

        public long Id { get; set; }
        public string StrName { get; set; }
        public string StrDescription { get; set; }
        public bool? BitValid { get; set; }

        public virtual ICollection<GroupsRights> GroupsRights { get; set; }
        public virtual ICollection<UsersGroups> UsersGroups { get; set; }
    }
}
