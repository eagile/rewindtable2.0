﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EAManagerLib.Database.Models
{
    public class RecoveryData
    {
        public Guid id { get; set; }
        public string TID { get; set; }
        public string EPC { get; set; }
        public DateTime IndexTimeStamp { get; set; }              
        public int WriteSequenceId { get; set; }
        public DateTime WriteReportTimeStamp { get; set; }
        public int WriteSuccess { get; set; }
        public int VerifySequenceId { get; set; }
        public DateTime VerifyReportTimeStamp { get; set; }
        public int VerifySuccess { get; set; }
        public int PcBits { get; set; }
        public string WriteTableBuildData { get; set; }
        public int VerifyLock { get; set; }
        public int VerifyUser { get; set; }
        public string OrigEPC { get; set; }
        public int Scalar { get; set; }

    }
}
