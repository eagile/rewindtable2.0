﻿using System;
using System.Collections.Generic;

namespace KioskManagerLib.Database.Models
{
    public partial class Settings
    {
        public Guid Id { get; set; }
        public Guid DeviceId { get; set; }
        public Guid SkuId { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
        public bool Active { get; set; }
        public int SyncStatus { get; set; }
        public int Sortby { get; set; }
    }
}
