﻿using System;
using System.Collections.Generic;

namespace KioskManagerLib.Database.Models
{
    public partial class Rights
    {
        public Rights()
        {
            GroupsRights = new HashSet<GroupsRights>();
            UsersRights = new HashSet<UsersRights>();
        }

        public long Id { get; set; }
        public string StrName { get; set; }
        public string StrDescription { get; set; }
        public bool? BitValid { get; set; }

        public virtual ICollection<GroupsRights> GroupsRights { get; set; }
        public virtual ICollection<UsersRights> UsersRights { get; set; }
    }
}
