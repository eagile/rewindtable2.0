﻿using System;
using System.Collections.Generic;

namespace KioskManagerLib.Database.Models
{
    public partial class Skus
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
        public int SyncStatus { get; set; }
    }
}
