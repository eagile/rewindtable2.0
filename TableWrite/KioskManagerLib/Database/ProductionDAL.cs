﻿namespace KioskManagerLib.Database
{
    using EA.Impinj;
    using EAManagerLib.Database.Models;
    using EAManagerLib.TableManager.UHF_Factories;
    using EATags;
    using KioskManagerLib.Configuration;
    using KioskManagerLib.Database.DataContexts;
    using KioskManagerLib.Database.Models;
    using LineManager.Stats;
    using Microsoft.Data.SqlClient;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Query.SqlExpressions;
    using Microsoft.EntityFrameworkCore.Storage;
    using SupportClasses;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Xml.Serialization;

    /// <summary>
    /// RemoteKioskManagerDAL is used to connect to remote RLM line and view last result of a TID.
    /// </summary>
    public partial class ProductionDAL
    {
        /// <summary>
        ///  Remote DB connection string
        /// </summary>
        public bool ProductionConnectionStringExists => ProductionContext.ConnectionStringExists;

        /// <summary>
        /// Remote DB Context . contains dbset of lines related to TID. 
        /// </summary>
        /// <returns></returns>
        private ProductionContext GetProductionContext()
        {
            return new ProductionContext();
        }

        /// <summary>
        /// Constuctor may need to build out.
        /// </summary>
        public ProductionDAL()
        {
            
        }

        public async Task<List<TagInfo>> GetRecoveryData( CancellationToken ct)
        {
            List<TagInfo> TempTagInfo = new List<TagInfo>();

            try
            {
                using var db = GetProductionContext();

                List<RecoveryData> TempRecoveryData = db.RecoveryData.ToList();
                

                foreach (var newitem in TempRecoveryData)
                {
                    var addtag = new TagInfo
                    {
                        tid = newitem.TID,
                        epc = newitem.EPC,
                        IndexTimeStamp = newitem.IndexTimeStamp,
                        writeSequenceId = (uint)newitem.WriteSequenceId,
                        writeReportTimeStamp = newitem.WriteReportTimeStamp,
                        writeSuccess = newitem.WriteSuccess != 0,
                        verifySequenceId = (uint)newitem.VerifySequenceId,
                        verifySuccess = newitem.VerifySuccess != 0,
                        verifyLock = newitem.VerifyLock != 0,
                        verifyUser = newitem.VerifyUser != 0,
                        verifyReportTimeStamp = newitem.VerifyReportTimeStamp,
                        PcBits = (ushort)newitem.PcBits,
                        Scalar = newitem.Scalar,
                    };
                   
                    var serializer = new XmlSerializer(typeof(FK4425Model));
                    
                    using (TextReader reader = new StringReader(newitem.WriteTableBuildData))
                    {
                        FK4425Model result = (FK4425Model)serializer.Deserialize(reader);

                        //result.KILL_LOCK = findLockState(result.KILL_LOCK);
                        //result.USER_LOCK = findLockState(r_Item.dataentry.GetType().GetProperty("USER_LOCK").GetValue(r_Item.dataentry, null).ToString());
                        //result.EPC_LOCK = findLockState(r_Item.dataentry.GetType().GetProperty("EPC_LOCK").GetValue(r_Item.dataentry, null).ToString());
                        //result.ACCESS_LOCK = findLockState(r_Item.dataentry.GetType().GetProperty("ACCESS_LOCK").GetValue(r_Item.dataentry, null).ToString());

                    }





                    TempTagInfo.Add(addtag);

                }
            }
            catch(Exception ex)
            {
               // var aa = ex.ToString();
            }

            return TempTagInfo;
        }
        private TagLockState findLockState(string lockstring)
        {

            if (lockstring.ToUpper().Contains("PERMALOCK"))
            {
                //temp
                return TagLockState.Lock;
            }
            else if (lockstring.ToUpper().Contains("?") || string.IsNullOrEmpty(lockstring))
            {
                return TagLockState.None;
            }
            else if (lockstring.ToUpper().Contains("PERMAUNLOCK"))
            {
                return TagLockState.Permaunlock;
            }
            else if (lockstring.ToUpper().Contains("UNLOCK"))
            {
                return TagLockState.Unlock;
            }
            else if (lockstring.ToUpper().StartsWith("LOCK"))
            {
                return TagLockState.Lock;
            }
            return TagLockState.None;

        }


        /// <summary>
        ///  Get all lines. not sure if will be used.
        /// </summary>
        /// <param name="ct"></param>
        /// <returns></returns>
        public async Task<ProductionPartWrapper> GetPartAttribute(string partnumber,CancellationToken ct)
        {
            using var db = GetProductionContext();
            ProductionPartWrapper ProductionInfo = new ProductionPartWrapper()
            {
                PartProductionAttrib = new PartAttributes(),
                PartProductionDB = new PartDatabase(),
            }; 
            ProductionInfo.PartProductionAttrib = db.PartAttributes.FirstOrDefault(p => p.PartId == partnumber);
            ProductionInfo.PartProductionDB =db.PartDatabase.FirstOrDefault(p => p.PartId == partnumber);

            return ProductionInfo;
        }
        public async Task WriteRecoveryData(TagInfo tagdata,int scalar, CancellationToken ct)
        {
            try
            {
                using var db = GetProductionContext();

                if (db.RecoveryData.FirstOrDefault(d => d.TID == tagdata.tid) == null)
                {
                    RecoveryData newline = new RecoveryData();
                    newline.id = new Guid();
                    newline.TID = tagdata.tid;
                    //byte[] ba = Encoding.Default.GetBytes(tagdata.epc);
                    //var hexString = BitConverter.ToString(ba);
                    //hexString = hexString.Replace("-", "");
                    newline.EPC = tagdata.epc.ToString();
                    newline.IndexTimeStamp = tagdata.IndexTimeStamp;
                    newline.OrigEPC = tagdata.OrigEPC;
                    newline.WriteSequenceId = Convert.ToInt32(tagdata.writeSequenceId);
                    newline.WriteReportTimeStamp = tagdata.writeReportTimeStamp;
                    newline.WriteSuccess = Convert.ToInt32(tagdata.writeSuccess);
                    newline.VerifySequenceId = Convert.ToInt32(tagdata.verifySequenceId);
                    newline.VerifySuccess = Convert.ToInt32(tagdata.verifySuccess);
                    newline.VerifyLock = Convert.ToInt32(tagdata.verifyLock);
                    newline.VerifyUser = Convert.ToInt32(tagdata.verifyUser);
                    newline.VerifyReportTimeStamp = tagdata.verifyReportTimeStamp;
                    newline.Scalar = scalar;
                    using (var stringwriter = new System.IO.StringWriter())
                    {
                        var serializer = new XmlSerializer(tagdata.dataentry.GetType());
                        serializer.Serialize(stringwriter, tagdata.dataentry);
                        newline.WriteTableBuildData = stringwriter.ToString();
                    }

                    newline.PcBits = tagdata.PcBits;

                    await db.RecoveryData.AddAsync(newline);
                    await db.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                var aa = ex.ToString();
            
            };
        }

        public async Task<bool> UpdateRecoveryData(TagInfo tagdata,int scalar, CancellationToken ct)
        {
            try
            {
                using var db = GetProductionContext();

               

                var resultitem = db.RecoveryData.FirstOrDefault(r => r.TID == tagdata.tid);
                if(resultitem != null )
                {
                    resultitem.WriteSequenceId = Convert.ToInt32(tagdata.writeSequenceId);
                    resultitem.WriteReportTimeStamp = tagdata.writeReportTimeStamp;
                    resultitem.WriteSuccess = Convert.ToInt32(tagdata.writeSuccess);
                    resultitem.VerifySequenceId = Convert.ToInt32(tagdata.verifySequenceId);
                    resultitem.VerifySuccess = Convert.ToInt32(tagdata.verifySuccess);
                    resultitem.VerifyLock = Convert.ToInt32(tagdata.verifyLock);
                    resultitem.VerifyUser = Convert.ToInt32(tagdata.verifyUser);
                    resultitem.VerifyReportTimeStamp = tagdata.verifyReportTimeStamp;
                    resultitem.Scalar = scalar;
                    db.RecoveryData.Update(resultitem);
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                var aa = ex.ToString();

            };
            return false;
        }

    }

}
