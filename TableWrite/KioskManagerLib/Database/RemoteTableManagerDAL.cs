﻿namespace KioskManagerLib.Database
{
    using KioskManagerLib.Configuration;
    using KioskManagerLib.Database.DataContexts;
    using KioskManagerLib.Database.Models;
    using LineManager.Stats;
    using Microsoft.Data.SqlClient;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Query.SqlExpressions;
    using Microsoft.EntityFrameworkCore.Storage;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    /// <summary>
    /// RemoteKioskManagerDAL is used to connect to remote RLM line and view last result of a TID.
    /// </summary>
    public partial class RemoteTableManagerDAL
    {
        /// <summary>
        ///  Remote DB connection string
        /// </summary>
        public bool KioskConnectionStringExists => RemoteTableContext.ConnectionStringExists;

        /// <summary>
        /// Remote DB Context . contains dbset of lines related to TID. 
        /// </summary>
        /// <returns></returns>
        private RemoteTableContext GetKioskContext()
        {
            return new RemoteTableContext();
        }

        /// <summary>
        /// Constuctor may need to build out.
        /// </summary>
        public RemoteTableManagerDAL()
        {
            
        }

        /// <summary>
        ///  Get all lines. not sure if will be used.
        /// </summary>
        /// <param name="ct"></param>
        /// <returns></returns>
        public async Task<List<ValidationTriggerData>> GetAll(CancellationToken ct)
        {
            using (var db = GetKioskContext())
            {
                return await db.ValidationTriggerData.ToListAsync(ct).ConfigureAwait(false); ;
            }
        }

        /// <summary>
        ///  TID to look for. 
        /// </summary>
        /// <param name="TID"></param>
        /// <param name="ct"></param>
        /// <returns> latest line result for a TID. </returns>
        public MoReturnItems FindMOPath(string moPath)
        {
            MoReturnItems returnMo = new MoReturnItems();
            try
            {

                using var db = GetKioskContext();
                var command = db.Database.GetDbConnection().CreateCommand();
                {
                    db.Database.OpenConnection();
                    command.CommandText = $"Select s.id,s.part,s.Job,m.db1, m.item_size,m.class from econtrol.items s left join econtrol.parts m on s.part = m.id where s.id = '{moPath}'";
                    var readList = command.ExecuteReader();
                    while (readList.Read())
                    {
                        returnMo.Items_ID = (string)readList[0];
                        returnMo.Item_Part = (string)readList[1];
                        returnMo.Item_Job = (string)readList[2];
                        returnMo.Parts_DBb1 = (string)readList[3];
                        returnMo.Parts_ItemSize = (int)readList[4];
                        returnMo.Parts_Class = (string)readList[5];

                        return returnMo;

                    }

                }
            }  
            catch(Exception ex)
            {
                var aa = ex.ToString();
            }
            return returnMo;
        }
        public string ValidationFlagsToString(ValidationFlags flags)
        {
            string s = "";
            foreach (string name in Enum.GetNames(typeof(ValidationFlags)))
            {
                ValidationFlags flag;
                if (Enum.TryParse(name, out flag))
                {
                    if ((flags & flag) == flag)
                    {
                        s += " " + flag.ToString();
                    }
                }
            }
            return s;
        }

    }

}
