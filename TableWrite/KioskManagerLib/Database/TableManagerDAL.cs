﻿using KioskManagerLib.Configuration;
using KioskManagerLib.Database.DataContexts;
using KioskManagerLib.Database.Models;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query.SqlExpressions;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KioskManagerLib.Database
{
    public partial class TableManagerDAL
    {
        public string KioskName { get; }

        public bool KioskNameExists => !string.IsNullOrEmpty(KioskName);

        public bool KioskConnectionStringExists => TableContext.ConnectionStringExists;

        private TableContext GetKioskContext()
        {
            return new TableContext();
        }

        public TableManagerDAL(string kioskId)
        {
            KioskName = kioskId;
        }

        public async Task<bool> EnsureKioskDatabaseCreatedAsync(CancellationToken ct)
        {
            try
            {
                bool created = false;
                using (var db = GetKioskContext())
                {
                    created = await db.Database.EnsureCreatedAsync(ct).ConfigureAwait(false);
                }
                if (created)
                {
                    await CreateDefaultData(ct).ConfigureAwait(false);
                }
                return created;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                throw;
            }
        }

        public async Task<List<Users>> GetUsers(CancellationToken ct)
        {
            using (var db = GetKioskContext())
            {
                List<string> knownRights = new List<string>(Enum.GetNames(typeof(KioskRight)));
                List<Users> users = await db.Users.AsNoTracking()
                       .Include(u => u.UsersRights).ThenInclude(ur => ur.IdRightNavigation)
                       .Include(u => u.UsersGroups).ThenInclude(ug => ug.IdGroupNavigation).ThenInclude(g => g.GroupsRights).ThenInclude(rg => rg.IdRightNavigation)
                       .ToListAsync(ct).ConfigureAwait(false);
                return users;
            }
        }

        public async Task<List<Rights>> GetRights(CancellationToken ct)
        {
            using (var db = GetKioskContext())
            {
                List<string> knownRights = new List<string>(Enum.GetNames(typeof(KioskRight)));
                List<Rights> rights = await db.Rights.Where(p => knownRights.Contains(p.StrName)).AsNoTracking().ToListAsync(ct).ConfigureAwait(false);
                return rights;
            }
        }

        public async Task<UserLogin> GetUserLogin(string userName, string passwordHash, CancellationToken ct)
        {
            using (var db = GetKioskContext())
            {
                Users user = null;
                try
                {
                    if (passwordHash.Length == 0)
                    {
                        user = await db.Users
                               .Include(u => u.UsersRights).ThenInclude(ur => ur.IdRightNavigation)
                               .Include(u => u.UsersGroups).ThenInclude(ug => ug.IdGroupNavigation).ThenInclude(g => g.GroupsRights).ThenInclude(rg => rg.IdRightNavigation)
                               .FirstOrDefaultAsync(p => p.StrName == userName && (p.StrPassword == string.Empty || p.StrPassword == null) && p.BitValid == true).ConfigureAwait(false);
                    }
                    else
                    {
                        user = await db.Users
                               .Include(u => u.UsersRights).ThenInclude(ur => ur.IdRightNavigation)
                               .Include(u => u.UsersGroups).ThenInclude(ug => ug.IdGroupNavigation).ThenInclude(g => g.GroupsRights).ThenInclude(rg => rg.IdRightNavigation)
                               .FirstOrDefaultAsync(p => p.StrName == userName && p.StrPassword == passwordHash && p.BitValid == true).ConfigureAwait(false);
                    }

                }
                catch (Exception ex)
                {
                    throw new DatabaseException(ex);
                }

                if (user == null)
                {
                    throw new InvalidUsernameOrPasswordException();
                }

                List<KioskRight> kioskRights = new List<KioskRight>();

                foreach (var dbGroup in user.UsersGroups)
                {
                    foreach (var dbGroupRight in dbGroup.IdGroupNavigation.GroupsRights)
                    {
                        if (Enum.TryParse(dbGroupRight.IdRightNavigation.StrName, true, out KioskRight kioskRight)
                            && !kioskRights.Contains(kioskRight))
                        {
                            kioskRights.Add(kioskRight);
                        }
                    }
                }

                foreach (var dbUserRight in user.UsersRights)
                {
                    if (Enum.TryParse(dbUserRight.IdRightNavigation.StrName, true, out KioskRight kioskRight)
                        && !kioskRights.Contains(kioskRight))
                    {
                        kioskRights.Add(kioskRight);
                    }
                }

                UserLogin userLogin = new UserLogin();
                userLogin.Username = user.StrName;

                lock (userLogin.KioskRightsSyncRoot)
                {
                    foreach (KioskRight kioskRight in kioskRights)
                    {
                        userLogin.KioskRights.Add(kioskRight);
                    }
                }

                userLogin.IsLoggedIn = true;

                if (!userLogin.IsLoggedIn)
                {
                    throw new UserRightsException();
                }

                if (user.BitLocked)
                {
                    throw new UserLockedException();
                }

                if (user.DteExpires != null && user.DteExpires <= DateTime.UtcNow)
                {
                    throw new UserExpiredException();
                }

                return userLogin;
            }
        }
        
        public async Task SaveUser(Users saveuser , List<UsersRights> newrights, SaveState savestate, CancellationToken ct)
        {


                using (var db = GetKioskContext())
                {
                    switch (savestate)
                    {
                        case SaveState.Add:
                            db.Users.Add(saveuser);
                            break;
                        case SaveState.Update:


                            db.Users.Update(saveuser);
                            break;
                    }

                    await db.SaveChangesAsync(ct).ConfigureAwait(false);

                    db.UsersRights.RemoveRange(saveuser.UsersRights);
                    await db.SaveChangesAsync(ct).ConfigureAwait(false);
                    foreach (var rights in newrights)
                    {
                        var newright = new UsersRights();
                        newright.IdRight = rights.IdRight;
                        newright.IdUser = saveuser.Id;
               
                        db.UsersRights.Add(newright);
                        await db.SaveChangesAsync(ct).ConfigureAwait(false);
                    }
            }
        }

        public async Task<List<Logs>> GetLogItems(CancellationToken ct)
        {
            List<Logs> Logs = null;
            using (var db = GetKioskContext())
            {
                try
                {
                    Logs = await db.Logs.OrderByDescending(v => v.LogTime).AsNoTracking().Take(100).ToListAsync(ct).ConfigureAwait(false);
                }
                catch
                { 

                }
            }
            return Logs;
        }
        public async Task<GetDbSettingsResult> GetSettingsAsync(CancellationToken ct)
        {
            Lines line;
            List<Devices> devices;
            List<Skus> skus;
            List<Settings> settings;

            using (var db = GetKioskContext())
            {
                line = await db.Lines.Where(p => p.Active && p.Name.ToUpper() == KioskName.ToUpper()).AsNoTracking().SingleOrDefaultAsync(ct).ConfigureAwait(false);
                if (line != null)
                {
                    devices = await db.Devices.Where(d => d.Active && d.LineId == line.Id).AsNoTracking().ToListAsync(ct).ConfigureAwait(false);
                    settings = await db.Settings.Where(s => s.Active && devices.Select(d => d.Id).Contains(s.DeviceId)).AsNoTracking().ToListAsync(ct).ConfigureAwait(false);
                }
                else
                {
                    devices = new List<Devices>();
                    settings = new List<Settings>();
                }
                skus = await db.Skus.Where(p => p.Active).AsNoTracking().ToListAsync(ct).ConfigureAwait(false);
            }

            string lineName = line?.Name ?? KioskName;
            List<string> productNames = skus.Select(p => p.Name).ToList();
            List<DbSetting> dbSettings = new List<DbSetting>();

            foreach (Settings setting in settings)
            {
                Devices device = devices.FirstOrDefault(d => setting.DeviceId == d.Id);
                Skus sku = skus.FirstOrDefault(s => setting.SkuId == s.Id);

                DbSetting dbSetting = new DbSetting()
                {
                    KioskName = line?.Name,
                    DeviceName = device?.Name,
                    DeviceType = device?.Type,
                    ProductName = sku?.Name,
                    Name = setting?.Name,
                    Type = setting?.Type,
                    Value = setting?.Value,
                    SortBy = setting?.Sortby ?? 0,
                };
                dbSettings.Add(dbSetting);
            }

            return new GetDbSettingsResult(lineName, productNames, dbSettings);
        }

        public async Task SetSettingsAsync(string kioskName, IList<string> productNames, IList<DbSetting> dbSettings, CancellationToken ct)
        {
            if (string.IsNullOrWhiteSpace(kioskName))
                throw new ArgumentNullException(kioskName);

            using (var db = GetKioskContext())
            using (var transaction = db.Database.BeginTransaction())
            {
                IEnumerable<DbSetting> lineSettings = dbSettings.Where(p => string.Equals(p.KioskName, kioskName, StringComparison.OrdinalIgnoreCase));

                List<Lines> lines = await db.Lines.Where(l => l.Name.ToUpper() == kioskName.ToUpper()).ToListAsync(ct).ConfigureAwait(false);
                if (!lines.Any())
                {
                    db.Lines.Add(new Lines()
                    {
                        Name = kioskName,
                        Active = true,
                        SyncStatus = (int)SyncStatus.Unsynced,
                    });
                    await db.SaveChangesAsync(ct).ConfigureAwait(false);
                    lines = await db.Lines.Where(l => l.Name.ToUpper() == kioskName.ToUpper()).ToListAsync(ct).ConfigureAwait(false);
                }
                Lines line = lines.LastOrDefault(p => p.Active);
                if (line == null)
                {
                    line = lines.LastOrDefault();
                    if (line == null)
                        throw new Exception("Unable to save line record");
                    line.Active = true;
                    line.SyncStatus = (int)SyncStatus.Unsynced;
                }
                foreach (Lines l in lines.Where(p => p != line && p.Active))
                {
                    l.Active = false;
                    l.SyncStatus = (int)SyncStatus.Unsynced;
                }
                await db.SaveChangesAsync(ct).ConfigureAwait(false);

                bool deviceAdded = false;
                List<string> deviceNames = lineSettings.Select(p => p.DeviceName).Distinct(StringComparer.OrdinalIgnoreCase).ToList();
                List<Devices> devices = await db.Devices.Where(d => d.LineId == line.Id).ToListAsync(ct).ConfigureAwait(false);
                foreach (string deviceName in deviceNames)
                {
                    if (!devices.Any(p => string.Equals(p.Name, deviceName, StringComparison.OrdinalIgnoreCase)))
                    {
                        db.Devices.Add(new Devices()
                        {
                            LineId = line.Id,
                            Name = deviceName,
                            Type = lineSettings.First(p => string.Equals(p.DeviceName, deviceName, StringComparison.OrdinalIgnoreCase)).DeviceType,
                            Active = true,
                            SyncStatus = (int)SyncStatus.Unsynced
                        });
                        deviceAdded = true;
                    }
                }
                if (deviceAdded)
                {
                    await db.SaveChangesAsync(ct).ConfigureAwait(false);
                    devices = await db.Devices.Where(d => d.LineId == line.Id).ToListAsync(ct).ConfigureAwait(false);
                }
                List<Devices> activeDevices = new List<Devices>();
                foreach (string deviceName in deviceNames)
                {
                    Devices device = devices.LastOrDefault(p => string.Equals(p.Name, deviceName, StringComparison.OrdinalIgnoreCase) && p.Active);
                    if (device == null)
                    {
                        device = devices.LastOrDefault(p => string.Equals(p.Name, deviceName, StringComparison.OrdinalIgnoreCase));
                        if (device == null)
                            throw new Exception("Unable to save device record");
                        device.Active = true;
                        device.SyncStatus = (int)SyncStatus.Unsynced;
                    }
                    activeDevices.Add(device);
                }
                foreach (Devices d in devices.Where(p => !activeDevices.Contains(p) && p.Active))
                {
                    d.Active = false;
                    d.SyncStatus = (int)SyncStatus.Unsynced;
                }
                await db.SaveChangesAsync(ct).ConfigureAwait(false);

                bool skuAdded = false;
                List<string> allProductNames = lineSettings.Select(p => p.ProductName).Distinct(StringComparer.OrdinalIgnoreCase).ToList();
                foreach (string productName in productNames)
                {
                    if (!allProductNames.Contains(productName, StringComparer.OrdinalIgnoreCase))
                    {
                        allProductNames.Add(productName);
                    }
                }
                allProductNames.Sort();
                List<Skus> skus = await db.Skus.ToListAsync(ct).ConfigureAwait(false);
                foreach (string productName in allProductNames)
                {
                    if (!skus.Any(p => string.Equals(p.Name, productName, StringComparison.OrdinalIgnoreCase)))
                    {
                        db.Skus.Add(new Skus()
                        {
                            Name = productName,
                            Active = true,
                            SyncStatus = (int)SyncStatus.Unsynced,
                        });
                        skuAdded = true;
                    }
                }
                if (skuAdded)
                {
                    await db.SaveChangesAsync(ct).ConfigureAwait(false);
                    skus = await db.Skus.ToListAsync(ct).ConfigureAwait(false);
                }
                List<Skus> activeSkus = new List<Skus>();
                foreach (string productName in allProductNames)
                {
                    Skus sku = skus.LastOrDefault(p => string.Equals(p.Name, productName, StringComparison.OrdinalIgnoreCase) && p.Active);
                    if (sku == null)
                    {
                        sku = skus.LastOrDefault(p => string.Equals(p.Name, productName, StringComparison.OrdinalIgnoreCase));
                        if (sku == null)
                            throw new Exception("Unable to save sku record");
                        sku.Active = true;
                        sku.SyncStatus = (int)SyncStatus.Unsynced;
                    }
                    activeSkus.Add(sku);
                }
                // TODO: Better handling of deactivating skus wihtout conflicting with other lines?
                // May need to add a SKU to Line xref table so we can deactivate skus by line?
                foreach (Skus s in skus.Where(p => activeSkus.Any(ls => string.Equals(p.Name, ls.Name, StringComparison.OrdinalIgnoreCase)) && !activeSkus.Contains(p) && p.Active))
                {
                    s.Active = false;
                    s.SyncStatus = (int)SyncStatus.Unsynced;
                }
                await db.SaveChangesAsync(ct).ConfigureAwait(false);

                // Add a setting
                bool settingAdded = false;
                List<Settings> settings = await db.Settings.Where(s => activeDevices.Select(d => d.Id).Contains(s.DeviceId)).ToListAsync(ct).ConfigureAwait(false);
                foreach (DbSetting ls in lineSettings)
                {
                    List<Settings> matchingSettings = settings.Where(
                        s => string.Equals(ls.ProductName, activeSkus.Single(p => p.Id == s.SkuId).Name, StringComparison.OrdinalIgnoreCase)
                        && string.Equals(ls.DeviceName, activeDevices.Single(d => d.Id == s.DeviceId).Name, StringComparison.OrdinalIgnoreCase)
                        && string.Equals(ls.Name, s.Name, StringComparison.OrdinalIgnoreCase)).ToList();

                    if (!matchingSettings.Any())
                    {
                        db.Settings.Add(new Settings()
                        {
                            DeviceId = activeDevices.Single(p => string.Equals(p.Name, ls.DeviceName, StringComparison.OrdinalIgnoreCase)).Id,
                            SkuId = activeSkus.Single(p => string.Equals(p.Name, ls.ProductName, StringComparison.OrdinalIgnoreCase)).Id,
                            Name = ls.Name,
                            Type = ls.Type,
                            Value = ls.Value,
                            Active = true,
                            SyncStatus = (int)SyncStatus.Unsynced,
                            Sortby = ls.SortBy,
                        });
                        settingAdded = true;
                    }
                }
                if (settingAdded)
                {
                    await db.SaveChangesAsync(ct).ConfigureAwait(false);
                    settings = await db.Settings.Where(s => activeDevices.Select(d => d.Id).Contains(s.DeviceId)).ToListAsync(ct).ConfigureAwait(false);
                }

                // Check for active statuses
                List<Settings> activeSettings = new List<Settings>();
                foreach (DbSetting ls in lineSettings)
                {
                    Settings setting = settings.LastOrDefault(
                        s => string.Equals(ls.ProductName, activeSkus.Single(p => p.Id == s.SkuId).Name, StringComparison.OrdinalIgnoreCase)
                        && string.Equals(ls.DeviceName, activeDevices.Single(d => d.Id == s.DeviceId).Name, StringComparison.OrdinalIgnoreCase)
                        && string.Equals(ls.Name, s.Name, StringComparison.OrdinalIgnoreCase)
                        && s.Active);
                    if (setting == null)
                    {
                        setting = settings.LastOrDefault(
                            s => string.Equals(ls.ProductName, activeSkus.Single(p => p.Id == s.SkuId).Name, StringComparison.OrdinalIgnoreCase)
                            && string.Equals(ls.DeviceName, activeDevices.Single(d => d.Id == s.DeviceId).Name, StringComparison.OrdinalIgnoreCase)
                            && string.Equals(ls.Name, s.Name, StringComparison.OrdinalIgnoreCase));
                        if (setting == null)
                            throw new Exception("Unable to save setting record");
                        setting.Active = true;
                        setting.SyncStatus = (int)SyncStatus.Unsynced;
                    }
                    setting.Value = ls.Value;
                    activeSettings.Add(setting);
                }
                foreach (Settings s in settings.Where(p => !activeSettings.Contains(p) && p.Active))
                {
                    s.Active = false;
                    s.SyncStatus = (int)SyncStatus.Unsynced;
                }
                await db.SaveChangesAsync(ct).ConfigureAwait(false);

                await transaction.CommitAsync().ConfigureAwait(false);
            }
        }

        public async Task WriteLog(Logs log, CancellationToken ct)
        {
            using (var db = GetKioskContext())
            {
                db.Logs.Add(log);
                await db.SaveChangesAsync(ct).ConfigureAwait(false);
            }
        }

        public async Task<double> GetDatabaseSizeMB(CancellationToken ct)
        {
            double size = 0;
            using (var db = GetKioskContext())
            {
                var sqlConn = db.Database.GetDbConnection() as SqlConnection;
                var cmd = new SqlCommand("sp_spaceused")
                {
                    CommandType = CommandType.StoredProcedure,
                    Connection = sqlConn,
                };
                await sqlConn.OpenAsync(ct).ConfigureAwait(false);
                try
                {
                    SqlDataReader reader = await cmd.ExecuteReaderAsync(ct).ConfigureAwait(false);
                    if (reader.HasRows)
                    {
                        while (await reader.ReadAsync(ct).ConfigureAwait(false))
                        {
                            string sizeString = reader["database_size"].ToString().Replace("MB", "", StringComparison.OrdinalIgnoreCase).Trim();
                            if (double.TryParse(sizeString, out double sizeDouble))
                            {
                                size = sizeDouble;
                            }
                        }
                    }
                }
                finally
                {
                    await sqlConn.CloseAsync().ConfigureAwait(false);
                }
            }
            return size;
        }

        public async Task TruncateDatabaseLog(CancellationToken ct)
        {
            using (var db = GetKioskContext())
            {
                int result = await db.Database.ExecuteSqlRawAsync("truncate table logs", ct).ConfigureAwait(false);
            }
        }

        public async Task BackupDatabase(string backupPath, CancellationToken ct)
        {
            using (var db = GetKioskContext())
            {
                string dbname = db.Database.GetDbConnection().Database;
                string backupCommand = @"BACKUP DATABASE [{0}] TO  DISK = N'{1}' WITH NOFORMAT, NOINIT,  NAME = N'Full Database Backup', SKIP, NOREWIND, NOUNLOAD, STATS = 10";
                int result = await db.Database.ExecuteSqlRawAsync(string.Format(backupCommand, dbname, backupPath), ct).ConfigureAwait(false);
            }
        }
    }

    public class GetDbSettingsResult
    {
        public string LineName { get; }
        public List<string> ProductNames { get; }
        public List<DbSetting> DbSettings { get; }
        public GetDbSettingsResult(string lineName, List<string> productNames, List<DbSetting> dbSettings)
        {
            LineName = lineName;
            ProductNames = new List<string>(productNames);
            DbSettings = new List<DbSetting>(dbSettings);
        }
        
    }
}
