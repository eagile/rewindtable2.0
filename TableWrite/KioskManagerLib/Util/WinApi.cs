﻿using System.Runtime.InteropServices;

namespace System
{
    /// <summary>
    /// Class WinApi.
    /// </summary>
    public static class WinApi
    {
        /// <summary>
        /// Requests the Windows timer resolution.
        /// </summary>
        /// <param name="DesiredResolutionInTicks">The desired resolution in ticks.</param>
        public static void RequestWindowsTimerResolution(long DesiredResolutionInTicks)
        {
            long currentResolution = 0;
            NtSetTimerResolution(DesiredResolutionInTicks, true, ref currentResolution);
        }

        /// <summary>
        /// Reverts the Windows timer resolution.
        /// </summary>
        public static void RevertWindowsTimerResolution()
        {
            long currentResolution = 0;
            NtSetTimerResolution(0, false, ref currentResolution);
        }

        /// <summary>
        /// Gets the Windows timer resolution.
        /// </summary>
        /// <returns>WindowsTimerResolution.</returns>
        public static WindowsTimerResolution GetWindowsTimerResolution()
        {
            NtQueryTimerResolution(out long maxResolutionTicks, out long minResolutionTicks, out long currentResolutionTicks);
            return new WindowsTimerResolution()
            {
                MaxResolutionTicks = maxResolutionTicks,
                MinResolutionTicks = minResolutionTicks,
                CurrentResolutionTicks = currentResolutionTicks,
            };
        }

        /// <summary>
        /// Get the Windows idle time in milliseconds
        /// </summary>
        /// <returns>Number of milliseconds since the last input event</returns>
        public static int GetIdleMilliseconds()
        {
            LASTINPUTINFO lastInputInfo = new LASTINPUTINFO()
            {
                cbSize = _sizeOfLastInputInfo,
                dwTime = 0,
            };
            int idleMilliseconds = 0;
            if (GetLastInputInfo(ref lastInputInfo))
            {
                idleMilliseconds = Environment.TickCount - (int)lastInputInfo.dwTime;
            }
            return idleMilliseconds;
        }


        /// <summary>
        /// Set the native timer resolution.
        /// </summary>
        /// <param name="DesiredResolution">The desired resolution.</param>
        /// <param name="SetResolution">if set to <c>true</c> [set resolution].</param>
        /// <param name="CurrentResolution">The current resolution.</param>
        [DllImport("ntdll.dll", EntryPoint = "NtSetTimerResolution", SetLastError = true)]
        private static extern void NtSetTimerResolution(long DesiredResolution, bool SetResolution, ref long CurrentResolution);

        /// <summary>
        /// Get the native timer resolution.
        /// </summary>
        /// <param name="MaximumResolution">The maximum resolution.</param>
        /// <param name="MinimumResolution">The minimum resolution.</param>
        /// <param name="CurrentResolution">The current resolution.</param>
        /// <returns>System.Int32.</returns>
        [DllImport("ntdll.dll", EntryPoint = "NtQueryTimerResolution", SetLastError = true)]
        private static extern int NtQueryTimerResolution(out long MaximumResolution, out long MinimumResolution, out long CurrentResolution);

        /// <summary>
        /// Retrieves the time of the last input event.
        /// </summary>
        /// <param name="plii">A pointer to a LASTINPUTINFO structure that receives the time of the last input event.</param>
        /// <returns>True if the function succeeds, else false</returns>
        [DllImport("user32.dll", EntryPoint = "GetLastInputInfo", SetLastError = true)]
        private static extern bool GetLastInputInfo(ref LASTINPUTINFO plii);

        /// <summary>
        /// The LASTINPUTINFO structure contains the time of the last input.
        /// </summary>
        private struct LASTINPUTINFO
        {
            public uint cbSize;
            public uint dwTime;
        }
        private static readonly uint _sizeOfLastInputInfo = (uint)Marshal.SizeOf(typeof(LASTINPUTINFO));
    }

    /// <summary>
    /// Class WindowsTimerResolution.
    /// </summary>
    public class WindowsTimerResolution
    {
        /// <summary>
        /// Gets or sets the maximum resolution ticks.
        /// </summary>
        /// <value>The maximum resolution ticks.</value>
        public long MaxResolutionTicks { get; set; }
        /// <summary>
        /// Gets or sets the minimum resolution ticks.
        /// </summary>
        /// <value>The minimum resolution ticks.</value>
        public long MinResolutionTicks { get; set; }
        /// <summary>
        /// Gets or sets the current resolution ticks.
        /// </summary>
        /// <value>The current resolution ticks.</value>
        public long CurrentResolutionTicks { get; set; }
    }
}
