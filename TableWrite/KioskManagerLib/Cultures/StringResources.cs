﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Resources;

namespace EAManagerLib.Cultures
{
    public static class StringResources
    {
        public static CultureInfo CurrentCulture { get; set; }

        public static string GetLocalizedString(ResourceManager resourceManager, string name)
        {
            try
            {
                return resourceManager.GetString(name, CurrentCulture ?? CultureInfo.GetCultureInfo("en-US"));
            }
            catch
            {
                return null;
            }
        }
    }
}
